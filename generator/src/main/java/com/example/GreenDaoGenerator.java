package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Index;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

/**
 * author：SummerRC  348934952@qq.com
 * date:2015.10.17
 * description:除了前期 DepartmentEntity、UserEntity、GroupEntity、MessageEntity四个实体类，其他所有实体类均为后期所加，
 * 而且，Base的GreenDao版本较低（1.3.7），自动生成代码需要依赖一个Java SE工程，截至目前，GreenDao最新版本2.0.0
 * 也可以将Generator类放在android工程直接运行该类，也能自动生成代码。
 *
 * 【备注】维护是一件很麻烦的事情，添加新的实体类时很容易影响到TCP相关的实体类，建议如果只是单纯添加实体类，那么可以先提前备份一下
 * DepartmentEntity、UserEntity、GroupEntity、MessageEntity四个类，每次运行都会重新生成代码
 * 其中UserEntity、 GroupEntity 继承PeerEntity,由于 UserEntity、 GroupEntity是自动生成，PeerEntity会有重复字段，
 * 所以每次生成之后要处理下UserEntity、 GroupEntity的成员变量，PeerEntity成员变量名与子类统一。
 */
public class GreenDaoGenerator {
    private static String entityPath = "com.yuxip.DB.entity";

    public static void main(String[] args) throws Exception {
        int dbVersion = 12;
        Schema schema = new Schema(dbVersion, "com.yuxip.DB.dao");

        schema.enableKeepSectionsByDefault();
        addDepartment(schema);
        addUserInfo(schema);
        addGroupInfo(schema);
        addMessage(schema);
        addSessionInfo(schema);
        addExpression(schema);
        addApplyFriend(schema);
        addApplyGroup(schema);
        addLastChat(schema);
        addGroupAnnouncement(schema);
        addNodesEntity(schema);
        addStoryBookMarkEntity(schema);

        /** 绝对路径,根据自己的路径设定， 例子如下 */
        String path = "E:/code/git/android/youxi/app/src/main/java";
        new DaoGenerator().generateAll(schema, path);
    }

    private static void addDepartment(Schema schema) {
        Entity department = schema.addEntity("DepartmentEntity");
        department.setTableName("Department");
        department.setClassNameDao("DepartmentDao");
        department.setJavaPackage(entityPath);

        department.addIdProperty().autoincrement();     //方法addIdProperty()默认生成主键_id，而方法autoincrement()只能对long类型的主键使用
        department.addIntProperty("departId").unique().notNull().index();
        department.addStringProperty("departName").unique().notNull().index();
        department.addIntProperty("priority").notNull();
        department.addIntProperty("status").notNull();

        department.addIntProperty("created").notNull();
        department.addIntProperty("updated").notNull();

        department.setHasKeepSections(true);
    }

    private static void addUserInfo(Schema schema) {
        Entity userInfo = schema.addEntity("UserEntity");
        userInfo.setTableName("UserInfo");
        userInfo.setClassNameDao("UserDao");
        userInfo.setJavaPackage(entityPath);

        userInfo.addIdProperty().autoincrement();
        userInfo.addIntProperty("peerId").unique().notNull().index();
        userInfo.addIntProperty("gender").notNull();
        userInfo.addStringProperty("mainName").notNull();
        /** 这个可以自动生成pinyin */
        userInfo.addStringProperty("pinyinName").notNull();
        userInfo.addStringProperty("realName").notNull();
        userInfo.addStringProperty("avatar").notNull();
        userInfo.addStringProperty("phone").notNull();
        userInfo.addStringProperty("email").notNull();
        userInfo.addIntProperty("departmentId").notNull();

        userInfo.addIntProperty("status").notNull();
        userInfo.addIntProperty("created").notNull();
        userInfo.addIntProperty("updated").notNull();

        userInfo.setHasKeepSections(true);

        //todo 索引还没有设定
        // 一对一 addToOne 的使用
        // 支持protobuf
        // schema.addProtobufEntity();
    }

    private static void addGroupInfo(Schema schema) {
        Entity groupInfo = schema.addEntity("GroupEntity");
        groupInfo.setTableName("GroupInfo");
        groupInfo.setClassNameDao("GroupDao");
        groupInfo.setJavaPackage(entityPath);

        groupInfo.addIdProperty().autoincrement();
        groupInfo.addIntProperty("peerId").unique().notNull();
        groupInfo.addIntProperty("groupType").notNull();
        groupInfo.addStringProperty("mainName").notNull();
        groupInfo.addStringProperty("avatar").notNull();
        groupInfo.addIntProperty("creatorId").notNull();
        groupInfo.addIntProperty("userCnt").notNull();

        groupInfo.addStringProperty("userList").notNull();
        groupInfo.addIntProperty("version").notNull();
        groupInfo.addIntProperty("status").notNull();
        groupInfo.addIntProperty("created").notNull();
        groupInfo.addIntProperty("updated").notNull();

        groupInfo.setHasKeepSections(true);
    }

    private static void addMessage(Schema schema) {
        Entity message = schema.addEntity("MessageEntity");
        message.setTableName("Message");
        message.setClassNameDao("MessageDao");
        message.setJavaPackage(entityPath);

        message.implementsSerializable();
        message.addIdProperty().autoincrement();
        Property msgProId = message.addIntProperty("msgId").notNull().getProperty();
        message.addIntProperty("fromId").notNull();
        message.addIntProperty("toId").notNull();
        /** 是不是需要添加一个sessionkey标示一下，登陆的用户在前面 */
        Property sessionPro = message.addStringProperty("sessionKey").notNull().getProperty();
        message.addStringProperty("content").notNull();
        message.addIntProperty("msgType").notNull();
        message.addIntProperty("displayType").notNull();

        message.addIntProperty("status").notNull().index();
        message.addIntProperty("created").notNull().index();
        message.addIntProperty("updated").notNull();

        Index index = new Index();
        index.addProperty(msgProId);
        index.addProperty(sessionPro);
        index.makeUnique();
        message.addIndex(index);

        message.setHasKeepSections(true);
    }

    private static void addSessionInfo(Schema schema) {
        Entity sessionInfo = schema.addEntity("SessionEntity");
        sessionInfo.setTableName("Session");
        sessionInfo.setClassNameDao("SessionDao");
        sessionInfo.setJavaPackage(entityPath);

        /** point to userId/groupId need sessionType 区分 */
        sessionInfo.addIdProperty().autoincrement();
        sessionInfo.addStringProperty("sessionKey").unique().notNull(); //.unique()
        sessionInfo.addIntProperty("peerId").notNull();
        sessionInfo.addIntProperty("peerType").notNull();

        sessionInfo.addIntProperty("latestMsgType").notNull();
        sessionInfo.addIntProperty("latestMsgId").notNull();
        sessionInfo.addStringProperty("latestMsgData").notNull();

        sessionInfo.addIntProperty("talkId").notNull();
        sessionInfo.addIntProperty("created").notNull();
        sessionInfo.addIntProperty("updated").notNull();

        sessionInfo.setHasKeepSections(true);
    }

    /**
     * 自定义表情相关
     */
    private static void addExpression(Schema schema) {
        Entity expressionEntity = schema.addEntity("ExpressionEntity");
        expressionEntity.setTableName("Expression");
        expressionEntity.setClassNameDao("ExpressionDao");
        expressionEntity.setJavaPackage(entityPath);

        expressionEntity.addIdProperty().autoincrement();
        expressionEntity.addStringProperty("label").notNull();          //标签
        expressionEntity.addStringProperty("content").notNull();        //内容
        expressionEntity.setHasKeepSections(true);
    }

    /**
     * 申请好友相关
     */
    private static void addApplyFriend(Schema schema) {
        Entity applyFriendEntity = schema.addEntity("ApplyFriendEntity");
        applyFriendEntity.setTableName("ApplyFriend");
        applyFriendEntity.setClassNameDao("ApplyFriendDao");
        applyFriendEntity.setJavaPackage(entityPath);

        applyFriendEntity.addIdProperty().autoincrement();
        applyFriendEntity.addStringProperty("uid").notNull();            //申请者id
        applyFriendEntity.addStringProperty("nickname");                 //用户名
        applyFriendEntity.addStringProperty("portrait");                 //头像
        applyFriendEntity.addStringProperty("agree").notNull();          //true : 同意  false : 未处理   reject : 拒绝
        applyFriendEntity.addStringProperty("msgdata");                  //附加消息
        applyFriendEntity.addStringProperty("applyTime");                //申请时间
        applyFriendEntity.addStringProperty("gender");

        applyFriendEntity.setHasKeepSections(true);
    }

    private static void addApplyGroup(Schema schema) {
        Entity applyGroupEntity = schema.addEntity("ApplyGroupEntity");
        applyGroupEntity.setTableName("ApplyGroup");
        applyGroupEntity.setClassNameDao("ApplyGroupDao");
        applyGroupEntity.setJavaPackage(entityPath);

        applyGroupEntity.addIdProperty().autoincrement();
        applyGroupEntity.addStringProperty("groupId").notNull();                //  群id
        applyGroupEntity.addStringProperty("portrait");                         //  头像
        applyGroupEntity.addStringProperty("entityType").notNull();             //  family /  story
        applyGroupEntity.addStringProperty("entityId").notNull();               //  家族id  /  剧id
        applyGroupEntity.addStringProperty("entityName").notNull();             //  家族名  /   剧名

        applyGroupEntity.addStringProperty("creator").notNull();                //  创建者
        applyGroupEntity.addStringProperty("creatorId").notNull();              //  创建者id
        applyGroupEntity.addStringProperty("createTime").notNull();             //  创建时间
        applyGroupEntity.addStringProperty("type").notNull();                   //  0: 申请 1: 邀请

        applyGroupEntity.addStringProperty("applyId").notNull();                //  申请者id
        applyGroupEntity.addStringProperty("applyName").notNull();              //  申请者名字
        applyGroupEntity.addStringProperty("applyPortrait").notNull();          //  申请者头像
        applyGroupEntity.addStringProperty("agree").notNull();                  //  true ：已处理（同意）  false : 未处理 reject ； 拒绝

        applyGroupEntity.setHasKeepSections(true);
    }

    private static void addLastChat(Schema schema) {
        Entity lastChatEntity = schema.addEntity("LastChatEntity");
        lastChatEntity.setTableName("LastChat");
        lastChatEntity.setClassNameDao("LastChatDao");
        lastChatEntity.setJavaPackage(entityPath);

        lastChatEntity.addIdProperty().autoincrement();
        lastChatEntity.addStringProperty("currentSessionKey").unique().notNull();     //1_id / 2_id
        lastChatEntity.addStringProperty("content");                //内容
        lastChatEntity.setHasKeepSections(true);
    }

    private static void addGroupAnnouncement(Schema schema) {
        Entity groupAnnouncementEntity = schema.addEntity("GroupAnnouncementEntity");
        groupAnnouncementEntity.setTableName("GroupAnnouncement");
        groupAnnouncementEntity.setClassNameDao("GroupAnnouncementDao");
        groupAnnouncementEntity.setJavaPackage(entityPath);

        groupAnnouncementEntity.addIdProperty().autoincrement();
        groupAnnouncementEntity.addStringProperty("groupId").notNull();
        groupAnnouncementEntity.addStringProperty("title");
        groupAnnouncementEntity.addStringProperty("content");
        groupAnnouncementEntity.addStringProperty("dateTime");
        groupAnnouncementEntity.setHasKeepSections(true);
    }

    private static void addNodesEntity(Schema schema) {
        Entity nodesEntity = schema.addEntity("NodesEntity");
        nodesEntity.setTableName("Nodes");
        nodesEntity.setClassNameDao("NodesDao");
        nodesEntity.setJavaPackage(entityPath);

        nodesEntity.addLongProperty("_id").primaryKey().autoincrement().notNull();
        nodesEntity.addStringProperty("id").notNull();
        nodesEntity.addStringProperty("ishighlight");
        nodesEntity.addStringProperty("name");
        nodesEntity.addStringProperty("type");
        nodesEntity.addStringProperty("url");
        nodesEntity.addStringProperty("loadType");
        nodesEntity.setHasKeepSections(true);
    }

    private static void addStoryBookMarkEntity(Schema schema) {
        Entity nodesEntity = schema.addEntity("StoryBookMarkEntity");
        nodesEntity.setTableName("StoryBookMark");
        nodesEntity.setClassNameDao("StoryBookMarkDao");
        nodesEntity.setJavaPackage(entityPath);

        nodesEntity.addIdProperty().autoincrement();
        nodesEntity.addStringProperty("storyId").notNull();
        nodesEntity.addStringProperty("markItem");
        nodesEntity.addStringProperty("totalCount");
        nodesEntity.addStringProperty("fromMsgId");
        nodesEntity.setHasKeepSections(true);
    }
}
