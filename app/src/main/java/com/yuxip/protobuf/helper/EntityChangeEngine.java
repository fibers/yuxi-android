package com.yuxip.protobuf.helper;

import android.text.TextUtils;

import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.SessionEntity;
import com.yuxip.config.DBConstant;

/**
 * @author : yingmu on 15-1-5.
 * @email : yingmu@im.com.
 */
public class EntityChangeEngine {

    public static SessionEntity getSessionEntity(MessageEntity msg){
        SessionEntity sessionEntity = new SessionEntity();

        // [图文消息] [图片] [语音]
        sessionEntity.setLatestMsgData(msg.getMessageDisplay());
        sessionEntity.setUpdated(msg.getUpdated());
        sessionEntity.setCreated(msg.getUpdated());
        sessionEntity.setLatestMsgId(msg.getMsgId());
        //sessionEntity.setPeerId(msg.getFromId());
        sessionEntity.setTalkId(msg.getFromId());
        sessionEntity.setPeerType(msg.getSessionType());
        sessionEntity.setLatestMsgType(msg.getMsgType());
        return  sessionEntity;
    }

    // todo enum
    // 组建与解析统一地方，方便以后维护
    public static String getSessionKey(int peerId,int sessionType){
        String sessionKey = sessionType + "_" + peerId;
        return sessionKey;
    }

    public static String[] spiltSessionKey(String sessionKey){
        if(TextUtils.isEmpty(sessionKey)){
            throw new IllegalArgumentException("spiltSessionKey error,cause by empty sessionKey");
        }
        String[] sessionInfo = sessionKey.split("_",2);
        return sessionInfo;
    }

    //add by guoq-s
    public static int getSessionType (String sessionKey) {
        try {
            String[] results = spiltSessionKey(sessionKey);
            return  Integer.valueOf(results[0]);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return DBConstant.SESSION_TYPE_ERROR;
    }

    public static String getSessionPeerId (String sessionKey) {
        try {
            String[] results = spiltSessionKey(sessionKey);
            return  results[1];
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return "0";
    }
    //add by guoq-e
}
