package com.yuxip.upyun.block.api.listener;

public interface CompleteListener {
	void result(boolean isComplete, String result, String error);
}
