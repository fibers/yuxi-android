package com.yuxip.imservice.entity;

/**
 * Handler
 * Created by SummerRC on 2015/5/17.
 */
public  class MessageType {
    public final static int HANDLER_MESSAGE_SUCCESS = 100;
    public final static int HANDLER_MESSAGE_ERROR = 101;

    public final static int HANDLER_MESSAGE_APPLY_GROUP_SUCCESS = 102;
    public final static int HANDLER_MESSAGE_APPLY_GROUP_ERROR = 103;
}
