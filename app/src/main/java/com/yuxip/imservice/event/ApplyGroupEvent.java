package com.yuxip.imservice.event;

/**
 * Created by SummerRC on 2015/5/20.
 */

public class ApplyGroupEvent {

    public Object object;
    public Event event;
    public String type;

    public enum  Event{
        NONE,

        MSG_APPLY_GROUP,
        MSG_INVITED_GROUP
    }
}