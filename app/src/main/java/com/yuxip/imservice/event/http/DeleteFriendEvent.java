package com.yuxip.imservice.event.http;

/**
 * Created by SummerRC on 2015/9/29.
 * description:删除好友的事件,用于通知FriendChatActivity finish掉自己
 */

public class DeleteFriendEvent {
    private String peerId;

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }
}