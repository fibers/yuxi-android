package com.yuxip.imservice.event;

import com.yuxip.JsonBean.TopicDetailsJsonBean;

/**
 * Created by ly on 2015/10/14.
 */
public class SquareCommentEvent {

    public TopicDetailsJsonBean.CommentEntity commentEntity;
    public String commentId;
    public String totalCommentCount;
    public String favorCount;
    public String isFavored;


    public EventType eventType;

    /**
     *  类型 TYPE_RET_CHILD_COMMENT 重新设置评论的实体类
     *  类型 TYPE_REMOVE_COMMENT 删除集合中某些评论
     *  类型 TYPE_RESET_COMMENT_CONTENT 集合中某些内容的改变
     *  类型 TYPE_DELETE_STORY  删除剧或自戏
     */
    public enum EventType {
        TYPE_RESET_CHILD_COMMENT,TYPE_REMOVE_COMMENT,TYPE_RESET_COMMENT_CONTENT,TYPE_FINISH_ACTIVITY,
        TYPE_DELETE_STORY_TOPIC
    }


}
