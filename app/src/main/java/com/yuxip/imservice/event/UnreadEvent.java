package com.yuxip.imservice.event;

import com.yuxip.imservice.entity.UnreadEntity;

public class UnreadEvent {

    public UnreadEntity entity;
    public Event event;

    public UnreadEvent(){}
    public UnreadEvent(Event e){
        this.event = e;
    }

    public enum Event {
        UNREAD_MSG_LIST_OK,
        UNREAD_MSG_RECEIVED,
        UNREAD_MSG_SYSTEM,

        SESSION_READED_UNREAD_MSG
    }
}
