package com.yuxip.imservice.event;

import com.yuxip.ui.adapter.album.ImageItem;

import java.util.List;

/**
 * Edited by SummerRC
 * description:自定义相册选择图片的事件（时间接受者有：家族、剧、个人聊天、发布话题）
 */
public class SelectEvent {
    private List<ImageItem> list;
    private Event event;

    public SelectEvent(List<ImageItem> list, Event event) {
        this.list = list;
        this.event = event;
    }

    public List<ImageItem> getList() {
        return list;
    }

    public void setList(List<ImageItem> list) {
        this.list = list;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public enum Event {
        TYPE_RELEASE_TOPIC, TYPE_CHAT, TYPE_INVALID
    }
}
