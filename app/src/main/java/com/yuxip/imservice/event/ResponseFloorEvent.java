package com.yuxip.imservice.event;

/**
 * Created by Ly on 2015/11/19.
 */
public class ResponseFloorEvent {
    public String netResponse;
    public Event eventType;
    public enum Event {
        TYPE_DELETE_CHILD_COMMENT_SUCCESS,TYPE_DELETE_CHILD_COMMENT_FAIL,
        TYPE_LOAD_COMMENTS_SUCCESS,TYPE_LOAD_COMMENTS_FAIL,
        TYPE_COMMENT_FLOOR_SUCCESS,TYPE_COMMENT_FLOOR_FAIL,
    }
}
