package com.yuxip.imservice.event.http;

/**
 * Created by SummerRC on 2015/10/6.
 * description:好友分组的事件
 */

public class FriendGroupEvent {
    public Event event;

    public enum Event {
        UPDATE_SUCCESS, UPDATE_FAIL, REFRESH
    }
}