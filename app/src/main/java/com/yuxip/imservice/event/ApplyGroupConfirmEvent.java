package com.yuxip.imservice.event;

/**
 * Created by SummerRC on 2015/5/20.
 */

public class ApplyGroupConfirmEvent {

    public Object object;
    public Event event;

    public enum  Event{
        NONE,

        MSG_APPLY_GROUP_CONFIRM,
        MSG_INVITE_GROUP_CONFIRM
    }
}