package com.yuxip.imservice.event.http;

import java.util.ArrayList;

/**
 * Created by SummerRC on 2015/10/2.
 * description:未读通知事件
 */
public class UnreadNotifyEvent {
    public ArrayList<?> list;
    public EventType eventType;

    public enum EventType {
        UNREAD_NOTIFY_EVENT
    }
}
