package com.yuxip.imservice.event;

/**
 * Created by Administrator on 2015/10/23.
 */
public class CreateStoryTopicEvent {
    public Event eventType;

    public enum Event {
        TYPE_CREATE_TOPIC, TYPE_CREATE_STORY, TYPE_CREATE_PLAY, TYPE_CREATE_FAMILY, TYPE_CREATE_ALLTYPE, TYPE_DISMISS_FAMILY, TYPE_EXIT_FAMILY
    }
}
