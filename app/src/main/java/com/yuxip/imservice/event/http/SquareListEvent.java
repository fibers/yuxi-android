package com.yuxip.imservice.event.http;

/**
 * Created by ZQF on 2015/10/20.
 * 广场数据
 */
public class SquareListEvent {
    private Event event;
    private Object object;

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }

    /** 返回结果 */
    public enum Event {
        HEAD, LIST, REQUEST_FAIL
    }
}
