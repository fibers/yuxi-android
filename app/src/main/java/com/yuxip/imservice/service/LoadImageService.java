package com.yuxip.imservice.service;

import android.app.IntentService;
import android.content.Intent;
import android.graphics.Bitmap;

import com.yuxip.config.SysConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.event.MessageEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.utils.Logger;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import de.greenrobot.event.EventBus;

public class LoadImageService extends IntentService {

    private static Logger logger = Logger.getLogger(LoadImageService.class);

    private UpImgUtil   upImgUtil;

    public LoadImageService(){
        super("LoadImageService");
    }

    public LoadImageService(String name) {
        super(name);
    }

    /**
     * This method is invoked on the worker thread with a request to process.
     * Only one Intent is processed at a time, but the processing happens on a
     * worker thread that runs independently from other application logic.
     * So, if this code takes a long time, it will hold up other requests to
     * the same IntentService, but it will not hold up anything else.
     * When all requests have been handled, the IntentService stops itself,
     * so you should not call {@link #stopSelf}.
     *
     * @param intent The value passed to {@link
     *               android.content.Context#startService(android.content.Intent)}.
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        final ImageMessage messageInfo = (ImageMessage)intent.getSerializableExtra(SysConstant.UPLOAD_IMAGE_INTENT_PARAMS);
            String result = null;
            Bitmap bitmap;
            try {

                upImgUtil = new UpImgUtil();

                Date date = new Date();
                /** 获取当前时间并且进一步转化为字符串 */
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
                String upPicName = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

                upImgUtil.upLoadPictureWithPath(new HeadImgListener() {
                    @Override
                    public void notifyImgUploadFinished(String url) {

                        if (url == null) {
                            EventBus.getDefault().post(new MessageEvent(MessageEvent.Event.IMAGE_UPLOAD_FAILD
                                    ,messageInfo));
                            return;
                        }
                        //add by guo
                        logger.i("upload image succcess,imageUrl is %s",url);
                        String imageUrl = url;
                        messageInfo.setUrl(imageUrl);
                        EventBus.getDefault().post(new MessageEvent(
                                MessageEvent.Event.IMAGE_UPLOAD_SUCCESS
                                ,messageInfo));

                    }
                }, messageInfo.getPath(), upPicName);
                /*
                File file= new File(messageInfo.getPath());
                if(file.exists() && FileUtil.getExtensionName(messageInfo.getPath()).toLowerCase().equals(".gif"))
                {
                    MoGuHttpClient httpClient = new MoGuHttpClient();
                    SystemConfigSp.instance().init(getApplicationContext());
                    logger.d("image path is ", messageInfo.getPath());
                    Log.d("httpClient.uploadImage3", "url is " + SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.MSFSSERVER));

                    result = httpClient.uploadImage3(SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.MSFSSERVER), FileUtil.File2byte(messageInfo.getPath()), messageInfo.getPath());
                }
                else
                {


                    logger.d("image path is ", messageInfo.getPath());
                    bitmap = PhotoHelper.revitionImage(messageInfo.getPath());
                    if (null != bitmap) {
                        MoGuHttpClient httpClient = new MoGuHttpClient();
                        byte[] bytes = PhotoHelper.getBytes(bitmap);
                        Log.d("httpClient.uploadImage3", "url is " + SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.MSFSSERVER));
                        result = httpClient.uploadImage3(SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.MSFSSERVER), bytes, messageInfo.getPath());
                    }
                }

                if (TextUtils.isEmpty(result)) {
                    logger.i("upload image faild,cause by result is empty/null");
                    EventBus.getDefault().post(new MessageEvent(MessageEvent.Event.IMAGE_UPLOAD_FAILD
                    ,messageInfo));
                } else {
                    logger.i("upload image succcess,imageUrl is %s",result);
                    String imageUrl = result;
                    messageInfo.setUrl(imageUrl);
                    EventBus.getDefault().post(new MessageEvent(
                            MessageEvent.Event.IMAGE_UPLOAD_SUCCESS
                            ,messageInfo));
                }*/
            } catch (Exception e) {
                logger.e(e.getMessage());
            }
    }
}
