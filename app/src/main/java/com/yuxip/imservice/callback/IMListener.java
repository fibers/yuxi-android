package com.yuxip.imservice.callback;

public interface IMListener<T> {
    void onSuccess(T response);

    void onFaild();

    void onTimeout();
}
