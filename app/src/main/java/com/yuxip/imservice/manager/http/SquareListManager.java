package com.yuxip.imservice.manager.http;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.BannersJsonBean;
import com.yuxip.JsonBean.SquareListJsonBean;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.event.http.SquareListEvent;

import de.greenrobot.event.EventBus;

/**
 * Edited by SummerRC on 2015/10/23.
 * description:广场列表的管理类
 */
public class SquareListManager {

    private static SquareListManager instance;

    private SquareListManager() {
    }

    public static SquareListManager getInstance() {
        if (instance == null) {
            instance = new SquareListManager();
        }
        return instance;
    }

    /**
     * 获取顶部轮播图片
     */
    public void requestHead(String uid) {
        ReqImgBanners(uid);
    }

    private void ReqImgBanners(String uid) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GetBanners, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        SquareListEvent squareListEvent = new SquareListEvent();
                        squareListEvent.setEvent(SquareListEvent.Event.HEAD);
                        BannersJsonBean banners = new Gson().fromJson(response, BannersJsonBean.class);
                        squareListEvent.setObject(banners);
                        EventBus.getDefault().post(squareListEvent);
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        SquareListEvent squareListEvent = new SquareListEvent();
                        squareListEvent.setEvent(SquareListEvent.Event.HEAD);
                        squareListEvent.setObject(e.toString());
                        EventBus.getDefault().post(squareListEvent);
                    }
                });
    }

    public void requestSquareList(String uid, String token, String index, String count) {
        ReqSquareData(uid, token, index, count);
    }

    /**
     * 获取广场
     */
    private void ReqSquareData(String uid, String token, String index, String count) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("token", token);
        params.addParams("index", index);
        params.addParams("count", count);
        OkHttpClientManager.postAsy(ConstantValues.SQUARE_LIST, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        SquareListEvent squareListEvent = new SquareListEvent();
                        squareListEvent.setEvent(SquareListEvent.Event.REQUEST_FAIL);
                        squareListEvent.setObject(e.toString());
                        EventBus.getDefault().post(squareListEvent);
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveSquareResult(response);
                    }
                });
    }

    /**
     * 处理返回数据
     */
    private void onReceiveSquareResult(String response) {
        try {
            SquareListJsonBean squareBean = new Gson().fromJson(response, SquareListJsonBean.class);
            if ("100".equals(squareBean.getStatus().getResult())) {
                SquareListEvent squareListEvent = new SquareListEvent();
                squareListEvent.setEvent(SquareListEvent.Event.LIST);
                squareListEvent.setObject(squareBean.getSquareList());
                EventBus.getDefault().post(squareListEvent);
            }
        }catch (Exception e){
            SquareListEvent squareListEvent = new SquareListEvent();
            squareListEvent.setEvent(SquareListEvent.Event.REQUEST_FAIL);
            EventBus.getDefault().post(squareListEvent);
        }

    }

}
