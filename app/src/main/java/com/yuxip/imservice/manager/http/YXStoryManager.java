package com.yuxip.imservice.manager.http;

import com.squareup.okhttp.Request;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.MyGroupInfoDao;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMManager;
import com.yuxip.utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by SummerRC on 2015/10/20.
 * description:我的剧的管理类
 */
public class YXStoryManager extends IMManager {
    private Map<Integer, StoryParentEntity> storyMap;           //<剧id，剧实体>
    private Map<Integer, MyGroupInfoDao> myGroupInfoDaoMap;     //<剧群id，群信息>
    private static YXStoryManager instance;
    private Logger logger;

    private YXStoryManager() {
        storyMap = new ConcurrentHashMap<>();
        myGroupInfoDaoMap = new ConcurrentHashMap<>();
        logger = Logger.getLogger(YXStoryManager.class);
    }

    /**
     * 线程安全的单例模式
     *
     * @return 返回一个实例
     */
    public static YXStoryManager instance() {
        if (instance == null) {
            synchronized (YXStoryManager.class) {
                if (instance == null) {
                    instance = new YXStoryManager();
                }
            }
        }
        return instance;
    }

    /**
     * 获取所有我的剧
     */
    public void getMyStory() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GETMYSTROY, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        storyMap.clear();
                        myGroupInfoDaoMap.clear();
                        try {
                            JSONObject object = new JSONObject(response);
                            String id = object.getString("result");
                            if (id.equals("1")) {
                                JSONArray array = object.getJSONArray("storys");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject obj = array.getJSONObject(i);
                                    StoryParentEntity myStoryEntity = new StoryParentEntity();
                                    myStoryEntity.setStoryId(Integer.valueOf(obj.get("id").toString()));
                                    myStoryEntity.setPortrait(obj.get("portrait").toString());
                                    myStoryEntity.setIntro(obj.get("intro").toString());
                                    myStoryEntity.setApplynums(obj.get("applynums").toString());
                                    myStoryEntity.setPraisenum(obj.get("praisenum").toString());
                                    myStoryEntity.setTitle(obj.get("title").toString());
                                    myStoryEntity.setCreatorid(obj.get("creatorid").toString());
                                    myStoryEntity.setCreatorname(obj.get("creatorname").toString());
                                    myStoryEntity.setRelation(obj.get("relation").toString());
                                    myStoryEntity.setIspraisedByUser(obj.get("ispraisedbyuser").toString());
                                    myStoryEntity.setCopyright(obj.get("copyright").toString());
                                    myStoryEntity.setRole(obj.get("role").toString());
                                    myStoryEntity.setNickname(obj.get("nickname").toString());
                                    myStoryEntity.setWordsnum(obj.get("wordsnum").toString());
                                    setMyGroupInfoDaoMap(myStoryEntity, obj.getJSONArray("groups"), Integer.valueOf(obj.get("id").toString()));
                                    storyMap.put(Integer.valueOf((String) obj.get("id")), myStoryEntity);
                                }
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        logger.e(e.toString());
                    }
                }
        );
    }

    /**
     * 根据网络取回的内容设定群的信息
     *
     * @param array   array
     * @param storyId storyId
     */
    private void setMyGroupInfoDaoMap(StoryParentEntity myStoryEntity, JSONArray array, int storyId) {
        try {
            for (int i = 0; i < array.length(); i++) {
                int groupId = Integer.valueOf(array.getJSONObject(i).getString("groupid"));
                String isPlay = array.getJSONObject(i).getString("isplay");
                YXGroupTypeManager.instance().setGroupType(groupId, isPlay);
                MyGroupInfoDao myGroupInfoDao = new MyGroupInfoDao();
                myGroupInfoDao.setGroupId(groupId);
                myGroupInfoDao.setTitle(array.getJSONObject(i).getString("title"));
                myGroupInfoDao.setIsPlay(Integer.valueOf(isPlay));
                myGroupInfoDao.setStoryId(storyId);
                myGroupInfoDaoMap.put(myGroupInfoDao.getGroupId(), myGroupInfoDao);
                myStoryEntity.addMyGroupInfoDaoItem(myGroupInfoDao);
            }
        } catch (Exception e) {
            logger.e(e.toString());
        }
    }

    /**
     * @param storyId  剧id
     * @param typeList 群类型的集合：审核群——0  水聊群——1   对戏群——2   评论群——3
     * @return 群id集合
     */
    public Set<Integer> getGroupIdsByStoryIdAndGroupTypes(int storyId, List<Integer> typeList) {
        Set<Integer> set = new HashSet<>();
        if (storyMap.containsKey(storyId)) {
            List<MyGroupInfoDao> MyGroupInfoDaoList = storyMap.get(storyId).getMyGroupInfoDaoList();
            for (MyGroupInfoDao myGroupInfoDao : MyGroupInfoDaoList) {
                for (Integer type : typeList) {
                    if (myGroupInfoDao.getIsPlay() == type) {
                        set.add(myGroupInfoDao.getGroupId());
                    }
                }
            }
        }
        return set;
    }


    /**
     * @param storyId 剧id
     *                群类型的集合：审核群——0  水聊群——1   对戏群——2   评论群——3
     * @return 群id
     */
    public String getGroupIdsByStoryIdAndGroupType(int storyId, int type) {
        String groupId = "";
        if (storyMap.containsKey(storyId)) {
            List<MyGroupInfoDao> MyGroupInfoDaoList = storyMap.get(storyId).getMyGroupInfoDaoList();
            for (MyGroupInfoDao myGroupInfoDao : MyGroupInfoDaoList) {
                if (myGroupInfoDao.getIsPlay() == type) {
                    groupId = myGroupInfoDao.getGroupId() + "";
                }
            }
        }
        return groupId;
    }

    public int getStoryId(int groupId) {
        if (getStoryByGroupid(groupId) == null) {
            return -1;
        }
        return getStoryByGroupid(groupId).getStoryId();
    }

    public StoryParentEntity getStoryByGroupid(int groupid) {
        StoryParentEntity result = null;
        MyGroupInfoDao groupInfo = myGroupInfoDaoMap.get(groupid);
        if (groupInfo != null) {
            result = storyMap.get(groupInfo.getStoryId());
        }
        return result;
    }

    public String getGroupPortrait(String groupId) {
        Integer id = Integer.valueOf(groupId);
        if (YXFamilyManager.instance().isMember(id)) {
            return YXFamilyManager.instance().getFamilyPortrait(id);
        } else if (myGroupInfoDaoMap.containsKey(id)) {
            return getStoryByGroupid(id).getPortrait();
        } else {
            return "";
        }
    }

    /**
     * 判断用户是否在对戏群
     *
     * @return 是否在对戏群
     */
    public boolean isInDuixiGroup(String groupId) {
        int storyId = getStoryId(Integer.valueOf(groupId));
        if (storyId == -1) {     //说明这个剧不再我的剧列表
            return false;
        }
        if (!storyMap.containsKey(storyId)) {
            return false;
        } else {
            StoryParentEntity storyParentEntity = storyMap.get(storyId);
            if (storyParentEntity.getMyGroupInfoDaoList() != null) {
                for (MyGroupInfoDao myGroupInfoDao : storyParentEntity.getMyGroupInfoDaoList()) {
                    if (myGroupInfoDao.getIsPlay() == 2) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @Override
    public void doOnStart() {
    }

    @Override
    public void reset() {
    }
}
