package com.yuxip.imservice.manager.http;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/11/27.
 */
public class CatchNewUserManager {

    private static CatchNewUserManager instance;
    private List<String> listSend = new ArrayList<>();//已发送好友请求的uid


    public static CatchNewUserManager getInstance(){
        if(instance ==null){
            instance = new CatchNewUserManager();
        }
        return instance;
    }


    public void addNewUserId(String uid){
        if(!listSend.contains(uid)){
            listSend.add(uid);
        }
    }

    /**
     *  判断是否已经发送了好友请求
     * @return
     */
    public boolean hasSendApply(String uid){
        if(listSend.contains(uid)){
            return true;
        }else{
            return  false;
        }
    }

}
