package com.yuxip.imservice.manager;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.entity.UnreadNotifyEntity;
import com.yuxip.imservice.entity.MessageType;
import com.yuxip.imservice.event.http.UnreadNotifyEvent;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.protobuf.helper.EntityChangeEngine;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import de.greenrobot.event.EventBus;

/**
 * 未读通知的处理相关管理类，线程安全的单例模式
 * Created by SummerRC on 2015/6/10.
 * <p/>
 * 之前在用户登录成功或者网络OK之后调用GetUnreadNotify接口，但这样做会引起两个问题：
 * （1）SplashActivity界面停留时间过长导致用户体验不好
 * （2）SplashActivity跳转到MainActivity时会存在短暂黑屏的现象
 * 因此现在在MainActivity的onCreate()方法调用GetUnreadNotify接口
 */
public class YXUnreadNotifyManager extends IMManager {
    private Logger logger = Logger.getLogger(YXUnreadNotifyManager.class);
    private static YXUnreadNotifyManager YXUnreadNotifyManager;
    private ArrayList<UnreadNotifyEntity> list = new ArrayList<>();
    private DBInterface dbInterface;

    private YXUnreadNotifyManager() {
        dbInterface = DBInterface.instance();
    }

    public static YXUnreadNotifyManager instance() {
        if (YXUnreadNotifyManager == null) {
            synchronized (YXUnreadNotifyManager.class) {
                if (YXUnreadNotifyManager == null) {
                    YXUnreadNotifyManager = new YXUnreadNotifyManager();
                }
            }
        }
        return YXUnreadNotifyManager;
    }

    /**
     * 获取服务器关于好友通知的数据
     */
    public void getUnreadNotify() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("token", "");
        OkHttpClientManager.postAsy(ConstantValues.GetUnreadNotify, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONArray array = new JSONArray(response);
                            if (array.length() == 0) {
                                return;
                            }
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);
                                String type = object.getString("type");
                                JSONArray array_notify_list = object.getJSONArray("notifylist");
                                for (int j = 0; j < array_notify_list.length(); j++) {
                                    UnreadNotifyEntity entity = new UnreadNotifyEntity();
                                    entity.setType(type);
                                    entity.setId(array_notify_list.getJSONObject(j).getString("id"));
                                    entity.setFromid(array_notify_list.getJSONObject(j).getString("fromid"));
                                    entity.setToid(array_notify_list.getJSONObject(j).getString("toid"));
                                    entity.setDatetime(array_notify_list.getJSONObject(j).getString("datetime"));
                                    entity.setGroupid(array_notify_list.getJSONObject(j).getString("groupid"));
                                    entity.setResult(array_notify_list.getJSONObject(j).getString("result"));
                                    entity.setIsfamily(array_notify_list.getJSONObject(j).getString("isfamily"));
                                    entity.setMsgdata(array_notify_list.getJSONObject(j).getString("msgdata"));
                                    list.add(entity);
                                }

                                if (list.size() > 0) {
                                    UnreadNotifyEvent unreadNotifyEvent = new UnreadNotifyEvent();
                                    unreadNotifyEvent.list = getList();
                                    unreadNotifyEvent.eventType = UnreadNotifyEvent.EventType.UNREAD_NOTIFY_EVENT;
                                    EventBus.getDefault().post(unreadNotifyEvent);
                                }
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {

                    }
                });
    }

    public void getAddFriendEntity(final int uid, final int pid, final String msgData, final Handler handler) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid + "");
        params.addParams("personid", pid + "");
        params.addParams("token", "2");
        OkHttpClientManager.postAsy(ConstantValues.GetPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        ApplyFriendEntity entity = new ApplyFriendEntity();
                        JSONObject object = obj.getJSONObject("personinfo");
                        entity.setAgree("false");
                        entity.setUid(object.get("id").toString());
                        entity.setNickname(object.get("nickname").toString());
                        entity.setPortrait(object.get("portrait").toString());
                        entity.setGender(object.get("gender").toString());
                        entity.setMsgdata(msgData);
                        entity.setApplyTime((DateUtil.getTimes() / 1000) + "");

                        /** 如果数据库已存在申请请求，且用户未同意，则不再插入数据库 */
                        ApplyFriendEntity applyFriendEntity = dbInterface.getApplyFriendEntityByUidAndAgree(entity.getUid(), "false");
                        if (applyFriendEntity == null) {
                            dbInterface.batchInsertOrUpdateApplyFriendEntity(entity);
                            IMUnreadMsgManager.instance().setUnreadAddReq(EntityChangeEngine.getSessionKey(pid, DBConstant.SESSION_TYPE_SINGLE));
                            /** 通知方法调用者数据库操作完成 */
                            Message msg = new Message();
                            msg.what = MessageType.HANDLER_MESSAGE_SUCCESS;
                            msg.arg1 = uid;
                            msg.arg2 = pid;
                            handler.sendMessage(msg);
                        }

                    } else {
                        handler.sendEmptyMessage(MessageType.HANDLER_MESSAGE_ERROR);
                    }
                } catch (Exception e) {
                    Logger.getLogger(ApplyGroupEntity.class).e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                handler.sendEmptyMessage(MessageType.HANDLER_MESSAGE_ERROR);
            }
        });
    }

    @Override
    public void doOnStart() {
    }

    @Override
    public void reset() {
    }

    private ArrayList<UnreadNotifyEntity> getList() {
        ArrayList<UnreadNotifyEntity> l = new ArrayList<>(Arrays.asList(new UnreadNotifyEntity[list.size()]));
        Collections.copy(l, list);
        list.clear();
        return l;
    }


    /**
     * @param type 类型 0: 申请  1: 邀请
     */
    public void setApplyGroupEntity(final int adminId, final int applyId, final int groupId, final String type, final Handler handler, final Context context) {
        ApplyGroupEntity entity = new ApplyGroupEntity();
        entity.setAgree("false");
        entity.setCreatorId(adminId + "");
        entity.setApplyId(applyId + "");
        entity.setGroupId(groupId + "");
        entity.setType(type);

        try {
            /** 如果数据库已存在申请/邀请请求，且管理员未同意，则不再插入数据库 */
            ApplyGroupEntity applyGroupEntity = dbInterface.getApplyGroupEntityByExtras(
                    entity.getGroupId(), entity.getCreatorId(), entity.getApplyId(), "false");
            if (applyGroupEntity != null) {
                return;
            }
            //设定未读的系统消息的数目
            IMUnreadMsgManager.instance().setUnreadAddReq(EntityChangeEngine.getSessionKey(groupId, DBConstant.SESSION_TYPE_GROUP));
            /***
             * 获取消息列表
             */
            getGroupInfo(groupId, entity, handler, applyId);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void getGroupInfo(int groupId, final ApplyGroupEntity entity, final Handler handler, final int applyId) {
        final String loginId = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("groupid", groupId + "");
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetGroupDetail, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("result").equals("1")) {
                                JSONObject object = obj.getJSONObject("groupdetails");
                                String mYEntityType = object.getString("type");
                                if (mYEntityType.equals("1")) {                                 //家族
                                    entity.setEntityType("family");
                                } else if (mYEntityType.equals("2")) {                          //剧
                                    entity.setEntityType("story");
                                }
                                entity.setEntityId(object.get("refid").toString());             //家族/剧id
                                entity.setEntityName(object.get("refname").toString());               //家族/剧名字
                                entity.setPortrait(object.get("portrait").toString());          //头像
                                entity.setCreator(object.get("creator").toString());            //创建者
                                entity.setCreatorId(object.get("creatorid").toString());        //创建者id
                                //entity.setCreateTime(object.get("createtime").toString());      //创建者时间
                            }
                            getAppleInfo(applyId, loginId, entity, handler);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        handler.sendEmptyMessage(MessageType.HANDLER_MESSAGE_ERROR);
                    }
                });
    }

    /**
     * 获取申请者的信息
     * 申请者的id
     */
    private void getAppleInfo(int applyId, final String loginId, final ApplyGroupEntity entity, final Handler handler) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("personid", applyId + "");
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetPersonInfo, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("result").equals("1")) {
                                JSONObject personinfo = obj.getJSONObject("personinfo");
                                entity.setApplyName(personinfo.get("nickname").toString());               //家族/剧名字
                                entity.setApplyPortrait(personinfo.get("portrait").toString());          //头像
                            }
                            entity.setCreateTime((DateUtil.getTimes() / 1000) + "");
                            dbInterface.batchInsertOrUpdateApplyGroupEntity(entity);
                            Message msg = new Message();
                            msg.what = MessageType.HANDLER_MESSAGE_APPLY_GROUP_SUCCESS;
                            msg.obj = entity;
                            handler.sendMessage(msg);
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        handler.sendEmptyMessage(MessageType.HANDLER_MESSAGE_ERROR);
                    }
                });
    }

    /**
     * 之前在用户登录成功或者网络OK之后调用GetUnreadNotify接口，但这样做会引起两个问题：
     * （1）SplashActivity界面停留时间过长导致用户体验不好
     * （2）SplashActivity跳转到MainActivity时会存在短暂黑屏的现象
     * 因此现在在MainActivity的onCreate()方法调用GetUnreadNotify接口
     *
     * @param ctx ctx
     */
    public void onNormalLoginOk(Context ctx) {
       /* Intent intent = new Intent();
        intent.setAction(ConstantValues.BROADCAST_LOGIN_OK);
        ctx.sendBroadcast(intent);
        onLocalLoginOk();
        onLocalNetOk();*/
    }

    private void onLocalLoginOk() {
        getUnreadNotify();
    }

    private void onLocalNetOk() {
    }

    public void clearList() {
        if (list != null) {
            list.clear();
        }
    }
}
