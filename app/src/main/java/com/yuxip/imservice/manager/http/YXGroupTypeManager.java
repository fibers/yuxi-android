package com.yuxip.imservice.manager.http;

import com.yuxip.config.ConstantValues;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by SummerRC on 2015/10/20.
 * description:GroupType的管理类
 */
public class YXGroupTypeManager {
    private Map<Integer, String> groupTypeMap;          //groupId -- type
    private static YXGroupTypeManager instance;

    private YXGroupTypeManager() {
        groupTypeMap = new ConcurrentHashMap<>();
    }

    /**
     * 线程安全的单例模式
     *
     * @return 返回一个实例
     */
    public static YXGroupTypeManager instance() {
        if (instance == null) {
            synchronized (YXFamilyManager.class) {
                if (instance == null) {
                    instance = new YXGroupTypeManager();
                }
            }
        }
        return instance;
    }

    public void setGroupType(int groupId, String groupType) {
        groupTypeMap.put(groupId, groupType);
    }

    public void deleteGroupType(int groupId) {
        groupTypeMap.remove(groupId);
    }

    public boolean isCommentGroup(int groupId) {
        if (groupTypeMap.containsKey(groupId)) {
            String type = groupTypeMap.get(groupId);
            return type.equals(ConstantValues.GROUP_TYPE_COMMENT);
        } else {
            return true;
        }
    }

    public boolean isShenHeGroup(int groupId) {
        if (groupTypeMap.containsKey(groupId)) {
            String type = groupTypeMap.get(groupId);
            return type.equals(ConstantValues.GROUP_TYPE_SHENHE);
        } else {
            return false;
        }
    }

    public boolean isStoryGroupOrFamilyGroup(int groupId) {
        if (groupTypeMap.containsKey(groupId)) {
            String type = groupTypeMap.get(groupId);
            return type.equals(ConstantValues.GROUP_TYPE_SHENHE) ||
                    type.equals(ConstantValues.GROUP_TYPE_SHUILIAO) ||
                    type.equals(ConstantValues.GROUP_TYPE_PLAY) ||
                    type.equals(ConstantValues.GROUP_TYPE_FAMILY);
        } else {
            return false;
        }
    }


    /**
     * 通过群id获取群类型
     *
     * @param groupId groupId
     * @return 在ConstantValues类中去找群类型的定义
     */
    public String isGroupType(int groupId) {
        if (groupTypeMap.containsKey(groupId)) {
            return groupTypeMap.get(groupId);
        }
        return "";
    }
}
