package com.yuxip.imservice.manager.http;

import android.content.Context;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.imservice.event.ResponseFloorEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/11/19.
 */
public class ResponseFloorManager {

    private Context context;
    private static ResponseFloorManager instance;
    public static ResponseFloorManager getInstance(Context context){
        if(instance == null)
            instance = new ResponseFloorManager(context);
        return instance;
    }

    private ResponseFloorManager(Context context){
        super();
        this.context = context;
    }

    public  void RequestCommentList(int type,String topicid,String storyid,String commentId,int index,int count,String requestUrl) {
        if(IMLoginManager.instance().getLoginId()<1000){
            Toast.makeText(context,context.getResources().getString(R.string.square_login_status_error), Toast.LENGTH_LONG).show();
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        if(type==0){
            params.addParams("storyID",storyid);
        }else{
            params.addParams("topicID",topicid);
        }
        params.addParams("commentId",commentId);
        params.addParams("index",index+"");
        params.addParams("count", count + "");
        OkHttpClientManager.postAsy(requestUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                          triggerEvent(ResponseFloorEvent.Event.TYPE_LOAD_COMMENTS_FAIL);
                    }

                    @Override
                    public void onResponse(String response) {
                        triggerEvent(ResponseFloorEvent.Event.TYPE_LOAD_COMMENTS_SUCCESS,response);
                    }
                });
    }




    //删除二级评论
    public void deleteChildComment(String resourceID,String resourceType,String deleteUrl){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",resourceID);
        if(resourceType!=null&&resourceType.equals("1")) {
            params.addParams("resourceType","2");
        }else{
            params.addParams("resourceType","3");
        }
        OkHttpClientManager.postAsy(deleteUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        triggerEvent(ResponseFloorEvent.Event.TYPE_DELETE_CHILD_COMMENT_FAIL);
                    }

                    @Override
                    public void onResponse(String response) {
                        triggerEvent(ResponseFloorEvent.Event.TYPE_DELETE_CHILD_COMMENT_SUCCESS,response);
                    }
                });
    }


    /**
     *  评论  用来回复该话题
     */
    public void commentSquareResource(boolean responseFloor,String commentId,String childTopicId,String toUserId,String content,String commentUrl){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        if(responseFloor){
            params.addParams("resourceID",commentId);
            params.addParams("resourceType","1");
        }else{
            params.addParams("resourceID",childTopicId);
            params.addParams("resourceType","2");
        }

        params.addParams("toUserID",toUserId);
        params.addParams("content", content);
        OkHttpClientManager.postAsy(commentUrl , params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        triggerEvent(ResponseFloorEvent.Event.TYPE_COMMENT_FLOOR_FAIL);
                    }

                    @Override
                    public void onResponse(String response) {
                        triggerEvent(ResponseFloorEvent.Event.TYPE_COMMENT_FLOOR_SUCCESS,response);

                    }
                });
    }




    private void triggerEvent(ResponseFloorEvent.Event event,String netResponse){
        ResponseFloorEvent responseFloorEvent = new ResponseFloorEvent();
        responseFloorEvent.eventType = event;
        responseFloorEvent.netResponse = netResponse;
        EventBus.getDefault().post(responseFloorEvent);
    }

    private void triggerEvent(ResponseFloorEvent.Event event){
        ResponseFloorEvent responseFloorEvent = new ResponseFloorEvent();
        responseFloorEvent.eventType = event;
        EventBus.getDefault().post(responseFloorEvent);
    }

}
