package com.yuxip.imservice.manager.http;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.RemarkNameJsonBean;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.RemarkNameEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.YXUnreadNotifyManager;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SummerRC on 2015/10/19.
 * description:昵称
 */
public class RemarkNameManager {
    private List<RemarkNameEntity> remarkNameEntityList;
    private Logger logger;

    private RemarkNameManager() {
        remarkNameEntityList = new ArrayList<>();
        logger = Logger.getLogger(RemarkNameManager.class);
    }

    private static RemarkNameManager remarkNameManager;

    public static RemarkNameManager getInstance() {
        if (remarkNameManager == null) {
            synchronized (YXUnreadNotifyManager.class) {
                if (remarkNameManager == null) {
                    remarkNameManager = new RemarkNameManager();
                }
            }
        }
        return remarkNameManager;
    }

    /**
     * 通过网络获取用户昵称
     */
    public void getUsersNameFromNet() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetUsersName, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            RemarkNameJsonBean remarkNameJsonBean = new Gson().fromJson(response, RemarkNameJsonBean.class);
                            if (remarkNameJsonBean.getResult().equals("1")) {
                                remarkNameEntityList.clear();
                                remarkNameEntityList = remarkNameJsonBean.getUsers();
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        logger.e(e.toString());
                    }
                });
    }

    /**
     * 根据用户id更新用户备注名
     * @param userId        用户id
     * @param nickName      备注
     */
    public void editNickNameByUserId(String userId, String nickName) {
        for (RemarkNameEntity entity : remarkNameEntityList) {
            if(entity.getUserid().equals(userId)) {
                entity.setNickname(nickName);
            }
        }
    }


    /**
     * 根据用户id获取用户备注名
     * @param userId        用户id
     * @return              备注
     */
    public String getNickNameByUserId(String userId) {
        for (RemarkNameEntity entity : remarkNameEntityList) {
            if(entity.getUserid().equals(userId)) {
                return entity.getNickname();
            }
        }
        return "";
    }
}
