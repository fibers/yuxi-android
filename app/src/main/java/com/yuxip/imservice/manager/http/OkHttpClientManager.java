package com.yuxip.imservice.manager.http;

import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;
import com.google.gson.internal.$Gson$Types;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by SummerRC on 2015/10/14.
 * description:OKHttp的封装，暴露的两个访问网络的接口均为异步请求。
 */
public class OkHttpClientManager {
    private static OkHttpClientManager mInstance;
    private OkHttpClient mOkHttpClient;
    private Handler mDelivery;
    private Gson mGSon;

    /**
     * 单例模式，保证只有一个OkHttpClient对象
     *
     * @return OkHttpClientManager
     */
    public static OkHttpClientManager getInstance() {
        if (mInstance == null) {
            synchronized (OkHttpClientManager.class) {
                if (mInstance == null) {
                    mInstance = new OkHttpClientManager();
                }
            }
        }
        return mInstance;
    }

    private OkHttpClientManager() {
        mOkHttpClient = new OkHttpClient();
        /** cookie enabled **/
        mOkHttpClient.setCookieHandler(new CookieManager(null, CookiePolicy.ACCEPT_ORIGINAL_SERVER));
        mDelivery = new Handler(Looper.getMainLooper());
        mGSon = new Gson();
    }

    /**
     * 异步的get请求
     *
     * @param url
     * @param callback
     */
    private void _getAsyn(String url, final ResultCallback callback) {
        final Request request = new Request.Builder()
                .url(url)
                .build();
        deliveryResult(callback, request);
    }

    /**
     * 异步的post请求
     *
     * @param url      url
     * @param callback callback
     * @param params   List
     */
    private void _postAsy(String url, final ResultCallback callback, List<Param> params) {
        Request request = buildPostRequest(url, params);
        deliveryResult(callback, request);
    }

    /**
     * 异步下载文件
     *
     * @param url      文件地址
     * @param picPath  本地文件存储的文件夹
     * @param callback 回调函数
     */
    private void _downloadAsyn(final String url, final String picPath, final ResultCallback callback) {
        final Request request = new Request.Builder().url(url).build();
        final Call call = mOkHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(final Request request, final IOException e) {
                sendFailedStringCallback(request, e, callback);
            }

            @Override
            public void onResponse(Response response) {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len;
                FileOutputStream fos = null;
                try {
                    is = response.body().byteStream();
                    File file = new File(picPath);
                    fos = new FileOutputStream(file);
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                    }
                    fos.flush();
                    /** 如果下载文件成功，第一个参数为文件的绝对路径 */
                    sendSuccessResultCallback(file.getAbsolutePath(), callback);
                } catch (IOException e) {
                    sendFailedStringCallback(response.request(), e, callback);
                } finally {
                    try {
                        if (is != null) is.close();
                    } catch (IOException e) {

                    }
                    try {
                        if (fos != null) fos.close();
                    } catch (IOException e) {
                    }
                }

            }
        });
    }

    /**
     * @param callback 回调类
     * @param request  封装的请求
     */
    private void deliveryResult(final ResultCallback callback, Request request) {
        mOkHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(final Request request, final IOException e) {
                sendFailedStringCallback(request, e, callback);
            }

            @Override
            public void onResponse(final Response response) {
                try {
                    final String string = response.body().string();
                    if (callback.mType == String.class) {
                        sendSuccessResultCallback(string, callback);
                    } else {
                        Object o = mGSon.fromJson(string, callback.mType);
                        sendSuccessResultCallback(o, callback);
                    }
                } catch (Exception e) {
                    sendFailedStringCallback(response.request(), e, callback);
                }
            }
        });
    }

    /**
     * 请求失败执行实现类的回掉
     */
    private void sendFailedStringCallback(final Request request, final Exception e, final ResultCallback callback) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null)
                    callback.onError(request, e);
            }
        });
    }

    /**
     * 请求成功执行实现类的回掉
     */
    private void sendSuccessResultCallback(final Object object, final ResultCallback callback) {
        mDelivery.post(new Runnable() {
            @Override
            public void run() {
                if (callback != null) {
                    callback.onResponse(object);
                }
            }
        });
    }

    /**
     * @param url    接口地址
     * @param params 参数列表
     * @return Request
     */
    private Request buildPostRequest(String url, List<Param> params) {
        if (params == null) {
            params = new ArrayList<>();
        }
        FormEncodingBuilder builder = new FormEncodingBuilder();
        for (Param param : params) {
            builder.add(param.key, param.value);
        }
        RequestBody requestBody = builder.build();
        return new Request.Builder().url(url)
                .post(requestBody)
                .build();
    }

    private static class Param {
        private String key;
        private String value;

        private Param(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    /**
     * --------------------------------------------对外公布的方法和接口、类  start-------------------------------------------------------
     */

    public static void getAsyn(String url, ResultCallback callback) {
        getInstance()._getAsyn(url, callback);
    }

    public static void postAsy(String url, List<Param> params, final ResultCallback callback) {
        getInstance()._postAsy(url, callback, params);
    }

    public static void downLoadAsy(String url, String picPath, final ResultCallback callback) {
        getInstance()._downloadAsyn(url, picPath, callback);
    }




    public static abstract class ResultCallback<T> {
        private Type mType;

        public ResultCallback() {
            mType = getSuperclassTypeParameter(getClass());
        }

        static Type getSuperclassTypeParameter(Class<?> subclass) {
            Type superclass = subclass.getGenericSuperclass();
            if (superclass instanceof Class) {
                throw new RuntimeException("Missing type parameter.");
            }
            ParameterizedType parameterizedType = (ParameterizedType) superclass;
            return $Gson$Types.canonicalize(parameterizedType.getActualTypeArguments()[0]);
        }

        public abstract void onResponse(T response);

        public abstract void onError(Request request, Exception e);
    }

    public static class RequestParams {
        private List<Param> params = new ArrayList<>();

        public void addParams(String key, String value) {
            params.add(new Param(key, value));
        }

        public List<Param> getParams() {
            return params;
        }
    }
/**
 * --------------------------------------------对外公布的方法和接口、类  end--------------------------------------------------------
 */
}
