package com.yuxip.imservice.manager.http;

import android.widget.ProgressBar;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.JsonBean.FG_FriendJsonBean;
import com.yuxip.JsonBean.FG_FriendsGroupJsonBean;
import com.yuxip.JsonBean.FG_GSon_GetFriendListWithGroup;
import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.event.http.FriendGroupEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.thread.AsyncTaskBase;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by SummerRC on 2015/9/2.
 * description:好友分组对应的Module
 */
public class FriendGroupManager {
    private List<FG_GroupJsonBean> group;                   //分组信息
    private List<List<FG_FriendJsonBean>> child;            //所有好友信息
    private List<Integer> num;                              //每个分组的成员数量


    private static FriendGroupManager instance;


    /**
     * 线程安全的单例模式
     *
     * @return 返回一个实例
     */
    public static FriendGroupManager getInstance() {
        if (instance == null) {
            synchronized (FriendGroupManager.class) {
                if (instance == null) {
                    instance = new FriendGroupManager();
                }
            }
        }
        return instance;
    }

    /**
     * 构造函数私有化
     */
    private FriendGroupManager() {
        group = new ArrayList<>();
        child = new ArrayList<>();
        num = new ArrayList<>();
    }

    /**
     * 访问网络获取好友列表
     */
    public void getFriendListWithGroupOK(final ProgressBar progressBar) {
        OkHttpClientManager.RequestParams requestParams = new OkHttpClientManager.RequestParams();
        requestParams.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        requestParams.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetFriendListWithGroup, requestParams.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {

                    }

                    @Override
                    public void onResponse(String response) {
                        new AsyncTaskLoading(progressBar).execute(response);
                    }
                }
        );
    }


    /**
     * 好友量比较大的时候，JSon解析很耗时
     */
    private class AsyncTaskLoading extends AsyncTaskBase {
        public AsyncTaskLoading(ProgressBar progressBar) {
            super(progressBar);
        }

        @Override
        protected Integer doInBackground(String... params) {
            String content = params[0];
            try {
                FG_GSon_GetFriendListWithGroup fg_GSon_GetFriendListWithGroup = new Gson().fromJson(content, FG_GSon_GetFriendListWithGroup.class);
                if (fg_GSon_GetFriendListWithGroup != null && fg_GSon_GetFriendListWithGroup.getFriendgroups().size() > 0) {
                    updateData(fg_GSon_GetFriendListWithGroup);
                }
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            /** 后台操作完成之后 */
            FriendGroupEvent friendGroupEvent = new FriendGroupEvent();
            if (result == 1) {
                friendGroupEvent.event = FriendGroupEvent.Event.UPDATE_SUCCESS;
            } else {
                friendGroupEvent.event = FriendGroupEvent.Event.UPDATE_FAIL;
            }
            EventBus.getDefault().post(friendGroupEvent);
        }
    }

    /**
     * @param fg_GSon_GetFriendListWithGroup Json转换后的数据，用于更新group child num信息
     */
    private void updateData(FG_GSon_GetFriendListWithGroup fg_GSon_GetFriendListWithGroup) {
        group.clear();
        child.clear();
        List<FG_FriendsGroupJsonBean> list = fg_GSon_GetFriendListWithGroup.getFriendgroups();
        FG_GroupJsonBean fg_groupJsonBean;
        FG_FriendsGroupJsonBean fg_friendsGroupJsonBean;
        for (int i = 0; i < list.size(); i++) {
            fg_groupJsonBean = new FG_GroupJsonBean();                  //分组实体类的对象
            fg_friendsGroupJsonBean = list.get(i);                      //每个分组对应的成员信息
            fg_groupJsonBean.setGroupid(fg_friendsGroupJsonBean.getGroupid());
            fg_groupJsonBean.setGroupname(fg_friendsGroupJsonBean.getGroupname());
            group.add(fg_groupJsonBean);                                //填充分组数据
            child.add(fg_friendsGroupJsonBean.getGroupmembers());       //填充分组成员信息
            num.add(fg_friendsGroupJsonBean.getGroupmembers().size());
        }
    }

    /**
     * @return 分组信息
     */
    public List<FG_GroupJsonBean> getGroup() {
        return group;
    }


    /**
     * @return 分组的好友信息
     */
    public List<List<FG_FriendJsonBean>> getChild() {
        return child;
    }

    /**
     * @return 分组对应的成员数量
     */
    public List<Integer> getNum() {
        num.clear();
        for (int i = 0; i < child.size(); i++) {
            num.add(child.get(i).size());
        }
        return num;
    }

    /**
     * @return 分组名字集合
     */
    public List<String> getGroupNameList() {
        List<String> names = new ArrayList<>();
        for (int i = 0; i < group.size(); i++) {
            names.add(group.get(i).getGroupname());
        }
        return names;
    }

    /**
     * @return 分组id集合
     */
    public List<String> getGroupIdList() {
        List<String> ids = new ArrayList<>();
        for (int i = 0; i < group.size(); i++) {
            ids.add(group.get(i).getGroupid());
        }
        return ids;
    }

    /**
     * @param uid 好友id
     * @return 所属群组名
     */
    public FG_GroupJsonBean getGroupByUid(String uid) {
        for (int i = 0; i < child.size(); i++) {
            for (FG_FriendJsonBean friend : child.get(i)) {
                if (friend.getUserid().equals(uid)) {
                    return group.get(i);
                }
            }
        }
        return null;
    }

    /**
     * @param uid 好友id
     * @return    好友昵称
     */
    public String getNickNameByUid(String uid) {
        for (int i = 0; i < child.size(); i++) {
            for (FG_FriendJsonBean friend : child.get(i)) {
                if (friend.getUserid().equals(uid)) {
                    return friend.getMarkname();
                }
            }
        }
        return "";
    }

    /**
     * 变更好友分组
     *
     * @param uid 用户id
     * @param oid 老的分组id
     * @param nid 新的分组id
     */
    public void changeGroup(String uid, String oid, String nid) {
        int op = 0;
        int cp = 0;
        for (int position = 0; position < group.size(); position++) {
            if (oid.equals(group.get(position).getGroupid())) {
                op = position;
            }
            if (nid.equals(group.get(position).getGroupid())) {
                cp = position;
            }
        }
        FG_FriendJsonBean friendJsonBean = null;
        for (FG_FriendJsonBean fg_friendJsonBean : child.get(op)) {
            if (fg_friendJsonBean.getUserid().equals(uid)) {
                friendJsonBean = fg_friendJsonBean;
                break;
            }
        }
        if (friendJsonBean == null) {
            return;
        }
        child.get(op).remove(friendJsonBean);
        child.get(cp).add(friendJsonBean);
        triggerEvent();
    }

    /**
     * 删除好友
     *
     * @param uid 好友id
     */
    public void deleteFriend(String uid) {
        for (int i = 0; i < child.size(); i++) {
            for (FG_FriendJsonBean friend : child.get(i)) {
                if (friend.getUserid().equals(uid)) {
                    child.get(i).remove(friend);
                    break;
                }
            }
        }
        triggerEvent();
    }

    /**
     * 默认分组添加好友
     *
     * @param applyFriendEntity 好友
     */
    public void addFriend(ApplyFriendEntity applyFriendEntity) {
        FG_FriendJsonBean fg_friendJsonBean = new FG_FriendJsonBean();
        fg_friendJsonBean.setUserid(applyFriendEntity.getUid());
        fg_friendJsonBean.setGender(applyFriendEntity.getGender());
        fg_friendJsonBean.setPortrait(applyFriendEntity.getPortrait());
        fg_friendJsonBean.setMarkname("");
        fg_friendJsonBean.setRealname(applyFriendEntity.getNickname());
        child.get(0).add(fg_friendJsonBean);
        triggerEvent();
    }


    /**
     * 更改分组名
     *
     * @param groupId 分组id
     * @param newName 新的分组名
     */
    public void changeGroupNameByGroupId(String groupId, String newName) {
        for (FG_GroupJsonBean fg_groupJsonBean : group) {
            if (fg_groupJsonBean.getGroupid().equals(groupId)) {
                fg_groupJsonBean.setGroupname(newName);
            }
        }
        triggerEvent();
    }

    /**
     * 删除分组
     *
     * @param groupId 分组id
     */
    public void deleteGroupByGroupId(String groupId) {
        for (int i = 0; i < group.size(); i++) {
            if (group.get(i).getGroupid().equals(groupId)) {
                group.remove(i);
                child.remove(i);
                triggerEvent();
                return;
            }
        }
    }

    /**
     * 添加新的分组
     *
     * @param fg_groupJsonBean 新分组实体集
     */
    public void addNewGroup(FG_GroupJsonBean fg_groupJsonBean) {
        group.add(fg_groupJsonBean);
        child.add(new ArrayList<FG_FriendJsonBean>());
        triggerEvent();
    }

    /**
     * 发布分组信息有变更的事件，通知Activity或者Fragment更新UI
     */
    private void triggerEvent() {
        FriendGroupEvent friendGroupEvent = new FriendGroupEvent();
        friendGroupEvent.event = FriendGroupEvent.Event.REFRESH;
        EventBus.getDefault().post(friendGroupEvent);
    }

    /**
     * 根据用户id判断是否是好友
     * @param uid   用户id
     * @return      是否是好友
     */
    public boolean isFriend(String uid) {
        if(uid.equals(IMLoginManager.instance().getLoginId()+"")) {
            return true;
        }
        for(int i=0; i<child.size(); i++) {
            for (int j=0; j<child.get(i).size(); j++) {
                return child.get(i).get(j).getUserid().equals(uid);
            }
        }
        return false;
    }
}
