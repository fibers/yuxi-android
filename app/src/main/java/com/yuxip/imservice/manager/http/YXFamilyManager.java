package com.yuxip.imservice.manager.http;

import android.text.TextUtils;

import com.squareup.okhttp.Request;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FamilyInfoDao;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMManager;
import com.yuxip.utils.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by SummerRC on 2015/10/20.
 * description:我的家族的管理类
 */
public class YXFamilyManager extends IMManager{
    private static YXFamilyManager instance;
    private Logger logger;
    private Map<Integer, FamilyInfoDao> familyMap;        //k-v : 家族id(与群id相同) - 家族实体

    private YXFamilyManager() {
        familyMap = new HashMap<>();
        logger = Logger.getLogger(YXFamilyManager.class);
    }

    /**
     * 线程安全的单例模式
     *
     * @return 返回一个实例
     */
    public static YXFamilyManager instance() {
        if (instance == null) {
            synchronized (YXFamilyManager.class) {
                if (instance == null) {
                    instance = new YXFamilyManager();
                }
            }
        }
        return instance;
    }

    /**
     * 获取所有家族列表
     */
    public void getMyFamilyList() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        OkHttpClientManager.postAsy(ConstantValues.GetMyFamilyList, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                familyMap.clear();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONArray array = object.getJSONArray("familylist");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            FamilyInfoDao entity = new FamilyInfoDao();
                            entity.setPortrait(obj.getString("portrait"));
                            entity.setGroupid(Integer.valueOf(obj.getString("id")));
                            /** 插入map和数据库 */
                            YXGroupTypeManager.instance().setGroupType(entity.getGroupid(), ConstantValues.GROUP_TYPE_FAMILY);
                            familyMap.put(entity.getGroupid(), entity);
                        }
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                logger.e(e.toString());
            }
        });
    }


    /**
     * @param groupId   家族群id
     * @return          家族头像
     */
    public String getFamilyPortrait(int groupId) {
        String portrait = "";
        if(familyMap.containsKey(groupId)) {
            portrait = familyMap.get(groupId).getPortrait();
        }
        if(portrait==null || TextUtils.isEmpty(portrait)) {
            portrait = "";
        }
        return portrait;
    }

    /**
     * map增加一条家族信息
     *
     * @param groupId           家族群id
     * @param familyInfoDao     家族实体集
     */
    public void addFamilyInfoDao(int groupId, FamilyInfoDao familyInfoDao) {
        /** 插入map和数据库 */
        YXGroupTypeManager.instance().setGroupType(groupId, ConstantValues.GROUP_TYPE_FAMILY);
        familyMap.put(groupId, familyInfoDao);
    }

    public void deleteFamilyInfoDao(int groupId) {
        /** 插入map和数据库 */
        YXGroupTypeManager.instance().deleteGroupType(groupId);
        familyMap.remove(groupId);
    }


    public boolean isMember(int groupId) {
        return familyMap.containsKey(groupId);
    }

    @Override
    public void doOnStart() {

    }

    @Override
    public void reset() {
        familyMap.clear();
    }
}
