package com.yuxip.imservice.manager;

import android.text.TextUtils;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.PeerEntity;
import com.yuxip.DB.entity.SessionEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.DB.sp.ConfigurationSp;
import com.yuxip.config.DBConstant;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.entity.UnreadEntity;
import com.yuxip.imservice.event.SessionEvent;
import com.yuxip.imservice.manager.http.YXGroupTypeManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.protobuf.IMBaseDefine;
import com.yuxip.protobuf.IMBuddy;
import com.yuxip.protobuf.helper.EntityChangeEngine;
import com.yuxip.protobuf.helper.Java2ProtoBuf;
import com.yuxip.protobuf.helper.ProtoBuf2JavaBean;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import de.greenrobot.event.EventBus;

/**
 * app显示首页
 * 最近联系人列表
 */
public class IMSessionManager extends IMManager {
    private Logger logger = Logger.getLogger(IMSessionManager.class);
    private static IMSessionManager inst = new IMSessionManager();

    public static IMSessionManager instance() {
        return inst;
    }

    private IMSocketManager imSocketManager = IMSocketManager.instance();
    private IMLoginManager imLoginManager = IMLoginManager.instance();
    private DBInterface dbInterface = DBInterface.instance();
    private IMGroupManager groupManager = IMGroupManager.instance();

    private Map<String, SessionEntity> sessionMap = new ConcurrentHashMap<>();      // key = sessionKey -->  sessionType_peerId
    private boolean sessionListReady = false;                                       //SessionManager 状态字段

    @Override
    public void doOnStart() {
    }

    @Override
    public void reset() {
        sessionListReady = false;
        sessionMap.clear();
    }

    /**
     * 实现自身的事件驱动
     *
     * @param event SessionEvent
     */
    public void triggerEvent(SessionEvent event) {
        switch (event) {
            case RECENT_SESSION_LIST_SUCCESS:
                sessionListReady = true;
                break;
        }
        EventBus.getDefault().post(event);
    }

    public void onNormalLoginOk() {
        logger.d("recent#onLogin Successful");
        onLocalLoginOk();
        onLocalNetOk();
    }

    public void onLocalLoginOk() {
        logger.i("session#loadFromDb");
        List<SessionEntity> sessionInfoList = dbInterface.loadAllSession();
        for (SessionEntity sessionInfo : sessionInfoList) {
            sessionMap.put(sessionInfo.getSessionKey(), sessionInfo);
        }
        triggerEvent(SessionEvent.RECENT_SESSION_LIST_SUCCESS);
    }

    public void onLocalNetOk() {
        int latestUpdateTime = dbInterface.getSessionLastTime();
        logger.d("session#更新时间:%d", latestUpdateTime);
        reqGetRecentContacts(latestUpdateTime);
    }

    /**----------------------------分割线--------------------------------*/

    /**
     * 请求最近回话
     */
    private void reqGetRecentContacts(int latestUpdateTime) {
        logger.i("session#reqGetRecentContacts");
        int loginId = IMLoginManager.instance().getLoginId();
        IMBuddy.IMRecentContactSessionReq recentContactSessionReq = IMBuddy.IMRecentContactSessionReq
                .newBuilder()
                .setLatestUpdateTime(latestUpdateTime)
                .setUserId(loginId)
                .build();
        int sid = IMBaseDefine.ServiceID.SID_BUDDY_LIST_VALUE;
        int cid = IMBaseDefine.BuddyListCmdID.CID_BUDDY_LIST_RECENT_CONTACT_SESSION_REQUEST_VALUE;
        imSocketManager.sendRequest(recentContactSessionReq, sid, cid);
    }

    /**
     * 最近回话返回:由消息分发中心IMPacketDispatcher分发
     * 与本地的进行merge
     *
     * @param recentContactSessionRsp recentContactSessionRsp
     */
    public void onRepRecentContacts(IMBuddy.IMRecentContactSessionRsp recentContactSessionRsp) {
        logger.i("session#onRepRecentContacts");
        int userId = recentContactSessionRsp.getUserId();
        List<IMBaseDefine.ContactSessionInfo> contactSessionInfoList = recentContactSessionRsp.getContactSessionListList();
        logger.i("contact#user:%d  cnt:%d", userId, contactSessionInfoList.size());

        /** 更新最近联系人列表 */
        ArrayList<SessionEntity> needDb = new ArrayList<>();
        for (IMBaseDefine.ContactSessionInfo sessionInfo : contactSessionInfoList) {
            // 返回的没有主键Id
            SessionEntity sessionEntity = ProtoBuf2JavaBean.getSessionEntity(sessionInfo);
            //并没有按照时间来排序
            sessionMap.put(sessionEntity.getSessionKey(), sessionEntity);
            needDb.add(sessionEntity);
        }
        logger.d("session#onRepRecentContacts is ready,now broadcast");

        /** 将最新的session信息保存在DB中 */
        dbInterface.batchInsertOrUpdateSession(needDb);
        if (needDb.size() > 0) {
            triggerEvent(SessionEvent.RECENT_SESSION_LIST_UPDATE);
        }
    }

    /**
     * 请求删除会话
     */
    public void reqRemoveSession(RecentInfo recentInfo) {
        logger.i("session#reqRemoveSession");

        int loginId = imLoginManager.getLoginId();

        String sessionKey = recentInfo.getSessionKey();
        /**直接本地先删除,清楚未读消息*/
        if (sessionMap.containsKey(sessionKey)) {
            sessionMap.remove(sessionKey);
            IMUnreadMsgManager.instance().readUnreadSession(sessionKey);
            dbInterface.deleteSession(sessionKey);
            ConfigurationSp.instance(ctx, loginId).setSessionTop(sessionKey, false);
            triggerEvent(SessionEvent.RECENT_SESSION_LIST_UPDATE);
        }

        IMBuddy.IMRemoveSessionReq removeSessionReq = IMBuddy.IMRemoveSessionReq
                .newBuilder()
                .setUserId(loginId)
                .setSessionId(recentInfo.getPeerId())
                .setSessionType(Java2ProtoBuf.getProtoSessionType(recentInfo.getSessionType()))
                .build();
        int sid = IMBaseDefine.ServiceID.SID_BUDDY_LIST_VALUE;
        int cid = IMBaseDefine.BuddyListCmdID.CID_BUDDY_LIST_REMOVE_SESSION_REQ_VALUE;
        imSocketManager.sendRequest(removeSessionReq, sid, cid);
    }

    /**
     * 删除会话返回
     */
    public void onRepRemoveSession(IMBuddy.IMRemoveSessionRsp removeSessionRsp) {
        logger.i("session#onRepRemoveSession");
        int resultCode = removeSessionRsp.getResultCode();
        if (0 != resultCode) {
            logger.e("session#removeSession failed");
        }
    }

    /**
     * 新建群组时候的更新
     */
    public void updateSession(GroupEntity entity) {
        logger.d("recent#updateSession GroupEntity:%s", entity);
        SessionEntity sessionEntity = new SessionEntity();
        sessionEntity.setLatestMsgType(DBConstant.MSG_TYPE_GROUP_TEXT);
        sessionEntity.setUpdated(entity.getUpdated());
        sessionEntity.setCreated(entity.getCreated());
        sessionEntity.setLatestMsgData("[你创建的新群喔]");
        sessionEntity.setTalkId(entity.getCreatorId());
        sessionEntity.setLatestMsgId(0);
        sessionEntity.setPeerId(entity.getPeerId());
        sessionEntity.setPeerType(DBConstant.SESSION_TYPE_GROUP);
        sessionEntity.buildSessionKey();

        sessionMap.put(sessionEntity.getSessionKey(), sessionEntity);
        ArrayList<SessionEntity> needDb = new ArrayList<>(1);
        needDb.add(sessionEntity);
        dbInterface.batchInsertOrUpdateSession(needDb);
        triggerEvent(SessionEvent.RECENT_SESSION_LIST_UPDATE);
    }


    /**
     * 接收到消息和发送消息都会调用：群组  个人
     * 1.自己发送消息
     * 2.收到消息
     *
     * @param msg msg
     */
    public void updateSession(MessageEntity msg) {
        logger.d("recent#updateSession msg:%s", msg);
        if (msg == null) {
            logger.d("recent#updateSession is end,cause by msg is null");
            return;
        }
        int loginId = imLoginManager.getLoginId();
        boolean isSend = msg.isSend(loginId);
        // 因为多端同步的问题
        int peerId = msg.getPeerId(isSend);

        SessionEntity sessionEntity = sessionMap.get(msg.getSessionKey());
        if (sessionEntity == null) {
            logger.d("session#updateSession#not found msgSessionEntity");
            sessionEntity = EntityChangeEngine.getSessionEntity(msg);
            sessionEntity.setPeerId(peerId);
            sessionEntity.buildSessionKey();
            // 判断群组的信息是否存在
            if (sessionEntity.getPeerType() == DBConstant.SESSION_TYPE_GROUP) {
                GroupEntity groupEntity = groupManager.findGroup(peerId);
                if (groupEntity == null) {
                    groupManager.reqGroupDetailInfo(peerId);
                }
            }
        } else {
            logger.d("session#updateSession#msgSessionEntity already in Map");
            sessionEntity.setUpdated(msg.getUpdated());
            sessionEntity.setLatestMsgData(msg.getMessageDisplay());
            sessionEntity.setTalkId(msg.getFromId());
            //todo check if msgid is null/0
            sessionEntity.setLatestMsgId(msg.getMsgId());
            sessionEntity.setLatestMsgType(msg.getMsgType());
        }

        /**DB 先更新*/
        ArrayList<SessionEntity> needDb = new ArrayList<>(1);
        needDb.add(sessionEntity);
        dbInterface.batchInsertOrUpdateSession(needDb);

        sessionMap.put(sessionEntity.getSessionKey(), sessionEntity);
        triggerEvent(SessionEvent.RECENT_SESSION_LIST_UPDATE);
    }


    public List<SessionEntity> getRecentSessionList() {
        return new ArrayList<>(sessionMap.values());
    }

    private static void sort(List<RecentInfo> data) {
        Collections.sort(data, new Comparator<RecentInfo>() {
            public int compare(RecentInfo o1, RecentInfo o2) {
                Integer a = o1.getUpdateTime();
                Integer b = o2.getUpdateTime();

                boolean isTopA = o1.isTop();
                boolean isTopB = o2.isTop();

                if (isTopA == isTopB) {
                    // 升序
                    //return a.compareTo(b);
                    // 降序
                    return b.compareTo(a);
                } else {
                    if (isTopA) {
                        return -1;
                    } else {
                        return 1;
                    }
                }

            }
        });
    }


    /**
     * 获取最近联系人列表，RecentInfo 是sessionEntity unreadEntity user/group 等等实体的封装(包含个人 群组）
     *
     * @return 去除了评论群的最近联系人、群
     */
    // todo every time it has to sort, kind of inefficient, change it
    public List<RecentInfo> getRecentListInfo() {
        /** 整理topList */
        List<RecentInfo> recentSessionList = new ArrayList<>();
        int loginId = IMLoginManager.instance().getLoginId();

        List<SessionEntity> sessionList = getRecentSessionList();
        Map<Integer, UserEntity> userMap = IMContactManager.instance().getUserMap();
        Map<String, UnreadEntity> unreadMsgMap = IMUnreadMsgManager.instance().getUnreadMsgMap();
        Map<Integer, GroupEntity> groupEntityMap = IMGroupManager.instance().getGroupMap();
        HashSet<String> topList = ConfigurationSp.instance(ctx, loginId).getSessionTopList();


        for (SessionEntity recentSession : sessionList) {
            int sessionType = recentSession.getPeerType();
            int peerId = recentSession.getPeerId();
            String sessionKey = recentSession.getSessionKey();

            UnreadEntity unreadEntity = unreadMsgMap.get(sessionKey);
            if (sessionType == DBConstant.SESSION_TYPE_GROUP) {
                logger.d("[fatal right] SESSION_TYPE_GROUP");
                if (YXGroupTypeManager.instance().isCommentGroup(peerId)) {
                    logger.d("[fatal right] SESSION_TYPE_GROUP : COMMENT");
                    continue;
                }

                GroupEntity groupEntity = groupEntityMap.get(peerId);
                RecentInfo recentInfo = new RecentInfo(recentSession, groupEntity, unreadEntity);
                if (topList != null && topList.contains(sessionKey)) {
                    recentInfo.setTop(true);
                }

                StoryParentEntity storyParentEntity = YXStoryManager.instance().getStoryByGroupid(peerId);
                if (storyParentEntity != null && !TextUtils.isEmpty(storyParentEntity.getPortrait())) {
                    ArrayList<String> list = new ArrayList<>();
                    list.add(storyParentEntity.getPortrait());
                    recentInfo.setAvatar(list);
                    if (!TextUtils.isEmpty(storyParentEntity.getTitle())) {
                        recentInfo.setName(storyParentEntity.getTitle() + "[" + recentInfo.getName() + "]");
                    }
                }
                //谁说的这条信息，只有群组需要，例如 【XXX:您好】
                int lastFromId = recentSession.getTalkId();
                UserEntity talkUser = userMap.get(lastFromId);
                // 用户已经不存在了
                if (talkUser != null) {
                    String oriContent = recentInfo.getLatestMsgData();
                    String finalContent = talkUser.getMainName() + ": " + oriContent;
                    recentInfo.setLatestMsgData(finalContent);
                }
                recentSessionList.add(recentInfo);
            } else if (sessionType == DBConstant.SESSION_TYPE_SINGLE) {
                UserEntity userEntity = userMap.get(peerId);
                RecentInfo recentInfo = new RecentInfo(recentSession, userEntity, unreadEntity);
                if (topList != null && topList.contains(sessionKey)) {
                    recentInfo.setTop(true);
                }
                recentSessionList.add(recentInfo);
            }
        }

        RecentInfo recentInfo = getLatestSystemMessage();
        if (recentInfo != null) {
            recentSessionList.add(recentInfo);
        }
        sort(recentSessionList);
        return recentSessionList;
    }


    /**
     * @return 系统消息
     */
    private RecentInfo getLatestSystemMessage() {
        int updateTime = getSystemLatestApplyTime();
        if (updateTime != 0) {
            RecentInfo recentInfo = new RecentInfo();
            recentInfo.setName("系统消息");
            recentInfo.setSessionType(DBConstant.SESSION_TYPE_SYSTEM);
            recentInfo.setUpdateTime(updateTime);

            long number = dbInterface.getAgreeFalseNum() + dbInterface.getApplyGroupAgreeFalseNum();
            if (number > 0) {
                recentInfo.setUnReadCnt((int) number);
                recentInfo.setLatestMsgData("有" + number + "条未处理的消息");
            } else {
                recentInfo.setUnReadCnt(0);
                recentInfo.setLatestMsgData("");
            }
            return recentInfo;
        } else {
            return null;
        }
    }

    /**
     * @return 最新请求的时间
     */
    private int getSystemLatestApplyTime() {
        int applyFriendTime = 0;
        int applyGroupTime = 0;
        List<ApplyFriendEntity> addFriendEntities = dbInterface.loadAllApplyFriendEntity();
        if (addFriendEntities != null && !addFriendEntities.isEmpty()) {
            ApplyFriendEntity applyFriendEntity = addFriendEntities.get(addFriendEntities.size() - 1);
            applyFriendTime = Integer.valueOf(applyFriendEntity.getApplyTime());
        }
        List<ApplyGroupEntity> applyGroupEntities = dbInterface.loadAllApplyGroupEntity();
        if (applyGroupEntities != null && !applyGroupEntities.isEmpty()) {
            ApplyGroupEntity applyGroupEntity = applyGroupEntities.get(applyGroupEntities.size() - 1);
            applyGroupTime = Integer.valueOf(applyGroupEntity.getCreateTime());
        }
        return (applyFriendTime > applyGroupTime) ? applyFriendTime : applyGroupTime;
    }


    public SessionEntity findSession(String sessionKey) {
        if (sessionMap.size() <= 0 || TextUtils.isEmpty(sessionKey)) {
            return null;
        }
        if (sessionMap.containsKey(sessionKey)) {
            return sessionMap.get(sessionKey);
        }
        return null;
    }

    public PeerEntity findPeerEntity(String sessionKey) {
        if (TextUtils.isEmpty(sessionKey)) {
            return null;
        }
        // 拆分
        PeerEntity peerEntity;
        String[] sessionInfo = EntityChangeEngine.spiltSessionKey(sessionKey);
        int peerType = Integer.parseInt(sessionInfo[0]);
        int peerId = Integer.parseInt(sessionInfo[1]);
        switch (peerType) {
            case DBConstant.SESSION_TYPE_SINGLE: {       //好友
                peerEntity = IMContactManager.instance().findContact(peerId);
            }
            break;
            case DBConstant.SESSION_TYPE_GROUP: {        //群组
                peerEntity = IMGroupManager.instance().findGroup(peerId);
            }
            break;
            default:
                throw new IllegalArgumentException("findPeerEntity#peerType is illegal,cause by " + peerType);
        }
        return peerEntity;
    }

    /**
     * ------------------------实体的get set-----------------------------
     */
    public boolean isSessionListReady() {
        return sessionListReady;
    }

    public void setSessionListReady(boolean sessionListReady) {
        this.sessionListReady = sessionListReady;
    }


    /**
     * ------------------------------------------以下是扩展的部分-------------------------------------------------------------------
     */

    /**
     * 请求删除会话
     */
    public void reqRemoveSession(String sessionKey) {
        logger.i("session#reqRemoveSession");

        int loginId = imLoginManager.getLoginId();

        int sessionType = EntityChangeEngine.getSessionType(sessionKey);

        String peerId = EntityChangeEngine.getSessionPeerId(sessionKey);

        /**直接本地先删除,清楚未读消息*/
        if (sessionMap.containsKey(sessionKey)) {
            sessionMap.remove(sessionKey);
            IMUnreadMsgManager.instance().readUnreadSession(sessionKey);
            dbInterface.deleteSession(sessionKey);
            ConfigurationSp.instance(ctx, loginId).setSessionTop(sessionKey, false);
            triggerEvent(SessionEvent.RECENT_SESSION_LIST_UPDATE);
        }

        IMBuddy.IMRemoveSessionReq removeSessionReq = IMBuddy.IMRemoveSessionReq
                .newBuilder()
                .setUserId(loginId)
                .setSessionId(Integer.valueOf(peerId))
                .setSessionType(Java2ProtoBuf.getProtoSessionType(sessionType))
                .build();
        int sid = IMBaseDefine.ServiceID.SID_BUDDY_LIST_VALUE;
        int cid = IMBaseDefine.BuddyListCmdID.CID_BUDDY_LIST_REMOVE_SESSION_REQ_VALUE;
        imSocketManager.sendRequest(removeSessionReq, sid, cid);
    }


    //add by guoq-s
    // 获取最近联系人列表，RecentInfo 是sessionEntity unreadEntity user/group 等等实体的封装(包含个人 群组）
    // todo every time it has to sort, kind of inefficient, change it
    public List<RecentInfo> getRecentCommentListInfo() {
        /**整理topList*/
        List<RecentInfo> recentSessionList = new ArrayList<>();
        int loginId = IMLoginManager.instance().getLoginId();

        List<SessionEntity> sessionList = getRecentSessionList();
        Map<Integer, UserEntity> userMap = IMContactManager.instance().getUserMap();
        Map<String, UnreadEntity> unreadMsgMap = IMUnreadMsgManager.instance().getUnreadMsgMap();
        Map<Integer, GroupEntity> groupEntityMap = IMGroupManager.instance().getGroupMap();
        HashSet<String> topList = ConfigurationSp.instance(ctx, loginId).getSessionTopList();

        for (SessionEntity recentSession : sessionList) {
            int sessionType = recentSession.getPeerType();
            int peerId = recentSession.getPeerId();
            String sessionKey = recentSession.getSessionKey();

            UnreadEntity unreadEntity = unreadMsgMap.get(sessionKey);
            if (sessionType == DBConstant.SESSION_TYPE_GROUP) {

                if (!YXGroupTypeManager.instance().isCommentGroup(peerId)) {

                    continue;
                }

                GroupEntity groupEntity = groupEntityMap.get(peerId);
                logger.e("groupEntity: " + groupEntity);

                RecentInfo recentInfo = new RecentInfo(recentSession, groupEntity, unreadEntity);
                if (topList != null && topList.contains(sessionKey)) {
                    recentInfo.setTop(true);
                }

                //谁说的这条信息，只有群组需要，例如 【XXX:您好】
                int lastFromId = recentSession.getTalkId();
                UserEntity talkUser = userMap.get(lastFromId);
                // 用户已经不存在了
                if (talkUser != null) {
                    String oriContent = recentInfo.getLatestMsgData();
                    String finalContent = talkUser.getMainName() + ": " + oriContent;
                    recentInfo.setLatestMsgData(finalContent);
                }
                recentSessionList.add(recentInfo);
            }
        }
        sort(recentSessionList);
        return recentSessionList;
    }
    //add by guoq-e

    /**------------------------------------------add by SummerRC begin---------------------------------------------------------------*/
    /**
     * @return 返回最近联系人列表(只包含个人)
     */
    public List<RecentInfo> getRecentListInfoUser() {
        /**整理topList*/
        List<RecentInfo> recentSessionList = new ArrayList<>();
        int loginId = IMLoginManager.instance().getLoginId();

        List<SessionEntity> sessionList = getRecentSessionList();
        Map<Integer, UserEntity> userMap = IMContactManager.instance().getUserMap();
        Map<String, UnreadEntity> unreadMsgMap = IMUnreadMsgManager.instance().getUnreadMsgMap();
        HashSet<String> topList = ConfigurationSp.instance(ctx, loginId).getSessionTopList();

        for (SessionEntity recentSession : sessionList) {
            int sessionType = recentSession.getPeerType();
            int peerId = recentSession.getPeerId();
            String sessionKey = recentSession.getSessionKey();

            UnreadEntity unreadEntity = unreadMsgMap.get(sessionKey);
            if (sessionType == DBConstant.SESSION_TYPE_SINGLE) {
                UserEntity userEntity = userMap.get(peerId);
                RecentInfo recentInfo = new RecentInfo(recentSession, userEntity, unreadEntity);
                if (topList != null && topList.contains(sessionKey)) {
                    recentInfo.setTop(true);
                }
                recentSessionList.add(recentInfo);
            }
        }
        sort(recentSessionList);
        return recentSessionList;
    }


    /**
     * ------------------------------------------add by SummerRC end---------------------------------------------------------------
     */
    //add by guoq-s
    // 获取最近联系人列表，RecentInfo 是sessionEntity unreadEntity user/group 等等实体的封装(包含个人 群组）
    // todo every time it has to sort, kind of inefficient, change it
    public int getUnreadCommentCount() {
        /**整理topList*/

        List<SessionEntity> sessionList = getRecentSessionList();
        Map<String, UnreadEntity> unreadMsgMap = IMUnreadMsgManager.instance().getUnreadMsgMap();

        int count = 0;

        for (SessionEntity recentSession : sessionList) {
            int sessionType = recentSession.getPeerType();
            int peerId = recentSession.getPeerId();
            String sessionKey = recentSession.getSessionKey();

            UnreadEntity unreadEntity = unreadMsgMap.get(sessionKey);
            if (sessionType == DBConstant.SESSION_TYPE_GROUP) {

                if (YXGroupTypeManager.instance().isCommentGroup(peerId) && unreadEntity != null) {
                    count += unreadEntity.getUnReadCnt();

                }
            }
        }

        return count;
    }
}
