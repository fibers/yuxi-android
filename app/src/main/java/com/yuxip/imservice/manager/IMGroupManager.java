package com.yuxip.imservice.manager;

import android.util.Log;
import android.widget.Toast;

import com.google.protobuf.CodedInputStream;
import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.SessionEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.imservice.callback.Packetlistener;
import com.yuxip.imservice.event.CreateStoryTopicEvent;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.SessionEvent;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.protobuf.IMBaseDefine;
import com.yuxip.protobuf.IMGroup;
import com.yuxip.protobuf.helper.EntityChangeEngine;
import com.yuxip.protobuf.helper.ProtoBuf2JavaBean;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.pinyin.PinYin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import de.greenrobot.event.EventBus;

public class IMGroupManager extends IMManager {
    private Logger logger = Logger.getLogger(IMGroupManager.class);
    private static IMGroupManager inst = new IMGroupManager();

    public static IMGroupManager instance() {
        return inst;
    }

    // 依赖的服务管理
    private IMSocketManager imSocketManager = IMSocketManager.instance();
    private IMLoginManager imLoginManager = IMLoginManager.instance();
    private DBInterface dbInterface = DBInterface.instance();


    //正式群,临时群都会有的，存在竞争 如果不同时请求的话
    private Map<Integer, GroupEntity> groupMap = new ConcurrentHashMap<>();
    // 群组状态
    private boolean isGroupReady = false;

    @Override
    public void doOnStart() {
//        groupMap.clear();
    }

    public void onNormalLoginOk() {
        onLocalLoginOk();
        onLocalNetOk();
    }

    /**
     * 1. 加载本地信息
     * 2. 请求正规群信息 ， 与本地进行对比
     * 3. version groupId 请求
     */
    public void onLocalLoginOk() {
        logger.i("group#loadFromDb");
        if (!EventBus.getDefault().isRegistered(inst)) {
            EventBus.getDefault().registerSticky(inst);
        }

        /** 加载本地group */
        List<GroupEntity> localGroupInfoList = dbInterface.loadAllGroup();
        for (GroupEntity groupInfo : localGroupInfoList) {
            groupMap.put(groupInfo.getPeerId(), groupInfo);
        }
        logger.e("group#loadFromDb", localGroupInfoList);
        logger.e("groupMap", groupMap.size() + "");
        triggerEvent(new GroupEvent(GroupEvent.Event.GROUP_INFO_OK));
    }

    public void onLocalNetOk() {
        reqGetNormalGroupList();
        YXStoryManager.instance().getMyStory();
        YXFamilyManager.instance().getMyFamilyList();
    }

    @Override
    public void reset() {
        isGroupReady = false;
//        groupMap.clear();
        EventBus.getDefault().unregister(inst);
    }

    public void onEvent(SessionEvent event) {
        switch (event) {
            case RECENT_SESSION_LIST_UPDATE:
                // groupMap 本地已经加载完毕之后才触发
                loadSessionGroupInfo();
                break;
        }
    }

    /**
     * 实现自身的事件驱动
     *
     * @param event
     */
    public synchronized void triggerEvent(GroupEvent event) {
        switch (event.getEvent()) {
            case GROUP_INFO_OK:
                isGroupReady = true;
                break;
            case GROUP_INFO_UPDATED:
                isGroupReady = true;
                break;
        }
        EventBus.getDefault().postSticky(event);
    }

    /**---------------事件驱动end------------------------------*/

    /**
     * 1. 加载本地信息
     * 2. 从session中获取 群组信息，从本地中获取这些群组的version信息
     * 3. 合并上述的merge结果， version groupId 请求
     */
    private void loadSessionGroupInfo() {
        logger.i("group#loadSessionGroupInfo");
        List<SessionEntity> sessionInfoList = IMSessionManager.instance().getRecentSessionList();
        List<IMBaseDefine.GroupVersionInfo> needReqList = new ArrayList<>();
        for (SessionEntity sessionInfo : sessionInfoList) {
            int version = 0;
            if (sessionInfo.getPeerType() == DBConstant.SESSION_TYPE_GROUP) {     /**群组*/
                if (groupMap.containsKey(sessionInfo.getPeerId())) {
                    version = groupMap.get(sessionInfo.getPeerId()).getVersion();
                }
                logger.e("groupMap", groupMap.size() + "");
                IMBaseDefine.GroupVersionInfo versionInfo = IMBaseDefine.GroupVersionInfo.newBuilder()
                        .setVersion(version)
                        .setGroupId(sessionInfo.getPeerId())
                        .build();
                needReqList.add(versionInfo);
            }
        }
        // 事件触发的时候需要注意
        if (needReqList.size() > 0) {
            reqGetGroupDetailInfo(needReqList);
        }
    }

    /**
     * 联系人页面正式群的请求
     * todo 正式群与临时群逻辑上的分开的，但是底层应该是想通的
     */
    private void reqGetNormalGroupList() {
        logger.i("group#reqGetNormalGroupList");
        int loginId = imLoginManager.getLoginId();
        IMGroup.IMNormalGroupListReq normalGroupListReq = IMGroup.IMNormalGroupListReq.newBuilder()
                .setUserId(loginId)
                .build();
        int sid = IMBaseDefine.ServiceID.SID_GROUP_VALUE;                       //4
        int cid = IMBaseDefine.GroupCmdID.CID_GROUP_NORMAL_LIST_REQUEST_VALUE;  //1025
        imSocketManager.sendRequest(normalGroupListReq, sid, cid);
        logger.i("group#send packet to server");
    }

    /**
     * 正式群请求的回掉，由消息分发中心IMPacketDispatcher调用、
     */
    public void onRepNormalGroupList(IMGroup.IMNormalGroupListRsp normalGroupListRsp) {
        logger.i("group#onRepNormalGroupList");
        int groupSize = normalGroupListRsp.getGroupVersionListCount();
        logger.i("group#onRepNormalGroupList cnt:%d", groupSize);
        List<IMBaseDefine.GroupVersionInfo> versionInfoList = normalGroupListRsp.getGroupVersionListList();

        /**对比DB中的version字段*/
        // 这块对比的可以抽离出来
        List<IMBaseDefine.GroupVersionInfo> needInfoList = new ArrayList<>();

        for (IMBaseDefine.GroupVersionInfo groupVersionInfo : versionInfoList) {
            int groupId = groupVersionInfo.getGroupId();
            int version = groupVersionInfo.getVersion();
            if (groupMap.containsKey(groupId) && groupMap.get(groupId).getVersion() == version) {
                continue;
            }
            logger.e("groupMap", groupMap.size() + "");
            IMBaseDefine.GroupVersionInfo versionInfo = IMBaseDefine.GroupVersionInfo.newBuilder()
                    .setVersion(0)
                    .setGroupId(groupId)
                    .build();
            needInfoList.add(versionInfo);
        }

        // 事件触发的时候需要注意 todo
        if (needInfoList.size() > 0) {
            reqGetGroupDetailInfo(needInfoList);
        }
    }

    /**
     * 请求群组的详细信息
     */
    public void reqGroupDetailInfo(int groupId) {
        IMBaseDefine.GroupVersionInfo groupVersionInfo = IMBaseDefine.GroupVersionInfo.newBuilder()
                .setGroupId(groupId)
                .setVersion(0)
                .build();
        ArrayList<IMBaseDefine.GroupVersionInfo> list = new ArrayList<>();
        list.add(groupVersionInfo);
        reqGetGroupDetailInfo(list);
    }

    /**
     * 请求群组的详细信息
     */
    public void reqGetGroupDetailInfo(List<IMBaseDefine.GroupVersionInfo> versionInfoList) {
        logger.i("group#reqGetGroupDetailInfo");
        if (versionInfoList == null || versionInfoList.size() <= 0) {
            logger.e("group#reqGetGroupDetailInfo# please check your params,cause by empty/null");
            return;
        }
        int loginId = imLoginManager.getLoginId();
        IMGroup.IMGroupInfoListReq groupInfoListReq = IMGroup.IMGroupInfoListReq.newBuilder()
                .setUserId(loginId)
                .addAllGroupVersionList(versionInfoList)
                .build();

        int sid = IMBaseDefine.ServiceID.SID_GROUP_VALUE;
        int cid = IMBaseDefine.GroupCmdID.CID_GROUP_INFO_REQUEST_VALUE;
        imSocketManager.sendRequest(groupInfoListReq, sid, cid);
    }


    /**
     * 这个地方有问题需要修改
     *
     * @param groupInfoListRsp groupInfoListRsp
     */
    public void onRepGroupDetailInfo(IMGroup.IMGroupInfoListRsp groupInfoListRsp) {
        logger.i("group#onRepGroupDetailInfo");
        int groupSize = groupInfoListRsp.getGroupInfoListCount();
        int userId = groupInfoListRsp.getUserId();
        int loginId = imLoginManager.getLoginId();
        logger.i("group#onRepGroupDetailInfo cnt:%d", groupSize);
        if (groupSize <= 0 || userId != loginId) {
            logger.i("group#onRepGroupDetailInfo size empty or userid[%d]≠ loginId[%d]", userId, loginId);
            return;
        }
        ArrayList<GroupEntity> needDb = new ArrayList<>();
        //add by guoq-s
        boolean isAddGroup = false;
        GroupEntity groupEntity = null;
        //add by guoq-e

        for (IMBaseDefine.GroupInfo groupInfo : groupInfoListRsp.getGroupInfoListList()) {
            // 群组的详细信息
            // 保存在DB中
            // GroupManager 中的变量
            groupEntity = ProtoBuf2JavaBean.getGroupEntity(groupInfo);
            if (!groupMap.containsKey(groupEntity.getPeerId())) {
                isAddGroup = true;
            }
            groupMap.put(groupEntity.getPeerId(), groupEntity);
            logger.e("groupMap", groupMap.size() + "");
            needDb.add(groupEntity);
        }

        dbInterface.batchInsertOrUpdateGroup(needDb);
        GroupEvent groupEvent = new GroupEvent(GroupEvent.Event.GROUP_INFO_UPDATED);

        if (isAddGroup) {
            //这里仅仅是把最后一个groupentity作为参数返回。
            groupEvent.setGroupEntity(groupEntity);
        }

        triggerEvent(groupEvent);

    }


    /**
     * 创建群
     * 默认是创建临时群，且客户端只能创建临时群
     */
    public void reqCreateTempGroup(String groupName, Set<Integer> memberList) {

        logger.i("group#reqCreateTempGroup, tempGroupName = %s", groupName);

        int loginId = imLoginManager.getLoginId();

        IMGroup.IMGroupCreateReq groupCreateReq = IMGroup.IMGroupCreateReq.newBuilder()
                .setUserId(loginId)
                .setGroupType(IMBaseDefine.GroupType.GROUP_TYPE_NORMAL)
                .setGroupName(groupName)
                .setGroupAvatar("")// todo 群头像 现在是四宫格
                .addAllMemberIdList(memberList)
                .build();

        int sid = IMBaseDefine.ServiceID.SID_GROUP_VALUE;
        int cid = IMBaseDefine.GroupCmdID.CID_GROUP_CREATE_REQUEST_VALUE;
        imSocketManager.sendRequest(groupCreateReq, sid, cid, new Packetlistener() {
            @Override
            public void onSuccess(Object response) {
                try {
                    IMGroup.IMGroupCreateRsp groupCreateRsp = IMGroup.IMGroupCreateRsp.parseFrom((CodedInputStream) response);
                    IMGroupManager.instance().onReqCreateTempGroup(groupCreateRsp);
                } catch (IOException e) {
                    logger.e("reqCreateTempGroup parse error");
                    triggerEvent(new GroupEvent(GroupEvent.Event.CREATE_GROUP_FAIL));
                }
            }

            @Override
            public void onFaild() {
                triggerEvent(new GroupEvent(GroupEvent.Event.CREATE_GROUP_FAIL));
            }

            @Override
            public void onTimeout() {
                triggerEvent(new GroupEvent(GroupEvent.Event.CREATE_GROUP_TIMEOUT));
            }
        });

    }

    public void onReqCreateTempGroup(IMGroup.IMGroupCreateRsp groupCreateRsp) {
        logger.d("group#onReqCreateTempGroup");

        int resultCode = groupCreateRsp.getResultCode();
        if (0 != resultCode) {
            logger.e("group#createGroup failed");
            triggerEvent(new GroupEvent(GroupEvent.Event.CREATE_GROUP_FAIL));
            return;
        }
        GroupEntity groupEntity = ProtoBuf2JavaBean.getGroupEntity(groupCreateRsp);
        // 更新DB 更新map
        groupMap.put(groupEntity.getPeerId(), groupEntity);
        logger.e("groupMap", groupMap.size() + "");
        IMSessionManager.instance().updateSession(groupEntity);
        dbInterface.insertOrUpdateGroup(groupEntity);

        triggerEvent(new GroupEvent(GroupEvent.Event.CREATE_GROUP_OK, groupEntity)); // 接收到之后修改UI
    }

    /**
     * 删除群成员
     * REMOVE_CHANGE_MEMBER_TYPE
     * 可能会触发头像的修改
     */
    public void reqRemoveGroupMember(int groupId, Set<Integer> removeMemberlist) {
        reqChangeGroupMember(groupId, IMBaseDefine.GroupModifyType.GROUP_MODIFY_TYPE_DEL, removeMemberlist);
    }


    /**
     * 新增群成员
     * ADD_CHANGE_MEMBER_TYPE
     * 可能会触发头像的修改
     */
    public void reqAddGroupMember(int groupId, Set<Integer> addMemberlist) {
        reqChangeGroupMember(groupId, IMBaseDefine.GroupModifyType.GROUP_MODIFY_TYPE_ADD, addMemberlist);
    }

    private void reqChangeGroupMember(final int groupId, final IMBaseDefine.GroupModifyType groupModifyType, Set<Integer> changeMemberlist) {
        logger.i("group#reqChangeGroupMember, changeGroupMemberType = %s", groupModifyType.toString());

        final int loginId = imLoginManager.getLoginId();
        IMGroup.IMGroupChangeMemberReq groupChangeMemberReq = IMGroup.IMGroupChangeMemberReq.newBuilder()
                .setUserId(loginId)
                .setChangeType(groupModifyType)
                .addAllMemberIdList(changeMemberlist)
                .setGroupId(groupId)
                .build();

        int sid = IMBaseDefine.ServiceID.SID_GROUP_VALUE;
        int cid = IMBaseDefine.GroupCmdID.CID_GROUP_CHANGE_MEMBER_REQUEST_VALUE;
        imSocketManager.sendRequest(groupChangeMemberReq, sid, cid, new Packetlistener() {
            @Override
            public void onSuccess(Object response) {
                try {
                    IMGroup.IMGroupChangeMemberRsp groupChangeMemberRsp = IMGroup.IMGroupChangeMemberRsp.parseFrom((CodedInputStream) response);
                    IMGroupManager.instance().onReqChangeGroupMember(groupChangeMemberRsp);

                } catch (IOException e) {
                    logger.e("reqChangeGroupMember parse error!");
                    triggerEvent(new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_FAIL));
                }
            }

            @Override
            public void onFaild() {
                triggerEvent(new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_FAIL));
            }

            @Override
            public void onTimeout() {
                triggerEvent(new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_TIMEOUT));
            }
        });

    }

    //todo  这个地方有问题
    public void onReqChangeGroupMember(IMGroup.IMGroupChangeMemberRsp groupChangeMemberRsp) {
        int resultCode = groupChangeMemberRsp.getResultCode();
        logger.d("onReqChangeGroupMember", "onReqChangeGroupMember is called changelist " + groupChangeMemberRsp.toString());
        if (0 != resultCode) {
            triggerEvent(new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_FAIL));
            return;
        }

        int groupId = groupChangeMemberRsp.getGroupId();
        List<Integer> changeList = groupChangeMemberRsp.getChgUserIdListList();
        IMBaseDefine.GroupModifyType groupModifyType = groupChangeMemberRsp.getChangeType();

        //add by guoq-s
        Integer userId = IMLoginManager.instance().getLoginId();

        GroupEntity groupEntityRet = groupMap.get(groupId);
        logger.e("groupMap", groupMap.size() + "");
        //进群或者是退群。
        if (changeList.contains(userId)) {
            if (groupModifyType == IMBaseDefine.GroupModifyType.GROUP_MODIFY_TYPE_ADD) {
                Log.d("onReqChangeGroupMember", "group add is called");
                //更新个人信息
                reqGroupDetailInfo(groupId);

                //新群的场合，需要获取新的群信息。不需要通知成员变更。
                return;
            } else {
                //删除群组
                String sessionKey = EntityChangeEngine.getSessionKey(groupId, DBConstant.SESSION_TYPE_GROUP);
                groupMap.remove(groupId);
                IMSessionManager.instance().reqRemoveSession(sessionKey);
            }
        } else {
            groupEntityRet.setlistGroupMemberIds(groupChangeMemberRsp.getCurUserIdListList());
            groupMap.put(groupId, groupEntityRet);
            logger.e("groupMap", groupMap.size() + "");
            dbInterface.insertOrUpdateGroup(groupEntityRet);
        }


        GroupEvent groupEvent = new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_SUCCESS);
        groupEvent.setChangeList(changeList);
        groupEvent.setChangeType(ProtoBuf2JavaBean.getGroupChangeType(groupModifyType));
        groupEvent.setGroupEntity(groupEntityRet);
        triggerEvent(groupEvent);
    }

    /**
     * 屏蔽群消息
     * IMGroupShieldReq
     * 备注:应为屏蔽之后大部分操作依旧需要客户端做
     */
    public void reqShieldGroup(final int groupId, final int shieldType) {
        final GroupEntity entity = groupMap.get(groupId);
        logger.e("groupMap", groupMap.size() + "");
        if (entity == null) {
            logger.i("GroupEntity do not exist!");
            return;
        }
        final int loginId = IMLoginManager.instance().getLoginId();
        IMGroup.IMGroupShieldReq shieldReq = IMGroup.IMGroupShieldReq.newBuilder()
                .setShieldStatus(shieldType)
                .setGroupId(groupId)
                .setUserId(loginId)
                .build();
        int sid = IMBaseDefine.ServiceID.SID_GROUP_VALUE;
        int cid = IMBaseDefine.GroupCmdID.CID_GROUP_SHIELD_GROUP_REQUEST_VALUE;
        imSocketManager.sendRequest(shieldReq, sid, cid, new Packetlistener() {
            @Override
            public void onSuccess(Object response) {
                try {
                    IMGroup.IMGroupShieldRsp groupShieldRsp = IMGroup.IMGroupShieldRsp.parseFrom((CodedInputStream) response);
                    int resCode = groupShieldRsp.getResultCode();
                    if (resCode != 0) {
                        triggerEvent(new GroupEvent(GroupEvent.Event.SHIELD_GROUP_FAIL));
                        return;
                    }
                    if (groupShieldRsp.getGroupId() != groupId || groupShieldRsp.getUserId() != loginId) {
                        return;
                    }
                    // 更新DB状态
                    entity.setStatus(shieldType);
                    dbInterface.insertOrUpdateGroup(entity);
                    // 更改未读计数状态
                    boolean isFor = shieldType == DBConstant.GROUP_STATUS_SHIELD;
                    IMUnreadMsgManager.instance().setForbidden(
                            EntityChangeEngine.getSessionKey(groupId, DBConstant.SESSION_TYPE_GROUP), isFor);
                    triggerEvent(new GroupEvent(GroupEvent.Event.SHIELD_GROUP_OK, entity));

                } catch (IOException e) {
                    logger.e("reqChangeGroupMember parse error!");
                    triggerEvent(new GroupEvent(GroupEvent.Event.SHIELD_GROUP_FAIL));
                }
            }

            @Override
            public void onFaild() {
                triggerEvent(new GroupEvent(GroupEvent.Event.SHIELD_GROUP_FAIL));
            }

            @Override
            public void onTimeout() {
                triggerEvent(new GroupEvent(GroupEvent.Event.SHIELD_GROUP_TIMEOUT));
            }
        });
    }


    /**
     * 收到群成员发生变更消息
     * 服务端主动发出
     * DB
     */
    public void receiveGroupChangeMemberNotify(IMGroup.IMGroupChangeMemberNotify notify) {
        int groupId = notify.getGroupId();
        int changeType = ProtoBuf2JavaBean.getGroupChangeType(notify.getChangeType());
        List<Integer> changeList = notify.getChgUserIdListList();

        Log.d("receiveGroupChangeMemb", "onReqChangeGroupMember is called changelist " + notify.toString());
        List<Integer> curMemberList = notify.getCurUserIdListList();
        //add by guoq-s
        Integer userId = IMLoginManager.instance().getLoginId();

        GroupEntity entity = groupMap.get(groupId);
        logger.e("groupMap", groupMap.size() + "");
        //进群或者是退群。
        if (changeList.contains(userId)) {
            if (notify.getChangeType() == IMBaseDefine.GroupModifyType.GROUP_MODIFY_TYPE_ADD) {
                //更新个人信息
                reqGroupDetailInfo(groupId);
                //新群的场合，需要获取新的群信息。不需要通知成员变更。
                return;
            } else {
                //删除群组
                String sessionKey = EntityChangeEngine.getSessionKey(groupId, DBConstant.SESSION_TYPE_GROUP);
                groupMap.remove(groupId);
                logger.e("groupMap", groupMap.size() + "");
                IMSessionManager.instance().reqRemoveSession(sessionKey);
            }

        } else {

            entity.setlistGroupMemberIds(curMemberList);
            dbInterface.insertOrUpdateGroup(entity);
            groupMap.put(groupId, entity);
            logger.e("groupMap", groupMap.size() + "");
        }


        GroupEvent groupEvent = new GroupEvent(GroupEvent.Event.CHANGE_GROUP_MEMBER_SUCCESS);
        groupEvent.setChangeList(changeList);
        groupEvent.setChangeType(changeType);
        groupEvent.setGroupEntity(entity);
        triggerEvent(groupEvent);

    }

    public List<GroupEntity> getNormalGroupList() {
        List<GroupEntity> normalGroupList = new ArrayList<>();
        for (Entry<Integer, GroupEntity> entry : groupMap.entrySet()) {
            GroupEntity group = entry.getValue();
            if (group == null) {
                continue;
            }
            if (group.getGroupType() == DBConstant.GROUP_TYPE_NORMAL) {
                normalGroupList.add(group);
            }
        }
        logger.e("groupMap", groupMap.size() + "");
        return normalGroupList;
    }

    // 该方法只有正式群
    // todo eric efficiency
    public List<GroupEntity> getNormalGroupSortedList() {
        List<GroupEntity> groupList = getNormalGroupList();
        Collections.sort(groupList, new Comparator<GroupEntity>() {
            @Override
            public int compare(GroupEntity entity1, GroupEntity entity2) {
                if (entity1.getPinyinElement().pinyin == null) {
                    PinYin.getPinYin(entity1.getMainName(), entity1.getPinyinElement());
                }
                if (entity2.getPinyinElement().pinyin == null) {
                    PinYin.getPinYin(entity2.getMainName(), entity2.getPinyinElement());
                }
                return entity1.getPinyinElement().pinyin.compareToIgnoreCase(entity2.getPinyinElement().pinyin);
            }
        });

        return groupList;
    }

    public GroupEntity findGroup(int groupId) {
        logger.d("group#findGroup groupId:%s", groupId);
        if (groupMap == null || groupMap.size() == 0) {
            logger.e("NULL", "NULL");
        }
        if (groupMap.containsKey(groupId)) {
            logger.e("groupMap", groupMap.size() + "");
            return groupMap.get(groupId);
        }
        logger.e("groupMap", groupMap.size() + "");
        return null;
    }

    public List<GroupEntity> getSearchAllGroupList(String key) {
        List<GroupEntity> searchList = new ArrayList<>();
        for (Entry<Integer, GroupEntity> entry : groupMap.entrySet()) {
            GroupEntity groupEntity = entry.getValue();
            if (IMUIHelper.handleGroupSearch(key, groupEntity)) {
                searchList.add(groupEntity);
            }
        }
        logger.e("groupMap", groupMap.size() + "");
        return searchList;
    }


    /**
     * 获取评论群的成员的信息。因为这帮人不是联系人，需要额外取信息。
     *
     * @param groupId
     */
    public void getGroupNonContactMembers(int groupId) {
        logger.d("group#getGroupMembers groupId:%s", groupId);

        GroupEntity group = findGroup(groupId);
        if (group == null) {
            logger.e("group#no such group id:%s", groupId);
            return;
        }
        Set<Integer> userList = group.getlistGroupMemberIds();
        ArrayList<Integer> memberList = new ArrayList<Integer>();
        for (Integer id : userList) {
            UserEntity contact = IMContactManager.instance().findContact(id);
            if (contact == null) {
                logger.e("group#no such contact id:%s", id);
                memberList.add(id);
            }
        }

        IMContactManager.instance().reqGetDetaillUsers(memberList);
    }

    public List<UserEntity> getGroupMembers(int groupId) {
        logger.d("group#getGroupMembers groupId:%s", groupId);

        GroupEntity group = findGroup(groupId);
        if (group == null) {
            logger.e("group#no such group id:%s", groupId);
            return null;
        }
        Set<Integer> userList = group.getlistGroupMemberIds();
        ArrayList<UserEntity> memberList = new ArrayList<UserEntity>();
        for (Integer id : userList) {
            UserEntity contact = IMContactManager.instance().findContact(id);
            if (contact == null) {
                logger.e("group#no such contact id:%s", id);
                continue;
            }
            memberList.add(contact);
        }
        return memberList;
    }


    /**
     * @param groupId 群id
     * @return 群成员
     */
    public Set<Integer> getGroupMember(String groupId) {
        GroupEntity group = findGroup(Integer.valueOf(groupId));
        if (group == null) {
            return null;
        }
        return group.getlistGroupMemberIds();
    }


    //add by guoq-s
    private void exitFamily(final int groupid) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("familyid", groupid + "");
        OkHttpClientManager.postAsy(ConstantValues.ExitFamily, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                Toast.makeText(ctx.getApplicationContext(), "天下没有不散的筵席", Toast.LENGTH_SHORT).show();
                YXFamilyManager.instance().deleteFamilyInfoDao(groupid);
                CreateStoryTopicEvent createStoryTopicEvent = new CreateStoryTopicEvent();
                createStoryTopicEvent.eventType = CreateStoryTopicEvent.Event.TYPE_DISMISS_FAMILY;
                EventBus.getDefault().post(createStoryTopicEvent);
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(ctx.getApplicationContext(), e.toString(), 1);
            }
        });
    }




    /**
     * @param groupid   群解散。
     * @param isCreator 是否是管理员，如果是管理员，则解散群，否则退群。
     */
    public void quitOrDismissGroup(int groupid, boolean isCreator) {
        if (isCreator) {
            GroupEntity entity = groupMap.get(groupid);
            Set<Integer> userList = entity.getlistGroupMemberIds();
            reqRemoveGroupMember(groupid, userList);
            exitFamily(groupid);
        } else {
            //退出群
            Set<Integer> removeMemberlist = new HashSet<>(1);
            Integer userId = IMLoginManager.instance().getLoginId();
            removeMemberlist.add(userId);
            reqRemoveGroupMember(groupid, removeMemberlist);
        }
        logger.e("groupMap", groupMap.size() + "");
    }
    //add by guoq-e

    /**
     * ------set/get 的定义
     */
    public Map<Integer, GroupEntity> getGroupMap() {
        logger.e("groupMap", groupMap.size() + "");
        return groupMap;
    }

    public boolean isGroupReady() {
        return isGroupReady;
    }


}
