package com.yuxip.imservice.manager;

import com.yuxip.entity.MemberEntity;

import java.util.HashMap;
import java.util.Map;

/**
 * 管理相关数据，目前有：剧群的信息  家族群的信息
 * Created by SummerRC on 2015/6/16.
 */
public class DataManager{
    private Map<String, Map<String, MemberEntity>> storyGroupMemberMap = new HashMap<>();     //剧群信息：   k - v : 剧群的id - 成员集合<k - v : 成员id - 成员实体 >
    private Map<String, Map<String, MemberEntity>> familyGroupMemberMap = new HashMap<>();    //家族群信息： k - v : 家族群的id - 成员集合<k - v : 成员id - 成员实体 >
    private static DataManager instance = new DataManager();

    /**
     * 单例模式
     * @return 返回一个实例
     */
    public static DataManager getInstance() {
        return instance;
    }

    /**
     * 构造函数私有化
     */
    private DataManager() {}

    /**
     * 根据剧群的id获取家族群的成员map
     * @param groupId   家族群id
     * @return          家族群的成员map
     */
    public Map<String, MemberEntity> getStoryGroupMemberMap(String groupId) {
        return  storyGroupMemberMap.get(groupId);
    }

    /**
     * 存储剧群成员信息
     * @param groupId                   家族群id
     * @param memberEntityArrayList     家族群成员map
     */
    public void putStoryGroupMemberMap(String groupId, Map<String, MemberEntity> memberEntityArrayList) {
        if(storyGroupMemberMap.containsKey(groupId)) {
            storyGroupMemberMap.remove(groupId);
        }
        storyGroupMemberMap.put(groupId, memberEntityArrayList);
    }

    public boolean containsGroup(String groupId) {
        if(storyGroupMemberMap.size() > 0) {
            return storyGroupMemberMap.containsKey(groupId);
        } else {
            return false;
        }
    }

    /**
     * 根据剧群的id获取家族群的成员map
     * @param groupId   家族群id
     * @return          家族群的成员map
     */
    public Map<String, MemberEntity> getFamilyGroupMemberMap(String groupId) {
        return familyGroupMemberMap.get(groupId);
    }

    /**
     * 存储家族群成员信息
     * @param groupId                   家族群id
     * @param memberEntityArrayList     家族群成员map
     */
    public void putFamilyGroupMemberMap(String groupId, Map<String, MemberEntity> memberEntityArrayList) {
        if(familyGroupMemberMap.containsKey(groupId)) {
            familyGroupMemberMap.remove(groupId);
        }
        familyGroupMemberMap.put(groupId, memberEntityArrayList);
    }
}
