package com.yuxip.imservice.manager.http;

import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.JsonBean.StoryShowTypeJsonBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/9/23.
 * 包含了主页 家族 喜欢 收藏 的信息 以及 全部家族的 类型 条目
 */
public class HomeEntityManager {
    private static HomeEntityManager instance = new HomeEntityManager();
    private HomeInfo.PersoninfoEntity myPersoninfo;
    private HomeInfo.PersoninfoEntity otherPersoninfo;
    private List<StoryShowTypeJsonBean.FamilytypeEntity> list_familytype = new ArrayList<>();

    public static HomeEntityManager getInstance() {
        return instance;
    }


    public void setMypersonInfo(HomeInfo.PersoninfoEntity myPersoninfo) {
        if (myPersoninfo != null) {
            this.myPersoninfo = myPersoninfo;
        }
    }

    public void setOtherPersoninfo(HomeInfo.PersoninfoEntity otherPersoninfo) {
        if (otherPersoninfo != null)
            this.otherPersoninfo = otherPersoninfo;
    }

    public void setFamilyList(List<StoryShowTypeJsonBean.FamilytypeEntity> list_familytype) {
        if (list_familytype != null)
            this.list_familytype = list_familytype;
    }

    public List<StoryShowTypeJsonBean.FamilytypeEntity> getFamilyList() {
        return this.list_familytype;
    }

    /**
     * 返回list集合 用以收藏或喜欢及家族界面的传递    其中 0  喜欢  1 收藏  2 家族
     */
    public List<HomeInfo.PersoninfoEntity.MyHomeEntity> getUserList(int type, String fromUser) {
        List<HomeInfo.PersoninfoEntity.MyHomeEntity> list = new ArrayList<>();
        switch (type) {
            case 0:
                if (fromUser.equals("0")) {
                    if(otherPersoninfo == null) {
                        return list;
                    }
                    list = otherPersoninfo.getMyfavor();
                } else {
                    if(myPersoninfo == null) {
                        return list;
                    }
                    list = myPersoninfo.getMyfavor();
                }

                break;
            case 1:
                if (fromUser.equals("0")) {
                    if(otherPersoninfo == null) {
                        return list;
                    }
                    list = otherPersoninfo.getMycollects();
                } else {
                    if(myPersoninfo == null) {
                        return list;
                    }
                    list = myPersoninfo.getMycollects();
                }
                break;
            case 2:
                if (fromUser.equals("0")) {
                    if(otherPersoninfo == null) {
                        return list;
                    }
                    list = otherPersoninfo.getMyfamily();
                } else {
                    if(myPersoninfo == null) {
                        return list;
                    }
                    list = myPersoninfo.getMyfamily();
                }

                break;
        }
        return list;
    }


}
