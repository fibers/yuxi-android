package com.yuxip.imservice.manager.http;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.JsonBean.StoryShowTypeJsonBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/10/20.
 */
public class ChannelSelectDataManager {
    private static ChannelSelectDataManager instance;
    private DBInterface dbInterface;
    private boolean isUnfold = false;   //是否展开状态
    private String loadType;            // 0 表示剧的  1 表示自戏的
    private List<NodesEntity> listStoryAll = new ArrayList<>();
    private List<NodesEntity> listPlayAll = new ArrayList<>();
    private List<NodesEntity> listStorySave = new ArrayList<>();
    private List<NodesEntity> listStoryShow = new ArrayList<>();
    private List<NodesEntity> listPlayShow = new ArrayList<>();
    private List<NodesEntity> listPlaySave = new ArrayList<>();

    public ChannelSelectDataManager(){
        super();
        dbInterface = DBInterface.instance();
    }
    public static  ChannelSelectDataManager getInstance(){
        if(instance == null){
            instance = new ChannelSelectDataManager();
        }
        return  instance;
    }

    public boolean isUnfold() {
        return isUnfold;
    }

    public void setIsUnfold(boolean isUnfold) {
        this.isUnfold = isUnfold;
    }


    public List<NodesEntity> getListStoryAll() {
        return listStoryAll;
    }

    public List<NodesEntity> getListPlayAll() {
        return listPlayAll;
    }

    public List<NodesEntity> getListStoryShow() {
        return listStoryShow;
    }

    public List<NodesEntity> getListPlayShow() {
        return listPlayShow;
    }

    public  List<NodesEntity> getListAll(String loadType){
        if(loadType.equals("0")){
            return  this.listStoryAll;
        }else{
            return  this.listPlayAll;
        }
    }


    public void setListAll(List<StoryShowTypeJsonBean.ShowtypeEntity.NodesEntity> listAll,String loadType) {
        if(loadType.equals("0")){
            listStoryAll.clear();
            for(int i = 0; i<listAll.size();i++){
             listAll.get(i).set_id(i);
             listStoryAll.add(listAll.get(i));
            }
        }else{
            listPlayAll.clear();
            for(int i=0;i<listAll.size();i++){
                listAll.get(i).set_id(i);
                listPlayAll.add(listAll.get(i));
            }
        }

    }

    /**
     * 这里初始化 list\
     */
    public void initListShow(List<NodesEntity> listAll,String loadType){
        if(loadType.equals("0")){
            try {
                listStorySave = dbInterface.loadAllNodesEntityByEntityType("0");
                if(listStorySave==null||listPlaySave.size()==0){
                    if(listAll!=null&&listAll.size()>0){
                        setLoadType(listAll,"0");
                        for(int i = 0;i<7;i++){
                            dbInterface.batchInsertOrUpdateNodesEntity(listAll.get(i));
                        }
                    }
                    listStorySave = dbInterface.loadAllNodesEntityByEntityType("0");
                }
                checkListStoryShow(listAll);
            }catch (Exception e){
                listStorySave = new ArrayList<>();
            }

        }else{
            listPlaySave = dbInterface.loadAllNodesEntityByEntityType("1");
        }

    }

    /**
     * 给每个node打标签 用于后期与自习数据区分  0表示剧 1 表示自戏
     */
    private void setLoadType(List<NodesEntity> list,String loadType){
        if(list!=null){
            for (int i = 0 ;i<list.size();i++){
                list.get(i).setLoadType(loadType);
            }
        }
    }

    /**
     * 判断本地存储的是否包含在请求过来的数据之中，从而重置集合与数据存储
     */
    private void checkListStoryShow(List<NodesEntity> listAll) {
        listStoryShow.clear();
        for (int i = 0; i < listStorySave.size(); i++) {
            for (int j = 0; j < listAll.size(); j++) {
                if (listAll.get(j).getId().equals(listStorySave.get(i).getId())) {
                    listStoryShow.add(listStorySave.get(i));
                    break;
                }
            }
        }
        if (listStoryShow.size() != listStorySave.size()) {
            dbInterface.deleteNodesEntityByTypeId("0");
            for (int i = 0; i < listStoryShow.size(); i++) {
                dbInterface.batchInsertOrUpdateNodesEntity(listStoryShow.get(i));
            }

        }
    }





}
