package com.yuxip.imservice.manager.http;

import android.text.TextUtils;
import com.yuxip.utils.DateUtil;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/10/12.
 * 广场评论 楼层的管理类 包含 位置记录 构造楼层内容等
 */
public class SquareCommentManager {

 private static SquareCommentManager instance;
 private List<Long> list_long_1 = new ArrayList<>();    //用以存储 话题id的集合主要用于判断 话题楼层的展开状态 全部评论的
 private List<Long> list_long_2 = new ArrayList<>();   //用以存储 话题id的集合主要用于判断 话题楼层的展开状态 热门评论的
 private int foldPopPosition = -1;
 private String type = "";      //类型   FLOOR_TYPE_STORY 表示 剧或自戏的   FLOOR_TYPE_TOPIC 表示话题的
 private String lzId = "";
 private String topicId = "";   //话题的 topicid
 private String storyId = "";   //剧或自戏的 storyid
 private String regex = "&$louzhu$&";   //构造内容时 用来替换 lz图片的文字

public static SquareCommentManager getInstance(){
    if(instance==null){
        instance  = new SquareCommentManager();
    }
    return  instance;
}

    public void addNewData(long value){
        addValue(value);
    }

    public void addNewHotData(long value){
        addHotValue(value);
    }
    public void removeData(long value){
        list_long_1.remove(value);
    }

    public void clearList(){
        list_long_1.clear();
        list_long_2.clear();
    }


    /**
     * 判断全部评论的展开项
     * @param value
     * @return
     */
    public  boolean containsValeu(long value){
        for(int i=0;i<list_long_1.size();i++){
            if(list_long_1.get(i)==value){
                return true;
            }
        }
        return  false;
    }


    /***
     * 判断热门评论的展开项
     * @param value
     * @return
     */
    public boolean containsHotValue(long value){
        for(int i=0;i<list_long_2.size();i++){
            if(list_long_2.get(i) == value){
                return true;
            }
        }
        return false;
    }


    /**
     * 添加全部评论的展开项 这里放的是commentid
     * @param value
     */
    private void addValue(long value){
        for(int i=0;i<list_long_1.size();i++){
            if(list_long_1.get(i)==value){
                return;
            }
        }
        list_long_1.add(value);
    }


    /**
     * 添加热门评论的展开项 这里放的是commentid
     * @param value
     */
    private void addHotValue(long value){
        for (int i =0;i<list_long_2.size();i++){
            if(list_long_2.get(i) ==value){
                return;
            }
        }
        list_long_2.add(value);
    }

    /**
     * 这里存储的是右侧 展开的pop的展开状况
     * @param position
     */
    public void setFoldPopPosition(int position){
        this.foldPopPosition = position;
    }

    public void resetFoldPopPosition(){
        this.foldPopPosition = -1;
    }

    public int getFoldPopPosition() {
        return foldPopPosition;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public void setLzId(String lzId){
        this.lzId = lzId;
    }

    public String getLzId() {
        return lzId;
    }

    public String getRegex() {
        return regex;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public String getType() {
        return type;
    }

    public String getStoryId() {
        return storyId;
    }

    public  boolean isLz(String passId){
        if(!TextUtils.isEmpty(passId)&&passId.equals(lzId)){
            return  true;
        }else{
            return false;
        }
    }


    /**
     *  构造发送者名字  如果是楼主 需要额外添加一个字符串
     * @param name
     * @param isLz
     * @return
     */
    public String constructFloorName(String name,boolean isLz){
        String resultName;
        if(isLz){
            resultName = name +" "+regex +"：";
        }else{
            resultName = name +"：";
        }
        return  resultName;
    }

    /**
     *  构造发送内容 如果发送者名字不为空 则。。。
     * @param toUserName
     * @param content
     * @param time
     * @return
     */
    public String constructCommentContent(String toUserName,String content,String time){
        String resultContent;
        if(!TextUtils.isEmpty(toUserName)){
            resultContent = "回复 "+toUserName+"："+content+"  "+ DateUtil.getDateWithHoursAndSeconds(Long.valueOf(time));
        }else{
            resultContent = content +"  "+DateUtil.getDateWithHoursAndSeconds(Long.valueOf(time));
        }
        return  resultContent;

    }
    public String constructCommentContent(String toUserName){
        String resultContent="";
        if(!TextUtils.isEmpty(toUserName)){
            resultContent = "回复 "+toUserName+"：";
        }
        return  resultContent;

    }


}

