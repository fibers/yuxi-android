package com.yuxip.entity;

import java.util.ArrayList;

/**
 * 请求好友列表返回好友集用到的好友实体
 * @author SummerRC
 *
 */
public class FriendEntity {
	private int _id;
	private String id;
	private String gender;
	private String name;
	private String portrait;
	private String FirstLetter;
    private String sortLetters;  //显示数据拼音的首字母

    public static ArrayList<String> FirstLetterList = new ArrayList<String>();
    public String getSortLetters() {
        return sortLetters;
    }
    public void setSortLetters(String sortLetters) {
        this.sortLetters = sortLetters;
    }
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPortrait() {
		return portrait;
	}
	public void setPortrait(String portrait) {
		this.portrait = portrait;
	}
	public String getFirstLetter() {
		return FirstLetter;
	}
	public void setFirstLetter(String FirstLetter) {
		this.FirstLetter = FirstLetter;
	}
	@Override
	public String toString() {
		return "id: " + id + "name: " + name 
				+ "FirstLetter: " + FirstLetter 
				+ "portrait: " + portrait + "gender: " 
				+ gender;
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}
}
