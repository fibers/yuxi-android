package com.yuxip.entity;

/**
 * Created by Administrator on 2015/5/4.
 *
 * 剧的界面的bean
 */
public class StorysEntity {

    private String id;
    private String title; //标题
    private String creatorid;
    private String creatorname; //昵称
    private String portrait; //地址链接
    private String praisenum; //数量
    private String relation;


    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public String getCreatorname() {
        return creatorname;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getPraisenum() {
        return praisenum;
    }

    public String getRelation() {
        return relation;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    public void setCreatorname(String creatorname) {
        this.creatorname = creatorname;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setPraisenum(String praisenum) {
        this.praisenum = praisenum;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }




}
