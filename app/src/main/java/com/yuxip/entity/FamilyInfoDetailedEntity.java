package com.yuxip.entity;

import java.util.ArrayList;

/**
 * 家族详细信息
 * Created by SummerRC on 2015/5/19.
 */
public class FamilyInfoDetailedEntity {
    private String id = "";
    private String intro = "";
    private String name = "";
    private String nickname = "";
    private String ownerid = "";
    private String ownername = "";       //创建者昵称
    private String ownernickname = "";   //创建者在群众中昵称
    private String portrait = "";
    private String rank = "";
    private String creattime = "";
    private String isvip = "";
    private ArrayList<MemberEntity> memberList;

    private int member_num;

    public String getIsvip() {
        return isvip;
    }

    public void setIsvip(String isvip) {
        this.isvip = isvip;
    }

    public int getMember_num() {
        return member_num;
    }

    public void setMember_num(int member_num) {
        this.member_num = member_num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getOwnerid() {
        return ownerid;
    }

    public void setOwnerid(String ownerid) {
        this.ownerid = ownerid;
    }

    public String getOwnername() {
        return ownername;
    }

    public void setOwnername(String ownername) {
        this.ownername = ownername;
    }

    public String getOwnernickname() {
        return ownernickname;
    }

    public void setOwnernickname(String ownernickname) {
        this.ownernickname = ownernickname;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public ArrayList<MemberEntity> getMemberList() {
        return memberList;
    }

    public void setMemberList(ArrayList<MemberEntity> memberList) {
        this.memberList = memberList;
        member_num = memberList.size();
    }
}
