package com.yuxip.entity;

/**
 * 家族信息
 * Created by SummerRC on 2015/5/18.
 */
public class FamilyInfoEntity {
    private String id;
    private String intro;
    private String portrait;
    private String name;
    private String rank;
    private String creattime;
    private String number;
    private String creatorid;
    private String ismember;
    private String isvip;
    private String creatorname;
    public void setIsmember(String ismember) {
        this.ismember = ismember;
    }

    public String getIsmember() {
        return ismember;
    }

    public String getCreatorname() {
        return creatorname;
    }

    public void setCreatorname(String creatorname) {
        this.creatorname = creatorname;
    }


    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public String getIsvip() {
        return isvip;
    }

    public void setIsvip(String isvip) {
        this.isvip = isvip;
    }
}

