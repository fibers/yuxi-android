package com.yuxip.entity;

import java.io.Serializable;

/**
 * Created by SummerRC on 2015/5/17.
 */
public class GroupsEntity implements Serializable{
    public String getGroupId() {
        return groupId;
    }

    public String getTitle() {
        return title;
    }

    public String getIsPlay() {
        return isPlay;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIsPlay(String isPlay) {
        this.isPlay = isPlay;
    }

    private String groupId;
    private String title;
    private String isPlay;
}
