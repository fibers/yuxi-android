package com.yuxip.entity;

/**
 * Created by Administrator on 2015/8/11.
 */
public class StoryBookMarkEntity {

    private int id;
    private String storyId;
    private String markItem;
    private String totalCount;
    private String fromMsgId;

    public String getFromMsgId() {
        return fromMsgId;
    }

    public void setFromMsgId(String fromMsgId) {
        this.fromMsgId = fromMsgId;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public int getId() {
        return id;
    }

    public String getStoryId() {
        return storyId;
    }

    public String getMarkItem() {
        return markItem;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setStoryId(String storyId) {
        this.storyId = storyId;
    }

    public void setMarkItem(String markItem) {
        this.markItem = markItem;
    }
}
