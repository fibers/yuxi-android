package com.yuxip.entity;



/**
 * Created by Administrator on 2015/9/21.
 * family类型的保存
 */
public class FamilytypeEntity {
        private int id;
        private String typeid;
        private String typename;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTypeid() {
            return typeid;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }
}
