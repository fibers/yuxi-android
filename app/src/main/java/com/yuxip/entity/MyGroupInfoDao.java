package com.yuxip.entity;

/**
 * Created by dev01 on 15/6/15.
 */
public class MyGroupInfoDao {

    private     int     id;

    private     int     groupId;
    private     int     storyId;
    private     int     isPlay;     //审核群：0  水聊群：1  对戏群：2  评论群:3
    private     String  title;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }

    public int getIsPlay() {
        return isPlay;
    }

    public void setIsPlay(int isPlay) {
        this.isPlay = isPlay;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
