package com.yuxip.entity;

/**
 * 主线人物实体类
 * Created by SummerRC on 2015/5/25.
 */
public class MainPersonEntity {
    private String id;
    private String title;
    private String intro;
    private String num;

    public void setNum(String num) {
        this.num = num;
    }

    public String getNum() {
        return num;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }
}
