package com.yuxip.entity;

/**
 * Created by SummerRC on 2015/8/12.
 * 好友昵称
 */
public class RemarkNameEntity {
    private  int id;
    private String userid;
    private String realname;
    private String nickname;

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }


    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
