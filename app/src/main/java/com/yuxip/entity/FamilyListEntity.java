package com.yuxip.entity;

/**
 * Created by Administrator on 2015/5/5.
 */
public class FamilyListEntity {


    private String id;
    private String intro; //信息
    private String name;
    private String portrait; //图片地址
    private String rank;
    private String creattime; //创建时间
    private String isMember;

    public void setIsMember(String isMember) {
        this.isMember = isMember;
    }

    public String getIsMember() {
        return isMember;
    }

    public String getId() {
        return id;
    }

    public String getIntro() {
        return intro;
    }

    public String getName() {
        return name;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getRank() {
        return rank;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }
}
