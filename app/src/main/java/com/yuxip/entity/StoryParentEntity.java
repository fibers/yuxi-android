package com.yuxip.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dev01 on 15/6/15.
 */
public class StoryParentEntity {

    private int id;
    private String title;
    private String creatorid;
    private String creatorname;
    private String portrait;
    private String praisenum;
    private String intro;
    private String relation;
    private String applynums;
    private String type;
    private String ispraisedByUser;

    private String rule;
    private String copyright;
    private String role;
    private String nickname;
    private String wordsnum;
    private String catetory;
    private String storyimg;
    private int     storyId;
    private String unreadCount;
    private String createtime;
    private List<MyGroupInfoDao> myGroupInfoDaoList;

    public int getStoryId() {
        return storyId;
    }

    public void setStoryId(int storyId) {
        this.storyId = storyId;
    }


    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }


    public void setStoryimg(String storyimg) {
        this.storyimg = storyimg;
    }

    public String getStoryimg() {
        return storyimg;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public void setCatetory(String catetory) {
        this.catetory = catetory;
    }

    public String getCatetory() {
        return catetory;
    }

    public String getCreatetime() {
        return createtime;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }


    public String getCopyright() {
        return copyright;
    }

    public String getRole() {
        return role;
    }

    public String getNickname() {
        return nickname;
    }

    public String getWordsnum() {
        return wordsnum;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setWordsnum(String wordsnum) {
        this.wordsnum = wordsnum;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public String getCreatorname() {
        return creatorname;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getPraisenum() {
        return praisenum;
    }

    public String getIntro() {
        return intro;
    }

    public String getRelation() {
        return relation;
    }

    public String getApplynums() {
        return applynums;
    }

    public String getType() {
        return type;
    }

    public String getIspraisedByUser() {
        return ispraisedByUser;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    public void setCreatorname(String creatorname) {
        this.creatorname = creatorname;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setPraisenum(String praisenum) {
        this.praisenum = praisenum;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public void setApplynums(String applynums) {
        this.applynums = applynums;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setIspraisedByUser(String ispraisedByUser) {
        this.ispraisedByUser = ispraisedByUser;
    }


    public List<MyGroupInfoDao> getMyGroupInfoDaoList() {
        if(myGroupInfoDaoList == null) {
            myGroupInfoDaoList = new ArrayList<>();
        }
        return myGroupInfoDaoList;
    }

    public void addMyGroupInfoDaoItem(MyGroupInfoDao myGroupInfoDao) {
        if(myGroupInfoDaoList == null) {
            myGroupInfoDaoList = new ArrayList<>();
        }
        myGroupInfoDaoList.add(myGroupInfoDao);
    }
}
