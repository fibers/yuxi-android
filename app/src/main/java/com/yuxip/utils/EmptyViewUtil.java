package com.yuxip.utils;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by SummerRC on 2015/8/25.
 * description : 用于辅助ListView为空时填充默认内容
 */
public class EmptyViewUtil {

    /**
     *
     * @param context   上下文参数
     * @param listView  当ListView条目为空时，显示默认视图
     * @param text      默认视图上的文本内容
     * @return          TextView
     */
    public static TextView getView(Context context, ListView listView, String text) {
        TextView emptyView = new TextView(context);
        emptyView.setGravity(Gravity.CENTER);
        emptyView.setText(text);
        emptyView.setVisibility(View.GONE);
        emptyView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((ViewGroup)listView.getParent()).addView(emptyView);
        listView.setEmptyView(emptyView);

        return emptyView;
    }
}
