package com.yuxip.utils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.Animation;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

public class UgcAnimationUtil {

    private static int xOffset = 0;
    private static int yOffset = 100;

    public static void initOffset(Context context) {
        xOffset = (int) (10.667 * context.getResources().getDisplayMetrics().density);
        yOffset = -(int) (8.667 * context.getResources().getDisplayMetrics().density);
    }

    public static Animation getRotateAnimation(float fromDegrees,
                                               float toDegrees, int durationMillis) {
        RotateAnimation rotate = new RotateAnimation(fromDegrees, toDegrees,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        rotate.setDuration(durationMillis);
        rotate.setFillAfter(true);
        return rotate;
    }

    public static void startAnimationsIn(ViewGroup viewgroup, int durationMillis) {
        viewgroup.setVisibility(View.VISIBLE);
        for (int i = 0; i < viewgroup.getChildCount(); i++) {
            View inoutimagebutton = viewgroup
                    .getChildAt(i);
//            inoutimagebutton.setVisibility(View.VISIBLE);
            MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton
                    .getLayoutParams();
            System.out.println("in-" + mlp.rightMargin);
            Animation animation = new TranslateAnimation(mlp.rightMargin
                    - xOffset, 0F, yOffset + mlp.bottomMargin, 0F);

            animation.setFillAfter(true);
            animation.setDuration(durationMillis);
            animation.setStartOffset((i * 100)
                    / (-1 + viewgroup.getChildCount()));
            animation.setInterpolator(new OvershootInterpolator(2F));
            inoutimagebutton.startAnimation(animation);
        }
    }

    public static void startAnimationsOut(final ViewGroup viewgroup,
                                          int durationMillis) {
        for (int i = 0; i < viewgroup.getChildCount(); i++) {
            final View inoutimagebutton = viewgroup
                    .getChildAt(i);
            MarginLayoutParams mlp = (MarginLayoutParams) inoutimagebutton
                    .getLayoutParams();
            System.out.println("out-" + mlp.rightMargin);
            Animation animation = new TranslateAnimation(0F, mlp.rightMargin
                    - xOffset, 0F, yOffset + mlp.bottomMargin);

            animation.setFillAfter(true);
            animation.setDuration(durationMillis);
            animation.setStartOffset(((viewgroup.getChildCount() - i) * 100)
                    / (-1 + viewgroup.getChildCount()));
            animation.setInterpolator(new AnticipateInterpolator(2F));
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation arg0) {
                }

                @Override
                public void onAnimationRepeat(Animation arg0) {
                }

                @Override
                public void onAnimationEnd(Animation arg0) {
                    // TODO Auto-generated method stub
//                    inoutimagebutton.setVisibility(View.GONE);
                    viewgroup.setVisibility(View.GONE);
                }
            });
            inoutimagebutton.startAnimation(animation);
        }

    }

    public static void doLikeAnimation(View view, Animator.AnimatorListener listener) {
        if (view == null) {
            return;
        }
        if (view.getAnimation() != null) {
            if(view.getAnimation().hasStarted() && !view.getAnimation().hasEnded()){
                return;
            }
        }
        ObjectAnimator anim1 = ObjectAnimator.ofFloat(view, "scaleX", 1f, 1.5f).setDuration(300);
        ObjectAnimator anim2 = ObjectAnimator.ofFloat(view, "scaleY", 1f, 1.5f).setDuration(300);
        ObjectAnimator anim3 = ObjectAnimator.ofFloat(view, "scaleX", 1.5f, 1);
        anim3.setStartDelay(300);
        ObjectAnimator anim4 = ObjectAnimator.ofFloat(view, "scaleY", 1.5f, 1);
        anim4.setStartDelay(300);
        AnimatorSet animSet = new AnimatorSet();
        animSet.playTogether(anim1, anim2, anim3, anim4);
        if (listener != null) {
            animSet.addListener(listener);
        }
        animSet.start();
    }

}