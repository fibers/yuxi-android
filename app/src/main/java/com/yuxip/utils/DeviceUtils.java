package com.yuxip.utils;

/**
 * Created by Animastor on 2015/8/4.
 */


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;




public class DeviceUtils {



    public static synchronized String getDeviceId(Context context) {
        String DEVICE_ID="";
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        DEVICE_ID = tm.getDeviceId();
        if(TextUtils.isEmpty(DEVICE_ID))
        return DEVICE_ID;
        DEVICE_ID = "35" +

                Build.BOARD.length()%10 +
                Build.BRAND.length()%10 +
                Build.CPU_ABI.length()%10 +
                Build.DEVICE.length()%10 +
                Build.DISPLAY.length()%10 +
                Build.HOST.length()%10 +
                Build.ID.length()%10 +
                Build.MANUFACTURER.length()%10 +
                Build.MODEL.length()%10 +
                Build.PRODUCT.length()%10 +
                Build.TAGS.length()%10 +
                Build.TYPE.length()%10 +
                Build.USER.length()%10 ;
        if(TextUtils.isEmpty(DEVICE_ID))
            return  DEVICE_ID;
        return  null;

    }



    public static final synchronized PackageInfo getSoftwarePackageInfo(Context context) {
        if (context == null)
            return null;
        PackageManager packageManager = context.getPackageManager();
        PackageInfo packInfo = null;
        try {
            packInfo = packageManager.getPackageInfo(context.getPackageName(),
                    0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return packInfo;
    }

}


