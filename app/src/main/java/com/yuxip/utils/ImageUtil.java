package com.yuxip.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;

import com.yuxip.ui.helper.PhotoHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * @Description 图片处理
 *
 */
public class ImageUtil {
    private static Logger logger = Logger.getLogger(ImageUtil.class);

    public static Bitmap getBigBitmapForDisplay(Bitmap bitmap,
                                                Context context) {

        float max_bmp_width_height = 2048;
        if (null == bitmap)
            return null;
        try {

            DisplayMetrics dm = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
            float scale = bitmap.getWidth() / max_bmp_width_height;
            float scaleh = bitmap.getHeight() / max_bmp_width_height;

            if (scale > scaleh) {
                scale = scaleh;
            }

            Bitmap newBitMap = bitmap;
            if (scale > 1) {
                newBitMap = zoomBitmap(bitmap, (int) (bitmap.getWidth() / scale), (int) (bitmap.getHeight() / scale));
                bitmap.recycle();
                return newBitMap;
            }

            return newBitMap;
        } catch (Exception e) {
            logger.e(e.getMessage());
            return null;
        }
    }

	public static Bitmap getBigBitmapForDisplay(String imagePath,
			Context context) {
		if (null == imagePath || !new File(imagePath).exists())
			return null;
		try {
			int degeree = PhotoHelper.readPictureDegree(imagePath);
			Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
			if (bitmap == null)
				return null;
			DisplayMetrics dm = new DisplayMetrics();
			((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(dm);
			float scale = bitmap.getWidth() / (float) dm.widthPixels;
			Bitmap newBitMap = null;
			if (scale > 1) {
				newBitMap = zoomBitmap(bitmap, (int) (bitmap.getWidth() / scale), (int) (bitmap.getHeight() / scale));
				bitmap.recycle();
				Bitmap resultBitmap = PhotoHelper.rotaingImageView(degeree, newBitMap);
				return resultBitmap;
			}
			Bitmap resultBitmap = PhotoHelper.rotaingImageView(degeree, bitmap);
			return resultBitmap;
		} catch (Exception e) {
			logger.e(e.getMessage());
			return null;
		}
	}

	private static Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {
		if (null == bitmap) {
			return null;
		}
		try {
			int w = bitmap.getWidth();
			int h = bitmap.getHeight();
			Matrix matrix = new Matrix();
			float scaleWidth = ((float) width / w);
			float scaleHeight = ((float) height / h);
			matrix.postScale(scaleWidth, scaleHeight);
			Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
			return newbmp;
		} catch (Exception e) {
			logger.e(e.getMessage());
			return null;
		}
	}

	//返回一个绝对路径、作为照片的存储位置
	public static String getPicPath() {
		//获得SD卡的路径
		final String SDpath = Environment.getExternalStorageDirectory().getAbsolutePath();
		File file = new File(SDpath +"/higgs/yuxi/");
		if(!file.exists()) {
			// 创建文件夹
			file.mkdirs();
		}
		//照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同
		Date date = new Date();
		//获取当前时间并且进一步转化为字符串
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
		String picName = format.format(date);
		String path = SDpath + "/higgs/yuxi/" + picName + ".jpg";
		return path;
	}

	/**
	 * 保存头像图片到指定路径
	 * @param bitmap
	 */
	public static String saveBitmapImg(Bitmap bitmap) {
		File file = new File(Environment.getExternalStorageDirectory() +"/higgs/yuxi/");

		if(!file.exists()) {
			file.mkdirs();
		}
		String filePath=ImageUtil.getPicPath();
		FileOutputStream fos;
		try {
			fos = new FileOutputStream(filePath);
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);               // (0 - 100)压缩文件
			if(!bitmap.isRecycled()){
				try {
					bitmap.recycle();
					bitmap = null;
				}catch (Exception e){
					e.printStackTrace();
				}finally {
					System.gc();
				}

			}

			return  filePath;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return filePath;
	}

	/**
	 *  获取uri对应的的真实路径
	 * @param uri
	 * @param contentResolver
	 * @return
	 */
	public static  String getUriPath(Uri uri,ContentResolver contentResolver) {
		String[] proj = { MediaStore.Images.Media.DATA };
		Cursor actualImageCursor = contentResolver.query(uri, proj, null, null, null);
		int actual_image_column_index = actualImageCursor
				.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		actualImageCursor.moveToFirst();
		String currentImagePath = actualImageCursor
				.getString(actual_image_column_index);
		return currentImagePath;
	}

	/**
	 * 	获取 对应路径下的图片bitmap 尺寸大小限制1024 超过一定倍率以最小长度压缩
	 * @param context
	 * @param path
	 * @param compress
	 * @return
	 */
	public static Bitmap getPathBitmap(Context context,String path,boolean compress){
		int limitvalue = 1024;
		BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap bitmap;
		if(compress) {
			int scaleSize ;
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path,options);
			int width =options.outWidth;
			int height = options.outHeight;
			if(width > limitvalue && height >limitvalue){
				if(width > height){
					scaleSize = (int) Math.round(((double)height)/limitvalue);
				}else{
					scaleSize = (int) (Math.round((double) width)/limitvalue);
				}
				options.inSampleSize = scaleSize ==0?1:scaleSize;
			}else{
				options.inSampleSize = 1;
			}



		}else{
			options.inSampleSize = 1;
		}
		options.inJustDecodeBounds = false;
		options.inInputShareable = true;
		options.inPurgeable = true;
//		options.inPreferredConfig = Bitmap.Config.RGB_565;
		bitmap = BitmapFactory.decodeFile(path, options);
		bitmap = ImageUtil.getBigBitmapForDisplay(bitmap,context);
		return  bitmap;
	}

	/**
	 * 	获取 对应路径下的图片bitmap 尺寸大小限制1024 超过一定倍率以最小长度压缩
	 * @param context
	 * @param path
	 * @return
	 */
	public static Bitmap getPathBitmap(Context context,String path,int limitvalue){
		BitmapFactory.Options options = new BitmapFactory.Options();
		Bitmap bitmap;
			int scaleSize ;
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeFile(path,options);
			int width =options.outWidth;
			int height = options.outHeight;
			if(width > limitvalue && height >limitvalue){
				if(width > height){
					scaleSize = (int) Math.round(((double)height)/limitvalue);
				}else{
					scaleSize = (int) (Math.round((double) width)/limitvalue);
				}
				options.inSampleSize = scaleSize ==0?1:scaleSize;
			}else{
				options.inSampleSize = 1;
			}

		options.inJustDecodeBounds = false;
		bitmap = BitmapFactory.decodeFile(path, options);
		bitmap = ImageUtil.getBigBitmapForDisplay(bitmap,context);
		return  bitmap;
	}

	/**
	 * 以最省内存的方式读取本地资源的图片
	 * @param context
	 * @param resId
	 * @return
	 */
	public static Bitmap readBitMap(Context context, int resId){
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		//获取资源图片
		InputStream is = context.getResources().openRawResource(resId);
		return BitmapFactory.decodeStream(is,null,opt);
	}
}
