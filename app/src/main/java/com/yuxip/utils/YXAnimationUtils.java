package com.yuxip.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.yuxip.R;

public class YXAnimationUtils {

    /**
     * 抖动的动画
     *
     * @param context   上下文参数
     * @param view      播放动画的view
     */
    public static void skakeAnimation(Context context, View view) {
        Animation shake = AnimationUtils.loadAnimation(context, R.anim.shake);
        view.startAnimation(shake);
    }

}