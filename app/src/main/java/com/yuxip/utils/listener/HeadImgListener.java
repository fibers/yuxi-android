package com.yuxip.utils.listener;

/**
 * Created by guoq on 15-5-23.
 */
public interface HeadImgListener {

    void notifyImgUploadFinished(String url);
}
