package com.yuxip.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.ContextThemeWrapper;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TextView.BufferType;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.yuxip.DB.entity.DepartmentEntity;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.JsonBean.BookDetailResult;
import com.yuxip.R;
import com.yuxip.config.DBConstant;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.config.UrlConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.entity.SearchElement;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.activity.add.CreateFamilyActivity;
import com.yuxip.ui.activity.add.CreateStoryActivity;
import com.yuxip.ui.activity.add.CreateZiXiActivity;
import com.yuxip.ui.activity.add.ReleaseTopicActivity;
import com.yuxip.ui.activity.add.SelectStoryClassActivity;
import com.yuxip.ui.activity.chat.AddOrEditDoActivity;
import com.yuxip.ui.activity.chat.EditFriendGroupActivity;
import com.yuxip.ui.activity.chat.EditRemarkNameActivity;
import com.yuxip.ui.activity.chat.FFMessageCommentActivity;
import com.yuxip.ui.activity.chat.FamilyDataActivity;
import com.yuxip.ui.activity.chat.FriendGroupManagerActivity;
import com.yuxip.ui.activity.chat.FriendSearchActivity;
import com.yuxip.ui.activity.chat.FriendSettingActivity;
import com.yuxip.ui.activity.chat.GroupCardActivity;
import com.yuxip.ui.activity.chat.GroupMessageSettingActivity;
import com.yuxip.ui.activity.chat.SearchFamilyActivity;
import com.yuxip.ui.activity.chat.SystemMessageActivity;
import com.yuxip.ui.activity.chat.drama.DramaChatActivity;
import com.yuxip.ui.activity.chat.family.FamilyChatActivity;
import com.yuxip.ui.activity.chat.friend.FriendChatActivity;
import com.yuxip.ui.activity.home.HisFamilyE;
import com.yuxip.ui.activity.home.Hisfamily;
import com.yuxip.ui.activity.home.Hisplay;
import com.yuxip.ui.activity.home.UserHomePageActivity;
import com.yuxip.ui.activity.story.ApplyRoleActivity;
import com.yuxip.ui.activity.story.ApplyRoleAdminActivity;
import com.yuxip.ui.activity.add.story.CreateStoryFifthActivity;
import com.yuxip.ui.activity.add.story.CreateStoryFirstActivity;
import com.yuxip.ui.activity.add.story.CreateStoryForthActivity;
import com.yuxip.ui.activity.add.story.CreateStorySecondActivity;
import com.yuxip.ui.activity.add.story.CreateStorySixthActivity;
import com.yuxip.ui.activity.add.story.CreateStoryThirdActivity;
import com.yuxip.ui.activity.home.RoleApplyListActivity;
import com.yuxip.ui.activity.home.RoleAuditActivity;
import com.yuxip.ui.activity.other.FamilyDetailsActivity;
import com.yuxip.ui.activity.other.FavorOrCollectActivity;
import com.yuxip.ui.activity.other.GroupLabelActivity;
import com.yuxip.ui.activity.other.GroupMemberInviteActivity;
import com.yuxip.ui.activity.other.GroupMemberSelectActivity;
import com.yuxip.ui.activity.other.InvitedGroupMembersActivity;
import com.yuxip.ui.activity.other.LoginMobileActivity;
import com.yuxip.ui.activity.other.ModifyFamilyPersonInfoActivity;
import com.yuxip.ui.activity.other.ModifyMyStoryActivity;
import com.yuxip.ui.activity.other.PreviewReadImgActivity;
import com.yuxip.ui.activity.other.ReportActivity;
import com.yuxip.ui.activity.other.SplashActivity;
import com.yuxip.ui.activity.square.ReportCommentActivity;
import com.yuxip.ui.activity.square.ResponseFloorActivity;
import com.yuxip.ui.activity.square.SquareCommentActivity;
import com.yuxip.ui.activity.square.TopicDetailsActivity;
import com.yuxip.ui.activity.story.AddGroupAnnouncementActivity;
import com.yuxip.ui.activity.story.CopyRightActivity;
import com.yuxip.ui.activity.story.GroupAnnouncementActivity;
import com.yuxip.ui.activity.story.ReadRightSettingActivity;
import com.yuxip.ui.activity.story.StoryDetailsActivity;
import com.yuxip.ui.activity.story.StoryEditActivity;
import com.yuxip.ui.activity.story.StoryGroupDetailsActivity;
import com.yuxip.ui.activity.story.StoryGroupSelectActivity;
import com.yuxip.ui.activity.story.StoryPersonSettingModuleActivity;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.utils.pinyin.PinYin.PinYinElement;

import java.util.ArrayList;

public class IMUIHelper {

    // 在视图中，长按用户信息条目弹出的对话框
    public static void handleContactItemLongClick(final UserEntity contact, final Context ctx) {
        if (contact == null || ctx == null) {
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle(contact.getMainName());
        String[] items = new String[]{ctx.getString(R.string.check_profile),
                ctx.getString(R.string.start_session)};

        final int userId = contact.getPeerId();
        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
//                        IMUIHelper.openUserProfileActivity(ctx, userId);
                        break;
                    case 1:
                        //IMUIHelper.openChatActivity(ctx, contact.getSessionKey());
                        break;
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


    // 根据event 展示提醒文案
    public static int getLoginErrorTip(LoginEvent event) {
        switch (event) {
            case LOGIN_AUTH_FAILED:
                return R.string.login_error_general_failed;
            case LOGIN_INNER_FAILED:
                return R.string.login_error_unexpected;
            default:
                return R.string.login_error_unexpected;
        }
    }

    public static int getSocketErrorTip(SocketEvent event) {
        switch (event) {
            case CONNECT_MSG_SERVER_FAILED:
                return R.string.connect_msg_server_failed;
            case REQ_MSG_SERVER_ADDRS_FAILED:
                return R.string.req_msg_server_addrs_failed;
            default:
                return R.string.login_error_unexpected;
        }
    }

    /**
     * 登录页面
     *
     * @param ctx 上下文参数
     */
    public static void openLoginActivity(Context ctx) {
        Intent intent = new Intent(ctx, LoginMobileActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ctx.startActivity(intent);

    }

    /**
     * 话题聊天
     *
     * @param context     context
     * @param type        type
     * @param sessionKey  sessionKey
     * @param portrait    portrait
     * @param title       title
     * @param creatorName creatorName
     * @param createTime  createTime
     */
    public static void openTopicMessageActivity(Context context, String type, String sessionKey, String portrait, String title, String creatorName, String createTime) {
//        Intent intent = new Intent(context, TopicMessageActivity.class);
//        intent.putExtra(IntentConstant.TYPE, type);
//        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
//        intent.putExtra(IntentConstant.PORTRAIT, portrait);
//        intent.putExtra(IntentConstant.TITLE, title);
//        intent.putExtra(IntentConstant.CREATOR_NAME, creatorName);
//        intent.putExtra(IntentConstant.CREATOR_TIME, createTime);
//        context.startActivity(intent);
    }


    /**
     * 话题详情
     *
     * @param ctx      上下文参数
     * @param topic_id 话题id
     */
    public static void openTopicDetailsActivity(Context ctx, String topic_id) {
        Intent intent = new Intent(ctx, TopicDetailsActivity.class); //创建剧
        intent.putExtra(IntentConstant.TOPIC_ID, topic_id);
        ctx.startActivity(intent);

    }

    // 话题详情页
    public static void openActivity(Context ctx, Class<?> cls) {
        Intent intent = new Intent(ctx, cls); //
        ctx.startActivity(intent);

    }

    // 跳转到创建剧页面
    public static void openCreateStoryActivity(Context ctx, String ids) {
        Intent intent = new Intent(ctx, CreateStoryActivity.class); //创建剧
        intent.putExtra(IntentConstant.SELECTED_IDS, ids);
        ctx.startActivity(intent);
    }

    // 跳转到创建剧页面
    public static void openCreateStoryActivity_New(Context ctx) {
        Intent intent = new Intent(ctx, CreateStoryFirstActivity.class);
        ctx.startActivity(intent);
    }

    // 跳转到创建家族页面
    public static void openCreateFamilyActivity(Context ctx) {
        Intent intent = new Intent(ctx, CreateFamilyActivity.class); //创建剧
        ctx.startActivity(intent);
    }

    // 跳转到给定的剧我参与的剧群列表的界面
    public static void openStoryGroupSelectActivity(Context ctx, String storyId, boolean IS_ADMIN, String shenHeId, String shenHeTitle) {
        Intent intent = new Intent(ctx, StoryGroupSelectActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.IS_ADMIN, IS_ADMIN);
        intent.putExtra(IntentConstant.ShenHeGroupId, shenHeId);
        intent.putExtra(IntentConstant.ShenHeGroupTitle, shenHeTitle);
        ctx.startActivity(intent);
    }

    // 跳转搜索家族界面
    public static void openFamilySearchActivity(Context ctx) {
        Intent intent = new Intent(ctx, SearchFamilyActivity.class);
        ctx.startActivity(intent);
    }

    // 跳转我的家族界面
    public static void openHisfamilyActivity(Context ctx) {
        Intent intent = new Intent(ctx, Hisfamily.class);
        ctx.startActivity(intent);
    }

    public static void openHisplayActivity(Context ctx) {
        Intent intent = new Intent(ctx, Hisplay.class);
        ctx.startActivity(intent);
    }

    public static void openCopyRightActivity(Context ctx, String storyId, String copyRightUrl) {
        Intent intent = new Intent(ctx, CopyRightActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.CORY_RIGHT_URL, copyRightUrl);
        ctx.startActivity(intent);
    }


    // 跳转到家族聊天页面
    public static void openFamilyChatActivity(Context ctx, String sessionKey) {
        Intent intent = new Intent(ctx, FamilyChatActivity.class);
        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        ctx.startActivity(intent);
    }

    // 跳转到家族资料页面
    public static void openFamilyDetailsActivity(Context ctx, String sessionKey) {
        Intent i = new Intent(ctx, FamilyDetailsActivity.class);
        i.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        i.putExtra(IntentConstant.FAMILY_INFO_START_FROM, "message");
        ctx.startActivity(i);
    }

    // 修改群名片
    public static void openGroupLabelActivity(Context ctx, String storyId) {
        Intent intent = new Intent(ctx, GroupLabelActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        ctx.startActivity(intent);
    }

    // 跳转到好友聊天页面
    public static void openFriendChatActivity(Context ctx, String sessionKey) {
        Intent intent = new Intent(ctx, FriendChatActivity.class);
        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        ctx.startActivity(intent);
    }

    // 跳转到剧聊天页面
    public static void openDramaChatActivity(Context ctx, String sessionKey, String storyId) {
        Intent intent = new Intent(ctx, DramaChatActivity.class);
        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        ctx.startActivity(intent);
    }

    // 跳转群消息设置页面
    public static void openGroupMessageSettingActivity(Context ctx, String sessionKey) {
        Intent intent = new Intent(ctx, GroupMessageSettingActivity.class);
        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        ctx.startActivity(intent);
    }

    // 查看群公告
    public static void openGroupAnnouncementActivity(Context ctx, String groupId) {
        Intent intent = new Intent(ctx, GroupAnnouncementActivity.class);
        intent.putExtra(IntentConstant.GROUP_ID, groupId);
        ctx.startActivity(intent);
    }

    // 编辑群公告
    public static void openAddGroupAnnouncementActivity(Context ctx, String groupId) {
        Intent intent = new Intent(ctx, AddGroupAnnouncementActivity.class);
        intent.putExtra(IntentConstant.GROUP_ID, groupId);
        ctx.startActivity(intent);
    }

    // 阅读权限设置
    public static void openReadRightSettingActivity(Context ctx, String storyId, String permission) {
        Intent intent = new Intent(ctx, ReadRightSettingActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.PERMISSION, permission);
        ((Activity) ctx).startActivityForResult(intent, IntentConstant.ReadRightSettingActivity);
    }

    // 阅读权限设置
    public static void openStoryDetailsActivity(Context ctx, String storyId) {
        Intent intent = new Intent(ctx, StoryDetailsActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        ctx.startActivity(intent);
    }

    // 阅读权限设置
    public static void openZiXiDetailsActivity(Context ctx, String storyId) {
        Intent intent = new Intent(ctx, ZiXiDetailsActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        ctx.startActivity(intent);
    }

    // 阅读权限设置
    public static void openStoryEditActivity(Context context, String storyId, String creatorId, String relation, String permission) {
        Intent intent = new Intent(context, StoryEditActivity.class);
        intent.putExtra(IntentConstant.CREATOR_ID, creatorId);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.RELATION, relation);
        intent.putExtra(IntentConstant.PERMISSION, permission);
        context.startActivity(intent);

    }


    //跳转搜索好友界面
    public static void openFriendSearchActivity(Context ctx) {
        Intent intent = new Intent(ctx, FriendSearchActivity.class);
        ctx.startActivity(intent);
    }


    //跳转到家族资料页面
    public static void openFamilyDataActivity(Context ctx, IntentConstant.FamilyDataActivityType type, String familyid) {
        Intent intent = new Intent(ctx, FamilyDataActivity.class);
        intent.putExtra(IntentConstant.FamilyDataActivity_Type, type);
        intent.putExtra(IntentConstant.FAMILY_ID, familyid);
        ctx.startActivity(intent);
    }


    //跳转到修改家族资料页面：修改我的昵称或者家族昵称
    public static void openModifyFamilyPersonInfoActivity(Context ctx, IntentConstant.ModifyFamilyPersonInfoActivityTYPE type, String familyid) {
        Intent intent = new Intent(ctx, ModifyFamilyPersonInfoActivity.class);
        intent.putExtra(IntentConstant.ModifyFamilyPersonInfoActivity_TYPE, type);
        intent.putExtra(IntentConstant.FAMILY_ID, familyid);
        ctx.startActivity(intent);
    }


    public static void openGroupMemberSelectActivity(Context ctx, String sessionKey) {
        Intent intent = new Intent(ctx, GroupMemberSelectActivity.class);
        intent.putExtra(IntentConstant.SESSION_KEY, sessionKey);
        ctx.startActivity(intent);
    }

    public static void openGroupMemberInviteActivity(Context ctx, String groupid) {
        Intent intent = new Intent(ctx, GroupMemberInviteActivity.class);
        intent.putExtra("groupid", groupid);
        ctx.startActivity(intent);
    }

    public static void openInvitedGroupMembersActivity(Context ctx, String groupid, String storyid, String shenheId) {
        Intent intent = new Intent(ctx, InvitedGroupMembersActivity.class);
        intent.putExtra("groupid", groupid);
        intent.putExtra("storyid", storyid);
        intent.putExtra("shenheId", shenheId);
        ctx.startActivity(intent);
    }


    /**
     * 创建自戏
     *
     * @param context 上下文参数
     */
    public static void openCreateZiXiActivity(Context context) {
        Intent intent = new Intent(context, CreateZiXiActivity.class);
        context.startActivity(intent);
    }

    /**
     * 选择要创建剧的类别
     *
     * @param context 上下文参数
     */
    public static void openSelectStoryClassActivity(Context context) {
        Intent intent = new Intent(context, SelectStoryClassActivity.class);
        context.startActivity(intent);
    }


    /**
     * 创建话题
     *
     * @param context 上下文参数
     */
    public static void openTopicActivity(Context context) {
//        Intent intent = new Intent(context, CreateTopicActivity.class);
        Intent intent = new Intent(context, ReleaseTopicActivity.class);
        context.startActivity(intent);
    }


    public static void openZiXiModifyActivityForResult(Context context, String storyid, BookDetailResult.DetatilsEntity entity) {
        Intent intent = new Intent(context, ModifyMyStoryActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyid);
        intent.putExtra(IntentConstant.HISBOOK_ENTITY, entity);
        ((Activity) context).startActivityForResult(intent, 1);

    }


    public static void openReportActivity(Context context, String storyId, String type) {
        Intent intent = new Intent(context, ReportActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.REPORT_TYPE, type);
        context.startActivity(intent);
    }

    /**
     * 系统消息
     */
    public static void openSystemMessageActivity(Context context) {
        Intent intent = new Intent(context, SystemMessageActivity.class);
        context.startActivity(intent);
    }


    /**
     * 评论列表界面
     */
    public static void openFFMessageCommentActivity(Context context) {
        Intent intent = new Intent(context, FFMessageCommentActivity.class);
        context.startActivity(intent);
    }


    /**
     * 举报
     */
    public static void openReportPersonActivity(Context context, String personId) {
        Intent intent = new Intent(context, ReportActivity.class);
        intent.putExtra(IntentConstant.REPORT_TYPE, IntentConstant.PERSION_ID);
        intent.putExtra(IntentConstant.PERSION_ID, personId);
        context.startActivity(intent);
    }

    /**
     * 修改好友昵称
     */
    public static void openEditRemarkNameActivity(Context context, String uid, String remark_name) {
        Intent intent = new Intent(context, EditRemarkNameActivity.class);
        intent.putExtra(IntentConstant.UID, uid);
        intent.putExtra(IntentConstant.REMARK_NAME, remark_name);
        ((Activity) context).startActivityForResult(intent, SysConstant.ACTIVITY_EDIT_REMARK_NAME);
    }

    /******
     * 修改好友设置界面
     *********/
    public static void openFriendSettingActivity(Context context, String personId) {
        Intent intent = new Intent(context, FriendSettingActivity.class);
        intent.putExtra(IntentConstant.PERSION_ID, personId);
        ((Activity) context).startActivityForResult(intent, SysConstant.ACTIVITY_FRIEND_SETTING);
    }

    /***************
     * 修改好友的分组
     *****************/
    public static void openEditFriendGroupActivity(Context context, String uid, String groupId) {
        Intent intent = new Intent(context, EditFriendGroupActivity.class);
        intent.putExtra(IntentConstant.GROUP_ID, groupId);
        intent.putExtra(IntentConstant.UID, uid);
        ((Activity) context).startActivityForResult(intent, SysConstant.ACTIVITY_FRIEND_MODIFY_GROUP);
    }

    /***************
     * 好友分组管理
     *****************/
    public static void openFriendGroupManagerActivity(Context context) {
        Intent intent = new Intent(context, FriendGroupManagerActivity.class);
        context.startActivity(intent);
    }

    /******************
     * 查看他人家族
     *********************/
    public static void openHisFamilyEActivity(Context context, String fromUser) {
        Intent intent = new Intent(context, HisFamilyE.class);
        intent.putExtra(IntentConstant.FROMUSER, fromUser);
        context.startActivity(intent);
    }

    /**
     * 添加或者修改套界面
     *
     * @param context
     * @param title
     * @param content
     * @param type    TYPE_EDIT  TYPE_Add
     */
    public static void openAddOrEditDoActivity(Context context, String title, String content, String type) {
        Intent intent = new Intent(context, AddOrEditDoActivity.class);
        intent.putExtra(IntentConstant.TITLE, title);
        intent.putExtra(IntentConstant.CONTENT, content);
        intent.putExtra(IntentConstant.TYPE, type);
        context.startActivity(intent);
    }

    /******************
     * 打开喜欢或收藏界面
     *******************/
    public static void openFavorOrCollectActivity(Context context, String type, String fromUser) {
        Intent intent = new Intent(context, FavorOrCollectActivity.class);
        intent.putExtra(IntentConstant.TYPE, type);
        intent.putExtra(IntentConstant.FROMUSER, fromUser);
//        intent.putExtra(IntentConstant.LIST, (Serializable) list);
        context.startActivity(intent);
    }

    /*****************
     * 跳转剧详情界面
     *******************/
    public static void openStoryDetailActivity(Context context, String storyid) {
        Intent intent = new Intent(context, StoryDetailsActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyid);
        context.startActivity(intent);
    }


    /*********************
     * 跳转自戏界面
     *********************/
    public static void openHisBookActivity(Context context, String storyid, String creatorid) {
        Intent intent = new Intent(context, ZiXiDetailsActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyid);
        intent.putExtra(IntentConstant.CREATOR_ID, creatorid);
        context.startActivity(intent);
    }

    /****************
     * 跳转话题界面
     *******************/
    public static void openTopicContentActivity(Context context, String topicid) {
        Intent intent = new Intent(context, TopicDetailsActivity.class);
        intent.putExtra(IntentConstant.TOPIC_ID, topicid);
        context.startActivity(intent);
    }

    /********************
     * 跳转他人主页
     *********************/
    public static void openUserHomePageActivity(Context context, String personId) {
        Intent intent = new Intent(context, UserHomePageActivity.class);
        intent.putExtra(IntentConstant.PERSION_ID, personId);
        context.startActivity(intent);
    }

    /**
     * 群成员的人设界面
     */
    public static void openHisGroupCardActivity(Context context, String uid, String storyId, String groupId, String portrait) {
        String creator_id;
        if(TextUtils.isEmpty(uid) || TextUtils.isEmpty(storyId) || TextUtils.isEmpty(groupId) || IMGroupManager.instance().findGroup(Integer.parseInt(groupId))==null) {
            return;
        } else {
            creator_id = IMGroupManager.instance().findGroup(Integer.parseInt(groupId)).getCreatorId() + "";
        }
        Intent intent = new Intent(context, GroupCardActivity.class);
        intent.putExtra(IntentConstant.UID, uid);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        intent.putExtra(IntentConstant.PORTRAIT, portrait);
        intent.putExtra(IntentConstant.GROUP_ID, groupId);
        intent.putExtra(IntentConstant.CREATOR_ID, creator_id);
        context.startActivity(intent);
    }

    /**
     * 剧群详情
     */
    public static void openStoryGroupDetailsActivity(Context context, String storyId, String groupId, boolean isFromChat) {
        if (TextUtils.isEmpty(groupId)) {
            return;
        }
        Intent i = new Intent(context, StoryGroupDetailsActivity.class);
        i.putExtra(IntentConstant.STORY_ID, storyId);
        i.putExtra(IntentConstant.GROUP_ID, groupId);
        i.putExtra(IntentConstant.IS_FROM_CHAT, isFromChat);
        context.startActivity(i);
    }


    /**
     * 人设模版
     */
    public static void openStoryPersonSettingModuleActivity(Context context, String storyId) {
        Intent intent = new Intent(context, StoryPersonSettingModuleActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyId);
        context.startActivity(intent);
    }


    /**
     * 跳转浏览图片界面
     */
    public static void OpenPreviewReadImaActivity(Context context, ArrayList<ImageMessage> list, String position) {
        Intent intent = new Intent(context, PreviewReadImgActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(IntentConstant.CUR_MESSAGE, list);
        bundle.putSerializable(IntentConstant.POSITION, position);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }


    /**
     * 跳转 楼层界面
     *
     * @param context
     * @param title   标题
     * @param passId  传入的id　分别为 0 剧或自戏  1 话题
     * @param type
     */
    public static void openSquareCommentActivity(Context context, String title, String passId, String type) {
        Intent intent = new Intent(context, SquareCommentActivity.class);
        intent.putExtra(IntentConstant.FLOOR_TITLE, title);
        SquareCommentManager.getInstance().setType(type);
        if (type.equals(IntentConstant.FLOOR_TYPE_STORY)) {
            intent.putExtra(IntentConstant.FLOOR_STORYID, passId);
            SquareCommentManager.getInstance().setStoryId(passId);
        } else {
            intent.putExtra(IntentConstant.FLOOR_TOPICID, passId);
            SquareCommentManager.getInstance().setTopicId(passId);
        }
        context.startActivity(intent);
    }


    public static void openResponseFloorActivity(Context context, String floorNum, String userName, String userId, String commentid, String childCommentId) {
        Intent intent = new Intent(context, ResponseFloorActivity.class);
        intent.putExtra(IntentConstant.FLOOR_NUMBER, floorNum);
        intent.putExtra(IntentConstant.FLOOR_USERNAME, userName);
        intent.putExtra(IntentConstant.FlOOR_USERID, userId);
        intent.putExtra(IntentConstant.FLOOR_COMMENTID, commentid);
        intent.putExtra(IntentConstant.FLOOR_CHILD_TOPICID, childCommentId);
        context.startActivity(intent);
    }

    /**
     * @param context
     * @param reportId
     * @param type     0表示 剧或自戏 或 话题  1 表示 评论
     */
    public static void openReportCommentActivty(Context context, String reportId, String type) {
        Intent intent = new Intent(context, ReportCommentActivity.class);
        intent.putExtra(IntentConstant.FlOOR_REPORTID, reportId);
//        if(type.equals("0")){
//            intent.putExtra(IntentConstant.FLOOR_REPORTTYPE,IntentConstant.FLOOR_REPORTTYPE_TOPIC);
//        }else{
        intent.putExtra(IntentConstant.FLOOR_REPORTTYPE, type);
//        }
        context.startActivity(intent);
    }

    public static void openRoleAuditActivity(Context context, String applyid) {
        Intent intent = new Intent(context, RoleAuditActivity.class);
        intent.putExtra(IntentConstant.AUDIT_ROLE_APPLY_ID, applyid);
        context.startActivity(intent);
    }


    public static void openRoleApplyActivity(Context context, String storyid) {
        Intent intent = new Intent(context, ApplyRoleActivity.class);
        intent.putExtra(IntentConstant.STORY_ID, storyid);
        context.startActivity(intent);
    }


    public static void openMyRoleAuditActivity(Context context) {
        Intent intent = new Intent(context, RoleApplyListActivity.class);
        context.startActivity(intent);
    }

    public static void openApplyRoleAdminActivity(Context context,String storyid){
        Intent intent = new Intent(context, ApplyRoleAdminActivity.class);
        intent.putExtra(IntentConstant.STORY_ID,storyid);
        context.startActivity(intent);
    }

    /**
     *  创建剧流程
     * @param context
     * @param ordernum  启动的第几个activity
     * @param openType  打开类型 这是 默认的进入动画 或是 左进右出的回归动画 null 为默认
     */
    public static void openCreateStoryOrderActivity(Context context,int ordernum,String openType){
        Intent intent;
        if(ordernum == 1){
            intent = new Intent(context, CreateStoryFirstActivity.class);
        }else if(ordernum == 2){
            intent = new Intent(context, CreateStorySecondActivity.class);
        }else if(ordernum == 3){
            intent = new Intent(context, CreateStoryThirdActivity.class);
        }else if(ordernum == 4){
            intent = new Intent(context, CreateStoryForthActivity.class);
        }else if(ordernum == 5){
            intent = new Intent(context, CreateStoryFifthActivity.class);
        }else{
            intent = new Intent(context, CreateStorySixthActivity.class);
        }
            context.startActivity(intent);
        if(openType!=null&&openType.equals(IntentConstant.ACTIVITY_OPEN_TYPE_LEFT)){
            ((Activity)context).overridePendingTransition(R.anim.slide_in_left_all,R.anim.slide_out_right);
        }



    }





    //
    public static void openSplashActivity(Context ctx) {
        Intent intent = new Intent(ctx, SplashActivity.class);
        ctx.startActivity(intent);
    }

    // 对话框回调函数
    public interface dialogCallback {
        void callback();
    }


    // 文字高亮显示
    public static void setTextHilighted(TextView textView, String text, SearchElement searchElement) {
        textView.setText(text);
        if (textView == null
                || TextUtils.isEmpty(text)
                || searchElement == null) {
            return;
        }

        int startIndex = searchElement.startIndex;
        int endIndex = searchElement.endIndex;
        if (startIndex < 0 || endIndex > text.length()) {
            return;
        }
        // 开始高亮处理
        int color = Color.rgb(69, 192, 26);
        textView.setText(text, BufferType.SPANNABLE);
        Spannable span = (Spannable) textView.getText();
        span.setSpan(new ForegroundColorSpan(color), startIndex, endIndex, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }


    /**
     * 如果图片路径是以  http开头,直接返回
     * 如果不是， 需要集合自己的图像路径生成规律
     *
     * @param avatarUrl
     * @return
     */
    public static String getRealAvatarUrl(String avatarUrl) {
        if (avatarUrl.toLowerCase().contains("http")) {
            return avatarUrl;
        } else if (avatarUrl.trim().isEmpty()) {
            return "";
        } else {
            return UrlConstant.AVATAR_URL_PREFIX + avatarUrl;
        }
    }


    // search helper start
    public static boolean handleDepartmentSearch(String key, DepartmentEntity department) {
        if (TextUtils.isEmpty(key) || department == null) {
            return false;
        }
        department.getSearchElement().reset();

        return handleTokenFirstCharsSearch(key, department.getPinyinElement(), department.getSearchElement())
                || handleTokenPinyinFullSearch(key, department.getPinyinElement(), department.getSearchElement())
                || handleNameSearch(department.getDepartName(), key, department.getSearchElement());
    }


    public static boolean handleGroupSearch(String key, GroupEntity group) {
        if (TextUtils.isEmpty(key) || group == null) {
            return false;
        }
        group.getSearchElement().reset();

        return handleTokenFirstCharsSearch(key, group.getPinyinElement(), group.getSearchElement())
                || handleTokenPinyinFullSearch(key, group.getPinyinElement(), group.getSearchElement())
                || handleNameSearch(group.getMainName(), key, group.getSearchElement());
    }

    public static boolean handleContactSearch(String key, UserEntity contact) {
        if (TextUtils.isEmpty(key) || contact == null) {
            return false;
        }

        contact.getSearchElement().reset();

        return handleTokenFirstCharsSearch(key, contact.getPinyinElement(), contact.getSearchElement())
                || handleTokenPinyinFullSearch(key, contact.getPinyinElement(), contact.getSearchElement())
                || handleNameSearch(contact.getMainName(), key, contact.getSearchElement());
        // 原先是 contact.name 代表花名的意思嘛??
    }

    public static boolean handleNameSearch(String name, String key,
                                           SearchElement searchElement) {
        int index = name.indexOf(key);
        if (index == -1) {
            return false;
        }

        searchElement.startIndex = index;
        searchElement.endIndex = index + key.length();

        return true;
    }

    public static boolean handleTokenFirstCharsSearch(String key, PinYinElement pinYinElement, SearchElement searchElement) {
        return handleNameSearch(pinYinElement.tokenFirstChars, key.toUpperCase(), searchElement);
    }

    public static boolean handleTokenPinyinFullSearch(String key, PinYinElement pinYinElement, SearchElement searchElement) {
        if (TextUtils.isEmpty(key)) {
            return false;
        }

        String searchKey = key.toUpperCase();

        //onLoginOut the old search result
        searchElement.reset();

        int tokenCnt = pinYinElement.tokenPinyinList.size();
        int startIndex = -1;
        int endIndex = -1;

        for (int i = 0; i < tokenCnt; ++i) {
            String tokenPinyin = pinYinElement.tokenPinyinList.get(i);

            int tokenPinyinSize = tokenPinyin.length();
            int searchKeySize = searchKey.length();

            int keyCnt = Math.min(searchKeySize, tokenPinyinSize);
            String keyPart = searchKey.substring(0, keyCnt);

            if (tokenPinyin.startsWith(keyPart)) {

                if (startIndex == -1) {
                    startIndex = i;
                }

                endIndex = i + 1;
            } else {
                continue;
            }

            if (searchKeySize <= tokenPinyinSize) {
                searchKey = "";
                break;
            }

            searchKey = searchKey.substring(keyCnt, searchKeySize);
        }

        if (!searchKey.isEmpty()) {
            return false;
        }

        if (startIndex >= 0 && endIndex > 0) {
            searchElement.startIndex = startIndex;
            searchElement.endIndex = endIndex;

            return true;
        }

        return false;
    }

    // search helper end


    // 这个还是蛮有用的,方便以后的替换
    public static int getDefaultAvatarResId(int sessionType) {
        if (sessionType == DBConstant.SESSION_TYPE_SINGLE) {
            return R.drawable.friend_headimg;
        } else if (sessionType == DBConstant.SESSION_TYPE_GROUP) {
            return R.drawable.head_img;
        } else if (sessionType == DBConstant.SESSION_TYPE_GROUP) {
            return R.drawable.discussion_group_default;
        }

        return R.drawable.friend_headimg;
    }


    public static void displayImage(ImageView imageView,
                                    String resourceUri, int defaultResId, int roundPixel) {

        Logger logger = Logger.getLogger(IMUIHelper.class);

        logger.d("displayimage#displayImage resourceUri:%s, defeaultResourceId:%d", resourceUri, defaultResId);

        if (resourceUri == null) {
            resourceUri = "";
        }

        boolean showDefaultImage = !(defaultResId <= 0);

        if (TextUtils.isEmpty(resourceUri) && !showDefaultImage) {
            logger.e("displayimage#, unable to display image");
            return;
        }


        DisplayImageOptions options;
        if (showDefaultImage) {
            options = new DisplayImageOptions.Builder().
                    showImageOnLoading(defaultResId).
                    showImageForEmptyUri(defaultResId).
                    showImageOnFail(defaultResId).
                    cacheInMemory(true).
                    cacheOnDisk(true).
                    considerExifParams(true).
                    displayer(new RoundedBitmapDisplayer(roundPixel)).
                    imageScaleType(ImageScaleType.EXACTLY).// 改善OOM
                    bitmapConfig(Bitmap.Config.RGB_565).// 改善OOM
                    build();
        } else {
            options = new DisplayImageOptions.Builder().
                    cacheInMemory(true).
                    cacheOnDisk(true).
//			considerExifParams(true).
//			displayer(new RoundedBitmapDisplayer(roundPixel)).
//			imageScaleType(ImageScaleType.EXACTLY).// 改善OOM
//			bitmapConfig(Bitmap.Config.RGB_565).// 改善OOM
        build();
        }

        ImageLoader.getInstance().displayImage(resourceUri, imageView, options, null);
    }


}
