package com.yuxip.utils;

import android.content.Context;
import android.text.TextUtils;

import com.squareup.okhttp.Request;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.http.OkHttpClientManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by luoyan on 2015/8/4.
 * description:
 */
public class FirstPostUtils {

    /**
     * @param context
     * @param uid     登录uid
     * @param type    类型 0：首次启动  1：首次登录
     */
    public static void sendHttpRequest(final Context context, String uid, final int type) {
        if (DeviceUtils.getDeviceId(context) == null)
            return;
        if (TextUtils.isEmpty(CommonUtil.getProducer(context)))
            return;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("device", DeviceUtils.getDeviceId(context));
        params.addParams("producer", CommonUtil.getProducer(context));
        OkHttpClientManager.postAsy(ConstantValues.SUBMITDEICEINFO, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            String result = object.optString("result", "-1");
                            if (result.equals("1")) {
                                if (type == 0) {
                                    SharedPreferenceUtils.saveBooleanDate(context, ConstantValues.FIRST_DOWNLOAD, false);
                                } else {
                                    SharedPreferenceUtils.saveBooleanDate(context, ConstantValues.FIRST_LOGIN, false);
                                }

                            }
                        } catch (JSONException e) {
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {

                    }
                }
        );
    }
}
