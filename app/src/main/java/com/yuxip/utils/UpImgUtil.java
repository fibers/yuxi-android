package com.yuxip.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.yuxip.upyun.block.api.listener.CompleteListener;
import com.yuxip.upyun.block.api.listener.ProgressListener;
import com.yuxip.upyun.block.api.main.UploaderManager;
import com.yuxip.upyun.block.api.utils.UpYunUtils;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Map;

/**
 * 上传下载头像的工具类
 * Created by SummerRC on 2015/5/7.
 */
public class UpImgUtil {

    String bucket = "cosimage";                                 // 空间名
    String formApiSecret = "r5byGWJvozlUIWedAIwiqlVRmOQ=";      // 表单密钥
    private File localFile;
    private String localFilePath;
    private String upyunSavePath;
    private HeadImgListener mHeadImgListener;

    /**
     * 获得头像
     * @return  Bitmap
     */
    public Bitmap getHeadImg() {
        return BitmapFactory.decodeFile(localFilePath);
    }

    /**
     * 设置图片本地保存路径和又拍云存储路径
     * @param localPicName  localPicName
     * @param upPicName     upPicName
     */
    public void setFilePath(String localPicName, String upPicName) {
        localFilePath = Environment.getExternalStorageDirectory() + "/youxi/" + localPicName;
        upyunSavePath = "/youxi/" + upPicName;
    }

    /**
     * 保存头像图片到指定路径
     *
     * @param bitmap    bitmap
     */
    public void saveHeadImg(Bitmap bitmap) {
        File file = new File(Environment.getExternalStorageDirectory() + "/youxi/");
        if (!file.exists()) {
            file.mkdirs();
        }
        localFile = new File(localFilePath);
        FileOutputStream fos;
        try {
            fos = new FileOutputStream(localFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 75, fos);               // (0 - 100)压缩文件
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public class UploadTask extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                /*
				 * 设置进度条回掉函数
				 * 注意：由于在计算发送的字节数中包含了图片以外的其他信息，最终上传的大小总是大于图片实际大小，
				 * 为了解决这个问题，代码会判断如果实际传送的大小大于图片
				 * ，就将实际传送的大小设置成'fileSize-1000'（最小为0）
				 */
                ProgressListener progressListener = new ProgressListener() {
                    @Override
                    public void transferred(long transferedBytes, long totalBytes) {
                        // do something...
                        System.out.println("trans:" + transferedBytes + "; total:" + totalBytes);
                    }
                };

                CompleteListener completeListener = new CompleteListener() {
                    @Override
                    public void result(boolean isComplete, String result, String error) {
                        // do something...
                        try {
                            Log.d("completeListener", "isComplete:" + isComplete + ";result:" + result + ";error:" + error);
                            if (result == null) {
                                mHeadImgListener.notifyImgUploadFinished(result);
                                return;
                            }
                            JSONObject json = new JSONObject(result);
                            JSONObject args = json.getJSONObject("args");
                            String path = args.getString("path");
                            path = "http://cosimage.b0.upaiyun.com" + path;
                            Log.d("complateListener", "path:" + path);
                            mHeadImgListener.notifyImgUploadFinished(path);
                        } catch (Exception e) {
                            e.printStackTrace();
                            mHeadImgListener.notifyImgUploadFinished(null);
                        }

                    }
                };

                UploaderManager uploaderManager = UploaderManager.getInstance(bucket);
                uploaderManager.setConnectTimeout(5);
                uploaderManager.setResponseTimeout(5);
                Map<String, Object> paramsMap = uploaderManager.fetchFileInfoDictionaryWith(localFile, upyunSavePath);       //本地文件路径和存储在又拍云的文件路径
                //还可以加上其他的额外处理参数...
                paramsMap.put("return_url", "http://httpbin.org/get");
                // signature & policy 建议从服务端获取
                String policyForInitial = UpYunUtils.getPolicy(paramsMap);
                String signatureForInitial = UpYunUtils.getSignature(paramsMap, formApiSecret);
                uploaderManager.upload(policyForInitial, signatureForInitial, localFile, progressListener, completeListener);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            return "http://cosimage.b0.upaiyun.com" + upyunSavePath;
        }

        /**
         * @param result : 图片存储的路径
         */
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
        }
    }

    public void upLoadPicture(HeadImgListener headImgListener) {
        this.mHeadImgListener = headImgListener;
        new UploadTask().execute();
    }

    public void upLoadPictureWithPath(HeadImgListener headImgListener, String path, String upyunSavePath) {
        this.upyunSavePath = upyunSavePath;
        this.localFile = new File(path);
        this.mHeadImgListener = headImgListener;
        new UploadTask().execute();
    }
}
