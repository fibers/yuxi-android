package com.yuxip.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.imservice.service.IMService;

/**
 * Created by SummerRC on 2015/8/27.
 * description : 与Dialog相关
 */
public class DialogHelper {

    /**
     * 退出的提示框
     *
     * @param context 上下文
     */
    public static void showExitDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("提示");
        builder.setMessage("要退出当前页面吗?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ((Activity) context).finish();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    /**
     * 提示框
     *
     * @param context 上下文
     */
    public static void showTipDialog(final Context context, String content) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("提示");
        builder.setMessage(content);
        builder.setPositiveButton("我知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    /**
     * 添加好友输入验证信息的弹出框
     *
     * @param context   上下文
     * @param imService imService
     * @param uid       被申请者id
     */
    public static void showAddFriendDialog(final Context context, final IMService imService, final int uid) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_friend, null);
        final Dialog dialog = new Dialog(context, R.style.dialog_theme_transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setContentView(view);

        final AppCompatEditText dialog_message = (AppCompatEditText) view.findViewById(R.id.dialog_message);
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.dialog_cancel:
                        dialog.dismiss();
                        break;
                    case R.id.dialog_confirm:
                        T.show(context, "申请成功", 0);
                        dialog.dismiss();
                        imService.getMessageManager().sendAddFriendReq(uid, dialog_message.getText().toString().trim());
                        break;
                }
            }
        };
        view.findViewById(R.id.dialog_confirm).setOnClickListener(myListener);
        view.findViewById(R.id.dialog_cancel).setOnClickListener(myListener);
    }

    /**
     * 添加好友输入验证信息的弹出框
     *
     * @param context   上下文
     * @param imService imService
     * @param uid       被申请者id
     */
    public static void showAddFriendDialog(final Context context, final IMService imService, final int uid,DialogListener dialogListener) {
        final DialogListener dialogCallBack = dialogListener;
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_friend, null);
        final Dialog dialog = new Dialog(context, R.style.dialog_theme_transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setContentView(view);

        final AppCompatEditText dialog_message = (AppCompatEditText) view.findViewById(R.id.dialog_message);
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.dialog_cancel:
                        dialog.dismiss();
                        break;
                    case R.id.dialog_confirm:
                        T.show(context, "申请成功", 0);
                        dialog.dismiss();
                        imService.getMessageManager().sendAddFriendReq(uid, dialog_message.getText().toString().trim());
                        if(dialogCallBack!=null){
                            dialogCallBack.addNewItem(null);
                        }
                        break;
                }
            }
        };
        view.findViewById(R.id.dialog_confirm).setOnClickListener(myListener);
        view.findViewById(R.id.dialog_cancel).setOnClickListener(myListener);
    }

    /**
     * 添加分组的弹出框
     */
    public static void showAddItemDialog(Context context, String title, String secondTitle, String editHint, final int inputLimit, DialogListener dialogListener) {
        final DialogListener dialogCallback = dialogListener;
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_add_group, null);
        final Dialog dialog = new Dialog(context, R.style.dialog_theme_transparent);
        final EditText etContent = (EditText) view.findViewById(R.id.edit_content);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        TextView tvSecondTitle = (TextView) view.findViewById(R.id.tv_hint);
        tvTitle.setText(title);
        tvSecondTitle.setText(secondTitle);
        etContent.setHint(editHint);
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tv_cancel:
                        dialog.dismiss();
                        break;
                    case R.id.tv_confirm:
                        if (dialogCallback != null && !TextUtils.isEmpty(etContent.getText().toString().trim())) {
                            dialogCallback.addNewItem(etContent.getText().toString().trim());
                        }
                        dialog.dismiss();
                        break;
                }
            }
        };
        view.findViewById(R.id.tv_confirm).setOnClickListener(myListener);
        view.findViewById(R.id.tv_cancel).setOnClickListener(myListener);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s.toString())) {
                    if (s.toString().length() > inputLimit) {
                        etContent.setText(s.toString().substring(0, inputLimit));
                        etContent.setSelection(inputLimit);
                    }
                }
            }
        });

        etContent.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return keyCode == KeyEvent.KEYCODE_ENTER;
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setContentView(view);
    }


    public interface DialogListener {
        void addNewItem(String dialogContent);
    }


}
