package com.yuxip.thread;

import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;

/**
 * created By SummerRC  2015/8/23
 * description:AsyncTask的基类
 */
public class AsyncTaskBase extends AsyncTask<String, Integer, Integer> {
    private ProgressBar progressBar;

    public AsyncTaskBase(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public AsyncTaskBase() {

    }

    /**
     * doInBackground()方法里不允许操作UI
     * @param params    传入的数据
     * @return          返回的执行结果
     */
    @Override
    protected Integer doInBackground(String... params) {
        return null;
    }

    /**
     * onPostExecute()方法在任务执行结束后被调用
     * @param result    任务执行的结果
     */
    @Override
    protected void onPostExecute(Integer result) {
        super.onPostExecute(result);
        if(progressBar != null) {
            if (result == 1) {
                progressBar.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * onPreExecute方法在开始执行任务之前设置UI
     */
    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(progressBar != null) {
            progressBar.setVisibility(View.VISIBLE);
        }

    }


    /**
     * onCancelled方法用于在取消执行中的任务时更改UI
     */
    @Override
    protected void onCancelled() {
        if(progressBar != null) {
            progressBar.setVisibility(View.GONE);
        }
    }
}
