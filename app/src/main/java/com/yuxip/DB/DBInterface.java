package com.yuxip.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.yuxip.DB.dao.ApplyFriendDao;
import com.yuxip.DB.dao.ApplyGroupDao;
import com.yuxip.DB.dao.DaoMaster;
import com.yuxip.DB.dao.DaoSession;
import com.yuxip.DB.dao.DepartmentDao;
import com.yuxip.DB.dao.ExpressionDao;
import com.yuxip.DB.dao.GroupAnnouncementDao;
import com.yuxip.DB.dao.GroupDao;
import com.yuxip.DB.dao.LastChatDao;
import com.yuxip.DB.dao.MessageDao;
import com.yuxip.DB.dao.NodesDao;
import com.yuxip.DB.dao.SessionDao;
import com.yuxip.DB.dao.StoryBookMarkDao;
import com.yuxip.DB.dao.UserDao;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.DB.entity.DepartmentEntity;
import com.yuxip.DB.entity.ExpressionEntity;
import com.yuxip.DB.entity.GroupAnnouncementEntity;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.LastChatEntity;
import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.DB.entity.SessionEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.config.DBConstant;
import com.yuxip.config.MessageConstant;
import com.yuxip.imservice.entity.AudioMessage;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.entity.MixMessage;
import com.yuxip.imservice.entity.TextMessage;
import com.yuxip.utils.Logger;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import de.greenrobot.dao.query.DeleteQuery;
import de.greenrobot.dao.query.Query;
import de.greenrobot.dao.query.QueryBuilder;

/**
 * @author : yingmu on 15-1-5.
 *         email : yingmu@im.com.
 *         <p/>
 *         有两个静态标识可开启QueryBuilder的SQL和参数的日志输出：
 *         QueryBuilder.LOG_SQL = true;
 *         QueryBuilder.LOG_VALUES = true;
 */
public class DBInterface {
    private Logger logger = Logger.getLogger(DBInterface.class);
    private static DBInterface dbInterface = null;
    private DaoMaster.DevOpenHelper openHelper;
    private Context context = null;
    private int loginUserId = 0;

    public static DBInterface instance() {
        if (dbInterface == null) {
            synchronized (DBInterface.class) {
                if (dbInterface == null) {
                    dbInterface = new DBInterface();
                }
            }
        }
        return dbInterface;
    }

    private DBInterface() {
    }

    /**
     * 上下文环境的更新
     * 1. 环境变量的clear
     * check
     */
    public void close() {
        if (openHelper != null) {
            openHelper.close();
            openHelper = null;
            context = null;
            loginUserId = 0;
        }
    }


    public void initDbHelp(Context ctx, int loginId) {
        if (ctx == null || loginId <= 0) {
            throw new RuntimeException("#DBInterface# init DB exception!");
        }
        // 临时处理，为了解决离线登陆db实例初始化的过程
        if (context != ctx || loginUserId != loginId) {
            context = ctx;
            loginUserId = loginId;
            close();
            logger.i("DB init,loginId:%d", loginId);
            String DBName = "tt_" + loginId + ".db";
            openHelper = new DaoMaster.DevOpenHelper(ctx, DBName, null);
        }
    }

    /**
     * Query for readable DB
     */
    private DaoSession openReadableDb() {
        isInitOk();
        SQLiteDatabase db = openHelper.getReadableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }

    /**
     * Query for writable DB
     */
    private DaoSession openWritableDb() {
        isInitOk();
        SQLiteDatabase db = openHelper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);
        return daoMaster.newSession();
    }


    private void isInitOk() {
        if (openHelper == null) {
            logger.e("DBInterface#isInit not success or start,cause by openHelper is null");
            // 抛出异常 todo
            throw new RuntimeException("DBInterface#isInit not success or start,cause by openHelper is null");
        }
    }


    /**
     * -------------------------下面开始department 操作相关---------------------------------------
     */
    public void batchInsertOrUpdateDepart(List<DepartmentEntity> entityList) {
        if (entityList.size() <= 0) {
            return;
        }
        DepartmentDao dao = openWritableDb().getDepartmentDao();
        dao.insertOrReplaceInTx(entityList);
    }

    /**
     * update
     */
    public int getDeptLastTime() {
        DepartmentDao dao = openReadableDb().getDepartmentDao();
        DepartmentEntity entity = dao.queryBuilder()
                .orderDesc(DepartmentDao.Properties.Updated)
                .limit(1)
                .unique();
        if (entity == null) {
            return 0;
        } else {
            return entity.getUpdated();
        }
    }

    /**
     * 部门被删除的情况
     *
     * @return List<DepartmentEntity>
     */
    public List<DepartmentEntity> loadAllDept() {
        DepartmentDao dao = openReadableDb().getDepartmentDao();
        return dao.loadAll();
    }

    /**-------------------------下面开始User 操作相关---------------------------------------*/
    /**
     * @return todo  USER_STATUS_LEAVE
     */
    public List<UserEntity> loadAllUsers() {
        UserDao dao = openReadableDb().getUserDao();
        return dao.loadAll();
    }

    public UserEntity getByUserPinyinName(String pinyinName) {
        UserDao dao = openReadableDb().getUserDao();
        return dao.queryBuilder().where(UserDao.Properties.PinyinName.eq(pinyinName)).unique();
    }

    public UserEntity getByLoginId(int loginId) {
        UserDao dao = openReadableDb().getUserDao();
        return dao.queryBuilder().where(UserDao.Properties.PeerId.eq(loginId)).unique();
    }


    public void insertOrUpdateUser(UserEntity entity) {
        UserDao userDao = openWritableDb().getUserDao();
        long rowId = userDao.insertOrReplace(entity);
    }

    public void batchInsertOrUpdateUser(List<UserEntity> entityList) {
        if (entityList.size() <= 0) {
            return;
        }
        UserDao userDao = openWritableDb().getUserDao();
        userDao.insertOrReplaceInTx(entityList);
    }

    /**
     * update
     */
    public int getUserInfoLastTime() {
        UserDao sessionDao = openReadableDb().getUserDao();
        UserEntity userEntity = sessionDao.queryBuilder()
                .orderDesc(UserDao.Properties.Updated)
                .limit(1)
                .unique();
        if (userEntity == null) {
            return 0;
        } else {
            return userEntity.getUpdated();
        }
    }

    /**-------------------------下面开始Group 操作相关---------------------------------------*/
    /**
     * 载入Group的所有数据
     *
     * @return List<GroupEntity>
     */
    public List<GroupEntity> loadAllGroup() {
        GroupDao dao = openReadableDb().getGroupDao();
        return dao.loadAll();
    }

    public long insertOrUpdateGroup(GroupEntity groupEntity) {
        GroupDao dao = openWritableDb().getGroupDao();
        return dao.insertOrReplace(groupEntity);
    }

    public void batchInsertOrUpdateGroup(List<GroupEntity> entityList) {
        if (entityList.size() <= 0) {
            return;
        }
        GroupDao dao = openWritableDb().getGroupDao();
        dao.insertOrReplaceInTx(entityList);
    }

    /**-------------------------下面开始session 操作相关---------------------------------------*/
    /**
     * 载入session 表中的所有数据
     *
     * @return List<SessionEntity>
     */
    public List<SessionEntity> loadAllSession() {
        SessionDao dao = openReadableDb().getSessionDao();
        return dao.queryBuilder()
                .orderDesc(SessionDao.Properties.Updated)
                .list();
    }

    public long insertOrUpdateSession(SessionEntity sessionEntity) {
        SessionDao dao = openWritableDb().getSessionDao();
        return dao.insertOrReplace(sessionEntity);
    }

    public void batchInsertOrUpdateSession(List<SessionEntity> entityList) {
        if (entityList.size() <= 0) {
            return;
        }
        SessionDao dao = openWritableDb().getSessionDao();
        dao.insertOrReplaceInTx(entityList);
    }

    public void deleteSession(String sessionKey) {
        SessionDao sessionDao = openWritableDb().getSessionDao();
        DeleteQuery<SessionEntity> bd = sessionDao.queryBuilder()
                .where(SessionDao.Properties.SessionKey.eq(sessionKey))
                .buildDelete();

        bd.executeDeleteWithoutDetachingEntities();
    }

    /**
     * 获取最后回话的时间，便于获取联系人列表变化
     * 问题: 本地消息发送失败，依旧会更新session的时间 [存在会话、不存在的会话]
     * 本质上还是最后一条成功消息的时间
     *
     * @return session最后一次更新的时间
     */
    public int getSessionLastTime() {
        int timeLine = 0;
        MessageDao messageDao = openReadableDb().getMessageDao();
        String successType = String.valueOf(MessageConstant.MSG_SUCCESS);
        String sql = "select created from Message where status=? order by created desc limit 1";
        Cursor cursor = messageDao.getDatabase().rawQuery(sql, new String[]{successType});
        try {
            if (cursor != null && cursor.getCount() == 1) {
                cursor.moveToFirst();
                timeLine = cursor.getInt(0);
            }
        } catch (Exception e) {
            logger.e("DBInterface#getSessionLastTime cursor 查询异常");
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return timeLine;
    }

    /**
     * -------------------------下面开始message 操作相关---------------------------------------
     */

    // where (msgId >= startMsgId and msgId<=lastMsgId) or
    // (msgId=0 and status = 0)
    // order by created desc
    // limit count;
    // 按照时间排序
    public List<MessageEntity> getHistoryMsg(String chatKey, int lastMsgId, int lastCreateTime, int count) {
        /**解决消息重复的问题*/
        int preMsgId = lastMsgId + 1;
        MessageDao dao = openReadableDb().getMessageDao();
        List<MessageEntity> listMsg = dao.queryBuilder().where(MessageDao.Properties.Created.le(lastCreateTime)
                , MessageDao.Properties.SessionKey.eq(chatKey)
                , MessageDao.Properties.MsgId.notEq(preMsgId))
                .whereOr(MessageDao.Properties.MsgId.le(lastMsgId),
                        MessageDao.Properties.MsgId.gt(90000000))
                .orderDesc(MessageDao.Properties.Created)
                .orderDesc(MessageDao.Properties.MsgId)
                .limit(count)
                .list();

        return formatMessage(listMsg);
    }

    /**
     * IMGetLatestMsgIdReq 后去最后一条合法的msgId
     */
    public List<Integer> refreshHistoryMsgId(String chatKey, int beginMsgId, int lastMsgId) {
        MessageDao dao = openReadableDb().getMessageDao();

        String sql = "select MSG_ID from Message where SESSION_KEY = ? and MSG_ID >= ? and MSG_ID <= ? order by MSG_ID asc";
        Cursor cursor = dao.getDatabase().rawQuery(sql, new String[]{chatKey, String.valueOf(beginMsgId), String.valueOf(lastMsgId)});

        List<Integer> msgIdList = new ArrayList<>();
        try {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                int msgId = cursor.getInt(0);
                msgIdList.add(msgId);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return msgIdList;
    }


    public int getMaxMsgId(String chatKey) {
        MessageDao dao = openReadableDb().getMessageDao();

        String sql = "select MSG_ID from Message where SESSION_KEY = ? order by MSG_ID DESC";
        Cursor cursor = dao.getDatabase().rawQuery(sql, new String[]{chatKey});

        int msgId = 0;
        try {
            for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
                msgId = cursor.getInt(0);
                break;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return msgId;
    }

    public long insertOrUpdateMix(MessageEntity message) {
        MessageDao dao = openWritableDb().getMessageDao();
        MessageEntity parent = dao.queryBuilder().where(MessageDao.Properties.MsgId.eq(message.getMsgId())
                , MessageDao.Properties.SessionKey.eq(message.getSessionKey())).unique();

        long resId = parent.getId();
        if (parent.getDisplayType() != DBConstant.SHOW_MIX_TEXT) {
            return resId;
        }

        boolean needUpdate = false;
        MixMessage mixParent = (MixMessage) formatMessage(parent);
        List<MessageEntity> msgList = mixParent.getMsgList();
        for (int index = 0; index < msgList.size(); index++) {
            if (msgList.get(index).getId().equals(message.getId())) {
                msgList.set(index, message);
                needUpdate = true;
                break;
            }
        }

        if (needUpdate) {
            mixParent.setMsgList(msgList);
            return dao.insertOrReplace(mixParent);
        }
        return resId;
    }

    /**
     * 有可能是混合消息
     * 批量接口{batchInsertOrUpdateMessage} 没有存在场景
     */
    public long insertOrUpdateMessage(MessageEntity message) {
        if (message.getId() != null && message.getId() < 0) {
            // mix消息
            return insertOrUpdateMix(message);
        }
        MessageDao dao = openWritableDb().getMessageDao();
        return dao.insertOrReplace(message);
    }

    /**
     * todo 这个地方调用存在特殊场景，如果list中包含Id为负Mix子类型，更新就有问题
     * 现在的调用列表没有这个情景，使用的时候注意
     */
    public void batchInsertOrUpdateMessage(List<MessageEntity> entityList) {
        MessageDao dao = openWritableDb().getMessageDao();
        dao.insertOrReplaceInTx(entityList);
    }


    public void deleteMessageById(long localId) {
        if (localId <= 0) {
            return;
        }
        Set<Long> setIds = new TreeSet<>();
        setIds.add(localId);
        batchDeleteMessageById(setIds);
    }

    public void batchDeleteMessageById(Set<Long> pkIds) {
        if (pkIds.size() <= 0) {
            return;
        }
        MessageDao dao = openWritableDb().getMessageDao();
        dao.deleteByKeyInTx(pkIds);
    }

    public void deleteMessageByMsgId(int msgId) {
        if (msgId <= 0) {
            return;
        }
        MessageDao messageDao = openWritableDb().getMessageDao();
        QueryBuilder<MessageEntity> qb = openWritableDb().getMessageDao().queryBuilder();
        DeleteQuery<MessageEntity> bd = qb.where(MessageDao.Properties.MsgId.eq(msgId)).buildDelete();
        bd.executeDeleteWithoutDetachingEntities();
    }

    public MessageEntity getMessageByMsgId(int messageId) {
        MessageDao dao = openReadableDb().getMessageDao();
        Query query = dao.queryBuilder().where(
                MessageDao.Properties.Id.eq(messageId))
                .build();
        return formatMessage((MessageEntity) query.unique());
    }

    /**
     * 根据主键查询
     * not use
     */
    public MessageEntity getMessageById(long localId) {
        MessageDao dao = openReadableDb().getMessageDao();
        MessageEntity messageEntity =
                dao.queryBuilder().where(MessageDao.Properties.Id.eq(localId)).unique();
        return formatMessage(messageEntity);
    }


    private MessageEntity formatMessage(MessageEntity msg) {
        MessageEntity messageEntity = null;
        int displayType = msg.getDisplayType();
        switch (displayType) {
            case DBConstant.SHOW_MIX_TEXT:
                try {
                    messageEntity = MixMessage.parseFromDB(msg);
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
                break;
            case DBConstant.SHOW_AUDIO_TYPE:
                messageEntity = AudioMessage.parseFromDB(msg);
                break;
            case DBConstant.SHOW_IMAGE_TYPE:
                messageEntity = ImageMessage.parseFromDB(msg);
                break;
            case DBConstant.SHOW_ORIGIN_TEXT_TYPE:
                messageEntity = TextMessage.parseFromDB(msg);
                break;
        }
        return messageEntity;
    }


    public List<MessageEntity> formatMessage(List<MessageEntity> msgList) {
        if (msgList.size() <= 0) {
            return Collections.emptyList();
        }
        ArrayList<MessageEntity> newList = new ArrayList<>();
        for (MessageEntity info : msgList) {
            int displayType = info.getDisplayType();
            switch (displayType) {
                case DBConstant.SHOW_MIX_TEXT:
                    try {
                        newList.add(MixMessage.parseFromDB(info));
                    } catch (JSONException e) {
                        logger.e(e.toString());
                    }
                    break;
                case DBConstant.SHOW_AUDIO_TYPE:
                    newList.add(AudioMessage.parseFromDB(info));
                    break;
                case DBConstant.SHOW_IMAGE_TYPE:
                    newList.add(ImageMessage.parseFromDB(info));
                    break;
                case DBConstant.SHOW_ORIGIN_TEXT_TYPE:
                    newList.add(TextMessage.parseFromDB(info));
                    break;
            }
        }
        return newList;
    }


    /**
     * -------------------------下面开始LastChatEntity 操作相关---------------------------------------
     */

    /**
     * 根据currentSessionKey查询
     */
    public LastChatEntity getLastChatEntityByCurrentSessionKey(String currentSessionKey) {
        LastChatDao dao = openReadableDb().getLastChatDao();
        return dao.queryBuilder().
                where(LastChatDao.Properties.CurrentSessionKey.eq(currentSessionKey)).limit(1).unique();
    }

    public long insertOrUpdateLastChatEntity(LastChatEntity lastChatEntity) {
        LastChatDao lastChatDao = openWritableDb().getLastChatDao();
        return lastChatDao.insertOrReplace(lastChatEntity);
    }

    /**
     * -------------------------下面开始GroupAnnouncementEntity 操作相关---------------------------------------
     */

    /**
     * 根据currentSessionKey查询
     */
    public GroupAnnouncementEntity getGroupAnnouncementEntityByGroupId(String groupId) {
        GroupAnnouncementDao dao = openReadableDb().getGroupAnnouncementDao();
        try {
            return dao.queryBuilder().
                    where(GroupAnnouncementDao.Properties.GroupId.eq(groupId)).limit(1).unique();
        } catch (Exception e) {
            logger.e(e.toString());
            return null;
        }

        /*QueryBuilder<GroupAnnouncementEntity> qb = dao.queryBuilder();
        qb.where(GroupAnnouncementDao.Properties.Id.eq(groupId));
        if (qb.list().size() > 0) {
            return qb.list().get(0);
        } else {
            return null;
        }*/
    }

    public long insertOrUpdateGroupAnnouncementEntity(GroupAnnouncementEntity groupAnnouncementEntity) {
        GroupAnnouncementDao lastChatDao = openWritableDb().getGroupAnnouncementDao();
        return lastChatDao.insertOrReplace(groupAnnouncementEntity);
    }


    /**
     * -------------------------下面开始ExpressionEntity 操作相关---------------------------------------
     */
    public ExpressionEntity getExpressionEntityByLabel(String label) {
        ExpressionDao dao = openReadableDb().getExpressionDao();
        return dao.queryBuilder().
                where(ExpressionDao.Properties.Label.eq(label)).limit(1).unique();
    }

    public List<ExpressionEntity> loadAllExpressionEntity() {
        ExpressionDao dao = openReadableDb().getExpressionDao();
        return dao.loadAll();
    }

    public ExpressionEntity getExpressionEntityByLabelAndContent(String label, String content) {
        ExpressionDao dao = openReadableDb().getExpressionDao();
        return dao.queryBuilder().
                where(ExpressionDao.Properties.Label.eq(label), ExpressionDao.Properties.Content.eq(content)).limit(1).unique();
    }

    public void insertOrUpdateExpressionEntity(ExpressionEntity expressionEntity) {
        ExpressionDao dao = openWritableDb().getExpressionDao();
        dao.insertOrReplace(expressionEntity);
    }

    public void insertOrUpdateAllExpressionEntity(List<ExpressionEntity> expressionList) {
        if (expressionList.size() <= 0) {
            return;
        }
        ExpressionDao dao = openWritableDb().getExpressionDao();
        dao.insertOrReplaceInTx(expressionList);
    }

    public void deleteExpressionEntityByLabelAndContent(String label, String content) {
        ExpressionDao dao = openWritableDb().getExpressionDao();
        QueryBuilder<ExpressionEntity> qb = dao.queryBuilder();
        DeleteQuery<ExpressionEntity> bd = qb.where(ExpressionDao.Properties.Label.eq(label), ExpressionDao.Properties.Content.eq(content)).buildDelete();
        bd.executeDeleteWithoutDetachingEntities();
    }


    /**
     * -------------------------下面开始ApplyFriendEntity 操作相关---------------------------------------
     */
    public ApplyFriendEntity getApplyFriendEntityByUidAndAgree(String uid, String agree) {
        ApplyFriendDao dao = openReadableDb().getApplyFriendDao();
        return dao.queryBuilder().where(ApplyFriendDao.Properties.Uid.eq(uid),
                (ApplyFriendDao.Properties.Agree.eq(agree))).limit(1).unique();
    }


    public long batchInsertOrUpdateApplyFriendEntity(ApplyFriendEntity applyFriendEntity) {
        ApplyFriendDao dao = openWritableDb().getApplyFriendDao();
        return dao.insertOrReplace(applyFriendEntity);
    }

    public List<ApplyFriendEntity> loadAllApplyFriendEntity() {
        ApplyFriendDao dao = openReadableDb().getApplyFriendDao();
        return dao.loadAll();
    }

    public long getAgreeFalseNum() {
        ApplyFriendDao dao = openReadableDb().getApplyFriendDao();
        return dao.queryBuilder().where(ApplyFriendDao.Properties.Agree.eq("false")).count();
    }

    /**
     * -------------------------下面开始ApplyGroupEntity 操作相关---------------------------------------
     */

    public long batchInsertOrUpdateApplyGroupEntity(ApplyGroupEntity applyGroupEntity) {
        ApplyGroupDao dao = openWritableDb().getApplyGroupDao();
        return dao.insertOrReplace(applyGroupEntity);
    }

    public ApplyGroupEntity getApplyGroupEntityByExtras(String groupId, String creator, String applyId, String agree) {
        ApplyGroupDao dao = openWritableDb().getApplyGroupDao();
        return dao.queryBuilder().where(ApplyGroupDao.Properties.GroupId.eq(groupId),
                (ApplyGroupDao.Properties.Creator.eq(creator)),
                (ApplyGroupDao.Properties.ApplyId.eq(applyId)),
                (ApplyGroupDao.Properties.Agree.eq(agree))).limit(1).unique();
    }

    public List<ApplyGroupEntity> loadAllApplyGroupEntity() {
        ApplyGroupDao dao = openReadableDb().getApplyGroupDao();
        return dao.loadAll();
    }

    public long getApplyGroupAgreeFalseNum() {
        ApplyGroupDao dao = openReadableDb().getApplyGroupDao();
        return dao.queryBuilder().where(ApplyGroupDao.Properties.Agree.eq("false")).count();
    }

    /**
     * @param entityType  family / story
     */
    public List<ApplyGroupEntity> loadAllApplyGroupEntityByEntityType(String entityType) {
        ApplyGroupDao dao = openReadableDb().getApplyGroupDao();
        return dao.queryBuilder().where(ApplyGroupDao.Properties.EntityType.eq(entityType)).list();
    }



    /**
     * -------------------------下面开始NodesEntity 操作相关---------------------------------------
     */

    public long batchInsertOrUpdateNodesEntity(NodesEntity nodesEntity) {
        NodesDao dao = openWritableDb().getNodesDao();
        return dao.insertOrReplace(nodesEntity);
    }

    /**
     * @param loadType  story / selfplay
     */
    public List<NodesEntity> loadAllNodesEntityByEntityType(String loadType) {
        NodesDao dao = openReadableDb().getNodesDao();
        return dao.queryBuilder().where(NodesDao.Properties.LoadType.eq(loadType)).list();
    }

    public void deleteNodesEntityByTypeId(String loadType) {
        NodesDao dao = openWritableDb().getNodesDao();
        QueryBuilder<NodesEntity> qb = openWritableDb().getNodesDao().queryBuilder();
        DeleteQuery<NodesEntity> bd = qb.where(NodesDao.Properties.LoadType.eq(loadType)).buildDelete();
        bd.executeDeleteWithoutDetachingEntities();
    }

    /**
     * -------------------------下面开始storyBookMarkEntity 操作相关---------------------------------------
     */

    public long batchInsertOrUpdateStoryBookMarkEntity(com.yuxip.DB.entity.StoryBookMarkEntity storyBookMarkEntity) {
        StoryBookMarkDao dao = openWritableDb().getStoryBookMarkDao();
        return dao.insertOrReplace(storyBookMarkEntity);
    }

    public com.yuxip.DB.entity.StoryBookMarkEntity getStoryBookMarkEntityById(String storyid) {
        StoryBookMarkDao dao = openReadableDb().getStoryBookMarkDao();
        return dao.queryBuilder().where(StoryBookMarkDao.Properties.StoryId.eq(storyid)).limit(1).unique();
    }

    public void deleteStoryBookMarkEntityByTypeId(String storyid) {
        StoryBookMarkDao dao = openWritableDb().getStoryBookMarkDao();
        QueryBuilder<com.yuxip.DB.entity.StoryBookMarkEntity> qb = openWritableDb().getStoryBookMarkDao().queryBuilder();
        DeleteQuery<com.yuxip.DB.entity.StoryBookMarkEntity> bd = qb.where(StoryBookMarkDao.Properties.StoryId.eq(storyid)).buildDelete();
        bd.executeDeleteWithoutDetachingEntities();
    }



}
