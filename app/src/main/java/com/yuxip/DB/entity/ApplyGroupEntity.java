package com.yuxip.DB.entity;

// THIS CODE IS GENERATED BY greenDAO, EDIT ONLY INSIDE THE "KEEP"-SECTIONS

// KEEP INCLUDES - put your custom includes here
// KEEP INCLUDES END
/**
 * Entity mapped to table "ApplyGroup".
 */
public class ApplyGroupEntity {

    private Long id;
    /** Not-null value. */
    private String groupId;
    private String portrait;
    /** Not-null value. */
    private String entityType;
    /** Not-null value. */
    private String entityId;
    /** Not-null value. */
    private String entityName;
    /** Not-null value. */
    private String creator;
    /** Not-null value. */
    private String creatorId;
    /** Not-null value. */
    private String createTime;
    /** Not-null value. */
    private String type;
    /** Not-null value. */
    private String applyId;
    /** Not-null value. */
    private String applyName;
    /** Not-null value. */
    private String applyPortrait;
    /** Not-null value. */
    private String agree;

    // KEEP FIELDS - put your custom fields here
    // KEEP FIELDS END

    public ApplyGroupEntity() {
    }

    public ApplyGroupEntity(Long id) {
        this.id = id;
    }

    public ApplyGroupEntity(Long id, String groupId, String portrait, String entityType, String entityId, String entityName, String creator, String creatorId, String createTime, String type, String applyId, String applyName, String applyPortrait, String agree) {
        this.id = id;
        this.groupId = groupId;
        this.portrait = portrait;
        this.entityType = entityType;
        this.entityId = entityId;
        this.entityName = entityName;
        this.creator = creator;
        this.creatorId = creatorId;
        this.createTime = createTime;
        this.type = type;
        this.applyId = applyId;
        this.applyName = applyName;
        this.applyPortrait = applyPortrait;
        this.agree = agree;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /** Not-null value. */
    public String getGroupId() {
        return groupId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    /** Not-null value. */
    public String getEntityType() {
        return entityType;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    /** Not-null value. */
    public String getEntityId() {
        return entityId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /** Not-null value. */
    public String getEntityName() {
        return entityName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    /** Not-null value. */
    public String getCreator() {
        return creator;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /** Not-null value. */
    public String getCreatorId() {
        return creatorId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    /** Not-null value. */
    public String getCreateTime() {
        return createTime;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /** Not-null value. */
    public String getType() {
        return type;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setType(String type) {
        this.type = type;
    }

    /** Not-null value. */
    public String getApplyId() {
        return applyId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setApplyId(String applyId) {
        this.applyId = applyId;
    }

    /** Not-null value. */
    public String getApplyName() {
        return applyName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }

    /** Not-null value. */
    public String getApplyPortrait() {
        return applyPortrait;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setApplyPortrait(String applyPortrait) {
        this.applyPortrait = applyPortrait;
    }

    /** Not-null value. */
    public String getAgree() {
        return agree;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setAgree(String agree) {
        this.agree = agree;
    }

    // KEEP METHODS - put your custom methods here
    // KEEP METHODS END

}
