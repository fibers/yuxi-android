package com.yuxip.DB.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.yuxip.DB.entity.NodesEntity;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "Nodes".
*/
public class NodesDao extends AbstractDao<NodesEntity, Long> {

    public static final String TABLENAME = "Nodes";

    /**
     * Properties of entity NodesEntity.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property _id = new Property(0, long.class, "_id", true, "_ID");
        public final static Property Id = new Property(1, String.class, "id", false, "ID");
        public final static Property Ishighlight = new Property(2, String.class, "ishighlight", false, "ISHIGHLIGHT");
        public final static Property Name = new Property(3, String.class, "name", false, "NAME");
        public final static Property Type = new Property(4, String.class, "type", false, "TYPE");
        public final static Property Url = new Property(5, String.class, "url", false, "URL");
        public final static Property LoadType = new Property(6, String.class, "loadType", false, "LOAD_TYPE");
    };


    public NodesDao(DaoConfig config) {
        super(config);
    }
    
    public NodesDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"Nodes\" (" + //
                "\"_ID\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ," + // 0: _id
                "\"ID\" TEXT NOT NULL ," + // 1: id
                "\"ISHIGHLIGHT\" TEXT," + // 2: ishighlight
                "\"NAME\" TEXT," + // 3: name
                "\"TYPE\" TEXT," + // 4: type
                "\"URL\" TEXT," + // 5: url
                "\"LOAD_TYPE\" TEXT);"); // 6: loadType
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"Nodes\"";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, NodesEntity entity) {
        stmt.clearBindings();
        stmt.bindLong(1, entity.get_id());
        stmt.bindString(2, entity.getId());
 
        String ishighlight = entity.getIshighlight();
        if (ishighlight != null) {
            stmt.bindString(3, ishighlight);
        }
 
        String name = entity.getName();
        if (name != null) {
            stmt.bindString(4, name);
        }
 
        String type = entity.getType();
        if (type != null) {
            stmt.bindString(5, type);
        }
 
        String url = entity.getUrl();
        if (url != null) {
            stmt.bindString(6, url);
        }
 
        String loadType = entity.getLoadType();
        if (loadType != null) {
            stmt.bindString(7, loadType);
        }
    }

    /** @inheritdoc */
    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.getLong(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public NodesEntity readEntity(Cursor cursor, int offset) {
        NodesEntity entity = new NodesEntity( //
            cursor.getLong(offset + 0), // _id
            cursor.getString(offset + 1), // id
            cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2), // ishighlight
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // name
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // type
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // url
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // loadType
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, NodesEntity entity, int offset) {
        entity.set_id(cursor.getLong(offset + 0));
        entity.setId(cursor.getString(offset + 1));
        entity.setIshighlight(cursor.isNull(offset + 2) ? null : cursor.getString(offset + 2));
        entity.setName(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setType(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setUrl(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setLoadType(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    /** @inheritdoc */
    @Override
    protected Long updateKeyAfterInsert(NodesEntity entity, long rowId) {
        entity.set_id(rowId);
        return rowId;
    }
    
    /** @inheritdoc */
    @Override
    public Long getKey(NodesEntity entity) {
        if(entity != null) {
            return entity.get_id();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
