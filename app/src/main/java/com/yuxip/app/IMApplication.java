package com.yuxip.app;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Environment;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.yuxip.imservice.service.IMService;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import im.fir.sdk.FIR;


public class IMApplication extends MultiDexApplication {

    private Logger logger = Logger.getLogger(IMApplication.class);
    private boolean isDownload;
    private static boolean isNetwork = true;
    public static boolean IS_REFRESH = false;

    public static boolean IS_KILLED = false;


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        isDownload = false;
        logger.i("Application starts");
        startIMService();
//		startNetwork();
        ImageLoaderUtil.initImageLoaderConfig(getApplicationContext());
        //这个是第三方的bug收集
        FIR.init(this);
        //内存泄露的检测
        refWatcher = LeakCanary.install(this);
        /*
        这里是用来捕获全局异常的,如果需要的话,请改为true
        上面那个是第三方的bug收集，用第三方的。
         */
//        if (ConstantValues.isCatchException) {
//        Thread.setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHander());
//        }
        /*Stetho.initialize(Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());*/
    }

    private void startNetwork() {
        logger.i("start NetworkState");
        Intent intent = new Intent();
        intent.setAction("com.BeeFramework.NetworkState.Service");
        startService(intent);
    }

    private void startIMService() {
        logger.i("start IMService");
        Intent intent = new Intent();
        intent.setClass(this, IMService.class);
        startService(intent);
    }

    public static boolean gifRunning = true;//gif是否运行

    public boolean isDownload() {
        return isDownload;
    }

    public void setDownload(boolean isDownload) {
        this.isDownload = isDownload;
    }


    /**
     * TODO ---打包前开启------ 实现,未处理异常的处理器,在内部uncaughtException()方法中,留遗嘱
     */
    private class MyUncaughtExceptionHander implements Thread.UncaughtExceptionHandler {

        private String readFile(File file) {
            if (null != file && file.isFile() && file.exists()) {
                InputStreamReader read = null;
                BufferedReader bufferedReader = null;
                try {
                    read = new InputStreamReader(new FileInputStream(file));
                    bufferedReader = new BufferedReader(read);
                    String line = null;
                    StringBuilder sb = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    return sb.toString();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        // 当一个线程,终止时,该方法,可以,执行些剩余的操作___就是,临终前,留遗嘱用的
        public void uncaughtException(Thread thread, Throwable ex) {
            // 异常已经产生了，虚拟机即将关闭，在虚拟机关闭之前，留下一段时间，留遗嘱
//			Log.e("Test",
//					"发生了一个未捕获的异常,最终在我这被处理了.=====\n" + ex.toString() + "====\n"
//							+ ex.getMessage() + "====\n"
//							+ ex.getLocalizedMessage());
            try {
                // 操作系统版本 2.2 2.3 4.1
                int version = Build.VERSION.SDK_INT; // 2.3-10 4.1.x- 16
                // ------------★----Build类,可以获取,关于手机的一些,详细属性.例如,生产厂家等
                // 手机类型 厂商
                String factory = Build.MANUFACTURER;
                // 时间
                long time = System.currentTimeMillis();

                // 正在运行的程序_将出现的异常信息,写到本地
                File file = new File(Environment.getExternalStorageDirectory(),
                        "LogError.log");
                PrintWriter err = new PrintWriter(file);// 打印流
                ex.printStackTrace(err);
                err.append("------YUXI--------version:" + version
                        + "-------factory:" + factory + "-------time:" + time);
//                ex.printStackTrace(System.out);// 打印到控制台
                err.close();


//				readErrorLog();
            } catch (FileNotFoundException e) {
                e.printStackTrace();// android日志信息打印到logcat 控制台 环形的内存空间
            }
            // 早死早超生-----留完,遗嘱后,使用自杀.---这里可以,让Android系统,认为,本程序,是不正当关闭,他会再次启动应用.--实现原地复活(前提系统有足够的空间)
            android.os.Process.killProcess(android.os.Process.myPid());// 专注自杀的方法,只能自杀的方法.
            System.exit(0);
        }
    }


    private RefWatcher refWatcher;

    public static RefWatcher getRefWatcher(Context context) {
        IMApplication application = (IMApplication) context.getApplicationContext();
        return application.refWatcher;
    }

}
