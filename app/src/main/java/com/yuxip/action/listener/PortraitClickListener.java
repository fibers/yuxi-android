package com.yuxip.action.listener;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.yuxip.config.type.PortraitType;
import com.yuxip.utils.IMUIHelper;

/**
 * Created by SummerRC on 2015/8/20.
 * description : 头像的点击事件
 */
public class PortraitClickListener implements View.OnClickListener {
    private PortraitType type;
    private String uid;
    private Context context;
    private String storyId = "";
    private String portrait = "";
    private String groupId;

    public PortraitClickListener(PortraitType type, String uid, Context context){
        this.type = type;
        this.uid = uid;
        this.context = context;
    }

    public PortraitClickListener(PortraitType type, String uid, String storyId, String groupId, String portrait, Context context){
        this(type, uid, context);
        this.storyId = storyId;
        this.portrait = portrait;
        this.groupId = groupId;
    }

    @Override
    public void onClick(View v) {
        switch (type) {
            case PORTRAIT_IN_FRIEND_CHAT:       //好友聊天
                IMUIHelper.openUserHomePageActivity(context, uid);
                break;
            case PORTRAIT_IN_FAMILY_CHAT:       //家族聊天
                IMUIHelper.openUserHomePageActivity(context, uid);
                break;
            case PORTRAIT_IN_DRAMA_CHAT:        //剧聊天
                if(TextUtils.isEmpty(storyId) || TextUtils.isEmpty(groupId)) {
                    IMUIHelper.openUserHomePageActivity(context, uid);
                } else {
                    IMUIHelper.openHisGroupCardActivity(context, uid, storyId, groupId, portrait);
                }
                break;
            case PORTRAIT_IN_TOPIC_CHAT:        //话题聊天
                break;
        }
    }
}
