package com.yuxip.config;

public interface ConstantValues {

    String FIRST_DOWNLOAD = "first_download";
    String FIRST_LOGIN = "first_login";
    String ONLY_ONE_ID = "1000001";
    String USERTYPE = "1_";
    String QQAPPID = "1104542960";


    //测试服务器地址  开发用下面的地址
//    String SERVERURL = "http://112.126.67.151:10001/";
//    String ShareStoryUrl = "http://123.56.154.93:88/ichees/read?storyid=";            //分享剧
//    String ShareSelfStory = "http://123.56.154.93:88/ichees/selfstory?storyid=";      //分享自戏
//    String ACCESS_SERVER_ADDRESS = "http://123.56.154.93:8080/msg_server";
//    String BqunUrl = "http://123.56.154.93:88/ichees/copyright?storyid=";


    String SERVERURL = "http://123.57.136.1:10001/";  //接口服务器
    String ShareStoryUrl = "http://reader.yuxip.com/ichees/read?storyid=";              //分享剧
    String ShareSelfStory = "http://reader.yuxip.com/ichees/selfstory?storyid=";        //分享自戏  123.56.154.93:86 换成 http://reader.yuxip.com
    String ACCESS_SERVER_ADDRESS = "http://123.56.129.7:8080/msg_server";               //IM服务器
    String BqunUrl = "http://123.56.154.93:86/ichees/copyright?storyid=";

    //注册
    String Register = SERVERURL + "Register";
    //获得好友列表
    String GETFRIENDLIST = SERVERURL + "GetFriendList";
    //获得好友分组列表
    String GetFriendListWithGroup = SERVERURL + "GetFriendListWithGroup";

    String GetContact = SERVERURL + "GetContact";
    //第一次启动及登陆用的统计接口
    String SUBMITDEICEINFO = SERVERURL + "SubmitDeiceInfo";


    //获得我的剧列表
    String GETMYSTROY = SERVERURL + "GetMyStory";
    String GETSTORYINFO = SERVERURL + "GetStoryInfo";
    //校验短信验证
    String TESTGETCODE = SERVERURL + "VCodeRequest";
    //校验验证码
    String VCODECHECK = SERVERURL + "VCodeCheck";


    //搜索好友列表
    String SEARCHPERSON = SERVERURL + "SearchPerson";

    //发送意见反馈
    String SENDFEELBACK = SERVERURL + "SendCustomProblem";
    //添加好友流程的获取未读请求
    String GetUnreadNotify = SERVERURL + "GetUnreadNotify";
    //添加好友流程的更新服务器状态
    String UpdateNotifyState = SERVERURL + "UpdateNotifyState";
    //获取个人信息
    String GetPersonInfo = SERVERURL + "GetPersonInfo";


    //获取自戏
    String CreateSelfStory = SERVERURL + "CreateSelfStory";

    //获取人设信息
    String GetUserRoleInfoInStory = SERVERURL + "GetUserRoleInfoInStory";


    //修改自戏
    String ModifySelfStoryContent = SERVERURL + "ModifySelfStoryContent";

    //创建剧
    String CreateStory = SERVERURL + "CreateStory";
    //关联剧和群
    String AddGroupToStory = SERVERURL + "AddGroupToStory";

    //创建家族
    String NewFamily = SERVERURL + "NewFamily";
    //获取我的家族列表
    String GetMyFamilyList = SERVERURL + "GetMyFamilyList";


    String GetFamilyListByType = SERVERURL + "GetFamilyListByType";
    //根据Groupid，返回是家族还是剧的信息
    String GetGroupDetail = SERVERURL + "GetGroupDetail";

    //推出家族
    String ExitFamily = SERVERURL + "ExitFamily";




    //获取所有家族列表
    String GetFamilyInfo = SERVERURL + "GetFamilyInfo";

    //获取自戏详情列表
    String GetSelfStoryDetail = SERVERURL + "GetSelfStoryDetail";
    //点赞
    String PraiseStory = SERVERURL + "PraiseStory";

    String ChangePass = SERVERURL + "ChangePass";

    //提交个人信息
    String SubmitPersonInfo = SERVERURL + "SubmitPersonInfo";

    //获得主线剧情
    String GetStoryScenes = SERVERURL + "GetStoryScenes";

    //获得主线剧情
    String GetStoryRoles = SERVERURL + "GetStoryRoles";


    //获取群公告
    String GetStoryGroupBoard = SERVERURL + "GetStoryGroupBoard";


    //获得已经开戏的剧
    String GetPlayingStory = SERVERURL + "GetPlayingStory";

    //搜索家族
    String SearchFamily = SERVERURL + "SearchFamily";

    //搜索ju
    String SearchStory = SERVERURL + "SearchStory";

    String GetStoryGroupInfo = SERVERURL + "GetStoryGroupInfo";

    //更新群公告
    String UpdateStoryGroupBoard = SERVERURL + "UpdateStoryGroupBoard";

    //更新家族名称
    String ModifyStoryGroupInfo = SERVERURL + "ModifyStoryGroupInfo";

    //修改家族里面的昵称
    String ModifyFamilyPersonInfo = SERVERURL + "ModifyFamilyPersonInfo";

    //获取广告图
    String GetBanners = SERVERURL + "GetBanners";

    //获取激活码信息
    String GetConfiguration = SERVERURL + "GetConfiguration";

    //举报剧
    String ReportStory = SERVERURL + "ReportStory";

    //举报好友
    String ReportPerson = SERVERURL + "ReportPerson";

    //举报家族
    String ReportFamily = SERVERURL + "ReportFamily";


    //修改剧的简介和规则
    String ModifyStory = SERVERURL + "ModifyStory";

    //创建主线剧情
    String CreateStoryScene = SERVERURL + "CreateStoryScene";

    //修改主线剧情
    String ModifyStoryScene = SERVERURL + "ModifyStoryScene";

    //创建主线角色
    String CreateStoryRole = SERVERURL + "CreateStoryRole";

    //修改主线角色
    String ModifyStoryRole = SERVERURL + "ModifyStoryRole";

    //阅读权限设置
    String SetGroupPerssion = SERVERURL + "SetGroupPerssion";
    //修改家族信息
    String ModifyFamilyInfo = SERVERURL + "ModifyFamilyInfo";

    //获取剧的内容
    String GetStoryContent = SERVERURL + "GetStoryContent";

    //修改用户的昵称
    String ModifyUserName = SERVERURL + "ModifyUserName";

    String GetUsersName = SERVERURL + "GetUsersName";

    //删除好友分组
    String DelFriendsGroup = SERVERURL + "DelFriendsGroup";

    //新建好友分组
    String CreateFriendsGroup = SERVERURL + "CreateFriendsGroup";
    //变更好友分组
    String ChangeFriendsGroupMember = SERVERURL + "ChangeFriendsGroupMember";

    //修改好友分组
    String UpdateFirendsGroupInfo = SERVERURL + "UpdateFirendsGroupInfo";

    //修改用户在剧中人设信息
    String SetUserRoleInfoInStory = SERVERURL + "SetUserRoleInfoInStory";


    //获取人设模版
    String GetStoryRoleTemplate = SERVERURL + "GetStoryRoleTemplate";

    //修改人设模版
    String ModifyStoryRoleTemplate = SERVERURL + "ModifyStoryRoleTemplate";

    //获取推荐的剧列表
    String GetSelectedStorys = SERVERURL + "GetSelectedStorys";

    //获取剧展示的类别
    String GetStoryShowType = SERVERURL + "GetStoryShowType";

    //获取指定剧中已经加入的剧群列表列表。
    String GetEnteredStoryGroups = SERVERURL + "GetEnteredStoryGroups";

    //检验邀请码
    String VerifyInviteCode = SERVERURL + "VerifyInviteCode";

    //广场列表
    String GetTopics = SERVERURL + "GetTopics";

    //广场话题详情
    String TopicDetail =SERVERURL +"TopicDetail";

    String StoryDetail = SERVERURL +"StoryDetail";

    //广场评论话题
    String CommentSquareResource = SERVERURL + "CommentSquareResource";

    String CommentStoryResource =   SERVERURL + "CommentStoryResource";

    //广场获取子楼层
    String TopicCommentDetail = SERVERURL + "TopicCommentDetail";

    String StoryCommentDetail  = SERVERURL + "StoryCommentDetail";

    //广场删除广场资源
    String DeleteSquareResource = SERVERURL + "DeleteSquareResource";

    String DeleteStoryResource = SERVERURL + "DeleteStoryResource";

    //广场 收藏资源
    String CollectSquareResource  =SERVERURL + "CollectSquareResource";

    String CollectStoryResource = SERVERURL +"CollectStoryResource";
    //广场点赞资源
    String PraiseSquareResource = SERVERURL +"PraiseSquareResource";

   //广场举报 资源
    String ReportSquareResource = SERVERURL +"ReportSquareResource";

    String  ReportStoryResource = SERVERURL + "ReportStoryResource";

    String PraiseStoryResource  = SERVERURL +"PraiseStoryResource";


    //获取当前用户的角色属性列表
    String GetStoryRoleNatures = SERVERURL + "GetStoryRoleNatures";

    //获取该剧的角色申请
    String GetStoryRoleApplication = SERVERURL + "GetStoryRoleApplication";

    //同意或拒绝审核角色的申请
    String ReviewRoleApplication = SERVERURL + "ReviewRoleApplication";

    //获取当前申请入审核群的对应剧的角色设定
    String  GetStoryRoleSettings = SERVERURL + "GetStoryRoleSettings";
    //提交角色申请
    String  ApplyStoryRole  = SERVERURL  + "ApplyStoryRole";
    //提交管理员的角色申请
    String ApplyMyStoryRole = SERVERURL + "ApplyMyStoryRole";
    //获取自己未处理的角色申请
    String GetMyStoryRoleApplications = SERVERURL  + "GetMyStoryRoleApplications";

    //获取不同的剧的列表
    String GetStorysByType = SERVERURL + "GetStorysByType";

    //创建剧完成提交
    String CreateStoryStepByStep = SERVERURL + "CreateStoryStepByStep";

    //收藏剧
    String CollectStory = SERVERURL + "CollectStory";

    String GetNewUserList = SERVERURL + "GetNewUserList";


    String GetStoryCategory = SERVERURL + "GetStoryCategory";


    String STORY_DETAILS = SERVERURL + "GetStoryInfo";

    String STORY_PRAISE = SERVERURL + "PraiseStory";

    String GetTopicDetail = SERVERURL + "GetTopicDetail";
    String PublishTopic = SERVERURL + "PublishTopic";

    String DeleteStory = SERVERURL + "DeleteStory";

    String RecommendStory = SERVERURL + "RecommendStory";

    String GetMyCommentGroupsInfo = SERVERURL + "GetMyCommentGroupsInfo";

    String GetSelfStory = SERVERURL + "GetSelfStory";
    String GROUP_TYPE_SHENHE = "0";
    String GROUP_TYPE_PLAY = "1";
    String GROUP_TYPE_SHUILIAO = "2";
    String GROUP_TYPE_COMMENT = "3";
    String GROUP_TYPE_FAMILY = "9";

    int HOME_TYPE_PLAY = 10;
    String STORY_TYPE_ZIXI = "1";

    String RECOMMAND_TYPE = "recommand_type";
    String RECOMMAND_STORY = "recommand_story";
    String RECOMMAND_PLAY = "recommand_play";
    String RECOMMAND_TYPE_JUMP = "recommand_type_jump";





    String BROADCAST_REFRESH_HOME = "broadcase_refresh_home";
    String PraiseTopics = SERVERURL + "PraiseTopics";
    String BROADCAST_REFRESH_RECOMMAND = "broadcast_refresh_recommand";

    String GETGROUPHISTORYMSG = SERVERURL + "GetGroupHistoryMsg";

    String GetCommentGroupDetail = SERVERURL + "GetCommentGroupDetail";

    String GetLatestPackageInfo = SERVERURL + "GetLatestPackageInfo";

    String CREATE_NEW_TOPIC = SERVERURL + "CreateNewTopic";
    String SQUARE_LIST = SERVERURL + "SquareList";
}
