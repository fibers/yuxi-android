package com.yuxip.config;

import android.os.Environment;

public class UrlConstant {

    // 头像路径前缀
    public final static String AVATAR_URL_PREFIX = "";

    public final static String ACCESS_MSG_ADDRESS = ConstantValues.ACCESS_SERVER_ADDRESS;

    //本地头像路径
    public final static String HEAD_IMG_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + "/HeadImg.png";

}
