package com.yuxip.config;

/**
 * 创建群的类型
 * created by SummerRC
 */
public class CreateGroupType {
    public static final int CREATE_GROUP_TYPE_PLAY_REVIEW       = 10;         //审核
    public static final int CREATE_GROUP_TYPE_PLAY_WATER_CHAT   = 11;         //水聊
    public static final int CREATE_GROUP_TYPE_PLAY_IS_PLAY      = 12;        //对戏
    public static final int CREATE_GROUP_TYPE_PLAY_COMMENT      = 13;        //评论
}
