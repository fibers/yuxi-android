package com.yuxip.config.type;

/**
 * Created by user on 2015/8/20.
 * description : 标识头像类别
 */
public enum PortraitType {
    PORTRAIT_IN_FRIEND_CHAT,        //好友聊天
    PORTRAIT_IN_FAMILY_CHAT,        //家族聊天
    PORTRAIT_IN_DRAMA_CHAT,         //剧聊天
    PORTRAIT_IN_TOPIC_CHAT          //话题聊天
}
