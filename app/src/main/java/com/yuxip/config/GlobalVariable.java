package com.yuxip.config;

/**
 * 全局变量类
 * Created by SummerRC on 2015/6/2.
 */
public class GlobalVariable {
    /** ------------------------------------ 创建群时用于EventBus区分建群操作所在的Activity begin---------------------- */
    public enum CurrentActivity {
        Activity_MAIN, ACTIVITY_ADD_STORY_GROUP,
        ACTIVITY_CREATE_PLAY, ACTIVITY_CREATE_FAMILY,
        ACTIVITY_CREATE_ZIXI,ACTIVITY_CREATE_TOPIC,
        ACTIVTIY_STORY_DETAILS_ADD_MEMBER, ACTIVTIY_STORY_DETAILS_REQ_INFO,
        ACTIVITY_DEFAULT
    }
    /** 创建群操作所在的Activity */
    public static CurrentActivity currentActivity = CurrentActivity.ACTIVITY_DEFAULT;
    /** ------------------------------------ 创建群时用于EventBus区分建群操作所在的Activity end  ---------------------- */


    /** ------------------------------------ 获取未读消息设置MainActivity底部按钮小红点时的区分 begin---------------------- */
    public enum SetUnreadButton {
        BUTTON_PLAY, BUTTON_FRIEND, BUTTON_FAMILY, BUTTON_DEFAULT
    }

    public static SetUnreadButton setUnreadButton = SetUnreadButton.BUTTON_DEFAULT;
    /** ------------------------------------ 获取未读消息设置MainActivity底部按钮小红点时的区分 end  ---------------------- */

}
