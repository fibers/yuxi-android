package com.yuxip.config;

/**
 * intent bundle 传值设定
 */
public class IntentConstant {
    public static final String KEY_LOGIN_NOT_AUTO = "login_not_auto";
    public static final String KEY_LOCATE_DEPARTMENT = "key_locate_department";
    public static final String FAMILY_INFO_START_FROM = "family_start_from";
    public static final String IS_FROM_CHAT = "IS_FROM_CHAT";
    public static final String PREVIEW_TEXT_CONTENT = "content";
    public static final String EXTRA_IMAGE_LIST = "imagelist";
    public static final String EXTRA_ALBUM_NAME = "name";
    public static final String WEBVIEW_URL = "WEBVIEW_URL";
    public static final String CUR_MESSAGE = "CUR_MESSAGE";
    public static final String POSITION="POSITION";

    /**
     * ---------------------------------  add by SummerRC  ------------------------------------------------
     */
    public static final String REPORT_TYPE = "REPORT_TYPE";
    public static final String GROUP_ID = "GROUP_ID";
    public static final String PERSION_ID = "PERSION_ID";
    public static final String STORY_ID = "STORY_ID";
    public static final String CREATOR_ID = "CREATOR_ID";
    public static final String CREATOR_NAME = "CREATOR_NAME";
    public static final String CREATOR_TIME = "CREATOR_TIME";
    public static final String RELATION = "relation";
    public static final String TOPIC_ID = "TOPIC_ID";
    public static final String IS_ADMIN = "IS_ADMIN";
    public static final String SESSION_KEY = "SESSION_KEY";
    public static final String PERMISSION = "PERMISSION";
    public static final String UID = "UID";
    public static final String REMARK_NAME = "REMARK_NAME";
    public static final String INPUT_CONTENT = "INPUT_CONTENT";
    public static final String CORY_RIGHT_URL = "CORY_RIGHT_URL";

    public static final String SELECTED_IDS = "SELECTED_IDS";

    public static final String STORY_DETAIL_ID = "storyid";

    public static final String GROUP_DETAIL_ID = "familyid";

    public static final String IS_PLAY = "isplay";

    public static final String TYPE_EDIT = "TYPE_EDIT";
    public static final String TYPE_ADD = "TYPE_ADD";
    public static final String TITLE = "TITLE";
    public static final String CONTENT = "CONTENT";

    public static final String HISBOOK_ENTITY = "hisbook_entity";
    public static final String PORTRAIT = "portrait";
    public static final String GROUP_NAME = "GROUP_NAME";
    public static final String READ_RIGHT = "READ_RIGHT";

    public static final String ShenHeGroupId = "ShenHeGroupId";
    public static final String ShenHeGroupTitle = "ShenHeGroupTitle";

    public static final String TYPE = "TYPE";
    public static final String LIST = "LIST";
    public static final String FROMUSER = "FROMUSER";
    public static final int ChangeStoryGroupName = 1;
    public static final int ReadRightSettingActivity = 222;
    public static final String FRIEND_GROUPS_NAME = "FRIEND_GROUPS_NAME";
    public static final String FRIEND_GROUPS_ID = "FRIEND_GROUPS_ID";
    public static final String FRIEND_GROUPS_NUMS = "FRIEND_GROUPS_NUMS";
    public static final String STORY = "STORY";                             //剧的评论群
    public static final String SELF_STORY = "SELF_STORY";                   //自戏的评论群
    public static final String TOPIC = "TOPIC";                             //话题的评论群

    public static final String STORY_DETAIL_NEED_REFRESH = "STORY_DETAIL_NEED_REFRESH";

    /**
     * 所有家族跳转到家族详情页时的判断
     */
    public static final String FAMILY_ID = "family_id";
    public static final String FamilyDataActivity_Type = "FamilyDataActivity_Type";

    public enum FamilyDataActivityType {
        TYPE_ADMIN, TYPE_MEMBER, TYPE_NOT_MEMBER
    }

    /**
     * 修改家族资料时的判断：个人昵称或者家族逆臣提供
     */
    public static final String ModifyFamilyPersonInfoActivity_TYPE = "ModifyFamilyPersonInfoActivity_TYPE";

    public enum ModifyFamilyPersonInfoActivityTYPE {
        TYPE_PERSON_NICKNAME, TYPE_FAMILY_NAME
    }

    /**
     * ---------------------------------  add by Ly  ------------------------------------------------
     */
    public static final String FLOOR_TITLE =  "FLOOR_TITLE";
    public static final String FLOOR_NUMBER = "FLOOR_NUMBER";
    public static final String FlOOR_USERID = "FLOOR_USERID";
    public static final String FLOOR_USERNAME = "FLOOR_USERNAME";
    public static final String FLOOR_TOPICID  = "FLOOR_TOPICID";
    public static final String FLOOR_COMMENTID ="FLOOR_COMMENTID";
    public static final String FLOOR_CHILD_TOPICID = "FLOOR_CHILD_TOPICID";
    public static final String FLOOR_PASS_TYPE = "FLOOR_PASS_TYPE";
    public static final String FLOOR_TYPE_STORY = "FLOOR_TYPE_STORY";
    public static final String FLOOR_TYPE_TOPIC = "FLOOR_TYPE_TOPIC";
    public static final String FLOOR_STORYID = "FLOOR_STORYID";
    public static final String FlOOR_REPORTID = "FlOOR_REPORTID";
    public static final String FLOOR_REPORTTYPE = "FLOOR_REPORTTYPE";
    public static final String FLOOR_REPORTTYPE_TOPIC = "FLOOR_REPORTTYPE_TOPIC";
    public static final String FLOOR_REPORTTYPE_COMMENT = "FLOOR_REPORTTYPE_COMMENT";

    public static final String SELECT_EVENT_TYPE = "SELECT_EVENT_TYPE";
    public static final String SELECT_FROM_RELEASE_TOPIC = "SELECT_FROM_RELEASE_TOPIC";
    public static final String SELECT_FROM_CHAT = "SELECT_FROM_CHAT";

    public static final String FLOOR_COMMENT_TYPE_HOT = "FLOOR_COMMENT_TYPE_HOT";
    public static final String FLOOR_COMMENT_TYPE_ALL ="FLOOR_COMMENT_TYPE_ALL";
    public static final String AUDIT_ROLE_APPLY_ID = "AUDIT_ROLE_APPLY_ID";
    public static final String APPLY_ROLE_STORY_ID = "APPLY_ROLE_STORY_ID";
    public static final String ACTIVITY_OPEN_TYPE_LEFT ="ACTIVITY_OPEN_TYPE_LEFT";
    public static final String ACTIVTY_OPEN_TYPE_RIGHT = "ACTIIVTY_OPEN_TYPE_RIGHT";
    public static final String FAMILY_CHANGE_RESULT = "FAMILY_CHANGE_RESULT";
}
