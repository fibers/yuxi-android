package com.yuxip.ui.activity.square;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.activity.base.TTBaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ly on 2015/10/21.
 * 举报 话题 或 评论
 */
public class ReportCommentActivity extends TTBaseActivity {
    private ListView listView;
    private MyAdapter adapter;
    private String reportResons[] = new String[]{"广告", "色情", "反动", "头像"};
    private int reportReason = -1;
    private String reportUrl;
    private int type;
    private String reportId;
    private String reportType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_report_comment, topContentView);
        initRes();
    }

    private void initRes() {
        listView = (ListView) findViewById(R.id.listview_report_comment_activity);
        adapter = new MyAdapter();
        listView.setAdapter(adapter);
        setTitle("举报");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("确定");
        reportId = getIntent().getStringExtra(IntentConstant.FlOOR_REPORTID);
        reportType = getIntent().getStringExtra(IntentConstant.FLOOR_REPORTTYPE);
        if(reportType.equals(IntentConstant.FLOOR_REPORTTYPE_TOPIC)){
            type = 0;
        }else{
            type = 1;
        }
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            reportUrl = ConstantValues.ReportStoryResource;
        }else{
            reportUrl =ConstantValues.ReportSquareResource;
        }
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reportReason != -1) {
                    reportResource();
                } else {
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_report_noselect),Toast.LENGTH_LONG).show();
                }
            }
        });


    }


    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return reportResons.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(ReportCommentActivity.this).inflate(R.layout.item_list_report_comment_actiivty, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tv_reportReason.setText(reportResons[position]);
            viewHolder.checkBox.setTag(position);
            viewHolder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    for (int i = 0; i < listView.getChildCount(); i++) {
                        ((CheckBox) listView.getChildAt(i).findViewById(R.id.cb_listitem_reportcomment)).setChecked(false);
                    }
                    ((CheckBox) listView.getChildAt(position).findViewById(R.id.cb_listitem_reportcomment)).setChecked(true);
                    reportReason = position+1;
                }
            });
            return convertView;
        }

        class ViewHolder {
            TextView tv_reportReason;
            CheckBox checkBox;

            public ViewHolder(View view) {
                tv_reportReason = (TextView) view.findViewById(R.id.tv_listitem_reportcomment);
                checkBox = (CheckBox) view.findViewById(R.id.cb_listitem_reportcomment);
            }
        }
    }


    private void reportResource() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("token", "110");
        params.addParams("resourceID", reportId);
        params.addParams("reportReason",reportReason+"");
        params.addParams("resourceType",type+"");
        OkHttpClientManager.postAsy(reportUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }


    private void onReceiveData(String response) {
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(response);
            if(jsonObject.optString("result","").equals("100")) {
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_report_success),Toast.LENGTH_LONG).show();
                ReportCommentActivity.this.finish();
            }else{
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_report_failed),Toast.LENGTH_LONG).show();
            }
            } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_report_failed),Toast.LENGTH_LONG).show();
        }

    }
}