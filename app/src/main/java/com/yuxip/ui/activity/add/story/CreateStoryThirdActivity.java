package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

/**
 * Created by Ly on 2015/10/30.
 * 创建主线剧情
 */
public class CreateStoryThirdActivity extends Activity implements View.OnClickListener, TextWatcher {
    private EditText etMainScene;
    private TextView tvNextStep;
//    private boolean isMainSceneReady = false;
    private CustomDividerProgressbar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story_third);
        initRes();
        checkManagerData();
    }

    private void initRes() {
        setTitle(getResources().getString(R.string.story_main_scene));
        ImageView ivBack = (ImageView) findViewById(R.id.iv_back_create_story_thir);
        etMainScene = (EditText) findViewById(R.id.et_create_stroy_third);
        tvNextStep = (TextView) findViewById(R.id.tv_next_step_create_story_thir);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_thir);
        tvNextStep.setSelected(true);
        tvNextStep.setOnClickListener(this);
        etMainScene.addTextChangedListener(this);
        ivBack.setOnClickListener(this);
        progressbar.setCurProgress((int) Math.ceil(100 * 3 / 6));
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
    }

    private void checkManagerData() {
        String content = CreateStoryManager.getInstance().getSceneContent();
        if (!TextUtils.isEmpty(content) && CreateStoryManager.getInstance().getCharacterCount(content) > 0) {
            etMainScene.setText(content);
//            tvNextStep.setSelected(true);
        }
    }

//    private void checkStatus() {
//        if (isMainSceneReady) {
//            tvNextStep.setSelected(true);
//        } else {
//            tvNextStep.setSelected(false);
//        }
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_next_step_create_story_thir:
                if (!tvNextStep.isSelected()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), Toast.LENGTH_LONG).show();
                    return;
                }
                int sceneWordLimit = 500;
                if (CreateStoryManager.getInstance().getCharacterCount(etMainScene.getText().toString().trim()) > sceneWordLimit) {
                    T.show(getApplicationContext(), getResources().getString(R.string.story_main_scene_limit_500), 0);
                    return;
                }
                saveManageData();
                IMUIHelper.openCreateStoryOrderActivity(this,4,null);
                finish();
                break;
            case R.id.iv_back_create_story_thir:
                openPreAcitivty();
                finish();
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s.toString())) {
//            isMainSceneReady = false;
//            checkStatus();
        } else {
            if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) == 0) {
//                isMainSceneReady = false;
//                checkStatus();
                return;
            } else if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) > 500) {
                etMainScene.setTextColor(getResources().getColor(R.color.red_text));
            } else {
                etMainScene.setTextColor(getResources().getColor(R.color.black));
            }
//            isMainSceneReady = true;
//            checkStatus();

        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            openPreAcitivty();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void saveManageData() {
        CreateStoryManager.getInstance().setSceneContent(etMainScene.getText().toString());
    }

    private void openPreAcitivty(){
        saveManageData();
        IMUIHelper.openCreateStoryOrderActivity(this, 2, IntentConstant.ACTIVITY_OPEN_TYPE_LEFT);
    }
}
