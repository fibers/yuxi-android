package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * 修改家族个人信息的界面（家族昵称或者个人昵称）
 * Created by SummerRC on 2015/4/21.
 */
public class ModifyFamilyPersonInfoActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(ModifyFamilyPersonInfoActivity.class);

    private EditText et_content;
    private String familyid;
    private IntentConstant.ModifyFamilyPersonInfoActivityTYPE modifyFamilyPersonInfoActivityTYPE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_modify_family_person_info, topContentView);
        et_content = (EditText) findViewById(R.id.introContent);

        /** 获取从Activity传递过来的数据 ：家族id */
        familyid = getIntent().getStringExtra(IntentConstant.FAMILY_ID);
        modifyFamilyPersonInfoActivityTYPE = (IntentConstant.ModifyFamilyPersonInfoActivityTYPE) getIntent().getSerializableExtra(IntentConstant.ModifyFamilyPersonInfoActivity_TYPE);
        switch (modifyFamilyPersonInfoActivityTYPE) {
            case TYPE_FAMILY_NAME:
                setTitle("修改家族昵称");
                break;
            case TYPE_PERSON_NICKNAME:
                setTitle("修改个人昵称");
                break;
        }

        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String storyIntroContent = et_content.getText().toString().trim();
                if (TextUtils.isEmpty(storyIntroContent)) {
                    Toast.makeText(getApplicationContext(), "请输入内容", Toast.LENGTH_SHORT).show();
                } else {
                    /** 修改信息 */
                    switch (modifyFamilyPersonInfoActivityTYPE) {
                        case TYPE_PERSON_NICKNAME:
                            modifyFamilyPersonInfo();
                            break;
                        case TYPE_FAMILY_NAME:
                            modifyFamilyInfo();
                            break;
                    }
                }
            }
        });
    }


    /**
     * 修改个人信息信息
     */
    private void modifyFamilyPersonInfo() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("groupid", familyid);
        params.addParams("nickname", et_content.getText().toString());
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyFamilyPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
                finish();
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
                finish();
            }
        });
    }

    /**
     * 修改家族信息
     */
    private void modifyFamilyInfo() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("familyid", familyid);
        params.addParams("familyname", et_content.getText().toString());
        params.addParams("portrait", "");
        params.addParams("familyintro", "");
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyFamilyInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
                finish();
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
                finish();
            }
        });
    }
}
