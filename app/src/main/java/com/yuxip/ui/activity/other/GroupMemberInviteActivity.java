package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.FriendGroups;
import com.yuxip.JsonBean.FriendsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.GroupMemberInviteAdapter;

import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/6/5.
 */
public class GroupMemberInviteActivity extends TTBaseActivity {
    @InjectView(R.id.memberslist)
    ListView lvGroupMemberlist;

    private  GroupMemberInviteAdapter adapter;
    private List<FriendGroups> friendGroups =null;
    private IMService imService;
    private String groupid;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();

        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        ButterKnife.reset(this);
        imServiceConnector.disconnect(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        View.inflate(GroupMemberInviteActivity.this, R.layout.activity_group_member_invite,topContentView);
        ButterKnife.inject(GroupMemberInviteActivity.this, topContentView);
        initRes();
    }

    /**
     * 初始化资源
     */
    private void initRes() {
        setTitle("邀请群成员");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("添加");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter.getCheckListSet().size()<=0){
                    showToast("请选择添加成员");
                    return;
                }
                Set<Integer> checkListSet =  adapter.getCheckListSet();
                imService.getMessageManager().sendMsgGroupAddMemberReq(0,
                        Integer.valueOf(groupid),
                        checkListSet);

                showToast("邀请已经发出,请主人稍等!");
                finish();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        groupid = getIntent().getStringExtra("groupid");
        GetFriendList();
    }

    private void GetFriendList() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        OkHttpClientManager.postAsy(ConstantValues.GETFRIENDLIST, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        FriendsJsonBean friendsBean = new Gson().fromJson(response, FriendsJsonBean.class);
                        if (friendsBean.getResult().equals("1")) {
                            friendGroups = friendsBean.getGroups();
                            adapter = new GroupMemberInviteAdapter(GroupMemberInviteActivity.this, friendGroups, groupid);
                            lvGroupMemberlist.setAdapter(adapter);
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                    }
                }
        );
    }
}
