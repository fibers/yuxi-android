package com.yuxip.ui.activity.story;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.JsonBean.StoryDetailsBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewCompatActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.customview.SaveImagePopupWindow;
import com.yuxip.ui.customview.StoryDetailsPopupWindow;
import com.yuxip.ui.fragment.other.Select1Fragment;
import com.yuxip.ui.fragment.other.Select2Fragment;
import com.yuxip.ui.fragment.story.HotFragment;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 剧详情
 * Edited by SummerRC on 2015/07/13.
 */
public class StoryDetailsActivity extends TTBaseNewCompatActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(StoryDetailsActivity.class);
    //返回,头像,title
    @InjectView(R.id.left_btn)
    ImageView leftBtn;
    @InjectView(R.id.img_icon)
    CustomHeadImage imgIcon;
    @InjectView(R.id.tv_name)
    TextView tvName;
    //最近更新的时间
    @InjectView(R.id.tv_time)
    TextView tvTime;
    //喜欢,分享
    @InjectView(R.id.img_collection)
    ImageView imgCollection;
    @InjectView(R.id.tv_cellection)
    TextView tvCellection;
    @InjectView(R.id.img_more)
    ImageView imgMore;
    //上传的照片
    @InjectView(R.id.img_bg)
    ImageView imgBg;
    //更新时间 现在更改为创建时间
    @InjectView(R.id.tv_update_time)
    TextView tvUpdateTime;
    //字数
    @InjectView(R.id.tv_words_num)
    TextView tvWordsNum;
    //4个标签
    @InjectView(R.id.tv_select_1)
    TextView tvSelect1;
    @InjectView(R.id.tv_select_2)
    TextView tvSelect2;
    @InjectView(R.id.tv_select_3)
    TextView tvSelect3;
    @InjectView(R.id.tv_select_4)
    TextView tvSelect4;
    //评论
    @InjectView(R.id.img_chat)
    ImageView imgChat;
    //种类
    @InjectView(R.id.tv_class)
    TextView tvClass;
    //照片的描述
    @InjectView(R.id.tv_title)
    TextView tvTitle;
    @InjectView(R.id.img_shenqin)
    ImageView imgShenqin;

    @InjectView(R.id.img_read)
    ImageView imgRead;
    //参与
    @InjectView(R.id.img_jinqun)
    ImageView imgJinqun;
    //使用到的变量
    private boolean isCollect;
    private boolean isCollectTemp;              //判断初始收集状态和之后的收集状态是否一致,从而决定是否刷新
    private String storyid;
    private StoryDetailsBean sdb;
    private String adminId;
    private IMService imService;
    private final int START_FLG_SET = 1;
    private final int START_FLG_NO_SET = 0;
    private int startFlg;
    private StoryDetailsPopupWindow pop;       //页面右上角的弹出框,包含
    private boolean Is_Admin;
    private SaveImagePopupWindow saveImagePopupWindow;  //保存或预览剧图片的Pop
    private ImageMessage img = new ImageMessage();
    private ArrayList<ImageMessage> list_mess = new ArrayList<>();
    private List<TextView> list_text = new ArrayList<>();
    private boolean hasPraise;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        setContentView(R.layout.activity_story_details);
        ButterKnife.inject(this);
        EventBus.getDefault().register(this);
        saveImagePopupWindow = new SaveImagePopupWindow(this);
        initView();
        request();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        storyid = intent.getStringExtra(IntentConstant.STORY_ID);
        request();
        super.onNewIntent(intent);
    }

    /*
    *判断传递是什么类型的信息.
    * */
    public void onEventMainThread(SquareCommentEvent event) {
        switch (event.eventType) {
            case TYPE_DELETE_STORY_TOPIC:
                finish();
                break;
        }
    }

    //Activity销毁后,数据保存
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //判断是否是收藏或是喜欢,更改主页个人信息页面显示数目
        if (behaviorInint != behavior || isCollectTemp != isCollect) {
            Intent intent1 = new Intent();
            intent1.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
            sendBroadcast(intent1);
        }
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
    }

    private void initView() {
        tvSelect1.setOnClickListener(this);
        tvSelect2.setOnClickListener(this);
        tvSelect3.setOnClickListener(this);
        tvSelect4.setOnClickListener(this);

        list_text.add(tvSelect1);
        list_text.add(tvSelect2);
        list_text.add(tvSelect3);
        list_text.add(tvSelect4);

        imgBg.setOnClickListener(this);

        imgCollection.setOnClickListener(this);
        tvCellection.setOnClickListener(this);
        imgMore.setOnClickListener(this);
        imgShenqin.setOnClickListener(this);
        imgRead.setOnClickListener(this);
        imgChat.setOnClickListener(this);
        leftBtn.setOnClickListener(this);
        imgIcon.setOnClickListener(this);
        imgJinqun.setOnClickListener(this);

        changeFragment((String) tvSelect1.getTag(), Select1Fragment.class);

        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);

        setPOP();
    }

    //气泡的设置
    private void setPOP() {
        pop = new StoryDetailsPopupWindow(this);
        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                imgMore.setSelected(false);
            }
        });
        pop.setDelStoryInterface(new StoryDetailsPopupWindow.IsDelStoryInterface() {
            @Override
            public void isDel(boolean isDel) {
                if (isDel) {
                    Intent intent = new Intent();
                    intent.putExtra("id", sdb.getId());
                    intent.setAction(HotFragment.DEL_STORY_ACTION);
                    sendBroadcast(intent);
                    intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
                    sendBroadcast(intent);
                    StoryDetailsActivity.this.finish();
                }
            }

            @Override
            public void isCollect(boolean isCollect) {
                StoryDetailsActivity.this.isCollect = isCollect;
            }
        });
    }


    /*请求网络获取数据*/
    private void request() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.STORY_DETAILS, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    sdb = new Gson().fromJson(content, StoryDetailsBean.class);
//                            setData();
                    if (sdb != null && !TextUtils.isEmpty(sdb.getResult())) {
                        if (sdb.getResult().equals("1")) {
                            setData();
                        } else {
                            Toast.makeText(StoryDetailsActivity.this, "该剧不存在或已删除", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(StoryDetailsActivity.this, "获取剧详情失败", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                    Toast.makeText(StoryDetailsActivity.this, "获取剧详情失败", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(StoryDetailsActivity.this, "获取剧详情失败", 1);
            }
        });
    }

    private void setData() {
        if (sdb == null) {
            return;
        }
        ImageLoader imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        imgIcon.setVipSize(12f);
        imgIcon.loadImage(sdb.getPortrait(), "0");
        adminId = sdb.getCreatorid();
        if (!TextUtils.isEmpty(adminId))
            Is_Admin = adminId.equals(IMLoginManager.instance().getLoginId() + "");
        //判断是否是剧的成员
        if (TextUtils.isEmpty(adminId)) {
            Toast.makeText(getApplicationContext(), "该剧不存在或已删除或阅读权限已关闭", Toast.LENGTH_LONG).show();
            return;
        }
        if (sdb.getIsCollectedByUser().equals("1")) {
            isCollectTemp = isCollect = true;
        }

        tvName.setText(sdb.getCreatorname());

        if (!TextUtils.isEmpty(sdb.getCreatetime()) && !sdb.getCreatetime().equals("")) {
            tvTime.setText(DateUtil.getDateWithOutYear(Integer.parseInt(sdb.getCreatetime())));
        }
        if (!TextUtils.isEmpty(sdb.getIspraisedbyuser()))
            behaviorInint = behavior = Integer.valueOf(sdb.getIspraisedbyuser());
        if (sdb.getIspraisedbyuser().equals("1")) {
            imgCollection.setSelected(true);
            hasPraise = true;
        } else {
            imgCollection.setSelected(false);
            hasPraise = false;
        }

        tvCellection.setText(sdb.getPraisenum());
        imageLoader.displayImage(sdb.getStoryimg() + SysConstant.W900, imgBg);
        tvTitle.setText(sdb.getTitle());
        if (!TextUtils.isEmpty(sdb.getUpdatetime())) {
            tvUpdateTime.setText("创建时间： " + DateUtil.returnDateWithDayAndTime(Integer.parseInt(sdb.getCreatetime())));
        }

        if (!TextUtils.isEmpty(sdb.getWordcount())) {
            float numWord = Float.parseFloat(sdb.getWordcount());
            if (numWord >= 10000) {
                float n = numWord / 10000;
                tvWordsNum.setText("字数：" + String.format("%.1f", n) + "w");

            } else if (numWord >= 1000) {
                float n = numWord / 1000;
                tvWordsNum.setText("字数：" + String.format("%.1f", n) + "k");
            } else {
                tvWordsNum.setText("字数：" + (int) numWord);
            }
        }
        setFragmentData();
        tvClass.setText("类别：" + sdb.getCategory());
        /******************/
        img.setUrl(sdb.getStoryimg());
        list_mess.add(img);
        saveImagePopupWindow.setList(list_mess);
    }
/*设置4个展示界面的按钮*/
    private void setFragmentData() {
        if (sdb == null) {
            return;
        }
        if (!TextUtils.isEmpty(curTag)) {
            Select1Fragment select1Fragment = null;
            Select2Fragment select2Fragment = null;
            switch (curTag) {
                case "1": //简介
                    select1Fragment = (Select1Fragment) getSupportFragmentManager().findFragmentByTag(curTag);
                    if (select1Fragment == null || sdb.getIntro() == null)
                        return;
                    select1Fragment.setData(sdb.getIntro());
                    break;
                case "2": //主线剧情
                    select2Fragment = (Select2Fragment) getSupportFragmentManager().findFragmentByTag(curTag);
                    select2Fragment.setData(curTag, sdb);
                    break;
                case "3": //主线人物
                    select2Fragment = (Select2Fragment) getSupportFragmentManager().findFragmentByTag(curTag);
                    select2Fragment.setData(curTag, sdb);
                    break;
                case "4": //规则
                    select1Fragment = (Select1Fragment) getSupportFragmentManager().findFragmentByTag(curTag);
                    if(select1Fragment != null) {
                        select1Fragment.setData(sdb.getRule());
                    }
                    break;
            }
        }
    }

    private String curTag;

    private void changeFragment(String tag, Class<? extends Fragment> clzz) {
        if (null != curTag && curTag.equals(tag)) {
            return;
        }

        FragmentManager fm = getSupportFragmentManager();
        Fragment f = fm.findFragmentByTag(tag);
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (null == f) {
            try {
                if (!TextUtils.isEmpty(curTag)) {
                    Fragment curF = fm.findFragmentByTag(curTag);
                    if (null != curF) {
                        ft.hide(curF);
                    }
                }
                curTag = tag;
                f = clzz.newInstance();
                setData(f);
                ft.add(R.id.frame_content, f, tag);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } else {
            Fragment curF = fm.findFragmentByTag(curTag);
            if (null != curF) {
                curTag = tag;
                setData(f);
                ft.hide(curF);
                ft.show(f);
            }
        }
        ft.commit();
        setTextSelector(curTag);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                setFragmentData();
            }
        }, 300);
    }

    private void setTextSelector(String tag) {
        if (TextUtils.isEmpty(tag))
            return;
        for (int i = 0; i < list_text.size(); i++) {
            list_text.get(i).setSelected(false);
        }
        if (tag.equals("1")) {
            list_text.get(0).setSelected(true);
        } else if (tag.equals("2")) {
            list_text.get(1).setSelected(true);
        } else if (tag.equals("3")) {
            list_text.get(2).setSelected(true);
        } else if (tag.equals("4")) {
            list_text.get(3).setSelected(true);
        }
    }

    private void setData(Fragment fragment) {
        if (fragment == null || sdb == null) {
            return;
        }
        switch (curTag) {
            case "1": //简介
                ((Select1Fragment) fragment).setData(sdb.getIntro());
                break;
            case "2": //主线剧情
                ((Select2Fragment) fragment).setData(curTag, sdb);
                break;
            case "3": //主线人物
                ((Select2Fragment) fragment).setData(curTag, sdb);
                break;
            case "4": //规则
                ((Select1Fragment) fragment).setData(sdb.getRule());
                break;
        }
    }


    @Override
    public void onClick(View view) {
        if (sdb == null) {
            return;
        }
        switch (view.getId()) {
            case R.id.tv_select_1:  //简介
                changeFragment((String) tvSelect1.getTag(), Select1Fragment.class);
                break;
            case R.id.tv_select_2:  //主线剧情
                changeFragment((String) tvSelect2.getTag(), Select2Fragment.class);
                break;
            case R.id.tv_select_3: //主线人物
                changeFragment((String) tvSelect3.getTag(), Select2Fragment.class);
                break;
            case R.id.tv_select_4: //规则
                changeFragment((String) tvSelect4.getTag(), Select1Fragment.class);
                break;
            case R.id.img_collection:
            case R.id.tv_cellection: //收藏
                if (hasPraise) {
                    behavior = 0;
                } else {
                    behavior = 1;
                }
                requestPraise();
                break;
            case R.id.img_more: //更多
                if (imgMore.isSelected()) {
                    imgMore.setSelected(false);
                } else {
                    imgMore.setSelected(true);
                }
                pop.show(imgMore, sdb);
                break;
            case R.id.img_shenqin: //申请 -->>>> 貌似现在已经被砍掉
                String groupId = "";                                //剧对应的审核群的id，这里申请剧其实是申请剧的审核群

                if (Integer.valueOf(adminId) == IMLoginManager.instance().getLoginId()) {
                    new GGDialog().showOneSelectDialog(StoryDetailsActivity.this, "您已经是该剧成员");
                    return;
                }
                /** 遍历所有群选择出审核群的id */
                for (int i = 0; i < sdb.getGroups().size(); i++) {
                    String isPlay = sdb.getGroups().get(i).getIsplay();
                    if (isPlay.equals("0")) {
                        groupId = sdb.getGroups().get(i).getGroupid();
                    }
                }
                /**  申请加入剧 */
                Set<Integer> memberIds = new HashSet<>();
                if (groupId.equals("")) {
                    return;
                }
                imService.getMessageManager().sendMsgGroupAddMemberReq(Integer.valueOf(adminId), Integer.valueOf(groupId), memberIds);
                T.showShort(StoryDetailsActivity.this, "申请成功");
                //只让用户点击一次
                imgShenqin.setClickable(false);
                break;
            case R.id.img_read: //阅读
                if (TextUtils.isEmpty(sdb.getReadautority()) || sdb.getReadautority().equals("0")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(StoryDetailsActivity.this);
                    builder.setTitle("提示");
                    builder.setMessage("哎哟，剧的主人没有开通阅读权限呢！");
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
                    return;
                } else {
                    Intent i = new Intent(this, ReadActivity.class);
                    i.putExtra(IntentConstant.STORY_ID, storyid);
                    startActivity(i);
                }
                break;
            case R.id.img_chat: //聊天
                IMUIHelper.openSquareCommentActivity(StoryDetailsActivity.this, sdb.getTitle(), sdb.getId(), IntentConstant.FLOOR_TYPE_STORY);
                break;
            case R.id.left_btn:
                finish();
                break;
            case R.id.img_icon:     //用户头像
                IMUIHelper.openUserHomePageActivity(this, sdb.getCreatorid());
                break;
            case R.id.img_jinqun:   //进群
                if (sdb.getGroups() != null && sdb.getGroups().size() > 0) {
                    IMUIHelper.openStoryGroupSelectActivity(StoryDetailsActivity.this,
                            storyid, Is_Admin, sdb.getGroups().get(0).getGroupid(), sdb.getGroups().get(0).getTitle());
                }
                break;
            case R.id.img_bg:       //剧封面
                saveImagePopupWindow.showPop(imgBg, img);
                break;
        }
    }

    private int gId;

    /**
     * 检查是否是成员，如果不是，需要先添加到群里。
     *
     * @param groupEntity groupEntity
     */
    private void checkExistAndStartChat(GroupEntity groupEntity) {
        String userList = groupEntity.getUserList();
        String userId = IMLoginManager.instance().getLoginId() + "";
        if (userList.contains(userId)) {
            startCommentGroupChat();
        } else {
            Set<Integer> set = new HashSet<>();
            set.add(IMLoginManager.instance().getLoginId());
            IMGroupManager.instance().reqAddGroupMember(gId, set);
        }
    }

    /**
     * 开始聊天。
     */
    private void startCommentGroupChat() {
        startFlg = START_FLG_NO_SET;
        String sessionKey, portrait, title, creatorName, createTime, type;
        sessionKey = "2_" + gId;
        portrait = sdb.getPortrait();
        title = sdb.getTitle();
        createTime = sdb.getCreatetime();
        creatorName = sdb.getCreatorname();
        type = IntentConstant.STORY;
        IMUIHelper.openTopicMessageActivity(this, type, sessionKey, portrait, title, creatorName, createTime);
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {

            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                startFlg = START_FLG_NO_SET;
//                T.showShort(this, "暂时不能评论");
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
                Log.d("storyDeatilsActivity", "CHANGE_GROUP_MEMBER_SUCCESS is called");
                break;
            }
            //向群里添加人员的时候，最后也会获取群的信息。因此不需要处理变更人员的消息。
            case GROUP_INFO_UPDATED: {
                //不是在点击状态下，忽略更新事件。
                if (startFlg != START_FLG_SET) {
                    break;
                }
                GroupEntity groupEntity = event.getGroupEntity();
                //如果是当前评论群的信息的变化
                if (groupEntity.getPeerId() == gId) {
                    checkExistAndStartChat(groupEntity);
                }
            }
            break;
        }
    }

    private int behavior;       //记录点赞或喜欢的状态, 为用户操作后的值,界面退出时比对 不同时则去刷新个人主页
    private int behaviorInint;  //同上,是第一次获取的值,不会改变

    private void requestPraise() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyid);
        params.addParams("behavior", behavior + "");
        OkHttpClientManager.postAsy(ConstantValues.STORY_PRAISE, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    String result = jsonObject.getString("result");
                    if ("1".equals(result)) { //成功
                        String num = "0";
                        if (!hasPraise) { //点赞
                            hasPraise = true;
                            tvCellection.setText(num = (Integer.parseInt(
                                    tvCellection.getText().toString()) + 1) + "");
                            imgCollection.setSelected(true);
                            sdb.setPraisenum(num);
                            sdb.setIspraisedbyuser("1");
                        } else {
                            hasPraise = false;
                            num = (Integer.parseInt(tvCellection.getText().toString()) - 1) + "";
                            tvCellection.setText(num);
                            imgCollection.setSelected(false);
                            sdb.setPraisenum(num);
                            sdb.setIspraisedbyuser("0");
                        }
                    } else {
                        if (behavior == 1) {
                            T.showShort(StoryDetailsActivity.this, "点赞失败");
                        } else {
                            T.showShort(StoryDetailsActivity.this, "取消失败");
                        }
                    }
                } catch (JSONException e) {
                    T.showShort(StoryDetailsActivity.this, "服务器错误");
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.showShort(StoryDetailsActivity.this, "服务器错误");
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        imServiceConnector.unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imServiceConnector.connect(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (data != null && data.getBooleanExtra(IntentConstant.STORY_DETAIL_NEED_REFRESH, false)) {
                if (sdb != null) {
                    if ("1".equals(sdb.getReadautority())) {
                        sdb.setReadautority("0");
                    } else {
                        sdb.setReadautority("1");
                    }
                }
            }
        }
    }
}
