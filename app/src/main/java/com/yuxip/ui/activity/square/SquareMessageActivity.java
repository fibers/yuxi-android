package com.yuxip.ui.activity.square;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.album.SquareMessageAdapter;

/**
 * Created by ZQF on 2015/10/13.'
 * 广场通知
 */
public class SquareMessageActivity extends TTBaseActivity {

    private Context mContext;
    
    private SwipeRefreshLayout mRefreshLayout;
    private ListView mListView;
    private SquareMessageAdapter mAdapter;
    private View mFootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        mContext = this;
        LayoutInflater.from(mContext).inflate(R.layout.activity_square_message, topContentView);
        initView();
        initData();
        refresh();
    }
    
    private void initView() {
        setTitle("广场通知");
        setLeftButton(R.drawable.back_default_btn);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mListView = (ListView) findViewById(R.id.listView);
        mRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        // TODO "null"要替换成真实数据集
        mAdapter = new SquareMessageAdapter(mContext, null);
    }

    private void initData() {
        mListView.addFooterView(mFootView = View.inflate(mContext, R.layout.footer_view, null));
        mListView.setAdapter(mAdapter);
        mRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //重置各种初始参数
//                        resetFromId();
                        Log.d("Refresh", "on refresh listener");
                        refresh();
                    }
                });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
//                if (squarelist.isEmpty()) {
                if (true) {
                    mRefreshLayout.setRefreshing(true);
                }
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount + firstVisibleItem == totalItemCount) {
                    load();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            refresh();
        }
    }

    private void refresh() {
//        direction = 0;
        ReqSquareData();
        Log.d("Refresh", "refresh function");
    }

    private void load() {
        if (!mRefreshLayout.isRefreshing()) {
//            fromid = minTopicId();
//            boundaryid = maxTopicId();
//            direction = 1;
//            if (fromid == fromidTemp) {
//                return;
//            }
//            fromidTemp = fromid;
//            ReqSquareData();
            Log.d("Load", "load function");
        }
    }

    /**
     * 获取广场
     */
    private void ReqSquareData() {
        /** 放在params里面传递 */
//        RequestParams params = new RequestParams();
//        params.addBodyParameter("uid", uid);
//        params.addBodyParameter("fromid", fromid + "");
//        params.addBodyParameter("boundaryid", boundaryid + "");
//        params.addBodyParameter("direction", direction + "");
//        params.addBodyParameter("count", "10");
//        new HttpUtils().send(HttpRequest.HttpMethod.POST, ConstantValues.GetTopics, params,
//                new RequestCallBack<String>() {
//                    /**
//                     * 请求成功后返回的Json
//                     * @param responseInfo
//                     */
//                    @Override
//                    public void onSuccess(ResponseInfo<String> responseInfo) {
//                        swipeRefreshLayout.setRefreshing(false);
//                        onReceiveSquareResult(responseInfo.result);
//                    }
//
//                    @Override
//                    public void onFailure(HttpException error, String msg) {
//                        swipeRefreshLayout.setRefreshing(false);
//                    }
//                });

        // 假装在进行网络请求
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1000);
                    SquareMessageActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mRefreshLayout.setRefreshing(false);
                            if (null != mFootView) {
                                mFootView.setVisibility(View.GONE);
                            }
                            Toast.makeText(mContext, "请求成功!!!!~~~~~", Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}
