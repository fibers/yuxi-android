package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.im.Security;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/6/4.
 */
public class SettingPwdActivity extends TTBaseActivity {
    private View curView;
    @InjectView(R.id.settingpwdView)
    EditText settingpwdView;
    @InjectView(R.id.progress_bar)
    ProgressBar progressBar;
    private String settingpwdStr;
    private String mobileno;
    private Logger logger = Logger.getLogger(SettingPwdActivity.class);
    private IMService imService;
    private boolean loginSuccess = false;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(SettingPwdActivity.this);
        EventBus.getDefault().register(this);
        curView = View.inflate(SettingPwdActivity.this, R.layout.mb_activity_setting_pwd, topContentView);
        mobileno = getIntent().getStringExtra("mobileno");
        ButterKnife.inject(this, topContentView);
        setTitle("设置密码");
        setLeftButton(R.drawable.back_default_btn);
        curView.findViewById(R.id.submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                settingpwdStr = settingpwdView.getText().toString().trim();
                if (TextUtils.isEmpty(settingpwdStr)) {
                    showToast("输入不能为空");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    curView.setClickable(false);
                    settingPwd();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(SettingPwdActivity.this);
    }

    /**
     * 设置密码
     */
    private void settingPwd() {
        /**
         * 放在params里面传递
         */
        String newPwd = new String(Security.getInstance().EncryptPass(settingpwdStr));
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("rid", "mb:" + mobileno);
        params.addParams("password", newPwd);
        OkHttpClientManager.postAsy(ConstantValues.ChangePass, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        showToast("设置新密码成功");
                        attemptLogin("mb:" + mobileno, settingpwdStr);
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                progressBar.setVisibility(View.GONE);
                curView.setClickable(true);
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * QQ注册成功登陆
     */
    private void attemptLogin(String loginName, String mPassword) {
        if (imService != null) {
            //im服务 获取登陆管理  然后进行登陆操作
            imService.getLoginManager().login(loginName, mPassword);
        }
    }

    /**
     * ----------------------------event 事件驱动----------------------------
     */
    public void onEventMainThread(LoginEvent event) {
        progressBar.setVisibility(View.GONE);
        curView.setClickable(true);
        switch (event) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGIN_OK:
                //登陆成功后
                onLoginSuccess();

                break;
            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED:
                if (!loginSuccess)
                    onLoginFailure(event);
                break;
        }
    }


    public void onEventMainThread(SocketEvent event) {
        switch (event) {
            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                if (!loginSuccess)
                    onSocketFailure(event);
                break;
        }
    }

    /**
     * 登陆成功后的操作
     */
    private void onLoginSuccess() {
        logger.i("login#onLoginSuccess");
        loginSuccess = true;
        int loginId = imService.getLoginManager().getLoginId();

        Intent intent = new Intent(SettingPwdActivity.this, MainActivity.class);
        startActivity(intent);
        SettingPwdActivity.this.finish();
    }

    //登陆失败后
    private void onLoginFailure(LoginEvent event) {
        logger.e("login#onLoginError -> errorCode:%s", event.name());
        //showLoginPage();
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip, Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        logger.e("login#onLoginError -> errorCode:%s,", event.name());
        //showLoginPage();
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "2", Toast.LENGTH_SHORT).show();
    }
}
