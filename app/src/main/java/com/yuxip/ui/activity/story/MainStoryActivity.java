package com.yuxip.ui.activity.story;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.StoryScenesEntity;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * changed by SummerRC on 2015/4/21.
 */
public class MainStoryActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(MainStoryActivity.class);

    private ListView listView;
    private String createId;
    private String storyid;
    private boolean IS_ADMIN;
    private ArrayList<StoryScenesEntity> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }

        LayoutInflater.from(this).inflate(R.layout.activity_story_main, topContentView);

        storyid = getIntent().getStringExtra("storyid");
        createId = getIntent().getStringExtra("creatorid");

        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("主线剧情");

        listView = (ListView) findViewById(R.id.listView);

        IS_ADMIN = getIntent().getBooleanExtra("IS_ADMIN", false);
        if (IS_ADMIN) {
            setRighTitleText("添加");
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainStoryActivity.this, MainStoryAddActivity.class);
                    intent.putExtra("storyid", storyid);
                    intent.putExtra("IS_ADMIN", true);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
        GetStoryScenes();
    }

    /**
     * 得到主线剧情
     */
    private void GetStoryScenes() {
        if(TextUtils.isEmpty(createId)||TextUtils.isEmpty(storyid)){
            return;
        }
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", createId);
        params.addParams("storyid", storyid);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryScenes, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                list.clear();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {

                        JSONArray scenes = object.getJSONArray("scenes");
                        for (int i = 0; i < scenes.length(); i++) {
                            JSONObject obj = scenes.getJSONObject(i);
                            StoryScenesEntity scenesEntity = new StoryScenesEntity();
                            scenesEntity.setId(obj.get("id").toString() + "");
                            scenesEntity.setTitle(obj.get("title").toString() + "");
                            scenesEntity.setContent(obj.get("content").toString() + "");
                            list.add(scenesEntity);
                        }
                    }
                    MainStoryAdapter mAdapter = new MainStoryAdapter(MainStoryActivity.this, list);
                    listView.setAdapter(mAdapter);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    class MainStoryAdapter extends BaseAdapter {
        ArrayList<StoryScenesEntity> mlist;
        Context context;
        boolean IS_NULL = false;

        public MainStoryAdapter(Context context, ArrayList<StoryScenesEntity> list) {
            this.context = context;
            mlist = list;
        }

        @Override
        public int getCount() {
            if (mlist.size() == 0 || mlist == null) {
                IS_NULL = true;
                return 1;
            } else {
                return mlist.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView = View.inflate(context, R.layout.mian_story_item, null);
            TextView tv_main_story_name = (TextView) convertView.findViewById(R.id.tv_main_story_name);
            if (IS_NULL) {
                tv_main_story_name.setText("暂无剧情");
                convertView.findViewById(R.id.iv_main_story_name_btm).setVisibility(View.GONE);
            } else {
                final String title = mlist.get(position).getTitle();
                if (TextUtils.isEmpty(title)) {
                    tv_main_story_name.setText("没有剧情");
                }
                tv_main_story_name.setText(mlist.get(position).getContent());
                convertView.findViewById(R.id.rl_btm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainStoryActivity.this, MainStoryAddActivity.class);
                        intent.putExtra("name", title);
                        intent.putExtra("content", mlist.get(position).getContent());
                        intent.putExtra("storyid", storyid);
                        intent.putExtra("sceneid", list.get(position).getId());
                        intent.putExtra("modify", true);
                        intent.putExtra("IS_ADMIN", IS_ADMIN);
                        startActivity(intent);
                    }
                });
            }
            return convertView;
        }
    }

}
