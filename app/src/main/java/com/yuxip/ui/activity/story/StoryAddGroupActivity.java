package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.GlobalVariable;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * add by SummerRC on 2015/6/1.
 * 添加剧群
 */
public class StoryAddGroupActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryAddGroupActivity.class);

    private IMService imService;
    private EditText et_name;
    private EditText et_info;
    private String is_play = "0";
    private enum GroupType {
        GROUP_TYPE_DUIXI, GROUP_TYPE_SHENHE
    }
    private GroupType type = GroupType.GROUP_TYPE_SHENHE;
    private String storyId;
    private String groupName;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    private static int GROUP_EVENT_NUM = 0;             //有时EventBus会发送两次建群成功的通知

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        // 在这个地方加可能会有问题吧
        EventBus.getDefault().register(this);
        imServiceConnector.connect(this);

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        LayoutInflater.from(this).inflate(R.layout.activity_story_add_group, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(et_info.getText().toString()) || TextUtils.isEmpty(et_name.getText().toString())) {
                    Toast.makeText(StoryAddGroupActivity.this, "内容不能为空！", Toast.LENGTH_SHORT).show();
                    return;
                }
                /** 创建群 */
                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_ADD_STORY_GROUP;
                createGroup();
            }
        });
        setTitle("生成剧群");
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initView() {
        et_name = (EditText) findViewById(R.id.et_name);
        et_info = (EditText) findViewById(R.id.et_info);
        CheckBox  checkBtn = (CheckBox) findViewById(R.id.checkBtn);
        checkBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    type = GroupType.GROUP_TYPE_DUIXI;
                    is_play = "1";
                }else {
                    type = GroupType.GROUP_TYPE_SHENHE;
                    is_play = "0";
                }
            }
        });
//        mToggleButton = (ToggleButton) findViewById(R.id.mToggleButton);
//        mToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                mToggleButton.setChecked(isChecked);
//                if (isChecked) {
//                    type = GroupType.GROUP_TYPE_DUIXI;
//                    is_play = "1";
//                } else {
//                    type = GroupType.GROUP_TYPE_SHENHE;
//                    is_play = "0";
//                }
//            }
//
//        });
    }

    /**
     * 创建群
     */
    private void createGroup() {
        Set<Integer> ListSet = new HashSet<>();
        IMGroupManager groupMgr = imService.getGroupManager();
        int loginId = imService.getLoginManager().getLoginId();
        ListSet.add(loginId);
        switch (type) {
            case GROUP_TYPE_DUIXI:
                groupName = et_name.getText().toString() + "(对戏群)";
                break;
            case GROUP_TYPE_SHENHE:
                /***
                 * 生成不带字的群
                 */
                groupName = et_name.getText().toString();
                break;
        }
        groupMgr.reqCreateTempGroup(groupName, ListSet);
    }

    /**
     * 处理群创建成功、失败事件
     * @param event
     */
    public void onEventMainThread(GroupEvent event){
        if(GROUP_EVENT_NUM != 0) {
            return;
        }
        /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
        if(GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_ADD_STORY_GROUP) {
            return;
        }
        int groupId = event.getGroupEntity().getPeerId();
        switch (event.getEvent()){
            case CREATE_GROUP_OK:
                handleCreateGroupSuccess(String.valueOf(groupId));
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                handleCreateGroupFail();
                break;
        }
    }

    /**
     * 处理群创建成功事件
     */
    private void handleCreateGroupSuccess(String groupId) {
        GROUP_EVENT_NUM++;
        Toast.makeText(this, "剧群创建成功！", Toast.LENGTH_SHORT).show();
        addGroupToStory(groupId);
    }

    /**
     * 处理群创建失败事件
     */
    private void handleCreateGroupFail() {
        Toast.makeText(this, getString(R.string.create_temp_group_failed), Toast.LENGTH_SHORT).show();
    }

    /**
     *关联剧和群
     */
    private void addGroupToStory(String groupId) {
        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);            //剧管理员id(能进入这个界面的用户就是管理员）
        params.addParams("storyid", storyId);    //剧id
        params.addParams("title", groupName);
        params.addParams("intro", et_info.getText().toString());

        params.addParams("groupid", groupId);
        params.addParams("isplay", is_play);     //0: 非戏群   1: 戏群
        params.addParams("token", "1");

        OkHttpClientManager.postAsy(ConstantValues.AddGroupToStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        finish();
                    } else {
                        showToast("创建失败");
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    @Override
    protected void onDestroy() {
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        GROUP_EVENT_NUM = 0;
        super.onDestroy();
    }
}
