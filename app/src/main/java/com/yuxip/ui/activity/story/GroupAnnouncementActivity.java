package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.GroupAnnouncementEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by SummerRC on 2015/9/4.
 * description：管理员添加修改群公告以及群成员查看群公告
 */
public class GroupAnnouncementActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(GroupAnnouncementActivity.class);

    @InjectView(R.id.tv_title)
    TextView tv_title;

    @InjectView(R.id.tv_name)
    TextView tv_name;

    @InjectView(R.id.tv_date)
    TextView tv_date;

    @InjectView(R.id.tv_time)
    TextView tv_time;

    @InjectView(R.id.tv_content)
    TextView tv_content;


    private String groupId;
    private StoryParentEntity storyParentEntity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View.inflate(this, R.layout.activity_group_announcement, topContentView);
        ButterKnife.inject(this, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        storyParentEntity = YXStoryManager.instance().getStoryByGroupid(Integer.parseInt(groupId));
        initView();
    }

    private void initView() {
        setTitle("群公告");
        setLeftButton(R.drawable.back_default_btn);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if(storyParentEntity == null) {
            tv_name.setText("大帅比");
        } else  {
            tv_name.setText(storyParentEntity.getCreatorname());
        }
        getStoryGroupBoard();
    }


    /**
     * 网络获取群公告
     */
    private void getStoryGroupBoard() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        //** 放在params里面传递 *//*
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("groupid", groupId);
        params.addParams("latesttime", "0");     //获取最新的公告
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupBoard, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        JSONObject contentObject = object.getJSONArray("boardinfo").getJSONObject(0);
                        if (TextUtils.isEmpty(contentObject.getString("title"))) {
                            T.show(getApplicationContext(), "暂无公告！", 0);
                        } else {
                            tv_title.setText(contentObject.getString("title"));
                            tv_content.setText(contentObject.getString("content"));
                            SimpleDateFormat date = new SimpleDateFormat("yyyy年M月d日 h:mm");
                            String time = date.format(Long.valueOf(object.getString("datetime") + "000"));
                            tv_date.setText(time.split("日")[0] + "日");
                            tv_time.setText(time.split("日")[1]);
                            saveAnnouncement(object.getString("datetime"));
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }


    /**
     * 保存公共到本地数据库
     *
     * @param dateTime 公共的时间戳
     */
    public void saveAnnouncement(String dateTime) {
        GroupAnnouncementEntity entity = new GroupAnnouncementEntity();
        entity.setGroupId(groupId);
        entity.setTitle(tv_title.getText().toString());
        entity.setContent(tv_content.getText().toString());
        entity.setDateTime(dateTime);
        DBInterface dbInterface = DBInterface.instance();
        assert dbInterface != null;
        dbInterface.insertOrUpdateGroupAnnouncementEntity(entity);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
