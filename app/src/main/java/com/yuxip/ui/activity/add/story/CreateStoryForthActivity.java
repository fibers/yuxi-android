package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.JsonBean.RoleTypeSettingEntity;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.ui.helper.KeyBoardObserver;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.List;

/**
 * Created by Ly on 2015/11/2.
 * 创建剧的第四个流程 角色类型
 */
public class CreateStoryForthActivity extends Activity implements View.OnClickListener, DialogHelper.DialogListener, AddRoleView.AddRoleViewCallBack {
    private TextView tvNextStep;
    private AddRoleView addRoleView;
    private CustomDividerProgressbar progressbar;
    private RelativeLayout rlbottom;
    private View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView  = LayoutInflater.from(this).inflate(R.layout.activity_create_story_forth,null);
        setContentView(rootView);
        initRes();
        checkManagerData();
    }

    private void initRes() {
        ImageView ivBack = (ImageView) findViewById(R.id.iv_back_create_story_forth);
        tvNextStep = (TextView) findViewById(R.id.tv_next_step_create_story_forth);
        addRoleView = (AddRoleView) findViewById(R.id.addroleview_create_story_forth);
        TextView tvAddRole = (TextView) findViewById(R.id.tv_add_create_story_forth);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_forth);
        rlbottom = (RelativeLayout) findViewById(R.id.rl_bottom_create_story_forth);
        addRoleView.setAddRoleViewCallBack(this);
        ivBack.setOnClickListener(this);
        tvAddRole.setOnClickListener(this);
        tvNextStep.setOnClickListener(this);
        tvNextStep.setSelected(false);
        progressbar.setCurProgress((int) Math.ceil(100 * 4 / 6));
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
        KeyBoardObserver keyBoardObserver = new KeyBoardObserver();
        keyBoardObserver.setBaseRootAndView(rootView,rlbottom);

    }


    private void checkManagerData() {
        List<RoleTypeSettingEntity> list = CreateStoryManager.getInstance().getListRoleTypes();
        if (list == null || list.size() == 0) {
            addRoleView.addNewHintRole();
            addRoleView.addNewHintRole();
        } else {
            for (int i = 0; i < list.size(); i++) {
                addRoleView.addNewRole(list.get(i));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_create_story_forth:           //返回
                openPreAcitivty();
                finish();
                break;
            case R.id.tv_add_create_story_forth:            //添加
                DialogHelper.showAddItemDialog(this, getResources().getString(R.string.story_add), getResources().getString(R.string.story_add_role_type), "", 8, this);
                break;
            case R.id.tv_next_step_create_story_forth:      //下一步
                if (!tvNextStep.isSelected()) {
                    T.show(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), 0);
                    return;
                }
                if (addRoleView.isAllItemInflate()) {
                    saveManageData();
                    IMUIHelper.openCreateStoryOrderActivity(this,5,null);
                    finish();
                }
                break;
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            openPreAcitivty();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }


    private void saveManageData() {
        CreateStoryManager.getInstance().setListRoleTypes(addRoleView.getAllChildData());
    }

    /**
     * 这是添加 对话框 的回调
     *
     * @param dialogContent dialogContent
     */
    @Override
    public void addNewItem(String dialogContent) {
        if(!TextUtils.isEmpty(dialogContent)&&!TextUtils.isEmpty(dialogContent.trim())){
            addRoleView.addNewRole(dialogContent);
            tvNextStep.setSelected(true);
        }
    }

    /**
     * addRoleView 的回调 当删除所有子项时调用
     */
    @Override
    public void noDataExists() {
        tvNextStep.setSelected(false);
    }

    @Override
    public void isDataInflate(boolean isInflate) {
        if(isInflate){
            tvNextStep.setSelected(true);
        }else{
            tvNextStep.setSelected(false);
        }
    }


    private void openPreAcitivty(){
        saveManageData();
        IMUIHelper.openCreateStoryOrderActivity(this, 3, IntentConstant.ACTIVITY_OPEN_TYPE_LEFT);
    }
}
