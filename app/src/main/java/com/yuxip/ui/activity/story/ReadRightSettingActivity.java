package com.yuxip.ui.activity.story;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 *  edit by SummerRC on 2015/8/12.
 *  description : 阅读权限设置
 */
public class ReadRightSettingActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(ReadRightSettingActivity.class);

    private String storyId;
    private String permission;        //阅读权限： 1开0关

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_read_right, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("阅读权限设置");

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        permission = getIntent().getStringExtra(IntentConstant.PERMISSION);
        CheckBox openCheckBox = (CheckBox)findViewById(R.id.openCheckBox);
        openCheckBox.setChecked("1".equals(permission));
        openCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    permission = "1";
                    SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), storyId, true);
                } else {
                    permission = "0";
                    SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), storyId, false);
                }
                changePermission(permission);
            }
        });
    }

    /***
     * 修改阅读权限
     */
    private void changePermission(final String permission) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");      //能进来的都是管理员
        params.addParams("storyid", storyId);
        params.addParams("permission", permission);
        params.addParams("token", "1");

        OkHttpClientManager.postAsy(ConstantValues.SetGroupPerssion, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent();
                        intent.putExtra(IntentConstant.READ_RIGHT, permission);
                        setResult(RESULT_OK, intent);
                        ReadRightSettingActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
                finish();
            }
        });
    }

}
