package com.yuxip.ui.activity.chat;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by SummerRC on 2015/8/12.
 * description : 剧群成员的群名片
 */
public class GroupCardActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(GroupCardActivity.class);

    @InjectView(R.id.iv_home_head_img)
    CircularImage iv_home_head_img;
    @InjectView(R.id.tv_person_setting)
    TextView tv_person_setting;
    @InjectView(R.id.tv_name)
    TextView tv_name;
    @InjectView(R.id.tv_go_home)
    TextView tv_go_home;

    @InjectView(R.id.ll_container)
    LinearLayout ll_container;

    private String uid;
    private String storyId;
    private String portrait;
    private String groupId;
    private Set<Integer> set;

    private IMService imService;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card, topContentView);
        ButterKnife.inject(this, view);
        imServiceConnector.connect(this);
        initExtras();
        initView();
    }

    private void initExtras() {
        uid = getIntent().getStringExtra(IntentConstant.UID);
        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        portrait = getIntent().getStringExtra(IntentConstant.PORTRAIT);
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        String creatorId = getIntent().getStringExtra(IntentConstant.CREATOR_ID);
        View item;
        if (!uid.equals(IMLoginManager.instance().getLoginId() + "")) {
            if (creatorId.equals(IMLoginManager.instance().getLoginId() + "")) {       //是不是管理员
                if (isMember()) {
                    if (isFriend()) {
                        item = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card_bottom_admin_one_delete, ll_container);
                        item.findViewById(R.id.bt_one).setOnClickListener(this);
                    } else {
                        item = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card_bottom_admin_two_delete_and_apply, ll_container);
                        item.findViewById(R.id.bt_one).setOnClickListener(this);
                        item.findViewById(R.id.bt_three).setOnClickListener(this);
                    }
                } else {
                    if (isFriend()) {
                        item = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card_bottom_admin_two_delete_and_move, ll_container);
                        item.findViewById(R.id.bt_one).setOnClickListener(this);
                        item.findViewById(R.id.bt_two).setOnClickListener(this);
                    } else {
                        item = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card_bottom_admin_three, ll_container);
                        item.findViewById(R.id.bt_one).setOnClickListener(this);
                        item.findViewById(R.id.bt_two).setOnClickListener(this);
                        item.findViewById(R.id.bt_three).setOnClickListener(this);
                    }
                }
            } else {
                if (!isFriend()) {
                    item = LayoutInflater.from(this).inflate(R.layout.activity_his_group_card_bottom_member_one_apply, ll_container);
                    item.findViewById(R.id.bt_three).setOnClickListener(this);
                }
            }
        }
    }

    /**
     * @return 是否是好友
     */
    private boolean isFriend() {
        return FriendGroupManager.getInstance().isFriend(uid);
    }

    /**
     * @return 是否是剧的水聊或者对戏群成员
     */
    private boolean isMember() {
        List<Integer> typeList = new ArrayList<>();
        typeList.add(1);
        typeList.add(2);
        set = YXStoryManager.instance().getGroupIdsByStoryIdAndGroupTypes(Integer.valueOf(storyId), typeList);
        if (set != null && set.size() > 0) {
            for (Integer id : set) {
                List<UserEntity> list = IMGroupManager.instance().getGroupMembers(id);
                if (list != null && list.size() > 0)
                    for (UserEntity userEntity : list) {
                        if (userEntity.getId().equals(Long.valueOf(uid))) {
                            return true;
                        }
                    }
            }
        }
        return false;
    }

    private void initView() {
        topLeftBtn.setVisibility(View.VISIBLE);
        tv_person_setting.setMovementMethod(ScrollingMovementMethod.getInstance());
        setTitle("TA的名片");
        tv_go_home.setOnClickListener(this);
        Drawable drawable = DrawableCache.getInstance(getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
        DisplayImageOptions displayImageOptions = ImageLoaderUtil.getOptions(drawable);
        ImageLoaderUtil.getImageLoaderInstance().displayImage(portrait, iv_home_head_img, displayImageOptions);
        getUserRoleInfoInStory();
    }

    private void getUserRoleInfoInStory() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyId);
        params.addParams("token", "1");

        OkHttpClientManager.postAsy(ConstantValues.GetUserRoleInfoInStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        JSONObject object = obj.getJSONObject("roleinfo");
                        String roleTitle = object.getString("roletitle");           //角色
                        String userrolename = object.getString("userrolename");     //角色名字
                        tv_name.setText("[" + roleTitle + "] " + userrolename);
                        tv_person_setting.setText(object.getString("userroleinfo"));
                    } else {
                        Toast.makeText(GroupCardActivity.this, obj.getString("describe"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.reset(this);
        imServiceConnector.disconnect(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_go_home:
                IMUIHelper.openUserHomePageActivity(this, uid);
                break;
            case R.id.bt_one:       //移除剧群
                if(set == null) {
                    return;
                }
                Set<Integer> removeSet = new HashSet<>();
                removeSet.add(Integer.valueOf(uid));
                imService.getGroupManager().reqRemoveGroupMember(Integer.valueOf(groupId), removeSet);
                findViewById(R.id.bt_one).setClickable(false);
                T.show(this, "移除成功！！", 0);
                finish();
                break;
            case R.id.bt_two:       //移至水聊和对戏群
                Set<Integer> memberSet = new HashSet<>();
                memberSet.add(Integer.valueOf(uid));
                for (Integer curId : set) {
                    IMGroupManager.instance().reqAddGroupMember(curId, memberSet);
                }
                T.show(this, "移动成功！！", 0);
                findViewById(R.id.bt_two).setClickable(false);
                finish();
                break;
            case R.id.bt_three:     //申请好友
                DialogHelper.showAddFriendDialog(this, imService, Integer.valueOf(uid));
                findViewById(R.id.bt_three).setClickable(false);
                break;
        }
    }


}
