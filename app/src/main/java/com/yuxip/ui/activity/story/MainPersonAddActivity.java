package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * Created by SummerRC on 2015/5/25.
 * 添加主线角色
 */
public class MainPersonAddActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(MainPersonAddActivity.class);

    private EditText et_num;
    private EditText et_title;
    private EditText et_content;
    private String storyid;
    private String num;
    private String title;
    private String intro;
    private String request_url = "";

    private String roleId = "0";
    private boolean isModify;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_persion_main_add, topContentView);
        et_num = (EditText) findViewById(R.id.et_num);
        et_title = (EditText) findViewById(R.id.et_title);
        et_content = (EditText) findViewById(R.id.et_content);
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        roleId = getIntent().getStringExtra("roleid");
        num = getIntent().getStringExtra("num");
        title = getIntent().getStringExtra("title");
        intro = getIntent().getStringExtra("content");
        et_num.setText(num);
        et_title.setText(title);
        et_content.setText(intro);

        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("主线角色");

        /** 是管理员可以添加主线角色 */
        if (getIntent().getBooleanExtra("IS_ADMIN", false)) {
            /**判断是否是修改或是添加数据*/
            isModify = getIntent().getBooleanExtra("modify", false);
            if (!isModify) {
                request_url = ConstantValues.CreateStoryRole;
                setRighTitleText("添加");
            } else {
                request_url = ConstantValues.ModifyStoryRole;
                setRighTitleText("修改");

            }
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(et_num.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "尚未选择角色数量", Toast.LENGTH_SHORT).show();
                        return;
                    }
//                    if (TextUtils.isEmpty(et_content.getText().toString())) {
//                        Toast.makeText(getApplicationContext(), "内容尚未填写", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
                    if (TextUtils.isEmpty(et_title.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "尚未填写标题", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    addOrChangeMainPerson();
                }
            });
        } else {
            et_num.setEnabled(false);
            et_content.setEnabled(false);
            et_title.setEnabled(false);
        }
    }

    /**
     * 添加或修改主线角色
     */
    private void addOrChangeMainPerson() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");  //管理员id，能进入这里的id都是管理员
        params.addParams("storyid", storyid);
        params.addParams("intro", et_content.getText().toString().trim());
        params.addParams("title", et_title.getText().toString().trim());
        params.addParams("num", et_num.getText().toString().trim());
        params.addParams("token", "1");
        if (isModify)
            params.addParams("roleid", roleId);
        OkHttpClientManager.postAsy(request_url, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        if (!isModify) {
                            Toast.makeText(getApplicationContext(), "添加成功", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        if (!isModify) {
                            Toast.makeText(getApplicationContext(), "添加失败", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}