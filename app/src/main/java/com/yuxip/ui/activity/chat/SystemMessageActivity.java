package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.JsonBean.AddMessageBean;
import com.yuxip.JsonBean.GroupAddMessageBean;
import com.yuxip.JsonBean.MessageBean;
import com.yuxip.R;
import com.yuxip.config.DBConstant;
import com.yuxip.imservice.event.UnreadEvent;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.protobuf.helper.EntityChangeEngine;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.SystemMessageAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 系统消息
 * Created by HeTianpeng on 2015/07/06.
 */
public class SystemMessageActivity extends TTBaseActivity {
    @InjectView(R.id.ultimate_recyvler_view)
    UltimateRecyclerView ultimateRecyvlerView;
    private SystemMessageAdapter systemMessageAdapter;
    private List<MessageBean> messageBeans = new ArrayList<>();
    private IMService imService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View view = View.inflate(this, R.layout.activity_system_message, topContentView);
        ButterKnife.inject(this, view);
        setTitle(getString(R.string.sys_msg));
        setLeftButton(R.drawable.back_default_btn);

        imServiceConnector.connect(this);
        ultimateRecyvlerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
    }

    IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            DBInterface dbInterface = imService.getDbInterface();

            try {
                /** 获取未处理的好友请求 */
                List<ApplyFriendEntity> applyFriendEntityList = dbInterface.loadAllApplyFriendEntity();
                if (null != applyFriendEntityList && !applyFriendEntityList.isEmpty()) {
                    for (ApplyFriendEntity ae : applyFriendEntityList) {
                        AddMessageBean amb = new AddMessageBean();
                        amb.setiMessageTypeDetail(MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_FRIEDNS_ADD);
                        amb.setName(ae.getNickname());
                        if(TextUtils.isEmpty(ae.getMsgdata())) {
                            amb.setMessage(ae.getNickname() + "申请添加你为好友");
                        } else {
                            amb.setMessage(ae.getNickname() + "申请添加你为好友"+"【附加消息：" + ae.getMsgdata() +"】");
                        }
                        amb.setIconUrl(ae.getPortrait());
                        amb.setApplyId(Integer.parseInt(ae.getUid()));
                        amb.setAddFriendEntity(ae);
                        amb.setIsAgree(Boolean.parseBoolean(ae.getAgree()));
                        amb.setIsReject(ae.getAgree().equals("reject"));
                        amb.setTime(ae.getApplyTime());
                        messageBeans.add(amb);
                        imService.getUnReadMsgManager().clearUnreadAddReq(EntityChangeEngine.getSessionKey(amb.getApplyId(), DBConstant.SESSION_TYPE_SINGLE));
                    }
                }

                List<ApplyGroupEntity> applyGroupEntities = dbInterface.loadAllApplyGroupEntityByEntityType("family");
                applyGroupEntity(applyGroupEntities, true);
                applyGroupEntities = dbInterface.loadAllApplyGroupEntityByEntityType("story");
                applyGroupEntity(applyGroupEntities, false);
                EventBus.getDefault().post(new UnreadEvent(UnreadEvent.Event.SESSION_READED_UNREAD_MSG));
                if (null != messageBeans && !messageBeans.isEmpty()) {
                    //升序排序
                    Collections.sort(messageBeans, new Comparator<MessageBean>() {
                        @Override
                        public int compare(MessageBean messageBean, MessageBean t1) {
                            try {
                                Integer m = Integer.parseInt(messageBean.getTime());
                                Integer m1 = Integer.parseInt(t1.getTime());
                                return m.compareTo(m1);
                            } catch (Exception e) {
                                return -1;
                            }
                        }
                    });
                }
                systemMessageAdapter = new SystemMessageAdapter(messageBeans, imService);
                ultimateRecyvlerView.setAdapter(systemMessageAdapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onServiceDisconnected() {

        }
    };

    /**
     * 将 申请 or 邀请的 群 的消息  封装
     *
     * @param applyGroupEntities
     * @param isFamily           是否是家族
     */
    private void applyGroupEntity(List<ApplyGroupEntity> applyGroupEntities, boolean isFamily) {
        if (null != applyGroupEntities && !applyGroupEntities.isEmpty()) {
            String str = "";
            if (isFamily) {
                str = "家族";
            } else {
                str = "剧";
            }

            for (ApplyGroupEntity age : applyGroupEntities) {
                GroupAddMessageBean ga = new GroupAddMessageBean();

                ga.setApplyId(Integer.parseInt(age.getApplyId()));
                ga.setApplyName(age.getApplyName());
                ga.setGroupId(age.getGroupId());
                ga.setEntityId(age.getEntityId());
                ga.setName(age.getEntityName());
                ga.setIconUrl(age.getPortrait());
                ga.setIsAgree(Boolean.parseBoolean(age.getAgree()));
                ga.setIsReject(age.getAgree().equals("reject"));
                ga.setApplyGroupEntity(age);
                ga.setTime(age.getCreateTime());
                if (age.getType().equals("0")) { //申请

                    ga.setMessage(age.getApplyName() + "申请加入" + str + "【" + age.getEntityName() + "】");
                    if (isFamily) {
                        ga.setiMessageTypeDetail(
                            MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_ADD);
                    } else {
                        ga.setiMessageTypeDetail(
                            MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_ADD);
                    }
                } else if (age.getType().equals("1")) { //邀请
                    ga.setMessage(age.getCreator() + "邀请你加入" + str + "【" + age.getEntityName() + "】");
                    if (isFamily) {
                        ga.setiMessageTypeDetail(
                                MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_INVITATION);
                    } else {
                        ga.setiMessageTypeDetail(
                                MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_INVITATION);
                    }
                }
                messageBeans.add(ga);

                imService.getUnReadMsgManager().clearUnreadAddReq(EntityChangeEngine.getSessionKey(
                        Integer.valueOf(ga.getGroupId()),
                        DBConstant.SESSION_TYPE_GROUP));
            }
        }
    }

    @Override
    protected void onDestroy() {
        imServiceConnector.unbindService(this);
        imServiceConnector.disconnect(this);
        super.onDestroy();
    }
}
