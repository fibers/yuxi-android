package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.customview.StoryTypeSelectPopWindow;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.YXAnimationUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/10/29.
 * 创建剧情流程第一步
 */
public class CreateStoryFirstActivity extends Activity implements View.OnClickListener, TextWatcher, CreateStoryManager.UploadImageListener {
    private View topLine;
    private ImageView ivCover;
    private RelativeLayout rlCover;
    private ImageView ivArror;
    private ImageView ivBack;
    private ImageView ivAddCover;
    private TextView tvType;
    private TextView tvNextStep;
    private EditText etStoryName;
    private ProgressBar pbCover;
    private ImageView ivFail;
    private CustomDividerProgressbar progressbar;
    private StoryTypeSelectPopWindow storyTypeSelectPopWindow;
    private boolean isNetDataReady = false;
    private boolean isStoryNameReady = false;
    private boolean isTypeReady = false;
    private boolean isCoverReady = false;
    private Bitmap photo;
    private String capturePath;
    private final int titleWordLimit = 30;
    private static final int PHOTOHRAPH = 1;      // 拍照
    private static final int PHOTOZOOM = 2;       // 缩放
    private static final int PHOTORESOULT = 3;    // 结果
    private static final String IMAGE_UNSPECIFIED = "image/*";
    private List<StoryClass.ListEntity> listTypes = new ArrayList<>();
    private List<StoryClass.ListEntity> listSelectTypes = new ArrayList<>();
    private List<Integer> listIntegerSelect = new ArrayList<>();
    private Logger logger;
    private String picPath;
//    private boolean applyPhoto = false; //这里获取bitmap 因为路径是空的
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story_first);
        logger = Logger.getLogger(CreateStoryFirstActivity.class);
        initRes();
        if(!checkManagerData()){
            requestStoryTypes();
            CreateStoryManager.getInstance().requestRoleNatureList();
        }
//        requestStoryTypes();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(photo!=null&&!photo.isRecycled())
            photo.recycle();
        System.gc();
    }

    private void initRes() {
        CreateStoryManager.getInstance().setUploadImageListener(this);
        topLine = findViewById(R.id.view_top_create_story_fir);
        ivCover = (ImageView) findViewById(R.id.iv_bg_cover_create_stroy_fir);
        rlCover = (RelativeLayout) findViewById(R.id.rl_bg_cover_create_story_first);
        ivArror = (ImageView) findViewById(R.id.arror_create_story_fir);
        ivBack = (ImageView) findViewById(R.id.iv_back_create_story_fir);
        ivAddCover = (ImageView) findViewById(R.id.iv_add_cover_create_story_fir);
        tvType = (TextView) findViewById(R.id.tv_label_create_story_fir);
        tvNextStep = (TextView) findViewById(R.id.tv_next_step_create_story_fir);
        etStoryName = (EditText) findViewById(R.id.et_storyname_create_story_fir);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_fir);
        pbCover = (ProgressBar) findViewById(R.id.pb_cover_create_story_fir);
        ivFail = (ImageView) findViewById(R.id.iv_fail_cover_create_story_fir);
        storyTypeSelectPopWindow = new StoryTypeSelectPopWindow(this);
        storyTypeSelectPopWindow.getSureView().setOnClickListener(this);
        ivBack.setOnClickListener(this);
        ivAddCover.setOnClickListener(this);
        ivCover.setOnClickListener(this);
        ivArror.setOnClickListener(this);
        tvNextStep.setOnClickListener(this);
        tvType.setOnClickListener(this);
        ivFail.setOnClickListener(this);
        tvNextStep.setSelected(false);
        etStoryName.addTextChangedListener(this);
        etStoryName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    //禁用换行
                    return true;
                }
                return false;
            }
        });
        progressbar.setCurProgress((int) Math.ceil(100 / 6) + 1);
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add_cover_create_story_fir:
            case R.id.iv_bg_cover_create_stroy_fir:
                showAddImgDialog();
                break;
            case R.id.arror_create_story_fir:
            case R.id.tv_label_create_story_fir:
                if (isNetDataReady)
                    storyTypeSelectPopWindow.showPopWindow(topLine, listTypes);
                break;
            case R.id.tv_next_step_create_story_fir://跳转 设置 剧名称 剧 类型集合
                if (!tvNextStep.isSelected()) {
                    if(!isStoryNameReady) {
                        YXAnimationUtils.skakeAnimation(this, etStoryName);
                    }
                    if(!isCoverReady) {
                        YXAnimationUtils.skakeAnimation(this, rlCover);
                    }
                    if(!isTypeReady) {
                        YXAnimationUtils.skakeAnimation(this, tvType);
                    }
                    T.show(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), 0);
                    return;
                }

                constructSelectList();
                if (listIntegerSelect != null&& listIntegerSelect.size()>0) {
                    if (CreateStoryManager.getInstance().getCharacterCount(etStoryName.getText().toString().trim()) > titleWordLimit) {
                        T.show(getApplicationContext(), getResources().getString(R.string.story_title_limit_30), 0);
                        return;
                    }
                    CreateStoryManager.getInstance().setStoryName(etStoryName.getText().toString().trim());
                    CreateStoryManager.getInstance().setListStoryTypes(listIntegerSelect);
                    IMUIHelper.openCreateStoryOrderActivity(this,2,null);
                    finish();
                }
                break;
            case R.id.tv_sure_pop_type_select:
                storyTypeSelectPopWindow.dissmissPopWindow();
                listSelectTypes = storyTypeSelectPopWindow.getSelectLabels();
                if (constructSlectContent() != null) {
                    CreateStoryManager.getInstance().setStoryStringType(constructSlectContent());
                    tvType.setText(constructSlectContent());
                    isTypeReady = true;
                    checkStatus();
                }
                break;
            case R.id.iv_back_create_story_fir:
                CreateStoryManager.getInstance().clearAllData();
                finish();
                break;
            case R.id.iv_fail_cover_create_story_fir:
                if(picPath!=null){
                    ivFail.setVisibility(View.GONE);
                    progressbar.setVisibility(View.VISIBLE);
                    savePicPhoto();
                }
                break;
        }
    }

    private String constructSlectContent() {
        if (listSelectTypes != null && listSelectTypes.size() > 0) {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < listSelectTypes.size(); i++) {
                if (i != listSelectTypes.size() - 1) {
                    stringBuilder.append(listSelectTypes.get(i).getName() + ",");
                } else {
                    stringBuilder.append(listSelectTypes.get(i).getName());
                }
            }
            return stringBuilder.toString();
        } else {
            return null;
        }
    }


    private void constructSelectList() {
        if(listIntegerSelect!=null&&listIntegerSelect.size()>0)
            return;
        listIntegerSelect.clear();
        for (int i = 0; i < listSelectTypes.size(); i++) {
            listIntegerSelect.add(Integer.valueOf(listSelectTypes.get(i).getId()));
        }
    }


    /**
     * 检查状态   其中
     */
    private void checkStatus() {
        if (isCoverReady && isTypeReady && isStoryNameReady) {
            tvNextStep.setSelected(true);
        } else {
            tvNextStep.setSelected(false);
        }
    }

//
//    private void checkPhotoReady(){
//        if(photo!=null){
//            isCoverReady =true;
//            ivAddCover.setVisibility(View.GONE);
//            ivCover.setImageBitmap(photo);
//            checkStatus();
//        }else{
//            isCoverReady =false;
//        }
//    }

    private void savePicPhoto() {
        Bitmap bitmap = ImageUtil.getPathBitmap(CreateStoryFirstActivity.this,picPath,false);
        CreateStoryManager.getInstance().saveLoadImage(bitmap);
        CreateStoryManager.getInstance().uploadImage();
        try {
            if(!bitmap.isRecycled()){
                bitmap.recycle();
                System.gc();
            }

        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"bitmap回收失败",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s.toString())) {
            isStoryNameReady = false;
            checkStatus();
        } else {
            if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) == 0) {
                isStoryNameReady = false;
                checkStatus();
                return;
            }
            isStoryNameReady = true;
            checkStatus();
        }
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            CreateStoryManager.getInstance().clearAllData();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTOZOOM && data == null) {
            pbCover.setVisibility(View.GONE);
            if(photo==null)
            ivAddCover.setVisibility(View.VISIBLE);
            return;
        }
        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
                try {
                    photo = ImageUtil.getPathBitmap(CreateStoryFirstActivity.this,capturePath,true);
                    picPath = capturePath;
                } catch (Exception e) {
                    logger.e(e.toString());
                }
                break;
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        try {
                           Bitmap bitmap = (Bitmap) bundle.get("data");
                            this.picPath =ImageUtil.saveBitmapImg(bitmap);
                            photo = ImageUtil.getPathBitmap(CreateStoryFirstActivity.this,picPath,true);
                            if(!bitmap.isRecycled()){
                                try{
                                    bitmap.recycle();
                                    bitmap = null;
                                    System.gc();
                                }catch (Exception e){
                                    e.printStackTrace();
                                }
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    } else {
                        T.show(getApplicationContext(), "没有图片", 0);
                        return;
                    }
                } else {
                    try {
                        String picPath = ImageUtil.getUriPath(uri,getContentResolver());
                        this.picPath = picPath;
                        photo = ImageUtil.getPathBitmap(CreateStoryFirstActivity.this,picPath,true);
                    } catch (Exception e) {
                        try {
                            photo = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                            photo = ImageUtil.getBigBitmapForDisplay(photo, CreateStoryFirstActivity.this);
                            CreateStoryManager.getInstance().saveLoadImage(photo);
                            CreateStoryManager.getInstance().uploadImage();
                            picPath = null;
                        } catch (IOException e1) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            e1.printStackTrace();
                        }

                        logger.e(e.toString());
                    }
                }
                break;
            case PHOTORESOULT: //暂时没用
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                }
                break;
        }


        isCoverReady = false;
        checkStatus();
        if(photo!= null){
            ivAddCover.setVisibility(View.GONE);
            ivCover.setImageBitmap(photo);
        }else{
            ivAddCover.setVisibility(View.VISIBLE);
            pbCover.setVisibility(View.GONE);
        }
        if(picPath != null){
            CreateStoryManager.getInstance().setPicPath(picPath);
            savePicPhoto();
        }else{
//            Toast.makeText(getApplicationContext(),getResources().getString(R.string.picture_error_try_repic),Toast.LENGTH_LONG).show();
        }
    }

    //弹出添加图片的dialog
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(CreateStoryFirstActivity.this, "创建剧", "选择图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        Intent intent = new Intent(Intent.ACTION_PICK, null);
                        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                IMAGE_UNSPECIFIED);
                        startActivityForResult(intent, PHOTOZOOM);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pbCover.setVisibility(View.VISIBLE);
                                ivAddCover.setVisibility(View.GONE);
                            }
                        },500);
                    }

                    @Override
                    public void onCancelClicked() {
                        getImageFromCamera();
                    }
                });
    }

    protected void getImageFromCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");
            String out_file_path = Environment.getExternalStorageDirectory() + "/yuxi_temp";
            File dir = new File(out_file_path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            capturePath = Environment.getExternalStorageDirectory() + "/yuxi_temp/" + System.currentTimeMillis() + ".jpg";
            getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(capturePath)));
            getImageByCamera.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0.8);
            startActivityForResult(getImageByCamera, PHOTOHRAPH);
        } else {
            T.show(getApplicationContext(), "请确认已经插入SD卡", 0);
        }
    }


    /**
     * 获取剧的种类
     */
    private void requestStoryTypes() {
        OkHttpClientManager.getAsyn(ConstantValues.GetStoryCategory, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    StoryClass sc = new Gson().fromJson(response, StoryClass.class);
                    if (sc.getList() != null && sc.getList().size() > 0) {
                        listTypes = sc.getList();
                        CreateStoryManager.getInstance().setListStoryTypesOrigin(listTypes);
                        isNetDataReady = true;
                    }
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                    T.show(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), 0);
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), getResources().getString(R.string.story_net_status_error), 0);
            }
        });
    }


    private boolean checkManagerData(){
        List<StoryClass.ListEntity> listStoryTypes = CreateStoryManager.getInstance().getListStoryTypesOrigin();
        String storyTitle = CreateStoryManager.getInstance().getStoryName();
        String storyStringType = CreateStoryManager.getInstance().getStoryStringType();
        picPath = CreateStoryManager.getInstance().getPicPath();
        listIntegerSelect = CreateStoryManager.getInstance().getListStoryTypes();
        if(listStoryTypes!=null&&listStoryTypes.size()>0&&!TextUtils.isEmpty(storyTitle)&&!TextUtils.isEmpty(storyStringType)&&!TextUtils.isEmpty(picPath)){
            this.listTypes = listStoryTypes;
            etStoryName.setText(storyTitle);
            tvType.setText(storyStringType);

            photo = ImageUtil.getPathBitmap(CreateStoryFirstActivity.this,picPath,true);
            ivAddCover.setVisibility(View.GONE);
            ivCover.setImageBitmap(photo);

            isCoverReady = true;
            isTypeReady = true;
            isStoryNameReady = true;
            isNetDataReady = true;
            checkStatus();
            return true;
        }else{
            return  false;
        }

    }



    @Override
    public void uploadImgSuccess() {
        isCoverReady = true;
        checkStatus();
        pbCover.setVisibility(View.GONE);
    }

    @Override
    public void uploadImgFailed() {
        isCoverReady =false;
        checkStatus();
        pbCover.setVisibility(View.GONE);
        ivFail.setVisibility(View.VISIBLE);
    }
}

