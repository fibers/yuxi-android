package com.yuxip.ui.activity.home;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.ResponseFamily;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FamilyInfoEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.HisfamilyAdapter;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zl on 2015/7/1.
 * 我的家族列表
 */
public class Hisfamily extends TTBaseActivity {
    private View curView;
    private String uid;
    private List<FamilyInfoEntity> familylist = new ArrayList<>();
    private HisfamilyAdapter adapter;
    private TextView noFamilyTxt;
    private ListView hisFamilyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        initRes();
        ReqFamilyList();
    }

    private void initRes() {
        curView = View.inflate(Hisfamily.this, R.layout.tt_activity_hisfamily, topContentView);
        uid = IMLoginManager.instance().getLoginId() + "";
        hisFamilyView = (ListView) curView.findViewById(R.id.hisFamilyView);
        noFamilyTxt = (TextView) curView.findViewById(R.id.no_family);
        adapter = new HisfamilyAdapter(Hisfamily.this, familylist);
        hisFamilyView.setAdapter(adapter);
        setLeftButton(R.drawable.back_default_btn);
        setTitle("家族");
    }

    /**
     * 获取我的家族列表
     */
    private void ReqFamilyList() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GetMyFamilyList, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveMyFamilyResponse(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), R.string.network_err, 1);
            }
        });
    }

    private void onReceiveMyFamilyResponse(String result) {
        familylist.clear();
        ResponseFamily response = new Gson().fromJson(result, ResponseFamily.class);
        if (response.getResult().equals("1")) {
            familylist.addAll(response.getFamilylist());
            if (familylist != null && familylist.size() >= 0) {
                adapter.notifyDataSetChanged();
            } else {
                //没有数据的时候显示
                noFamilyTxt.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * *********************************************
     * 家族详情页的回调 一般用于退出或解散家族
     * *******************************************
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            ReqFamilyList();
        }
    }
}
