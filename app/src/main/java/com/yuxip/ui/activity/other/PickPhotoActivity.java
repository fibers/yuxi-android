
package com.yuxip.ui.activity.other;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.album.AlbumHelper;
import com.yuxip.ui.adapter.album.ImageBucket;
import com.yuxip.ui.adapter.album.ImageBucketAdapter;
import com.yuxip.utils.Logger;

import java.io.Serializable;
import java.util.List;


/**
 * @Description 相册列表
 */
public class PickPhotoActivity extends TTBaseNewActivity {
    List<ImageBucket> dataList = null;
    ListView listView = null;
    ImageBucketAdapter adapter = null;
    AlbumHelper helper = null;
    TextView cancel = null;
    public static Bitmap bimap = null;
    boolean touchable = true;
    private String currentSessionKey;
    private Logger logger = Logger.getLogger(PickPhotoActivity.class);
    private String type;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            this.finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logger.d("pic#PickPhotoActivity onCreate");
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.tt_activity_pick_photo);
        initData();
        initView();
    }

    /**
     * 初始化数据
     */
    private void initData() {
        Bundle bundle = getIntent().getExtras();
        currentSessionKey = bundle.getString(IntentConstant.SESSION_KEY);
        if(TextUtils.isEmpty(currentSessionKey)) {
            type = IntentConstant.SELECT_FROM_RELEASE_TOPIC;
        } else {
            type = IntentConstant.SELECT_FROM_CHAT;
        }
        helper = AlbumHelper.getHelper(getApplicationContext());
        dataList = helper.getImagesBucketList(true);
        bimap = BitmapFactory.decodeResource(getResources(),
                R.drawable.tt_default_album_grid_image);
    }


    /**
     * 初始化view
     */
    private void initView() {
        listView = (ListView) findViewById(R.id.list);
        adapter = new ImageBucketAdapter(this, dataList);

        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PickPhotoActivity.this, ImageGridActivity.class);
                intent.putExtra(IntentConstant.EXTRA_IMAGE_LIST, (Serializable) dataList.get(position).imageList);
                intent.putExtra(IntentConstant.EXTRA_ALBUM_NAME, dataList.get(position).bucketName);
                intent.putExtra(IntentConstant.SELECT_EVENT_TYPE, type);
                startActivityForResult(intent, 1);//requestcode》＝0
            }
        });
        cancel = (TextView) findViewById(R.id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PickPhotoActivity.this.finish();
                overridePendingTransition(R.anim.tt_stay, R.anim.tt_album_exit);
            }
        });

    }

}
