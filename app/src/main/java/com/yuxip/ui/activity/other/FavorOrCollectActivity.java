package com.yuxip.ui.activity.other;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.FavorOrCollectAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/27.
 * 这是喜欢或收藏的界面
 */
public class FavorOrCollectActivity extends TTBaseActivity {
    @InjectView(R.id.listview_favor_collect_activity)
    ListView listView;
    @InjectView(R.id.tv_favor_collect_activity)
    TextView  tvInfo;
    List<HomeInfo.PersoninfoEntity.MyHomeEntity> mlists=new ArrayList<>();
    private String type;       //类型  0 喜欢   1 收藏
    private String fromUser;      //传递类型  0 表示他人主页  1 表示自己主页
    private FavorOrCollectAdapter favorOrCollectAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View view=getLayoutInflater().inflate(R.layout.activity_favor_or_collect,topContentView);
        ButterKnife.inject(this,view);
        initRes();

    }

    private void initRes() {
        type = getIntent().getStringExtra(IntentConstant.TYPE);
        fromUser = getIntent().getStringExtra(IntentConstant.FROMUSER);
        if (!TextUtils.isEmpty(fromUser) && fromUser.equals("0")) {
            mlists = HomeEntityManager.getInstance().getUserList(Integer.valueOf(type), "0");
        } else {
            mlists = HomeEntityManager.getInstance().getUserList(Integer.valueOf(type), "1");
        }
        if(mlists!=null&&mlists.size()>0){
            favorOrCollectAdapter=new FavorOrCollectAdapter(this,type,mlists);
            listView.setAdapter(favorOrCollectAdapter);
        }else{
            listView.setVisibility(View.GONE);
            tvInfo.setVisibility(View.VISIBLE);
        }

        topLeftBtn.setVisibility(View.VISIBLE);
        topBar.setBackgroundColor(Color.parseColor("#f5f5f5"));
        if(type.equals("0")){
            setTitle("喜欢");
        }else{
            setTitle("收藏");
        }

        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
    }


}
