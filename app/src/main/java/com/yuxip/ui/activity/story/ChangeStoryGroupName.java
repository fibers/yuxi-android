package com.yuxip.ui.activity.story;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;

import org.json.JSONObject;

/**
 * edit by SummerRC on 2015/8/12.
 * description : 修改剧群名称
 */
public class ChangeStoryGroupName extends TTBaseActivity {
    private Logger logger = Logger.getLogger(ChangeStoryGroupName.class);

    private View curView;
    private String uid;
    private String groupId;
    private EditText changeGroupName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        curView = View.inflate(ChangeStoryGroupName.this, R.layout.activity_change_group_name, topContentView);
        initRes();
    }

    @Override
    protected void onResume() {
        super.onResume();
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        uid = getIntent().getStringExtra(IntentConstant.CREATOR_ID);
    }

    private void initRes() {
        setLeftButton(R.drawable.back_default_btn);
        setTitle("修改群名称");
        setRighTitleText("完成");
        changeGroupName = (EditText) curView.findViewById(R.id.changeGroupName);
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = changeGroupName.getText().toString().trim();
                if (name.isEmpty()) {
                    showToast("修改不能为空");
                } else {
                    ModifyStoryGroupInfo(name);
                }
            }
        });
    }

    /**
     * 修改家族名称
     */
    private void ModifyStoryGroupInfo(final String name) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("groupid", groupId);
        params.addParams("title", name);
        OkHttpClientManager.postAsy(ConstantValues.ModifyStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        if (obj.getString("describe").equals("ok")) {
                            showToast("修改昵称成功");
                            Intent intent = new Intent();
                            intent.putExtra(IntentConstant.GROUP_NAME, name);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            showToast(obj.getString("describe"));
                        }

                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }
}
