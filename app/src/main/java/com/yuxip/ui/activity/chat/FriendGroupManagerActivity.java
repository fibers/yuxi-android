package com.yuxip.ui.activity.chat;

import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.List;

/**
 * edited by SummerRC on 2015/8/30.
 * description:好友分组管理界面
 */
public class FriendGroupManagerActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(FriendGroupManagerActivity.class);

    private List<String> names;     //分组name集
    private List<String> ids;       //分组id集
    private List<Integer> num;      //每个分组成员数量
    private MyAdapter adapter;
    private final static int TYPE_ADD = 0;              //新建
    private final static int TYPE_EDIT = 1;             //编辑
    private boolean isOK = false;                       //标志是否对分组进行了修改，以便回调成功之后刷新列表

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        FriendGroupManager friendGroupManager = FriendGroupManager.getInstance();
        names = friendGroupManager.getGroupNameList();
        ids = friendGroupManager.getGroupIdList();
        num = friendGroupManager.getNum();

        getLayoutInflater().inflate(R.layout.fg_activity_edit_friend_group_manager, topContentView);
        initView();
    }

    private void initView() {
        setTitle("好友分组");
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isOK) {
                    setResult(RESULT_OK);
                }
                finish();
            }
        });
        findViewById(R.id.iv_add).setOnClickListener(this);
        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new MyAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (ids.get(position).equals("0")) {
                    T.show(FriendGroupManagerActivity.this, "默认分组不可修改！", 0);
                    Animation shake02 = AnimationUtils.loadAnimation(FriendGroupManagerActivity.this, R.anim.shake);
                    view.startAnimation(shake02);
                } else {
                    showAddOrEditGroupDialog(TYPE_EDIT, position);
                }
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add:                               //添加分组
                showAddOrEditGroupDialog(TYPE_ADD, 0);
                break;
        }
    }


    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return names.size();
        }

        @Override
        public Object getItem(int position) {
            return names.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyListener listener = new MyListener(position);
            ViewHolder holder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.item_listview_friend_group_manager, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.tv_name.setText(names.get(position));
            holder.iv_delete.setOnClickListener(listener);

            return convertView;
        }
    }

    class ViewHolder {
        TextView tv_name;
        ImageView iv_delete;

        public ViewHolder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            iv_delete = (ImageView) view.findViewById(R.id.iv_delete);
        }
    }


    public class MyListener implements View.OnClickListener {
        private int position;

        MyListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_delete:    //删除分组
                    if (ids.get(position).equals("0")) {
                        T.show(FriendGroupManagerActivity.this, "默认分组不能删除！", 0);
                        Animation shake02 = AnimationUtils.loadAnimation(FriendGroupManagerActivity.this, R.anim.shake);
                        v.startAnimation(shake02);
                        return;
                    }
                    if (num.get(position) > 0) {
                        T.show(FriendGroupManagerActivity.this, "分组不为空！", 0);
                        Animation shake02 = AnimationUtils.loadAnimation(FriendGroupManagerActivity.this, R.anim.shake);
                        v.startAnimation(shake02);
                    } else {
                        deleteGroup(position);
                    }
                    break;
            }
        }
    }

    /**
     * 新建好友分组
     *
     * @param name 分组名，可重复
     */
    private void createFriendsGroup(final String name) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("friendgroupname", name);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.CreateFriendsGroup, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.getString("result").equals("0")) {
                        names.add(name);
                        ids.add(object.getString("result"));
                        num.add(0);
                        FG_GroupJsonBean fg_groupJsonBean = new FG_GroupJsonBean();
                        fg_groupJsonBean.setGroupname(name);
                        fg_groupJsonBean.setGroupid(object.getString("result"));
                        FriendGroupManager.getInstance().addNewGroup(fg_groupJsonBean);
                        adapter.notifyDataSetChanged();
                        isOK = true;
                    }
                    T.show(FriendGroupManagerActivity.this, object.getString("describe"), 0);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 删除好友分组
     *
     * @param position position
     */
    private void deleteGroup(final int position) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("friendgroupid", ids.get(position));
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.DelFriendsGroup, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        FriendGroupManager.getInstance().deleteGroupByGroupId(ids.get(position));
                        names.remove(position);
                        ids.remove(position);
                        num.remove(position);
                        adapter.notifyDataSetChanged();
                        isOK = true;
                    }
                    T.show(FriendGroupManagerActivity.this, object.getString("describe"), 0);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 修改好友分组
     *
     * @param name 分组名，可重复
     */
    private void updateFirendsGroupInfo(final String name, final int position) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("friendgroupid", ids.get(position));
        params.addParams("infotype", "1");       //1代表修改的是好友分组的名称
        params.addParams("newvalue", name);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.UpdateFirendsGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        FriendGroupManager.getInstance().changeGroupNameByGroupId(ids.get(position), name);
                        names.remove(position);
                        names.add(position, name);
                        adapter.notifyDataSetChanged();
                        isOK = true;
                    }
                    T.show(FriendGroupManagerActivity.this, object.getString("describe"), 0);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }


    /**
     * 添加或者修改分组的弹出框
     */
    public void showAddOrEditGroupDialog(final int type, final int position) {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_add_group, null);
        if (type == TYPE_EDIT) {
            ((TextView) view.findViewById(R.id.tv_title)).setText("编辑分组");
        }
        final Dialog dialog = new Dialog(this, R.style.dialog_theme_transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setContentView(view);

        final EditText dialog_message = (EditText) view.findViewById(R.id.edit_content);
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tv_cancel:
                        dialog.dismiss();
                        break;
                    case R.id.tv_confirm:
                        String newName = dialog_message.getText().toString();
                        if (newName.equals("")) {
                            T.show(FriendGroupManagerActivity.this, "名字不能为空！", 0);
                            return;
                        }
                        if (type == TYPE_ADD) {         //添加
                            createFriendsGroup(newName);
                        } else {                        //修改
                            if (names.get(position).equals(newName)) {
                                T.show(FriendGroupManagerActivity.this, "请修改名称！", 0);
                                return;
                            }
                            updateFirendsGroupInfo(newName, position);
                        }

                        dialog.dismiss();
                        break;
                }
            }
        };
        view.findViewById(R.id.tv_confirm).setOnClickListener(myListener);
        view.findViewById(R.id.tv_cancel).setOnClickListener(myListener);
    }

    /**
     * 返回键
     *
     * @param keyCode keyCode
     * @param event   event
     * @return 是否被消费
     */
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isOK) {
                setResult(RESULT_OK);
            }
            finish();
        }
        return true;
    }
}
