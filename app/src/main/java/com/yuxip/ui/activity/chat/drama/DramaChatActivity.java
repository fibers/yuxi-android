
package com.yuxip.ui.activity.chat.drama;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupAnnouncementEntity;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.LastChatEntity;
import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.PeerEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.JsonBean.StoryDetailsBean;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.config.HandlerConstant;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.entity.TextMessage;
import com.yuxip.imservice.entity.UnreadEntity;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.MessageEvent;
import com.yuxip.imservice.event.PriorityEvent;
import com.yuxip.imservice.event.SelectEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMStackManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXGroupTypeManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.thread.AsyncTaskBase;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.chat.TheSpellActivity;
import com.yuxip.ui.activity.other.MainActivity;
import com.yuxip.ui.activity.other.PickPhotoActivity;
import com.yuxip.ui.activity.story.CurseMemberListActivity;
import com.yuxip.ui.adapter.album.AlbumHelper;
import com.yuxip.ui.adapter.album.ImageBucket;
import com.yuxip.ui.adapter.album.ImageItem;
import com.yuxip.ui.customview.WaterGroupPopupWindow;
import com.yuxip.ui.helper.AudioPlayerHandler;
import com.yuxip.ui.widget.FaceInputView;
import com.yuxip.ui.widget.MGProgressbar;
import com.yuxip.utils.CommonUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * Description 剧群消息界面
 * date 2014-8-20
 * created by SummerRC
 */
public class DramaChatActivity extends TTBaseActivity implements OnRefreshListener2<ListView>, View.OnClickListener {

    private MyHandler uiHandler = null;          // 处理语音
    private PullToRefreshListView lvPTR = null;
    private DramaChatAdapter adapter = null;
    private String storyId = "";
    private String groupId = "";
    private GroupEntity groupEntity;
    private TextView textView_new_msg_tip = null;
    private List<ImageBucket> albumList = null;
    private MGProgressbar progressbar = null;
    private String takePhotoSavePath = "";
    private Logger logger = Logger.getLogger(DramaChatActivity.class);
    private IMService imService;
    private UserEntity loginUser;
    private PeerEntity peerEntity;

    // 当前的session
    private String currentSessionKey;
    private int historyTimes = 0;
    private LastChatEntity lastChatEntity;
    private String et_input_content = "";
    private ArrayList<Integer> idList;                      //被@的人的id集合
    private ArrayList<String> nameList;                     //被@的人的name集合
    private boolean IS_CALLBACK = false;                    //由于@回掉之后setText中也存在@符号，导致继续引起监听@的事件发生响应，故存在这个判断
    private static final int ACTIVITY_RESULT_CODE_CURSE = 222;
    private boolean isFirstLoad = true;                     //判断editText的状态是 历史数据 或是 当前填写的 （历史 数据 @结尾 不跳转选择好友界面）


    private FaceInputView faceInputView;                    //自定义表情键盘控件
    private Button bt_biaoqing_send;                        //自定义表情键盘输入表情时的发送按钮
    private TextView tv_text_send;                          //自定义表情键盘输入文本时的发送按钮
    private EditText et_input;                              //自定义表情键盘的输入框
    private TextMessage textMessage;

    private StoryParentEntity storyParentEntity;


    /**
     * 全局Toast
     */
    private Toast mToast;

    private WaterGroupPopupWindow waterGroupPopupWindow;

    public void showToast(int resId) {
        String text = getResources().getString(resId);
        if (mToast == null) {
            mToast = Toast.makeText(DramaChatActivity.this, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        IMApplication.gifRunning = false;
        cancelToast();
        super.onBackPressed();
    }

    /**
     * end 全局Toast
     */
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            logger.d("message_activity#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
            initData();
            /**  初始化输入编辑框的内容     */
            initEditMessage();
            /** 新手任务 */
            showTask();
            /** 获取群公告 */
            if (groupEntity != null && groupEntity.getCreatorId() != IMLoginManager.instance().getLoginId()) {
                GroupAnnouncementEntity announcementEntity = imService.getDbInterface().getGroupAnnouncementEntityByGroupId(groupId);
                String latestTime = "0";
                if (announcementEntity != null) {
                    latestTime = announcementEntity.getDateTime();
                } else {
                    announcementEntity = new GroupAnnouncementEntity();
                }
                getStoryGroupBoard(latestTime, announcementEntity);
            }
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logger.d("message_activity#onCreate:%s", this);
        super.onCreate(savedInstanceState);

        if (super.isNeedRestart()) {
            return;
        }
        //得到frament 携带过来的sessionkey值
        currentSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);
        groupId = currentSessionKey.split("_")[1];
        groupEntity = IMGroupManager.instance().findGroup(Integer.parseInt(groupId));
        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        storyParentEntity = YXStoryManager.instance().getStoryByGroupid(Integer.parseInt(groupId));

        //初始化数据（相册,表情,数据库相关）
        new AsyncTaskLoading().execute();
        /** audio状态的语音还在使用这个 */
        initAudioHandler();
        initView();
        initPopupWindow();
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this, SysConstant.MESSAGE_EVENTBUS_PRIORITY);
        logger.d("message_activity#register im service and eventBus");

    }

    // 触发条件,imService链接成功，或者newIntent
    private void initData() {
        historyTimes = 0;
        adapter.clearItem();
        ImageMessage.clearImageMessageList();
        loginUser = imService.getLoginManager().getLoginInfo();
        peerEntity = imService.getSessionManager().findPeerEntity(currentSessionKey);
        if (TextUtils.isEmpty(storyId)) {
            if (storyParentEntity == null) {
                T.show(getApplicationContext(), "数据错误，稍后重试", 0);
                finish();
                return;
            } else {
                storyId = storyParentEntity.getStoryId() + "";
            }
        }
        if (peerEntity == null || groupEntity == null) {
            imService.getGroupManager().reqGroupDetailInfo(Integer.valueOf(groupId));
            Set<Integer> memberSet = new HashSet<>();
            memberSet.add(IMLoginManager.instance().getLoginId());
            IMGroupManager.instance().reqAddGroupMember(Integer.valueOf(groupId), memberSet);
            T.show(getApplicationContext(), "该群可能已被删除", 0);
            this.finish();
            return;
        }
        // 头像、历史消息加载、取消通知
        setTitleByUser();
        reqHistoryMsg();
        adapter.setImService(imService, loginUser, storyId, groupId);
        imService.getUnReadMsgManager().readUnreadSession(currentSessionKey);
        imService.getNotificationManager().cancelSessionNotifications(currentSessionKey);

        isFirstLoad = false;
    }

    /**
     * 本身位于Message页面，点击通知栏其他session的消息
     */
    @Override
    protected void onNewIntent(Intent intent) {
        logger.d("DramaChatActivity#onNewIntent:%s", this);
        super.onNewIntent(intent);
        setIntent(intent);
        historyTimes = 0;
        if (intent == null) {
            return;
        }
        String newSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);
        if (newSessionKey == null) {
            return;
        }
        logger.d("chat#newSessionInfo:%s", newSessionKey);
        if (!newSessionKey.equals(currentSessionKey)) {
            currentSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);
            groupId = currentSessionKey.split("_")[1];
            groupEntity = IMGroupManager.instance().findGroup(Integer.parseInt(groupId));
            storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
            storyParentEntity = YXStoryManager.instance().getStoryByGroupid(Integer.parseInt(groupId));

            initData();
            /**  初始化输入编辑框的内容     */
            initEditMessage();
            /** 新手任务 */
            showTask();
            /** 获取群公告 */
            if (groupEntity != null && groupEntity.getCreatorId() != IMLoginManager.instance().getLoginId()) {
                GroupAnnouncementEntity announcementEntity = imService.getDbInterface().getGroupAnnouncementEntityByGroupId(groupId);
                String latestTime = "0";
                if (announcementEntity != null) {
                    latestTime = announcementEntity.getDateTime();
                } else {
                    announcementEntity = new GroupAnnouncementEntity();
                }
                getStoryGroupBoard(latestTime, announcementEntity);
            }
        }
    }

    @Override
    protected void onResume() {
        logger.d("message_activity#onresume:%s", this);
        super.onResume();
        IMApplication.gifRunning = true;
        historyTimes = 0;
        // not the first time
        if (imService != null) {
            // 处理session的未读信息
            handleUnreadMsgs();
        }
        /** 当activity之间进行切换时可能会发生数据的改变，如：添加新的表情标签 */
        faceInputView.refresh();
    }

    @Override
    protected void onDestroy() {
        logger.d("message_activity#onDestroy:%s", this);
        if (adapter == null) {
            super.onDestroy();
            return;
        }
        historyTimes = 0;
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        adapter.clearItem();
        if (albumList != null) {
            albumList.clear();
        }
        ImageMessage.clearImageMessageList();
        super.onDestroy();
    }

    /**
     * 设定聊天名称
     * 1. 如果是user类型， 点击触发UserProfile
     * 2. 如果是群组，检测自己是不是还在群中
     */
    private void setTitleByUser() {
        setTitle(peerEntity.getMainName());
        int peerType = peerEntity.getType();
        switch (peerType) {
            case DBConstant.SESSION_TYPE_GROUP: {
                GroupEntity group = (GroupEntity) peerEntity;
                Set<Integer> memberLists = group.getlistGroupMemberIds();
                if (!memberLists.contains(loginUser.getPeerId())) {
                    Toast.makeText(DramaChatActivity.this, R.string.no_group_member, Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case DBConstant.SESSION_TYPE_SINGLE: {
                topTitleTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode)
            return;
        switch (requestCode) {
            case SysConstant.CAMERA_WITH_DATA:
                handleTakePhotoData(data);
                break;
            case SysConstant.ALBUM_BACK_DATA:
                logger.d("pic#ALBUM_BACK_DATA");
                setIntent(data);
                break;
            case SysConstant.ACTIVITY_RESULT_CODE_BOARD:        //面板
                Bundle bundle_content = data.getExtras();
                String content = bundle_content.getString(IntentConstant.INPUT_CONTENT);
                if (content != null) {
                    et_input.setText(content);
                    et_input.setSelection(content.length());
                }
                et_input.requestFocus();
                break;

            case ACTIVITY_RESULT_CODE_CURSE:        //咒语：放大招
                Bundle bundle = data.getExtras();
                idList = bundle.getIntegerArrayList("id");
                nameList = bundle.getStringArrayList("name");
                if (nameList != null) {
                    for (String str : nameList) {
                        et_input_content += str + "  ";
                    }
                    et_input.setText(et_input_content);
                    et_input.setSelection(et_input_content.length());
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleImagePickData(List<ImageItem> list) {
        ArrayList<ImageMessage> listMsg = new ArrayList<>();
        ArrayList<ImageItem> itemList = (ArrayList<ImageItem>) list;
        for (ImageItem item : itemList) {
            ImageMessage imageMessage = ImageMessage.buildForSend(item, loginUser, peerEntity);
            listMsg.add(imageMessage);
            pushList(imageMessage);
        }
        imService.getMessageManager().sendImages(listMsg);
    }


    public void onEventMainThread(SelectEvent event) {
        switch (event.getEvent()) {
            case TYPE_CHAT:
                List<ImageItem> itemList = event.getList();
                if (itemList != null && itemList.size() > 0) {
                    handleImagePickData(itemList);
                }
                break;
        }
    }

    /**
     * 背景: 1.EventBus的cancelEventDelivery的只能在postThread中运行，而且没有办法绕过这一点
     * 2. onEvent(A a)  onEventMainThread(A a) 这个两个是没有办法共存的
     * 解决: 抽离出那些需要优先级的event，在onEvent通过handler调用主线程，
     * 然后cancelEventDelivery
     * <p/>
     * todo  need find good solution
     */
    public void onEvent(PriorityEvent event) {
        switch (event.event) {
            case MSG_RECEIVED_MESSAGE: {
                MessageEntity entity = (MessageEntity) event.object;
                /**正式当前的会话*/
                if (currentSessionKey.equals(entity.getSessionKey())) {
                    Message message = Message.obtain();
                    message.what = HandlerConstant.MSG_RECEIVED_MESSAGE;
                    message.obj = entity;
                    uiHandler.sendMessage(message);
                    EventBus.getDefault().cancelEventDelivery(event);
                }
            }
            break;
        }
    }

    public void onEventMainThread(MessageEvent event) {
        MessageEvent.Event type = event.getEvent();
        MessageEntity entity = event.getMessageEntity();
        switch (type) {
            case ACK_SEND_MESSAGE_OK: {
                onMsgAck(entity);
            }
            break;
            case ACK_SEND_MESSAGE_FAILURE:
                // 失败情况下新添提醒
                showToast(R.string.message_send_failed);
                break;
            case ACK_SEND_MESSAGE_TIME_OUT: {
                onMsgUnAckTimeoutOrFailure(entity);
            }
            break;
            case HANDLER_IMAGE_UPLOAD_FAILD: {
                logger.d("pic#onUploadImageFaild");
                ImageMessage imageMessage = (ImageMessage) entity;
                adapter.updateItemState(imageMessage);
                showToast(R.string.message_send_failed);
            }
            break;
            case HANDLER_IMAGE_UPLOAD_SUCCESS: {
                ImageMessage imageMessage = (ImageMessage) entity;
                adapter.updateItemState(imageMessage);
            }
            break;
            case HISTORY_MSG_OBTAIN: {
                if (historyTimes == 1) {
                    adapter.clearItem();
                    reqHistoryMsg();
                }
            }
            break;
        }
    }


    /**
     * [备注] DB保存，与session的更新manager已经做了
     *
     * @param messageEntity
     */
    private void onMsgAck(MessageEntity messageEntity) {
        logger.d("message_activity#onMsgAck");
        int msgId = messageEntity.getMsgId();
        logger.d("chat#onMsgAck, msgId:%d", msgId);

        /**到底采用哪种ID呐??*/
        long localId = messageEntity.getId();
        adapter.updateItemState(messageEntity);
    }


    private void handleUnreadMsgs() {
        logger.d("messageacitivity#handleUnreadMsgs sessionId:%s", currentSessionKey);
        // 清除未读消息
        UnreadEntity unreadEntity = imService.getUnReadMsgManager().findUnread(currentSessionKey);
        if (null == unreadEntity) {
            return;
        }
        int unReadCnt = unreadEntity.getUnReadCnt();
        if (unReadCnt > 0) {
            imService.getNotificationManager().cancelSessionNotifications(currentSessionKey);
            adapter.notifyDataSetChanged();
            scrollToBottomListItem();
        }
    }


    // 肯定是在当前的session内
    private void onMsgRecv(MessageEntity entity) {
        logger.d("message_activity#onMsgRecv");

        imService.getUnReadMsgManager().ackReadMsg(entity);
        logger.d("chat#start pushList");
        pushList(entity);
        ListView lv = lvPTR.getRefreshableView();
        if (lv != null) {

            if (lv.getLastVisiblePosition() < adapter.getCount()) {
                textView_new_msg_tip.setVisibility(View.VISIBLE);
            } else {
                scrollToBottomListItem();
            }
        }
    }


    private void onMsgUnAckTimeoutOrFailure(MessageEntity messageEntity) {
        logger.d("chat#onMsgUnAckTimeoutOrFailure, msgId:%s", messageEntity.getMsgId());
        // msgId 应该还是为0
        adapter.updateItemState(messageEntity);
    }


    /**
     * @Description 显示联系人界面
     */
    private void showGroupManageActivity() {
        if (!TextUtils.isEmpty(storyId)) {
            IMUIHelper.openStoryGroupDetailsActivity(this, storyId, groupId, true);
        }
    }


    /**
     * @Description 初始化数据（相册,表情,数据库相关）
     */
    private void initAlbumHelper() {
        AlbumHelper albumHelper = AlbumHelper.getHelper(getApplicationContext());
        albumList = albumHelper.getImagesBucketList(false);
    }


    /**
     * @Description 初始化界面控件
     * 有点庞大 todo
     */
    private void initView() {
        // 绑定布局资源(注意放所有资源初始化之前)
        topContentView.setBackgroundColor(getResources().getColor(R.color.chat_top_content_view));
        LayoutInflater.from(this).inflate(R.layout.xy_activity_drama_chat, topContentView);

        //TOP_CONTENT_VIEW
        setLeftButton(R.drawable.back_default_btn);
        setRightButton(R.drawable.msg_zhouyu);
        setRightTitleTextBackgroundResource(R.drawable.msg_family_h);
        righTitleTxt.setOnClickListener(this);
        topLeftBtn.setOnClickListener(this);
        topRightBtn.setOnClickListener(this);

        // 列表控件(开源PTR)
        lvPTR = (PullToRefreshListView) this.findViewById(R.id.message_list);
        textView_new_msg_tip = (TextView) this.findViewById(R.id.tt_new_msg_tip);
        lvPTR.getRefreshableView().addHeaderView(LayoutInflater.from(this).inflate(R.layout.tt_messagelist_header, lvPTR.getRefreshableView(), false));
        Drawable loadingDrawable = getResources().getDrawable(R.drawable.pull_to_refresh_indicator);
        final int indicatorWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 29,
                getResources().getDisplayMetrics());
        loadingDrawable.setBounds(new Rect(0, indicatorWidth, 0, indicatorWidth));
        lvPTR.getLoadingLayoutProxy().setLoadingDrawable(loadingDrawable);
        lvPTR.getRefreshableView().setCacheColorHint(Color.WHITE);
        lvPTR.getRefreshableView().setSelector(new ColorDrawable(Color.WHITE));
        adapter = new DramaChatAdapter(this);
        lvPTR.setAdapter(adapter);
        lvPTR.setOnRefreshListener(this);
        lvPTR.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true) {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
                            textView_new_msg_tip.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });

        lvPTR.getRefreshableView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                faceInputView.hideKeyborad();
                return false;
            }
        });
        textView_new_msg_tip.setOnClickListener(this);


        //LOADING
        View view = LayoutInflater.from(DramaChatActivity.this)
                .inflate(R.layout.tt_progress_ly, null);
        progressbar = (MGProgressbar) view.findViewById(R.id.tt_progress);
        LayoutParams pgParms = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);
        pgParms.bottomMargin = 50;
        addContentView(view, pgParms);


        //自定义键盘的初始化
        faceInputView = (FaceInputView) findViewById(R.id.myFaceInputView);
        et_input = (EditText) faceInputView.getView(FaceInputView.MyViewEnum.EDITTEXT_INPUT);
        bt_biaoqing_send = (Button) faceInputView.getView(FaceInputView.MyViewEnum.BUTTON_BIAOQING_SEND);
        bt_biaoqing_send.setOnClickListener(this);
        tv_text_send = (TextView) faceInputView.getView(FaceInputView.MyViewEnum.TEXTVIEW_TEXT_SEND);
        tv_text_send.setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.IMAGE_VIEW_TAKE_CAMERA).setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.IMAGE_VIEW_TAKE_PHOTO).setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.BUTTON_GO_MIANBAN).setOnClickListener(this);
        et_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.v("beforeTextChanged", "s: " + s
                        .toString() + " start: " + start + " count: " + count + " after: " + after);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.v("onTextChanged", "s: " + s
                        .toString() + " start: " + start + " before: " + before + " count: " + count);
                if (currentSessionKey.startsWith("1")) { //如果是个人聊天的话，就没必要有@相关操作了
                    return;
                }
                et_input_content = s.toString();
                if (IS_CALLBACK) {
                    IS_CALLBACK = false;
                } else {
                    String newStr = s.toString().substring(start, start + count);
                    Log.v("newStr", newStr);
                    if (isFirstLoad)
                        return;
                    if (newStr.contains("@")) {
                        IS_CALLBACK = true;
                        Intent intent = new Intent(DramaChatActivity.this, CurseMemberListActivity.class);
                        intent.putExtra("groupid", groupId);

                        if (null != storyId) {
                            intent.putExtra("storyid", storyId);
                        }
                        startActivityForResult(intent, ACTIVITY_RESULT_CODE_CURSE);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.v("afterTextChanged", s.toString());
            }
        });
    }


    /**
     * 1.初始化请求历史消息
     * 2.本地消息不全，也会触发
     */
    private void reqHistoryMsg() {
        historyTimes++;
        List<MessageEntity> msgList = imService.getMessageManager().loadHistoryMsg(historyTimes, currentSessionKey, peerEntity);
        pushList(msgList);
        scrollToBottomListItem();
    }

    /**
     * @param msg
     */
    public void pushList(MessageEntity msg) {
        logger.d("chat#pushList msgInfo:%s", msg);
        adapter.addItem(msg);
    }

    public void pushList(List<MessageEntity> entityList) {
        logger.d("chat#pushList list:%d", entityList.size());
        adapter.loadHistoryList(entityList);
    }


    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
    }

    /**
     * @param data
     * @Description 处理拍照后的数据
     * 应该是从某个 activity回来的
     */
    private void handleTakePhotoData(Intent data) {
        ImageMessage imageMessage = ImageMessage.buildForSend(takePhotoSavePath, loginUser, peerEntity);
        List<ImageMessage> sendList = new ArrayList<>(1);
        sendList.add(imageMessage);
        imService.getMessageManager().sendImages(sendList);
        // 格式有些问题
        pushList(imageMessage);
    }


    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
    }

    @Override
    public void onPullDownToRefresh(
            final PullToRefreshBase<ListView> refreshView) {
        // 获取消息
        refreshView.postDelayed(new Runnable() {
            @Override
            public void run() {
                ListView mlist = lvPTR.getRefreshableView();
                int preSum = mlist.getCount();
                MessageEntity messageEntity = adapter.getTopMsgEntity();
                if (messageEntity != null) {
                    List<MessageEntity> historyMsgInfo = imService.getMessageManager().loadHistoryMsg(messageEntity, historyTimes);
                    if (historyMsgInfo.size() > 0) {
                        historyTimes++;
                        adapter.loadHistoryList(historyMsgInfo);
                    }
                }

                int afterSum = mlist.getCount();
                mlist.setSelection(afterSum - preSum);
                /**展示位置为这次消息的最末尾*/
                //mlist.setSelection(size);
                // 展示顶部
//                if (!(mlist).isStackFromBottom()) {
//                    mlist.setStackFromBottom(true);
//                }
//                mlist.setStackFromBottom(false);
                refreshView.onRefreshComplete();
            }
        }, 200);
    }


    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.bt_take_camera: {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePhotoSavePath = CommonUtil.getImageSavePath(String.valueOf(System
                        .currentTimeMillis())
                        + ".jpg");
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(takePhotoSavePath)));
                startActivityForResult(intent, SysConstant.CAMERA_WITH_DATA);
                scrollToBottomListItem();
            }
            break;
            case R.id.bt_take_photo: {
                if (albumList == null || albumList.size() < 1) {
                    Toast.makeText(DramaChatActivity.this,
                            getResources().getString(R.string.not_found_album), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                // 选择图片的时候要将session的整个回话 传过来
                Intent intent = new Intent(DramaChatActivity.this, PickPhotoActivity.class);
                intent.putExtra(IntentConstant.SESSION_KEY, currentSessionKey);
                startActivityForResult(intent, SysConstant.ALBUM_BACK_DATA);

                DramaChatActivity.this.overridePendingTransition(R.anim.tt_album_enter, R.anim.tt_stay);
                scrollToBottomListItem();
            }
            break;
            case R.id.bt_panel:
                break;
            case R.id.bt_biaoqing_send:
            case R.id.tv_text_send:
                /** 发送的内容 */
                String content = et_input.getText().toString();
                if (content.trim().equals("")) {
                    T.show(getApplicationContext(), getResources().getString(R.string.message_null), 0);
                    return;
                }
                if (null != nameList && nameList.size() > 0) {
                    if (et_input_content.contains("去吧皮卡丘") || et_input_content
                            .contains("去吧,皮卡丘") || et_input_content.contains("去吧，皮卡丘")) {
                        theSpellGo();  //咒语   去吧皮卡丘  意思是在审核群当中 移动成员到  对戏 和 水聊
                    } else if (et_input_content.contains("离开皮卡丘") || et_input_content
                            .contains("离开,皮卡丘") || et_input_content.contains("离开，皮卡丘")) {
                        theSpellDel(); //咒语 离开皮卡丘  意思是删除群成员
                    } else if (et_input_content.contains("一套带走")) { //这里的咒语是  一套带走    就是加好友的意思
                        theSpellAdd();
                    } else {
                        /** 构建发送的message */
                        textMessage = TextMessage.buildForSend(content, loginUser, peerEntity);
                        sendMessage();
                    }
                } else {
                    /** 构建发送的message */
                    textMessage = TextMessage.buildForSend(content, loginUser, peerEntity);
                    sendMessage();
                }
                break;
            case R.id.left_btn:
            case R.id.left_txt:
                actFinish();
                break;
            case R.id.right_btn:    //咒语介绍
                TheSpellActivity.startTheSpallActivity(this, TheSpellActivity.FROM_STORY);
                break;
            case R.id.right_txt:    //剧资料页面
                waterGroupPopupWindow.show(storyId, groupId, righTitleTxt);
                break;
            case R.id.tt_new_msg_tip: {
                scrollToBottomListItem();
                textView_new_msg_tip.setVisibility(View.GONE);
            }
            break;
        }
    }


    @Override
    protected void onStop() {
        logger.d("message_activity#onStop:%s", this);

        if (null != adapter) {
            adapter.hidePopup();
        }
        saveEditMessage();

        AudioPlayerHandler.getInstance().clear();
        super.onStop();
    }

    @Override
    protected void onStart() {
        logger.d("message_activity#onStart:%s", this);
        super.onStart();
    }

    /**
     * @Description 滑动到列表底部
     */
    private void scrollToBottomListItem() {
        logger.d("message_activity#scrollToBottomListItem");

        // todo eric, why use the last one index + 2 can real scroll to the
        // bottom?
        ListView lv = lvPTR.getRefreshableView();
        if (lv != null) {
            lv.setSelection(adapter.getCount() + 1);
        }
        textView_new_msg_tip.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        logger.d("message_activity#onPause:%s", this);
        super.onPause();
    }


    private void actFinish() {
        IMStackManager.getStackManager().popTopActivitys(MainActivity.class);
        IMApplication.gifRunning = false;
        DramaChatActivity.this.finish();
    }


    private void initPopupWindow() {
        waterGroupPopupWindow = new WaterGroupPopupWindow(this, new WaterGroupPopupWindow.WaterPopGroupMsg() {
            @Override
            public void showGroupMess() {
                IMApplication.IS_REFRESH = true;
                showGroupManageActivity();
            }
        });
    }

    /*
     发送消息
     */
    private void sendMessage() {
        /** 得到信息管理者，然后将信息发送出去 */
        imService.getMessageManager().sendText(textMessage);
        et_input.setText("");
        pushList(textMessage);
        scrollToBottomListItem();
        /** 隐藏发送TextView,显示去面板按钮 */
        faceInputView.getView(FaceInputView.MyViewEnum.TEXTVIEW_TEXT_SEND)
                .setVisibility(View.GONE);
        faceInputView.getView(FaceInputView.MyViewEnum.BUTTON_ADD)
                .setVisibility(View.VISIBLE);
    }

    private void initEditMessage() {
        lastChatEntity = imService.getDbInterface().getLastChatEntityByCurrentSessionKey(currentSessionKey);
        if (lastChatEntity != null && !TextUtils.isEmpty(lastChatEntity.getContent())) {
            et_input.setText(lastChatEntity.getContent());
        }
    }

    private void saveEditMessage() {
        if (lastChatEntity == null) {
            lastChatEntity = new LastChatEntity();
            lastChatEntity.setCurrentSessionKey(currentSessionKey);
            lastChatEntity.setContent(et_input.getText().toString());
        } else {
            lastChatEntity.setContent(et_input.getText().toString());
        }
        imService.getDbInterface().insertOrUpdateLastChatEntity(lastChatEntity);
    }

    /**
     * 咒语添加好友
     */
    private void theSpellAdd() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("发送好友添加请求？");
        builder.setPositiveButton(getString(android.R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                        if (null != idList && !idList.isEmpty()) {
                            for (Integer integer : idList) {
                                imService.getMessageManager().sendAddFriendReq(integer);
                            }
                            idList.clear();
                            nameList.clear();
                            et_input.setText("");

                            T.show(getApplicationContext(), "发送成功！", 0);
                        }
                    }
                });
        builder.show();
    }

    /**
     * 咒语删除群成员
     */
    private void theSpellDel() {
        if (groupEntity.getCreatorId() != IMLoginManager.instance().getLoginId()) {
            T.showShort(this, "您不是管理员");
        } else {
            curTheSpell = THE_SPELL_DEL;

            String name = "";
            Set<Integer> memberSet = new HashSet<>();
            for (int i = 0; i < nameList.size(); i++) {
                name += nameList.get(i);
                memberSet.add(idList.get(i));
            }
            IMGroupManager.instance().reqRemoveGroupMember(groupEntity.getPeerId(), memberSet);
            nameList.clear();
            idList.clear();
            textMessage = TextMessage.buildForSend(name + "被管理员移出", loginUser, peerEntity);
            sendMessage();
        }
    }

    /**
     * 咒语去吧皮卡丘
     */
    private void theSpellGo() {
        if (storyId == null) {
            T.showShort(this, "不是剧");
        } else if (!YXGroupTypeManager.instance().isGroupType(groupEntity.getPeerId())
                .equals(ConstantValues.GROUP_TYPE_SHENHE)) {
            T.showShort(this, "不是审核群");
        } else if (groupEntity.getCreatorId() != IMLoginManager.instance()
                .getLoginId()) {
            T.showShort(this, "您不是管理员");
        } else {
            curTheSpell = THE_SPELL_GO;
            requestGroups();
        }
    }

    /**
     * 咒语去吧皮卡丘相关操作
     * <p/>
     * 获取剧详情中的 对戏  和 水聊 群，将@到的人移动到这两个群里
     */
    private void requestGroups() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        OkHttpClientManager.postAsy(ConstantValues.STORY_DETAILS, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    StoryDetailsBean sdb = new Gson()
                            .fromJson(response, StoryDetailsBean.class);
                    List<StoryDetailsBean.GroupsEntity> groupsEntities = sdb.getGroups();

                    String name = "";
                    Set<Integer> memberSet = new HashSet<>();
                    for (int i = 0; i < nameList.size(); i++) {
                        name += nameList.get(i);
                        memberSet.add(idList.get(i));
                    }

                    StoryDetailsBean.GroupsEntity waterChatGroup = new StoryDetailsBean().new GroupsEntity();  //水聊群
                    StoryDetailsBean.GroupsEntity playGroup = new StoryDetailsBean().new GroupsEntity();          //对戏群
                    /** 获取对戏群水聊群信息 */
                    for (StoryDetailsBean.GroupsEntity entity : groupsEntities) {
                        if (entity.getIsplay().equals("1")) {              //对戏群
                            playGroup = entity;
                        } else if (entity.getIsplay().equals("2")) {       //水聊群
                            waterChatGroup = entity;
                        }
                    }

                    IMGroupManager.instance().reqAddGroupMember(Integer.valueOf(waterChatGroup.getGroupid()), memberSet);
                    IMGroupManager.instance().reqAddGroupMember(Integer.valueOf(playGroup.getGroupid()), memberSet);

                    nameList.clear();
                    idList.clear();
                    textMessage = TextMessage
                            .buildForSend(name + "被管理员用咒语发送到了对戏水聊群", loginUser, peerEntity);
                    sendMessage();

                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    //add by guoq-s
    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                int changeType = event.getChangeType();
                switch (changeType) {
                    case DBConstant.GROUP_MODIFY_TYPE_DEL:
                        break;
                    case DBConstant.GROUP_MODIFY_TYPE_ADD:
                        if (curTheSpell == THE_SPELL_GO) { //移动成员至对戏  和 水聊 群失败
                            T.showShort(this, "移动失败");
                        }
                        break;
                    default:
                        Toast.makeText(this, getString(R.string.change_temp_group_failed), Toast.LENGTH_SHORT).show();
                        break;
                }
                curTheSpell = THE_SPELL_NONE;
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
                int changeType = event.getChangeType();
                switch (changeType) {
                    case DBConstant.GROUP_MODIFY_TYPE_DEL:
                        break;
                    case DBConstant.GROUP_MODIFY_TYPE_ADD:
                        if (curTheSpell == THE_SPELL_GO) { //移动成员至对戏  和 水聊 群
                            T.showShort(this, "小主V5，咒语发威，人已被成功转移");
                        } else {
                            //T.showShort(this, "删除成功");
                        }
                        curTheSpell = THE_SPELL_NONE;
                        break;
                }

                onMemberChangeSuccess(event);
            }
            break;
        }
    }

    /**
     * 收到删除群成员的通知
     *
     * @param event
     */
    private void onMemberChangeSuccess(GroupEvent event) {
        int groupId = event.getGroupEntity().getPeerId();
        if (groupId != peerEntity.getPeerId()) {
            return;
        }
        List<Integer> changeList = event.getChangeList();
        if (changeList == null || changeList.size() <= 0) {
            return;
        }
        int changeType = event.getChangeType();
        switch (changeType) {
            case DBConstant.GROUP_MODIFY_TYPE_DEL:
                Integer loginId = IMLoginManager.instance().getLoginId();
                if (changeList.contains(loginId)) {
                    //退出群的场合。
                    finish();
                    break;
                }
                break;
            case DBConstant.GROUP_MODIFY_TYPE_ADD:
                if (curTheSpell == THE_SPELL_GO) { //移动成员至对戏  和 水聊 群
                    T.showShort(this, "移动成功");
                }
                break;
        }
    }

    //add by guoq-e

    private static final int THE_SPELL_GO = 1; // 咒语  去吧皮卡丘
    private static final int THE_SPELL_DEL = 2; // 咒语  离开皮卡丘
    private static final int THE_SPELL_NONE = -1;  //咒语  空
    private int curTheSpell = THE_SPELL_NONE; // 当前咒语

    /**
     * audio状态的语音还在使用这个
     */
    protected void initAudioHandler() {
        uiHandler = new MyHandler(this);
    }


    /**
     * 网络获取群公告
     */
    private void getStoryGroupBoard(String latestTime, final GroupAnnouncementEntity groupAnnouncementEntity) {
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("groupid", groupId);
        params.addParams("latesttime", latestTime);     //获取最新的公告
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupBoard, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if ("1".equals(object.getString("result"))) {
                        JSONObject contentObject = object.getJSONArray("boardinfo").getJSONObject(0);
                        if (!TextUtils.isEmpty(contentObject.getString("title"))) {
                            final View view = LayoutInflater.from(DramaChatActivity.this).inflate(R.layout.group_announcement,
                                    (RelativeLayout) findViewById(R.id.rl_group_announcement));
                            ((TextView) view.findViewById(R.id.tv_title)).setText(contentObject.getString("title"));
                            ((TextView) view.findViewById(R.id.tv_content)).setText(contentObject.getString("content"));
                            SimpleDateFormat date = new SimpleDateFormat("yyyy年M月d日 h:mm");
                            String time = date.format(Long.valueOf(object.getString("datetime") + "000"));
                            ((TextView) view.findViewById(R.id.tv_date)).setText(time.split("日")[0] + "日");
                            ((TextView) view.findViewById(R.id.tv_time)).setText(time.split("日")[1]);
                            if (storyParentEntity != null) {
                                ((TextView) view.findViewById(R.id.tv_name)).setText(storyParentEntity.getCreatorname());
                            } else {
                                ((TextView) view.findViewById(R.id.tv_name)).setText("神秘人物");
                            }
                            view.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    IMUIHelper.openGroupAnnouncementActivity(DramaChatActivity.this, groupId);
                                }
                            });
                            view.findViewById(R.id.iv_delete).setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    view.findViewById(R.id.ll_group_announcement).setVisibility(View.GONE);
                                }
                            });
                            hideView(view);
                            groupAnnouncementEntity.setGroupId(groupId);
                            groupAnnouncementEntity.setTitle(contentObject.getString("title"));
                            groupAnnouncementEntity.setContent(contentObject.getString("content"));
                            groupAnnouncementEntity.setDateTime(object.getString("datetime"));
                            imService.getDbInterface().insertOrUpdateGroupAnnouncementEntity(groupAnnouncementEntity);
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    private void hideView(final View view) {
        uiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                view.findViewById(R.id.ll_group_announcement).setVisibility(View.GONE);
            }
        }, 10000);
    }

    private boolean showTask() {
        if (YXGroupTypeManager.instance().isShenHeGroup(Integer.valueOf(groupId)) &&
                !YXStoryManager.instance().isInDuixiGroup(groupId)) {
            final View view = LayoutInflater.from(DramaChatActivity.this).inflate(R.layout.activity_novice_task,
                    (RelativeLayout) findViewById(R.id.rl_group_announcement));
            view.findViewById(R.id.ll_task).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openRoleApplyActivity(DramaChatActivity.this, storyId);
                }
            });
            view.findViewById(R.id.iv_ignore).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    view.setVisibility(View.GONE);
                }
            });
            return true;
        }

        return false;
    }

    static class MyHandler extends Handler {
        WeakReference<DramaChatActivity> activityWeakReference;         //持有DramaChatActivity对象的弱引用

        MyHandler(DramaChatActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final DramaChatActivity activity = activityWeakReference.get();
            if (activity == null) {
                return;
            }
            super.handleMessage(msg);
            switch (msg.what) {
                case HandlerConstant.MSG_RECEIVED_MESSAGE:
                    MessageEntity entity = (MessageEntity) msg.obj;
                    activity.onMsgRecv(entity);
                    break;

                default:
                    break;
            }
        }
    }


    private class AsyncTaskLoading extends AsyncTaskBase {
        public AsyncTaskLoading() {
            super();
        }

        @Override
        protected Integer doInBackground(String... params) {
            initAlbumHelper();
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
        }
    }

}
