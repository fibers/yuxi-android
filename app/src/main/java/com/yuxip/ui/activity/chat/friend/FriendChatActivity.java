
package com.yuxip.ui.activity.chat.friend;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.DB.entity.LastChatEntity;
import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.PeerEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.DBConstant;
import com.yuxip.config.HandlerConstant;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.entity.TextMessage;
import com.yuxip.imservice.entity.UnreadEntity;
import com.yuxip.imservice.event.DelFriendEvent;
import com.yuxip.imservice.event.MessageEvent;
import com.yuxip.imservice.event.PriorityEvent;
import com.yuxip.imservice.event.SelectEvent;
import com.yuxip.imservice.manager.IMStackManager;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.protobuf.IMMessage;
import com.yuxip.thread.AsyncTaskBase;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.other.MainActivity;
import com.yuxip.ui.activity.other.PickPhotoActivity;
import com.yuxip.ui.adapter.album.AlbumHelper;
import com.yuxip.ui.adapter.album.ImageBucket;
import com.yuxip.ui.adapter.album.ImageItem;
import com.yuxip.ui.helper.AudioPlayerHandler;
import com.yuxip.ui.widget.FaceInputView;
import com.yuxip.utils.CommonUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * @Description 主消息界面
 * @date 2014-7-15
 * <p/>
 */
public class FriendChatActivity extends TTBaseActivity implements OnRefreshListener2<ListView>, View.OnClickListener {

    private PullToRefreshListView lvPTR = null;
    private TextView textView_new_msg_tip = null;
    private FriendChatAdapter adapter = null;
    private FaceInputView faceInputView;               //自定义表情键盘控件
    private EditText et_input;                          //自定义表情键盘的输入框
    private TextMessage textMessage;

    private List<ImageBucket> albumList = null;
    private String takePhotoSavePath = "";
    private Logger logger = Logger.getLogger(FriendChatActivity.class);
    private IMService imService;
    private UserEntity loginUser;
    private PeerEntity peerEntity;

    // 当前的session
    private String currentSessionKey;
    private int historyTimes = 0;
    private LastChatEntity lastChatEntity;
    private MyHandler uiHandler;
    private DBInterface dbInterface;

    /**
     * 全局Toast
     */
    private Toast mToast;

    public void showToast(int resId) {
        String text = getResources().getString(resId);
        if (mToast == null) {
            mToast = Toast.makeText(FriendChatActivity.this, text, Toast.LENGTH_SHORT);
        } else {
            mToast.setText(text);
            mToast.setDuration(Toast.LENGTH_SHORT);
        }
        mToast.setGravity(Gravity.CENTER, 0, 0);
        mToast.show();
    }

    public void cancelToast() {
        if (mToast != null) {
            mToast.cancel();
        }
    }

    @Override
    public void onBackPressed() {
        IMApplication.gifRunning = false;
        cancelToast();
        super.onBackPressed();
    }

    /**
     * end 全局Toast
     */
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            logger.d("message_activity#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
            initData();
            /**  初始化输入编辑框的内容     */
            initEditMessage();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        logger.d("message_activity#onCreate:%s", this);
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        dbInterface = DBInterface.instance();

        //得到frament 携带过来的sessionkey值
        currentSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);

        //初始化数据（相册,表情,数据库相关）
        new AsyncTaskLoading().execute();
        /** audio状态的语音还在使用这个 */
        initAudioHandler();
        initView();
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this, SysConstant.MESSAGE_EVENTBUS_PRIORITY);
        logger.d("message_activity#register im service and eventBus");
    }

    // 触发条件,imservice链接成功，或者newIntent
    private void initData() {
        historyTimes = 0;
        adapter.clearItem();
        ImageMessage.clearImageMessageList();
        loginUser = imService.getLoginManager().getLoginInfo();
        peerEntity = imService.getSessionManager().findPeerEntity(currentSessionKey);
        if (peerEntity == null) {
            imService.getContactManager().reqUsersByids(currentSessionKey.split("_")[1]);
            this.finish();
            return;
        }
        // 头像、历史消息加载、取消通知
        setTitleByUser();
        reqHistoryMsg();
        adapter.setImService(imService, loginUser);
        imService.getUnReadMsgManager().readUnreadSession(currentSessionKey);
        imService.getNotificationManager().cancelSessionNotifications(currentSessionKey);
    }


    /**
     * 本身位于Message页面，点击通知栏其他session的消息
     */
    @Override
    protected void onNewIntent(Intent intent) {
        logger.d("FriendChatActivity#onNewIntent:%s", this);
        super.onNewIntent(intent);
        setIntent(intent);
        historyTimes = 0;
        if (intent == null) {
            return;
        }
        String newSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);
        if (newSessionKey == null) {
            return;
        }
        logger.d("chat#newSessionInfo:%s", newSessionKey);
        if (!newSessionKey.equals(currentSessionKey)) {
            currentSessionKey = newSessionKey;
            dbInterface = DBInterface.instance();
            initData();
            /** 初始化输入编辑框的内容 */
            initEditMessage();
        }
    }

    @Override
    protected void onResume() {
        logger.d("message_activity#onresume:%s", this);
        super.onResume();
        IMApplication.gifRunning = true;
        historyTimes = 0;
        // not the first time
        if (imService != null) {
            // 处理session的未读信息
            handleUnreadMsgs();
        }
        /** 当activity之间进行切换时可能会发生数据的改变，如：添加新的表情标签 */
        faceInputView.refresh();
    }

    @Override
    protected void onDestroy() {
        logger.d("message_activity#onDestroy:%s", this);
        if (adapter != null) {
            historyTimes = 0;
            imServiceConnector.disconnect(this);
            EventBus.getDefault().unregister(this);
            adapter.clearItem();
            if (albumList != null) {
                albumList.clear();
            }
            ImageMessage.clearImageMessageList();
        }
        super.onDestroy();
    }

    /**
     * 设定聊天名称
     * 1. 如果是user类型， 点击触发UserProfile
     * 2. 如果是群组，检测自己是不是还在群中
     */
    private void setTitleByUser() {
        String nickName = FriendGroupManager.getInstance().getNickNameByUid(currentSessionKey.split("_")[1]);
        if(TextUtils.isEmpty(nickName)) {
            nickName = peerEntity.getMainName();
        }
        setTitle(nickName);
        int peerType = peerEntity.getType();
        switch (peerType) {
            case DBConstant.SESSION_TYPE_GROUP: {
                GroupEntity group = (GroupEntity) peerEntity;
                Set<Integer> memberLists = group.getlistGroupMemberIds();
                if (!memberLists.contains(loginUser.getPeerId())) {
                    Toast.makeText(FriendChatActivity.this, R.string.no_group_member, Toast.LENGTH_SHORT).show();
                }
            }
            break;
            case DBConstant.SESSION_TYPE_SINGLE: {
                topTitleTxt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
            }
            break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode)
            return;
        switch (requestCode) {
            case SysConstant.CAMERA_WITH_DATA:
                handleTakePhotoData(data);
                break;
            case SysConstant.ALBUM_BACK_DATA:
                logger.d("pic#ALBUM_BACK_DATA");
                setIntent(data);
                break;
            case SysConstant.ACTIVITY_RESULT_CODE_BOARD:
                String text = data.getStringExtra(IntentConstant.INPUT_CONTENT);
                et_input.setText(text);
                et_input.setSelection(text.length());
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void handleImagePickData(List<ImageItem> list) {
        ArrayList<ImageMessage> listMsg = new ArrayList<>();
        ArrayList<ImageItem> itemList = (ArrayList<ImageItem>) list;
        for (ImageItem item : itemList) {
            ImageMessage imageMessage = ImageMessage.buildForSend(item, loginUser, peerEntity);
            listMsg.add(imageMessage);
            pushList(imageMessage);
        }
        imService.getMessageManager().sendImages(listMsg);
    }


    /**
     * 删除好友的事件通知处理
     *
     * @param delFriendEvent 删除好友的事件
     */
    public void onEventMainThread(DelFriendEvent delFriendEvent) {
        IMMessage.IMMsgDelFriendNotify delFriendNotify = (IMMessage.IMMsgDelFriendNotify) delFriendEvent.object;
        int uid = delFriendNotify.getSessionId();
        if (("1_" + uid).equals(currentSessionKey)) {
            finish();
        }
    }

    public void onEventMainThread(SelectEvent event) {
        switch (event.getEvent()) {
            case TYPE_CHAT:
                List<ImageItem> itemList = event.getList();
                if (itemList != null && itemList.size() > 0) {
                    handleImagePickData(itemList);
                }
                break;
        }
    }


    public void onEventMainThread(MessageEvent event) {
        MessageEvent.Event type = event.getEvent();
        MessageEntity entity = event.getMessageEntity();
        switch (type) {
            case ACK_SEND_MESSAGE_OK: {
                onMsgAck(entity);
            }
            break;

            case ACK_SEND_MESSAGE_FAILURE:
                // 失败情况下新添提醒
                showToast(R.string.message_send_failed);
            case ACK_SEND_MESSAGE_TIME_OUT: {
                onMsgUnAckTimeoutOrFailure(entity);
            }
            break;

            case HANDLER_IMAGE_UPLOAD_FAILD: {
                logger.d("pic#onUploadImageFaild");
                ImageMessage imageMessage = (ImageMessage) entity;
                adapter.updateItemState(imageMessage);
                showToast(R.string.message_send_failed);
            }
            break;

            case HANDLER_IMAGE_UPLOAD_SUCCESS: {
                ImageMessage imageMessage = (ImageMessage) entity;
                adapter.updateItemState(imageMessage);
            }
            break;

            case HISTORY_MSG_OBTAIN: {
                if (historyTimes == 1) {
                    adapter.clearItem();
                    reqHistoryMsg();
                }
            }
            break;
        }
    }


    /**
     * [备注] DB保存，与session的更新manager已经做了
     *
     * @param messageEntity    messageEntity
     */
    private void onMsgAck(MessageEntity messageEntity) {
        logger.d("message_activity#onMsgAck");
        int msgId = messageEntity.getMsgId();
        logger.d("chat#onMsgAck, msgId:%d", msgId);

        /**到底采用哪种ID呐??*/
        long localId = messageEntity.getId();
        adapter.updateItemState(messageEntity);
    }


    private void handleUnreadMsgs() {
        // 清除未读消息
        UnreadEntity unreadEntity = imService.getUnReadMsgManager().findUnread(currentSessionKey);
        if (null == unreadEntity) {
            return;
        }
        int unReadCnt = unreadEntity.getUnReadCnt();
        if (unReadCnt > 0) {
            imService.getNotificationManager().cancelSessionNotifications(currentSessionKey);
            adapter.notifyDataSetChanged();
            scrollToBottomListItem();
        }
    }


    // 肯定是在当前的session内
    private void onMsgRecv(MessageEntity entity) {
        logger.d("message_activity#onMsgRecv");

        imService.getUnReadMsgManager().ackReadMsg(entity);
        logger.d("chat#start pushList");
        pushList(entity);
        ListView lv = lvPTR.getRefreshableView();
        if (lv != null) {

            if (lv.getLastVisiblePosition() < adapter.getCount()) {
                textView_new_msg_tip.setVisibility(View.VISIBLE);
            } else {
                scrollToBottomListItem();
            }
        }
    }


    private void onMsgUnAckTimeoutOrFailure(MessageEntity messageEntity) {
        logger.d("chat#onMsgUnAckTimeoutOrFailure, msgId:%s", messageEntity.getMsgId());
        // msgId 应该还是为0
        adapter.updateItemState(messageEntity);
    }


    /**
     * Description 初始化数据（相册,表情,数据库相关）
     */
    private void initAlbumHelper() {
        AlbumHelper albumHelper = AlbumHelper.getHelper(getApplicationContext());
        albumList = albumHelper.getImagesBucketList(false);
    }

    /**
     * Description 初始化界面控件
     * 有点庞大 todo
     */
    private void initView() {
        // 绑定布局资源(注意放所有资源初始化之前)
        topContentView.setBackgroundColor(getResources().getColor(R.color.chat_top_content_view));
        LayoutInflater.from(this).inflate(R.layout.xy_activity_message, topContentView);


        setLeftButton(R.drawable.back_default_btn);
        setRightButton(R.drawable.msg_family_h);
        topLeftBtn.setOnClickListener(this);
        topRightBtn.setOnClickListener(this);

        // 列表控件(开源PTR)
        lvPTR = (PullToRefreshListView) this.findViewById(R.id.message_list);
        textView_new_msg_tip = (TextView) this.findViewById(R.id.tt_new_msg_tip);
        lvPTR.getRefreshableView().addHeaderView(LayoutInflater.from(this).inflate(R.layout.tt_messagelist_header, lvPTR.getRefreshableView(), false));
        Drawable loadingDrawable = getResources().getDrawable(R.drawable.pull_to_refresh_indicator);
        final int indicatorWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 29,
                getResources().getDisplayMetrics());
        loadingDrawable.setBounds(new Rect(0, indicatorWidth, 0, indicatorWidth));
        lvPTR.getLoadingLayoutProxy().setLoadingDrawable(loadingDrawable);
        lvPTR.getRefreshableView().setCacheColorHint(Color.WHITE);
        lvPTR.getRefreshableView().setSelector(new ColorDrawable(Color.WHITE));

        adapter = new FriendChatAdapter(this);
        lvPTR.setAdapter(adapter);
        lvPTR.setOnRefreshListener(this);
        lvPTR.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true) {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        if (view.getLastVisiblePosition() == (view.getCount() - 1)) {
                            textView_new_msg_tip.setVisibility(View.GONE);
                        }
                        break;
                }
            }
        });
        lvPTR.getRefreshableView().setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                faceInputView.hideKeyborad();
                return false;
            }
        });
        textView_new_msg_tip.setOnClickListener(this);


        //LOADING
        View view = LayoutInflater.from(FriendChatActivity.this)
                .inflate(R.layout.tt_progress_ly, null);
        LayoutParams pgParms = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        pgParms.bottomMargin = 50;
        addContentView(view, pgParms);

        //自定义键盘的初始化
        faceInputView = (FaceInputView) findViewById(R.id.myFaceInputView);
        et_input = (EditText) faceInputView.getView(FaceInputView.MyViewEnum.EDITTEXT_INPUT);
        Button bt_biaoqing_send = (Button) faceInputView.getView(FaceInputView.MyViewEnum.BUTTON_BIAOQING_SEND);
        bt_biaoqing_send.setOnClickListener(this);
        TextView tv_text_send = (TextView) faceInputView.getView(FaceInputView.MyViewEnum.TEXTVIEW_TEXT_SEND);
        tv_text_send.setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.IMAGE_VIEW_TAKE_CAMERA).setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.IMAGE_VIEW_TAKE_PHOTO).setOnClickListener(this);
        faceInputView.getView(FaceInputView.MyViewEnum.BUTTON_GO_MIANBAN).setOnClickListener(this);
    }


    /**
     * 1.初始化请求历史消息
     * 2.本地消息不全，也会触发
     */
    private void reqHistoryMsg() {
        historyTimes++;
        List<MessageEntity> msgList = imService.getMessageManager().loadHistoryMsg(historyTimes, currentSessionKey, peerEntity);
        pushList(msgList);
        scrollToBottomListItem();
    }

    /**
     * @param msg   msg
     */
    public void pushList(MessageEntity msg) {
        logger.d("chat#pushList msgInfo:%s", msg);
        adapter.addItem(msg);
    }

    public void pushList(List<MessageEntity> entityList) {
        logger.d("chat#pushList list:%d", entityList.size());
        adapter.loadHistoryList(entityList);
    }


    @Override
    public void onConfigurationChanged(Configuration config) {
        super.onConfigurationChanged(config);
    }

    /**
     * @param data  data
     * Description 处理拍照后的数据
     * 应该是从某个 activity回来的
     */
    private void handleTakePhotoData(Intent data) {
        ImageMessage imageMessage = ImageMessage.buildForSend(takePhotoSavePath, loginUser, peerEntity);
        List<ImageMessage> sendList = new ArrayList<>(1);
        sendList.add(imageMessage);
        imService.getMessageManager().sendImages(sendList);
        // 格式有些问题
        pushList(imageMessage);
    }


    @Override
    public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
    }

    @Override
    public void onPullDownToRefresh(final PullToRefreshBase<ListView> refreshView) {
        // 获取消息
        refreshView.postDelayed(new Runnable() {
            @Override
            public void run() {
                ListView mList = lvPTR.getRefreshableView();
                int preSum = mList.getCount();
                MessageEntity messageEntity = adapter.getTopMsgEntity();
                if (messageEntity != null) {
                    List<MessageEntity> historyMsgInfo = imService.getMessageManager().loadHistoryMsg(messageEntity, historyTimes);
                    if (historyMsgInfo.size() > 0) {
                        historyTimes++;
                        adapter.loadHistoryList(historyMsgInfo);
                    }
                }

                int afterSum = mList.getCount();
                mList.setSelection(afterSum - preSum);
                refreshView.onRefreshComplete();
            }
        }, 200);
    }


    @Override
    public void onClick(View v) {
        final int id = v.getId();
        switch (id) {
            case R.id.bt_take_camera: {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePhotoSavePath = CommonUtil.getImageSavePath(String.valueOf(System
                        .currentTimeMillis())
                        + ".jpg");
                if(takePhotoSavePath != null) {
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(takePhotoSavePath)));
                    startActivityForResult(intent, SysConstant.CAMERA_WITH_DATA);
                    scrollToBottomListItem();
                }
            }
            break;
            case R.id.bt_take_photo: {
                if (albumList.size() < 1) {
                    Toast.makeText(FriendChatActivity.this,
                            getResources().getString(R.string.not_found_album), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                // 选择图片的时候要将session的整个回话 传过来
                Intent intent = new Intent(FriendChatActivity.this, PickPhotoActivity.class);
                intent.putExtra(IntentConstant.SESSION_KEY, currentSessionKey);
                startActivityForResult(intent, SysConstant.ALBUM_BACK_DATA);

                FriendChatActivity.this.overridePendingTransition(R.anim.tt_album_enter, R.anim.tt_stay);
                scrollToBottomListItem();
            }
            break;
            case R.id.bt_panel:
                break;
            case R.id.bt_biaoqing_send:
            case R.id.tv_text_send:
                /** 发送的内容 */
                String content = et_input.getText().toString();
                if (content.trim().equals("")) {
                    Toast.makeText(FriendChatActivity.this,
                            getResources().getString(R.string.message_null), Toast.LENGTH_LONG)
                            .show();
                    return;
                }
                /** 构建发送的message */
                textMessage = TextMessage.buildForSend(content, loginUser, peerEntity);
                sendMessage();
                break;
            case R.id.left_btn:
            case R.id.left_txt:
                actFinish();
                break;
            case R.id.right_btn:
                IMUIHelper.openUserHomePageActivity(this, currentSessionKey.split("_")[1]);
                break;
            case R.id.tt_new_msg_tip: {
                scrollToBottomListItem();
                textView_new_msg_tip.setVisibility(View.GONE);
            }
            break;
        }
    }


    /**
     * 发送消息
     */
    private void sendMessage() {
        /** 得到信息管理者，然后将信息发送出去 */
        imService.getMessageManager().sendText(textMessage);
        et_input.setText("");
        pushList(textMessage);
        scrollToBottomListItem();
    }


    @Override
    protected void onStop() {
        if (null != adapter) {
            adapter.hidePopup();
        }
        saveEditMessage();
        AudioPlayerHandler.getInstance().clear();
        super.onStop();
    }

    @Override
    protected void onStart() {
        logger.d("message_activity#onStart:%s", this);
        super.onStart();
    }


    /**
     * 滑动到列表底部
     */
    private void scrollToBottomListItem() {
        logger.d("message_activity#scrollToBottomListItem");

        // todo eric, why use the last one index + 2 can real scroll to the
        // bottom?
        ListView lv = lvPTR.getRefreshableView();
        if (lv != null) {
            lv.setSelection(adapter.getCount() + 1);
        }
        textView_new_msg_tip.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        logger.d("message_activity#onPause:%s", this);
        super.onPause();
    }

    /**
     * 退出
     */
    private void actFinish() {
        IMStackManager.getStackManager().popTopActivitys(MainActivity.class);
        IMApplication.gifRunning = false;
        FriendChatActivity.this.finish();
    }

    /**
     * 进入时读取草稿
     */
    private void initEditMessage() {
        lastChatEntity = dbInterface.getLastChatEntityByCurrentSessionKey(currentSessionKey);
        if (lastChatEntity != null && !TextUtils.isEmpty(lastChatEntity.getContent())) {
            et_input.setText(lastChatEntity.getContent());
        }
    }

    /**
     * 退出时保存草稿
     */
    private void saveEditMessage() {
        if (lastChatEntity == null) {
            lastChatEntity = new LastChatEntity();
            lastChatEntity.setCurrentSessionKey(currentSessionKey);
            lastChatEntity.setContent(et_input.getText().toString());
        } else {
            lastChatEntity.setContent(et_input.getText().toString());
        }
        dbInterface.insertOrUpdateLastChatEntity(lastChatEntity);
    }

    /**
     * audio状态的语音还在使用这个
     */
    protected void initAudioHandler() {
        uiHandler = new MyHandler(this);
    }

    /**
     * 背景: 1.EventBus的cancelEventDelivery的只能在postThread中运行，而且没有办法绕过这一点
     * 2. onEvent(A a)  onEventMainThread(A a) 这个两个是没有办法共存的
     * 解决: 抽离出那些需要优先级的event，在onEvent通过handler调用主线程，
     * 然后cancelEventDelivery
     * <p/>
     * todo  need find good solution
     */
    public void onEvent(PriorityEvent event) {
        switch (event.event) {
            case MSG_RECEIVED_MESSAGE: {
                MessageEntity entity = (MessageEntity) event.object;
                /**正式当前的会话*/
                if (currentSessionKey.equals(entity.getSessionKey())) {
                    Message message = Message.obtain();
                    message.what = HandlerConstant.MSG_RECEIVED_MESSAGE;
                    message.obj = entity;
                    uiHandler.sendMessage(message);
                    EventBus.getDefault().cancelEventDelivery(event);
                }
            }
            break;
        }
    }


    static class MyHandler extends Handler {
        WeakReference<FriendChatActivity> activityWeakReference;         //持有DramaChatActivity对象的弱引用

        MyHandler(FriendChatActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final FriendChatActivity activity = activityWeakReference.get();
            if (activity == null) {
                return;
            }
            super.handleMessage(msg);
            switch (msg.what) {
                case HandlerConstant.MSG_RECEIVED_MESSAGE:
                    MessageEntity entity = (MessageEntity) msg.obj;
                    activity.onMsgRecv(entity);
                    break;
                default:
                    break;
            }
        }
    }

    private class AsyncTaskLoading extends AsyncTaskBase {
        public AsyncTaskLoading( ) {
            super();
        }

        @Override
        protected Integer doInBackground(String... params) {
            initAlbumHelper();
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
        }
    }
}
