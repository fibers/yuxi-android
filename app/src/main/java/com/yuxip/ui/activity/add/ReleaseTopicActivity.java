package com.yuxip.ui.activity.add;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.event.CreateStoryTopicEvent;
import com.yuxip.imservice.event.SelectEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.thread.AsyncTaskBase;
import com.yuxip.ui.activity.other.PickPhotoActivity;
import com.yuxip.ui.adapter.album.AlbumHelper;
import com.yuxip.ui.adapter.album.ImageBucket;
import com.yuxip.ui.adapter.album.ImageItem;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.customview.ReleaseTopicPicsView;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by ZQF on 2015/10/11.
 * 发布新话题
 */
public class ReleaseTopicActivity extends Activity implements View.OnClickListener, View.OnTouchListener {

    private static Logger logger = Logger.getLogger(ReleaseTopicActivity.class);

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final String IMAGE_UNSPECIFIED = "image/*";

    private Context mContext;

    private EditText mTitleEt, mContentEt;                                     // 标题,内容
    private TextView mCancleTv, mReleaseTv, mCountTv, mCountPointTv;           // 取消,发布,1/9,红点
    private RelativeLayout mUploadRl;                                          //
    private ReleaseTopicPicsView mPicDetialLl;                                 // 底部布局,图片列表布局
    private HorizontalScrollView mHoScrollView;
    private boolean isClickable = false;                                       // 全局点击事件控制
    private int picCount = 0;
    private String mTitle, mContent, mImageUrls;
    private ProgressBar my_progress_bar;
    private List<ImageBucket> albumList = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic_release);
        mContext = this;

        EventBus.getDefault().register(this);
        new AsyncTaskLoading().execute();
        initView();
        addListeners();
    }

    private void addListeners() {
        mCancleTv.setOnClickListener(this);
        mReleaseTv.setOnClickListener(this);
        mUploadRl.setOnClickListener(this);

        mTitleEt.setOnTouchListener(this);
        mContentEt.setOnTouchListener(this);
        mTitleEt.addTextChangedListener(mTextChangeListener);
        mContentEt.addTextChangedListener(mTextChangeListener);
    }

    private void initView() {
        mCancleTv = (TextView) findViewById(R.id.tv_cancle);
        mReleaseTv = (TextView) findViewById(R.id.tv_release);
        my_progress_bar = (ProgressBar) findViewById(R.id.my_progress_bar);

        mTitleEt = (EditText) findViewById(R.id.et_topic_title);
        mContentEt = (EditText) findViewById(R.id.et_topic_content);
        mCountTv = (TextView) findViewById(R.id.tv_pic_count);
        mCountPointTv = (TextView) findViewById(R.id.tv_pic_count_point);
        mHoScrollView = (HorizontalScrollView) findViewById(R.id.hsv_topic_release);
        mUploadRl = (RelativeLayout) findViewById(R.id.rl_pic_upload);
        mPicDetialLl = (ReleaseTopicPicsView) findViewById(R.id.ll_pic_upload_detail);
        mPicDetialLl.setAddClickListener(new ReleaseTopicPicsView.CountChangeListener() {
            @Override
            public void onClick() {
                if (albumList == null || albumList.size() < 1) {
                    T.show(ReleaseTopicActivity.this, getResources().getString(R.string.not_found_album), Toast.LENGTH_LONG);
                    return;
                }
                showAddImgDialog();
            }

            @Override
            public void onChange() {
                addImage();
            }
        });
        mPicDetialLl.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        /** 隐藏软键盘 */
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mTitleEt.getWindowToken(), 0);
        switch (v.getId()) {
            case R.id.tv_cancle:
                finish();
                break;
            case R.id.tv_release:
                mPicDetialLl.closePicsLayout();
                createTopic();
                break;
            case R.id.rl_pic_upload: // 显示|隐藏图片布局
                if (mPicDetialLl.getVisibility() == View.VISIBLE) {
                    mPicDetialLl.setVisibility(View.GONE);
                } else {
                    mPicDetialLl.setVisibility(View.VISIBLE);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mPicDetialLl.closePicsLayout();
                break;
        }
        return false;
    }

    private void createTopic() {
        mTitle = mTitleEt.getText().toString();
        mContent = mContentEt.getText().toString();
        if (TextUtils.isEmpty(mTitle) || TextUtils.isEmpty(mContent)) {
            T.showShort(mContext, "标题和内容不能为空");
            return;
        }
        mReleaseTv.setClickable(false); // 禁用点击事件，避免连续多次点击
        my_progress_bar.setVisibility(View.VISIBLE);
        List<String> listUrls = mPicDetialLl.getListUrls();
        if(listUrls==null){
            mReleaseTv.setClickable(true);
            T.showShort(mContext, "有图片未上传成功,请重新上传");
            if(mPicDetialLl.getVisibility() == View.GONE){
                mPicDetialLl.setVisibility(View.VISIBLE);
            }
            my_progress_bar.setVisibility(View.GONE);
            return;
        }

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("title", mTitle);
        params.addParams("token", "1");
        params.addParams("content", mContent);
        /** 拼接images字符串 */
        StringBuilder builder = new StringBuilder();
        if (listUrls.size() > 0) {
            builder.append("{\"images\": [");
            boolean isFirst = true;
            for (int i =0 ; i<listUrls.size();i++) {
                if ("".equals(listUrls.get(i))) {
                    T.showShort(mContext, "有图片未上传成功,点击提示重传");
                    return;
                } else {
                    if (isFirst) {
                        isFirst = false;
                    } else {
                        builder.append(",");
                    }
                    builder.append("\"" + listUrls.get(i) + "\"");
                }
            }
            builder.append("]}");
        } else {
            builder.append("{}");
        }
        params.addParams("images", builder.toString());

        OkHttpClientManager.postAsy(ConstantValues.CREATE_NEW_TOPIC, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onError(Request request, Exception e) {
                mReleaseTv.setClickable(true);
                T.show(getApplicationContext(), "发布失败", 0);
                my_progress_bar.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onResponse(String response) {
                mReleaseTv.setClickable(true);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String result = jsonObject.getString("result");
                    if ("100".equals(result)) {
                        CreateStoryTopicEvent event = new CreateStoryTopicEvent();
                        event.eventType = CreateStoryTopicEvent.Event.TYPE_CREATE_TOPIC;
                        EventBus.getDefault().post(event);
                        T.showLong(mContext, jsonObject.getString("describe"));
                        finish();
                    } else {
                        T.showShort(mContext, jsonObject.getString("describe"));
                    }
                    my_progress_bar.setVisibility(View.INVISIBLE);
                } catch (JSONException e) {
                    logger.e(e.toString());
                    my_progress_bar.setVisibility(View.INVISIBLE);
                    T.showShort(mContext, "未知错误，请重试！");
                }
            }
        });
    }

    /**
     * 弹出添加图片的dialog
     */
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(mContext, "发起话题", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        /*Intent intent = new Intent(Intent.ACTION_PICK, null);
                        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                        startActivityForResult(intent, PHOTOZOOM);*/
                        Intent intent = new Intent(ReleaseTopicActivity.this, PickPhotoActivity.class);
                        intent.putExtra(IntentConstant.SESSION_KEY, "");
                        startActivityForResult(intent, PHOTOZOOM);
                        ReleaseTopicActivity.this.overridePendingTransition(R.anim.tt_album_enter, R.anim.tt_stay);
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }

    /**
     * 监听内容变化
     */
    private TextWatcher mTextChangeListener = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String title = mTitleEt.getText().toString().trim();
            String content = mContentEt.getText().toString().trim();
            if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(content)) {
                mReleaseTv.setTextColor(Color.WHITE);
                mReleaseTv.setBackgroundResource(R.drawable.topic_release_btn_blue);
                mReleaseTv.setClickable(true);
            } else {
                mReleaseTv.setTextColor(mContext.getResources().getColor(R.color.topic_message_text_1));
                mReleaseTv.setBackgroundResource(R.drawable.topic_release_btn_white);
                mReleaseTv.setClickable(false);
            }
            if (!TextUtils.isEmpty(title) && title.length() >= 30) {
                Toast.makeText(mContext, "标题要少于30字", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {
            return;
        }
        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
                mPicDetialLl.addImage(data);
                break;
            case PHOTOZOOM:   //选图片
                setIntent(data);
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 添加图片到布局
     */
    private void addImage() {
        picCount = mPicDetialLl.getPicCount();
        if (picCount > 0) {
            mCountPointTv.setText(String.valueOf(picCount));
            if (mCountPointTv.getVisibility() != View.VISIBLE) {
                mCountPointTv.setVisibility(View.VISIBLE);
            }
            mCountPointTv.setText(String.valueOf(picCount));
        } else {
            mCountPointTv.setVisibility(View.GONE);
        }

    }

    public void onEventMainThread(SelectEvent event) {
        switch (event.getEvent()) {
            case TYPE_RELEASE_TOPIC:
                List<ImageItem> itemList = event.getList();
                if (itemList != null && itemList.size() > 0) {
                    for (int i=0; i<itemList.size(); i++) {
                        String picPath = itemList.get(i).getImagePath();
                        if(picPath == null) {
                            picPath = itemList.get(i).getThumbnailPath();
                        }
                        mPicDetialLl.addImages(picPath);
                    }
                }
                break;
        }
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        if (albumList != null) {
            albumList.clear();
        }
        ImageMessage.clearImageMessageList();
        mPicDetialLl.recycleAllImages();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
//        if (isPicsVisible) {
//            /** 如果图片布局显示,则隐藏,否则正常back */
//            closePicsLayout();
//            return;
//        }
        super.onBackPressed();
    }


    private class AsyncTaskLoading extends AsyncTaskBase {
        public AsyncTaskLoading() {
            super();
        }

        @Override
        protected Integer doInBackground(String... params) {
            initAlbumHelper();
            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
        }
    }


    /**
     * 初始化数据（相册,表情,数据库相关）
     */
    private void initAlbumHelper() {
        ImageMessage.clearImageMessageList();
        AlbumHelper albumHelper = AlbumHelper.getHelper(getApplicationContext());
        albumList = albumHelper.getImagesBucketList(false);
    }


}
