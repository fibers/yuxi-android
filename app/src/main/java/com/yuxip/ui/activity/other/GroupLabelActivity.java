package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryRolesJsonBean;
import com.yuxip.JsonBean.UserRoleInfoInStoryJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.GroupLabelAdapter;
import com.yuxip.ui.customview.NoScrollGridView;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/12.
 * description : 设置群名片
 */
public class GroupLabelActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(GroupLabelActivity.class);

    private View curView;
    @InjectView(R.id.rel_top_grouplabel_activity)
    RelativeLayout relTop;

    @InjectView(R.id.edit_name_grouplabel_activity)
    EditText editName;

    @InjectView(R.id.edit_content_grouplabel_activity)
    EditText editContent;

    @InjectView(R.id.iv_selsect_grouplabel_activity)
    ImageView ivArror;

    @InjectView(R.id.tv_title_grouplabel_activity)
    TextView tvTitle;

    @InjectView(R.id.gv_grouplabel_activity)
    NoScrollGridView gridView;

    private GroupLabelAdapter adapter;

    /**
     * 用户的信息
     */
    private UserRoleInfoInStoryJsonBean userRoleInfo;
    private UserRoleInfoInStoryJsonBean.RoleinfoEntity roleinfoEntity;

    /**
     * 皮表相关
     */
    private StoryRolesJsonBean storyRoles;
    private List<StoryRolesJsonBean.RolesEntity> listRoles = new ArrayList<>();

    private String uid;
    private String storyid;

    public static String roleId;
    private String roletitle;
    private String roleName;
    private String roleContent;

    private String roleTemplate;

    /**
     * 用以记录下拉箭头状态  true 展示pop  false
     */
    public static boolean isSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        uid = IMLoginManager.instance().getLoginId() + "";
        initView();
        initListener();
        initRoleData();
        initRoleListData();
//        getRoleTemplate();
    }


    private void initView() {
        curView = LayoutInflater.from(this).inflate(R.layout.activity_mygrouplabel, topContentView);
        ButterKnife.inject(this, curView);

//        popupWindow=new GroupLablePopupWindow(this,ivArror);
        setTitle("我的群名片");
        setRighTitleText("保存");
        setLeftButton(R.drawable.back_default_btn);

        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editName.getText().toString()) || TextUtils.isEmpty(editContent.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "内容不能为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(roleId)) {
                    Toast.makeText(getApplicationContext(), "尚未选择皮表", Toast.LENGTH_SHORT).show();
                    return;
                }
                Toast.makeText(getApplicationContext(), "保存中...", Toast.LENGTH_SHORT).show();
                modifyMyRole();
            }
        });


    }

    private void initGridView() {
        adapter = new GroupLabelAdapter(this, listRoles);
        adapter.setGridView(gridView);
        gridView.setAdapter(adapter);
    }

    private void initListener() {

        /** ################# 编辑名片名称 #######################*/
        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        /**###################  编辑名片内容  ################################*/
        editContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ivArror.setOnClickListener(this);


    }


    /**
     * ################# 初始化个人的人设信息 ###########################
     */
    private void initRoleData() {

        if (TextUtils.isEmpty(storyid)) {
            Toast.makeText(getApplicationContext(), "数据异常，请重试", Toast.LENGTH_LONG).show();
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetUserRoleInfoInStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveHttpData(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), "网络不给力", 1);
            }
        });
    }


    private void onReceiveHttpData(String json) {
        userRoleInfo = new Gson().fromJson(json, UserRoleInfoInStoryJsonBean.class);
        if (userRoleInfo == null) {
            Toast.makeText(getApplicationContext(), "数据解析异常", Toast.LENGTH_LONG).show();
            return;
        }
        if (userRoleInfo.getResult().equals("1")) {
            roleinfoEntity = userRoleInfo.getRoleinfo();
            roleId = roleinfoEntity.getRoleid();
            roletitle = roleinfoEntity.getRoletitle();
            roleName = roleinfoEntity.getUserrolename();
            roleContent = roleinfoEntity.getUserroleinfo();
            if (!TextUtils.isEmpty(roletitle))
                tvTitle.setText(roletitle);
            if (!TextUtils.isEmpty(roleName))
                editName.setText(roleName);
            if (!TextUtils.isEmpty(roleContent)) {
                editContent.setText(roleContent);
            } else {
                getRoleTemplate();
            }
        }

    }


    /**
     * ################# 初始化皮表列表  ###########################
     */
    private void initRoleListData() {
        if (TextUtils.isEmpty(storyid))
            return;

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoles, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveRoleList(response);
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    private void onReceiveRoleList(String json) {
        storyRoles = new Gson().fromJson(json, StoryRolesJsonBean.class);
        if (storyRoles == null)
            return;

        if (storyRoles.getResult().equals("1")) {
            listRoles = storyRoles.getRoles();
//            initPopupWindow();
            initGridView();
        }

    }


    /**
     * ################# 提交修改个人皮表信息 ###########################
     */
    private void modifyMyRole() {

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyid);
        if (TextUtils.isEmpty(roleId)) {
            Toast.makeText(getApplicationContext(), "尚未选择皮表", Toast.LENGTH_LONG).show();
            return;
        }
        params.addParams("roleid", roleId);
        params.addParams("userrolename", editName.getText().toString());
        params.addParams("userroleinfo", editContent.getText().toString());
        OkHttpClientManager.postAsy(ConstantValues.SetUserRoleInfoInStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.optString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_LONG).show();
                        GroupLabelActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "保存失败", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), "网络异常", 1);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_selsect_grouplabel_activity:
                if (adapter == null || listRoles == null || listRoles.size() == 0) {
                    Toast.makeText(getApplicationContext(), "该剧尚未设置皮表", Toast.LENGTH_LONG).show();
                    return;
                }
                if (isSelected) {
                    isSelected = false;
                    gridView.setVisibility(View.GONE);
                } else {
                    isSelected = true;
                    gridView.setVisibility(View.VISIBLE);
                }
                ivArror.setSelected(isSelected);
                break;
        }
    }


    private void getRoleTemplate() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleTemplate, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject json = new JSONObject(response);
                    if (json.optString("result", "-1").equals("1")) {
                        roleTemplate = json.optString("content", "");
                        if (!TextUtils.isEmpty(roleTemplate)) {
                            editContent.setText(roleTemplate);
                        } else {
                            if (!TextUtils.isEmpty(json.optString("describe", ""))) {
                                Toast.makeText(getApplicationContext(), json.optString("describe"), Toast.LENGTH_LONG).show();
                            } else {
                                Toast.makeText(getApplicationContext(), "该剧尚未设置模版", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            roleId = data.getStringExtra("roleid");

            String role = data.getStringExtra("role");
            if (!TextUtils.isEmpty(role))
                tvTitle.setText(role);
        }
    }


}
