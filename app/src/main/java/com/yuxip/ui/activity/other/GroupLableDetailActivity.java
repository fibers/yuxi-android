package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseActivity;

/**
 * Created by ly on 2015/8/12.
 */
public class GroupLableDetailActivity extends TTBaseActivity {
    private EditText et_num;
    private EditText et_name;
    private EditText et_content;
//    private String storyid;
    private String roleId;
    private String num;
    private String title;
    private String name;
    private String intro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_persion_main_add, topContentView);
        et_num = (EditText) findViewById(R.id.et_num);
        et_name = (EditText) findViewById(R.id.et_title);
        et_content = (EditText) findViewById(R.id.et_content);
//        storyid = getIntent().getStringExtra("story_id");
        num = getIntent().getStringExtra("num");
        title = getIntent().getStringExtra("title");
        name=getIntent().getStringExtra("name");
        intro = getIntent().getStringExtra("content");
        roleId=getIntent().getStringExtra("roleid");
        et_num.setText(num);
        et_name.setText(name);
        et_content.setText(intro);

        et_num.setEnabled(false);
        et_content.setEnabled(false);
        et_name.setEnabled(false);

        /** 顶部导航栏 */
        setTitle(title);
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("完成");
        setTitle("皮表");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.putExtra("roleid",roleId);
                intent.putExtra("role",title);
                setResult(RESULT_OK,intent);
                finish();
            }
        });
    }

}
