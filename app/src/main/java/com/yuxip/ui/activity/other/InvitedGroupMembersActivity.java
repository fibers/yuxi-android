package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.FriendGroups;
import com.yuxip.JsonBean.FriendsJsonBean;
import com.yuxip.JsonBean.GroupData;
import com.yuxip.JsonBean.GroupDataJsonBean;
import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.GroupMemberInviteAdapter;
import com.yuxip.ui.adapter.InviteMembersAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2015/6/5.
 */
public class InvitedGroupMembersActivity extends TTBaseNewActivity {
    private View curView;
    @InjectView(R.id.memberslist)
    ListView memberslist;

    @InjectView(R.id.topRightAdd)
    TextView topRightAdd;

    @InjectView(R.id.tv_invited_friends)
    TextView tv_invited_friends;

    @InjectView(R.id.tv_invited_Members)
    TextView tv_invited_Members;

    @InjectView(R.id.black)
    ImageView tv_back;

    private InviteMembersAdapter adapter1;
    private List<FriendGroups> friendGroups = null;
    private IMService imService;
    private String groupid;
    private String storyid;
    private String loginId;
    private List<Members> list = new ArrayList<>();
    private GroupMemberInviteAdapter adapter;
    private Boolean isFriend = false;
    private String shenheId;
    private int color_pink;
    private int color_black;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();

        }
    };


    @Override
    public void onDestroy() {
        ButterKnife.reset(this);
        imServiceConnector.disconnect(this);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        imServiceConnector.connect(this);
        setContentView(R.layout.activity_invite_shenhe_members);
        ButterKnife.inject(InvitedGroupMembersActivity.this);
        initRes();
        //初始化好友列表
        tv_invited_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFriend = true;
                tv_invited_friends.setTextColor(color_pink);
                tv_invited_Members.setTextColor(color_black);
                initData();
            }
        });
        //初始化审核群列表
        tv_invited_Members.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isFriend = false;
                tv_invited_friends.setTextColor(color_black);
                tv_invited_Members.setTextColor(color_pink);
                initData();
            }
        });
    }

    /**
     * 初始化资源
     */
    private void initRes() {
        color_black = getResources().getColor(R.color.black);
        color_pink = getResources().getColor(R.color.pink);
        loginId = IMLoginManager.instance().getLoginId() + "";
        tv_invited_Members.setTextColor(color_pink);
        tv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        topRightAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFriend) {
                    //邀请好友加入对戏群
                    if (adapter.getCheckListSet().size() <= 0) {
                        Toast.makeText(getApplicationContext(), "请选择添加成员", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Set<Integer> checkListSet = adapter.getCheckListSet();
                    imService.getMessageManager().sendMsgGroupAddMemberReq(0,
                            Integer.valueOf(groupid),
                            checkListSet);

                    finish();
                } else {
                    // 邀请审核群成员
                    if (adapter1.getCheckListSet().size() <= 0) {
                        Toast.makeText(getApplicationContext(), "请选择添加成员", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Set<Integer> checkListSet = adapter1.getCheckListSet();
                    //TODO 改成不需要别人同意
                    imService.getGroupManager().reqAddGroupMember(
                            Integer.valueOf(groupid),
                            checkListSet);

                    Toast.makeText(getApplicationContext(), R.string.drag_into_group, Toast.LENGTH_SHORT).show();
                    finish();

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        groupid = getIntent().getStringExtra("groupid");
        storyid = getIntent().getStringExtra("storyid");
        shenheId = getIntent().getStringExtra("shenheId");
        initData();

    }

    private void initData() {
        if (isFriend) {
            GetFriendList();
        } else {
            GetStoryGroupInfo();
        }
    }

    /**
     * 获取审核群列表
     */
    private void GetStoryGroupInfo() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyid);
        params.addParams("groupid", shenheId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                GroupDataJsonBean jsonBean = new Gson().fromJson(response, GroupDataJsonBean.class);
                if (jsonBean.getResult().equals("1")) {
                    GroupData data = jsonBean.getGroupData();
                    list = data.getMembers();
                    resetList1();
                    removeMemberExists();
                    adapter1 = new InviteMembersAdapter(InvitedGroupMembersActivity.this, list);
                    memberslist.setAdapter(adapter1);
                    memberslist.setOnScrollListener(
                            new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    /**
     * ---------------------------  群成员添加列表 移除自己---------------------------------------------------------   start    --- -------------
     */

    private void resetList1() {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getId().equals(loginId)) {
                list.remove(i);
                break;
            }
        }
    }
    /**--------------------------------------------------------------------------------------------   end    --- -------------*/

    /**
     * 获取好友列表
     */
    private void GetFriendList() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        OkHttpClientManager.postAsy(ConstantValues.GETFRIENDLIST, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        FriendsJsonBean friendsBean = new Gson().fromJson(response, FriendsJsonBean.class);
                        if (friendsBean.getResult().equals("1")) {
                            friendGroups = friendsBean.getGroups();
                            adapter = new GroupMemberInviteAdapter(InvitedGroupMembersActivity.this, friendGroups, groupid);
                            memberslist.setAdapter(adapter);
                            memberslist.setOnScrollListener(
                                    new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                    }
                }
        );
    }


    /**
     * ---------------------------  群成员添加列表 导入审核群成员  移除已经在对戏群的成员  ---------------------------------------------------------   start    --- -------------
     */

    private boolean isGroupMember(Members members) {
        if (TextUtils.isEmpty(groupid))
            return false;
        Set<Integer> set = IMGroupManager.instance().getGroupMember(groupid);
        for (int id : set) {
            if (Integer.valueOf(members.getId()) == id) {
                return true;
            }
        }
        return false;
    }


    private void removeMemberExists() {
        List<Members> list_member = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            if (!isGroupMember(list.get(i))) {
                list_member.add(list.get(i));
            }
        }
        list.clear();
        list.addAll(list_member);
    }
    /**--------------------------------------------------------------------------------------------   end    --- -------------*/
}
