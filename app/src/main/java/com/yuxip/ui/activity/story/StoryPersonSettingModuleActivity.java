package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * created by SummerRC on 2015/8/12.
 * description : 人设模板
 */
public class StoryPersonSettingModuleActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryPersonSettingModuleActivity.class);

    private EditText et_content;
    private String storyId;
    private String loginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_story_introk, topContentView);
        et_content = (EditText) findViewById(R.id.et_rule);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        et_content.setLayoutParams(params);
        et_content.setHint("人设模版");
//        et_content.setBackgroundResource(R.drawable.activity_his_group_card_line);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("编辑人设模版");

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        loginId = IMLoginManager.instance().getLoginId() + "";
        getInfo();
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String storyIntroContent = et_content.getText().toString().trim();
                if (TextUtils.isEmpty(storyIntroContent)) {
                    Toast.makeText(getApplicationContext(), "请完善信息", Toast.LENGTH_SHORT).show();
                } else {
                    /** 修改简介 */
                    changeInfo();
                }
            }
        });

    }

    /**
     * 修改简介
     */
    private void changeInfo() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyId);
        params.addParams("content", et_content.getText().toString().trim());
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyStoryRoleTemplate, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), obj.getString("describe"), Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), obj.getString("describe"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }


    /**
     * 获取人设模版，如果已经设置过的话
     */
    private void getInfo() {
        if(TextUtils.isEmpty(loginId)||TextUtils.isEmpty(storyId))
            return;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyId);
        /***
         *  网络请求，并填充到布局中
         */
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleTemplate, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        String content = obj.optString("content", "");
                        et_content.setText(content);
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }
}
