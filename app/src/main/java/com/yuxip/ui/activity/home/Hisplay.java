package com.yuxip.ui.activity.home;

import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HisplayResult;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXGroupTypeManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.BuddyAdapter;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by zl on 2015/7/1.
 * description : 他的剧
 */
public class Hisplay extends TTBaseActivity {
    private LinkedList<HisplayResult.StorysEntity> courseGroupList = new LinkedList<HisplayResult.StorysEntity>();
    private List<LinkedList<HisplayResult.StorysEntity.GroupsEntity>> childArray = new ArrayList<>();
    private ExpandableListView expandablelistview;
    private String uid;
    private ExpandableListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View.inflate(Hisplay.this, R.layout.activity_hisplay, topContentView);
        uid = IMLoginManager.instance().getLoginId() + "";
        initRes();
        ReqHisplay();
    }

    private void ReqHisplay() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GETMYSTROY, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        onRecentHisplayDataReady(response);
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), e.toString(), 1);
                    }
                }
        );
    }

    /**
     * 获得他喜欢的剧
     */
    private void onRecentHisplayDataReady(String result) {
        HisplayResult hisplayData = new Gson().fromJson(result, HisplayResult.class);
        courseGroupList.clear();
        childArray.clear();
        //请求成功
        if (hisplayData.getResult().equals("1")) {
            //获取剧的对象的集合
            LinkedList<HisplayResult.StorysEntity> data = hisplayData.getStorys();
            //把群组信息添加到集合里面
            for (int index = data.size() - 1; index >= 0; index--) {
                HisplayResult.StorysEntity entity = data.get(index);
                LinkedList<HisplayResult.StorysEntity.GroupsEntity> groupLists = entity.getGroups();
                //只是有一个评论群，说明不是剧的成员，只是评论过，需要移除。
                if (groupLists.size() <= 1) {
                    HisplayResult.StorysEntity.GroupsEntity groupsEntity = groupLists.get(0);

                    if (YXGroupTypeManager.instance()
                            .isCommentGroup(Integer.valueOf(groupsEntity.getGroupid()))) {
                        data.remove(entity);
                        continue;
                    }
                }
                for (int iindex = groupLists.size() - 1; iindex >= 0; iindex--) {
                    HisplayResult.StorysEntity.GroupsEntity group = groupLists.get(iindex);
                    if (group.getIsplay().equals(ConstantValues.GROUP_TYPE_COMMENT)) {
                        groupLists.remove(group);
                    }

                }
                childArray.add(groupLists);
                courseGroupList.add(entity);
            }
            //courseGroupList.addAll(data);
            adapter = new BuddyAdapter(this, courseGroupList, childArray);
            expandablelistview.setAdapter(adapter);
        }

    }

    private void initRes() {
        setLeftButton(R.drawable.back_default_btn);
        setTitle("剧");
        expandablelistview = (ExpandableListView) findViewById(R.id.buddy_expandablelistview);
        expandablelistview.setGroupIndicator(null);
        int groupCount = expandablelistview.getCount();
        for (int i = 0; i < groupCount; i++) {
            expandablelistview.expandGroup(i);
        }
        //分组展开
        expandablelistview.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            public void onGroupExpand(int groupPosition) {
            }
        });
        //分组关闭
        expandablelistview
                .setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
                    public void onGroupCollapse(int groupPosition) {
                    }
                });
        //子项单击
        expandablelistview.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            public boolean onChildClick(ExpandableListView arg0, View arg1,
                                        int groupPosition, int childPosition, long arg4) {
                HisplayResult.StorysEntity.GroupsEntity group = childArray.get(groupPosition).get(childPosition);
                HisplayResult.StorysEntity storysEntity = courseGroupList.get(groupPosition);
                IMUIHelper.openStoryGroupDetailsActivity(Hisplay.this, storysEntity.getId(), group.getGroupid(), false);
                return false;
            }
        });
    }
}
