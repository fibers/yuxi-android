package com.yuxip.ui.activity.story;


import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.base.TTBaseActivity;

/**
 * Created by Administrator on 2015/6/2.
 * description:版权信息
 */
public class CopyRightActivity extends TTBaseActivity {
    private View curView;
    private WebView webView;
    private String storyid;
    private String copyRightUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        curView = View.inflate(CopyRightActivity.this, R.layout.fragment_read_story, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        setTitle("版权");
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        copyRightUrl = getIntent().getStringExtra(IntentConstant.CORY_RIGHT_URL);
    }


    @Override
    public void onResume() {
        super.onResume();
        initWebView();
    }

    private void initWebView() {
        webView = (WebView) curView.findViewById(R.id.webView);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        if (null != copyRightUrl) {
            webView.loadUrl(copyRightUrl);
        }
    }
}
