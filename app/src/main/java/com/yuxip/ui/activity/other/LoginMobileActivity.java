package com.yuxip.ui.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.im.Security;
import com.squareup.okhttp.Request;
import com.tencent.connect.UserInfo;
import com.tencent.connect.common.Constants;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.Tencent;
import com.tencent.tauth.UiError;
import com.yuxip.DB.sp.SystemConfigSp;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.UrlConstant;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * 手机登陆
 * Created by Administrator on 2015/5/13.
 */
public class LoginMobileActivity extends TTBaseNewActivity {
    private Tencent mTencent;
    private String opendid;
    private Logger logger = Logger.getLogger(LoginMobileActivity.class);
    private IMService imService;
    private boolean loginSuccess = false;
    private boolean isClickable = true;

    private EditText mNameView;
    private EditText mPasswordView;
    private ProgressBar mProgressView;
    private InputMethodManager inputMethodManager;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(LoginMobileActivity.this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        /** 系统的sp文件中 初始化 */
        SystemConfigSp.instance().init(getApplicationContext());
        /** 设置用户登陆服务器的地址 */
        if (TextUtils.isEmpty(SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.LOGINSERVER))) {
            SystemConfigSp.instance().setStrConfig(SystemConfigSp.SysCfgDimension.LOGINSERVER, UrlConstant.ACCESS_MSG_ADDRESS);
        }

        imServiceConnector.connect(LoginMobileActivity.this);
        EventBus.getDefault().register(this);
        /** 绑定布局资源(注意放所有资源初始化之前) */
        setContentView(R.layout.mb_activity_mobile_login);

        mNameView = (EditText) findViewById(R.id.nameView);
        mPasswordView = (EditText) findViewById(R.id.passwordView);
        mProgressView = (ProgressBar) findViewById(R.id.pb_above_login_mobile);
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        /***
         * 跳转手机登陆界面
         */
        findViewById(R.id.mobileLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClickable) {
                    return;
                }
                isClickable = false;
                attemptLoginMobile();
            }
        });

        findViewById(R.id.mobileLogin).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    inputMethodManager.hideSoftInputFromWindow(mNameView.getWindowToken(), 0);
                    isClickable = false;
                    attemptLoginMobile();
                }
            }
        });
        findViewById(R.id.forgetPwdView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isClickable) {
                    return;
                }
                Intent i = new Intent(LoginMobileActivity.this, ForgetPwdActivity.class);
                startActivity(i);
            }
        });
        findViewById(R.id.tvRegist).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isClickable) {
                    return;
                }
                startActivity(new Intent(LoginMobileActivity.this, RegisterActivity.class));
            }
        });
        findViewById(R.id.iv_qqlogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isClickable) {
                    return;
                }
                isClickable = false;
                login();
            }
        });
    }


    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
//            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    Toast.makeText(getApplicationContext(),"请求超时",Toast.LENGTH_LONG).show();
                    isClickable = true;
                    mProgressView.setVisibility(View.GONE);
                    break;
            }

        }
    };


    public void login() {
        mTencent = Tencent.createInstance(ConstantValues.QQAPPID, getApplicationContext());
        if (!mTencent.isSessionValid()) {
            mTencent.login(this, "all", loginListener);
        }
        mTencent.logout(this);
        updateUserInfo();
    }


    private void updateUserInfo() {
        if (mTencent != null && mTencent.isSessionValid()) {
            IUiListener listener = new IUiListener() {
                @Override
                public void onError(UiError e) {

                }

                @Override
                public void onComplete(final Object response) {
                    JSONObject json = (JSONObject) response;
                    try {
                        String imgUrl = (String) json.get("figureurl_qq_2");
                        String nickname = (String) json.get("nickname");
                        String qqgender = (String) json.get("gender");
                        attemptRegLogin(nickname, opendid, imgUrl, qqgender);
                    } catch (JSONException e) {
                        logger.e(e.toString());
                    }
                    logger.d("response: ", response);
                }

                @Override
                public void onCancel() {

                }
            };
            UserInfo mInfo = new UserInfo(this, mTencent.getQQToken());
            mInfo.getUserInfo(listener);
        }
    }

    public void initOpenidAndToken(JSONObject jsonObject) {
        try {
            String token = jsonObject.getString(Constants.PARAM_ACCESS_TOKEN);
            String expires = jsonObject.getString(Constants.PARAM_EXPIRES_IN);
            String openId = jsonObject.getString(Constants.PARAM_OPEN_ID);
            if (!TextUtils.isEmpty(token) && !TextUtils.isEmpty(expires)
                    && !TextUtils.isEmpty(openId)) {
                mTencent.setAccessToken(token, expires);
                mTencent.setOpenId(openId);
            }
        } catch (Exception e) {
            logger.e(e.toString());
        }

    }

    IUiListener loginListener = new BaseUiListener() {
        @Override
        protected void doComplete(JSONObject values) {
            Log.d("SDKQQAgentPref", "AuthorSwitch_SDK:" + SystemClock.elapsedRealtime());
            initOpenidAndToken(values);
            updateUserInfo();
        }
    };

    private class BaseUiListener implements IUiListener {
        @Override
        public void onComplete(Object response) {
            if (null == response) {;
                Toast.makeText(LoginMobileActivity.this, "登陆失败", Toast.LENGTH_SHORT).show();
                return;
            }
            JSONObject jsonResponse = (JSONObject) response;
            try {
                //用户的唯一标示
                opendid = jsonResponse.get("openid") + "";

            } catch (JSONException e) {
                logger.e(e.toString());
            }

            if (jsonResponse.length() == 0) {
                Toast.makeText(LoginMobileActivity.this, "登录失败", Toast.LENGTH_SHORT).show();
                return;
            }
            Toast.makeText(LoginMobileActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
            doComplete((JSONObject) response);
        }

        protected void doComplete(JSONObject values) {
        }

        @Override
        public void onError(UiError e) {
            Toast.makeText(LoginMobileActivity.this, e.errorDetail, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onCancel() {

        }
    }


    /**
     * qq注册
     */
    private void attemptRegLogin(String name, final String rid, String imgUrl, String gender) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("rid", "qq:" + rid);
        params.addParams("portraitImg", imgUrl);
        if (gender.equals("男")) {
            params.addParams("gender", "1");
        } else {
            params.addParams("gender", "0");
        }
        params.addParams("nickname", name);
        params.addParams("password", new String(Security.getInstance().EncryptPass("yuxi")));
        OkHttpClientManager.postAsy(ConstantValues.Register, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("result").equals("1")) {
                                attemptLogin("qq:" + rid, "yuxi");
                            } else if (obj.getString("result").equals("2")) {
                                attemptLogin("qq:" + rid, "yuxi");
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("describe"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    /**
     * QQ注册成功登陆
     */
    private void attemptLogin(String loginName, String mPassword) {
        if (imService != null) {
            //im服务 获取登陆管理  然后进行登陆操作
            imService.getLoginManager().login(loginName, mPassword);
        }
    }

    /**
     * 网络登陆
     */
    private void attemptLoginMobile() {
        String loginName = mNameView.getText().toString();
        String mPassword = mPasswordView.getText().toString();
        //判断用户名和密码是否为空
        if (TextUtils.isEmpty(mPassword) || TextUtils.isEmpty(loginName)) {
            T.showShort(this, "输入不能为空");
            isClickable = true;
        } else {
            if (imService != null) {
                loginName = loginName.trim();
                mPassword = mPassword.trim();
                //im服务 获取登陆管理  然后进行登陆操作
                imService.getLoginManager().login("mb:" + loginName, mPassword);
                mProgressView.setVisibility(View.VISIBLE);
                handler.sendEmptyMessageDelayed(0, 12000);
            }else{
                Toast.makeText(getApplicationContext(),"连接服务器失败",Toast.LENGTH_LONG).show();
                isClickable  = true;
            }
        }
    }

    /**
     * ----------------------------event 事件驱动----------------------------
     */
    public void onEventMainThread(LoginEvent event) {
        switch (event) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGIN_OK:
                //登陆成功后
                handler.removeMessages(0);
                onLoginSuccess();

                break;
            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED:
                if (!loginSuccess){
                    onLoginFailure(event);
                    handler.removeMessages(0);
                    mProgressView.setVisibility(View.GONE);
                }
                break;
        }
    }


    public void onEventMainThread(SocketEvent event) {
        switch (event) {
            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                if (!loginSuccess){
                    onSocketFailure(event);
                    handler.removeMessages(0);
                    mProgressView.setVisibility(View.GONE);
                }
                break;
        }
    }

    /**
     * 登陆成功后的操作
     */
    private void onLoginSuccess() {
        logger.i("login#onLoginSuccess");
        loginSuccess = true;
        int loginId = imService.getLoginManager().getLoginId();

        mTencent = null;
        Intent intent = new Intent(LoginMobileActivity.this, MainActivity.class);
        startActivity(intent);
        LoginMobileActivity.this.finish();
        T.showShort(this, "登陆成功");
    }

    //登陆失败后
    private void onLoginFailure(LoginEvent event) {
        isClickable = true;
        logger.e("login#onLoginError -> errorCode:%s", event.name());
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip, Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        isClickable = true;
        logger.e("login#onLoginError -> errorCode:%s,", event.name());
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "2", Toast.LENGTH_SHORT).show();
    }
}
