package com.yuxip.ui.activity.chat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.FamilyInfoDetailedEntity;
import com.yuxip.entity.MemberEntity;
import com.yuxip.imservice.entity.MessageType;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.activity.other.ReportActivity;
import com.yuxip.ui.customview.VhawkPopupWindow;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.NetWorkUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


/**
 * 家族资料界面
 * Created by SummerRC on 2015/5/19/.
 */
public class FamilyDataActivity extends TTBaseNewActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(FamilyDataActivity.class);

    private FamilyInfoDetailedEntity familyInfoDetailedEntity;
    private String familyid;
    private IntentConstant.FamilyDataActivityType familyDataActivityType;
    private ImageView iv_menu;
    private IMService imService;
    private VhawkPopupWindow window;
    private View view;
    private ImageView imgVip;

    private MyHandler mHandler = new MyHandler(this);
    private MyReceiver receiver;
    private boolean isNetOk = true;
    static class MyHandler extends Handler {
        WeakReference<FamilyDataActivity> mActivity;         //持有FamilyDataActivity对象的弱引用

        MyHandler(FamilyDataActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            FamilyDataActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case MessageType.HANDLER_MESSAGE_SUCCESS:
                    activity.initView();
                    break;
                case MessageType.HANDLER_MESSAGE_ERROR:
                    break;
                case 0:
                    break;
            }
        }
    }

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        imServiceConnector.connect(this);
        initBroadReceiver();
        /** 获取从Fragment传递过来的数据 ：家族id */
        familyid = getIntent().getStringExtra(IntentConstant.FAMILY_ID);
        familyDataActivityType = (IntentConstant.FamilyDataActivityType) getIntent().getSerializableExtra(IntentConstant.FamilyDataActivity_Type);

        view = getLayoutInflater().inflate(R.layout.activity_family_data_administrator, null);
        initMenu();

        /** 顶部返回键处理 */
        view.findViewById(R.id.storyBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });
        /** 顶部导航的菜单按钮 */
        iv_menu = (ImageView) view.findViewById(R.id.iv_menu);
        iv_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (familyDataActivityType) {
                    case TYPE_ADMIN:
                    case TYPE_MEMBER:
                        break;
                    case TYPE_NOT_MEMBER:
                        break;
                }
                window.showPop(iv_menu);
            }
        });
        setContentView(view);
    }

    @Override
    protected void onStart() {
        super.onStart();
        /** 获取家族信息 */
        getFamilyInfoDetailed();
    }

    private void initView() {
        /** 底部申请/聊天的监听 */
        findViewById(R.id.ll_bottom).setOnClickListener(this);
        switch (familyDataActivityType) {
            case TYPE_ADMIN:            //管理员
                findViewById(R.id.iv_bottom).setVisibility(View.VISIBLE);
                findViewById(R.id.iv_name_btn).setVisibility(View.VISIBLE);
                findViewById(R.id.iv_name_btn).setOnClickListener(this);
                findViewById(R.id.iv_nickname_btn).setVisibility(View.VISIBLE);
                findViewById(R.id.iv_nickname_btn).setOnClickListener(this);
                break;
            case TYPE_MEMBER:           //成员
                findViewById(R.id.iv_bottom).setVisibility(View.VISIBLE);
                findViewById(R.id.iv_nickname_btn).setVisibility(View.VISIBLE);
                findViewById(R.id.iv_nickname_btn).setOnClickListener(this);
                break;
            case TYPE_NOT_MEMBER:       //非成员
                ((TextView) findViewById(R.id.tv_bottom)).setText("申 请");
                break;
        }

        imgVip = (ImageView) view.findViewById(R.id.img_vip);

        if (familyInfoDetailedEntity.getIsvip().equals("1")) {
            imgVip.setVisibility(View.VISIBLE);
        } else {
            imgVip.setVisibility(View.GONE);
        }

        /** 我的昵称 */
        TextView tv_nickname = (TextView) findViewById(R.id.tv_nickname);
        tv_nickname.setText(familyInfoDetailedEntity.getNickname());
        switch (familyDataActivityType) {
            case TYPE_ADMIN:
            case TYPE_MEMBER:
                tv_nickname.setText(familyInfoDetailedEntity.getNickname());
                break;
            case TYPE_NOT_MEMBER:
                tv_nickname.setText("非成员");
                break;
        }
        /** 家族成员人数 */
        TextView tv_member_num = (TextView) findViewById(R.id.tv_member_num);
        tv_member_num.setText(familyInfoDetailedEntity.getMember_num() + "");
        /** 家族名称 */
        TextView tv_name = (TextView) findViewById(R.id.tv_name);
        tv_name.setText(familyInfoDetailedEntity.getName());
        /** 家族id */
        TextView tv_id = (TextView) findViewById(R.id.tv_id);
        tv_id.setText(familyInfoDetailedEntity.getId());
        /** 族长 */
        TextView tv_ownernickname = (TextView) findViewById(R.id.tv_ownernickname);
        tv_ownernickname.setText(familyInfoDetailedEntity.getOwnername());
        /** 简介 */
        TextView tv_intro = (TextView) findViewById(R.id.tv_intro);
        tv_intro.setText(familyInfoDetailedEntity.getIntro() + "");

        /** 右上角菜单  */
        ImageView family_member_icon = (ImageView) findViewById(R.id.family_member_icon);

        Drawable drawable = DrawableCache.getInstance(getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
        DisplayImageOptions displayImageOptions = ImageLoaderUtil.getOptions(drawable);
        ImageLoaderUtil.getImageLoaderInstance().displayImage(familyInfoDetailedEntity.getPortrait(), family_member_icon, displayImageOptions);
    }

    private void initMenu() {
        // todo  菜单显示时要根据用户类型做不同的显示
        window = new VhawkPopupWindow();
        window.setOnPopItemClicked(new VhawkPopupWindow.OnPopItemClickedListener() {
            @Override
            public void onPopItemClicked(int position, View view) {
                if (position == 0) {
//                    举报家族
                    Intent intent = new Intent(FamilyDataActivity.this, ReportActivity.class);
                    intent.putExtra(IntentConstant.REPORT_TYPE, IntentConstant.FAMILY_ID);
                    intent.putExtra(IntentConstant.FAMILY_ID, familyid);
                    FamilyDataActivity.this.startActivity(intent);
                }
            }
        });

        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public Object getItem(int position) {
                return "test";
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = View.inflate(FamilyDataActivity.this, R.layout.tv_item, null);
                TextView tv_item = (TextView) view.findViewById(R.id.tv_item);

                tv_item.setText("举报");
                return view;
            }
        };

        window.initPopUpwindow(FamilyDataActivity.this, adapter, 0);
    }

    private void initBroadReceiver(){
        receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        registerReceiver(receiver, filter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imServiceConnector.disconnect(this);
        unregisterReceiver(receiver);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_nickname_btn:  //修改我的昵称
                IMUIHelper.openModifyFamilyPersonInfoActivity(this, IntentConstant.ModifyFamilyPersonInfoActivityTYPE.TYPE_PERSON_NICKNAME,
                        String.valueOf(familyInfoDetailedEntity.getId()));
                break;
            case R.id.iv_name_btn:      //修改家族昵称
                IMUIHelper.openModifyFamilyPersonInfoActivity(this, IntentConstant.ModifyFamilyPersonInfoActivityTYPE.TYPE_FAMILY_NAME,
                        String.valueOf(familyInfoDetailedEntity.getId()));
                break;
            case R.id.ll_bottom:        //聊天或者申请
                switch (familyDataActivityType) {
                    case TYPE_ADMIN:
                    case TYPE_MEMBER:
                        /** 聊天 */
                        String sessionKey = "2_" + familyInfoDetailedEntity.getId();
                        IMUIHelper.openFamilyChatActivity(this, sessionKey);
                        break;
                    case TYPE_NOT_MEMBER:
                        /** 申请 */
                        if(isNetOk){
                            applyGroup();
                        }else{
                            Toast.makeText(getApplicationContext(),"网络异常,请检查网络连接",Toast.LENGTH_LONG).show();
                        }
                        break;
                }

                //顶部导航的菜单按钮

        }
    }

    /**
     * 获取家族信息
     */
    private void getFamilyInfoDetailed() {
        if(TextUtils.isEmpty(familyid))
            return;
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", String.valueOf(IMLoginManager.instance().getLoginId()));
        params.addParams("familyid", familyid);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetFamilyInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    Message msg = new Message();
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONObject obj = object.getJSONObject("familyinfo");
                        familyInfoDetailedEntity = new FamilyInfoDetailedEntity();
                        familyInfoDetailedEntity.setId(obj.getString("id"));
                        familyInfoDetailedEntity.setIntro(obj.getString("intro"));
                        familyInfoDetailedEntity.setIsvip(obj.getString("isvip"));
                        familyInfoDetailedEntity.setName(obj.getString("name"));
                        familyInfoDetailedEntity.setNickname(obj.getString("nickname"));
                        familyInfoDetailedEntity.setOwnerid(obj.getString("ownerid"));
                        familyInfoDetailedEntity.setOwnername(obj.getString("ownername"));
                        familyInfoDetailedEntity.setOwnernickname(obj.getString("ownernickname"));
                        familyInfoDetailedEntity.setPortrait(obj.getString("portrait"));
                        familyInfoDetailedEntity.setRank(obj.getString("rank"));
                        familyInfoDetailedEntity.setCreattime(obj.getString("creattime"));

                        JSONArray array = obj.getJSONArray("members");
                        ArrayList<MemberEntity> member_list = new ArrayList<MemberEntity>();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj_member = array.getJSONObject(i);
                            MemberEntity entity = new MemberEntity();
                            entity.setId(obj_member.getString("id"));
                            entity.setTitle(obj_member.getString("title"));
                            entity.setNickname(obj_member.getString("nickname"));
                            entity.setPortrait(obj_member.getString("portrait"));
                            member_list.add(entity);
                        }
                        familyInfoDetailedEntity.setMemberList(member_list);
                        familyInfoDetailedEntity.setMember_num(member_list.size());
                        msg.what = MessageType.HANDLER_MESSAGE_SUCCESS;
                        mHandler.sendMessage(msg);
                    } else {
                        msg.what = MessageType.HANDLER_MESSAGE_ERROR;
                        mHandler.sendMessage(msg);
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                mHandler.sendEmptyMessage(MessageType.HANDLER_MESSAGE_ERROR);
            }
        });
    }

    /**
     * 申请入群
     */
    private void applyGroup() {
        Set<Integer> memberIds = new HashSet<>();
        Integer adminid = Integer.valueOf(familyInfoDetailedEntity.getOwnerid());               //管理员id
        Integer groupid = Integer.valueOf(familyInfoDetailedEntity.getId());
        imService.getMessageManager().sendMsgGroupAddMemberReq(adminid, groupid, memberIds);

        Toast.makeText(FamilyDataActivity.this, "请求已发出！", Toast.LENGTH_SHORT).show();
        ((TextView) findViewById(R.id.tv_bottom)).setText("已申请");
        findViewById(R.id.ll_bottom).setClickable(false);
    }

    class MyReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                int netWorkType = NetWorkUtils.getNetWorkType(FamilyDataActivity.this);
                if (netWorkType == NetWorkUtils.NETWORKTYPE_INVALID) {
                   isNetOk = false;
                } else {
                    isNetOk =true;
                }
            }
        }
    }

}
