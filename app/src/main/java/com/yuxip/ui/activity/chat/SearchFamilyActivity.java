package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FamilyListEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.SearchFamilyAdapter;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by Administrator on 2015/5/26.
 * 搜索家族界面
 */
public class SearchFamilyActivity extends TTBaseNewActivity {
    private Logger logger = Logger.getLogger(SearchFamilyActivity.class);

    //搜索按钮
    @InjectView(R.id.tv_search)
    TextView searchFamilyBtn;
    //搜索输入框
    @InjectView(R.id.et_search)
    EditText editTextSearch;
    //返回键
    @InjectView(R.id.iv_back)
    ImageView back;

    //listview
    @InjectView(R.id.lv_search)
    ListView lv_search;

    private String searchContent;

    private ArrayList<FamilyListEntity> familyList = new ArrayList<FamilyListEntity>();
    private SearchFamilyAdapter mFamilyAdapter;
    private ProgressBar progressBar;

    @Override
    protected void onResume() {
        super.onResume();
        //      如果请求成功  设置所有界面的adapter
        mFamilyAdapter = new SearchFamilyAdapter(familyList, SearchFamilyActivity.this);
        lv_search.setAdapter(mFamilyAdapter);
        lv_search.setOnScrollListener(
                new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        /** 隐藏标题栏 */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);
        ButterKnife.inject(SearchFamilyActivity.this);
        editTextSearch.setHint("请输入家族名称或ID号");
        initCurView();
    }

    private void initCurView() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //搜索按钮点击事件
        searchFamilyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContent = editTextSearch.getText().toString().trim();
                if (TextUtils.isEmpty(searchContent)) {
                    Toast.makeText(SearchFamilyActivity.this, "请输入内容", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                searchFamilyReq(searchContent);
            }
        });

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                String key = s.toString().trim();
                if (TextUtils.isEmpty(key)) {
                    familyList.clear();
                    mFamilyAdapter.notifyDataSetChanged();
                } else {
                    searchFamilyReq(key);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editTextSearch.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER)
                    return  true;
                return false;
            }
        });
    }

    /**
     * 搜索请求
     *
     * @param searchContent 搜索的内容
     */
    private void searchFamilyReq(String searchContent) {
        /**
         * 放在params里面传递
         */
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("cond", searchContent);
        OkHttpClientManager.postAsy(ConstantValues.SearchFamily, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                familyList.clear();
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        JSONArray array = obj.getJSONArray("familylist");
                        if (array.length() == 0) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(SearchFamilyActivity.this, "家族不存在！", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        for (int i = 0; i < array.length(); i++) {
                            Log.e("arr_length", i + "");
                            JSONObject jsonObject = array.getJSONObject(i);
                            FamilyListEntity familyEntity = new FamilyListEntity();
                            familyEntity.setId(jsonObject.get("id") + "");
                            familyEntity.setName(jsonObject.get("name").toString());
                            familyEntity.setPortrait(jsonObject.get("portrait").toString());
                            familyEntity.setIntro(jsonObject.get("intro") + "");
                            familyEntity.setIsMember(jsonObject.get("ismember") + "");
                            familyList.add(familyEntity);
                        }
                        progressBar.setVisibility(View.GONE);
                        mFamilyAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                progressBar.setVisibility(View.GONE);
                T.show(getApplicationContext(), "查询失败请重试！", 1);
            }
        });
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
