package com.yuxip.ui.activity.square;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.JsonBean.TopicDetailResult;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.TopicUserAdapter;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.widget.MyGridView;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/7/3.
 * description:话题详情
 */
public class TopicDetailsActivity extends TTBaseNewActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(TopicDetailsActivity.class);

    @InjectView(R.id.topicBlack)
    ImageView topicBlack;
    @InjectView(R.id.iv_book_head_img)
    CustomHeadImage ivBookHeadImg;
    @InjectView(R.id.tv_top_topic_create)
    TextView tvTopTopicCreate;
    @InjectView(R.id.tv_topic_time)
    TextView tvTopicTime;
    @InjectView(R.id.iv_top_fave)
    ImageView ivTopFave;
    @InjectView(R.id.tv_fave_num)
    TextView tvFaveNum;
    @InjectView(R.id.tv_topic_title)
    TextView tvTopicTitle;
    @InjectView(R.id.rl_topic_jubao)
    RelativeLayout rlTopicJubao;
    @InjectView(R.id.iv_topic_img)
    ImageView ivTopicImg;
    @InjectView(R.id.tv_topic_content)
    TextView tvTopicContent;
    @InjectView(R.id.tv_topic_comment_user_num)
    TextView tvTopicCommentUserNum;
    @InjectView(R.id.comment_num)
    TextView commentNum;
    @InjectView(R.id.topicGridview)
    MyGridView topicGridview;
    @InjectView(R.id.comment)
    TextView comment;
    @InjectView(R.id.scrolllview_topic_content)
    ScrollView scrollView;
    private String loginId;
    private String topicid;
    private int gId;            //groupId 群id
    private ImageLoader imageLoader;
    private int behavior;       //点赞或喜欢状态 从网络获取为初始,用户操作为终值，界面结束如果值不同，则去刷新用户界面-->>>一般指个人主页
    private int behaviorTemp;
    private List<TopicDetailResult.TopicdetailEntity.UserlistEntity> userlist = new ArrayList<>();
    private TopicUserAdapter adapter;
    private String praisenum;      //点赞或收藏的数量,用户点赞或收藏及其取消操作都要更新该对应控件的值
    private TopicDetailResult.TopicdetailEntity topicdetailEntity;
    private final int START_FLG_SET = 1;
    private final int START_FLG_NO_SET = 0;
    private int startFlg;
    private IMService imService;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.activity_topic_content);
        imServiceConnector.connect(this);
        ButterKnife.inject(this);
        EventBus.getDefault().register(this);
        initRes();
        GetTopicDetail();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        if (behaviorTemp != behavior) {
            Intent intent = new Intent();
            intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
            sendBroadcast(intent);
        }
    }

    private void initRes() {
        loginId = IMLoginManager.instance().getLoginId() + "";
        topicid = getIntent().getStringExtra(IntentConstant.TOPIC_ID);
        rlTopicJubao.setOnClickListener(this);
        ivTopFave.setOnClickListener(this);
        ivBookHeadImg.setOnClickListener(this);
        topicGridview.setSelector(new ColorDrawable(Color.TRANSPARENT));// 去掉点击时的黄色背影
        topicGridview.setOnScrollListener(
                new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        adapter = new TopicUserAdapter(TopicDetailsActivity.this, userlist);
        topicGridview.setAdapter(adapter);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gId == 0) {
                    Toast.makeText(TopicDetailsActivity.this, "这个话题太老了，请勿评论", Toast.LENGTH_SHORT).show();
                    return;
                }
                startFlg = START_FLG_SET;
                GroupEntity groupEntity = IMGroupManager.instance().findGroup(gId);
                if (groupEntity == null) {
                    IMGroupManager.instance().reqGroupDetailInfo(gId);
                } else {
                    checkExistAndStartChat(groupEntity);
                }
            }
        });
        topicBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void GetTopicDetail() {
        if(TextUtils.isEmpty(loginId)||TextUtils.isEmpty(topicid))
            return;
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("topicid", topicid);
        OkHttpClientManager.postAsy(ConstantValues.GetTopicDetail, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    onReceiveTopicDetail(response);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(TopicDetailsActivity.this, R.string.network_err, 1);
            }
        });
    }

    private void onReceiveTopicDetail(String result) {
        TopicDetailResult topicDetailResult = new Gson().fromJson(result, TopicDetailResult.class);
        if (topicDetailResult == null) {
            Toast.makeText(getApplicationContext(), "获取信息失败,请重试", Toast.LENGTH_SHORT).show();
            return;
        }
        if (topicDetailResult.getResult().equals("1")) {
            topicdetailEntity = topicDetailResult.getTopicdetail();
            gId = Integer.valueOf(topicdetailEntity.getGroupid());
            praisenum = topicdetailEntity.getPraisenum();
            tvFaveNum.setText(praisenum);
            tvTopTopicCreate.setText(topicdetailEntity.getCreatorname() + "的话题");
            tvTopicTitle.setText(topicdetailEntity.getTopictitle());
            tvTopicTime.setText(DateUtil.getDateWithOutYear(Integer.parseInt(topicdetailEntity.getCreattime())));
            behavior = behaviorTemp = Integer.valueOf(topicdetailEntity.getIspraised());
            if (topicdetailEntity.getIspraised().equals("1")) {
                ivTopFave.setSelected(true);
            } else {
                ivTopFave.setSelected(false);
            }
            imageLoader = ImageLoaderUtil.getImageLoaderInstance();
            ivBookHeadImg.setVipSize(12f);
            ivBookHeadImg.loadImage(topicdetailEntity.getCreatorportrait(), "0");
            if (!TextUtils.isEmpty(topicdetailEntity.getTopicimgs())) {
                ivTopicImg.setVisibility(View.VISIBLE);
                imageLoader.displayImage(topicdetailEntity.getTopicimgs(), ivTopicImg);
            }
            tvTopicContent.setText(topicdetailEntity.getTopiccontent());
            commentNum.setText(topicdetailEntity.getCommentnum() + "条评论");
            userlist.addAll(topicdetailEntity.getUserlist());
            adapter.notifyDataSetChanged();
            scrollView.smoothScrollTo(0, 20);
            if (userlist != null && userlist.size() > 0) {
                tvTopicCommentUserNum.setText(userlist.size() + "人");
            } else {
                tvTopicCommentUserNum.setText("0人");
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_topic_jubao:
                IMUIHelper.openReportActivity(TopicDetailsActivity.this, topicid, IntentConstant.TOPIC_ID);
                break;
            case R.id.topicBlack:
                finish();
                break;
            case R.id.comment:
                if (null == topicdetailEntity) {
                    T.showShort(this, "暂时不能评论");
                }
                IMGroupManager.instance()
                        .reqGroupDetailInfo(gId = Integer.parseInt(topicdetailEntity.getGroupid()));
                break;

            case R.id.iv_top_fave:
            case R.id.tv_fave_num:
                if (topicdetailEntity != null) {
                    if (topicdetailEntity.getIspraised().equals("1")) {
                        behavior = 0;
                    } else {
                        behavior = 1;
                    }
                    requestPraise();
                }
                break;
            case R.id.iv_book_head_img:
                IMUIHelper.openUserHomePageActivity(TopicDetailsActivity.this, topicdetailEntity.getCreatorid());
                break;
        }
    }

    private void requestPraise() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("topicid", topicid);
        params.addParams("behavior", behavior + "");
        OkHttpClientManager.postAsy(ConstantValues.PraiseTopics, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    String result = jsonObject.getString("result");
                    if ("1".equals(result)) { //成功
                        if (behavior == 1) { //点赞
                            if (!praisenum.equals(null)) {
                                praisenum = (Integer.parseInt(praisenum) + 1) + "";
                            }
                            tvFaveNum.setText(praisenum);
                            topicdetailEntity.setPraisenum(praisenum);
                            topicdetailEntity.setIspraised("1");
                            ivTopFave.setSelected(true);
                        } else {
                            praisenum = (Integer.parseInt(praisenum) - 1) + "";
                            tvFaveNum.setText(praisenum);
                            topicdetailEntity.setPraisenum(praisenum);
                            topicdetailEntity.setIspraised("0");
                            ivTopFave.setSelected(false);
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.showShort(TopicDetailsActivity.this, R.string.network_err);
            }
        });
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                startFlg = START_FLG_NO_SET;
                T.showShort(this, "暂时不能评论");
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
                Log.d("storyDeatilsActivity", "CHANGE_GROUP_MEMBER_SUCCESS is called");
                break;
            }
            //向群里添加人员的时候，最后也会获取群的信息。因此不需要处理变更人员的消息。
            case GROUP_INFO_UPDATED: {
                //不是在点击状态下，忽略更新事件。
                if (startFlg != START_FLG_SET) {
                    break;
                }
                GroupEntity groupEntity = event.getGroupEntity();

                //如果是当前评论群的信息的变化
                if (groupEntity.getPeerId() == gId) {
                    checkExistAndStartChat(groupEntity);
                }
            }
            break;
        }
    }

    /**
     * 检查是否是成员，如果不是，需要先添加到群里。
     *
     * @param groupEntity
     */
    private void checkExistAndStartChat(GroupEntity groupEntity) {
        String userList = groupEntity.getUserList();
        String userid = IMLoginManager.instance().getLoginId() + "";
        if (userList.contains(userid)) {
            startCommentGroupChat();
        } else {
            Set<Integer> set = new HashSet<Integer>();
            set.add(IMLoginManager.instance().getLoginId());
            IMGroupManager.instance().reqAddGroupMember(gId, set);
        }
    }

    /**
     * 开始聊天。
     */
    private void startCommentGroupChat() {
        startFlg = START_FLG_NO_SET;
        String sessionKey, portrait, title, creatorName, createTime, type;
        sessionKey = "2_" + gId;
        portrait = topicdetailEntity.getCreatorportrait();
        title = topicdetailEntity.getTopictitle();
        createTime = topicdetailEntity.getCreattime();
        creatorName = topicdetailEntity.getCreatorname();
        type = IntentConstant.TOPIC;
        IMUIHelper.openTopicMessageActivity(this, type, sessionKey, portrait, title, creatorName, createTime);
    }
}
