package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2015/5/25.
 */
public class FamilyNameSettingActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(FamilyNameSettingActivity.class);

    private View curView;
    @InjectView(R.id.familynameEt)
    EditText familynameEt;
    private String strfamily;
    private String groupid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        curView = View.inflate(this, R.layout.activity_family_name_setting, topContentView);
        groupid = getIntent().getStringExtra("groupid");
        ButterKnife.inject(this, curView);
        initRes();
        setTitle("家族名称");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("完成");
    }

    private void initRes() {

        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strfamily = familynameEt.getText().toString().trim();
                if (strfamily.isEmpty()) {
                    showToast("输入不能为空");
                } else {
                    ModifyFamilyIfno(strfamily, groupid);
                }
            }
        });
    }

    /**
     * 更新家族信息
     */
    private void ModifyFamilyIfno(final String strfamily, String groupid) {

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("familyname", strfamily);
        params.addParams("familyid", groupid);
        OkHttpClientManager.postAsy(ConstantValues.ModifyFamilyInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        showToast("修改成功");
                        Intent intent = new Intent();
                        intent.putExtra(IntentConstant.FAMILY_CHANGE_RESULT,strfamily);
                        setResult(RESULT_OK, intent);
                        FamilyNameSettingActivity.this.finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
