package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryRoleNature;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.ui.customview.NoScrollGridView;
import com.yuxip.ui.helper.KeyBoardObserver;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/11/2.
 * description：角色属性设定
 */
public class CreateStoryFifthActivity extends Activity implements View.OnClickListener {
    private final int editRuleWordLimit = 300;
    private TextView tvNextStep;
    private EditText etEditRules;
    private ImageView ivBack;
    private NoScrollGridView gridView;
    private CustomDividerProgressbar progressbar;
    private MyAdapter adapter;
    private List<StoryRoleNature.ListEntity> listTypes = new ArrayList<>();
    private List<Boolean> listSelect = new ArrayList<>();
    private List<Integer> listIds = new ArrayList<>();
    private List<String> listNews = new ArrayList<>();
    private RelativeLayout rlbottom;
    private View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView  = LayoutInflater.from(this).inflate(R.layout.activity_create_story_fifth,null);
        setContentView(rootView);
        initRes();
        checkManagerData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void initRes() {
        ivBack = (ImageView) findViewById(R.id.iv_back_create_story_fifth);
        tvNextStep = (TextView) findViewById(R.id.tv_next_step_create_story_fifth);
        etEditRules = (EditText) findViewById(R.id.et_audit_rules);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_fifth);
        gridView = (NoScrollGridView) findViewById(R.id.gv_create_story_fifth);
        rlbottom = (RelativeLayout) findViewById(R.id.rl_bottom_create_story_fifth);
        adapter = new MyAdapter();
        gridView.setAdapter(adapter);
        ivBack.setOnClickListener(this);
        tvNextStep.setOnClickListener(this);
        tvNextStep.setSelected(true);
        etEditRules.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s.toString())) {
//                    tvNextStep.setSelected(false);
                } else {
                    if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) == 0) {
//                        tvNextStep.setSelected(false);
//                        return;
                    } else if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) > editRuleWordLimit) {
                        etEditRules.setTextColor(getResources().getColor(R.color.red_text));
                    } else {
                        etEditRules.setTextColor(getResources().getColor(R.color.black));
                    }
//                    tvNextStep.setSelected(true);
                }
            }
        });
        progressbar.setCurProgress((int) Math.ceil(100 * 5 / 6));
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
        KeyBoardObserver keyBoardObserver = new KeyBoardObserver();
        keyBoardObserver.setBaseRootAndView(rootView, rlbottom);
        if (CreateStoryManager.getInstance().getListTypes() != null&&CreateStoryManager.getInstance().getListTypes().size()>0) {
            this.listTypes = CreateStoryManager.getInstance().getListTypes();
            initSelectList();
        }else{
            requestRoleNatureList();
        }
    }


    private void checkManagerData() {
        String auditContent = CreateStoryManager.getInstance().getAuditRules();
        if (!TextUtils.isEmpty(auditContent) && CreateStoryManager.getInstance().getCharacterCount(auditContent) > 0) {
            etEditRules.setText(auditContent);
            tvNextStep.setSelected(true);
        }
        List<String> listNewSelect = CreateStoryManager.getInstance().getListNewRoleNatures();
        if (listTypes != null && listTypes.size() > 0) {
            if (CreateStoryManager.getInstance().getListRoleNatures() != null) {
                //这是保存的选中的id
                List<Integer> listInt = CreateStoryManager.getInstance().getListRoleNatures();
                for (int i = 0; i < listTypes.size(); i++) {
                    if (listTypes.get(i).getId() != null) {
                        for (int j = 0; j < listInt.size(); j++) {
                            if (listTypes.get(i).getId().equals(listInt.get(j) + "")) {
                                listSelect.set(i, true);
                            }
                        }
                    } else {
                        if (listNewSelect != null && listNewSelect.size() > 0) {

                            if (checkHasDataSelectString(listNewSelect, listTypes.get(i).getName())) {
                                listSelect.set(i, true);
                            }
                        }
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }

    private boolean checkHasDataSelectString(List<String> list, String content) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(content))
                return true;
        }
        return false;
    }


    private void initSelectList() {
        listSelect.clear();
        for (int i = 0; i < listTypes.size(); i++) {
            if(i == 0){
                listSelect.add(true);
            }else {
                listSelect.add(false);
            }
        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_next_step_create_story_fifth:
                if (!tvNextStep.isSelected()) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), Toast.LENGTH_LONG).show();
                    return;
                }
                if (CreateStoryManager.getInstance().getCharacterCount(etEditRules.getText().toString().trim()) > editRuleWordLimit) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_audit_rule_limit_300), Toast.LENGTH_LONG).show();
                    return;
                }
                saveManageData();
                IMUIHelper.openCreateStoryOrderActivity(this, 6, null);
                finish();
                break;
            case R.id.iv_back_create_story_fifth:
                openPreAcitivty();
                finish();
                break;
        }
    }


    private boolean checkSelectLimit() {
        int selectCount = 0;
        for (int i = 0; i < listSelect.size(); i++) {
            if (listSelect.get(i)) {
                selectCount += 1;
            }
        }
        int selectLimit = 10;
        return selectCount < selectLimit;
    }


    private class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listTypes == null || listTypes.size() == 0 ? 0 : listTypes.size() + 1;
        }

        @Override
        public Object getItem(int position) {
            return listTypes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(CreateStoryFifthActivity.this).inflate(R.layout.item_person_setting_create_story, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.rlAdd.setVisibility(View.GONE);
            viewHolder.tvContent.setVisibility(View.VISIBLE);
            if (position == listSelect.size()) {
                viewHolder.rlAdd.setVisibility(View.VISIBLE);
                viewHolder.tvContent.setVisibility(View.GONE);
                viewHolder.rlAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogHelper.showAddItemDialog(CreateStoryFifthActivity.this, getResources().getString(R.string.story_add), getResources().getString(R.string.story_add_role_setting), "", 4, new DialogHelper.DialogListener() {
                            @Override
                            public void addNewItem(String dialogContent) {
                                StoryRoleNature.ListEntity listEntity = new StoryRoleNature.ListEntity();
                                listEntity.setName(dialogContent);
                                listTypes.add(listEntity);
                                listSelect.add(false);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }
                });
            } else {
                viewHolder.tvContent.setSelected(false);
                if (listSelect.get(position))
                    viewHolder.tvContent.setSelected(true);
                viewHolder.tvContent.setText(listTypes.get(position).getName());
                viewHolder.tvContent.setTag(position);
                viewHolder.tvContent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        if (_position == 0) {
                            Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_item_cannot_cancel),Toast.LENGTH_LONG).show();
                            return;
                        }
                        if (v.isSelected()) {
                            v.setSelected(false);
                            listSelect.set(_position, false);
                        } else {
                            if (checkSelectLimit()) {
                                v.setSelected(true);
                                listSelect.set(_position, true);
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_select_limit_10), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });

            }
            return convertView;
        }

        class ViewHolder {
            private TextView tvContent;
            private RelativeLayout rlAdd;

            ViewHolder(View view) {
                tvContent = (TextView) view.findViewById(R.id.tv_gv_item_story_person_set);
                rlAdd = (RelativeLayout) view.findViewById(R.id.rl_gv_item_story_person_set);
            }
        }
    }



    private void requestRoleNatureList() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleNatures, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            StoryRoleNature storyRoleNature = new Gson().fromJson(response, StoryRoleNature.class);
                            if (storyRoleNature.getList() != null && storyRoleNature.getList().size() > 0) {
                                listTypes = storyRoleNature.getList();
                                adapter.notifyDataSetChanged();
                            }
                        } catch (Exception e) {

                        }
                    }
                });
    }

    //填充选中的集合 包括一个ID 集合 或者 新建属性的 集合
    private void inflateSelectList() {
        listIds.clear();
        listNews.clear();
        for (int i = 0; i < listSelect.size(); i++) {
            if (listSelect.get(i)) {
                if (!TextUtils.isEmpty(listTypes.get(i).getId())) {
                    listIds.add(Integer.valueOf(listTypes.get(i).getId()));
                } else {
                    listNews.add(listTypes.get(i).getName());
                }
            }
        }
    }

    private void saveManageData() {
        inflateSelectList();
        CreateStoryManager.getInstance().setAuditRules(etEditRules.getText().toString());
        CreateStoryManager.getInstance().setListRoleNatures(listIds);
        CreateStoryManager.getInstance().setListNewRoleNatures(listNews);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            openPreAcitivty();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
    private void openPreAcitivty(){
        saveManageData();
        IMUIHelper.openCreateStoryOrderActivity(this, 4, IntentConstant.ACTIVITY_OPEN_TYPE_LEFT);
    }
}
