package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yuxip.DB.sp.ConfigurationSp;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.activity.base.TTBaseActivity;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * author:SummerRC
 * description:群消息屏蔽
 */
public class GroupMessageSettingActivity extends TTBaseActivity implements View.OnClickListener{
    @InjectView(R.id.ll_yes)
    LinearLayout ll_yes;

    @InjectView(R.id.ll_no)
    LinearLayout ll_no;

    @InjectView(R.id.iv_yes)
    ImageView iv_yes;

    @InjectView(R.id.iv_no)
    ImageView iv_no;

    private ConfigurationSp configMgr;
    private String curSessionKey;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View.inflate(this, R.layout.activity_group_message_setting, topContentView);
        ButterKnife.inject(this, topContentView);
        initView();
    }

    private void initView() {
        setTitle("群消息权限设置");
        setLeftButton(R.drawable.back_default_btn);
        curSessionKey = getIntent().getStringExtra(IntentConstant.SESSION_KEY);
        configMgr = ConfigurationSp.instance(getApplicationContext(), IMLoginManager.instance().getLoginId());
        if(configMgr.getCfg(curSessionKey, ConfigurationSp.CfgDimension.NOTIFICATION)) {
            no();
        } else {
            yes();
        }
        ll_yes.setOnClickListener(this);
        ll_no.setOnClickListener(this);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_yes:
                yes();
                break;
            case R.id.ll_no:
                no();
                break;
        }
    }

    private void yes() {
        iv_yes.setVisibility(View.VISIBLE);
        iv_no.setVisibility(View.INVISIBLE);
        configMgr.setCfg(curSessionKey, ConfigurationSp.CfgDimension.NOTIFICATION, false);
    }

    private void no() {
        iv_yes.setVisibility(View.INVISIBLE);
        iv_no.setVisibility(View.VISIBLE);
        configMgr.setCfg(curSessionKey, ConfigurationSp.CfgDimension.NOTIFICATION, true);
    }
}
