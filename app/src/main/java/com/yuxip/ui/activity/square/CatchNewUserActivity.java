package com.yuxip.ui.activity.square;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.NewUserListJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.CatchNewUserManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/11/26.
 * 获取小鲜肉列表
 */
public class CatchNewUserActivity  extends TTBaseActivity {
    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View footView;
    private MyAdapter adapter;
    private NewUserListJsonBean newUserListJsonBean;
    private List<NewUserListJsonBean.UserlistEntity> list = new ArrayList<>();
    private int index = 0;
    private int count  = 25;
    private boolean canLoad = true;

    private IMService imService;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_catch_new_user, topContentView);
        imServiceConnector.bindService(this);
        initRes();
    }

    private void initRes(){
        setTitle("新人认领");
        setLeftButton(R.drawable.back_default_btn);
        topLeftBtn.setVisibility(View.VISIBLE);
        footView = View.inflate(this, R.layout.footer_view, null);
        listView = (ListView) findViewById(R.id.lv_catch_new_user_activity);
        listView.addFooterView(footView);
        footView.setVisibility(View.GONE);
        adapter = new MyAdapter();
        listView.setAdapter(adapter);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_catch_new_user_activity);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.pink));
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                try {
                    swipeRefreshLayout.setRefreshing(true);
                } catch (Exception e) {

                }
                RequestUserList();
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    loadData();
                }
            }
        });

        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        imServiceConnector.unbindService(this);
    }

    private void refreshData(){
        index = 0;
        RequestUserList();
    }

    private void loadData(){
       if(!canLoad)
           return;
        canLoad = false;
        index = list.size();
        RequestUserList();
    }




    public void RequestUserList() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("index", index +"");
        params.addParams("count",count + "");
        params.addParams("token","110");
        OkHttpClientManager.postAsy(ConstantValues.GetNewUserList, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onRecevieData(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                footView.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(),"获取数据失败了",Toast.LENGTH_LONG).show();
                try {
                    swipeRefreshLayout.setRefreshing(false);
                } catch (Exception e1) {

                }
            }
        });
    }


    private void onRecevieData(String json){
        try{
            newUserListJsonBean = new Gson().fromJson(json,NewUserListJsonBean.class);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),"获取数据失败了",Toast.LENGTH_LONG).show();
        }

        try {
            swipeRefreshLayout.setRefreshing(false);
        }catch (Exception e){

        }

        if(newUserListJsonBean!= null){
           if(index == 0){
               if(newUserListJsonBean.getUserlist().size()==0){
                   canLoad =false;
                   footView.setVisibility(View.GONE);
                   Toast.makeText(getApplicationContext(),"最近没有小鲜肉登陆哦",Toast.LENGTH_LONG).show();

               }else if(newUserListJsonBean.getUserlist().size()>0&&newUserListJsonBean.getUserlist().size()<count){
                   canLoad = false;
                   footView.setVisibility(View.GONE);
               }else{
                   canLoad = true;
                   footView.setVisibility(View.VISIBLE);
               }
               list.clear();
               list.addAll(newUserListJsonBean.getUserlist());
               adapter.notifyDataSetChanged();
           }else{

               if(newUserListJsonBean.getUserlist().size() ==0){
                   canLoad = false;
                   footView.setVisibility(View.GONE);
                   return;
               }else if(newUserListJsonBean.getUserlist().size()>0&&newUserListJsonBean.getUserlist().size()<count){
                   canLoad = false;
                   footView.setVisibility(View.GONE);
               }else{
                   canLoad = true;
                   footView.setVisibility(View.VISIBLE);
               }
               list.addAll(newUserListJsonBean.getUserlist());
               adapter.notifyDataSetChanged();
           }
        }
    }


    class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if(convertView == null){
                convertView = LayoutInflater.from(CatchNewUserActivity.this).inflate(R.layout.item_lv_catch_new_user,null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tvIsAddedFriend.setVisibility(View.GONE);
            viewHolder.tvAddFriend.setVisibility(View.GONE);
            viewHolder.tvUserName.setText(list.get(position).getName());
            viewHolder.tvTime.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(list.get(position).getRegisttime())));
            viewHolder.ivHeadImg.loadImage(list.get(position).getPortrait());
            viewHolder.ivHeadImg.setTag(position);
            viewHolder.ivHeadImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (int) v.getTag();
                    if (!TextUtils.isEmpty(list.get(_position).getId())) {
                        IMUIHelper.openUserHomePageActivity(CatchNewUserActivity.this, list.get(_position).getId());
                    }

                }
            });


            viewHolder.tvIsAddedFriend.setText("已添加");
            if(list.get(position).getIsfriend().equals("1")){
                viewHolder.tvAddFriend.setVisibility(View.GONE);
                viewHolder.tvIsAddedFriend.setVisibility(View.VISIBLE);
            }else{
                if(CatchNewUserManager.getInstance().hasSendApply(list.get(position).getId())){
                    viewHolder.tvIsAddedFriend.setText("已发送");
                    viewHolder.tvAddFriend.setVisibility(View.GONE);
                    viewHolder.tvIsAddedFriend.setVisibility(View.VISIBLE);
                }else{
                    viewHolder.tvAddFriend.setVisibility(View.VISIBLE);
                    viewHolder.tvIsAddedFriend.setVisibility(View.GONE);
                    viewHolder.tvAddFriend.setTag(position);
                    viewHolder.tvAddFriend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int _position = (int) v.getTag();
                            String userId = list.get(_position).getId();
                            if (userId.equals(IMLoginManager.instance().getLoginId() + "")) {
                                T.show(getApplicationContext(), "不能添加自己为好友!", 0);
                            } else {
                                CatchNewUserManager.getInstance().addNewUserId(userId);
                                DialogHelper.showAddFriendDialog(CatchNewUserActivity.this, imService, Integer.valueOf(userId), new DialogHelper.DialogListener() {
                                    @Override
                                    public void addNewItem(String dialogContent) {
                                        viewHolder.tvAddFriend.setVisibility(View.GONE);
                                        viewHolder.tvIsAddedFriend.setVisibility(View.VISIBLE);
                                        viewHolder.tvIsAddedFriend.setText("已发送");
                                    }
                                });
                            }
                        }
                    });
                }


            }

            return convertView;
        }


        class ViewHolder{

            private TextView tvUserName;
            private CustomHeadImage ivHeadImg;
            private TextView tvTime;
            private TextView tvAddFriend;
            private TextView tvIsAddedFriend;
            public ViewHolder(View view){
                ivHeadImg = (CustomHeadImage) view.findViewById(R.id.iv_head_catch_new_user);
                tvUserName = (TextView) view.findViewById(R.id.tv_username_catch_new_user);
                tvTime = (TextView) view.findViewById(R.id.tv_time_catch_new_user);
                tvAddFriend = (TextView) view.findViewById(R.id.tv_add_friend_catch_new_user);
                tvIsAddedFriend = (TextView) view.findViewById(R.id.tv_isadded_fridend_catch_new_user);
            }
        }
    }




}
