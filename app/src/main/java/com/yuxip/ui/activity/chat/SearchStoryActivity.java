package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.album.WorldResultDataAdapter;
import com.yuxip.ui.customview.MyRecyclerView;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by Administrator on 2015/5/26.
 * 搜索剧界面
 */
public class SearchStoryActivity extends TTBaseNewActivity {
    private Logger logger = Logger.getLogger(SearchStoryActivity.class);

    //搜索按钮
    @InjectView(R.id.tv_search)
    TextView searchFamilyBtn;
    //搜索输入框
    @InjectView(R.id.et_search)
    EditText editTextSearch;
    //返回键
    @InjectView(R.id.iv_back)
    ImageView back;

    @InjectView(R.id.lv_search)
    MyRecyclerView recyclerView;

    private String searchContent;

    private ProgressBar progressBar;
    private WorldResultDataAdapter adapter;
    private List<HotBean.StorysEntity> hotBeans = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(super.isNeedRestart()) {
            return;
        }
        /** 隐藏标题栏 */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search_story);
        ButterKnife.inject(SearchStoryActivity.this);
        initRes();
        initCurView();
    }

    private void initRes() {
        editTextSearch.setHint("请输入剧的名称或ID号");
        initRecyclerView();

        adapter = new WorldResultDataAdapter(this, hotBeans);
        adapter.setCustomHeaderView(null);
        recyclerView.setAdapter(adapter);
        recyclerView.disableLoadmore();
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setEmptyView(R.layout.progress_layout);
        recyclerView.hideEmptyView();
//      recyclerView啥情况
        recyclerView.setVisibility(View.INVISIBLE);
        recyclerView.setDefaultOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        searchFamilyReq(searchContent);
                    }
                });


    }

    private void initCurView() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //搜索按钮点击事件
        searchFamilyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchContent = editTextSearch.getText().toString().trim();
                if (TextUtils.isEmpty(searchContent)) {
                    Toast.makeText(SearchStoryActivity.this, "请输入内容", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                searchFamilyReq(searchContent);
            }
        });

    }

    /**
     * 搜索请求
     *
     * @param searchContent 搜索的内容
     */
    private void searchFamilyReq(String searchContent) {
        /**
         * 放在params里面传递
         */
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("cond", searchContent);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.SearchStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    HotBean hotBean = new Gson().fromJson(response,
                            HotBean.class);
                    List<HotBean.StorysEntity> list = hotBean.getStorys();
                    hotBeans.clear();
                    hotBeans.addAll(list);
                    adapter.notifyDataSetChanged();
                    if (hotBeans.size() == 0) {
                        recyclerView.setVisibility(View.INVISIBLE);
                    } else {

                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    recyclerView.setRefreshing(false);
                    progressBar.setVisibility(View.GONE);
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                try {
                    recyclerView.setRefreshing(false);
                }catch (Exception e1){

                }
                    progressBar.setVisibility(View.GONE);
                    T.show(getApplicationContext(), "查询失败！", 1);
                }
            }

            );
        }

        @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
