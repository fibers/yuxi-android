package com.yuxip.ui.activity.other;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.DB.sp.LoginSp;
import com.yuxip.DB.sp.SystemConfigSp;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.UrlConstant;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/5/12.
 * description : app的启动界面
 */
public class SplashActivity extends Activity {
    private Handler uiHandler = new Handler();
    private Logger logger = Logger.getLogger(SplashActivity.class);
    private IMService imService;
    private boolean loginSuccess = false;

    private int delayMillis = 500;      //登录成功之后1秒钟之后跳转（1秒中有一些网络操作）这里必须设置一个延迟，不然进入MainActivity会黑屏一下
    private int delayAdMills = 2500;    //登录成功后展示宣传图 2秒后跳转
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
            try {
                do {
                    if (imService == null) {                                //后台服务启动链接失败
                        break;
                    }
                    IMLoginManager loginManager = imService.getLoginManager();
                    LoginSp loginSp = imService.getLoginSp();
                    if (loginManager == null || loginSp == null) {          // 无法获取登陆控制器
                        break;
                    }

                    LoginSp.SpLoginIdentity loginIdentity = loginSp.getLoginIdentity();
                    if (loginIdentity == null) {
                        /** 之前没有保存任何登陆相关的，跳转到登陆页面  */
                        handleNoLoginToLoginActivity();
                        break;
                    } else {
                        /** 判断密码是否为空 不为空就让他登陆 */
                        if (TextUtils.isEmpty(loginIdentity.getPwd())) {
                            handleNoLoginToLoginActivity();
                            break;
                        }
                        /** 自动登陆 */
                        handleGotLoginIdentity(loginIdentity);
                    }
                    return;
                } while (false);

                /** 异常分支都会执行这个 : 回到登陆界面 */
                handleNoLoginToLoginActivity();
            } catch (Exception e) {
                /**  任何未知的异常  */
                logger.w("loadIdentity failed");
                handleNoLoginToLoginActivity();
            }
        }
    };


    /**
     * 没有登陆跳转登陆界面
     */
    private void handleNoLoginToLoginActivity() {
        /** -------------------------------------判断是否使用邀请码成功登陆过 add by SummerRC  begin------------------------------------*/
        boolean invitation = SharedPreferenceUtils.getBooleanDate(this, "invitation", false);
        if (invitation) {
            uiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, LoginMobileActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                }
            }, delayMillis);
        } else {                //内测版本：如果本地配置文件显示没有注册过则请求网上的配置文件，确定是否跳到邀请码界面
            getConfiguration();
        }

        /** -------------------------------------判断是否使用邀请码成功登陆过 add by SummerRC    end------------------------------------*/
        /*uiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        }, 1000);*/
    }

    /**
     * 获取配置信息
     */
    private void getConfiguration() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("from", "Android");
        OkHttpClientManager.postAsy(ConstantValues.GetConfiguration, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if (object.getString("result").equals("1")) {
                                JSONObject Josnbject = new JSONObject(object.getString("describe"));
                                /** 判断是否跳转到邀请码界面 */
                                if (Josnbject.getString("invite_page_shown").equals("1")) {
                                    Intent intent = new Intent(SplashActivity.this, InvitationActivity.class);
                                    startActivity(intent);
                                    SplashActivity.this.finish();
                                } else {
                                    Intent intent = new Intent(SplashActivity.this, LoginMobileActivity.class);
                                    startActivity(intent);
                                    finish();
                                    overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
                                }
                            } else {
                                Toast.makeText(SplashActivity.this, object.getString("describe"), Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        } catch (JSONException e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(SplashActivity.this, "主人，网络不给力，我先休息一会！", 1);
                        finish();
                    }
                });
    }

    /**
     * 自动登陆
     */
    private void handleGotLoginIdentity(final LoginSp.SpLoginIdentity loginIdentity) {
        logger.i("login#handleGotLoginIdentity");
        uiHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                logger.d("login#start   login");
                if (imService == null || imService.getLoginManager() == null) {
                    Toast.makeText(SplashActivity.this, getString(R.string.login_failed), Toast.LENGTH_SHORT).show();
                    handleNoLoginToLoginActivity();
                    return;
                }
                imService.getLoginManager().login(loginIdentity);
            }
        }, 500);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_spash);
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);
//        checkPlatform();
        /** 系统的sp文件中 初始化 */
        SystemConfigSp.instance().init(this);
        /** 设置用户登陆服务器的地址 */
        if (TextUtils.isEmpty(SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.LOGINSERVER))) {
            SystemConfigSp.instance().setStrConfig(SystemConfigSp.SysCfgDimension.LOGINSERVER, UrlConstant.ACCESS_MSG_ADDRESS);
        }
    }


    private void checkPlatform() {
        /*String platfrom = getResources().getString(R.string.Platform_Type);
        if (platfrom.equals(getResources().getString(R.string.Platform_PP))) {
            img_splash.setBackgroundResource(R.drawable.activity_splash_pphelper);
        } else if (platfrom.equals(getResources().getString(R.string.Platform_360))) {
            img_splash.setBackgroundResource(R.drawable.activity_splash_360);
        } else {
            img_splash.setBackgroundResource(R.drawable.activity_splash);
        }*/
    }

    /**
     * ----------------------------event 事件驱动----------------------------
     */
    public void onEventMainThread(LoginEvent event) {
        switch (event) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGIN_OK:          //登陆成功后
                onLoginSuccess();
                break;
            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED:
                if (!loginSuccess)
                    onLoginFailure(event);
                break;
        }
    }


    public void onEventMainThread(SocketEvent event) {
        switch (event) {
            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                if (!loginSuccess)
                    onSocketFailure(event);
                break;
        }
    }

    //登陆成功后的操作
    private void onLoginSuccess() {
        logger.i("login#onLoginSuccess");
        loginSuccess = true;
        if(DateUtil.showAfterDate(2015,12,2)&&DateUtil.showBeforeDate(2015,12,4)){
            uiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    ImageView imageView = (ImageView) findViewById(R.id.image_splash);
                    imageView.setBackgroundResource(R.drawable.activity_splash_ad);
                }
            },delayMillis);
            uiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }
            }, delayAdMills);
        }else{
            uiHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    SplashActivity.this.finish();
                }
            }, delayMillis);
        }


    }

    //登陆失败后
    private void onLoginFailure(LoginEvent event) {
        logger.e("login#onLoginError -> errorCode:%s", event.name());
        handleNoLoginToLoginActivity();
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "1", Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        logger.e("login#onLoginError -> errorCode:%s,", event.name());
        handleNoLoginToLoginActivity();
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "2", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(SplashActivity.this);
    }
}
