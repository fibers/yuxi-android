package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;

import org.json.JSONObject;

public class InvitationActivity extends TTBaseNewActivity implements View.OnClickListener{
    private Logger logger = Logger.getLogger(InvitationActivity.class);

    private EditText et_code;       //指码输入框

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_invitation);
        initView();
    }

    private void initView() {
        et_code = (EditText) findViewById(R.id.et_code);
        findViewById(R.id.bt_sure).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_sure:
              String code = et_code.getText().toString().trim();
                if (code.isEmpty()){
                    Toast.makeText(InvitationActivity.this,"邀请码不能为空",Toast.LENGTH_SHORT).show();
                }else{
                    VerifyInviteCode(code);
                }
                break;
        }
    }

    private void VerifyInviteCode(String code) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("code", code);
        OkHttpClientManager.postAsy(ConstantValues.VerifyInviteCode, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        SharedPreferenceUtils.saveBooleanDate(InvitationActivity.this, "invitation", true);
                        IMUIHelper.openLoginActivity(InvitationActivity.this);
                        InvitationActivity.this.finish();
                    } else {
                        Toast.makeText(InvitationActivity.this,object.getString("describe"),Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
