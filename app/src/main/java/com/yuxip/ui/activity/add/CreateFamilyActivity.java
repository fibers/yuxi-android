package com.yuxip.ui.activity.add;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.GlobalVariable;
import com.yuxip.entity.FamilyInfoDao;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.manager.http.YXGroupTypeManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 创建家族
 * Created by SummerRC on 15/5/18.
 */
public class CreateFamilyActivity extends TTBaseActivity implements View.OnClickListener {

    private Logger logger = Logger.getLogger(CreateFamilyActivity.class);

    @InjectView(R.id.textinput_family_name)
    TextInputLayout textinputFamilyName;
    @InjectView(R.id.textinput_anhao)
    TextInputLayout textinputAnhao;
    @InjectView(R.id.textinput_introduce)
    TextInputLayout textinputIntroduce;
    @InjectView(R.id.iv_add_image)
    ImageView ivAddImage;

    private EditText et_title;      //家族名称

    private EditText et_info;       //家族简介
    private String groupId;

    private ProgressBar mProgressBar;
    private ScrollView mRootScrollView;

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";

    private Bitmap photo;
    private boolean CLICKABLE = true;
    private IMService imService;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);
        View view = LayoutInflater.from(this).inflate(R.layout.activity_create_zixi, topContentView);
        ButterKnife.inject(this, view);
        initView();
    }

    private void initView() {
        setTitle("创建家族");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleTextColor(getResources().getColor(R.color.pink));
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(this);
        textinputFamilyName.setHint("家族名称(30字以内)");
        textinputAnhao.setHint("尊贵用户特权码(没有可不填)");
        textinputIntroduce.setHint("家族介绍");
        ivAddImage.setOnClickListener(this);
        et_title = textinputFamilyName.getEditText();

        et_info = textinputIntroduce.getEditText();
        mProgressBar = (ProgressBar) findViewById(R.id.create_zixi_progress_bar);
        mRootScrollView = (ScrollView) findViewById(R.id.sv_create_zixi);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add_image:
                showAddImgDialog();
                break;
            case R.id.right_txt:
                createGroup();
                break;
        }
    }


    /**
     * 弹出添加图片的dialog
     */
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(CreateFamilyActivity.this, "创建家族", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }


    /**
     * 相机、相册的回调
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {
            return;
        }
        ContentResolver resolver = getContentResolver();

        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    //use bundle to get data
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        photo = (Bitmap) bundle.get("data"); //get bitmap
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateFamilyActivity.this);
                        ivAddImage.setImageBitmap(photo);

                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        //to do find the path of pic by uri
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateFamilyActivity.this);
                        ivAddImage.setImageBitmap(photo);
                    } catch (Exception e) {
                        T.show(getApplicationContext(), e.toString(), 0);
                    }
                }
                break;

            case PHOTORESOULT: //暂时没用
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    ivAddImage.setImageBitmap(photo);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * 创建家族群
     */
    private void createGroup() {
        if (TextUtils.isEmpty(et_title.getText().toString().trim()) || TextUtils.isEmpty(et_info.getText().toString().trim())) {
            T.show(getApplicationContext(), "输入内容不能为空", 0);
        } else if (photo == null) {
            T.show(getApplicationContext(), "请选择图片", 0);
        } else {
            if (CLICKABLE) {
                beginDeal();
                /** 先创建一个群，成功之后拿着群id去创建家族 */
                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_FAMILY;
                Set<Integer> ListSet = new HashSet<>();
                IMGroupManager groupMgr = imService.getGroupManager();
                int loginId = imService.getLoginManager().getLoginId();
                ListSet.add(loginId);
                groupMgr.reqCreateTempGroup(et_title.getText().toString(), ListSet);
            }
        }
    }

    @Override
    protected void onDestroy() {
        imServiceConnector.disconnect(this);
        imServiceConnector.unbindService(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void beginDeal() {
        setRighTitleText("创建中");
        mRootScrollView.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);

        CLICKABLE = false;
    }

    private void endDeal() {
        setRighTitleText("保存");
        mRootScrollView.setEnabled(true);
        mProgressBar.setVisibility(View.INVISIBLE);
        CLICKABLE = true;
    }

    /** ----------------------------------------------------------------事件驱动 -----------------------------------------------------------------*/
    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CREATE_GROUP_OK:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_FAMILY) {
                    return;
                }
                handleCreateGroupSuccess(event);
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_FAMILY) {
                    return;
                }
                handleCreateGroupFail();
                break;
        }
    }

    /**
     * 处理群创建成功、失败事件
     *
     * @param event 群创建成功、失败事件
     */
    private void handleCreateGroupSuccess(GroupEvent event) {
        /** 拿着群的id去创建家族 */
        groupId = event.getGroupEntity().getPeerId() + "";
        createFamily();
    }

    /**
     * 处理群创建成功、失败事件
     */
    private void handleCreateGroupFail() {
        T.show(getApplicationContext(), getString(R.string.create_temp_group_failed), 0);
        endDeal();
    }

    /**
     * 创建家族
     */
    private void createFamily() {
        /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
        Date date = new Date();
        /** 获取当前时间并且进一步转化为字符串 */
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
        String pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

        UpImgUtil upImgUtil = new UpImgUtil();
        upImgUtil.setFilePath(pic_path, pic_path);
        upImgUtil.saveHeadImg(photo);
        upImgUtil.upLoadPicture(new HeadImgListener() {
            public void notifyImgUploadFinished(final String url) {
                if (url == null) {
                    String notifyText = (String) getResources().getText(R.string.create_play_upload_img_fail);
                    T.show(getApplicationContext(), notifyText, 0);
                    endDeal();
                    return;
                }
                String uid = IMLoginManager.instance().getLoginId() + "";
                /** 放在params里面传递 */
                OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
                params.addParams("uid", uid);
                params.addParams("portrait", url);
                params.addParams("familyname", et_title.getText().toString().trim());
                params.addParams("familyintro", et_info.getText().toString().trim());
                params.addParams("groupid", groupId);
                params.addParams("token", "1");
                String vipCode = textinputAnhao.getEditText().getText().toString();
                if(!TextUtils.isEmpty(vipCode)) {
                    params.addParams("vipcode", vipCode);
                }
                OkHttpClientManager.postAsy(ConstantValues.NewFamily, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                    /** 请求成功后返回的Json */
                    @Override
                    public void onResponse(String response) {
                        endDeal();
                        try {
                            JSONObject object = new JSONObject(response);
                            String id = object.getString("result");
                            String describe = object.getString("describe");
                            switch (id) {
                                case "1": {     //创建成功、无特权或者特权码正确
                                    insertToMyFamilyMap(Integer.valueOf(groupId), url);
                                    finish();
                                    break;
                                }
                                case "2": {     //特权码不正确，创建失败
                                    textinputAnhao.getEditText().setText("");
                                    textinputAnhao.requestFocus();
                                    textinputAnhao.setFocusable(true);
                                    break;
                                }
                                case "3":       //家族重名。创建失败。
                                    et_title.setText("");
                                    et_title.requestFocus();
                                    et_title.setFocusable(true);
                                    break;
                                default:        //其他错误
                                    et_title.setText("");
                                    et_title.requestFocus();
                                    et_title.setFocusable(true);
                                    break;
                            }
                            endDeal();
                            showToast(describe);
                        } catch (Exception e) {
                            logger.e(e.toString());
                            endDeal();
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), e.toString(), 1);
                        endDeal();
                    }
                });
            }
        });
    }

    /**
     *  更新我的家族集合
     */
    private void insertToMyFamilyMap(Integer groupId, String portrait) {
        FamilyInfoDao familyInfoDao = new FamilyInfoDao();
        familyInfoDao.setPortrait(portrait);
        familyInfoDao.setGroupid(groupId);
        YXFamilyManager.instance().addFamilyInfoDao(groupId, familyInfoDao);
        YXGroupTypeManager.instance().setGroupType(groupId, ConstantValues.GROUP_TYPE_FAMILY);
    }

}
