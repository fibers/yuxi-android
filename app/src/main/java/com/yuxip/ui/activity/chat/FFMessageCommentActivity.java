package com.yuxip.ui.activity.chat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.JsonBean.ChatMessageBean;
import com.yuxip.JsonBean.CommentGroupsInfo;
import com.yuxip.JsonBean.MessageBean;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.ReconnectEvent;
import com.yuxip.imservice.event.SessionEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.event.UnreadEvent;
import com.yuxip.imservice.event.UserInfoEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMReconnectManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.FriendMessageAdapterH;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.NetworkUtil;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * 评论群的消息列表界面
 * Created by SummerRC on 2015/5/11.
 */
public class FFMessageCommentActivity extends TTBaseActivity implements AdapterView.OnItemSelectedListener,
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private FriendMessageAdapterH contactAdapter;
    private ListView contactListView;
    private View curView = null;
    private View noNetworkView;
    private View noChatView;
    private ImageView notifyImage;
    private TextView displayView;
    private ProgressBar reconnectingProgressBar;
    private IMService imService;
    private ProgressBar progress_bar;
    private Map<Integer, CommentGroupsInfo.StorysEntity> storysEntityMap;

    private List<CommentGroupsInfo.StorysEntity> otherStorys;
    private List<RecentInfo> recentInfos = new ArrayList<RecentInfo>();
    /**
     * 是否是手动点击重练。false:不显示各种弹出小气泡. true:显示小气泡直到错误出现
     */
    private volatile boolean isManualMConnect = false;
    private ArrayList<MessageBean> messageBeans = new ArrayList<>();
    private int count = 0;

    /**
     * 每个界面需要重写的方法
     */
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {

        /**service因异常而断开连接的时候，这个方法才会用到。 */
        @Override
        public void onServiceDisconnected() {
            if (EventBus.getDefault().isRegistered(FFMessageCommentActivity.this)) {
                EventBus.getDefault().unregister(FFMessageCommentActivity.this);
            }
        }

        /** 服务被调用的时候 */
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            /** 依赖联系人回话、未读消息、用户的信息三者的状态 */
            onRecentContactDataReady();
            EventBus.getDefault().registerSticky(FFMessageCommentActivity.this);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);

        initView();
    }

    public void initView() {
        LayoutInflater.from(this).inflate(R.layout.ff_fragment_message_h, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        setTitle("评论消息");
        /** 多端登陆也在用这个view */
        noNetworkView = findViewById(R.id.layout_no_network);
        noChatView = findViewById(R.id.layout_no_chat);
        reconnectingProgressBar = (ProgressBar) findViewById(R.id.progressbar_reconnect);
        displayView = (TextView) findViewById(R.id.disconnect_text);
        displayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                startActivity(intent);
            }
        });
        notifyImage = (ImageView) findViewById(R.id.imageWifi);
        progress_bar = (ProgressBar) findViewById(R.id.progress_bar);

        /** 初始化联系人列表视图 */
        initContactListView();
    }

    private void initContactListView() {
        contactListView = (ListView) findViewById(R.id.listview);
        contactListView.setOnItemClickListener(this);
        contactListView.setOnItemLongClickListener(this);
        /** 最近联系人的 消息列表 */
        contactAdapter = new FriendMessageAdapterH(this);
        contactListView.setAdapter(contactAdapter);

        /** this is critical, disable loading when finger sliding, otherwise
         you'll find sliding is not very smooth  */
        contactListView.setOnScrollListener(
                new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.i("123", "FFMessageCommentActvivity的resume方法");
        if (IMApplication.IS_REFRESH) {
            Log.i("123", "FFMessageCommentActvivity的resume方法true");
            IMApplication.IS_REFRESH = false;
            onRecentContactDataReady();
            contactAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(FFMessageCommentActivity.this)) {
            EventBus.getDefault().unregister(FFMessageCommentActivity.this);
        }
        imServiceConnector.disconnect(this);
        super.onDestroy();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    /**
     * 这个地方跳转一定要快
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!contactAdapter.getData().isEmpty()) {
            MessageBean mb = contactAdapter.getData().get(position);
            if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL
                    || mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_GROUP) { //消息类型是：聊天

                startCommentGroupChat((ChatMessageBean) mb);
            } else if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_SYSTEM) { //消息类型是：系统消息

                IMUIHelper.openSystemMessageActivity(this);
                imService.getSessionManager().reqRemoveSession(recentInfos.get(position));
            }
            MessageBean m = messageBeans.get(position);
            m.setMessageCount(0);
            contactAdapter.notifyDataSetChanged();
//           删除对应会话
//            imService.getSessionManager().reqRemoveSession(recentInfos.get(position));
        }
    }

    /**
     * 开始聊天。
     */
    private void startCommentGroupChat(ChatMessageBean cmb) {
        String sessionKey = cmb.getSessionKey();
        GroupEntity groupEntity = imService.getGroupManager().findGroup(cmb.getPeerId());
        if (groupEntity == null) {
            T.show(getApplicationContext(), "数据请求失败，请稍候再试！", 0);
            return;
        }
        String userList = groupEntity.getUserList();
        imService.getContactManager().reqUsersByids(userList);
        String portrait, title, creatorName, createTime, type;
        if (cmb.getType() == ChatMessageBean.MESSAGE_TYPE_STORY) {
       /*     StoryParentEntity entity = YXStoryManager.instance().getStoryByGroupid(cmb.getPeerId());*/
            StoryParentEntity entity = new StoryParentEntity();
            portrait = entity.getPortrait();
            title = entity.getTitle();
            createTime = entity.getCreatetime();
            creatorName = entity.getCreatorname();
            type = IntentConstant.STORY;
        } else {
            CommentGroupsInfo.StorysEntity entity = storysEntityMap.get(cmb.getPeerId());
            portrait = entity.getStoryimg();
            title = entity.getTitle();
            createTime = entity.getCreatetime();
            creatorName = entity.getCreatorname();
            type = IntentConstant.TOPIC;
        }
        IMUIHelper.openTopicMessageActivity(this, type, sessionKey, portrait, title, creatorName, createTime);
    }

    public void onEventMainThread(SessionEvent sessionEvent) {
        switch (sessionEvent) {
            case RECENT_SESSION_LIST_UPDATE:
            case RECENT_SESSION_LIST_SUCCESS:
            case SET_SESSION_TOP:
                onRecentContactDataReady();
                break;
        }
    }


    public void onEventMainThread(UnreadEvent event) {
        switch (event.event) {
            case UNREAD_MSG_RECEIVED:
            case UNREAD_MSG_LIST_OK:
            case SESSION_READED_UNREAD_MSG:
                onRecentContactDataReady();
                break;
        }
    }

    public void onEventMainThread(UserInfoEvent event) {
        switch (event) {
            case USER_INFO_UPDATE:
            case USER_INFO_OK:
                onRecentContactDataReady();
                searchDataReady();
                break;
        }
    }

    public void onEventMainThread(LoginEvent loginEvent) {
        switch (loginEvent) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGINING: {
                if (reconnectingProgressBar != null) {
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                }
            }
            break;

            case LOCAL_LOGIN_MSG_SERVICE:
            case LOGIN_OK: {
                isManualMConnect = false;
                noNetworkView.setVisibility(View.GONE);
            }
            break;

            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED: {
                onLoginFailure(loginEvent);
            }
            break;

            case PC_OFFLINE:
            case KICK_PC_SUCCESS:
                onPCLoginStatusNotify(false);
                break;

            case KICK_PC_FAILED:
                Toast.makeText(this, getString(R.string.kick_pc_failed),
                        Toast.LENGTH_SHORT).show();
                break;
            case PC_ONLINE:
                onPCLoginStatusNotify(true);
                break;

            default:
                reconnectingProgressBar.setVisibility(View.GONE);
                break;
        }
    }


    public void onEventMainThread(SocketEvent socketEvent) {
        switch (socketEvent) {
            case MSG_SERVER_DISCONNECTED:
                handleServerDisconnected();
                break;

            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                handleServerDisconnected();
                onSocketFailure(socketEvent);
                break;
        }
    }

    public void onEventMainThread(ReconnectEvent reconnectEvent) {
        switch (reconnectEvent) {
            case DISABLE: {
                handleServerDisconnected();
            }
            break;
        }
    }

    private void onLoginFailure(LoginEvent event) {
        if (!isManualMConnect) {
            return;
        }
        isManualMConnect = false;
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        reconnectingProgressBar.setVisibility(View.GONE);
        Toast.makeText(this, errorTip, Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        if (!isManualMConnect) {
            return;
        }
        isManualMConnect = false;
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        reconnectingProgressBar.setVisibility(View.GONE);
        Toast.makeText(this, errorTip, Toast.LENGTH_SHORT).show();
    }

    /**
     * 更新页面以及 下面的未读总计数
     */
    private void onShieldSuccess(GroupEntity entity) {
//        if (entity == null) {
//            return;
//        }
//        /** 更新某个sessionId */
//        contactAdapter.updateRecentInfoByShield(entity);
//        IMUnreadMsgManager unreadMsgManager = imService.getUnReadMsgManager();
//
//        int totalUnreadMsgCnt = unreadMsgManager.getTotalUnreadCount();
//         ((MainActivity) this).setUnreadMessageCntFriend(totalUnreadMsgCnt);
    }

    private void onShieldFail() {
        Toast.makeText(this, R.string.req_msg_failed, Toast.LENGTH_SHORT).show();
    }


    /**
     * 搜索数据OK
     * 群组数据与 user数据都已经完毕
     */
    public void searchDataReady() {
        if (imService.getContactManager().isUserDataReady() && imService.getGroupManager()
                .isGroupReady()) {
//            showSearchFrameLayout();
        }
    }

    /**
     * 多端，PC端在线状态通知
     *
     * @param isOnline
     */
    public void onPCLoginStatusNotify(boolean isOnline) {
        if (isOnline) {
            reconnectingProgressBar.setVisibility(View.GONE);
            noNetworkView.setVisibility(View.VISIBLE);
            notifyImage.setImageResource(R.drawable.pc_notify);
            displayView.setText(R.string.pc_status_notify);
            /**添加踢出事件*/
            noNetworkView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                    imService.getLoginManager().reqKickPCClient();
                }
            });
        } else {
            noNetworkView.setVisibility(View.GONE);
        }
    }

    private void handleServerDisconnected() {

        if (reconnectingProgressBar != null) {
            reconnectingProgressBar.setVisibility(View.GONE);
        }

        if (noNetworkView != null) {
            notifyImage.setImageResource(R.drawable.warning);
            noNetworkView.setVisibility(View.VISIBLE);
            if (imService != null) {
                if (imService.getLoginManager().isKickout()) {
                    displayView.setText(R.string.disconnect_kickout);
                } else {
                    displayView.setText(R.string.no_network);
                }
            }
            /**重连【断线、被其他移动端挤掉】*/
            noNetworkView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMReconnectManager manager = imService.getReconnectManager();
                    if (NetworkUtil.isNetWorkAvalible(FFMessageCommentActivity.this)) {
                        isManualMConnect = true;
                        IMLoginManager.instance().relogin();
                    } else {
                        Toast.makeText(FFMessageCommentActivity.this, R.string.no_network_toast, Toast.LENGTH_SHORT)
                                .show();
                        return;
                    }
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    /**
     * 这个处理有点过于粗暴
     */
    public void onRecentContactDataReady() {

        boolean isUserData = imService.getContactManager().isUserDataReady();
        boolean isSessionData = imService.getSessionManager().isSessionListReady();
        boolean isGroupData = imService.getGroupManager().isGroupReady();

        if (!(isUserData && isSessionData && isGroupData)) {
            return;
        }


//         recentInfos = imService.getSessionManager().getRecentCommentListInfo();
        recentInfos.clear();
//        imService.getSessionManager().reset();
        recentInfos = imService.getSessionManager().getRecentCommentListInfo();
        String groupIds = "";
        int index = 0;
        for (RecentInfo recentInfo : recentInfos) {
            if (recentInfo.getSessionType() == MessageBean.MessageType.MESSAGE_TYPE_GROUP
                    .ordinal()) {

                int groupid = recentInfo.getPeerId();

/*                StoryParentEntity entity = YXStoryManager.instance().getStoryByGroupid(groupid);*/
                StoryParentEntity entity = new StoryParentEntity();

                if (entity == null) {
                    if (index == 0) {
                        groupIds = recentInfo.getPeerId() + "";
                    } else {
                        groupIds = groupIds + "," + recentInfo.getPeerId();
                    }
                    index++;
                }


            }
        }

        if (index > 0) {
            OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
            params.addParams("uid", imService.getLoginManager().getLoginId() + "");
            params.addParams("grouplist", groupIds);
            final String finalGroupIds = groupIds;

            OkHttpClientManager.postAsy(ConstantValues.GetMyCommentGroupsInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                /** 请求成功后返回的Json */
                @Override
                public void onResponse(String response) {
                    CommentGroupsInfo commentGroupsInfo = new Gson().fromJson(response,
                            CommentGroupsInfo.class);
                    if (commentGroupsInfo.getResult().equals("1")) {
                        otherStorys = commentGroupsInfo.getStorys();
                        dealMessage(recentInfos, otherStorys, finalGroupIds);
                    }
                }

                @Override
                public void onError(Request request, Exception e) {
                    T.show(FFMessageCommentActivity.this,
                            "网络状况不好，请稍后重试", 1);
                }
            });
        } else {
            dealMessage(recentInfos, null, groupIds);
        }
    }

    private void dealMessage(List<RecentInfo> recentInfos1,
                             List<CommentGroupsInfo.StorysEntity> storys,
                             String groupids) {

        storysEntityMap = new HashMap<>();

        if (storys != null) {
            for (CommentGroupsInfo.StorysEntity story : storys) {
                storysEntityMap.put(Integer.valueOf(story.getGroupid()), story);
            }
        }

        ArrayList<RecentInfo> recentInfos = new ArrayList<>();
        recentInfos.addAll(recentInfos1);

        if (null != recentInfos && !recentInfos.isEmpty() && 0 != recentInfos.size()) {
            messageBeans.clear();
            for (int i = recentInfos.size() - 1; i >= 0; i--) {
                RecentInfo r = recentInfos.get(i);
                ChatMessageBean chatMB = new ChatMessageBean();
                if (r.getSessionType() == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL.ordinal()) {
                    chatMB.setiMessageTypeDetail(
                            MessageBean.MessageTypeDetailPersonal.MESSAGE_TYPE_PERSONAL);
                } else if (r.getSessionType() == MessageBean.MessageType.MESSAGE_TYPE_GROUP
                        .ordinal()) {
                    chatMB.setiMessageTypeDetail(
                            MessageBean.MessageTypeDetailGroup.MESSAGE_TYPE_GROUP_FAMILY);
                }

                chatMB.setSessionKey(r.getSessionKey());
                chatMB.setMessage(r.getLatestMsgData());
                chatMB.setMessageCount(r.getUnReadCnt());

                String name = "神秘故事";
                String img = "";
                int type = ChatMessageBean.MESSAGE_TYPE_STORY;
                //如果id包含在之前获取的非剧的id列表中，则判断为自戏或者话题
                if (groupids.contains(r.getPeerId() + "")) {
                    CommentGroupsInfo.StorysEntity story = storysEntityMap.get(r.getPeerId());
                    if (story != null) {
                        name = story.getTitle();
                        img = story.getStoryimg();
                    } else {
                        //todo 临时解决方法。当剧被删除，但是评论群没有被删除，会到这里。这个时候需要删除一下对应的信息。
                        recentInfos1.remove(r);
                        imService.getSessionManager().reqRemoveSession(r);
                        continue;
                    }
                    type = ChatMessageBean.MESSAGE_TYPE_OTHER;
                } else {
//                    StoryParentEntity entity = YXStoryManager.instance().getStoryByGroupid(r.getPeerId());
                    StoryParentEntity entity = new StoryParentEntity();
                    name = entity.getTitle();
                    img = entity.getPortrait();
                }

//                chatMB.setTime(DateUtil.getSessionTime(r.getUpdateTime()));
                chatMB.setTime(String.valueOf(r.getUpdateTime()));
                chatMB.setPeerId(r.getPeerId());
                chatMB.setType(type);


                chatMB.setName(name);
                LinkedList<String> list = new LinkedList<>();
                list.add(img);
                chatMB.setAvatar(list);
                int index = isHaveChatMsg(chatMB);
                if (index == -1) {
                    messageBeans.add(chatMB);
                } else {
                    if (r.getUnReadCnt() > 0) {
                        messageBeans.remove(index);
                        messageBeans.add(index, chatMB);
                    }
                }
            }

            if (!messageBeans.isEmpty()) {

                //升序排序
                Collections.sort(messageBeans, new Comparator<MessageBean>() {
                    @Override
                    public int compare(MessageBean messageBean, MessageBean t1) {
                        Integer m = Integer.parseInt(messageBean.getTime());
                        Integer m1 = Integer.parseInt(t1.getTime());
                        return m.compareTo(m1);
                    }
                });
                //反转
                Collections.reverse(messageBeans);
                contactAdapter.setData(messageBeans, this);

                count = 0;

            }
        } else {
            messageBeans.clear();
        }
    }


    /**
     * 返回这条消息的index，不存在返回-1.
     *
     * @param chatMessageBean
     * @return
     */
    private int isHaveChatMsg(ChatMessageBean chatMessageBean) {
        if (!messageBeans.isEmpty()) {
            for (int i = 0; i < messageBeans.size(); i++) {
                MessageBean mb = messageBeans.get(i);
                if (mb instanceof ChatMessageBean) {
                    if (((ChatMessageBean) mb).getSessionKey()
                            .equals(chatMessageBean.getSessionKey())) {
                        return i;
                    }
                }

            }
        }
        return -1;
    }


    private void setNoChatView(List<RecentInfo> recentSessionList) {
        if (recentSessionList.size() == 0) {
            noChatView.setVisibility(View.VISIBLE);
        } else {
            noChatView.setVisibility(View.GONE);
        }
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (position == 0) {
            return false;
        }

        MessageBean mb = contactAdapter.getData().get(position);

        if (mb == null) {
            return false;
        }
        handleContactItemLongClick(this, mb);

        return true;
    }


    private boolean isContains(int value, ArrayList<Integer> list) {
        for (int i = 0; i < list.size(); i++) {
            if (value == list.get(i)) {
                return true;
            }
        }
        return false;
    }

    private void handleContactItemLongClick(final Context ctx, final MessageBean messageBean) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(ctx, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle(messageBean.getName());

        String[] items = new String[]{ //ctx.getString(R.string.check_profile),
                ctx.getString(R.string.delete_session),
        };

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        ChatMessageBean chatMessageBean = (ChatMessageBean) messageBean;
                        RecentInfo recentInfo = new RecentInfo();
                        recentInfo.setSessionKey(chatMessageBean.getSessionKey());
                        recentInfo.setSessionType(2);   //代表是群消息
                        recentInfo.setPeerId(chatMessageBean.getPeerId());
                        imService.getSessionManager().reqRemoveSession(recentInfo);
                        break;
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();

    }

    /**
     * 现在只有群组存在免打扰的
     *
     * @param ctx
     * @param recentInfo
     */
    private void handleGroupItemLongClick(final Context ctx, final RecentInfo recentInfo) {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                new ContextThemeWrapper(ctx, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle(recentInfo.getName());

        final boolean isTop = imService.getConfigSp().isTopSession(recentInfo.getSessionKey());
        final boolean isForbidden = recentInfo.isForbidden();
        int topMessageRes = isTop ? R.string.cancel_top_message : R.string.top_message;
        int forbidMessageRes = isForbidden ? R.string.cancel_forbid_group_message : R.string.forbid_group_message;

        String[] items = new String[]{ctx.getString(R.string.delete_session),
                ctx.getString(topMessageRes),
                ctx.getString(forbidMessageRes)};

        builder.setItems(items, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        imService.getSessionManager().reqRemoveSession(recentInfo);
                        break;
                    case 1: {
                        imService.getConfigSp().setSessionTop(recentInfo.getSessionKey(), !isTop);
                    }
                    break;
                    case 2: {
                        /** 底层成功会事件通知 */
                        int shieldType = isForbidden ? DBConstant.GROUP_STATUS_ONLINE : DBConstant.GROUP_STATUS_SHIELD;
                        imService.getGroupManager()
                                .reqShieldGroup(recentInfo.getPeerId(), shieldType);
                    }
                    break;
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }

}
