package com.yuxip.ui.activity.home;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.other.FamilyDetailsActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/28.
 * 家族页面 这里是从个人主页或者他人主页点 家族 进去的
 */
public class HisFamilyE extends TTBaseActivity {
    @InjectView(R.id.hisFamilyView)
    ListView listView;
    @InjectView(R.id.no_family)
    TextView tv_nofamily;
    private List<HomeInfo.PersoninfoEntity.MyHomeEntity> mlist = new ArrayList<>();
    private String fromUser;
    private MyAdapter myAdapter;
    private ImageLoader imageLoader;
    private DisplayImageOptions imageOptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = getLayoutInflater().inflate(R.layout.tt_activity_hisfamily, topContentView);
        ButterKnife.inject(this, view);
        initRes();
    }

    private void initRes() {
        setTitle("家族");
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        Drawable drawable = DrawableCache.getInstance(getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
        imageOptions = ImageLoaderUtil.getOptions(drawable);
        topLeftBtn.setVisibility(View.VISIBLE);
        fromUser = getIntent().getStringExtra(IntentConstant.FROMUSER);
        if (fromUser.equals("0")) {
            mlist = HomeEntityManager.getInstance().getUserList(2, "0");
        } else {
            mlist = HomeEntityManager.getInstance().getUserList(2, "1");
        }
        myAdapter = new MyAdapter();
        listView.setAdapter(myAdapter);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isMember(mlist.get(position).getId())) {
                    /**     跳转家族详情页  暂时没有退群刷新列表  */
                    Intent i = new Intent(HisFamilyE.this, FamilyDetailsActivity.class);
                    i.putExtra(IntentConstant.SESSION_KEY, "2_" + mlist.get(position).getId());
                    i.putExtra(IntentConstant.FAMILY_INFO_START_FROM, "contact");
                    startActivity(i);
                } else {
                    /**      自己不在家族中     */
                    IMUIHelper.openFamilyDataActivity(HisFamilyE.this, IntentConstant.FamilyDataActivityType.TYPE_NOT_MEMBER, mlist.get(position).getId());
                }
            }
        });

        if (mlist == null || mlist.size() == 0) {
            tv_nofamily.setVisibility(View.VISIBLE);
            tv_nofamily.setText("该用户没有家族");
            listView.setVisibility(View.GONE);
        }
    }

    /**
     * 判断自己是否是在该家族中
     */
    private boolean isMember(String id) {
        return YXFamilyManager.instance().isMember(Integer.valueOf(id));
    }

    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mlist.size();
        }

        @Override
        public Object getItem(int position) {
            return mlist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.item_listview_hisfamilye, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.tv_name.setText(mlist.get(position).getTitle());
            if (!TextUtils.isEmpty(mlist.get(position).getPortrait()))
                imageLoader.displayImage(mlist.get(position).getPortrait(), viewHolder.iv_family, imageOptions);
            return convertView;
        }

        class ViewHolder {
            TextView tv_name;
            CircularImage iv_family;

            public ViewHolder(View view) {
                tv_name = (TextView) view.findViewById(R.id.tv_name_hisfamilye_adapter);
                iv_family = (CircularImage) view.findViewById(R.id.iv_cover_hisfamilye_adapter);
            }
        }
    }
}
