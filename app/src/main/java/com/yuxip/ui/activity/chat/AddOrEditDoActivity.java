package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ExpressionEntity;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.SharedPreferenceUtils;

/**
 * 添加或者修改DO（表情标签）的activity
 * Created by SRC on 2015/4/15.
 */
public class AddOrEditDoActivity extends TTBaseActivity implements View.OnClickListener {
    private String title = "";
    private String contont = "";
    private String type;
    private EditText et_biaoqian;
    private EditText et_biaoqing;
    private DBInterface dbInterface;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (super.isNeedRestart()) {
            return;
        }

        dbInterface = DBInterface.instance();
        getLayoutInflater().inflate(R.layout.activity_add_do, topContentView);
        et_biaoqian = (EditText) findViewById(R.id.et_biaoqian);
        et_biaoqing = (EditText) findViewById(R.id.et_biaoqing);

        type = getIntent().getStringExtra(IntentConstant.TYPE);
        if (!TextUtils.isEmpty(type)&&type.equals(IntentConstant.TYPE_ADD)) {
            setTitle("添加DO");
            righTitleTxt.setText("保存");
        } else {
            title = getIntent().getStringExtra(IntentConstant.TITLE);
            contont = getIntent().getStringExtra(IntentConstant.CONTENT);
            et_biaoqian.setText(title);
            et_biaoqing.setText(contont);
            righTitleTxt.setText("修改");
            setTitle("编辑DO");
            findViewById(R.id.iv_delete).setVisibility(View.VISIBLE);
            findViewById(R.id.iv_delete).setOnClickListener(this);
        }

        topLeftBtn.setVisibility(View.VISIBLE);

        topLeftBtn.setOnClickListener(this);

        righTitleTxt.setOnClickListener(this);
    }

    /**
     * 监听器
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);

        switch (v.getId()) {
            case R.id.left_btn:         //返回
                /** 隐藏键盘,否则activity返回后会影响到表情键盘 */
                imm.hideSoftInputFromWindow(et_biaoqian.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(et_biaoqing.getWindowToken(), 0);
                this.finish();
                break;
            case R.id.right_txt:        //保存或者修改标签
                if (TextUtils.isEmpty(et_biaoqian.getText().toString()) || TextUtils.isEmpty(et_biaoqing.getText().toString())) {
                    Toast.makeText(this, "请设置表情标签", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (type.equals(IntentConstant.TYPE_ADD)) {
                    /** 将用户新定义的表情添加到数据库表情表中 */
                    ExpressionEntity expressionEntity = new ExpressionEntity();
                    expressionEntity.setLabel(et_biaoqian.getText().toString());
                    expressionEntity.setContent(et_biaoqing.getText().toString());
                    insertOrUpdateExpression(expressionEntity);
                } else {
                    /** 更新套 */
                    ExpressionEntity expressionEntity = dbInterface.getExpressionEntityByLabelAndContent(title, contont);
                    if (expressionEntity != null) {
                        expressionEntity.setLabel(et_biaoqian.getText().toString());
                        expressionEntity.setContent(et_biaoqing.getText().toString());
                        insertOrUpdateExpression(expressionEntity);
                    }
                }

                /** 隐藏键盘,否则activity返回后会影响到表情键盘 */
                imm.hideSoftInputFromWindow(et_biaoqian.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(et_biaoqing.getWindowToken(), 0);
                this.finish();
                break;
            case R.id.iv_delete:        //删除套
                dbInterface.deleteExpressionEntityByLabelAndContent(title, contont);
                /** 隐藏键盘,否则activity返回后会影响到表情键盘 */
                imm.hideSoftInputFromWindow(et_biaoqian.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(et_biaoqing.getWindowToken(), 0);
                this.finish();
                break;
        }
    }

    public void insertOrUpdateExpression(ExpressionEntity expressionEntity) {
        dbInterface.insertOrUpdateExpressionEntity(expressionEntity);
    }


    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
    }
}
