package com.yuxip.ui.activity.home;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.MyRoleAuditsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.add.story.MyEmptyView;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/11/5.
 * description:剧角色申请列表
 */
public class RoleApplyListActivity extends TTBaseActivity {

    private MyEmptyView emptyView;
    private RoleAuditAdapter adapter;
    private List<MyRoleAuditsJsonBean.ListEntity> listRoleAudits = new ArrayList<>();
    private String loginId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_my_role_apply_audit, topContentView);
        initRes();
        requestRoleAudits();
    }

    private void initRes() {
        loginId = IMLoginManager.instance().getLoginId() + "";
        setTitle(getResources().getString(R.string.story_role_apply));
        topLeftBtn.setImageResource(R.drawable.back_default_btn);
        topLeftBtn.setVisibility(View.VISIBLE);
        ListView lvRoleAudits = (ListView) findViewById(R.id.lv_my_role_apply_audit);
        emptyView = (MyEmptyView) findViewById(R.id.eptview_my_role_apply_audit);
        emptyView.showProgressBar();
        adapter = new RoleAuditAdapter();
        lvRoleAudits.setAdapter(adapter);
        lvRoleAudits.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!TextUtils.isEmpty(listRoleAudits.get(position).getApplyid()))
                    IMUIHelper.openRoleAuditActivity(RoleApplyListActivity.this, listRoleAudits.get(position).getApplyid());
            }
        });
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * 请求申请列表
     */
    private void requestRoleAudits() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        OkHttpClientManager.postAsy(ConstantValues.GetMyStoryRoleApplications, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), getResources().getString(R.string.story_net_status_error), 0);
                        setClickRetry();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }

    /**
     * 解析数据
     * @param response      网络请求返回的body
     */
    private void onReceiveData(String response) {
        try {
            MyRoleAuditsJsonBean roleAuditsJsonBean = new Gson().fromJson(response, MyRoleAuditsJsonBean.class);
            if (roleAuditsJsonBean.getList() != null && roleAuditsJsonBean.getList().size() > 0) {
                listRoleAudits = roleAuditsJsonBean.getList();
                adapter.notifyDataSetChanged();
                emptyView.setVisibility(View.GONE);
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.role_apply_no_data), Toast.LENGTH_LONG).show();
                emptyView.showMsgText(getResources().getString(R.string.role_apply_no_data));
            }

        } catch (Exception e) {
            setClickRetry();
            T.show(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), 0);
        }
    }

    /**
     * 失败重新请求
     */
    private void setClickRetry() {
        emptyView.showFailText(getResources().getString(R.string.request_fail_click_to_retry));
        emptyView.setFailTextClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emptyView.showProgressBar();
                requestRoleAudits();
            }
        });
    }


    private class RoleAuditAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return listRoleAudits.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(RoleApplyListActivity.this).inflate(R.layout.item_lv_my_role_audits, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tvStoryName.setText(getResources().getString(R.string.story_story_name_e) + " " + listRoleAudits.get(position).getApplystoryname());
            viewHolder.tvRoleName.setText(getResources().getString(R.string.story_apply_role_e) + " " + listRoleAudits.get(position).getApplyroletype());
            viewHolder.tvUserName.setText(listRoleAudits.get(position).getApplicantname());
            viewHolder.tvApplyTime.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(listRoleAudits.get(position).getApplytime())));
            if (listRoleAudits.get(position).getApplicantportrait()!=null) {
                viewHolder.ivUserHead.loadImage(listRoleAudits.get(position).getApplicantportrait());
            }
            viewHolder.ivUserHead.setTag(position);
            viewHolder.ivUserHead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (int) v.getTag();
                    if (!TextUtils.isEmpty(listRoleAudits.get(_position).getApplicantuid()))
                        IMUIHelper.openUserHomePageActivity(RoleApplyListActivity.this, listRoleAudits.get(_position).getApplicantuid());
                }
            });
            return convertView;
        }

        class ViewHolder {
            private TextView tvStoryName;
            private TextView tvUserName;
            private TextView tvApplyTime;
            private TextView tvRoleName;
            private CustomHeadImage ivUserHead;

            public ViewHolder(View view) {
                tvStoryName = (TextView) view.findViewById(R.id.tv_item_lv_storyname_role_audit);
                tvUserName = (TextView) view.findViewById(R.id.tv_item_lv_username_role_audit);
                tvApplyTime = (TextView) view.findViewById(R.id.tv_item_lv_applytime_role_audit);
                tvRoleName = (TextView) view.findViewById(R.id.tv_item_lv_rolename_role_audit);
                ivUserHead = (CustomHeadImage) view.findViewById(R.id.iv_item_lv_userhead_role_audit);
            }
        }
    }

}
