package com.yuxip.ui.activity.story;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.GroupsEntity;
import com.yuxip.entity.MemberEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.manager.DataManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMSessionManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * add by SummerRC on 2015/5/16.
 * 剧的各种设置里面剧群设置的界面（只有管理员能进入这个界面）
 */
public class StoryGroupActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryGroupActivity.class);

    private ArrayList<GroupsEntity> groupList = new ArrayList<>();     //指定剧用户所在的群列表的数据源
    private ListView storyGroupList;                                    //指定剧用户所在的群列表
    private String storyId;                                             //剧id
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_story_group, topContentView);
        storyGroupList = (ListView) findViewById(R.id.storyGroupList);
        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("添加");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoryGroupActivity.this, StoryAddGroupActivity.class);
                intent.putExtra(IntentConstant.STORY_ID, storyId);
                startActivity(intent);

            }
        });
        setTitle("剧群");
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDate();
    }

    /**
     * 初始化数据
     */
    private void initDate() {
        if(TextUtils.isEmpty(storyId))
            return;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        /** 放在params里面传递 */
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        OkHttpClientManager.postAsy(ConstantValues.GETSTORYINFO, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {

            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                groupList.clear();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONArray groups = object.getJSONArray("groups");
                        for (int i = 0; i < groups.length(); i++) {
                            JSONObject obj = groups.getJSONObject(i);
                            String isPlay = (String) obj.get("isplay");

                            if (!isPlay.equals(ConstantValues.GROUP_TYPE_COMMENT)) {
                                GroupsEntity entity = new GroupsEntity();
                                entity.setGroupId((String) obj.get("groupid"));
                                entity.setTitle((String) obj.get("title"));
                                entity.setIsPlay((String) obj.get("isplay"));
                                groupList.add(entity);
                            }
                        }

                    }
                    GroupAdapter storyGroupAdapter = new GroupAdapter(groupList, StoryGroupActivity.this);
                    storyGroupList.setAdapter(storyGroupAdapter);
                    storyGroupList.setOnScrollListener(
                            new PauseOnScrollListener(ImageLoader.getInstance(), true, true));

                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }

    private class GroupAdapter extends BaseAdapter {
        private ArrayList<GroupsEntity> groupList;
        private Context context;
        private boolean IS_NULL = false;

        public GroupAdapter(ArrayList<GroupsEntity> groupList, Context context) {
            this.groupList = groupList;
            this.context = context;
        }

        @Override
        public int getCount() {
            if (groupList == null || groupList.size() == 0) {
                IS_NULL = true;
                return 1;
            } else {
                return groupList.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                convertView = View.inflate(context, R.layout.story_group_count_item, null);
                holder.groupType = (TextView) convertView.findViewById(R.id.groupType);
                holder.tv_unread_count = (TextView) convertView.findViewById(R.id.tv_unread_count);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (IS_NULL) {
                holder.groupType.setText("暂无内容");
            } else {
                holder.groupType.setText(groupList.get(position).getTitle() + "");
                final String isPlay = groupList.get(position).getIsPlay() + "";
                convertView.findViewById(R.id.rl_btm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String groupId = groupList.get(position).getGroupId();
                        progressBar.setVisibility(View.VISIBLE);
                        getStoryGroupInfo(groupId, isPlay);
                    }
                });
                /** 遍历所有的最近会话，取出当前群的未读消息数 */
                int unreadCount = 0;
                for (RecentInfo recentInfo : IMSessionManager.instance().getRecentListInfo()) {
                    if (String.valueOf(recentInfo.getPeerId()).equals(groupList.get(position).getGroupId())) {
                        unreadCount += recentInfo.getUnReadCnt();
                    }
                }
                holder.tv_unread_count.setText(unreadCount + "");
            }

            return convertView;
        }


        private class ViewHolder {
            private TextView groupType;             //内容
            private TextView tv_unread_count;       //未读消息数
        }
    }

    /**
     * 获取将要聊天的群成员信息用于显示角色名
     */
    private void getStoryGroupInfo(final String groupId, final String isPlay) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        params.addParams("groupid", groupId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                Map<String, MemberEntity> map = new HashMap<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String result = object.getString("result");
                    if (result.equals("1")) {
                        JSONObject object_groupInfo = object.getJSONObject("groupinfo");
                        JSONArray array = object_groupInfo.getJSONArray("members");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            MemberEntity entity = new MemberEntity();
                            entity.setId(obj.getString("id"));
                            entity.setTitle(obj.getString("title"));
                            entity.setNickname(obj.getString("nickname"));
                            entity.setRoleName(obj.getString("rolename"));
                            entity.setPortrait(obj.getString("portrait"));
                            map.put(entity.getId(), entity);
                        }
                    }
                    DataManager.getInstance().putStoryGroupMemberMap(groupId, map);
                    progressBar.setVisibility(View.GONE);
                    //IMUIHelper.opensChatActivity(StoryGroupActivity.this, storyId, groupId, isPlay, true);
                    IMUIHelper.openDramaChatActivity(StoryGroupActivity.this, "2_" + groupId, storyId);
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    T.show(getApplicationContext(), "网络失败请重试", 1);
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                progressBar.setVisibility(View.GONE);
                T.show(getApplicationContext(), "网络失败请重试", 1);
            }
        });
    }
}
