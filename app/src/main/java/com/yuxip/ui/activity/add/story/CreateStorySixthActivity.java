package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.CreateGroupType;
import com.yuxip.config.GlobalVariable;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.CreateStoryEvent;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/11/2.
 * 剧流程6:设置规则
 */
public class CreateStorySixthActivity extends Activity implements View.OnClickListener, CreateStoryManager.CreateStoryListener {
    private final int ruleSettingLimit = 300;
    private TextView tvPublish;
    private EditText etRuleRequire;
    private RelativeLayout rlAbovePb;
    private CustomDividerProgressbar progressbar;
    private InputMethodManager inputMethodManager;
    private ProgressBar progressBar;
    private int type;
    private IMService imService;
    private Logger logger;
    private String storyId;
    private String reviewGroupId;
    private CreateStoryManager createStoryManager;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story_sixth);
        imServiceConnector.connect(this);
        logger = Logger.getLogger(CreateStorySixthActivity.class);
        createStoryManager = CreateStoryManager.getInstance();
        EventBus.getDefault().register(this);
        initRes();
        checkManagerData();
    }

    private void initRes() {
        inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        ImageView ivBack = (ImageView) findViewById(R.id.iv_back_create_story_sixth);
        tvPublish = (TextView) findViewById(R.id.tv_publish_create_story_sixth);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        etRuleRequire = (EditText) findViewById(R.id.et_rule_require_create_story_sixth);
        rlAbovePb = (RelativeLayout) findViewById(R.id.rl_above_create_story_sixth);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_sixth);
        tvPublish.setSelected(true);
        ivBack.setOnClickListener(this);
        tvPublish.setOnClickListener(this);
        createStoryManager.setCreateStoryListener(this);
        etRuleRequire.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s.toString())) {
//                    tvPublish.setSelected(false);
                } else {
                    if (createStoryManager.getCharacterCount(s.toString()) == 0) {
//                        tvPublish.setSelected(false);
//                        return;
                    } else if (createStoryManager.getCharacterCount(s.toString()) > ruleSettingLimit) {
                        etRuleRequire.setTextColor(getResources().getColor(R.color.red_text));
                    } else {
                        etRuleRequire.setTextColor(getResources().getColor(R.color.black));
                    }
//                    tvPublish.setSelected(true);
                }
            }
        });
        progressbar.setCurProgress(100);
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
    }


    /**
     * 检查填充数据
     */
    private void checkManagerData() {
        String content = createStoryManager.getRuleSetting();
        if (!TextUtils.isEmpty(content) && createStoryManager.getCharacterCount(content) > 0) {
            etRuleRequire.setText(content);
//            tvPublish.setSelected(true);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_create_story_sixth:       //返回
                openPreAcitivty();
                finish();
                break;
            case R.id.tv_publish_create_story_sixth:    //发布
                startPublicStory();
                break;
        }
    }

    /**------------------------------------------------------发布剧的流程-----------------------------------------------------------------------
     *                  先上传图片、上传成功之后创建审核群、然后拿针审核区id去创建剧、用返回的剧id去关联审核群、
     *              关联成功之后创建水聊群、然后再关联剧、关联成功之后创建对戏群、成功之后再关联剧
     *---------------------------------------------------------------------------------------------------------------------------------------*/
    /**
     * 开始发布剧
     */
    private void startPublicStory() {
        if (!tvPublish.isSelected()) {
            T.show(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), 0);
            return;
        }
        if (createStoryManager.getCharacterCount(etRuleRequire.getText().toString().trim()) > ruleSettingLimit) {
            T.show(getApplicationContext(), getResources().getString(R.string.story_rule_setting_limit_300), 0);
            return;
        }
        saveManageData();
        inputMethodManager.hideSoftInputFromWindow(etRuleRequire.getWindowToken(), 0);
//        createStoryManager.uploadImage();
        rlAbovePb.setVisibility(View.VISIBLE);
        if (createStoryManager.checkAllData()) {
            beginDeal();
            /** 先创建一个审核群，成功之后创建剧，然后关联审核群和剧，关联成功之后创建水聊群和对戏群 */
            GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
            type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW;
            createGroup("审核群");
        } else {
            rlAbovePb.setVisibility(View.GONE);
            T.show(getApplicationContext(), getResources().getString(R.string.story_create_fail), 0);
        }
    }

//    @Override
//    public void uploadImgSuccess() {
//        T.show(getApplicationContext(), getResources().getString(R.string.story_upload_img_success), 0);
//        /** 这里处理关联群 然后提交 */
//        if (createStoryManager.checkAllData()) {
//            beginDeal();
//            /** 先创建一个审核群，成功之后创建剧，然后关联审核群和剧，关联成功之后创建水聊群和对戏群 */
//            GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
//            type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW;
//            createGroup("审核群");
//        } else {
//            rlAbovePb.setVisibility(View.GONE);
//            T.show(getApplicationContext(), getResources().getString(R.string.story_create_fail), 0);
//        }
//    }

//    @Override
//    public void uploadImgFailed() {
//        T.show(getApplicationContext(), getResources().getString(R.string.story_upload_img_fail), 0);
//        rlAbovePb.setVisibility(View.GONE);
//    }

    /**
     * @param groupName 创建群的名字：审核群 水聊群  对戏群
     */
    private void createGroup(String groupName) {
        Set<Integer> ListSet = new HashSet<>();
        IMGroupManager groupMgr = imService.getGroupManager();
        int loginId = imService.getLoginManager().getLoginId();
        ListSet.add(loginId);
        groupMgr.reqCreateTempGroup(groupName, ListSet);
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CREATE_GROUP_OK:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY) {
                    return;
                }
                handleCreateGroupSuccess(event);
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY) {
                    return;
                }
                handleCreateGroupFail();
                break;
        }
    }

    /**
     * 处理群创建成功事件
     *
     * @param event 创建群的事件
     */
    private void handleCreateGroupSuccess(GroupEvent event) {
        switch (type) {
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW:             //创建审核群成功事件
                reviewGroupId = event.getGroupEntity().getPeerId() + "";
                /** 创建剧 */
                createStoryManager.submitCreateStory(reviewGroupId);
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT:         //创建水聊群成功事件
                String waterChatGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联水聊群和剧   */
                addGroupToStory(waterChatGroupId, "水聊群");
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY:            //创建对戏群成功事件
                String isPlayGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联对戏群和剧   */
                addGroupToStory(isPlayGroupId, "对戏群");
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_COMMENT:            //创建评论群成功事件
                isPlayGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联评论群和剧   */
                addGroupToStory(isPlayGroupId, "评论群");
                break;
        }
    }

    /**
     * 处理群创建失败事件
     */
    private void handleCreateGroupFail() {
        endDeal();
        Toast.makeText(this, getString(R.string.create_temp_group_failed), Toast.LENGTH_SHORT).show();
        tvPublish.setClickable(true);
    }

    @Override
    public void createStorySuccess(String describe) {
        String[] array = getData(describe);
        storyId = array[0];
        /** 关联剧和审核群 */
        addGroupToStory(reviewGroupId, "审核群");
    }

    @Override
    public void createStoryFail(String reason) {
        T.show(getApplicationContext(), reason, 0);
        rlAbovePb.setVisibility(View.GONE);
        tvPublish.setClickable(true);
        endDeal();
    }

    /**
     * 关联剧和群
     *
     * @param groupId   ：审核群id  水聊群id  对戏群id
     * @param groupName ：审核群    水聊群    对戏群
     */
    private void addGroupToStory(String groupId, String groupName) {
        /** 是否是对戏群 */
        String isPlay;
        if (groupName.contains("对戏群")) {
            isPlay = ConstantValues.GROUP_TYPE_PLAY;
        } else if (groupName.contains("审核群")) {
            isPlay = ConstantValues.GROUP_TYPE_SHENHE;
        } else if (groupName.contains("评论群")) {
            isPlay = ConstantValues.GROUP_TYPE_COMMENT;
        } else {
            isPlay = ConstantValues.GROUP_TYPE_SHUILIAO;
        }

        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);            //剧管理员id
        params.addParams("storyid", storyId);    //剧id
        params.addParams("title", groupName);    //群的名字
        params.addParams("intro", createStoryManager.getAbstractContent());
        params.addParams("groupid", groupId);
        params.addParams("isplay", isPlay);
        params.addParams("token", "1");

        OkHttpClientManager.postAsy(ConstantValues.AddGroupToStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        switch (type) {
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW:             //关联审核群成功
                                /** 创建水聊群 */
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                                type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT;
                                createGroup("水聊群");
                                break;
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT:         //关联水聊群成功
                                /** 创建对戏群 */
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                                type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY;
                                createGroup("对戏群");
                                break;
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY:            //关联对戏群成功
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                                T.show(getApplicationContext(), "剧创建成功", 0);
                                IMApplication.IS_REFRESH = true;
                                /** 发送事件，关闭其他Activity */
                                createStoryManager.clearAllData();
                                CreateStoryEvent createStoryEvent = new CreateStoryEvent();
                                createStoryEvent.eventType = CreateStoryEvent.Event.TYPE_CREATE_STORY_SUCCESS;
                                EventBus.getDefault().post(createStoryEvent);
//                                IMUIHelper.openStoryEditActivity(CreateStorySixthActivity.this, storyId, IMLoginManager.instance().getLoginId() + "", "owener", "1");
                                IMUIHelper.openApplyRoleAdminActivity(CreateStorySixthActivity.this, storyId);
                                finish();
                                break;
                        }
                    } else {
                        endDeal();
                        T.show(getApplicationContext(), object.getString("describe"), 0);
                        tvPublish.setClickable(true);
                    }
                } catch (Exception e) {
                    endDeal();
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                endDeal();
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    private void saveManageData() {
        createStoryManager.setRuleSetting(etRuleRequire.getText().toString());
        T.show(getApplicationContext(), getResources().getString(R.string.story_publish), 0);
    }

    private void beginDeal() {
        progressBar.setVisibility(View.VISIBLE);
        tvPublish.setText("发布中");
    }

    private void endDeal() {
        progressBar.setVisibility(View.INVISIBLE);
        tvPublish.setText("发布");
    }

    /**
     * 分割字符串返回一个数组
     * 0 : 剧id
     * 1 : 审核群id
     */
    private String[] getData(String str) {
        return str.split(",");
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            openPreAcitivty();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void finish() {
        CreateStoryManager.getInstance().setCreateStoryListener(null);
        super.finish();
    }

    @Override
    protected void onDestroy() {
        createStoryManager.setCreateStoryListener(null);
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void openPreAcitivty(){
        saveManageData();
        IMUIHelper.openCreateStoryOrderActivity(this, 5, IntentConstant.ACTIVITY_OPEN_TYPE_LEFT);
    }
}

