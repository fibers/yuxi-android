package com.yuxip.ui.activity.add;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.R;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by ZQF on 2015/10/13.
 * 发表评论
 */
public class ReleaseCommentActivity extends Activity implements View.OnClickListener {

    private static Logger logger = Logger.getLogger(ReleaseCommentActivity.class);

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final String IMAGE_UNSPECIFIED = "image/*";

    private Context mContext;

    private EditText mContentEt;                                     // 标题,内容
    private TextView mCancleTv, mReleaseTv, mCountTv, mCountPointTv;           // 取消,发布,1/9,红点
    private ImageView mAddIv;                                                  // 添加图片
    private RelativeLayout mUploadRl;                                          //
    private LinearLayout mPicDetialLl, mPicsLl;                                // 底部布局,图片列表布局
    private HorizontalScrollView mHoScrollView;
    private boolean isPicsVisible = false;                                     // 图片列表是否可见
    private boolean isClickable = false;                                       // 全局点击事件控制
    private Bitmap photo;
    private int picCount = 0;
    private float density, pWidth, pHeight;

    private GestureDetector mGestureDetector;
    private View mDrapView;
    private MyHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment_release);

        mContext = this;
        mHandler = new MyHandler(this);
        density = mContext.getResources().getDisplayMetrics().density;
        pWidth = 91 * density + 0.5f;
        pHeight = 130 * density + 0.5f;

        initView();
        addListeners();
    }

    private void addListeners() {
        mCancleTv.setOnClickListener(this);
        mReleaseTv.setOnClickListener(this);
        mAddIv.setOnClickListener(this);
        mUploadRl.setOnClickListener(this);

        mContentEt.addTextChangedListener(mTextChangeListener);
        mContentEt.setOnFocusChangeListener(mFocusChangeListener);
    }

    private void initView() {
        mCancleTv = (TextView) findViewById(R.id.tv_cancle);
        mReleaseTv = (TextView) findViewById(R.id.tv_release);

        mContentEt = (EditText) findViewById(R.id.et_topic_content);
        mCountTv = (TextView) findViewById(R.id.tv_pic_count);
        mCountPointTv = (TextView) findViewById(R.id.tv_pic_count_point);
        mHoScrollView = (HorizontalScrollView) findViewById(R.id.hsv_topic_release);
        mUploadRl = (RelativeLayout) findViewById(R.id.rl_pic_upload);
        mAddIv = (ImageView) findViewById(R.id.iv_pic_add);
        mAddIv.setImageResource(R.drawable.topic_pic_add);
        mPicDetialLl = (LinearLayout) findViewById(R.id.ll_pic_upload_detail);
        mPicDetialLl.setVisibility(View.GONE);
        mPicsLl = (LinearLayout) findViewById(R.id.ll_pics);
        findViewById(R.id.tv_pic_delete).setBackgroundColor(Color.TRANSPARENT);

        mGestureDetector = new GestureDetector(this, new DrapGestureListener());
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_cancle:
                closePicsLayout();
                finish();
                break;
            case R.id.tv_release:
                Toast.makeText(mContext, "发布", Toast.LENGTH_SHORT).show();
                closePicsLayout();
                List<String> urls = new ArrayList<>();
                for (int i = 0; i < mPicsLl.getChildCount() - 1; i++) {
                    String url = (String) mPicsLl.getChildAt(i).getTag();
                    urls.add(url);
                }
                break;
            case R.id.iv_pic_add:
                showAddImgDialog();
                break;
            case R.id.rl_pic_upload:
                if (isPicsVisible) {
                    closePicsLayout();
                } else {
                    mPicDetialLl.setVisibility(View.VISIBLE);
                    int type = TranslateAnimation.RELATIVE_TO_SELF;

                    TranslateAnimation animation = new TranslateAnimation(type, 0, type, 0, type, 1, type, 0);
                    animation.setDuration(500);
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            isPicsVisible = true;
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    mPicDetialLl.startAnimation(animation);
                }
                break;
            default:
                break;
        }
    }

    private void closePicsLayout() {
        if (isPicsVisible) {
            int type = TranslateAnimation.RELATIVE_TO_SELF;
            TranslateAnimation animation = new TranslateAnimation(type, 0, type, 0, type,
                    0, type, 1);
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mPicDetialLl.setVisibility(View.GONE);
                    isPicsVisible = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            mPicDetialLl.startAnimation(animation);
        }
    }

    //弹出添加图片的dialog
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(mContext, "发起话题", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }

    private View.OnFocusChangeListener mFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (hasFocus) {
                closePicsLayout();
            }
        }
    };

    // 监听内容变化
    private TextWatcher mTextChangeListener = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String content = mContentEt.getText().toString().trim();
            if (!TextUtils.isEmpty(content)) {
                mReleaseTv.setTextColor(Color.WHITE);
                mReleaseTv.setBackgroundResource(R.drawable.topic_release_btn_blue);
                mReleaseTv.setClickable(true);
            } else {
                mReleaseTv.setTextColor(mContext.getResources().getColor(R.color.topic_message_text_1));
                mReleaseTv.setBackgroundResource(R.drawable.topic_release_btn_white);
                mReleaseTv.setClickable(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {
            return;
        }
        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
            case PHOTOZOOM:   //选图片
                addImage(data);
                break;
            default:
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void addImage(final Intent data) {
        final View layout = LayoutInflater.from(mContext).inflate(R.layout.topic_release_pic_item, null);
        ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        mPicsLl.addView(layout, mPicsLl.getChildCount() - 1);
        layout.setOnTouchListener(mOnTouchListener);
        layout.setOnDragListener(mOnDragListener);

        TextView delete = (TextView) layout.findViewById(R.id.tv_pic_delete);
        delete.setVisibility(View.VISIBLE);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getVisibility() != View.VISIBLE)
                    return;
                mPicsLl.removeView(layout);
                if (picCount == 9) {
                    mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.VISIBLE);
                }
                picCount--;
                mCountTv.setText(picCount + " / 9");
                if (picCount > 0) {
                    mCountPointTv.setText(String.valueOf(picCount));
                } else {
                    mCountPointTv.setVisibility(View.GONE);
                }
            }
        });
        mHoScrollView.scrollBy(0, layout.getWidth());
        picCount++;
        if (mCountPointTv.getVisibility() != View.VISIBLE) {
            mCountPointTv.setVisibility(View.VISIBLE);
        }
        mCountPointTv.setText(String.valueOf(picCount));
        mCountTv.setText(picCount + " / 9");
        if (picCount >= 9) {
            mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.GONE);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                Uri uri = data.getData();
                ContentResolver resolver = getContentResolver();
                if (uri == null) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        photo = (Bitmap) bundle.get("data"); //get bitmap
                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                        e.printStackTrace();
                    }
                }
                photo = ImageUtil.getBigBitmapForDisplay(photo, mContext);
                Message message = Message.obtain();
                message.obj = layout;
                mHandler.sendMessage(message);
            }
        }).start();
    }

    private static class MyHandler extends Handler {
        private WeakReference<ReleaseCommentActivity> weakReference;

        public MyHandler(ReleaseCommentActivity activity) {
            weakReference = new WeakReference<ReleaseCommentActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            ReleaseCommentActivity activity = weakReference.get();
            if (activity == null) {
                return;
            }
            final View layout = (View) msg.obj;
            activity.uploadPic(layout);
        }
    }

    private void uploadPic(final View layout) {
        /** 添加图片后滑动到加号位置 */
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mAddIv.getLayoutParams();
        mHoScrollView.scrollTo((layoutParams.width + layoutParams.leftMargin) * mPicsLl.getChildCount(), 0);

        /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
        Date date = new Date();
        /** 获取当前时间并且进一步转化为字符串 */
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
        String pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";
        UpImgUtil upImgUtil = new UpImgUtil();
        upImgUtil.setFilePath(pic_path, pic_path);
        upImgUtil.saveHeadImg(photo);
        upImgUtil.upLoadPicture(new HeadImgListener() {
            @Override
            public void notifyImgUploadFinished(String url) {
                layout.setTag(url);
            }
        });
        photo = zoomBitmap(photo, (int) pWidth, (int) (pWidth * photo.getHeight() / photo.getWidth()));
        ImageView imageView = (ImageView) layout.findViewById(R.id.iv_pic_add);
        imageView.setImageBitmap(photo);
        ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.GONE);
    }

    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mDrapView = v;
            if (mGestureDetector.onTouchEvent(event))
                return true;

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        }
    };

    private View.OnDragListener mOnDragListener = new View.OnDragListener() {

        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    View view = (View) event.getLocalState();
                    for (int i = 0, j = mPicsLl.getChildCount(); i < j; i++) {
                        if (mPicsLl.getChildAt(i) == v) {
                            // 当前位置
                            mPicsLl.removeView(view);
                            v.setAlpha(0);
                            mPicsLl.addView(view, i);
                            break;
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setAlpha(1F);
                    break;
                case DragEvent.ACTION_DROP:
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setAlpha(1F);
                default:
                    break;
            }
            return true;
        }
    };

    private class DrapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return super.onSingleTapConfirmed(e);
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
            ClipData data = ClipData.newPlainText("", "");
            MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
                    mDrapView);
            mDrapView.startDrag(data, shadowBuilder, mDrapView, 0);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    // 创建图片影子
    private class MyDragShadowBuilder extends View.DragShadowBuilder {

        private final WeakReference<View> mView;

        public MyDragShadowBuilder(View view) {
            super(view);
            mView = new WeakReference<View>(view);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
//			canvas.scale(1.5F, 1.5F);
            super.onDrawShadow(canvas);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {
            // super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);

            final View view = mView.get();
            if (view != null) {
                shadowSize.set((int) (view.getWidth() * 1.5F),
                        (int) (view.getHeight() * 1.5F));
                shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
            } else {
                // Log.e(View.VIEW_LOG_TAG,
                // "Asked for drag thumb metrics but no view");
            }
        }
    }

    private static Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {
        if (null == bitmap) {
            return null;
        }
        try {
            int w = bitmap.getWidth();
            int h = bitmap.getHeight();
            Matrix matrix = new Matrix();
            float scaleWidth = ((float) width / w);
            float scaleHeight = ((float) height / h);
            matrix.postScale(scaleWidth, scaleHeight);
            Bitmap newbmp = Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
            return newbmp;
        } catch (Exception e) {
            logger.e(e.getMessage());
            return null;
        }
    }
}
