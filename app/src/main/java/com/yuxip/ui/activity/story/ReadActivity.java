package com.yuxip.ui.activity.story;


import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.StoryBookMarkEntity;
import com.yuxip.JsonBean.StoryContent;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.ReadStoryAdapter;
import com.yuxip.ui.widget.xlistview.XListView;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/6/2.
 * 阅读界面
 */
public class ReadActivity extends TTBaseActivity {
    private View curView;
    private String loginId;
    private String storyid;

    private TextView tv_read_orgincount, tv_read_totalcount;
    private SeekBar seekBar;
    private ReadStoryAdapter adapter;

    private View footView;
    private XListView listView;
    private TextView emptyView;


    private List<StoryContent.ContentlistEntity> contentList = new ArrayList<>();
    private DBInterface dbInterface;
    private int frommsgid = 0;
    private int direction = 0;
    private int count = 10;
    private int progress;
    private boolean isLoading;          //判断是否是请求数据状态
    private boolean canLoad = true;       //判断是否全部加载完,不再加载更多
    private ProgressBar progressBar;

    private StoryBookMarkEntity storyMark;

    private int sumCount;
    private int currentItem;        //当前可视的条目
    private int totalCount;
    private boolean isFirstLoad;
    private boolean isFromUser;         //判断是否是来自用户的手势操作
    private int currentItemTemp;    //当前可视的条目,区别于数据库操作,一个是动态改变的,一个为静态存储
    private int countTemp;
    private int pullPosition;   //当前被用户拖拽到的位置
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 2:

                    progressBar.setVisibility(View.GONE);
                    if (isFirstLoad) {
                        isFirstLoad = false;
                        listView.setSelection(currentItemTemp);
                        if (contentList == null || contentList.size() == 0) {
                            emptyView.setText("该剧没有内容哦~~\n\n\n请换个剧试试吧(* ﾟДﾟ)");
                            emptyView.setVisibility(View.VISIBLE);
                        }
                    }
                    if (isFromUser) {
                        listView.setSelection(pullPosition);
                        progressBar.setVisibility(View.GONE);
                        isFromUser = false;   //设置后一定要把isFromUser置否,不然进度监听会失效
                    }
                    isLoading = false;
                    break;
                case 3:
//                    isFromUser=false;
                    break;
            }
        }
    };

    private void initBookMark() {
        dbInterface = DBInterface.instance();
        if (dbInterface == null) return;
        storyMark = dbInterface.getStoryBookMarkEntityById(storyid + "");
        if (storyMark != null && !TextUtils.isEmpty(storyMark.getMarkItem())) {
            currentItemTemp = currentItem = Integer.valueOf(storyMark.getMarkItem());
            countTemp = Integer.valueOf(storyMark.getTotalCount());
        }
    }

    private void saveBookMark() {
        if (dbInterface == null) return;
        if (storyMark == null) {
            storyMark = new StoryBookMarkEntity();
            storyMark.setStoryId(storyid);
            storyMark.setMarkItem(currentItem + "");
            storyMark.setTotalCount(totalCount + "");
            storyMark.setFromMsgId(((maxMsgId() - 1) > 0 ? maxMsgId() : 0) + "");
            dbInterface.batchInsertOrUpdateStoryBookMarkEntity(storyMark);
        } else {
            storyMark.setMarkItem(currentItem + "");
            storyMark.setTotalCount(totalCount + "");
            storyMark.setFromMsgId(((maxMsgId() - 1) > 0 ? maxMsgId() : 0) + "");
            dbInterface.batchInsertOrUpdateStoryBookMarkEntity(storyMark);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        curView = View.inflate(ReadActivity.this, R.layout.activity_read_play, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        setTitle("阅读");
        loginId = IMLoginManager.instance().getLoginId() + "";
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        initBookMark();
        initView();
        firstLoad();
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void initView() {
        emptyView = (TextView) curView.findViewById(R.id.empty_read);
        progressBar = (ProgressBar) curView.findViewById(R.id.progressbar_read);
        listView = (XListView) curView.findViewById(R.id.listview_read);
        listView.setPullRefreshEnable(false);
        listView.setPullLoadEnable(false);
        footView = View.inflate(ReadActivity.this, R.layout.footer_view, null);
        footView.setVisibility(View.GONE);
        listView.addFooterView(footView);
        adapter = new ReadStoryAdapter(this, contentList);

        listView.setAdapter(adapter);

        tv_read_orgincount = (TextView) curView.findViewById(R.id.tv_orgincount_read_play);
        tv_read_totalcount = (TextView) curView.findViewById(R.id.tv_totalcount_read_play);
        seekBar = (SeekBar) curView.findViewById(R.id.seekbar_read_play);


        /********************  listview的滑滚事件监听  *********************/
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentItem = firstVisibleItem;
                if (isFromUser)      //如果是用户拖拽操作,则不监听scroll状态
                    return;
                if (sumCount == 0)
                    return;
                if (firstVisibleItem == 0) {
                    seekBar.setProgress(0);
                    tv_read_orgincount.setText(0 + "");
                } else if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    if (canLoad) {
                        load();
                    } else {
                        footView.setVisibility(View.GONE);
                    }

                } else if (firstVisibleItem + visibleItemCount == (totalItemCount - 1) && !canLoad) {
                    seekBar.setProgress(100);
                } else {
                    seekBar.setProgress((firstVisibleItem) * 100 / sumCount);
                    tv_read_orgincount.setText(firstVisibleItem + "");
                }
            }
        });

        /*******************************  seekbar的进度状态监听  *********************************/
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                isFromUser = fromUser;
                if (progress == 100) {
                    tv_read_orgincount.setText(sumCount + "");
                } else {
                    tv_read_orgincount.setText(sumCount * progress / 100 + "");
                }


                ReadActivity.this.progress = progress;


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (isFromUser) {
                    pullPosition = sumCount * progress / 100;
                    if (contentList.size() >= (sumCount * progress / 100)) {
                        listView.setSelection(sumCount * progress / 100);
                        isFromUser = false;
                    } else {
                        progressBar.setVisibility(View.VISIBLE);
                        count = count + (sumCount * progress / 100 - contentList.size()) + 10; //在当前索引位置向后增10,避免过去继续加载下一页
                        load();
                    }

                }
            }
        });
    }


    /**
     * *******************执行网络请求************************
     */
    private void requestHttpData() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyid);
        params.addParams("frommsgid", frommsgid + "");
        params.addParams("direction", direction + "");
        if (isFirstLoad && countTemp != 0 && countTemp > 10) {
            params.addParams("count", countTemp + "");
        } else {
            params.addParams("count", count + "");
        }
        OkHttpClientManager.postAsy(ConstantValues.GetStoryContent, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveHttpData(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), "请求数据失败，请刷新重试", 1);
                footView.setVisibility(View.GONE);
                handler.sendEmptyMessageDelayed(2, 500);
            }
        });
    }

    private void onReceiveHttpData(String json) {
        StoryContent storyContent = null;
        try {
             storyContent = new Gson().fromJson(json, StoryContent.class);
        }catch (Exception e){
            e.printStackTrace();
            T.show(getApplicationContext(),"网络数据异常，请稍候重试",1);
            return;

        }
        if (storyContent != null) {
            if (storyContent.getResult().equals("1")) {
                List<StoryContent.ContentlistEntity> contents = storyContent.getContentlist();
                /**无内容 同时*/
                sumCount = Integer.valueOf(TextUtils.isEmpty(storyContent.getTotalcontens()) ? "0" : storyContent.getTotalcontens());
                tv_read_totalcount.setText(sumCount + "");
                if (contents != null && contents.size() == 0) {
                    footView.setVisibility(View.GONE);
                    if (direction == 0) {
                        canLoad = false;
                        progressBar.setVisibility(View.GONE);
                    }

                    handler.sendEmptyMessageDelayed(2, 600);
                    return;
                }

                if (contentList.size() == 0) {
                    if(contents != null) {
                        contentList.addAll(contents);
                        adapter.notifyDataSetChanged();
                    }

                } else {
                    if(contents != null) {
                        if (direction == 1) {
                            contentList.clear();
                        }
                        contentList.addAll(contents);
                        adapter.notifyDataSetChanged();
                    }
                }
                if(contents != null) {
                    /**第一次加载 如果不足十条则隐藏progressbar*/
                    if (isFirstLoad && contents.size() < 10) {
                        canLoad = false;
                        footView.setVisibility(View.GONE);
                    } else {
                        footView.setVisibility(View.VISIBLE);
                    }
                    tv_read_orgincount.setText((contentList.size() * progress / 100) + "");
                }
            } else {
                T.show(getApplicationContext(), "请求网络数据失败", 0);
                handler.sendEmptyMessageDelayed(2, 500);
                return;
            }
        }

        if (isFirstLoad) {
            handler.sendEmptyMessageDelayed(2, 1500);
        } else {
            count = 10;
            handler.sendEmptyMessageDelayed(2, 500);
        }


    }


    private void firstLoad() {
        isLoading = true;
        isFirstLoad = true;
        direction = 0;
        requestHttpData();
    }


    /**
     * 加载更多功能
     */
    private void load() {
        if (isLoading) {
            return;
        }
        isLoading = true;
        frommsgid = maxMsgId();
        direction = 0;
        requestHttpData();
    }


    private int maxMsgId() {
        if (contentList != null && !contentList.isEmpty()) {
            int max = Integer.parseInt(contentList.get(0).getMsgid());
            for (int i = 1; i < contentList.size(); i++) {
                int m = Integer.parseInt(contentList.get(i).getMsgid());
                if (m > max) {
                    max = m;
                }
            }
            return max;
        }
        return 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        totalCount = contentList.size();
        saveBookMark();
    }

}
