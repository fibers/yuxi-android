package com.yuxip.ui.activity.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.RemarkNameManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * created by SummerRC on 2015/8/11.
 * description : 编辑好友昵称
 */
public class EditRemarkNameActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(EditRemarkNameActivity.class);

    private EditText et_content;
    private Button bt_delete;
    private String uid;
    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_edit_remark_name, topContentView);
        et_content = (EditText) findViewById(R.id.et_rule);
        uid = getIntent().getStringExtra(IntentConstant.UID);
        name = getIntent().getStringExtra(IntentConstant.REMARK_NAME);
        if (name != null) {
            et_content.setText(name);
        }
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("修改备注");
        setRighTitleText("完成");
        topBar.setBackgroundColor(Color.parseColor("#f5f5f5"));
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!name.equals(et_content.getText().toString().trim())) {
                    name = et_content.getText().toString().trim();
                    modifyUserName();
                } else {
                    T.show(EditRemarkNameActivity.this, "请修改昵称", 1);
                }
            }
        });
        bt_delete = (Button) findViewById(R.id.bt_delete);
        bt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_content.setText("");
            }
        });
    }


    /**
     * 修改用户昵称
     */
    private void modifyUserName() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("personid", uid);
        params.addParams("personnick", name);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyUserName, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        RemarkNameManager.getInstance().editNickNameByUserId(uid, name);
                        Intent intent = new Intent();
                        intent.putExtra(IntentConstant.REMARK_NAME, et_content.getText().toString().trim());
                        EditRemarkNameActivity.this.setResult(RESULT_OK, intent);
                        EditRemarkNameActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "服务器忙", Toast.LENGTH_SHORT).show();
                        EditRemarkNameActivity.this.finish();
                        finish();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

}
