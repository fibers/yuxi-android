
package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;

import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseNewFragmentActivity;

public class GroupMemberSelectActivity extends TTBaseNewFragmentActivity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if(super.isNeedRestart()) {
            return;
        }

        setContentView(R.layout.tt_activity_group_member_select);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode)
            return;
    }

}
