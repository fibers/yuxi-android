package com.yuxip.ui.activity.story;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryRoleSettingJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.add.story.CreateStoryManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.NoScrollGridView;
import com.yuxip.ui.helper.KeyBoardObserver;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ly on 2015/11/2.
 * 申请角色界面
 */
public class ApplyRoleActivity extends TTBaseActivity implements View.OnClickListener {
    private NoScrollGridView gridView;   //想扮演的角色
    private TextView tvBackground;   //身世背景
    private LinearLayout llcontainer;
    private RelativeLayout rlBottom;
    private EditText etContent;      //审核戏文
    private TextView tvApply;
    private LinearLayout llbackground;
    private List<Boolean> listRoleSelect = new ArrayList<>();
    private RoleGridAdapter roleGridAdapter;
    private boolean isRoleselectOk;
    private boolean isListDataOk;
    private boolean isArticleOk;
    private final int articleWordsLimit = 1000;
    private String storyid;
    private StoryRoleSettingJsonBean storyRoleSettingJsonBean;
    private List<StoryRoleSettingJsonBean.RolesettingsEntity.RoletypesEntity> listRoletypes = new ArrayList<>();
    private List<StoryRoleSettingJsonBean.RolesettingsEntity.RolenaturesEntity> listRoleNatures = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_role_apply, topContentView);
        initRes();
        requestRoleSettings();
    }

    private void initRes() {
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        setTitle(getResources().getString(R.string.role_apply_role));
        topLeftBtn.setImageResource(R.drawable.back_default_btn);
        topLeftBtn.setVisibility(View.VISIBLE);
        gridView = (NoScrollGridView) findViewById(R.id.gv_role_apply);
        tvBackground = (TextView) findViewById(R.id.tv_role_background_intro);
        llcontainer = (LinearLayout) findViewById(R.id.ll_role_apply);
        etContent = (EditText) findViewById(R.id.et_article_to_aduit);
        tvApply = (TextView) findViewById(R.id.tv_apply_role_apply);
        llbackground = (LinearLayout) findViewById(R.id.ll_background_apply_role);
        rlBottom = (RelativeLayout) findViewById(R.id.rl_bottom_role_apply);
        roleGridAdapter = new RoleGridAdapter();
        gridView.setAdapter(roleGridAdapter);
        tvApply.setOnClickListener(this);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s.toString())) {
                    isArticleOk = true;
                } else {
                    isArticleOk = false;
                }
            }
        });
        etContent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()& MotionEvent.ACTION_MASK){
                    case MotionEvent.ACTION_DOWN:
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                    case MotionEvent.ACTION_UP:
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });
        gridView.setFocusable(false);
//        listView.setFocusable(false);
        tvBackground.setText(getResources().getString(R.string.story_no_background));
        llbackground.setVisibility(View.GONE);
        KeyBoardObserver keyBoardObserver = new KeyBoardObserver();
        keyBoardObserver.setBaseRootAndView(baseRoot,rlBottom);
    }


    private void initSelectList() {
        listRoleSelect.clear();
        for (int i = 0; i < listRoletypes.size(); i++) {
            listRoleSelect.add(false);
        }
    }

    private void setListData(){
        llcontainer.removeAllViews();
        for(int i = 0 ; i<listRoleNatures.size();i++){
            final View view = LayoutInflater.from(ApplyRoleActivity.this).inflate(R.layout.item_lv_role_apply,null);
            view.setTag(true);
            TextView tvRolesetName = (TextView) view.findViewById(R.id.tv_role_set_role_apply);
            tvRolesetName.setText(listRoleNatures.get(i).getName());
            final EditText etRoleset = (EditText) view.findViewById(R.id.et_role_set_role_apply);
            etRoleset.setMovementMethod(ScrollingMovementMethod.getInstance());
            etRoleset.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    return false;
                }
            });
            etRoleset.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    etRoleset.setTextColor(getResources().getColor(R.color.black));
                    if(!TextUtils.isEmpty(s)&&!TextUtils.isEmpty(s.toString())){
                        if(CreateStoryManager.getInstance().getCharacterCount(s.toString()) > 100){
                            etRoleset.setTextColor(getResources().getColor(R.color.red_text));
                            view.setTag(false);
                        }else{
                            view.setTag(true);
                        }
                    }
                }
            });
            llcontainer.addView(view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_apply_role_apply:
                checkListInflate();
                checkGridSelect();
                if(isArticleOk&&isRoleselectOk&&isListDataOk){
                    if(CreateStoryManager.getInstance().getCharacterCount(etContent.getText().toString().trim()) > articleWordsLimit){
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_aduit_article_limit_1000),Toast.LENGTH_LONG).show();
                        return;
                    }
                    if(!checkItemsData()){
                        Toast.makeText(getApplicationContext(),"属性填写不能超过100字哦亲,请检查后提交",Toast.LENGTH_LONG).show();
                        return;
                    }
                    tvApply.setClickable(false);
                    submitApplyRoleData();
                } else {
                    if(listRoletypes.isEmpty()) {
                        T.show(getApplicationContext(), "没有人设，请联系群主设置人设", 1);
                    } else {
                        if(checkRemainRloes()){
                            T.show(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), 1);
                        }else {
                            T.show(getApplicationContext(),getResources().getString(R.string.role_apply_no_more_roles),1);
                        }
                    }
                }
                break;

        }
    }

    private class RoleGridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listRoletypes.size();
        }

        @Override
        public Object getItem(int position) {
            return listRoletypes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(ApplyRoleActivity.this).inflate(R.layout.item_gv_role_apply, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tvRole.setBackgroundResource(R.drawable.selector_story_type_select);
            viewHolder.tvRole.setEnabled(true);
            viewHolder.tvRole.setText(listRoletypes.get(position).getType());
            if (listRoletypes.get(position).getCount().equals(listRoletypes.get(position).getApplied())) {
                viewHolder.tvRole.setEnabled(false);
                viewHolder.tvRole.setSelected(true);
                viewHolder.tvRole.setBackgroundResource(R.drawable.shape_grey_rect_solid);
            }

            viewHolder.tvRole.setTag(position);
            viewHolder.tvRole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (int) v.getTag();
                    if (v.isSelected()) {
                        v.setSelected(false);
                        listRoleSelect.set(_position, false);
                        checkGridSelect();
                    } else {
                        llbackground.setVisibility(View.VISIBLE);
                        clearGridSelectStatus();
                        initSelectList();
                        v.setSelected(true);
                        listRoleSelect.set(_position, true);
                        if (!TextUtils.isEmpty(listRoletypes.get(_position).getBackground())) {
                            tvBackground.setText(listRoletypes.get(_position).getBackground());
                        } else {
                            tvBackground.setText(ApplyRoleActivity.this.getResources().getString(R.string.story_no_background));
                        }
                    }
                }
            });


            return convertView;
        }

        class ViewHolder {
            private TextView tvRole;

            ViewHolder(View view) {
                tvRole = (TextView) view.findViewById(R.id.tv_role_role_apply);
            }
        }

    }


    private void clearGridSelectStatus() {
        for (int i = 0; i < gridView.getChildCount(); i++) {
            TextView tvRole = (TextView) gridView.getChildAt(i).findViewById(R.id.tv_role_role_apply);
            if (tvRole.isEnabled())
                tvRole.setSelected(false);
        }
    }

    private boolean checkListInflate() {
        for (int i = 0; i < llcontainer.getChildCount(); i++) {
            EditText editText = (EditText) llcontainer.getChildAt(i).findViewById(R.id.et_role_set_role_apply);
            if (TextUtils.isEmpty(editText.getText().toString().trim())) {
                isListDataOk = false;
                return false;
            }
        }
        isListDataOk = true;
        return true;
    }



    private boolean checkGridSelect() {
        for (int i = 0; i < listRoleSelect.size(); i++) {
            if (listRoleSelect.get(i)) {
                isRoleselectOk = true;
                return true;
            }
        }
        llbackground.setVisibility(View.GONE);
        tvBackground.setText(getResources().getString(R.string.story_no_background));
        isRoleselectOk = false;
        return false;
    }

    private int getGridSelect() {
        for (int i = 0; i < listRoleSelect.size(); i++) {
            if (listRoleSelect.get(i)) {
                return i;
            }
        }
        return -1;
    }


    private List<ApplyItems> getListItems() {
        List<ApplyItems> listApplys = new ArrayList<>();
        for (int i = 0; i < llcontainer.getChildCount(); i++) {
            ApplyItems applyItems = new ApplyItems();
            applyItems.setId(listRoleNatures.get(i).getId());
            applyItems.setValue(((EditText) llcontainer.getChildAt(i).findViewById(R.id.et_role_set_role_apply)).getText().toString().trim());
            listApplys.add(applyItems);
        }
        return listApplys;
    }

    /***
     *  检测每个子项的字数 不得超过100字
     * @return
     */
    private boolean checkItemsData(){
        for (int i = 0;i < llcontainer.getChildCount();i++){
            if(llcontainer.getChildAt(i).getTag()!=null&&llcontainer.getChildAt(i).getTag() ==false){
                return  false;
            }
        }
        return true;
    }

    private void requestRoleSettings() {
        if (TextUtils.isEmpty(storyid)) {
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleSettings, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), getResources().getString(R.string.story_net_status_error), 0);
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }


    private void onReceiveData(String response) {
        try {
            storyRoleSettingJsonBean = new Gson().fromJson(response, StoryRoleSettingJsonBean.class);
            if (storyRoleSettingJsonBean == null) {
                T.show(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), 0);
                return;
            }

            if (storyRoleSettingJsonBean.getRolesettings().getRoletypes() != null && storyRoleSettingJsonBean.getRolesettings().getRolenatures() != null) {
                listRoletypes = storyRoleSettingJsonBean.getRolesettings().getRoletypes();
                if(listRoletypes.isEmpty()) {
                    T.show(getApplicationContext(), "没有人设，请联系群主设置人设", 1);
                }
                listRoleNatures = storyRoleSettingJsonBean.getRolesettings().getRolenatures();
                initSelectList();
                roleGridAdapter.notifyDataSetChanged();
                setListData();
            }
        } catch (Exception e) {
            T.show(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), 0);
        }
    }


    private void submitApplyRoleData() {
        if (TextUtils.isEmpty(storyid)) {
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyid);
        params.addParams("roletype", listRoletypes.get(getGridSelect()).getType());
        params.addParams("rolenatures", new Gson().toJson(getListItems()));
        params.addParams("applytext", etContent.getText().toString().trim());
        OkHttpClientManager.postAsy(ConstantValues.ApplyStoryRole, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_net_status_error), Toast.LENGTH_LONG).show();
                        tvApply.setClickable(true);
                    }

                    @Override
                    public void onResponse(String response) {
                        onRecevieApplyData(response);
                    }
                });

    }

    private void onRecevieApplyData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString("result").equals("1")) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.operation_success), Toast.LENGTH_LONG).show();
                finish();
            } else {
                Toast.makeText(getApplicationContext(), jsonObject.getString("describe"), Toast.LENGTH_LONG).show();
                tvApply.setClickable(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tvApply.setClickable(true);
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showAlertDialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.story_quit_edit_and_exit));
        builder.setTitle(getResources().getString(R.string.square_prompt));
        builder.setPositiveButton(getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton(getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }


    /**
     * 判断是否有可选的角色
     * @return
     */
    private boolean checkRemainRloes(){
        if(listRoletypes!=null&&listRoletypes.size()>0){
            for(int i = 0 ; i <listRoletypes.size();i++){
                int applyCount  = Integer.valueOf(listRoletypes.get(i).getApplied());
                int totalCount = Integer.valueOf(listRoletypes.get(i).getCount());
                if(totalCount>applyCount){
                    return true;
                }
            }
            return false;

        }
        return false;
    }


    private class ApplyItems {
        private String id;
        private String value;

        public void setId(String id) {
            this.id = id;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "{" + "\"id\":" + "\"" + id + "\"," + "\"value\":" + "\"" + value + "\"" + "}";
        }
    }
}
