package com.yuxip.ui.activity.add.story;

import android.graphics.Bitmap;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.RoleTypeSettingEntity;
import com.yuxip.JsonBean.StoryClass;
import com.yuxip.JsonBean.StoryRoleNature;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ly on 2015/10/29.
 * 创建剧流程的管理
 */
public class CreateStoryManager {
    private static CreateStoryManager instance;
    private List<StoryClass.ListEntity> listStoryTypesOrigin = new ArrayList<>();
    private List<StoryRoleNature.ListEntity> listTypes = new ArrayList<>();
    private UpImgUtil upImgUtil;
    private String picPath;             //图片选取的地址
    private String imgSaveUrl;          //保存图片的url地址    发布的时候获取并上传
    private String storyName;           //剧的名称
    private String storyStringType;     //剧的类别  同人,古装 ...
    private String sceneContent;        //主线剧情的内容
    private String abstractContent;     //简介内容
    private String storyType;           //创建剧的类别  的id 集合  如原创 同人...  [1,2,3]
    private String roletypes;           //剧的角色类型 [{"type":"老师","count":3, "background":"blah blah blah"}]
    private String rolenatures;         //剧的每个角色的属性模板 [1, 2, 3, 4]
    private String newrolenatures;      //用户添加的角色属性 ["体重","身高"]
    private String auditRules;          //审核规则
    private String ruleSetting;         //规则设定
    private String groupId;
    private String regex = "[0-9a-zA-Z\u4e00-\u9fa5]";
    private Pattern pattern = Pattern.compile(regex);
    private CreateStoryListener createStoryListener;
    private UploadImageListener uploadImageListener;

    private List<Integer> listStoryTypes = new ArrayList<>();
    private List<RoleTypeSettingEntity> listRoleTypes = new ArrayList<>();
    private List<Integer> listRoleNatures = new ArrayList<>();
    private List<String> listNewRoleNatures = new ArrayList<>();    //这是新建的并且选中的
    private List<String> listNewRoleNaturesCreate = new ArrayList<>();  //这是新建的所有的

    public static CreateStoryManager getInstance() {
        if (instance == null) {
            instance = new CreateStoryManager();
        }
        return instance;
    }


    public void setUploadImageListener(UploadImageListener uploadImageListener){
        this.uploadImageListener = uploadImageListener;
    }

    public void setCreateStoryListener(CreateStoryListener createStoryListener) {
        this.createStoryListener = createStoryListener;
    }

    public void saveLoadImage(Bitmap bitmap) {
        upImgUtil = new UpImgUtil();
        if (bitmap != null) {
            /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
            Date date = new Date();
            /** 获取当前时间并且进一步转化为字符串 */
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
            String imgPathUri = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";
            upImgUtil.setFilePath(imgPathUri, imgPathUri);
            upImgUtil.saveHeadImg(bitmap);
        }
    }

    public void uploadImage() {
        upImgUtil.upLoadPicture(new HeadImgListener() {
            public void notifyImgUploadFinished(String url) {
                if (url == null) {
//                    if (createStoryListener != null)
//                        createStoryListener.uploadImgFailed();
                    if(uploadImageListener != null)
                        uploadImageListener.uploadImgFailed();
                } else {
                    imgSaveUrl = url;
//                    if (createStoryListener != null)
//                        createStoryListener.uploadImgSuccess();
                    if(uploadImageListener!= null)
                        uploadImageListener.uploadImgSuccess();
                }
            }
        });
    }

    public int getCharacterCount(String content) {
        int wordCount = 0;
        Matcher matcher = pattern.matcher(content);
        while (matcher.find()) {
            wordCount += 1;
        }
        return wordCount;
    }

    public String getIntString(List<Integer> list){
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for(int i = 0;i<list.size() ;i++){
            builder.append(list.get(i));
            if(i<list.size()-1){
                builder.append(",");
            }
        }
        builder.append("]");
        return  builder.toString();
    }

    public String getListString(List<String> list) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < list.size(); i++) {
            builder.append("\"");
            builder.append(list.get(i));
            builder.append("\"");
            if (i < list.size() - 1)
                builder.append(",");
        }
        builder.append("]");
        return builder.toString();
    }


    public void setAbstractContent(String abstractContent) {
        this.abstractContent = abstractContent;
    }

    public String getPicPath() {
        return picPath;
    }

    public void setPicPath(String picPath) {
        this.picPath = picPath;
    }

    public void setSceneContent(String sceneContent) {
        this.sceneContent = sceneContent;
    }

    public void setRuleSetting(String ruleSetting) {
        this.ruleSetting = ruleSetting;
    }

    public void setAuditRules(String auditRules) {
        this.auditRules = auditRules;
    }

    public void setStoryType(String storyType) {
        this.storyType = storyType;
    }

    public void setRoletypes(String roletypes) {
        this.roletypes = roletypes;
    }

    public void setRolenatures(String rolenatures) {
        this.rolenatures = rolenatures;
    }

    public void setNewrolenatures(String newrolenatures) {
        this.newrolenatures = newrolenatures;
    }

    public void setListNewRoleNaturesCreate(List<String> listNewRoleNaturesCreate) {
        this.listNewRoleNaturesCreate = listNewRoleNaturesCreate;
    }

    public List<String> getListNewRoleNaturesCreate() {
        return listNewRoleNaturesCreate;
    }

    public void setStoryName(String storyName) {
        this.storyName = storyName;
    }

    public String getStoryName() {
        return storyName;
    }

    public void setListStoryTypes(List<Integer> listStoryTypes) {
        if (listStoryTypes != null)
            this.listStoryTypes = listStoryTypes;
    }

    public List<Integer> getListStoryTypes() {
        return listStoryTypes;
    }

    public void setListRoleTypes(List<RoleTypeSettingEntity> listRoleTypes) {
        if (listRoleTypes != null)
            this.listRoleTypes = listRoleTypes;
    }

    public List<RoleTypeSettingEntity> getListRoleTypes() {
        return listRoleTypes;
    }

    public void setListRoleNatures(List<Integer> listRoleNatures) {
        if (listRoleNatures != null)
            this.listRoleNatures = listRoleNatures;
    }


    public List<Integer> getListRoleNatures() {
        return listRoleNatures;
    }

    public void setListNewRoleNatures(List<String> listNewRoleNatures) {
        if (listNewRoleNatures != null)
            this.listNewRoleNatures = listNewRoleNatures;
    }

    public List<String> getListNewRoleNatures() {
        return listNewRoleNatures;
    }

    public String getAbstractContent() {
        return abstractContent;
    }

    public String getSceneContent() {
        return sceneContent;
    }

    public String getAuditRules() {
        return auditRules;
    }

    public String getRuleSetting() {
        return ruleSetting;
    }

    public List<StoryClass.ListEntity> getListStoryTypesOrigin() {
        return listStoryTypesOrigin;
    }

    public void setListStoryTypesOrigin(List<StoryClass.ListEntity> listStoryTypesOrigin) {
        this.listStoryTypesOrigin = listStoryTypesOrigin;
    }

    public String getImgSaveUrl() {
        return this.imgSaveUrl;

    }

    public String getStoryStringType() {
        return storyStringType;
    }

    public void setStoryStringType(String storyStringType) {
        this.storyStringType = storyStringType;
    }

    public List<StoryRoleNature.ListEntity> getListTypes() {
        return listTypes;
    }


    public boolean checkAllData() {
        this.storyType = getIntString(listStoryTypes);
//        this.roletypes = listRoleTypes.toString();
        this.roletypes = new Gson().toJson(listRoleTypes);
        this.rolenatures = getIntString(listRoleNatures);
        this.newrolenatures = getListString(listNewRoleNatures);
        if (TextUtils.isEmpty(storyName) || TextUtils.isEmpty(imgSaveUrl) || TextUtils.isEmpty(storyType) || abstractContent == null ||
                null == sceneContent || TextUtils.isEmpty(roletypes) || TextUtils.isEmpty(rolenatures) || TextUtils.isEmpty(newrolenatures) ||
                null == auditRules || null == ruleSetting) {
            return false;
        }
        return true;
    }

    public void clearAllData() {
        this.uploadImageListener = null;
        this.createStoryListener = null;
        this.groupId = null;
        this.imgSaveUrl = null;
        this.storyName = null;
        this.sceneContent = null;
        this.abstractContent = null;
        this.storyType = null;
        this.roletypes = null;
        this.rolenatures = null;
        this.newrolenatures = null;
        this.auditRules = null;
        this.ruleSetting = null;
        this.picPath = null;
        this.listRoleTypes.clear();
        this.listStoryTypes.clear();
        this.listRoleNatures.clear();
        this.listNewRoleNatures.clear();
        this.listNewRoleNaturesCreate.clear();
    }

    public void requestRoleNatureList() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleNatures, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            StoryRoleNature storyRoleNature = new Gson().fromJson(response, StoryRoleNature.class);
                            if (storyRoleNature.getList() != null && storyRoleNature.getList().size() > 0) {
                                listTypes = storyRoleNature.getList();
                            }
                        } catch (Exception e) {

                        }
                    }
                });
    }

    /**
     * @param reviewGroupId     审核群id
     */
    public void submitCreateStory(String reviewGroupId) {
        groupId = reviewGroupId;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("groupid", groupId);
        params.addParams("title", storyName);
        params.addParams("storyimg", imgSaveUrl);
        params.addParams("categories", storyType);
        params.addParams("intro", abstractContent);
        params.addParams("mainplot", sceneContent);
        params.addParams("roletypes", roletypes);
        params.addParams("rolenatures", rolenatures);
        params.addParams("newrolenatures", newrolenatures);
        params.addParams("rolerule", auditRules);
        params.addParams("storyrule", ruleSetting);
        OkHttpClientManager.postAsy(ConstantValues.CreateStoryStepByStep, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        if (createStoryListener != null)
                            createStoryListener.createStoryFail("网络异常");
                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("1")) {
                                if (createStoryListener != null)
                                    createStoryListener.createStorySuccess(jsonObject.getString("describe"));
                            } else {
                                if (createStoryListener != null)
                                    createStoryListener.createStoryFail(jsonObject.getString("describe"));
                            }

                        } catch (Exception e) {
                            if (createStoryListener != null)
                                createStoryListener.createStoryFail("server error");
                        }
                    }
                });
    }


    public interface UploadImageListener{
        void uploadImgSuccess();

        void uploadImgFailed();
    }

    public interface CreateStoryListener {


        void createStorySuccess(String describe);

        void createStoryFail(String reason);

    }

}
