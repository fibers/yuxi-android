package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * add by SummerRC on 2015/5/25.
 */
public class MainStoryAddActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(MainStoryAddActivity.class);

    private EditText et_main_story_name;
    private EditText et_main_story_content;
    private String storyid;
    private String content = "";
    private String title = "";
    private String sceneId;
    private boolean isModify;
    private boolean IS_ADMIN;
    private String request_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }

        storyid = getIntent().getStringExtra("storyid");
        sceneId = getIntent().getStringExtra("sceneid");
        content = getIntent().getStringExtra("content") == null ? "" : getIntent().getStringExtra("content");
        title = getIntent().getStringExtra("name") == null ? "" : getIntent().getStringExtra("name");
        isModify = getIntent().getBooleanExtra("modify", false);
        IS_ADMIN = getIntent().getBooleanExtra("IS_ADMIN", false);
        LayoutInflater.from(this).inflate(R.layout.activity_story_main_add, topContentView);
//        et_main_story_name = (EditText) findViewById(R.id.et_main_story_name);
        et_main_story_content = (EditText) findViewById(R.id.et_main_story_content);
        et_main_story_content.setText(content);
//        et_main_story_name.setText(title);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("主线剧情");
        if (IS_ADMIN) {
            if (!isModify) {
                request_url = ConstantValues.CreateStoryScene;
                setRighTitleText("添加");
            } else {
                request_url = ConstantValues.ModifyStoryScene;
                setRighTitleText("修改");
            }
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /** 添加主线剧情 */
                    if (TextUtils.isEmpty(et_main_story_content.getText().toString())) {
                        Toast.makeText(getApplicationContext(), "内容不能为空", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        content = et_main_story_content.getText().toString().trim();
                    }
//                    if (TextUtils.isEmpty(et_main_story_name.getText().toString())) {
//                        Toast.makeText(getApplicationContext(), "标题不能为空", Toast.LENGTH_SHORT).show();
//                        return;
//                    }
//                    title = et_main_story_name.getText().toString().trim();
                    addMainStory();
                }
            });
        } else {
//            et_main_story_name.setEnabled(false);
            et_main_story_content.setEnabled(false);
        }

    }

    /**
     * 添加主线剧情
     */
    private void addMainStory() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");  //管理员id，能进入这里的id都是管理员
        params.addParams("storyid", storyid);
        params.addParams("content", content);
        params.addParams("title", title);
        params.addParams("token", "1");
        if (isModify) {
            params.addParams("sceneid", sceneId);
        }
        OkHttpClientManager.postAsy(request_url, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        if (!isModify) {
                            Toast.makeText(getApplicationContext(), "添加成功", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    } else {
                        if (!isModify) {
                            Toast.makeText(getApplicationContext(), "添加失败", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                        }
                        finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }
}
