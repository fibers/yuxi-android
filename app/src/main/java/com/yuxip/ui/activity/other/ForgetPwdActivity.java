package com.yuxip.ui.activity.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * 忘记密码界面
 * Created by Administrator on 2015/4/27.
 */

public class ForgetPwdActivity extends TTBaseActivity {
    private EditText findMobile;
    private EditText findCode;
    private String number;
    private boolean flag; //是否为手机号
    private BroadcastReceiver smsReceiver;
    private IntentFilter filter2;
    private MyHandler handler = new MyHandler(this);
    private EditText et;
    private String strContent;
    private String patternCoder = "(?<!\\d)\\d{4}(?!\\d)";
    private TextView fgTestGetCodeBtn;
    private TimeCount time;
    private String vcode;
    private Logger logger;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        time = new TimeCount(60000, 1000);//构造CountDownTimer对象
        /** 绑定布局资源(注意放所有资源初始化之前) */
        LayoutInflater.from(this).inflate(R.layout.mb_activity_forgetpwd, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("忘记密码");
        setRighTitleText("下一步");
        initViewID();
        //下一步
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vcode= findCode.getText().toString().trim();
                if (TextUtils.isEmpty(vcode)){
                    Toast.makeText(getApplicationContext(),"请输入验证码",Toast.LENGTH_SHORT).show();
                }else{
                    VCodeCheck();
                }

            }

        });

        logger = Logger.getLogger(ForgetPwdActivity.class);
    }

    /***
     * 校验验证码
     */
    private void VCodeCheck(){
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", "0");
        params.addParams("mobileno", number);
        params.addParams("vcode", vcode);
        OkHttpClientManager.postAsy(ConstantValues.VCODECHECK, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "验证成功", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ForgetPwdActivity.this, SettingPwdActivity.class);
                        intent.putExtra("mobileno", number);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "验证码错误", Toast.LENGTH_SHORT).show();
                        findCode.setText("");
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    private void initViewID() {
     initEnterCode();
     findMobile = (EditText) findViewById(R.id.forget_find_mobile);
        findCode =  (EditText) findViewById(R.id.forget_find_code);
        fgTestGetCodeBtn = (TextView) findViewById(R.id.testGetCodeBtn);
        fgTestGetCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                number = findMobile.getText().toString();
                // 判断是否是手机
                if (number.length() != 11) {
                    Toast.makeText(getApplicationContext(), "您输入的不是手机号", Toast.LENGTH_SHORT).show();
                } else {
                    time.start();//开始计时
                    getForgetPwdCode();
                }
            }
        });
    }


    /**
     * 自动输入短信验证码
     */
    private void initEnterCode() {
        filter2 = new IntentFilter();
        filter2.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter2.setPriority(Integer.MAX_VALUE);
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Object[] objs = (Object[]) intent.getExtras().get("pdus");
                for (Object obj : objs) {
                    byte[] pdu = (byte[]) obj;
                    SmsMessage sms = SmsMessage.createFromPdu(pdu);
                    // 短信的内容
                    String message = sms.getMessageBody();
                    Log.d("logo", "message     " + message);
                    // 短息的手机号。。+86开头？
                    String from = sms.getOriginatingAddress();
                    Log.d("logo", "from     " + from);
                    // Time time = new Time();
                    // time.set(sms.getTimestampMillis());
                    // String time2 = time.format3339(true);
                    // Log.d("logo", from + "   " + message + "  " + time2);
                    // strContent = from + "   " + message;
                    // handler.sendEmptyMessage(1);
                    if (!TextUtils.isEmpty(from)) {
                        String code = patternCode(message);
                        if (!TextUtils.isEmpty(code)) {
                            strContent = code;
                            handler.sendEmptyMessage(1);
                        }
                    }
                }
            }
        };
        registerReceiver(smsReceiver, filter2);
    }

    //获取验证码
    private void getForgetPwdCode() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", "0");
        params.addParams("mobileno", number);

        OkHttpClientManager.postAsy(ConstantValues.TESTGETCODE, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "发送验证码成功", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 0);
            }
        });
    }

    /**
     * 匹配短信中间的4个数字（验证码等）
     *
     * @param patternContent
     * @return
     */
    private String patternCode(String patternContent) {
        if (TextUtils.isEmpty(patternContent)) {
            return null;
        }
        Pattern p = Pattern.compile(patternCoder);
        Matcher matcher = p.matcher(patternContent);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //关闭广播
        this.unregisterReceiver(smsReceiver);
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);//参数依次为总时长,和计时的时间间隔
        }
        @Override
        public void onFinish() {//计时完毕时触发
            fgTestGetCodeBtn.setText("获取验证码");
            fgTestGetCodeBtn.setClickable(true);
        }
        @Override
        public void onTick(long millisUntilFinished){//计时过程显示
            fgTestGetCodeBtn.setClickable(false);
            fgTestGetCodeBtn.setText(millisUntilFinished /1000+"秒");
        }
    }

    static class MyHandler extends Handler {
        WeakReference<ForgetPwdActivity> mActivity;         //持有StoryChatActivity对象的弱引用
        MyHandler(ForgetPwdActivity activity) {
            mActivity = new WeakReference<>(activity);
        }
        @Override
        public void handleMessage(Message msg) {
            ForgetPwdActivity activity = mActivity.get();
            if(activity == null) {
                return;
            }
            activity.findCode.setText(activity.strContent);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
