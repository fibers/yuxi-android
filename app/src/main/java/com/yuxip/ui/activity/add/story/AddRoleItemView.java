package com.yuxip.ui.activity.add.story;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.JsonBean.RoleTypeSettingEntity;
import com.yuxip.R;

/**
 * Created by Ly on 2015/11/2.
 * description:
 */
public class AddRoleItemView extends RelativeLayout implements View.OnClickListener {
    private Context context;
    private TextView tvTitle;
    private EditText etRoleName;
    private EditText etRoleCount;
    private EditText etBackground;
    private ImageView ivDiv;
    private ImageView ivAdd;
    private View viewLine;
    private int positionInParent;   //在父控件中的位置
    private final int roleNameLimit = 8;
    private final int roleNumLimit = 4;
    private final int backgroundWordsLimit = 500;
    private AddRoleCallBack addRoleListener;

    public AddRoleItemView(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public AddRoleItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();
    }

    public AddRoleItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }

    public void setAddRoleListener(AddRoleCallBack addRoleListener) {
        if (addRoleListener != null)
            this.addRoleListener = addRoleListener;
    }


    private void initRes() {
        LayoutInflater.from(context).inflate(R.layout.customview_item_add_role_type, this, true);
        tvTitle = (TextView) findViewById(R.id.tv_title_item_role_type);
        etRoleName = (EditText) findViewById(R.id.et_rolename_add_role);
        etRoleCount = (EditText) findViewById(R.id.et_rolenum_add_role);
        etBackground = (EditText) findViewById(R.id.et_rolebackground_add_role);
        ivDiv = (ImageView) findViewById(R.id.iv_div_add_role);
        ivAdd = (ImageView) findViewById(R.id.iv_add_add_role);
        viewLine = findViewById(R.id.view_line_add_role);
        etRoleCount.setText("1");
        ivAdd.setOnClickListener(this);
        ivDiv.setOnClickListener(this);
        etRoleName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) || !TextUtils.isEmpty(s.toString().trim())) {
                    if (s.toString().length() > roleNameLimit) {
                        etRoleName.setText(s.toString().substring(0, roleNameLimit));
                        etRoleName.setSelection(roleNameLimit);
                    }
                }
                if(addRoleListener!=null)
                    addRoleListener.checkAllData();
            }
        });
        etRoleCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) || !TextUtils.isEmpty(s.toString().trim())) {
                    if (s.toString().length() > roleNumLimit) {
                        etRoleCount.setText(s.toString().substring(0, roleNumLimit));
                        etRoleCount.setSelection(roleNumLimit);
                    }
                }
                if(addRoleListener!=null)
                    addRoleListener.checkAllData();
            }
        });
        etBackground.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s) && !TextUtils.isEmpty(s.toString().trim())) {
                    if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) > backgroundWordsLimit) {
                        etBackground.setTextColor(getResources().getColor(R.color.red_text));
                    } else {
                        etBackground.setTextColor(getResources().getColor(R.color.black));
                    }
                }
            }
        });
    }

    /**
     * 设置title的可视情况
     */
    public void setTitleVisable() {
        viewLine.setVisibility(View.GONE);
        tvTitle.setVisibility(View.VISIBLE);
    }

    public void setPositionInParent(int positionInParent) {
        this.positionInParent = positionInParent;
    }

    public void setRoleName(String name) {
        etRoleName.setText(name);
    }

    public void setRolenum(String num) {
        etRoleCount.setText(num);
    }

    public void setRoleHint(String name) {
        etRoleName.setHint(name);
    }

    public void setRoleBackground(String background) {
        etBackground.setText(background);
    }

    public String getRoleName() {
        return etRoleName.getText().toString();
    }

    public String getRoleNum() {
        return etRoleCount.getText().toString();
    }

    public String getRoleBackground() {
        return etBackground.getText().toString();
    }

    public RoleTypeSettingEntity getRoleEntity() {
        RoleTypeSettingEntity roleTypeSettingEntity = new RoleTypeSettingEntity();
        roleTypeSettingEntity.setType(getRoleName());
        roleTypeSettingEntity.setCount(getRoleNum());
        roleTypeSettingEntity.setBackground(getRoleBackground());
        return roleTypeSettingEntity;
    }

    private void addRoleNum() {
        if (TextUtils.isEmpty(etRoleCount.getText().toString())) {
            return;
        }
        etRoleCount.setText(Integer.valueOf(etRoleCount.getText().toString()) + 1 + "");
    }

    private void divRoleNum() {
        if (TextUtils.isEmpty(etRoleCount.getText().toString()) || Integer.valueOf(etRoleCount.getText().toString()) - 1 == 0) {
            if (addRoleListener != null)
                addRoleListener.deleteItem(positionInParent);
            return;
        }
        etRoleCount.setText(Integer.valueOf(etRoleCount.getText().toString()) - 1 + "");

    }

    public boolean isItemOk() {
        if (TextUtils.isEmpty(etRoleName.getText().toString().trim()) || TextUtils.isEmpty(etRoleCount.getText().toString().trim())) {
//            Toast.makeText(context, context.getResources().getString(R.string.story_has_data_not_inflate), Toast.LENGTH_LONG).show();
//            YXAnimationUtils.skakeAnimation(context, etRoleName);
            return false;
        } else {
            if (CreateStoryManager.getInstance().getCharacterCount(etBackground.getText().toString().trim()) > backgroundWordsLimit) {
                Toast.makeText(context, context.getResources().getString(R.string.story_role_background_limit_500), Toast.LENGTH_LONG).show();
                return false;
            } else if (CreateStoryManager.getInstance().getCharacterCount(etBackground.getText().toString()) == 0) {
//                Toast.makeText(context, context.getResources().getString(R.string.story_has_data_not_inflate), Toast.LENGTH_LONG).show();
//                return  false;
            }
            return true;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_div_add_role:
                divRoleNum();
                break;
            case R.id.iv_add_add_role:
                addRoleNum();
                break;
        }
    }

    /**
     * 删除本项的回调
     * 检测所有必填是否填充的回调
     */
    public interface AddRoleCallBack {
        void deleteItem(int position);
        void checkAllData();
    }

}
