package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.ui.activity.base.TTBaseNewFragmentActivity;
import com.yuxip.ui.fragment.other.MessageImageFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/14.
 */
public class PreviewReadImgActivity extends TTBaseNewFragmentActivity {
    private View view;
    @InjectView(R.id.viewpager_preview_read)
     ViewPager viewPager;
    @InjectView(R.id.viewGroup_preview_read)
     LinearLayout linearLayout;
    @InjectView(R.id.back_btn_preview_read)
    ImageView back;
    private int currentPosition;
    private ArrayList<ImageMessage> list=new ArrayList<>();

    private List<MessageImageFragment> listFra=new ArrayList<>();
    private MessageImageFragment fragment;
    private ImageMessage messageInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.activity_previewimg_read);
        ButterKnife.inject(this);
        list = (ArrayList<ImageMessage>) getIntent().getSerializableExtra(IntentConstant.CUR_MESSAGE);
        currentPosition=Integer.valueOf(getIntent().getStringExtra(IntentConstant.POSITION));

        initListFra();
        viewPager.setPageMargin(20);
        viewPager.setAdapter(new MyFragmentAdyapter(getSupportFragmentManager()));
        viewPager.setCurrentItem(currentPosition);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.tt_stay, R.anim.zoom_out);
            }
        });
    }

    private void initListFra(){
        for(int i=0;i<list.size();i++){
            MessageImageFragment fragment=new MessageImageFragment();
            fragment.setImageInfo(list.get(i));
            listFra.add(fragment);
        }
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.tt_stay,R.anim.zoom_out);
    }

    private class MyFragmentAdyapter extends FragmentStatePagerAdapter{

        public MyFragmentAdyapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return listFra.get(position);
        }

        @Override
        public int getCount() {
            return listFra.size();
        }
    }
}
