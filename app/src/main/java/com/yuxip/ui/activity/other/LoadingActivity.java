package com.yuxip.ui.activity.other;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.umeng.analytics.MobclickAgent;
import com.yuxip.R;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.widget.JazzyViewPager.JazzyViewPager;
import com.yuxip.ui.widget.JazzyViewPager.OutlineContainer;
import com.yuxip.utils.FirstPostUtils;
import com.yuxip.utils.SharedPreferenceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/7/18.
 * description : app安装之后第一次启动的引导界面
 */
public class LoadingActivity extends TTBaseNewActivity {
    private JazzyViewPager mJazzy;
    private ImageView loading_start;
    private LoadingConfig loadingConfig;
    private List<Integer> mImageUrls = new ArrayList<>();       //放图片的list
    private LinearLayout pointLayout;                          //点layout

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MobclickAgent.updateOnlineConfig(this);
        /** 友盟：禁止默认的页面统计方式 */
        MobclickAgent.openActivityDurationTrack(false);
        loadingConfig = new LoadingConfig();

        SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
        if (!loadingConfig.isFirstLoad()) {
            startActivity(new Intent(LoadingActivity.this, SplashActivity.class));
            finish();
            return;
        } else {
            FirstPostUtils.sendHttpRequest(this, "0", 0);
        }

        setContentView(R.layout.activity_loading);
        pointLayout = (LinearLayout) findViewById(R.id.ll_points_layout);
        loading_start = (ImageView) findViewById(R.id.loading_start);
        loading_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingConfig.setFirstFlg();
                startActivity(new Intent(LoadingActivity.this, LoginMobileActivity.class));
                finish();
            }
        });
        /** 设置翻页格式 */
        setupJazziness(JazzyViewPager.TransitionEffect.Standard);
        initImage();
    }

    private void initImage() {
        mImageUrls.add(R.drawable.loading1);
        mImageUrls.add(R.drawable.loading2);
        mImageUrls.add(R.drawable.loading3);
        mImageUrls.add(R.drawable.loading4);
    }


    /**
     * 初始化viewpager
     *
     * @param effect
     */
    private void setupJazziness(JazzyViewPager.TransitionEffect effect) {
        mJazzy = (JazzyViewPager) findViewById(R.id.jazzy_pager);
        mJazzy.setTransitionEffect(effect);
        MainAdapter mainAdapter = new MainAdapter();
        mJazzy.setAdapter(mainAdapter);
        mJazzy.setPageMargin(0);
        mJazzy.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 3) {
                    loading_start.setVisibility(View.VISIBLE);
                } else {
                    loading_start.setVisibility(View.INVISIBLE);
                }

                /** 变更选中小点 */
                for (int i = 0; i < pointLayout.getChildCount(); i++) {
                    ImageView imageView = (ImageView) pointLayout.getChildAt(i);
                    if (i == position) {
                        imageView.setImageResource(R.drawable.loading_point_select);
                    } else {
                        imageView.setImageResource(R.drawable.loading_point_normal);
                    }
                }
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });

    }

    /**
     * viewpage适配器
     */
    private class MainAdapter extends PagerAdapter {

        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View v = View.inflate(LoadingActivity.this, R.layout.loading, null);
            ImageView imageView = (ImageView) v.findViewById(R.id.figure_img_bg);
            imageView.setImageResource(mImageUrls.get(position));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            container.addView(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mJazzy.setObjectForPosition(v, position);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object obj) {
            container.removeView(mJazzy.findViewFromObject(position));
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            if (view instanceof OutlineContainer) {
                return ((OutlineContainer) view).getChildAt(0) == obj;
            } else {
                return view == obj;
            }
        }
    }

    /**
     * 判断是否是第一次启动
     */
    private class LoadingConfig {

        public boolean isFirstLoad() {
            boolean result = true;
            /** 同样，在读取SharedPreferences数据前要实例化出一个SharedPreferences对象 */
            SharedPreferences sharedPreferences = getSharedPreferences("load_config", Activity.MODE_PRIVATE);
            /** 使用getString方法获得value，注意第2个参数是value的默认值 */
            String firstLogin = sharedPreferences.getString("firstlogin", "");
            if (firstLogin.equals("1")) {
                result = false;
            }
            return result;
        }

        public void setFirstFlg() {
            SharedPreferences sharedPreferences = getSharedPreferences("load_config", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("firstlogin", "1");
            editor.apply();
        }
    }

}
