package com.yuxip.ui.activity.story;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * 简介
 * changed by SummerRC on 2015/4/25.
 */
public class StoryIntroActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryIntroActivity.class);

    private EditText et_content;
    private String storyid;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_story_introk, topContentView);
        et_content = (EditText) findViewById(R.id.et_rule);

        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);


        boolean IS_ADMIN = getIntent().getBooleanExtra("IS_ADMIN", false);
        storyid = getIntent().getStringExtra("storyid");
        String intro = getIntent().getStringExtra("intro");
        type    =   getIntent().getStringExtra("type");

        if(!TextUtils.isEmpty(type)&&type.equals("title")) {
            setTitle("剧名");
            if(TextUtils.isEmpty(et_content.getText().toString().trim())){
                et_content.setHint("请输入剧名(30字以内)");
            }
        }
        else {
            setTitle("简介");
            if(TextUtils.isEmpty(et_content.getText().toString().trim())){
                et_content.setHint("必填.请输入剧简介,一句话概括剧情内容");
            }
        }

        et_content.setText(intro);
        if(IS_ADMIN) {
            et_content.setFocusable(true);
            et_content.requestFocus();
            setRighTitleText("完成");
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String  storyIntroContent  = et_content.getText().toString().trim();
                    if (TextUtils.isEmpty(storyIntroContent)){

                        Toast.makeText(getApplicationContext(),"请完善信息",Toast.LENGTH_SHORT).show();
                    }else{
                        /** 修改简介 */
                        changeInfo();
                    }
                }
            });
        } else {
            et_content.setFocusable(false);
            et_content.setClickable(false);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    /***
     * 修改简介
     */
    private void changeInfo() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("content", et_content.getText().toString().trim());
        params.addParams("storyid", storyid);
        if(type.equals("title")) {
            params.addParams("type", "4");           //剧名
        }
        else {
            params.addParams("type", "1");           //简介
        }
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        /**  通知更新主界面 劇列表*/
                        Intent intent=new Intent();
                        intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
                        sendBroadcast(intent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }
}
