package com.yuxip.ui.activity.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.ImageUtil;

/**
 * 咒语介绍界面
 * Edited by SummerRC on 2015/11/19.
 */
public class TheSpellActivity extends TTBaseActivity {
    public static final int FROM_FAMILY = 0;
    public static final int FROM_STORY = 1;
    public static final String TYPE = "TYPE";


    public static void startTheSpallActivity(Context context, int type) {
        Intent intent = new Intent(context, TheSpellActivity.class);
        intent.putExtra(TYPE, type);
        context.startActivity(intent);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = getLayoutInflater().inflate(R.layout.activity_the_spell, topContentView);
        ImageView image = (ImageView) view.findViewById(R.id.image);
        int type = getIntent().getIntExtra(TYPE, FROM_STORY);
        switch (type) {
            case FROM_FAMILY:
                image.setImageBitmap(ImageUtil.readBitMap(this, R.drawable.the_spell_family));
                break;
            case FROM_STORY:
                image.setImageBitmap(ImageUtil.readBitMap(this, R.drawable.the_spell_story));
                break;
        }
        setContentView(view);
        setLeftButton(R.drawable.back_default_btn);
    }
}
