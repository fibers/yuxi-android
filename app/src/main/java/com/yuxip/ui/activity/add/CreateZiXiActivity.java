package com.yuxip.ui.activity.add;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.GlobalVariable;
import com.yuxip.imservice.event.CreateStoryTopicEvent;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by yanchangsen on 15/5/17.
 * description:创建自戏
 */
public class CreateZiXiActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(CreateZiXiActivity.class);

    @InjectView(R.id.textinput_gushi_name)
    TextInputLayout textinputGushiName;
    @InjectView(R.id.textinput_introduce)
    TextInputLayout textinputIntroduce;
    @InjectView(R.id.iv_add_image)
    ImageView ivAddImage;
    private EditText etTitle;
    private EditText etContent;
    private String zixiTitle;
    private String content;
    private UpImgUtil upImgUtil;
    private IMService imService;
    private ProgressBar mProgressBar;
    private ScrollView mRootScrollView;
    private boolean CLICKABLE = true;
    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";

    private Bitmap photo;

    private String commentGroupid;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_write_gushi, topContentView);
        ButterKnife.inject(this, view);

        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("保存");
        setRighTitleTextColor(getResources().getColor(R.color.pink));
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zixiTitle = etTitle.getText().toString().trim();
                content = etContent.getText().toString().trim();
                if (TextUtils.isEmpty(zixiTitle) || TextUtils.isEmpty(content)) {
                    showToast("请输入内容");
                } else {
                    if (CLICKABLE) {
                        beginDeal();
                        GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_ZIXI;
                        T.showShort(CreateZiXiActivity.this, getString(R.string.please_wait));
                        IMApplication.IS_REFRESH = true;
                        createSelfStory();
                    }
                }

            }
        });
        setTitle("创建自戏");
        initView();
    }

    /**
     * 创建自戏
     */
    private void createSelfStory() {
        /*String groupName = "评论群";
        createGroup(groupName);*/

        commentGroupid = "";
        upLoadImg();
    }

    private void upLoadImg() {
        if (photo != null) {
            /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
            Date date = new Date();
            /** 获取当前时间并且进一步转化为字符串 */
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
            String pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

            upImgUtil = new UpImgUtil();
            upImgUtil.setFilePath(pic_path, pic_path);
            upImgUtil.saveHeadImg(photo);
            upImgUtil.upLoadPicture(new HeadImgListener() {
                public void notifyImgUploadFinished(String url) {
                    if(!TextUtils.isEmpty(url)){
                        createZixiReq(url);
                    }else{
                        Toast.makeText(getApplicationContext(),"网络异常,请检查网络连接",Toast.LENGTH_LONG).show();
                        endDeal();
                        return;
                    }
                }
            });

        } else {
            createZixiReq("");
        }
    }

    private void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(CreateZiXiActivity.this);
        builder.setTitle("提示");
        builder.setMessage("要退出当前页面吗?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            showDialog();
        }
        return false;
    }

    private void createZixiReq(String url) {
        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("portraitimg", url);
        params.addParams("title", zixiTitle);
        params.addParams("groupid", commentGroupid);
        params.addParams("content", content);
        OkHttpClientManager.postAsy(ConstantValues.CreateSelfStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                endDeal();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        CreateStoryTopicEvent event =new CreateStoryTopicEvent();
                        event.eventType = CreateStoryTopicEvent.Event.TYPE_CREATE_PLAY;
                        EventBus.getDefault().post(event);
                        finish();
                    } else {
                        endDeal();
                    }
                    showToast(object.getString("describe"));
                } catch (Exception e) {
                    endDeal();
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                endDeal();
            }
        });
    }

    private void initView() {
        ivAddImage.setOnClickListener(this);
        textinputGushiName.setHint("故事名称(30字以内)");
        textinputIntroduce.setHint("正文");
        etTitle = textinputGushiName.getEditText();
        etContent = textinputIntroduce.getEditText();
        mProgressBar = (ProgressBar) findViewById(R.id.create_zixi_progress_bar);
        mRootScrollView = (ScrollView) findViewById(R.id.sv_create_zixi);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_add_image:       // 添加图片
                showAddImgDialog();
                break;
        }
    }

    /**
     * 弹出添加图片的dialog
     */
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(CreateZiXiActivity.this, "创建自戏", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }


    /**
     * 回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {
            return;
        }
        ContentResolver resolver = getContentResolver();

        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    //use bundle to get data
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        photo = (Bitmap) bundle.get("data"); //get bitmap
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateZiXiActivity.this);
                        ivAddImage.setImageBitmap(photo);
                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        //to do find the path of pic by uri
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateZiXiActivity.this);

                        Log.d("bitmap option",
                                "bitmap width " + photo.getWidth() + " bitmap height : " + photo
                                        .getHeight());
                        ivAddImage.setImageBitmap(photo);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                        e.printStackTrace();
                    }
                }
                break;

            case PHOTORESOULT: //暂时没用
                /*  从本地图片取数据
                FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory() + "/temp.jpg");
                Bitmap bitmap = BitmapFactory.decodeStream(fis);
                addImage.setImageBitmap(bitmap);
                */
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    ivAddImage.setImageBitmap(photo);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(this);
        imServiceConnector.unbindService(this);
    }

    private void beginDeal() {
        setRighTitleText("发布中");
        mRootScrollView.setEnabled(false);
        mProgressBar.setVisibility(View.VISIBLE);
        CLICKABLE = false;
    }

    private void endDeal() {
        setRighTitleText("保存");
        mRootScrollView.setEnabled(true);
        mProgressBar.setVisibility(View.INVISIBLE);
        CLICKABLE = true;
    }

    /**
     * @param groupName 创建群的名字：评论群
     */
    private void createGroup(String groupName) {
        Set<Integer> ListSet = new HashSet<>();
        IMGroupManager groupMgr = imService.getGroupManager();
        int loginId = imService.getLoginManager().getLoginId();
        ListSet.add(loginId);
        groupMgr.reqCreateTempGroup(groupName, ListSet);
    }


    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CREATE_GROUP_OK:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_ZIXI) {
                    return;
                }
                handleCreateGroupSuccess(event);
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_ZIXI) {
                    return;
                }
                handleCreateGroupFail();
                break;
        }
    }

    /**
     * 处理群创建成功事件
     *
     * @param event 创建群的事件
     */
    private void handleCreateGroupSuccess(GroupEvent event) {
        commentGroupid = event.getGroupEntity().getPeerId() + "";
        upLoadImg();
    }

    /**
     * 处理群创建失败事件
     */
    private void handleCreateGroupFail() {
        Toast.makeText(this, getString(R.string.create_temp_group_failed), Toast.LENGTH_SHORT)
                .show();
        endDeal();
    }


}
