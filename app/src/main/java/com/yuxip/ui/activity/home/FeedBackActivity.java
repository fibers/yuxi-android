package com.yuxip.ui.activity.home;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;

import org.json.JSONObject;

/**
 * Created by Administrator on 2015/4/21.
 * description:反馈
 */
public class FeedBackActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(FeedBackActivity.class);

    private EditText feelbackQQ;
    private EditText feelbackCont;
    private String loginId;
    private String content;
    private String qqno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_feelback, topContentView);
        feelbackQQ = (EditText) findViewById(R.id.feelbackQQ);
        feelbackCont = (EditText) findViewById(R.id.feelbackCont);
        loginId = IMLoginManager.instance().getLoginId()+"";
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("发送");
        setTitle("反馈我们");
        topBar.setBackgroundColor(Color.parseColor("#f5f5f5"));
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qqno = feelbackQQ.getText().toString().trim();
                content = feelbackCont.getText().toString().trim();
                if (TextUtils.isEmpty(content) || TextUtils.isEmpty(qqno)) {
                    Toast.makeText(getApplicationContext(), "请完善信息", Toast.LENGTH_SHORT).show();
                } else {
                    //发送反馈信息
                    sendFeelBackMessage();
                }
            }
        });
    }


    /**
     * 发送意见反馈信息
     */
    private void sendFeelBackMessage() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("content", content);
        params.addParams("qqno", qqno);

        OkHttpClientManager.postAsy(ConstantValues.SENDFEELBACK, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "发送 成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "发送失败", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }
}
