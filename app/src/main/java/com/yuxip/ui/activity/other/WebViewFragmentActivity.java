package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.base.TTBaseNewFragmentActivity;
import com.yuxip.ui.fragment.other.WebviewFragment;
import com.yuxip.utils.IMUIHelper;

public class WebViewFragmentActivity extends TTBaseNewFragmentActivity {

    private String storyid;
    private WebviewFragment webviewFragment;
    private String isselfstory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }

        Intent intent = getIntent();
        if (intent.hasExtra(IntentConstant.WEBVIEW_URL)) {
            WebviewFragment.setUrl(intent.getStringExtra(IntentConstant.WEBVIEW_URL));
        }

        setContentView(R.layout.tt_fragment_activity_webview);
        webviewFragment = (WebviewFragment) getSupportFragmentManager().findFragmentById(R.id.webviewFragment);
        webviewFragment.rlRightText.setVisibility(View.GONE);
        storyid = intent.getStringExtra("storyid");
        isselfstory = intent.getStringExtra("isselfstory");
        if (!TextUtils.isEmpty(storyid) && !storyid.equals("0")) {
            webviewFragment.rlRightText.setVisibility(View.VISIBLE);
            webviewFragment.topRightTitleTxt.setVisibility(View.GONE);
            if (isselfstory.equals("0")) {
                webviewFragment.rlRightText.setText("进入剧");
            } else {
                webviewFragment.rlRightText.setText("进入自戏");
            }

            webviewFragment.rlRightText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isselfstory.equals("0")) {
                        IMUIHelper.openStoryDetailActivity(WebViewFragmentActivity.this, storyid);
                    } else {
                        IMUIHelper.openZiXiDetailsActivity(WebViewFragmentActivity.this, storyid);
                    }
                }
            });
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
