package com.yuxip.ui.activity.story;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryRoleSettingJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.NoScrollGridView;
import com.yuxip.ui.helper.KeyBoardObserver;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/11/9.
 * 剧的管理者选择角色
 */
public class ApplyRoleAdminActivity extends TTBaseActivity implements View.OnClickListener{
    private NoScrollGridView gridView;   //想扮演的角色
//    private MyListView listView;         //角色设定
private LinearLayout llcontainer;
    private RelativeLayout rlbottom;
    private TextView tvSure;
    private List<Boolean> listRoleSelect = new ArrayList<>();
    private RoleGridAdapter roleGridAdapter;
//    private RoleSetListAdapter roleSetListAdapter;
    private boolean isRoleselectOk;
    private boolean isListDataOk;
    private String  storyid;
    private StoryRoleSettingJsonBean storyRoleSettingJsonBean;
    private List<StoryRoleSettingJsonBean.RolesettingsEntity.RoletypesEntity>  listRoletypes = new ArrayList<>();
    private List<StoryRoleSettingJsonBean.RolesettingsEntity.RolenaturesEntity>  listRoleNatures = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_role_apply_admin, topContentView);
        initRes();
        requestRoleSettings();
    }

    private void initRes(){
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        setTitle("群主角色");
        topLeftBtn.setImageResource(R.drawable.back_default_btn);
        topLeftBtn.setVisibility(View.VISIBLE);
        gridView = (NoScrollGridView) findViewById(R.id.gv_role_apply);
        llcontainer = (LinearLayout) findViewById(R.id.ll_role_apply);
//        listView = (MyListView) findViewById(R.id.lv_role_apply);
        tvSure = (TextView) findViewById(R.id.tv_apply_role_sure);
        rlbottom = (RelativeLayout) findViewById(R.id.rl_bottom_role_apply);
        roleGridAdapter = new RoleGridAdapter();
//        roleSetListAdapter = new RoleSetListAdapter();
        gridView.setAdapter(roleGridAdapter);
//        listView.setAdapter(roleSetListAdapter);
        tvSure.setOnClickListener(this);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlertDialog();
            }
        });
        gridView.setFocusable(false);
//        listView.setFocusable(false);
        KeyBoardObserver keyBoardObserver = new KeyBoardObserver();
        keyBoardObserver.setBaseRootAndView(baseRoot,rlbottom);
    }



    private void initSelectList(){
        listRoleSelect.clear();
        for(int i = 0;i< listRoletypes.size() ;i++){
            listRoleSelect.add(false);
        }
    }

    private void setListData(){
        llcontainer.removeAllViews();
        for(int i = 0 ; i<listRoleNatures.size();i++){
            View view = LayoutInflater.from(ApplyRoleAdminActivity.this).inflate(R.layout.item_lv_role_apply, null);
            TextView tvRolesetName = (TextView) view.findViewById(R.id.tv_role_set_role_apply);
            tvRolesetName.setText(listRoleNatures.get(i).getName());
            EditText etRoleset = (EditText) view.findViewById(R.id.et_role_set_role_apply);
            etRoleset.setMovementMethod(ScrollingMovementMethod.getInstance());
            etRoleset.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                    return false;
                }
            });
            llcontainer.addView(view);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_apply_role_sure:
                checkListInflate();
                checkGridSelect();
                if(isRoleselectOk&&isListDataOk){
                    tvSure.setClickable(false);
                    submitApplyRoleData();
                }else{
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_has_data_not_inflate),Toast.LENGTH_LONG).show();
                }
                break;

        }
    }

    private class RoleGridAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return listRoletypes.size();
        }

        @Override
        public Object getItem(int position) {
            return listRoletypes.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = LayoutInflater.from(ApplyRoleAdminActivity.this).inflate(R.layout.item_gv_role_apply,null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

//            viewHolder.tvRole.setTextColor(getResources().getColor(R.color.selector_water_textcolor));
            viewHolder.tvRole.setBackgroundResource(R.drawable.selector_story_type_select);
            viewHolder.tvRole.setEnabled(true);
            viewHolder.tvRole.setText(listRoletypes.get(position).getType());
            if(listRoletypes.get(position).getCount().equals(listRoletypes.get(position).getApplied())){
                viewHolder.tvRole.setEnabled(false);
                viewHolder.tvRole.setSelected(true);
                viewHolder.tvRole.setBackgroundResource(R.drawable.shape_grey_rect_solid);
            }

            viewHolder.tvRole.setTag(position);
            viewHolder.tvRole.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position  = (int) v.getTag();
                    if(v.isSelected()){
                        v.setSelected(false);
                        listRoleSelect.set(_position,false);
                        checkGridSelect();
                    }else{
                        clearGridSelectStatus();
                        initSelectList();
                        v.setSelected(true);
                        listRoleSelect.set(_position, true);
                    }
                }
            });


            return convertView;
        }
        class  ViewHolder{
            private TextView tvRole;
            ViewHolder(View view){
                tvRole = (TextView) view.findViewById(R.id.tv_role_role_apply);
            }
        }

    }



    private class RoleSetListAdapter extends  BaseAdapter{

        @Override
        public int getCount() {
            return listRoleNatures.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if(convertView == null){
                convertView = LayoutInflater.from(ApplyRoleAdminActivity.this).inflate(R.layout.item_lv_role_apply,null);
                holder = new Holder(convertView);
                convertView.setTag(holder);
            }else{
                holder = (Holder) convertView.getTag();
            }

            holder.tvRolesetName.setText(listRoleNatures.get(position).getName());
            return convertView;
        }
        class Holder{
            private TextView tvRolesetName;
            private EditText etRoleset;
            Holder(View view){
                tvRolesetName = (TextView) view.findViewById(R.id.tv_role_set_role_apply);
                etRoleset = (EditText) view.findViewById(R.id.et_role_set_role_apply);
            }
        }


    }



    private void clearGridSelectStatus(){
        for(int i=0 ; i <gridView.getChildCount() ;i++){
            TextView tvRole = (TextView) gridView.getChildAt(i).findViewById(R.id.tv_role_role_apply);
            if(tvRole.isEnabled())
                tvRole.setSelected(false);
        }
    }

    private boolean checkListInflate(){
        for(int i =0;i<llcontainer.getChildCount() ;i++){
            EditText editText = (EditText) llcontainer.getChildAt(i).findViewById(R.id.et_role_set_role_apply);
            if(TextUtils.isEmpty(editText.getText().toString().trim())){
                isListDataOk = false;
                return  false;
            }
        }
        isListDataOk = true;
        return true;
    }

    private boolean checkGridSelect(){
        for(int i=0;i<listRoleSelect.size();i++){
            if(listRoleSelect.get(i) == true){
                isRoleselectOk = true;
                return true;
            }
        }
        isRoleselectOk = false;
        return  false;
    }

    private int getGridSelect(){
        for(int i = 0;i<listRoleSelect.size() ;i++){
            if(listRoleSelect.get(i) == true){
                return i;
            }
        }
        return  -1;
    }


    private List<ApplyItems> getListItems(){
        List<ApplyItems> listApplys = new ArrayList<>();
        for(int i =0; i<llcontainer.getChildCount();i++){
            ApplyItems applyItems = new ApplyItems();
            applyItems.setId(listRoleNatures.get(i).getId());
            applyItems.setValue(((EditText)llcontainer.getChildAt(i).findViewById(R.id.et_role_set_role_apply)).getText().toString().trim());
            listApplys.add(applyItems);
        }
        return  listApplys;
    }

    private void requestRoleSettings(){
        if(TextUtils.isEmpty(storyid)){
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid",storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleSettings, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_net_status_error),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }


    private void onReceiveData(String response){
        try {
            storyRoleSettingJsonBean = new Gson().fromJson(response,StoryRoleSettingJsonBean.class);
            if(storyRoleSettingJsonBean == null){
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_get_netdata_fail),Toast.LENGTH_LONG).show();
                return;
            }

            if(storyRoleSettingJsonBean.getRolesettings().getRoletypes()!=null&&storyRoleSettingJsonBean.getRolesettings().getRolenatures()!=null){
                listRoletypes = storyRoleSettingJsonBean.getRolesettings().getRoletypes();
                listRoleNatures = storyRoleSettingJsonBean.getRolesettings().getRolenatures();
                initSelectList();
                roleGridAdapter.notifyDataSetChanged();
                setListData();
//                roleSetListAdapter.notifyDataSetChanged();
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_get_netdata_fail),Toast.LENGTH_LONG).show();
        }
    }


    private void submitApplyRoleData(){
        if(TextUtils.isEmpty(storyid)){
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid",storyid);
        params.addParams("roletype",listRoletypes.get(getGridSelect()).getType());
        params.addParams("rolenatures",new Gson().toJson(getListItems()));
        OkHttpClientManager.postAsy(ConstantValues.ApplyMyStoryRole, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(),getResources().getString(R.string.story_net_status_error),Toast.LENGTH_LONG).show();
                        tvSure.setClickable(true);
                    }

                    @Override
                    public void onResponse(String response) {
                        onRecevieApplyData(response);
                    }
                });

    }

    private void onRecevieApplyData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("1")){
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.operation_success),Toast.LENGTH_LONG).show();
                finish();
            }else{
                Toast.makeText(getApplicationContext(),jsonObject.getString("describe"),Toast.LENGTH_LONG).show();
                tvSure.setClickable(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
            tvSure.setClickable(true);
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.server_busy_try_later),Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK){
            showAlertDialog();
            return  true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.story_quit_edit_and_exit));
        builder.setTitle(getResources().getString(R.string.square_prompt));
        builder.setPositiveButton(getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        }).setNegativeButton(getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).show();
    }


    private class ApplyItems{
        private String id ;
        private String value;

        public void setId(String id) {
            this.id = id;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
//            return super.toString();
            return "{"+"\"id\":"+"\""+id+"\","+"\"value\":"+"\""+value+"\""+"}";
        }
    }
}
