package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseNewCompatActivity;
import com.yuxip.ui.adapter.BaseRecyclerViewAdapter;
import com.yuxip.ui.adapter.StoryCategoryAdapter;
import com.yuxip.ui.customview.MyRecyclerView;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 搜索类别
 * Created by HeTianpeng on 2015/07/18.
 */
public class StoryCategoryActivity extends TTBaseNewCompatActivity implements BaseRecyclerViewAdapter.OnItemClickListener {

    @InjectView(R.id.left_btn)
    ImageView leftBtn;
    @InjectView(R.id.recycler_view)
    MyRecyclerView recyclerView;

    StoryCategoryAdapter adapter;

    private ArrayList<StoryClass.ListEntity> categorys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.activity_story_category);
        ButterKnife.inject(this);

        categorys = getIntent().getParcelableArrayListExtra("category");

        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
        recyclerView.setHasFixedSize(true);

        adapter = new StoryCategoryAdapter(this, categorys);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(this);

        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }



    @Override
    public void onItemClick(int postion, RecyclerView.ViewHolder holder) {
        StoryListSearchActivity.startActivity(this, categorys.get(postion).getId(), "", categorys.get(postion).getName(),
                StoryListSearchActivity.Type.SEARCH);
    }


}
