package com.yuxip.ui.activity.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.RemarkNameManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.adapter.HomePageAdapter;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.widget.MyListView;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zl on 2015/7/13.
 * description : 其他用户主页
 */
public class UserHomePageActivity extends TTBaseNewActivity implements View.OnClickListener, AdapterView.OnItemClickListener {
    @InjectView(R.id.ll_home_top_bg)
    RelativeLayout llHomeTopBg;
    @InjectView(R.id.tv_story_num)
    TextView storyNum;
    @InjectView(R.id.tv_mstory)
    TextView tvMstory;
    @InjectView(R.id.ll_home_story)
    LinearLayout llHomeStory;
    @InjectView(R.id.tv_book_num)
    TextView bookNum;
    @InjectView(R.id.tv_mbook)
    TextView tvMbook;
    @InjectView(R.id.ll_home_book)
    LinearLayout llHomeBook;
    @InjectView(R.id.tv_topic_num)
    TextView topicNum;
    @InjectView(R.id.tv_mtopic)
    TextView tvMtopic;
    @InjectView(R.id.ll_home_topic)
    LinearLayout llHomeTopic;
    @InjectView(R.id.homelist)
    MyListView homelist;
    @InjectView(R.id.iv_home_back)
    ImageView ivHomeBack;
    @InjectView(R.id.tv_word_num)
    TextView tvWordNum;
    @InjectView(R.id.tv_home_add_friend)
    TextView tvHomeAddFriend;
    @InjectView(R.id.tv_more)
    TextView tv_more;//this is the report event
    @InjectView(R.id.iv_home_head_img)
    CustomHeadImage ivHomeHeadImg;

    @InjectView(R.id.tv_home_name)
    TextView tvHomeName;
    @InjectView(R.id.iv_sex_icon)
    ImageView ivSexIcon;
    @InjectView(R.id.tv_id)
    TextView tvId;
    @InjectView(R.id.tv_introduce)
    TextView tvIntroduce;
    @InjectView(R.id.tv_home_chat)
    TextView tvHomeChat;
    @InjectView(R.id.tv_home_delete_friend)
    TextView tvHomeDeleteFri;


    /**
     * 这里是后加的
     */
    @InjectView(R.id.rel_family_homepage)
    RelativeLayout rel_family;
    @InjectView(R.id.rel_collect_homepage)
    RelativeLayout rel_collect;
    @InjectView(R.id.rel_favor_homepage)
    RelativeLayout rel_favor;
    @InjectView(R.id.tv_familynum_homepage)
    TextView tv_familynum;
    @InjectView(R.id.tv_collectnum_homepage)
    TextView tv_collectnum;
    @InjectView(R.id.tv_favornum_homepage)
    TextView tv_favormum;

    @InjectView(R.id.rel_home_bottom)
    RelativeLayout rel_bottom_homepage;

    @InjectView(R.id.tv_title_user_homepage)
    TextView tvTitle;
    @InjectView(R.id.iv_singwriter_user_home)
    ImageView iv_signWriter;       //是否是签约写手

    /**
     * 公用的 如果是本人的话
     */
    @InjectView(R.id.ll_edit_story_btn)     //编辑栏的整体内容
            LinearLayout editStoryBtn;
    @InjectView(R.id.tv_edit_des)           //编辑栏目的点击跳转
            TextView tvEditDes;
    @InjectView(R.id.tv_desc)               //编辑栏的二级描述
            TextView tvDes;
    @InjectView(R.id.tv_story_des)          //编辑栏的一级描述
            TextView tvStoryDes;


    private boolean isDataOk = false;
    private Logger logger;

    private boolean isMyFridend;
    private boolean isMine;
    private String loginId;
    private String personid;
    private List<HomeInfo.PersoninfoEntity.MyHomeEntity> mystorys = new ArrayList<>();
    private HomePageAdapter adapter;
    private HomeInfo.PersoninfoEntity personinfo;
    private String isFriend;
    private IMService imService;
    private List<TextView> list_title = new ArrayList<>();
    private List<TextView> list_num = new ArrayList<>();
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    private void initListener() {
        rel_family.setOnClickListener(this);
        rel_collect.setOnClickListener(this);
        rel_favor.setOnClickListener(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        logger = Logger.getLogger(UserHomePageActivity.class);
        setContentView(R.layout.activity_user_home_page);
        imServiceConnector.connect(this);
        ButterKnife.inject(this);
        initTextList();
        initListener();
        initRes();
        ReqHomeInfo();
        setSelected(0);   //设置第一个选中
    }

    /**
     * 获取用户信息
     */
    private void ReqHomeInfo() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("personid", personid);

        OkHttpClientManager.postAsy(ConstantValues.GetPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("result").equals("1")) {
                        onReceiveHomeInfoResponse(response);
                    } else {
                        T.show(getApplicationContext(), jsonObject.getString("describe"), 0);
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), R.string.network_err, 1);
            }
        });
    }

    /**
     * 解析数据
     *
     * @param result
     */
    private void onReceiveHomeInfoResponse(String result) {
        HomeInfo homeinfo = new Gson().fromJson(result, HomeInfo.class);
        if (homeinfo.getResult().equals("1")) {
            personinfo = homeinfo.getPersoninfo();
            HomeEntityManager.getInstance().setOtherPersoninfo(personinfo);
            String remarkName = null;
            if (personinfo != null && personinfo.getId() != null) {
                remarkName = RemarkNameManager.getInstance().getNickNameByUserId(personinfo.getId());
            }

            if (remarkName != null && !remarkName.equals("")) {
                personinfo.setNickname(remarkName);
            }
            onResAddData(personinfo);
        }
        if ((loginId).equals(personid)&&tvHomeAddFriend!=null) {
            tvHomeAddFriend.setVisibility(View.GONE);
        }
    }

    /**
     * 添加数据
     *
     * @param personinfo
     */
    private void onResAddData(HomeInfo.PersoninfoEntity personinfo) {
        if (personinfo == null || tvId == null)
            return;
        tvId.setText("语戏号: " + personinfo.getId());
        tvWordNum.setText(personinfo.getWordcount() + "字");
        tvHomeName.setText(personinfo.getNickname());
        isFriend = personinfo.getIsfriend();
        if (isFriend.equals("0")) {
            isMyFridend = false;
            tv_more.setText("举报");
            tvHomeDeleteFri.setVisibility(View.GONE);
            tvHomeAddFriend.setVisibility(View.VISIBLE);
            tvHomeChat.setVisibility(View.GONE);
        } else {
            isMyFridend = true;
            tv_more.setText("更多");
            tvHomeAddFriend.setVisibility(View.GONE);
            tvHomeChat.setVisibility(View.VISIBLE);
            tvHomeDeleteFri.setVisibility(View.VISIBLE);
        }
        //男女
        String sex = personinfo.getGender();
        if (sex.equals("1")) {
            ivSexIcon.setImageResource(R.drawable.home_man_icon);
            llHomeTopBg.setBackgroundResource(R.drawable.bg_info_blue);
            tvHomeChat.setBackgroundResource(R.drawable.iv_btn_send_man_userhome);
        } else {
            ivSexIcon.setImageResource(R.drawable.home_woman_icon);
            llHomeTopBg.setBackgroundResource(R.drawable.bg_info_pink);
            tvHomeChat.setBackgroundResource(R.drawable.iv_btn_send_woman_userhome);
        }

        /**     判断是是否是签约写手      */
        String type = personinfo.getType();
        if (type.equals("1") || type.equals("3")) {
            iv_signWriter.setVisibility(View.VISIBLE);
        } else {
            iv_signWriter.setVisibility(View.GONE);
        }
        //简介
        String intro = personinfo.getIntro();
        if (TextUtils.isEmpty(intro)) {
            tvIntroduce.setText("暂无简介");
        } else {
            tvIntroduce.setText(personinfo.getIntro());
        }
        //喜欢的数
        if (personinfo.getMyfavor() != null) {
            tv_favormum.setText(personinfo.getMyfavor().size() + "");
        }
        //故事
        if (personinfo.getMyselfstorys() != null) {
            bookNum.setText(personinfo.getMyselfstorys().size() + "");
        }
        //话题
        if (personinfo.getMytopics() != null) {
            topicNum.setText(personinfo.getMytopics().size() + "");
        }
        //家族
        if (personinfo.getMyfamily() != null) {
            tv_familynum.setText(personinfo.getMyfamily().size() + "");
        }
        //收藏
        if (personinfo.getMycollects() != null) {
            tv_collectnum.setText(personinfo.getMycollects().size() + "");
        }
        List<HomeInfo.PersoninfoEntity.MyHomeEntity> storys = personinfo.getMystorys();
        if (storys != null && storys.size() > 0) {
            mystorys.clear();
            mystorys.addAll(storys);
            adapter.notifyDataSetChanged();
        }
        storyNum.setText(storys.size() + "");
        ivHomeHeadImg.loadImage(personinfo.getPortrait(), "0");
        isDataOk = true;
    }

    private void initRes() {
        loginId = IMLoginManager.instance().getLoginId() + "";
        if (getIntent().getStringExtra(IntentConstant.PERSION_ID) != null) {
            personid = getIntent().getStringExtra(IntentConstant.PERSION_ID);
            isMine = personid.equals(loginId);
        } else {
            Toast.makeText(getApplicationContext(), "获取用户信息失败", Toast.LENGTH_SHORT).show();
            return;
        }
        if (isMine) {
            rel_bottom_homepage.setVisibility(View.GONE);
            tvMstory.setText("我的剧");
            tvMbook.setText("我的自戏");
            tvMtopic.setText("我的话题");
            tvTitle.setText("我的主页");
            tv_more.setVisibility(View.GONE);
        }
        adapter = new HomePageAdapter(UserHomePageActivity.this, mystorys);
        homelist.setAdapter(adapter);
        homelist.setSelected(false);
        homelist.setFocusable(false);
        homelist.setClickable(false);
        homelist.setHeaderDividersEnabled(false);
        ivHomeBack.setOnClickListener(this);
        tvHomeAddFriend.setOnClickListener(this);
        tvHomeDeleteFri.setOnClickListener(this);
        llHomeBook.setOnClickListener(this);
        llHomeStory.setOnClickListener(this);
        llHomeTopic.setOnClickListener(this);
        tvHomeChat.setOnClickListener(this);
        tv_more.setOnClickListener(this);
        homelist.setOnItemClickListener(this);
    }

    @Override
    public void onDestroy() {
        ButterKnife.reset(this);
        imServiceConnector.disconnect(this);
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        if (!isDataOk) {
            return;
        }
        switch (v.getId()) {
            case R.id.iv_home_back:         //改为设置
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.iv_home_edit:
                break;
            case R.id.tv_home_add_friend:   //添加好友
                if (isFriend.equals("0")) {
                    if (imService != null) {
                        DialogHelper.showAddFriendDialog(this, imService, Integer.valueOf(personid));
                    }
                }
                break;
            /**  剧列表  如果没有内容自己则有写剧按钮 他人只有无剧提示  下自戏,话题同 */
            case R.id.ll_home_story:         //剧
                setSelected(0);
                if (personinfo != null && personinfo.getMystorys() != null && personinfo.getMystorys().size() > 0) {
                    if (editStoryBtn == null)
                        return;
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMystorys());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    tvStoryDes.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                } else {
                    if (!isMine) {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvEditDes.setVisibility(View.GONE);
                        tvDes.setVisibility(View.GONE);
                        tvStoryDes.setText("他还没有发布过剧");
                        homelist.setVisibility(View.GONE);
                    } else {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        homelist.setVisibility(View.GONE);
                        tvEditDes.setVisibility(View.VISIBLE);
                        tvDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setText("你还没有发布过剧");
                        tvEditDes.setText("写剧本");
                        tvEditDes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                IMUIHelper.openCreateStoryActivity_New(UserHomePageActivity.this);
                            }
                        });
                    }
                }
                break;
            case R.id.ll_home_book:         //故事
                setSelected(1);
                if (personinfo != null && personinfo.getMyselfstorys() != null && personinfo.getMyselfstorys().size() > 0) {
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMyselfstorys());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    tvStoryDes.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                } else {
                    if (!isMine) {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvEditDes.setVisibility(View.GONE);
                        tvDes.setVisibility(View.GONE);
                        tvStoryDes.setText("他还没有发布过故事");
                        homelist.setVisibility(View.GONE);
                    } else {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        homelist.setVisibility(View.GONE);
                        tvEditDes.setVisibility(View.VISIBLE);
                        tvDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setText("你还没有发布故事");
                        tvEditDes.setText("写故事");
                        tvEditDes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                IMUIHelper.openCreateZiXiActivity(UserHomePageActivity.this);
                            }
                        });
                    }
                }
                break;
            case R.id.ll_home_topic:        //话题
                setSelected(2);
                if (personinfo != null && personinfo.getMytopics() != null && personinfo.getMytopics().size() > 0) {
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMytopics());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                    tvStoryDes.setVisibility(View.GONE);
                } else {
                    if (!isMine) {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        homelist.setVisibility(View.GONE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvEditDes.setVisibility(View.GONE);
                        tvDes.setVisibility(View.GONE);
                        tvStoryDes.setText("他还没有发布过话题");
                    } else {
                        editStoryBtn.setVisibility(View.VISIBLE);
                        homelist.setVisibility(View.GONE);
                        tvEditDes.setVisibility(View.VISIBLE);
                        tvDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setVisibility(View.VISIBLE);
                        tvStoryDes.setText("你还没有参与话题");
                        tvEditDes.setText("写话题");
                        tvEditDes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                IMUIHelper.openTopicActivity(UserHomePageActivity.this);
                            }
                        });
                    }
                }
                break;
            case R.id.tv_home_chat:
                IMUIHelper.openFriendChatActivity(this,
                        ConstantValues.USERTYPE + personid);
                break;
            case R.id.tv_home_delete_friend:            //删除
                showDeleteFriAlert();
                break;
            case R.id.tv_more:          //更多  如果是好友则跳转好友设置 否则是举报界面
                if (isMyFridend) {
                    /**   跳转设置界面  需要传 备注名 分组名 以及 (暂定)阅读权限等 */
                    IMUIHelper.openFriendSettingActivity(UserHomePageActivity.this, personid);
                } else {
                    IMUIHelper.openReportPersonActivity(UserHomePageActivity.this, personid);
                }
                break;
            /**  家族   */
            case R.id.rel_family_homepage:  //0表示他人主页 1表示自己主页
                IMUIHelper.openHisFamilyEActivity(this, "0");
                break;
            /** 喜欢 */
            case R.id.rel_favor_homepage:   //传值0表示喜欢  1表示收藏  第二个0表示他人主页
                IMUIHelper.openFavorOrCollectActivity(this, "0", "0");
                break;
            /**  收藏    */
            case R.id.rel_collect_homepage:
                IMUIHelper.openFavorOrCollectActivity(this, "1", "0");
                break;
        }
    }

    private void deleteFriend() {
        /** 删除好友 */
        imService.getMessageManager().sendDelFriendReq(Integer.valueOf(personid));
        /** 构建RecentInfo删除本地会话 */
        RecentInfo recentInfo = new RecentInfo();
        String sessionKey = "1" + "_" + personid;
        recentInfo.setSessionKey(sessionKey);
        recentInfo.setPeerId(Integer.valueOf(personid));
        recentInfo.setSessionType(1);
        imService.getSessionManager().reqRemoveSession(recentInfo);
        showToast("小主，" + personinfo.getNickname() + "已被打入冷宫");
        FriendGroupManager.getInstance().deleteFriend(personid);
        finish();
    }

    /**
     * 短时间显示Toast
     *
     * @param message
     */
    private void showToast(String message) {
        Toast.makeText(UserHomePageActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    private int currentSelected = 0;

    private void setSelected(int tag) {
        currentSelected = tag;
        setItemSelect(currentSelected);
    }

    /**
     * 初始化下面选择的text集合
     */
    private void initTextList() {
        tvMstory.setText("TA的剧");
        tvMbook.setText("TA的自戏");
        tvMtopic.setText("TA的话题");
        list_title.add(tvMstory);
        list_title.add(tvMbook);
        list_title.add(tvMtopic);
        list_num.add(storyNum);
        list_num.add(bookNum);
        list_num.add(topicNum);
    }

    /**
     * 设置选中状态 主要是  "我的剧"等的字体颜色 红<-->黑
     * position 从0开始  0, 1 , 2 , 3
     */
    private void setItemSelect(int position) {
        for (int i = 0; i < list_title.size(); i++) {
            list_title.get(i).setSelected(false);
            list_num.get(i).setSelected(false);
        }
        list_title.get(position).setSelected(true);
        list_num.get(position).setSelected(true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        HomeInfo.PersoninfoEntity.MyHomeEntity mh = adapter.getMystorys().get(i);
        String type = mh.getType();
        switch (currentSelected) {
            /**   跳转剧页面   */
            case 0:
                startActivity(mh.getStoryid(), type);
                break;
            /**  跳转自戏界面  */
            case 1:
                Intent intent = new Intent(this, ZiXiDetailsActivity.class);
                intent.putExtra(IntentConstant.STORY_ID, mh.getStoryid());
                intent.putExtra(IntentConstant.CREATOR_ID, personid);
                startActivity(intent);
                break;
            /**   跳转话题界面  */
            case 2:
//                IMUIHelper.openTopicDetailsActivity(this, mh.getTopicid());
                if(mh!=null)
                    IMUIHelper.openSquareCommentActivity(UserHomePageActivity.this,mh.getTitle(),mh.getTopicid(),IntentConstant.FLOOR_TYPE_TOPIC);
                break;
        }
    }

    /**
     * ？？？根据类型判断是跳转剧或是话题？？？
     */
    private void startActivity(String id, String type) {
        if (TextUtils.isEmpty(type)) {
            IMUIHelper.openStoryDetailsActivity(this, id);
        } else {
            if (type.equals("1")) {
                Intent intent = new Intent(this, ZiXiDetailsActivity.class);
                intent.putExtra(IntentConstant.STORY_ID, id);
                intent.putExtra(IntentConstant.CREATOR_ID, loginId);
                startActivity(intent);
            } else if (type.equals("0")) {
                IMUIHelper.openStoryDetailsActivity(this, id);
            } else {
                IMUIHelper.openTopicDetailsActivity(this, id);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        imServiceConnector.unbindService(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        imServiceConnector.connect(this);
    }


    private void showDeleteFriAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("提示");
        builder.setMessage("确定删除该好友吗?");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteFriend();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    /**
     * 主要是修改好友备注的回调
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode)
            return;
        switch (requestCode) {
            case SysConstant.ACTIVITY_FRIEND_SETTING:       //好友设置修改了备注名
                String remarkName = data.getStringExtra(IntentConstant.REMARK_NAME);
                personinfo.setNickname(remarkName);
                tvHomeName.setText(remarkName);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
