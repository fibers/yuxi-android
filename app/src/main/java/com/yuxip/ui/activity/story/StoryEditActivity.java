package com.yuxip.ui.activity.story;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.GroupsEntity;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * 编辑剧大纲
 */
public class StoryEditActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryEditActivity.class);

    private TextView tv_title;                                          //剧名字            TextView
    private TextView tv_creatorName;                                    //剧管理员名字的     TextView
    private TextView tv_createTime;                                     //剧创建时间的       TextView
    private TextView tv_intro;                                          //剧简介的          TextView
    private TextView tvGroupPermission;                                 //阅读权限          TextView

    private ImageView ivBackBtn;
    private ImageView storyInfoImg;
    private ImageView iv_apply_play;                                    //申请加入剧的按钮
    private ImageView action_fave;                                      //喜欢
    private String storyimg;
    private ArrayList<GroupsEntity> arr_group = new ArrayList<>();      //存放剧对应的群集合
    private ArrayList<StoryParentEntity> storyParentEntityList;

    private String storyId;                                             //剧id
    private String creatorId;                                           //剧管理员id
    private StoryParentEntity storyParentEntity;
    private boolean IS_ADMIN = false;                                   //是否是管理员
    private IMService imService;

    private String introContent;                                        //剧简介
    private String title;                                               //剧名字
    private String portrait;                                            //剧头像url
    private String relation;
    private Boolean isPraised;
    private ScrollView mScrollView;
    private String permission;

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";

    private String pic_path;
    private Bitmap photo;
    private UpImgUtil upImgUtil;
    private String capturePath;

    private boolean isNeedRefresh = false;               // 返回上级页面时是否需要更新数据

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        creatorId = getIntent().getStringExtra(IntentConstant.CREATOR_ID);
        relation = getIntent().getStringExtra(IntentConstant.RELATION);
        if(SharedPreferenceUtils.getBooleanDate(getApplicationContext(), storyId, true)) {
            permission = "1";
        } else {
            permission = "0";
        }

        initData();
        createView();
    }


    private void createView() {
        storyParentEntityList = new ArrayList<>();

        LayoutInflater.from(this).inflate(R.layout.activity_story_edit, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("编辑剧大纲");
        String loginId = IMLoginManager.instance().getLoginId() + "";
        initView();
        if (relation == null) {
            relation = "none";
        }
        if (creatorId == null) {
            creatorId = "none";
        }
        /** 判断用户是不是管理员 */
        if (relation.equals("owner") || creatorId.equals(loginId)) {
            IS_ADMIN = true;
            /** 剧群可见 */
            findViewById(R.id.storyGroup).setVisibility(View.VISIBLE);
            findViewById(R.id.storyGroup).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(StoryEditActivity.this, StoryGroupActivity.class);
                    intent.putExtra(IntentConstant.STORY_ID, storyId);
                    startActivity(intent);
                }
            });
            /** 人设模版 */
            findViewById(R.id.ll_person_setting).setVisibility(View.INVISIBLE);
            findViewById(R.id.ll_person_setting).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openStoryPersonSettingModuleActivity(StoryEditActivity.this, storyId);
                }
            });
            /** 阅读权限 */
            findViewById(R.id.rlReadQx).setVisibility(View.VISIBLE);
            findViewById(R.id.rlReadQx).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openReadRightSettingActivity(StoryEditActivity.this, storyId, permission);
                }
            });

        }
        /** 判断是否已在剧中  是 - 对戏  否 - 申请 */
        mScrollView = (ScrollView) findViewById(R.id.scroll);
    }


    @Override
    public void onDestroy() {
        imServiceConnector.disconnect(StoryEditActivity.this);
        super.onDestroy();
    }

    @Override
    public void finish() {
        if (isNeedRefresh) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.STORY_DETAIL_NEED_REFRESH, isNeedRefresh);
            setResult(RESULT_OK, intent);
        }
        super.finish();
    }

    /**
     * 初始化视图
     */
    private void initView() {
        tv_title = (TextView) findViewById(R.id.title);
        tv_creatorName = (TextView) findViewById(R.id.creatorname);
        tv_createTime = (TextView) findViewById(R.id.createtime);
        tv_intro = (TextView) findViewById(R.id.intro);
        tvGroupPermission = (TextView) findViewById(R.id.tvGroupPermission);
        //设置阅读状态
        if ("1".equals(permission)) {
            tvGroupPermission.setText("打开");
        } else {
            tvGroupPermission.setText("关闭");
        }
        storyInfoImg = (ImageView) findViewById(R.id.ivStoryEditStoryImg);
        ivBackBtn = (ImageView) findViewById(R.id.left_btn);
        ivBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        storyInfoImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddImgDialog();
            }
        });
        /** 剧名 */
        findViewById(R.id.storyName).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storyParentEntity == null) {
                    Toast.makeText(StoryEditActivity.this, "正在请求数据，请稍候", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(StoryEditActivity.this, StoryIntroActivity.class);
                intent.putExtra("IS_ADMIN", IS_ADMIN);
                intent.putExtra("storyid", storyId);
                intent.putExtra("type", "title");
                intent.putExtra("intro", storyParentEntity.getTitle());
                startActivityForResult(intent, 0);
            }
        });
        /** 简介 */
        findViewById(R.id.storyIntro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storyParentEntity == null) {
                    Toast.makeText(StoryEditActivity.this, "正在请求数据，请稍候", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(StoryEditActivity.this, StoryIntroActivity.class);
                intent.putExtra("IS_ADMIN", IS_ADMIN);
                intent.putExtra("storyid", storyId);
                intent.putExtra("type", "intro");
                intent.putExtra("intro", storyParentEntity.getIntro());
                startActivityForResult(intent, 0);
            }
        });
        /** 主线剧情 */
        findViewById(R.id.storyMain).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storyParentEntity == null) {
                    Toast.makeText(StoryEditActivity.this, "正在请求数据，请稍候", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(StoryEditActivity.this, MainStoryActivity.class);
                intent.putExtra("creatorid", creatorId);
                intent.putExtra("storyid", storyId);
                intent.putExtra("IS_ADMIN", IS_ADMIN);
                startActivityForResult(intent, 0);
            }
        });
        /** 主线人物 */
        findViewById(R.id.mianPerson).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storyParentEntity == null) {
                    Toast.makeText(StoryEditActivity.this, "正在请求数据，请稍候", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(StoryEditActivity.this, MainPersonActivity.class);
                intent.putExtra("creatorid", creatorId);
                intent.putExtra("storyid", storyId);
                intent.putExtra("IS_ADMIN", IS_ADMIN);
                startActivityForResult(intent, 0);
            }
        });
        /** 规则 */
        findViewById(R.id.storyRule).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storyParentEntity == null) {
                    Toast.makeText(StoryEditActivity.this, "正在请求数据，请稍候", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(StoryEditActivity.this, RuleStoryActivity.class);
                intent.putExtra("IS_ADMIN", IS_ADMIN);
                intent.putExtra("storyid", storyId);
                intent.putExtra("rule", storyParentEntity.getRule());
                startActivityForResult(intent, 0);
            }
        });
        /** 角色申请  */
        findViewById(R.id.role_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(storyId))
                IMUIHelper.openApplyRoleAdminActivity(StoryEditActivity.this,storyId);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || (requestCode == PHOTOZOOM && data == null)) {
            return;
        }
        ContentResolver resolver = getContentResolver();
        switch (requestCode) {
            case NONE:
                switch (resultCode) {
                    case RESULT_OK:
                        initData();
                        break;
                }
                break;
            case PHOTOHRAPH:  //拍照
                try {
                    FileInputStream fis = new FileInputStream(capturePath);
                    photo = BitmapFactory.decodeStream(fis);

                    //photo = (Bitmap) bundle.get("data"); //get bitmap
                    photo = ImageUtil.getBigBitmapForDisplay(photo, StoryEditActivity.this);
                    storyInfoImg.setImageBitmap(photo);
                    upLoadImg();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    //use bundle to get data
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        try {
                            //FileInputStream fis = new FileInputStream(capturePath);
                            //photo = BitmapFactory.decodeStream(fis);
                            photo = (Bitmap) bundle.get("data"); //get bitmap
                            photo = ImageUtil.getBigBitmapForDisplay(photo, StoryEditActivity.this);
                            storyInfoImg.setImageBitmap(photo);
                            upLoadImg();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        //to do find the path of pic by uri
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                        photo = ImageUtil.getBigBitmapForDisplay(photo, StoryEditActivity.this);

                        Log.d("bitmap option",
                                "bitmap width " + photo.getWidth() + " bitmap height : " + photo
                                        .getHeight());
                        storyInfoImg.setImageBitmap(photo);
                        upLoadImg();
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                        e.printStackTrace();
                    }
                }
                break;
            case PHOTORESOULT: //暂时没用
                /*  从本地图片取数据
                FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory() + "/temp.jpg");
                Bitmap bitmap = BitmapFactory.decodeStream(fis);
                addImage.setImageBitmap(bitmap);
                */
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    storyInfoImg.setImageBitmap(photo);
                }
                break;
            case IntentConstant.ReadRightSettingActivity:
                String content = data.getStringExtra(IntentConstant.READ_RIGHT);
                if (content != null && !content.equals(permission)) {
                    isNeedRefresh = true;
                    permission = content;
                    if ("1".equals(permission)) {
                        tvGroupPermission.setText("打开");
                    } else {
                        tvGroupPermission.setText("关闭");
                    }
                }
                break;
        }
    }

    /**
     * 初始化数据
     */
    private void initData() {
        if(TextUtils.isEmpty(storyId))
            return;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        params.addParams("token", "110");
        OkHttpClientManager.postAsy(ConstantValues.GETSTORYINFO, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        storyParentEntityList.clear();
                        try {
                            JSONObject object = new JSONObject(response);
                            String id = object.getString("result");
                            if (id.equals("1")) {
                                SimpleDateFormat format = new SimpleDateFormat("MM-dd HH:mm");
                                String createTime = format.format(Long.parseLong(object.get("createtime").toString()) * 1000);
                                tv_creatorName.setText(object.get("creatorname").toString());
                                tv_createTime.setText(createTime);
                                introContent = object.get("intro").toString();  //简介
                                title = object.get("title").toString();         //名字哦
                                String ispraisedbyuser = object.get("ispraisedbyuser").toString();
                                if (ispraisedbyuser.equals("0")) {
                                    // 不喜欢
                                    //action_fave.setImageResource(R.drawable.action_icon_faved);
                                    isPraised = false;
                                } else if (ispraisedbyuser.equals("1")) {
//                                    喜欢
                                    //action_fave.setImageResource(R.drawable.action_icon_fave);
                                    isPraised = true;
                                }
                                tv_title.setText(title);
                                portrait = object.get("portrait").toString();
                                storyimg = object.get("storyimg").toString();
                                storyParentEntity = new StoryParentEntity();
                                storyParentEntity.setId(Integer.valueOf(object.get("id").toString()));
                                storyParentEntity.setTitle(title);
                                storyParentEntity.setPortrait(portrait);
                                storyParentEntity.setPraisenum(object.get("praisenum").toString());
                                storyParentEntity.setIntro(introContent);
                                storyParentEntity.setCreatetime(object.get("createtime").toString());
                                storyParentEntity.setCreatorid(object.get("creatorid").toString());
                                storyParentEntity.setIspraisedByUser(object.get("ispraisedbyuser").toString());
                                storyParentEntity.setRule(object.get("rule").toString());
                                /** 解析剧对应的群列表 */
                                JSONArray array = object.getJSONArray("groups");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject group_obj = array.getJSONObject(i);
                                    GroupsEntity entity = new GroupsEntity();
                                    entity.setGroupId(group_obj.getString("groupid"));
                                    entity.setTitle(group_obj.getString("title"));
                                    entity.setIsPlay(group_obj.getString("isplay"));
                                    arr_group.add(entity);
                                }

                                ImageLoader imageLoader = ImageLoaderUtil.getImageLoaderInstance();
                                Drawable drawable = DrawableCache.getInstance(getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC);
                                DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
                                imageLoader.displayImage(storyimg, storyInfoImg, imageOptions);
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {

                    }
                });
    }

    /**
     * 对剧的点赞
     *
     * @param isPraised
     */
    private void PraiseStory(Boolean isPraised) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        if (isPraised) {
            params.addParams("behavior", "1");
        } else {
            params.addParams("behavior", "0");
        }
        OkHttpClientManager.postAsy(ConstantValues.PraiseStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        Toast.makeText(StoryEditActivity.this, "点赞成功", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }

    private void upLoadImg() {
        if (photo != null) {
            /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
            Date date = new Date();
            /** 获取当前时间并且进一步转化为字符串 */
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
            pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

            upImgUtil = new UpImgUtil();
            upImgUtil.setFilePath(pic_path, pic_path);
            upImgUtil.saveHeadImg(photo);
            upImgUtil.upLoadPicture(new HeadImgListener() {
                public void notifyImgUploadFinished(String url) {
                    changeInfo(url);
                }
            });
        }
    }

    /**
     * 修改简介
     */
    private void changeInfo(String url) {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("content", url);
        params.addParams("storyid", storyId);
        params.addParams("type", "3");           //简介
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "剧情图片修改成功", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "剧情图片修改失败", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    //弹出添加图片的dialog
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(StoryEditActivity.this, "编辑剧大纲", "修改图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        getImageFromCamera();
                    }
                });
    }

    protected void getImageFromCamera() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            Intent getImageByCamera = new Intent("android.media.action.IMAGE_CAPTURE");
            String out_file_path = Environment.getExternalStorageDirectory() + "/yuxi_temp";
            File
                    dir = new File(out_file_path);
            if (!dir.exists()) {
                dir.mkdirs();
            }
            capturePath = Environment.getExternalStorageDirectory() + "/yuxi_temp/" + System.currentTimeMillis() + ".jpg";
            getImageByCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(capturePath)));
            getImageByCamera.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0.8);
            startActivityForResult(getImageByCamera, PHOTOHRAPH);
        } else {
            Toast.makeText(getApplicationContext(), "请确认已经插入SD卡", Toast.LENGTH_LONG).show();
        }
    }

}
