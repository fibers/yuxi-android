
package com.yuxip.ui.activity.other;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.event.SelectEvent;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.album.ImageGridAdapter;
import com.yuxip.ui.adapter.album.ImageItem;
import com.yuxip.utils.Logger;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 *  相册图片列表
 */
public class ImageGridActivity extends TTBaseNewActivity implements OnTouchListener {
    private List<ImageItem> dataList = null;
    private  List<ImageItem> itemList = null;
    private GridView gridView = null;
    private static TextView finish = null;
    private String name = null;
    private static ImageGridAdapter adapter = null;
	private Logger logger = Logger.getLogger(ImageGridActivity.class);
    private String type;

    private IMServiceConnector imServiceConnector = new IMServiceConnector(){
        @Override
        public void onIMServiceConnected() {
            IMService imService = imServiceConnector.getIMService();
            if(imService == null){
                throw new RuntimeException("#connect imservice success,but is null");
            }
        }

        @Override
        public void onServiceDisconnected() {

        }
    };


    private MyHandler mHandler = new MyHandler(this);

    OnScrollListener onScrollListener = new OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            switch (scrollState) {
                case OnScrollListener.SCROLL_STATE_FLING:
                    adapter.lock();
                    break;
                case OnScrollListener.SCROLL_STATE_IDLE:
                    adapter.unlock();
                    break;
                case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
                    adapter.lock();
                    break;
                default:
                    break;
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                int visibleItemCount, int totalItemCount) {
        }
    };

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        setContentView(R.layout.tt_activity_image_grid);
        name = (String) getIntent().getSerializableExtra(IntentConstant.EXTRA_ALBUM_NAME);
        dataList = (List<ImageItem>) getIntent().getSerializableExtra(IntentConstant.EXTRA_IMAGE_LIST);
        type = getIntent().getStringExtra(IntentConstant.SELECT_EVENT_TYPE);
        initView();
        initAdapter();
    }

    private void initAdapter() {
        /**
         * gridView adapter
         */
        adapter = new ImageGridAdapter(ImageGridActivity.this, dataList, mHandler);
        adapter.setTextCallback(new ImageGridAdapter.TextCallback() {
            public void onListen(int count) {
                setSendText(count);
            }
        });
        gridView.setAdapter(adapter);
        gridView.setOnScrollListener(onScrollListener);
    }

    private void initView() {
        gridView = (GridView) findViewById(R.id.gridview);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));
        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                adapter.notifyDataSetChanged();
            }
        });

        TextView title = (TextView) findViewById(R.id.base_fragment_title);
        if (name.length() > 12) {
            name = name.substring(0, 11) + "...";
        }
        title.setText(name);
        ImageView leftBtn = (ImageView) findViewById(R.id.back_btn);
        leftBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageGridActivity.this.finish();
            }
        });
        TextView cancel = (TextView) findViewById(R.id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.setSelectMap(null);
                ImageGridActivity.this.finish();
            }
        });
        finish = (TextView) findViewById(R.id.finish);
        finish.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                logger.d("pic#click send image btn");
                if (adapter.getSelectMap().size() > 0) {
                    itemList = new ArrayList<>();
                    Iterator<Integer> iter = adapter.getSelectMap().keySet().iterator();

                    /*for (Map.Entry<Integer, ImageItem> entity : adapter.getSelectMap().entrySet()) {
                        int position = entity.getKey();
                        ImageItem imageItem = entity.getValue();
                    }*/

                    while (iter.hasNext()) {
                        int position = iter.next();
                        ImageItem imgItem = adapter.getSelectMap().get(position);
                        itemList.add(imgItem);
//                        String path = imgItem.getImagePath();
//                        BitmapCache cache = BitmapCache.getInstance();
//                        Bitmap bmp = cache.getCacheBitmap(path, path);
                    }
                    SelectEvent selectEvent;
                    switch (type) {
                        case IntentConstant.SELECT_FROM_CHAT:
                            selectEvent = new SelectEvent(itemList, SelectEvent.Event.TYPE_CHAT);
                            EventBus.getDefault().post(selectEvent);
                            break;
                        case IntentConstant.SELECT_FROM_RELEASE_TOPIC:
                            selectEvent = new SelectEvent(itemList, SelectEvent.Event.TYPE_RELEASE_TOPIC);
                            EventBus.getDefault().post(selectEvent);
                            break;
                        default:
                            selectEvent = new SelectEvent(itemList, SelectEvent.Event.TYPE_INVALID);
                            EventBus.getDefault().post(selectEvent);
                            break;
                    }
                    setResult(RESULT_OK, null);
                    finish();
                } else {
                    Toast.makeText(ImageGridActivity.this, R.string.need_choose_images, Toast.LENGTH_SHORT).show();
                }
            }

        });
        TextView preview = (TextView) findViewById(R.id.preview);
        preview.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                if (adapter.getSelectMap().size() > 0) {
                    Intent intent = new Intent(ImageGridActivity.this, PreviewActivity.class);
                    startActivityForResult(intent, SysConstant.ALBUM_PREVIEW_BACK);
                } else {
                    Toast.makeText(ImageGridActivity.this, R.string.need_choose_images, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        setAdapterSelectedMap(null);
        imServiceConnector.disconnect(this);
        super.onDestroy();
    }

    public static void setSendText(int selNum) {
        if (selNum == 0) {
            finish.setText("发送");
        } else {
            finish.setText("发送" + "(" + selNum + ")");
        }
    }

    public static void setAdapterSelectedMap(Map<Integer, ImageItem> map) {
        try {
            Iterator<Integer> it = adapter.getSelectMap().keySet().iterator();
            if (map != null) {
                while (it.hasNext()) {
                    int key = it.next();
                    if (map.containsKey(key)) {
                        adapter.updateSelectedStatus(key, true);
                    } else {
                        adapter.updateSelectedStatus(key, false);
                    }
                }
                adapter.setSelectMap(map);
                adapter.setSelectTotalNum(map.size());
            } else {
                while (it.hasNext()) {
                    int key = it.next();
                    adapter.updateSelectedStatus(key, false);
                }
                adapter.setSelectMap(null);
                adapter.setSelectTotalNum(0);
            }
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
            return;
        }

    }

    public static ImageGridAdapter getAdapter() {
        return adapter;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                adapter.unlock();
                break;
        }
        return false;
    }


    static class MyHandler extends Handler {
        WeakReference<ImageGridActivity> mActivity;         //持有StoryChatActivity对象的弱引用
        MyHandler(ImageGridActivity activity) {
            mActivity = new WeakReference<>(activity);
        }
        @Override
        public void handleMessage(Message msg) {
            ImageGridActivity activity = mActivity.get();
            if(activity == null) {
                return;
            }
            switch (msg.what) {
                case 0:
                    Toast.makeText(activity, "最多选择" + SysConstant.MAX_SELECT_IMAGE_COUNT + "张图片", Toast.LENGTH_LONG).show();
                    break;
                default:
                    break;
            }
        }
    }

}
