package com.yuxip.ui.activity.other;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.PeerEntity;
import com.yuxip.JsonBean.FamilyDataJsonBean;
import com.yuxip.JsonBean.GroupData;
import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.chat.ChangeNickNameInFamilyActivity;
import com.yuxip.ui.adapter.FamilyGroupAdapter;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.ui.widget.GroupManagerGridView;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import de.greenrobot.event.EventBus;

/**
 * 添加人设的皮表
 * changed by SummerRC on 2015/6/4.
 * <p/>
 * 家族资料页面
 */
public class FamilyDetailsActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(FamilyDetailsActivity.class);

    private List<Members> memberslist = new ArrayList<>();
    private String curSessionKey;
    private View chatView;
    private String groupId;
    private CircularImage familyInfoImg;        //家族头像
    private TextView persionNikename;           //昵称
    private TextView familyMemberNum;           //家族成员数量
    private GroupManagerGridView familyUser;    //家族里面的成员
    private TextView familyName;                //家族名称
    private TextView familyId;                  //家族id
    private TextView ownernickname;             //族长名称
    private TextView familyIntro;               //家族简介
    private LinearLayout messageBtn;            //聊天按钮
    private TextView conn;
    private GridView gridView;
    private FamilyGroupAdapter adapter;
    private PeerEntity peerEntity;
    private IMService imService;
    private ImageView iv_nickname_btn;
    private ImageView family_name_arr;
    private RelativeLayout changeNikename;      //修改昵称

    private PopupWindow mPopupWindow;
    private Boolean isCreated = false;
    private Boolean isComeFromMessage = false;  //如果是从消息窗口启动，不显示聊天框。

    private int creatorId;                  //创建者id
    private boolean isCreator;              //自己是否是创建者
    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";
    private Bitmap photo;
    private String img_path;
    private UpImgUtil upImgUtil;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {

            logger.d("groupmgr#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
            if (imService == null) {
                Toast.makeText(FamilyDetailsActivity.this,
                        getResources().getString(R.string.im_service_disconnected), Toast.LENGTH_SHORT).show();
                return;
            }
            peerEntity = imService.getSessionManager().findPeerEntity(curSessionKey);
            String LoginId = IMLoginManager.instance().getLoginId() + "";
            if (peerEntity == null) {
                Toast.makeText(getApplicationContext(), "该群不存在或您已不再该群中", Toast.LENGTH_LONG).show();
                return;
            }
            creatorId = peerEntity.getCreatorId();

            if (Integer.valueOf(LoginId) != creatorId) {
                hideView();
                isCreator = false;
            } else {
                isCreator = true;
            }
            initPopupWindow();
            initAdapter();
        }
    };

    private void hideView() {
        family_name_arr.setVisibility(View.GONE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);
        chatView = View.inflate(FamilyDetailsActivity.this, R.layout.activity_family_details, topContentView);
        //根据seession 截取拿到 groupid
        curSessionKey = this.getIntent().getStringExtra(IntentConstant.SESSION_KEY);

        String comeFrom = getIntent().getStringExtra(IntentConstant.FAMILY_INFO_START_FROM);
        isComeFromMessage = comeFrom.equals("message");

        String session[] = curSessionKey.split("_");
        groupId = session[1];
        inRes();
    }

    /**
     * 初始化PopupWindow
     */
    private void initPopupWindow() {
        View contentView = View.inflate(this, R.layout.pop_drama, null);
        TextView et_exit = (TextView) contentView.findViewById(R.id.tv_exit);
        if (isCreator) {
            et_exit.setText("解散家族");
        } else {
            et_exit.setText("退出家族");
        }
        et_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo 退群操作
                String alertContent;
                if (isCreator) {
                    alertContent = FamilyDetailsActivity.this.getResources().getString(R.string.dialog_family_dismiss_content_1);
                } else {
                    alertContent = FamilyDetailsActivity.this.getResources().getString(R.string.dialog_family_exit_content);
                }
                new GGDialog().showTwoSelcetDialog(FamilyDetailsActivity.this,
                        (String) getResources().getText(R.string.dialog_normal_title),
                        alertContent,
                        new GGDialog.OnDialogButtonClickedListenered() {
                            @Override
                            public void onConfirmClicked() {
                                //TODO 需要判断是否需要解散群，如果是管理员退群，就解散
                                Integer userId = IMLoginManager.instance().getLoginId();

                                imService.getGroupManager()
                                        .quitOrDismissGroup(peerEntity.getPeerId(),
                                                userId.equals(peerEntity.getCreatorId()));

                                Toast.makeText(FamilyDetailsActivity.this,
                                        R.string.dialog_family_dismiss_action,
                                        Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onCancelClicked() {

                            }
                        });
                mPopupWindow.dismiss();
            }
        });

        TextView et_report = (TextView) contentView.findViewById(R.id.popSet);
        et_report.setText("举报");

        mPopupWindow = new PopupWindow(contentView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setRightButton(R.drawable.icon_more);
        topRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.showAsDropDown(topRightBtn);
            }
        });
    }

    /**
     * 初始化资源文件
     */
    private void inRes() {
        setLeftButton(R.drawable.back_default_btn);
        familyInfoImg = (CircularImage) chatView.findViewById(R.id.familyInfoImg);
        persionNikename = (TextView) chatView.findViewById(R.id.persionNikename);
        familyMemberNum = (TextView) chatView.findViewById(R.id.familyMemberNum);
        familyUser = (GroupManagerGridView) chatView.findViewById(R.id.familyUser);
        familyName = (TextView) chatView.findViewById(R.id.familyName);
        familyId = (TextView) chatView.findViewById(R.id.familyId);
        ownernickname = (TextView) chatView.findViewById(R.id.ownernickname);
        familyIntro = (TextView) chatView.findViewById(R.id.familyIntro);
        messageBtn = (LinearLayout) chatView.findViewById(R.id.messageCommBtn);
        iv_nickname_btn = (ImageView) chatView.findViewById(R.id.iv_nickname_btn);
        family_name_arr = (ImageView) chatView.findViewById(R.id.family_name_arr);
        changeNikename = (RelativeLayout) chatView.findViewById(R.id.changeNikename);

        conn = (TextView) chatView.findViewById(R.id.connn);

        if (isComeFromMessage) {
            messageBtn.setVisibility(View.INVISIBLE);
        }

        messageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //启动聊天界面

                IMUIHelper.openFamilyChatActivity(FamilyDetailsActivity.this, curSessionKey);
            }
        });
        changeNikename.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FamilyDetailsActivity.this, ChangeNickNameInFamilyActivity.class);
                i.putExtra("groupid", groupId);
                startActivityForResult(i, 101);
            }
        });

        chatView.findViewById(R.id.rl_message_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openGroupMessageSettingActivity(FamilyDetailsActivity.this, "2_" + groupId);
            }
        });

    }


    //弹出更换头像dialog
    private void showHeadimgDialog() {
        new GGDialog().showImgSelcetDialog(FamilyDetailsActivity.this, "更换家族头像", "添加图片", new GGDialog.OnDialogButtonClickedListenered() {
            @Override
            public void onConfirmClicked() {
                try {
                    Intent intent = new Intent(Intent.ACTION_PICK, null);
                    intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                    startActivityForResult(intent, PHOTOZOOM);
                } catch (ActivityNotFoundException e) {
                    T.show(getApplicationContext(), e.toString(), 0);
                    logger.e(e.toString());
                }
            }

            @Override
            public void onCancelClicked() {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "temp.jpg")));
                startActivityForResult(intent, PHOTOHRAPH);
            }
        });
    }


    /**
     * 回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE)
            return;
        if(resultCode == RESULT_OK){
            if(data ==null) return;
            if(requestCode == 101){
                //修改家族昵称
                persionNikename.setText(data.getStringExtra(IntentConstant.FAMILY_CHANGE_RESULT));
                return;
            }else if(requestCode == 102){
                //修改家族名称
                familyName.setText(data.getStringExtra(IntentConstant.FAMILY_CHANGE_RESULT));
                return;
            }else if(requestCode == 103){
                //修改家族简介
                familyIntro.setText(data.getStringExtra(IntentConstant.FAMILY_CHANGE_RESULT));
                return;
            }
        }


        // 拍照
        if (requestCode == PHOTOHRAPH) {
            //设置文件保存路径这里放在跟目录下
            File picture = new File(Environment.getExternalStorageDirectory() + "/temp.jpg");
            startPhotoZoom(Uri.fromFile(picture));
        }

        if (data == null)
            return;

        // 读取相册缩放图片
        if (requestCode == PHOTOZOOM) {
            startPhotoZoom(data.getData());
        }
        // 处理结果
        if (requestCode == PHOTORESOULT) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                photo = extras.getParcelable("data");
                updataFamilyImg();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 进行裁剪
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 200);
        intent.putExtra("outputY", 200);
        //设置缩放
        //add by guoq-s
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        //add by guoq-e

        intent.putExtra("return-data", true);
        startActivityForResult(intent, PHOTORESOULT);
    }

    /**
     * 更新家族头像
     */
    private void updataFamilyImg() {
        /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
        Date date = new Date();
        /** 获取当前时间并且进一步转化为字符串 */
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
        img_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

        upImgUtil = new UpImgUtil();
        upImgUtil.setFilePath(img_path, img_path);
        upImgUtil.saveHeadImg(photo);
        upImgUtil.upLoadPicture(new HeadImgListener() {
            public void notifyImgUploadFinished(String url) {

                //add by guoq-s
                if (url == null) {
                    String notifyText = (String) getResources()
                            .getText(R.string.create_play_upload_img_fail);
                    Toast.makeText(FamilyDetailsActivity.this, notifyText, Toast.LENGTH_SHORT)
                            .show();
                    return;
                }
                //add by guoq-e

                String uid = IMLoginManager.instance().getLoginId() + "";
                /** 放在params里面传递 */
                OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
                params.addParams("uid", uid);
                params.addParams("familyid", groupId);
                params.addParams("portrait", url);
                params.addParams("token", "1");

                logger.e(url);
                OkHttpClientManager.postAsy(ConstantValues.ModifyFamilyInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                    /** 请求成功后返回的Json */
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            String id = object.getString("result");
                            if (id.equals("1")) {
                                showToast("修改成功");
                                familyInfoImg.setImageBitmap(photo);
                            } else {
                                showToast("修改失败");
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                    }
                });
            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * 初始化 gropRes
     */
    private void initAdapter() {
        gridView = (GridView) chatView.findViewById(R.id.familyUser);
        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));// 去掉点击时的黄色背影
        gridView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));

//        adapter = new FamilyUserAdapter(FamilyDetailsActivity.this, imService, peerEntity,groupid,memberslist);
        adapter = new FamilyGroupAdapter(FamilyDetailsActivity.this,
                imService,
                memberslist, groupId, creatorId, isCreated);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItem(position) != null) {
                    Members members = (Members) adapter.getItem(position);
                    IMUIHelper.openUserHomePageActivity(FamilyDetailsActivity.this, members.getId());
                }
            }
        });
        getGroupDetail();
    }

    /**
     * 获取家族的信息
     */
    private void getGroupDetail() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("familyid", groupId);

        OkHttpClientManager.postAsy(ConstantValues.GetFamilyInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                memberslist.clear();
                FamilyDataJsonBean bean = null;
                try {
                    bean = new Gson()
                            .fromJson(response, FamilyDataJsonBean.class);

                    if (bean.getResult().equals("1")) {
                        GroupData d = bean.getFamilyinfo();

                        // TODO 获得审核群的id
                        String shenheId = d.getJudgegroupid();
                        String createId = d.getOwnerid();
                        persionNikename.setText(d.getNickname());
                        familyName.setText(d.getName());
                        familyId.setText(d.getId());
                        ownernickname.setText(d.getOwnernickname());
                        familyIntro.setText(d.getIntro());
                        creatorId = Integer.valueOf(createId);

                        setTitle(d.getName());
                        String portrait = d.getPortrait();
                        ImageLoaderUtil.getImageLoaderInstance().displayImage(portrait, familyInfoImg, ImageLoaderUtil.getOptions(R.drawable.default_family_avatar_btn));
                        memberslist = d.getMembers();
                        //家族数量
                        familyMemberNum.setText(memberslist.size() + "人");
                        //判断是不是管理员
                        if (createId.equals(IMLoginManager.instance().getLoginId() + "")) {
                            isCreated = true;
                            chatView.findViewById(R.id.rl_family_setting)
                                    .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent i = new Intent(
                                                    FamilyDetailsActivity.this,
                                                    FamilyNameSettingActivity.class);
                                            i.putExtra("groupid", groupId);
                                            startActivityForResult(i,102);

                                        }
                                    });
                            chatView.findViewById(R.id.rlfamilyIntro)
                                    .setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent i = new Intent(
                                                    FamilyDetailsActivity.this,
                                                    FamilyIntroSettingActivity.class);
                                            i.putExtra("groupid", groupId);
                                            startActivityForResult(i,103);
                                        }
                                    });

                            familyInfoImg.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    showHeadimgDialog();
                                }
                            });
                        } else {
                            isCreated = false;
                        }
                        adapter.setMemberslist(memberslist, isCreated);
                    }
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }


    /**
     * 事件驱动通知
     */
    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {

            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                Toast.makeText(this, getString(R.string.change_temp_group_failed), Toast.LENGTH_SHORT).show();
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
                onMemberChangeSuccess(event);
            }
            break;
        }
    }

    private void onMemberChangeSuccess(GroupEvent event) {
        int groupId = event.getGroupEntity().getPeerId();
        if (groupId != peerEntity.getPeerId()) {
            return;
        }
        List<Integer> changeList = event.getChangeList();
        if (changeList == null || changeList.size() <= 0) {
            return;
        }
        int changeType = event.getChangeType();

        switch (changeType) {
            case DBConstant.GROUP_MODIFY_TYPE_ADD:
                getGroupDetail();
                break;
            case DBConstant.GROUP_MODIFY_TYPE_DEL:
                //add by guoq-s
                Integer loginId = IMLoginManager.instance().getLoginId();
                if (changeList.contains(loginId)) {
                    //退出群的场合。
                    setResult(RESULT_OK);
                    finish();
                    break;
                }
                //add by guoq-e
                for (Integer userId : changeList) {
                    memberslist.remove(userId);

                }
                adapter.setMemberslist(memberslist, isCreated);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(FamilyDetailsActivity.this);
    }
}
