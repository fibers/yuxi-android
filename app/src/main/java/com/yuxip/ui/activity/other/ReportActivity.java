package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;


/**
 * Created by yanchangsen on 15/5/17.
 * description ：举报
 */
public class ReportActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(ReportActivity.class);

    private CheckBox sexCheckBox;
    private CheckBox abuseCheckBox;
    private CheckBox adCheckBox;
    private CheckBox otherCheckBox;
    private CheckBox polityCheckBox;
    private CheckBox trickCheckBox;
    private String id;
    private String reason = "";
    private String loginId;

    private String report_style;
    private String report_url = "";
    private int reason_type = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        report_style = getIntent().getStringExtra(IntentConstant.REPORT_TYPE);
        getReportId();
        loginId = IMLoginManager.instance().getLoginId() + "";
        initView();
    }


    private void getReportId() {
        if (report_style.equals(IntentConstant.PERSION_ID)) {
            id = getIntent().getStringExtra(IntentConstant.PERSION_ID);
            report_url = ConstantValues.ReportPerson;
        } else if (report_style.equals(IntentConstant.STORY_ID)) {
            id = getIntent().getStringExtra(IntentConstant.STORY_ID);
            report_url = ConstantValues.ReportStory;
        } else if (report_style.equals(IntentConstant.FAMILY_ID)) {
            id = getIntent().getStringExtra(IntentConstant.FAMILY_ID);
            report_url = ConstantValues.ReportFamily;
        } else if (report_style.equals(IntentConstant.TOPIC_ID)) {
            id = getIntent().getStringExtra(IntentConstant.STORY_ID);
            report_url = ConstantValues.ReportStory;
        } else {
            id = getIntent().getStringExtra(IntentConstant.STORY_ID);
            report_url = ConstantValues.ReportStory;
        }

    }

    private void initView() {
        LayoutInflater.from(this).inflate(R.layout.activity_rules, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("完成");
        setTitle("规则");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 发布
                if (reason.equals("") || reason_type == -1) {
                    T.showShort(getApplicationContext(), "尚未选择举报类型");
                    return;
                }
                OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
                params.addParams("uid", loginId);
                if (report_style.equals(IntentConstant.PERSION_ID)) {
                    params.addParams("oid", id);
                    params.addParams("reason", reason_type + "");
                    params.addParams("token", "111");
                } else if (report_style.equals(IntentConstant.STORY_ID)) {
                    params.addParams("id", id);
                    params.addParams("reason", reason);
                } else if (report_style.equals(IntentConstant.FAMILY_ID)) {
                    params.addParams("id", id);
                    params.addParams("reason", reason_type + "");
                } else {
                    params.addParams("id", id);
                    params.addParams("reason", reason);
                }
                OkHttpClientManager.postAsy(report_url, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                    /** 请求成功后返回的Json */
                    @Override
                    public void onResponse(String response) {
                        try {
                            Message msg = new Message();
                            JSONObject object = new JSONObject(response);
                            if (object.getString("result").equals("1")) {
                                showToast("举报成功");
                                ReportActivity.this.finish();
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(ReportActivity.this, R.string.network_err, 1);

                    }
                });
            }
        });
        sexCheckBox = (CheckBox) findViewById(R.id.cb_sex); //seqing
        abuseCheckBox = (CheckBox) findViewById(R.id.cb_abuse); //诅咒谩骂
        adCheckBox = (CheckBox) findViewById(R.id.cb_ad); //广告骚扰
        otherCheckBox = (CheckBox) findViewById(R.id.cb_other); //qita
        polityCheckBox = (CheckBox) findViewById(R.id.cb_polity); //政治
        trickCheckBox = (CheckBox) findViewById(R.id.cb_trick); //欺诈骗钱
        sexCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (sexCheckBox.isChecked()) {
                    reason = "色情";
                    reason_type = 1;
                    abuseCheckBox.setChecked(false);
                    adCheckBox.setChecked(false);
                    otherCheckBox.setChecked(false);
                    polityCheckBox.setChecked(false);
                    trickCheckBox.setChecked(false);
                }
            }
        });
        abuseCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (abuseCheckBox.isChecked()) {
                    reason = "诅咒谩骂";
                    reason_type = 3;
                    sexCheckBox.setChecked(false);
                    adCheckBox.setChecked(false);
                    otherCheckBox.setChecked(false);
                    polityCheckBox.setChecked(false);
                    trickCheckBox.setChecked(false);
                }
            }
        });
        adCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (adCheckBox.isChecked()) {
                    reason = "广告骚扰";
                    reason_type = 4;
                    sexCheckBox.setChecked(false);
                    abuseCheckBox.setChecked(false);
                    otherCheckBox.setChecked(false);
                    polityCheckBox.setChecked(false);
                    trickCheckBox.setChecked(false);
                }
            }
        });
        otherCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (otherCheckBox.isChecked()) {
                    reason = "其他";
                    reason_type = 6;
                    sexCheckBox.setChecked(false);
                    abuseCheckBox.setChecked(false);
                    adCheckBox.setChecked(false);
                    polityCheckBox.setChecked(false);
                    trickCheckBox.setChecked(false);
                }
            }
        });
        polityCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (polityCheckBox.isChecked()) {
                    reason = "政治";
                    reason_type = 5;
                    sexCheckBox.setChecked(false);
                    abuseCheckBox.setChecked(false);
                    adCheckBox.setChecked(false);
                    otherCheckBox.setChecked(false);
                    trickCheckBox.setChecked(false);
                }
            }
        });
        trickCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (trickCheckBox.isChecked()) {
                    reason = "欺诈骗钱";
                    reason_type = 2;
                    sexCheckBox.setChecked(false);
                    abuseCheckBox.setChecked(false);
                    adCheckBox.setChecked(false);
                    otherCheckBox.setChecked(false);
                    polityCheckBox.setChecked(false);
                }
            }
        });
    }

}
