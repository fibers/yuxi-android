package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.GroupsEntity;
import com.yuxip.entity.MemberEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.manager.DataManager;
import com.yuxip.imservice.manager.IMContactManager;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMSessionManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXGroupTypeManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * add by SummerRC on 2015/6/1.
 * 指定剧显示我参与的剧群的列表
 */
public class StoryGroupSelectActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(StoryGroupSelectActivity.class);

    private String storyId;                         //剧的id
    private ListView lv_groups;                     //我的剧点击入戏时某个居对应的剧群的列表ListView
    private ArrayList<GroupsEntity> arr_groups;     //我的剧点击入戏时某个居对应的剧群的列表的数据源
    private ProgressBar progressBar;

    private String shenHeId = "@";
    private String shenHeTitle = "@";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        shenHeId = getIntent().getStringExtra(IntentConstant.ShenHeGroupId);
        shenHeTitle = getIntent().getStringExtra(IntentConstant.ShenHeGroupTitle);

        LayoutInflater.from(this).inflate(R.layout.activity_story_group, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("我的剧群");

        lv_groups = (ListView) findViewById(R.id.storyGroupList);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        getEntersStoryGroups();
    }

    /**
     * 获得当前用户指定剧的已加入的群
     */
    public void getEntersStoryGroups() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyId);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetEnteredStoryGroups, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                arr_groups = new ArrayList<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String result = object.getString("result");
                    if (result.equals("1")) {
                        JSONArray array = object.getJSONArray("groups");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            GroupsEntity entity = new GroupsEntity();
                            entity.setGroupId(obj.getString("groupid"));
                            entity.setTitle(obj.getString("title"));
                            entity.setIsPlay(obj.getString("isplay"));
                            if (!entity.getIsPlay().equals("3")) {  //排除评论群
                                arr_groups.add(entity);
                            }
                        }
                    }
                    lv_groups.setAdapter(new GroupListAdapter(arr_groups));
                    lv_groups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String groupId = arr_groups.get(position).getGroupId();
                            getStoryGroupInfo(groupId);
                        }
                    });
                    if (arr_groups.size() == 0) {       //说明用户不属于该剧
                        initEmptyView();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    /**
     * 当前用户指定剧的已加入的群的列表的适配器
     */
    private class GroupListAdapter extends BaseAdapter {
        private ArrayList<GroupsEntity> list;

        private GroupListAdapter(ArrayList<GroupsEntity> list) {
            this.list = list;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView == null) {
                holder = new ViewHolder();
                convertView =  View.inflate(StoryGroupSelectActivity.this, R.layout.story_group_count_item, null);
                holder.tv_title = (TextView) convertView.findViewById(R.id.groupType);
                holder.tv_unread_count = (TextView) convertView.findViewById(R.id.tv_unread_count);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            holder.tv_title.setText(list.get(position).getTitle() + "");
            /** 遍历所有的最近会话，取出当前群的未读消息数 */
            int unreadCount = 0;
            for (RecentInfo recentInfo : IMSessionManager.instance().getRecentListInfo()) {
                if (String.valueOf(recentInfo.getPeerId()).equals(list.get(position).getGroupId())) {
                    unreadCount += recentInfo.getUnReadCnt();
                }
            }
            holder.tv_unread_count.setText(unreadCount + "");
            return convertView;
        }

        private class ViewHolder {
            private TextView tv_title;          //显示剧群名称的TextView
            private TextView tv_unread_count;   //显示剧群名称的未读消息数
        }
    }

    /**
     * @return 聊天信息是否准备好了
     */
    private boolean isDataReady() {
        boolean isUserData = IMContactManager.instance().isUserDataReady();
        boolean isSessionData = IMSessionManager.instance().isSessionListReady();
        boolean isGroupData = IMGroupManager.instance().isGroupReady();
        return isUserData && isSessionData && isGroupData;
    }

    /**
     * 获取将要聊天的群成员信息用于显示角色名
     */
    private void getStoryGroupInfo(final String groupId) {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        params.addParams("groupid", groupId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                Map<String, MemberEntity> map = new HashMap<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String result = object.getString("result");
                    if (result.equals("1")) {
                        JSONObject object_groupInfo = object.getJSONObject("groupinfo");
                        JSONArray array = object_groupInfo.getJSONArray("members");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            MemberEntity entity = new MemberEntity();
                            entity.setId(obj.getString("id"));
                            entity.setNickname(obj.getString("nickname"));
                            entity.setRoleName(obj.getString("rolename"));
                            //这个是皮表的名称
                            entity.setTitle(obj.getString("title"));
                            entity.setPortrait(obj.getString("portrait"));
                            map.put(entity.getId(), entity);
                        }
                    }
                    DataManager.getInstance().putStoryGroupMemberMap(groupId, map);
                    progressBar.setVisibility(View.GONE);
                    if (isDataReady()) {
                        IMUIHelper.openDramaChatActivity(StoryGroupSelectActivity.this, "2_" + groupId, storyId);
                    } else {
                        T.show(getApplicationContext(), "数据错误请重试", 0);
                    }
                } catch (Exception e) {
                    progressBar.setVisibility(View.GONE);
                    T.show(StoryGroupSelectActivity.this, "网络失败请重试", 1);
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                progressBar.setVisibility(View.GONE);
                T.show(StoryGroupSelectActivity.this, "网络失败请重试", 1);
            }
        });
    }

    /**
     * 当ListView的数据源为空时，为其填充默认布局
     */
    private void initEmptyView() {
        T.show(getApplicationContext(), "成功进入该剧的审核群！", 0);
        View emptyView = View.inflate(StoryGroupSelectActivity.this, R.layout.story_group_count_item, null);
        TextView title = (TextView) emptyView.findViewById(R.id.groupType);
        title.setText(shenHeTitle);
        emptyView.findViewById(R.id.tv_unread_count).setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.GONE);
        emptyView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ((ViewGroup) lv_groups.getParent()).addView(emptyView);
        emptyView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDataReady()) {
                    Set<Integer> memberSet = new HashSet<>();
                    memberSet.add(IMLoginManager.instance().getLoginId());
                    IMGroupManager.instance().reqAddGroupMember(Integer.valueOf(shenHeId), memberSet);
                    YXGroupTypeManager.instance().setGroupType(Integer.valueOf(shenHeId), ConstantValues.GROUP_TYPE_SHENHE);
                    getStoryGroupInfo(shenHeId);
                }
            }
        });
        lv_groups.setEmptyView(emptyView);
    }
}
