package com.yuxip.ui.activity.square;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.SquareCommentAdapter;
import com.yuxip.ui.customview.CommentInputView;
import com.yuxip.ui.customview.CommentItemView;
import com.yuxip.ui.customview.SquareCommentHeadView;
import com.yuxip.ui.customview.SquareFootView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/10/9.
 * 主楼界面  包含 楼主  以及 个子楼层
 */
public class SquareCommentActivity extends TTBaseActivity {
    private SquareCommentAdapter adapter;
    private ListView listView;
    private SquareCommentHeadView mainAdapterHeadView;
    private CommentInputView commentInputView;
    private RelativeLayout relativeLayout;

    private EditText edit_content;
    private TextView tv_send;

    private int type;
    private String storyId;
    private String topicId;
    private String title;
    private String detailUrl;
    private String commentUrl;
    private int index;          //如果索引是0 则认为是第一页 1 是第二页  一次类推 如果当前页没有数据 怎返回实体类为 空  注意
    private boolean isFirstLoad = true;  //判断是否是第一次请求  因为可能有主楼 但没有内容 index无法判断
    private final int defaultCount = 15;
    private int count = 15;             //每页加载的数据量 与 index配合
    private boolean canLoad = false;    //判断是否可以继续加载更多
    private SquareFootView footView;
    private TextView tv_loadfail;       //footview中加载失败 可以点击重试
    private TextView tv_nomoredata;     //footview中无内容时 点击 加载可能更新出的内容
    private InputMethodManager inputMethodManager;

    private TopicDetailsJsonBean topicDetailsJsonBean;
    private TopicDetailsJsonBean.TopicDetailEntity topicDetailEntity;
    private List<TopicDetailsJsonBean.CommentEntity> list_hotComment = new ArrayList<>();
    private List<TopicDetailsJsonBean.CommentEntity> list_comment = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_square_comment, topContentView);
        initRes();
        EventBus.getDefault().register(this);
    }

    private void initRes() {
        setLeftButton(R.drawable.back_default_btn);
        title = getIntent().getStringExtra(IntentConstant.FLOOR_TITLE);
        if (SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)) {
            type = 0;
            detailUrl = ConstantValues.StoryDetail;
            commentUrl = ConstantValues.CommentStoryResource;
            storyId = getIntent().getStringExtra(IntentConstant.FLOOR_STORYID);
        } else {
            type = 1;
            detailUrl = ConstantValues.TopicDetail;
            commentUrl = ConstantValues.CommentSquareResource;
            topicId = getIntent().getStringExtra(IntentConstant.FLOOR_TOPICID);
        }
        if(!TextUtils.isEmpty(title)) {
            setTitle(title);
        }
        listView = (ListView) findViewById(R.id.listview_comment_main);
        relativeLayout = (RelativeLayout) findViewById(R.id.rel_comment_overlay);
        commentInputView = (CommentInputView) findViewById(R.id.commentinputview_main);
        footView = new SquareFootView(this);
        footView.showTopLine(false);
        footView.showLoadMore();
        inputMethodManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        adapter = new SquareCommentAdapter(this, list_hotComment, list_comment);

        mainAdapterHeadView = new SquareCommentHeadView(this);
        listView.addHeaderView(mainAdapterHeadView);
        listView.addFooterView(footView);
        listView.setAdapter(adapter);

        relativeLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                for (int i = 0; i < listView.getChildCount(); i++) {
                    if (listView.getChildAt(i) instanceof CommentItemView) {
                        CommentItemView commentItemView = (CommentItemView) listView.getChildAt(i);
                        commentItemView.hidePopComment();

                    }
                }
                return false;
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    loadMore();
                }
            }
        });

        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        setListener();
        RequestCommentList();

    }

    private void loadMore() {
        if (canLoad) {
            canLoad = false;
            index += 1;
            RequestCommentList();
        }
    }

    private void setListener() {
        tv_loadfail = (TextView) footView.getChildView(SquareFootView.ViewEnum.TEXTVIEW_LOADFAIL);
        tv_nomoredata = (TextView) footView.getChildView(SquareFootView.ViewEnum.TEXTVIEW_NOMOREDATA);
        edit_content = (EditText) commentInputView.getInputChildView(CommentInputView.InputViewEnum.EDIT_TEXT_CONTENT);
        tv_send = (TextView) commentInputView.getInputChildView(CommentInputView.InputViewEnum.TEXTVIEW_SEND);
        tv_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(edit_content.getText().toString())) {
                    //创建新楼层,如果楼层全部展开 请求数目为当前评论数+1   反之不去请求
                    commentSquareResource();
                    inputMethodManager.hideSoftInputFromWindow(edit_content.getWindowToken(), 0);
                    edit_content.setText("");
                } else {
                    Toast.makeText(getApplicationContext(), SquareCommentActivity.this.getResources().getString(R.string.square_input_nodata), Toast.LENGTH_LONG).show();
                }
            }
        });
        tv_loadfail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footView.showLoadMore();
                RequestCommentList();
            }
        });
        tv_nomoredata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list_comment != null) {
                    footView.showLoadMore();
                    if (list_comment.size() > 0) {
                        count = list_comment.size();
                        index = 1;
                    } else {
                        count = defaultCount;
                        index = 0;
                    }

                    RequestCommentList();
                }
            }
        });

    }


    public void onEventMainThread(SquareCommentEvent event) {
        switch (event.eventType) {
            case TYPE_RESET_CHILD_COMMENT:
                addChildComment(event.commentId, event.commentEntity);
                adapter.notifyDataSetChanged();
                break;
            case TYPE_REMOVE_COMMENT:
                removeComments(event.commentId);
                adapter.notifyDataSetChanged();
                break;
            case TYPE_RESET_COMMENT_CONTENT:
                resetCommentConent(event.commentId, event.commentEntity);
                resetCommentContent();
                break;
            case TYPE_DELETE_STORY_TOPIC:
                finish();
                break;
        }
    }

    /**
     * 重置 楼层的子楼层内容
     *
     * @param commentId
     * @param commentEntity
     */
    private void addChildComment(String commentId, TopicDetailsJsonBean.CommentEntity commentEntity) {
        if (list_hotComment != null && list_hotComment.size() > 0) {
            for (int i = 0; i < list_hotComment.size(); i++) {
                if (list_hotComment.get(i).getCommentID().equals(commentId)) {
                    list_hotComment.set(i, commentEntity);
                    break;
                }
            }
        }
        if (list_comment != null && list_comment.size() > 0) {
            for (int i = 0; i < list_comment.size(); i++) {
                if (list_comment.get(i).getCommentID().equals(commentId)) {
                    list_comment.set(i, commentEntity);
                    break;
                }
            }
        }

    }

    /**
     * 删除楼层
     */
    private void removeComments(String commentId) {
        for (int i = 0; i < list_hotComment.size(); i++) {
            if (list_hotComment.get(i).getCommentID().equals(commentId)) {
                list_hotComment.remove(i);
                break;
            }
        }
        for (int i = 0; i < list_comment.size(); i++) {
            if (list_comment.get(i).getCommentID().equals(commentId)) {
                list_comment.remove(i);
                break;
            }
        }
    }

    /**
     * 当热门和全部评论中某条内容发生改变时候    其相对应的ID 相同的内容也应该发生改变
     * 主要判断的内容有 是否点赞  是否收藏  点赞数目
     *
     * @param commentEntity
     */
    private void resetCommentConent(String commentId, TopicDetailsJsonBean.CommentEntity commentEntity) {
        for (int i = 0; i < list_hotComment.size(); i++) {
            if (list_hotComment.get(i).getCommentID().equals(commentId)) {
                list_hotComment.get(i).setIsFavor(commentEntity.getIsFavor());
                list_hotComment.get(i).setIsCollection(commentEntity.getIsCollection());
                list_hotComment.get(i).setFavorCount(commentEntity.getFavorCount());
                break;
            }
        }

        for (int i = 0; i < list_comment.size(); i++) {
            if (list_comment.get(i).getCommentID().equals(commentId)) {
                list_comment.get(i).setIsFavor(commentEntity.getIsFavor());
                list_comment.get(i).setIsCollection(commentEntity.getIsCollection());
                list_comment.get(i).setFavorCount(commentEntity.getFavorCount());
                break;
            }
        }
    }


    /**
     * 刷新每个 item的子视图 这里主要针对 点赞和收藏 楼层  不会去重绘listview
     */
    private void resetCommentContent() {
        for (int i = 0; i < listView.getChildCount(); i++) {
            if (listView.getChildAt(i) instanceof CommentItemView) {
                ((CommentItemView) listView.getChildAt(i)).notifyMainItemChanged();
            }
        }
    }

    private void initHotList(List<TopicDetailsJsonBean.HotCommentEntity> list) {
        list_hotComment.clear();
        for (int i = 0; i < list.size(); i++) {
            list_hotComment.add(list.get(i));
        }
    }


    private void RequestCommentList() {

        if ((type == 0 && storyId == null) || (type == 1 && topicId == null)) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_topic_not_exists), Toast.LENGTH_LONG).show();
            footView.showLoadFail();
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("token", "110");
        if (type == 0) {
            params.addParams("storyID", storyId);
        } else {
            params.addParams("topicId", topicId);
        }
        params.addParams("index", index + "");
        params.addParams("count", count + "");
        OkHttpClientManager.postAsy(detailUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        footView.showLoadFail();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }


    private void onReceiveData(String response) {
        if (isFirstLoad) {
            isFirstLoad = false;
            try {
                topicDetailsJsonBean = new Gson().fromJson(response, TopicDetailsJsonBean.class);
            } catch (Exception e) {
                isFirstLoad = true;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    Toast.makeText(getApplicationContext(), jsonObject.getString("describe"), Toast.LENGTH_LONG).show();
                } catch (Exception e1) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_topic_not_exists), Toast.LENGTH_LONG).show();
                    footView.showLoadFail();
                    e1.printStackTrace();
                    return;
                }
                footView.showLoadFail();
                return;
            }
            if (topicDetailsJsonBean != null) {
                SquareCommentManager.getInstance().setLzId(topicDetailsJsonBean.getTopicDetail().getCreator().getId());
                firstLoadData();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_loading_fail), Toast.LENGTH_LONG).show();
            }
        } else {
            TopicDetailsJsonBean topicDetailsJsonBean = null;
            try {
                topicDetailsJsonBean = new Gson().fromJson(response, TopicDetailsJsonBean.class);
            } catch (Exception e) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_parse_error), Toast.LENGTH_LONG).show();
                footView.showLoadFail();
                return;
            }

            if (topicDetailsJsonBean != null && topicDetailsJsonBean.getComment() != null) {
                if (topicDetailsJsonBean.getComment().size() == 0) {
                    footView.showNoMoreData();
                    mainAdapterHeadView.resetDate();
                    canLoad = false;
                } else if (topicDetailsJsonBean.getComment().size() < count) {
                    list_comment.addAll(topicDetailsJsonBean.getComment());
                    footView.showNoMoreData();
                    adapter.notifyDataSetChanged();
                    mainAdapterHeadView.resetDate();
                    canLoad = false;
                } else {
                    list_comment.addAll(topicDetailsJsonBean.getComment());
                    footView.showLoadMore();
                    adapter.notifyDataSetChanged();
                    mainAdapterHeadView.resetDate();
                    canLoad = true;
                }

            }

        }
    }


    /**
     * 首次进入获取数据
     */
    private void firstLoadData() {
        topicDetailEntity = topicDetailsJsonBean.getTopicDetail();
        // 这里 存储 当前 楼主的id
        if (topicDetailsJsonBean.getHotComment() != null && topicDetailsJsonBean.getComment() != null) {
            initHotList(topicDetailsJsonBean.getHotComment());
            list_comment = topicDetailsJsonBean.getComment();
            adapter.setListHotComment(list_hotComment);
            adapter.setListComment(list_comment);
            adapter.notifyDataSetChanged();
            if (list_comment.size() < count) {
                footView.showNoMoreData();
                canLoad = false;
            } else {
                footView.showLoadMore();
                canLoad = true;
            }
        } else {
            canLoad = false;
        }

//      填充 listview的头布局
        mainAdapterHeadView.setData(topicDetailsJsonBean.getTopicDetail());
        commentInputView.setData(topicDetailEntity.getTopicID(), topicDetailEntity.getFavorCount(), topicDetailEntity.getCollectionCount(), topicDetailEntity.getCommentCount(),
                topicDetailEntity.getIsFavor(), topicDetailEntity.getIsCollection());
    }


    private void commentSquareResource() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("token", "110");
        if (type == 0) {
            params.addParams("resourceID", storyId);
        } else {
            params.addParams("resourceID", topicId);
        }
        params.addParams("resourceType", "0");
        params.addParams("toUserID", SquareCommentManager.getInstance().getLzId());
        params.addParams("content", edit_content.getText().toString());
        OkHttpClientManager.postAsy(commentUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {

                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.optString("result", "").equals("100")) {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_comment_success), Toast.LENGTH_LONG).show();
                                if (!canLoad) {     //如果已经没有更多 则去刷新界面 反之不刷新 只提示 评论成功
                                    if (list_comment != null && list_comment.size() > 0) {
                                        count = list_comment.size();
                                        index = 1;
                                    } else {
                                        index = 0;
                                    }

                                    RequestCommentList();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), getResources().getString(R.string.square_comment_failed), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }


    @Override
    protected void onDestroy() {
        SquareCommentManager.getInstance().clearList();
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}


