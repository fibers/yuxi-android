package com.yuxip.ui.activity.story;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.JsonBean.BookDetailResult;
import com.yuxip.JsonBean.DeleteStoryResult;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.customview.SaveImagePopupWindow;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.ShareUtil;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by zl on 2015/7/13.
 * description:自戏的详情界面
 */
public class ZiXiDetailsActivity extends TTBaseNewActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(ZiXiDetailsActivity.class);

    @InjectView(R.id.topicBlack)
    ImageView topicBlack;
    @InjectView(R.id.iv_book_head_img)
    CustomHeadImage ivBookHeadImg;
    @InjectView(R.id.tv_top_topic_create)
    TextView tvTopTopicCreate;
    @InjectView(R.id.tv_topic_time)
    TextView tvTopicTime;
    @InjectView(R.id.iv_top_fave)
    ImageView ivTopFave;
    @InjectView(R.id.tv_fave_num)
    TextView tvFaveNum;
    @InjectView(R.id.img_more)
    ImageView imgMore;
    @InjectView(R.id.tv_book_title)
    TextView tvBookTitle;
    @InjectView(R.id.iv_content_img)
    ImageView ivContentImg;
    @InjectView(R.id.tv_content)
    TextView tvContent;
    @InjectView(R.id.tv_comment)
    TextView tv_comment;
    @InjectView(R.id.tv_book_textnum)
    TextView tvBookNum;
    private String createid = "0";
    private String storyid;
    private String loginId;
    private int behavior;
    private int behaviorTemp;
    private BookPopupWindow popupWindow;
    private String content;
    private String bookTitle;
    private String storyimg;
    private int commentGroupId;
    private BookDetailResult.DetatilsEntity entity;
    private final int START_FLG_SET = 1;
    private final int START_FLG_NO_SET = 0;
    private int startFlg;
    private float density;
    private ImageLoader imageLoader;
    private boolean isCollect;
    private boolean isCollectTemp;
    private SaveImagePopupWindow saveImagePopupWindow;
    private ImageMessage img = new ImageMessage();
    private ArrayList<ImageMessage> list_mess = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        density = getResources().getDisplayMetrics().density;
        EventBus.getDefault().register(this);
        setContentView(R.layout.activity_hisbook_content);
        ButterKnife.inject(this);
        initPopWindow();
        initExtra();
        GetSelfStoryDetail();
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    /**
     * ************
     * 初始化保存或预览图片的Pop
     * *****************
     */
    private void initPopWindow() {
        saveImagePopupWindow = new SaveImagePopupWindow(this);
    }

    private void initExtra() {
        storyid = getIntent().getStringExtra(IntentConstant.STORY_ID);
        if (!TextUtils.isEmpty(getIntent().getStringExtra(IntentConstant.CREATOR_ID))) {
            createid = getIntent().getStringExtra(IntentConstant.CREATOR_ID);
        }
        loginId = IMLoginManager.instance().getLoginId() + "";
        imgMore.setVisibility(View.VISIBLE);
        imgMore.setOnClickListener(this);
        ivContentImg.setOnClickListener(this);
        popupWindow = new BookPopupWindow(this, createid);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                imgMore.setSelected(false);
            }
        });
        topicBlack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        tv_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO: 评论聊天的入口
                if(entity!=null)
                IMUIHelper.openSquareCommentActivity(ZiXiDetailsActivity.this,entity.getTitle(),entity.getId(),IntentConstant.FLOOR_TYPE_STORY);
//                if (commentGroupId == 0) {
//                    Toast.makeText(ZiXiDetailsActivity.this, "该剧岁数比较大，不能评论。谢谢", Toast.LENGTH_SHORT).show();
//                    return;
//                }
//                startFlg = START_FLG_SET;
//                GroupEntity groupEntity = IMGroupManager.instance().findGroup(commentGroupId);
//                if (groupEntity == null) {
//                    IMGroupManager.instance().reqGroupDetailInfo(commentGroupId);
//                    groupEntity = IMGroupManager.instance().findGroup(commentGroupId);
//                    if (groupEntity != null) {
//                        checkExistAndStartChat(groupEntity);
//                    }
//                    T.show(getApplicationContext(), "暂时不能评论,再点一下就可以啦~！", 0);
//                } else {
//                    checkExistAndStartChat(groupEntity);
//                }
            }
        });
    }


    /**
     * **************
     * 点赞
     * *******************
     */
    private void requestPraise() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyid);
        params.addParams("behavior", behavior + "");
        OkHttpClientManager.postAsy(ConstantValues.STORY_PRAISE, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    String result = jsonObject.getString("result");
                    if ("1".equals(result)) { //成功
                        String num = "0";
                        if (behavior == 1) { //点赞
                            tvFaveNum.setText(num = ((Integer.parseInt(entity.getPraisenum()) + 1) + ""));
                            entity.setIspraisedbyuser("1");
                            ivTopFave.setSelected(true);
                        } else {
//                                    if (entity.getPraisenum().equals("0")) {
//                                        return;
//                                    }
                            tvFaveNum.setText(num = ((Integer.parseInt(entity.getPraisenum()) - 1) + ""));
                            entity.setIspraisedbyuser("0");
                            ivTopFave.setSelected(false);
//                                    }
                        }
                        entity.setPraisenum(num);
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    /**
     * 检查是否是成员，如果不是，需要先添加到群里。
     *
     * @param groupEntity
     */
    private void checkExistAndStartChat(GroupEntity groupEntity) {
        String userList = groupEntity.getUserList();
        String userid = IMLoginManager.instance().getLoginId() + "";
        if (userList.contains(userid)) {
            startCommentGroupChat();
        } else {
            Set<Integer> set = new HashSet<Integer>();
            set.add(IMLoginManager.instance().getLoginId());
            IMGroupManager.instance().reqAddGroupMember(commentGroupId, set);
        }
    }

    /**
     * 开始聊天。
     */
    private void startCommentGroupChat() {
        startFlg = START_FLG_NO_SET;
        String sessionKey, portrait, title, creatorName, createTime, type;
        sessionKey = "2_" + entity.getCommentgroupid();
        portrait = entity.getPortrait();
        title = entity.getTitle();
        createTime = entity.getCreatetime();
        creatorName = entity.getCreatorname();
        type = IntentConstant.SELF_STORY;
        IMUIHelper.openTopicMessageActivity(this, type, sessionKey, portrait, title, creatorName, createTime);
    }

    /**
     * ****************
     * 获取自戏详情
     * ********************
     */
    private void GetSelfStoryDetail() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetSelfStoryDetail, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveBookDetail(response);
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }

    /**
     * *****************
     * 填充数据
     * ******************
     */
    private void onReceiveBookDetail(String result) {
        try {
            BookDetailResult bookData = new Gson().fromJson(result, BookDetailResult.class);
            if (bookData.getResult().equals("1")) {
                entity = bookData.getDetatils();
                content = entity.getContent();
                tvTopTopicCreate.setText(entity.getCreatorname() + getString(R.string.de_zi_xi));
                tvTopicTime.setText(DateUtil.getSessionTime(Integer.parseInt(entity.getCreatetime())));
                bookTitle = entity.getTitle();
                if (TextUtils.isEmpty(entity.getPraisenum())) {
                    entity.setPraisenum("0");
                }
                tvFaveNum.setText(entity.getPraisenum());
                tvBookTitle.setText(bookTitle);
                tvContent.setText(content);
                tvBookNum.setText("创作字数: " + entity.getWordcount());
                storyimg = entity.getStoryimg();
                commentGroupId = Integer.valueOf(entity.getCommentgroupid());
                //是否收藏过
                behavior = behaviorTemp = Integer.parseInt(entity.getIspraisedbyuser());
                if (entity.getIspraisedbyuser().equals("1")) {
                    ivTopFave.setSelected(true);
                } else {
                    ivTopFave.setSelected(false);
                }
                ivBookHeadImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IMUIHelper.openUserHomePageActivity(ZiXiDetailsActivity.this, entity.getCreatorid());
                    }
                });
                if (entity.getIsCollectedByUser().equals("0")) {
                    isCollectTemp = isCollect = false;
                } else if (entity.getIsCollectedByUser().equals("1")) {
                    isCollectTemp = isCollect = true;
                }
                //判断有没有图
                if (storyimg.equals("")) {
                    ivContentImg.setVisibility(View.GONE);
                } else {
                    ivContentImg.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(storyimg, ivContentImg);
                }
                ivBookHeadImg.setVipSize(12f);
                ivBookHeadImg.loadImage(entity.getPortrait(), "0");
                ivTopFave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (entity != null) {
                            if (entity.getIspraisedbyuser().equals("1")) {
                                behavior = 0;
                            } else {
                                behavior = 1;
                            }
                            requestPraise();
                        }
                    }
                });
            } else {
                if (TextUtils.isEmpty(bookData.getDetatils().getId())) {
                    Toast.makeText(getApplicationContext(), "该自戏不存在或已删除", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "获取信息失败,请稍后重试", Toast.LENGTH_LONG).show();
                }
                return;
            }
            img.setUrl(storyimg);
            list_mess.add(img);
            saveImagePopupWindow.setList(list_mess);
        }catch (Exception e){
            Toast.makeText(getApplicationContext(), "获取信息失败,请稍后重试", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_more:
                if (imgMore.isSelected()) {
                    imgMore.setSelected(false);
                } else {
                    imgMore.setSelected(true);
                }
                popupWindow.show(imgMore);
                break;
            case R.id.iv_content_img:
                saveImagePopupWindow.showPop(ivContentImg, img);
                break;
        }
    }

    /**
     * ******************
     * 自戏右上角点击弹出的Pop  包含分享 举报 收藏 编辑 与 删除
     * ***********************
     */
    class BookPopupWindow implements View.OnClickListener {
        @InjectView(R.id.tv_share)
        TextView tvShare;   //分享
        @InjectView(R.id.tv_delete)
        TextView tvDelect;  //删除
        @InjectView(R.id.tv_report)
        TextView tvReport;  //举报 或 编辑 根据isMine 状态判断...
        @InjectView(R.id.linear_hisbook_pop)
        LinearLayout linear;
        @InjectView(R.id.img_msg_pop_bg)
        ImageView imgBg;
        @InjectView(R.id.tv_collect_hisbook)
        TextView tvCollect;   //收藏
        @InjectView(R.id.view_del1)
        View viewLine;
        private PopupWindow pop;
        private Context context;
        private boolean isMine;
        private ViewGroup.LayoutParams params;
        private RelativeLayout.LayoutParams paramsText;

        public BookPopupWindow(Context context, String createid) {
            this.context = context;
            View view = LayoutInflater.from(context).inflate(R.layout.pop_book, null);
            ButterKnife.inject(this, view);
            params = imgBg.getLayoutParams();
            paramsText = (RelativeLayout.LayoutParams) linear.getLayoutParams();
            pop = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            pop.setOutsideTouchable(true);
            pop.setTouchable(true);
            pop.setFocusable(true);
            pop.setBackgroundDrawable(new BitmapDrawable());
            tvShare.setOnClickListener(this);
            tvDelect.setOnClickListener(this);
            tvReport.setOnClickListener(this);
            tvCollect.setOnClickListener(this);
            /***************    如果是自己tvReport为编辑 否则是举报    *****************/
            if (createid.equals(IMLoginManager.instance().getLoginId() + "")) {
                isMine = true;
                tvCollect.setVisibility(View.GONE);
                tvReport.setText("编辑");
            } else {
                isMine = false;
                tvDelect.setVisibility(View.GONE);
                viewLine.setVisibility(View.GONE);
                tvCollect.setVisibility(View.VISIBLE);
            }
        }

        public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
            pop.setOnDismissListener(onDismissListener);
        }

        /**
         * PopupWindow关闭时显示；
         * 显示时关闭。
         */
        public void show(View view) {
            if (pop != null) {
                if (pop.isShowing()) {
                    pop.dismiss();
                } else {
                    showPopWindow(view);
                }
            }
        }

        /**
         * *************
         * 粗略计算确定pop弹出的位置
         * ******************
         */
        private void showPopWindow(View view) {
            if (isCollect) {
                tvCollect.setText("取消收藏");
            } else {
                tvCollect.setText("收藏");
            }
            int marginRight = (int) (75 * density);
            int marginTop = (int) (9 * density);
            params.height = (int) (78 * density);
            paramsText.setMargins(0, marginTop, 0, 0);
            imgBg.setLayoutParams(params);
            pop.showAsDropDown(view, -marginRight, 4);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tv_share:  //分享
                    new ShareUtil(context).addCustomPlatforms(content, ConstantValues.ShareSelfStory + storyid, bookTitle, storyimg);
                    break;
                case R.id.tv_delete: //删除
                    showDelAlert();
                    break;
                case R.id.tv_report:
                    if (isMine) {    //编辑
                        IMUIHelper.openZiXiModifyActivityForResult(ZiXiDetailsActivity.this, storyid, entity);
                    } else {        //举报
                        IMUIHelper.openReportActivity(ZiXiDetailsActivity.this, storyid, IntentConstant.STORY_ID);
                    }
                    break;
                case R.id.tv_collect_hisbook://收藏
                    collectSelfplay();
                    pop.dismiss();
                    break;
            }
        }

        /**
         * *************
         * 删除自戏的警告提示框
         * ******************
         */
        private void showDelAlert() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("警告");
            builder.setMessage("确定要删除该故事吗");
            builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    /**************        删除同时刷新个人主页      *************/
                    DeleteStoryBook();
                    Intent intent = new Intent();
                    intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
                    sendBroadcast(intent);
                }
            });
            builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.create().show();
        }

        /**
         * *******************
         * 收藏自戏
         * ************************
         */
        private void collectSelfplay() {
            OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
            params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
            params.addParams("storyid", storyid);
            params.addParams("type", "0");
            if (isCollect) {
                params.addParams("behavior", 0 + "");
            } else {
                params.addParams("behavior", 1 + "");
            }

            OkHttpClientManager.postAsy(ConstantValues.CollectStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                /** 请求成功后返回的Json */
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.getString("result").equals("1")) {
                            if (!isCollect) {
                                Toast.makeText(context, "收藏成功", Toast.LENGTH_LONG).show();
                                tvCollect.setText("取消收藏");
                                isCollect = true;
                            } else {
                                Toast.makeText(context, "取消收藏成功", Toast.LENGTH_LONG).show();
                                tvCollect.setText("收藏");
                                isCollect = false;
                            }
                        } else {
                            Toast.makeText(context, jsonObject.getString("describe"), Toast.LENGTH_LONG).show();
                        }
                        pop.dismiss();
                    } catch (JSONException e) {
                        logger.e(e.toString());
                    }
                }

                @Override
                public void onError(Request request, Exception e) {
                }
            });
        }
    }

    /**
     * ***************
     * 删除自戏
     * ************************
     */
    private void DeleteStoryBook() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.DeleteStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                DeleteStoryResult result = new Gson().fromJson(response, DeleteStoryResult.class);
                if (result.getResult().equals("1")) {
                    T.showShort(ZiXiDetailsActivity.this, result.getDescribe());
                    finish();
                } else {
                    T.showShort(ZiXiDetailsActivity.this, result.getDescribe());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (behavior != behaviorTemp || isCollectTemp != isCollect) {
            Intent intent = new Intent();
            intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
            sendBroadcast(intent);
        }
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                startFlg = START_FLG_NO_SET;
                T.showShort(this, "暂时不能评论");
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
                Log.d("storyDeatilsActivity", "CHANGE_GROUP_MEMBER_SUCCESS is called");
                break;
            }
            //向群里添加人员的时候，最后也会获取群的信息。因此不需要处理变更人员的消息。
            case GROUP_INFO_UPDATED: {
                //不是在点击状态下，忽略更新事件。
                if (startFlg != START_FLG_SET) {
                    break;
                }
                GroupEntity groupEntity = event.getGroupEntity();
                //如果是当前评论群的信息的变化
                if (groupEntity.getPeerId() == commentGroupId) {
                    checkExistAndStartChat(groupEntity);
                }
            }
            break;
        }
    }

    public void onEventMainThread(SquareCommentEvent event) {
        switch (event.eventType){
            case TYPE_DELETE_STORY_TOPIC:
                finish();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            GetSelfStoryDetail();
        }
    }
}
