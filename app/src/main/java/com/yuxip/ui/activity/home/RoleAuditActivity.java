package com.yuxip.ui.activity.home;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryRoleApplyJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.widget.MyListView;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/11/4.
 * description：角色审核
 */
public class RoleAuditActivity extends TTBaseActivity implements View.OnClickListener {
    private CustomHeadImage ivHeadImg;      //申请者头像
    private TextView tvUserName;            //申请者姓名
    private TextView tvCreateTime;          //
    private TextView tvRole;                //要扮演的角色
    private TextView tvArticle;             //审核戏文
    private TextView tvAccept;              //同意
    private TextView tvReject;              //拒绝
    private TextView tvRoleSet;             //角色设定的文字 如果没有角色设定 则tvRoleSet和 lvRoleSet  设为不可见
    private MyListView lvRoleSet;           //角色设定
    private TextView tvBackground;
    private RoleSetAdapter roleSetAdapter;
    private String loginId;
    private String applyId;
    private List<StoryRoleApplyJsonBean.ApplyrolenaturesEntity> listRoles = new ArrayList<>();
    private StoryRoleApplyJsonBean storyRoleApplyJsonBean;
    private String storyId;
    private String applyPersonId = "";
    private boolean isDeal = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_role_audit, topContentView);
        EventBus.getDefault().register(this);
        initRes();
        requestAuditRoles();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void initRes() {
        applyId = getIntent().getStringExtra(IntentConstant.AUDIT_ROLE_APPLY_ID);
        loginId = IMLoginManager.instance().getLoginId() + "";
        setTitle(getResources().getString(R.string.story_role_setting));
        topLeftBtn.setImageResource(R.drawable.back_default_btn);
        topLeftBtn.setVisibility(View.VISIBLE);
        ivHeadImg = (CustomHeadImage) findViewById(R.id.iv_userhead_role_audit);
        tvUserName = (TextView) findViewById(R.id.tv_username_role_audit);
        tvCreateTime = (TextView) findViewById(R.id.tv_createtime_role_audit);
        tvRole = (TextView) findViewById(R.id.tv_role_play_role_audit);
        tvArticle = (TextView) findViewById(R.id.tv_article_role_audit);
        tvAccept = (TextView) findViewById(R.id.tv_accept_role_audit);
        tvReject = (TextView) findViewById(R.id.tv_reject_role_audit);
        tvRoleSet = (TextView) findViewById(R.id.tv_role_setting_role_audit);
        tvBackground = (TextView) findViewById(R.id.tv_background_role_audit);
        lvRoleSet = (MyListView) findViewById(R.id.lv_role_setting_role_audit);
        ivHeadImg.setOnClickListener(this);
        tvAccept.setOnClickListener(this);
        tvReject.setOnClickListener(this);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        roleSetAdapter = new RoleSetAdapter();
        lvRoleSet.setAdapter(roleSetAdapter);
    }

    /**
     * 请求申请信息
     */
    private void requestAuditRoles() {
        if (TextUtils.isEmpty(applyId)) {
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("applyid", applyId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoleApplication, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_net_status_error), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceiveData(response);
                    }
                });
    }


    /**
     * 解析数据
     *
     * @param response 网络请求返回的body
     */
    private void onReceiveData(String response) {
        try {
            storyRoleApplyJsonBean = new Gson().fromJson(response, StoryRoleApplyJsonBean.class);
            if (storyRoleApplyJsonBean == null) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), Toast.LENGTH_LONG).show();
                return;
            }
            inflateData();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_get_netdata_fail), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 填充数据
     */
    private void inflateData() {
        storyId = storyRoleApplyJsonBean.getApplystoryid();
        applyPersonId = storyRoleApplyJsonBean.getApplicantuid();
        if (storyRoleApplyJsonBean.getApplyrolenatures() == null || storyRoleApplyJsonBean.getApplyrolenatures().size() == 0) {
            tvRoleSet.setVisibility(View.GONE);
            lvRoleSet.setVisibility(View.GONE);
        } else {
            listRoles = storyRoleApplyJsonBean.getApplyrolenatures();
            roleSetAdapter.notifyDataSetChanged();
        }
        ivHeadImg.loadImage(storyRoleApplyJsonBean.getApplicantportrait());
        tvUserName.setText(storyRoleApplyJsonBean.getApplicantname());
        tvRole.setText(storyRoleApplyJsonBean.getApplyroletype());
        tvCreateTime.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(storyRoleApplyJsonBean.getApplytime())));
        tvBackground.setText(storyRoleApplyJsonBean.getApplyrolebackground());
        tvArticle.setText(storyRoleApplyJsonBean.getApplytext());
    }


    /**
     * 同意/拒绝申请
     *
     * @param approved 同意1/拒绝0
     */
    private void requestApplyOperation(String approved) {
        if (TextUtils.isEmpty(applyId) || TextUtils.isEmpty(storyId)) {
            return;
        }
        if(isDeal) {
            return;
        } else {
            isDeal = true;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("applyid", applyId);
        params.addParams("approved", approved);
        OkHttpClientManager.postAsy(ConstantValues.ReviewRoleApplication, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.story_net_status_error), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            if (jsonObject.getString("result").equals("1")) {
                                T.show(getApplicationContext(), "操作成功!", 0);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("describe"), Toast.LENGTH_LONG).show();
                                tvAccept.setClickable(true);
                                tvReject.setClickable(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(), "server error", Toast.LENGTH_LONG).show();
                            tvAccept.setClickable(true);
                            tvReject.setClickable(true);
                        }
                    }
                });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_accept_role_audit:
                tvAccept.setClickable(false);
                tvReject.setClickable(false);
                moveToGroup();
                break;
            case R.id.tv_reject_role_audit:
                tvAccept.setClickable(false);
                tvReject.setClickable(false);
                requestApplyOperation("0");
                break;
            case R.id.iv_userhead_role_audit:
                if (!TextUtils.isEmpty(storyRoleApplyJsonBean.getApplicantuid())) {
                    IMUIHelper.openUserHomePageActivity(this, storyRoleApplyJsonBean.getApplicantuid());
                }
                break;
        }
    }

    /**
     * 移至水聊和对戏群
     */
    private void moveToGroup() {
        if(TextUtils.isEmpty(storyId) || TextUtils.isEmpty(applyPersonId)) {
            return;
        }
        List<Integer> types = new ArrayList<>();
        types.add(1);
        types.add(2);
        Set<Integer> groupSet = YXStoryManager.instance().getGroupIdsByStoryIdAndGroupTypes(Integer.valueOf(storyId), types);
        if(groupSet.size() == 0) {
            T.show(getApplicationContext(), "未知错误，请稍候再试", 0);
            return;
        }
        Set<Integer> memberSet = new HashSet<>();
        memberSet.add(Integer.valueOf(applyPersonId));
        for (Integer curId : groupSet) {
            IMGroupManager.instance().reqAddGroupMember(curId, memberSet);
        }
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CHANGE_GROUP_MEMBER_FAIL:
            case CHANGE_GROUP_MEMBER_TIMEOUT: {
                T.show(getApplicationContext(), "操作失败请重试！", 0);
                return;
            }
            case CHANGE_GROUP_MEMBER_SUCCESS: {
              requestApplyOperation("1");
            }
            break;
        }
    }

    private class RoleSetAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return listRoles.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if (convertView == null) {
                convertView = LayoutInflater.from(RoleAuditActivity.this).inflate(R.layout.item_lv_role_audit, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.tvName.setText(listRoles.get(position).getName() + "：");
            viewHolder.tvValue.setText(listRoles.get(position).getValue());
            return convertView;
        }

        class ViewHolder {
            private TextView tvName;
            private TextView tvValue;

            public ViewHolder(View view) {
                tvName = (TextView) view.findViewById(R.id.tv_name_item_lv_role_audit);
                tvValue = (TextView) view.findViewById(R.id.tv_value_item_lv_role_audit);
            }
        }
    }
}
