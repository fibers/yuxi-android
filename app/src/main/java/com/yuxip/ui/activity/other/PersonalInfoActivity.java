package com.yuxip.ui.activity.other;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMContactManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Edited by SummerRC on 2015/4/20.
 * description:更新个人资料
 */
public class PersonalInfoActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(PersonalInfoActivity.class);

    private View chatView;
    private EditText et_username;
    private EditText et_gender;
    private EditText et_intro;
    private TextView tv_id;
    private ImageView iv_person_icon;
    private RadioButton genderMan;
    private RadioButton genderWoman;

    private Boolean isManCheck = false;
    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";

    public static final String LOCAL_HEAD_IMG_NAME = IMLoginManager.instance().getLoginId() + ".png";
    private String upPicName;

    private CircularImage personIcon;
    private String ImageUrl;
    private String regex = "[\u4E00-\u9FA5]";
    private int etTotalCount;
    private int etChineseCount;
    private String userName = "";
    private Pattern p = Pattern.compile(regex);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        chatView = View.inflate(PersonalInfoActivity.this, R.layout.activity_personinfo, topContentView);
        initView();
        setLeftButton(R.drawable.back_default_btn);
        setTitle("编辑资料");
        setRighTitleText("完成");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_username.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "名字不能为空", Toast.LENGTH_LONG).show();
                    return;
                }
                setData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserInfo();
    }

    private void setData() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        String gd;
        if (isManCheck) {
            gd = "1";
        } else {
            //女
            gd = "0";
        }
        final String gender = gd;
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("intro", et_intro.getText().toString());        // 简介
        params.addParams("nickname", et_username.getText().toString());  // 昵称
        if (!TextUtils.isEmpty(ImageUrl)) {
            params.addParams("portraitImg", ImageUrl);                   // 头像url
        }
        params.addParams("gender", gender);                              //性别
        OkHttpClientManager.postAsy(ConstantValues.SubmitPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    int result = object.getInt("result");
                    if (result == 1) {
                        /**  add by SummerRC  修改个人信息后，更新LoginInfo和数据库 begin  */
                        UserEntity userEntity = IMLoginManager.instance().getLoginInfo();
                        userEntity.setMainName(et_username.getText().toString());
                        userEntity.setGender(Integer.valueOf(gender));
                        if (!TextUtils.isEmpty(ImageUrl)) {
                            userEntity.setAvatar(ImageUrl);
                        }
                        IMLoginManager.instance().setLoginInfo(userEntity);
                        IMContactManager.instance().UpdateDBUsers(userEntity);      //更新数据库信息
                        /**  add by SummerRC  修改个人信息后，更新LoginInfo和数据库   end  */
                        Toast.makeText(PersonalInfoActivity.this, "修改成功", Toast.LENGTH_LONG).show();
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        String describe = object.getString("describe");
                        Toast.makeText(PersonalInfoActivity.this, describe, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), "网络错误", 1);
            }
        });
    }

    public void initView() {
        et_username = (EditText) chatView.findViewById(R.id.et_username);
        et_username.setFocusable(true);
        et_username.setFocusableInTouchMode(true);
        et_username.requestFocus();
        et_username.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return event.getKeyCode() == KeyEvent.KEYCODE_ENTER;
            }
        });
        et_username.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            /**     进行判断汉字及其他字符  限制其数目  */
            @Override
            public void afterTextChanged(Editable s) {
                Matcher matcher = p.matcher(s.toString());
                etChineseCount = 0;
                etTotalCount = 0;
                while (matcher.find()) {
                    etChineseCount += 1;
                    etTotalCount += 2;
                }
                etTotalCount += s.toString().length() - etChineseCount;
                if (etTotalCount <= 12) {
                    userName = s.toString();
                } else {
                    Toast.makeText(getApplicationContext(), "字数不能超出12个字母或6个汉字", Toast.LENGTH_LONG).show();
                    et_username.setText(userName);
                    et_username.setSelection(userName.length());
                }
            }
        });

        genderWoman = (RadioButton) chatView.findViewById(R.id.genderWoman);
        genderMan = (RadioButton) chatView.findViewById(R.id.genderMan);
        et_intro = (EditText) chatView.findViewById(R.id.et_intro);
        tv_id = (TextView) chatView.findViewById(R.id.tv_id);
        personIcon = (CircularImage) chatView.findViewById(R.id.iv_person_icon);
        personIcon.setImageBitmap(new UpImgUtil().getHeadImg());
        findViewById(R.id.changeHeadimg).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showHeadImgDialog();
            }
        });
        genderWoman.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isNvChecked) {
                if (isNvChecked) {
                    genderMan.setChecked(false);
                    isManCheck = false;
                }
            }
        });
        genderMan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isManChecked) {
                if (isManChecked) {
                    genderWoman.setChecked(false);
                    isManCheck = true;
                }
            }
        });

    }


    /**
     * 得到个人信息
     */
    private void getUserInfo() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("personid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GetPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONObject personinfo = object.getJSONObject("personinfo");
                        String nikename = personinfo.getString("nickname");
                        et_username.setText(nikename);
                        et_username.setSelection(nikename.length());
                        //自定弹出软键盘
                        InputMethodManager inputManager = (InputMethodManager) et_username.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputManager.showSoftInput(et_username, 0);
                        tv_id.setText(personinfo.getString("id"));
                        et_intro.setText(personinfo.getString("intro"));
                        String gender = personinfo.getString("gender");
                        if (gender.equals("1")) {
                            genderMan.setChecked(true);
                            genderWoman.setChecked(false);
                        } else {
                            genderWoman.setChecked(true);
                            genderMan.setChecked(false);
                        }

                        ImageLoader imageLoader = ImageLoaderUtil.getImageLoaderInstance();
                        Drawable drawable = DrawableCache.getInstance(getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
                        DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
                        imageLoader.displayImage(personinfo.getString("portrait"), personIcon, imageOptions);

                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), "服务器忙,请稍后再试", 1);
            }
        });
    }

    //弹出更换头像dialog
    private void showHeadImgDialog() {
        new GGDialog().showImgSelcetDialog(PersonalInfoActivity.this, "更换头像", "添加图片", new GGDialog.OnDialogButtonClickedListenered() {
            @Override
            public void onConfirmClicked() {
                try {
                    try {
                        Intent intent = new Intent(Intent.ACTION_PICK, null);
                        intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                        startActivityForResult(intent, PHOTOZOOM);
                    } catch (ActivityNotFoundException e) {
                        T.show(getApplicationContext(), e.toString(), 0);
                        logger.e(e.toString());
                    }
                } catch (ActivityNotFoundException e) {
                    T.show(getApplicationContext(), e.toString(), 0);
                    logger.e(e.toString());
                }

            }

            @Override
            public void onCancelClicked() {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "temp.jpg")));
                startActivityForResult(intent, PHOTOHRAPH);
            }
        });
    }

    /**
     * 回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE)
            return;
        // 拍照
        if (requestCode == PHOTOHRAPH) {
            //设置文件保存路径这里放在跟目录下
            File picture = new File(Environment.getExternalStorageDirectory() + "/temp.jpg");
            startPhotoZoom(Uri.fromFile(picture));
        }

        if (data == null)
            return;

        // 读取相册缩放图片
        if (requestCode == PHOTOZOOM) {
            startPhotoZoom(data.getData());
        }
        // 处理结果
        if (requestCode == PHOTORESOULT) {
            Bundle extras = data.getExtras();
            if (extras != null) {
                Bitmap photo = extras.getParcelable("data");
                final UpImgUtil upImgUtil = new UpImgUtil();
                /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
                Date date = new Date();
                /** 获取当前时间并且进一步转化为字符串 */
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
                upPicName = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";
                upImgUtil.setFilePath(LOCAL_HEAD_IMG_NAME, upPicName);
                upImgUtil.saveHeadImg(photo);
                //命名，服务器上头像地址为：url/id.png
                upImgUtil.upLoadPicture(new HeadImgListener() {
                    public void notifyImgUploadFinished(String url) {
                        personIcon.setImageBitmap(upImgUtil.getHeadImg());
                        ImageUrl = url;
                    }
                });

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * 进行裁剪
     *
     * @param uri
     */
    public void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, IMAGE_UNSPECIFIED);
        intent.putExtra("crop", "true");
        // aspectX aspectY 是宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        // outputX outputY 是裁剪图片宽高
        intent.putExtra("outputX", 320);
        intent.putExtra("outputY", 320);

        //设置缩放
        //add by guoq-s
        intent.putExtra("scale", true);
        intent.putExtra("scaleUpIfNeeded", true);
        //add by guoq-e

        intent.putExtra("return-data", true);
        startActivityForResult(intent, PHOTORESOULT);
    }
}
