package com.yuxip.ui.activity.add;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.CreateGroupType;
import com.yuxip.config.GlobalVariable;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.CreateStoryTopicEvent;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 创建剧
 * Created by SummerRC on 15/5/18.
 */
public class CreateStoryActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(CreateStoryActivity.class);

    @InjectView(R.id.textinput_story_name)
    TextInputLayout textinputStoryName;
    @InjectView(R.id.textinput_introduce)
    TextInputLayout textinputIntroduce;
    @InjectView(R.id.iv_add_image)
    ImageView ivAddImage;
    @InjectView(R.id.create_zixi_progress_bar)
    ProgressBar mProgressBar;

    private EditText etTitle;
    private EditText etContent;
    private String title;
    private String content;
    private String category;
    private IMService imService;
    private int type;                           //创建群的类型
    private String storyId;                     //剧id
    private String reviewGroupId;               //审核群id

    private static final int NONE = 0;
    private static final int PHOTOHRAPH = 1;      // 拍照
    private static final int PHOTOZOOM = 2;       // 缩放
    private static final int PHOTORESOULT = 3;    // 结果
    private static final String IMAGE_UNSPECIFIED = "image/*";

    private Bitmap photo;
    private boolean CLICKABLE = true;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);

        category = getIntent().getStringExtra(IntentConstant.SELECTED_IDS);
        View view = LayoutInflater.from(this).inflate(R.layout.activity_create_story, topContentView);
        ButterKnife.inject(this, view);
        setTitle("创建剧");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("发布");
        setRighTitleTextColor(getResources().getColor(R.color.pink));
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogHelper.showExitDialog(CreateStoryActivity.this);
            }
        });
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CLICKABLE) {
                    CLICKABLE = false;
                } else {
                    return;
                }
                title = etTitle.getText().toString().trim();
                content = etContent.getText().toString().trim();
                if (TextUtils.isEmpty(title) || TextUtils.isEmpty(content)) {
                    CLICKABLE = true;
                    showToast("输入内容不能为空");
                } else if (photo == null) {
                    CLICKABLE = true;
                    showToast("请选择图片");
                } else {
                    beginDeal();
                    setRighTitleText("发布中");
                    IMApplication.IS_REFRESH = true;
                    T.showShort(CreateStoryActivity.this, "请稍等");
                    /** 先创建一个审核群，成功之后创建剧，然后关联审核群和剧，关联成功之后创建水聊群和对戏群 */
                    GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                    type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW;
                    createGroup("审核群");
                }
            }
        });
        initView();
    }

    private void initView() {
        ivAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddImgDialog();
            }
        });

        textinputStoryName.setHint("剧的名称(30字以内)");
        textinputIntroduce.setHint("简介");
        etTitle = textinputStoryName.getEditText();
        etContent = textinputIntroduce.getEditText();
    }


    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(this);
        imServiceConnector.unbindService(this);
        super.onDestroy();
    }


    /**
     * @param groupName 创建群的名字：审核群 水聊群  对戏群
     */
    private void createGroup(String groupName) {
        Set<Integer> ListSet = new HashSet<>();
        IMGroupManager groupMgr = imService.getGroupManager();
        int loginId = imService.getLoginManager().getLoginId();
        ListSet.add(loginId);
        groupMgr.reqCreateTempGroup(groupName, ListSet);
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CREATE_GROUP_OK:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY) {
                    return;
                }
                handleCreateGroupSuccess(event);
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                if (GlobalVariable.currentActivity != GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY) {
                    return;
                }
                handleCreateGroupFail();
                break;
        }
    }

    /**
     * 处理群创建成功事件
     *
     * @param event 创建群的事件
     */
    private void handleCreateGroupSuccess(GroupEvent event) {
        switch (type) {
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW:             //创建审核群成功事件
                reviewGroupId = event.getGroupEntity().getPeerId() + "";
                /** 创建剧 */
                createPlay();
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT:         //创建水聊群成功事件
                String waterChatGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联水聊群和剧   */
                addGroupToStory(waterChatGroupId, "水聊群");
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY:            //创建对戏群成功事件
                String isPlayGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联对戏群和剧   */
                addGroupToStory(isPlayGroupId, "对戏群");
                break;
            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_COMMENT:            //创建评论群成功事件
                isPlayGroupId = event.getGroupEntity().getPeerId() + "";
                /**  关联评论群和剧   */
                addGroupToStory(isPlayGroupId, "评论群");
                break;
        }
    }

    /**
     * 处理群创建失败事件
     */
    private void handleCreateGroupFail() {
        CLICKABLE = true;
        setRighTitleText("发布");
        Toast.makeText(this, getString(R.string.create_temp_group_failed), Toast.LENGTH_SHORT)
                .show();
    }

    /**
     * 拿审核群id去创建剧,成功之后关联剧和群
     */
    private void createPlay() {
        /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
        Date date = new Date();
        /** 获取当前时间并且进一步转化为字符串 */
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
        String pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

        UpImgUtil upImgUtil = new UpImgUtil();
        upImgUtil.setFilePath(pic_path, pic_path);
        upImgUtil.saveHeadImg(photo);
        upImgUtil.upLoadPicture(new HeadImgListener() {
            public void notifyImgUploadFinished(String url) {

                //add by guoq-s
                if (url == null) {
                    String notifyText = (String) getResources()
                            .getText(R.string.create_play_upload_img_fail);
                    Toast.makeText(CreateStoryActivity.this, notifyText, Toast.LENGTH_SHORT).show();
                    endDeal();
                    return;
                }
                //add by guoq-e

                String uid = IMLoginManager.instance().getLoginId() + "";
                /** 放在params里面传递 */
                OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
                params.addParams("uid", uid);
                params.addParams("storyimg", url);
                params.addParams("title", title);
                params.addParams("intro", content);
                params.addParams("groupid", reviewGroupId);
                if (category.isEmpty()) {
                    params.addParams("category", "12");
                } else {
                    params.addParams("category", category);
                }
                OkHttpClientManager.postAsy(ConstantValues.CreateStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
                    /** 请求成功后返回的Json */
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            String id = object.getString("result");
                            if (id.equals("1")) {
                                String text = object.getString("describe");
                                String[] array = getData(text);
                                storyId = array[0];
                                /** 关联剧和审核群 */
                                addGroupToStory(reviewGroupId, "审核群");
                            } else {
                                showToast(object.getString("describe"));
                                etTitle.requestFocus();
                                etTitle.setFocusable(true);
                                endDeal();
                            }
                        } catch (Exception e) {
                            endDeal();
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        endDeal();
                    }
                });
            }
        });
    }


    private void beginDeal() {
        mProgressBar.setVisibility(View.VISIBLE);
        CLICKABLE = false;
    }

    private void endDeal() {
        mProgressBar.setVisibility(View.INVISIBLE);
        CLICKABLE = true;
    }

    /**
     * 分割字符串返回一个数组
     * 0 : 剧id
     * 1 : 审核群id
     */
    private String[] getData(String str) {
        return str.split(",");
    }

    /**
     * 关联剧和群
     *
     * @param groupId   ：审核群id  水聊群id  对戏群id
     * @param groupName ：审核群    水聊群    对戏群
     */
    private void addGroupToStory(String groupId, String groupName) {
        /** 是否是对戏群 */
        String isPlay;
        if (groupName.contains("对戏群")) {
            isPlay = ConstantValues.GROUP_TYPE_PLAY;
        } else if (groupName.contains("审核群")) {
            isPlay = ConstantValues.GROUP_TYPE_SHENHE;
        } else if (groupName.contains("评论群")) {
            isPlay = ConstantValues.GROUP_TYPE_COMMENT;
        } else {
            isPlay = ConstantValues.GROUP_TYPE_SHUILIAO;
        }

        String uid = IMLoginManager.instance().getLoginId() + "";
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);            //剧管理员id
        params.addParams("storyid", storyId);    //剧id
        params.addParams("title", groupName);    //群的名字
        params.addParams("intro", content);
        params.addParams("groupid", groupId);
        params.addParams("isplay", isPlay);
        params.addParams("token", "1");

        OkHttpClientManager.postAsy(ConstantValues.AddGroupToStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        switch (type) {
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_REVIEW:             //关联审核群成功
                                /** 创建水聊群 */
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                                type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT;
                                createGroup("水聊群");
                                break;
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_WATER_CHAT:         //关联水聊群成功
                                /** 创建对戏群 */
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
                                type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY;
                                createGroup("对戏群");
                                break;
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_IS_PLAY:            //关联对戏群成功
                                GlobalVariable.currentActivity = GlobalVariable.CurrentActivity.ACTIVITY_CREATE_PLAY;
//                                type = CreateGroupType.CREATE_GROUP_TYPE_PLAY_COMMENT;
//                                createGroup("评论群");
//                                break;
                            case CreateGroupType.CREATE_GROUP_TYPE_PLAY_COMMENT:
                                showToast("剧创建成功");
                                IMApplication.IS_REFRESH = true;
                                /** 更新数据库中家族信息*/
//                                YXFamilyManager.instance().getMyFamilyList();
                                /** 发送广播，广播的定义以及注册在MainActivity里面 */
                                CreateStoryTopicEvent event = new CreateStoryTopicEvent();
                                event.eventType = CreateStoryTopicEvent.Event.TYPE_CREATE_STORY;
                                EventBus.getDefault().post(event);
                                IMUIHelper.openStoryEditActivity(CreateStoryActivity.this, storyId, IMLoginManager.instance().getLoginId() + "", "owener", "1");
                                finish();
                                break;
                        }
                    } else {
                        endDeal();
                        showToast(object.getString("describe"));
                        setRighTitleText("发布");
                    }
                } catch (Exception e) {
                    endDeal();
                    setRighTitleText("发布");
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                endDeal();
                setRighTitleText("发布");
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }


    /**
     * 弹出添加图片的dialog
     */
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(CreateStoryActivity.this, "创建剧", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        //intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(new File(Environment.getExternalStorageDirectory(), "temp.jpg")));
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }


    /**
     * 回调
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {

            return;
        }
        ContentResolver resolver = getContentResolver();

        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    //use bundle to get data
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        photo = (Bitmap) bundle.get("data"); //get bitmap
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateStoryActivity.this);
                        ivAddImage.setImageBitmap(photo);

                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        //to do find the path of pic by uri
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateStoryActivity.this);
                        ivAddImage.setImageBitmap(photo);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                        e.printStackTrace();
                    }
                }
                break;

            case PHOTORESOULT: //暂时没用
                /*  从本地图片取数据
                FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory() + "/temp.jpg");
                Bitmap bitmap = BitmapFactory.decodeStream(fis);

                addImage.setImageBitmap(bitmap);
                */
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    ivAddImage.setImageBitmap(photo);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            DialogHelper.showExitDialog(this);
        }
        return false;
    }
}
