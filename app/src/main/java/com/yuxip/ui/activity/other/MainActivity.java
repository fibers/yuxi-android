package com.yuxip.ui.activity.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.entity.UnreadNotifyEntity;
import com.yuxip.imservice.entity.MessageType;
import com.yuxip.imservice.event.ChannelSelectEvent;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.http.UnreadNotifyEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.YXUnreadNotifyManager;
import com.yuxip.imservice.manager.http.ChannelSelectDataManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.RemarkNameManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.add.story.CreateStoryFirstActivity;
import com.yuxip.ui.activity.base.TTBaseNewFragmentActivity;
import com.yuxip.ui.broadcast.HomeWatcherReceiver;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.fragment.home.MyHomePageFragment;
import com.yuxip.ui.widget.TabButton;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.FirstPostUtils;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.NetWorkUtils;
import com.yuxip.utils.SharedPreferenceUtils;
import com.yuxip.utils.UgcAnimationUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * 主体框架：Activity包含四个Fragment,每个Fragment通过Viewpager再包含多个Fragment
 * Created by SummerRC
 */
public class MainActivity extends TTBaseNewFragmentActivity implements View.OnClickListener {

    private Fragment[] mFragments;              //数组  包含主体框架的四个Fragment       :   世界 好友 家族 我
    private TabButton[] mTabButtons;            //数组  包含底部五个Button               ：  世界 好友 加号  家族 我
    private TextView[] mUnreadTextViews;        //数组  包含底部显示未读消息数的TextView  ：  世界 好友 家族
    private ImageView iv_add;                   //加号
    private MyHandler unreadNotifyHandler = new MyHandler(this);
    private FrameLayout frameLayout_top;        //最上层的透明层

    public static final int UNREAD_MESS_INDEX_SQUARE = 1;      //未读消息：广场
    public static final int UNREAD_MESS_INDEX_FAMILY = 2;      //未读消息：家族
    private Logger logger = Logger.getLogger(MainActivity.class);

    private long firstTime;

    private IMService imService;
    private String mustFlag = "-1";             //强制更新的标志  0：不需要更新     1：必须更新
    private boolean areButtonsShowing = false;  //点击加号的弹出层是否显示
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            imService.getYXUnreadNotifyManager().getUnreadNotify();
        }
    };
    private LinearLayout layout_no_network;     //网络连接的提示
    private LinearLayout llUgcWrapper;          //点击加号的弹出框
    private String downUrl;


    @Override
    protected void onStart() {
        super.onStart();
        RemarkNameManager.getInstance().getUsersNameFromNet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            logger.w("MainActivity#crashed and restarted, just exit");
            jumpToLoginPage();
            finish();
        }
        if (super.isNeedRestart()) {
            return;
        }

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.tt_activity_main);

        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);

        /**判断是否首次登录*/
        if (SharedPreferenceUtils.getBooleanDate(this, ConstantValues.FIRST_LOGIN, true)) {
            String loginId = IMLoginManager.instance().getLoginId() + "";
            FirstPostUtils.sendHttpRequest(this, loginId, 1);
        }
        frameLayout_top = (FrameLayout) findViewById(R.id.frame_layout_top_main);
        layout_no_network = (LinearLayout) findViewById(R.id.layout_no_network);
        /** 注册广播接收器 */
        MyReceiver receiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(ConstantValues.BROADCAST_REFRESH_HOME);
        /** 注册网络广播 */
        MainActivity.this.registerReceiver(receiver, filter);
        /** 初始化底部菜单栏：世界  好友  加号  家族  我  */
        initTab();
        /** 初始化加号弹出层按钮的坚听 */
        initUgcButtomReq();
        /** 初始化底部菜单的未读消息数  */
        initUnreadMessageText();
        /** 初始化底部菜单栏对应的fragment：世界  好友  加号  家族  我  */
        initFragment();
        setFragmentIndicator(0);
        updateApp();
        /** 网络连接失败或者多端登录要显示的view */

        /** 提示申请角色 */
        tipApplyRole();
        /*String info = getDeviceInfo(this);
        Log.e("info", info);*/
    }


    /**
     * 检查是否有新版本
     */
    private void updateApp() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("devicetype", "1");
        OkHttpClientManager.postAsy(ConstantValues.GetLatestPackageInfo, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        parsUpdateData(response);
                    }

                    @Override
                    public void onError(Request request, Exception e) {

                    }
                });
    }

    /**
     * 提示申请角色
     */
    private void tipApplyRole() {
        if(SharedPreferenceUtils.getBooleanDate(getApplicationContext(), SharedPreferenceValues.TIP_APPLY_ROLE, true)) {
            SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.TIP_APPLY_ROLE, false);
            DialogHelper.showTipDialog(this, getResources().getString(R.string.apply_role));
        }
    }

    private void parsUpdateData(String jsonString) {
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.getString("result").equals("1")) {
                JSONObject pkg = jsonObject.optJSONObject("pkginfo");
                String versionCode = pkg.optString("versioncode");
                String versionnumber = pkg.optString("versionnumber");
                String desc = pkg.optString("desc");
                String downurl = pkg.optString("downurl");
                mustFlag = pkg.optString("mustflag", "-1");
                toCompare(versionCode, versionnumber, desc, downurl);
            }
        } catch (JSONException e) {
            logger.e(e.toString());
        }
    }

    /**
     * 用versionCode与本地比较
     *
     * @param versionCode   versionCode
     * @param versionnumber versionnumber
     * @param desc          desc
     * @param downurl       downurl
     */
    private void toCompare(String versionCode, String versionnumber, String desc, String downurl) {
        if (!TextUtils.isEmpty(versionCode)) {
            int serverCode = Integer.parseInt(versionCode);
            int clientCode = getClientVersionCode();
            if (serverCode != 0 && clientCode != 0 && serverCode > clientCode) {
                showUpdateDialog(versionnumber, desc, downurl);
            }
        }
    }

    private AlertDialog dialog;

    private void showUpdateDialog(String versionNumber, String desc, final String downUrl) {
        this.downUrl = downUrl;
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogUpdate);
        builder.setView(getUpdateDialogView(versionNumber, desc), 25, 0, 25, 0);
        dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    public View getUpdateDialogView(String versionNumber, String desc) {
        View view = getLayoutInflater().inflate(R.layout.dialog_update, null);
        AppCompatTextView dialog_title = (AppCompatTextView) view.findViewById(R.id.dialog_title);
        AppCompatTextView dialog_message = (AppCompatTextView) view.findViewById(R.id.dialog_message);
        if (!TextUtils.isEmpty(versionNumber)) {
            dialog_title.setText(dialog_title.getText() + "  " + versionNumber);
        }
        if (!TextUtils.isEmpty(desc)) {
            dialog_message.setText(desc);
        }
        AppCompatTextView buttonCancel = (AppCompatTextView) view.findViewById(R.id.dialog_cancel);
        AppCompatTextView buttonOk = (AppCompatTextView) view.findViewById(R.id.dialog_ok);
        buttonCancel.setOnClickListener(this);
        buttonOk.setOnClickListener(this);
        return view;
    }

    private void startUpdateActivity() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(downUrl));
        startActivity(intent);
    }

    private int getClientVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            logger.e(e.toString());
        }
        return 0;
    }

    private void initUgcButtomReq() {
        findViewById(R.id.to_ugc_family).setOnClickListener(this);
        findViewById(R.id.to_ugc_story).setOnClickListener(this);
        findViewById(R.id.to_ugc_book).setOnClickListener(this);
        findViewById(R.id.to_ugc_topic).setOnClickListener(this);
        frameLayout_top.setOnClickListener(this);
    }


    /**
     * 获取广播数据
     */
    public class MyReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                int netWorkType = NetWorkUtils.getNetWorkType(MainActivity.this);
                if (netWorkType == NetWorkUtils.NETWORKTYPE_INVALID) {
                    layout_no_network.setVisibility(View.VISIBLE);
                } else {
                    layout_no_network.setVisibility(View.INVISIBLE);
                }
            }
            if (action.equals(ConstantValues.BROADCAST_REFRESH_HOME)) {
                MyHomePageFragment myHomePageFragment = (MyHomePageFragment) mFragments[3];
                myHomePageFragment.ReqHomeInfo();
                YXStoryManager.instance().getMyStory();
            }
        }
    }

    private void initTab() {
        /** 点击加号的弹出层 */
        llUgcWrapper = (LinearLayout) findViewById(R.id.to_ugc_wapper);
        llUgcWrapper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onClickView(true);
                return true;
            }
        });
        iv_add = (ImageView) findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);

        mTabButtons = new TabButton[4];
        mTabButtons[0] = (TabButton) findViewById(R.id.tb_story);
        mTabButtons[1] = (TabButton) findViewById(R.id.tb_square);
        mTabButtons[2] = (TabButton) findViewById(R.id.tb_chat);
        mTabButtons[3] = (TabButton) findViewById(R.id.tb_my);
        mTabButtons[0].setTitle("剧场");
        mTabButtons[0].setIndex(0);
        mTabButtons[0].setSelectedImage(getResources().getDrawable(R.drawable.buttom_navi_square_icon_selected));
        mTabButtons[0].setUnselectedImage(getResources().getDrawable(R.drawable.buttom_navi_square_icon));
        mTabButtons[1].setTitle("广场");
        mTabButtons[1].setIndex(1);
        mTabButtons[1].setSelectedImage(getResources().getDrawable(R.drawable.buttom_navi_word_icon_selected));
        mTabButtons[1].setUnselectedImage(getResources().getDrawable(R.drawable.buttom_navi_word_icon));
        mTabButtons[2].setTitle("聊天");
        mTabButtons[2].setIndex(2);
        mTabButtons[2].setSelectedImage(getResources().getDrawable(R.drawable.buttom_navi_chat_icon_selected));
        mTabButtons[2].setUnselectedImage(getResources().getDrawable(R.drawable.buttom_navi_chat_icon));
        mTabButtons[3].setTitle("我的");
        mTabButtons[3].setIndex(3);
        mTabButtons[3].setSelectedImage(getResources().getDrawable(R.drawable.buttom_navi_mine_icon_selected));
        mTabButtons[3].setUnselectedImage(getResources().getDrawable(R.drawable.buttom_navi_mine_icon));
    }

    private void initUnreadMessageText() {
        mUnreadTextViews = new TextView[3];
        mUnreadTextViews[0] = (TextView) findViewById(R.id.tv_story_message_count_notify);
        mUnreadTextViews[UNREAD_MESS_INDEX_SQUARE] = (TextView) findViewById(R.id.tv_square_message_count_notify);
        mUnreadTextViews[UNREAD_MESS_INDEX_FAMILY] = (TextView) findViewById(R.id.tv_family_message_count_notify);
    }

    private void initFragment() {
        mFragments = new Fragment[4];
        mFragments[0] = getSupportFragmentManager().findFragmentById(R.id.fragment_story);
        mFragments[1] = getSupportFragmentManager().findFragmentById(R.id.fragment_square);
        mFragments[2] = getSupportFragmentManager().findFragmentById(R.id.fragment_chat);
        mFragments[3] = getSupportFragmentManager().findFragmentById(R.id.fragment_my);
    }

    public void setFragmentIndicator(int which) {
        /** 点击按钮的时候隐藏弹出层 */
        if (areButtonsShowing) {
            onClickView(true);
        }
        getSupportFragmentManager().beginTransaction().hide(mFragments[0]).hide(mFragments[1]).hide(mFragments[2]).hide(mFragments[3]).show(mFragments[which]).commit();
        mTabButtons[0].setSelectedButton(false);
        mTabButtons[1].setSelectedButton(false);
        mTabButtons[2].setSelectedButton(false);
        mTabButtons[3].setSelectedButton(false);
        mTabButtons[which].setSelectedButton(true);
    }

    /**
     * 对外暴露的接口，用于设置底部菜单栏按钮右上方未读消息数的显示（提示有新消息）
     * <p/>
     * params unreadCnt ：未读消息数
     * params type  ：   未读消息类型
     * 0   ：   世界（剧）
     * 1   ：   好友
     * 2   ：   家族
     */
    public void setUnreadMessageCnt(int unreadCnt, int type) {
        if (unreadCnt == 0) {
            mUnreadTextViews[type].setVisibility(View.GONE);
            return;
        }
        mUnreadTextViews[type].setVisibility(View.VISIBLE);
        if (unreadCnt > 99) {
            mUnreadTextViews[type].setText("99+");
        } else {
            mUnreadTextViews[type].setText(unreadCnt + "");
        }
    }


    /**
     * 好友按钮的双击事件（这个地方应该让列表滚动到好友或者家族未读消息的顶部，但是由于嵌套的问题，暂时不作处理）
     */
    public void chatDoubleListener() {
    }

    /**
     * singleTask的启动方式（四大启动方式之一，我终于用到了其他的三种方式）
     *
     * @param intent intent
     */
    @Override
    protected void onNewIntent(Intent intent) {
        Log.d("MainActivity", "on new intent is called");

        super.onNewIntent(intent);
        setIntent(intent);
        if (mFragments == null) {
            return;
        }
        setFragmentIndicator(0);

    }


    private void jumpToLoginPage() {
        Intent intent = new Intent(this, LoadingActivity.class);
        intent.putExtra(IntentConstant.KEY_LOGIN_NOT_AUTO, true);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.to_ugc_book:      //创建故事 消除弹层 +下面变化回来
                onClickView(true);
                IMUIHelper.openCreateZiXiActivity(this);
                break;
            case R.id.to_ugc_story:     //创建剧
                onClickView(true);
//                IMUIHelper.openSelectStoryClassActivity(this);
                Intent intent_1 = new Intent(this, CreateStoryFirstActivity.class);
                startActivity(intent_1);
                break;
            case R.id.to_ugc_family:    //创建家族
                onClickView(true);
                IMUIHelper.openCreateFamilyActivity(this);
                break;
            case R.id.to_ugc_topic:     //创建话题
                onClickView(true);
                IMUIHelper.openTopicActivity(this);
                break;
            case R.id.iv_add:           //加号
                onClickView(false);
                break;
            case R.id.dialog_cancel:
                if (null != dialog && dialog.isShowing()) {
                    if (mustFlag.equals("1")) {
                        dialog.dismiss();
                        System.exit(0);
                    } else {
                        dialog.dismiss();
                    }

                }
                break;
            case R.id.dialog_ok:
                if (null != dialog && dialog.isShowing()) {
                    dialog.dismiss();
                    startUpdateActivity();
                    if (mustFlag.equals("1")) {
                        System.exit(0);
                    }
                }
                break;
        }

    }

    public void onClickView(boolean isOnlyClose) {
        if (isOnlyClose) {
            if (areButtonsShowing) {
                showUgcEntranceMenu();
                areButtonsShowing = !areButtonsShowing;
            }
        } else {
            if (!areButtonsShowing) {
                dismissUgcEntranceMenu();
            } else {
                showUgcEntranceMenu();
            }
            areButtonsShowing = !areButtonsShowing;
        }
    }

    private void showUgcEntranceMenu() {
        UgcAnimationUtil.startAnimationsOut(llUgcWrapper, 300);
        iv_add.startAnimation(UgcAnimationUtil
                .getRotateAnimation(-315, 0, 300));
    }

    private void dismissUgcEntranceMenu() {
        UgcAnimationUtil.startAnimationsIn(llUgcWrapper, 300);
        iv_add.startAnimation(UgcAnimationUtil
                .getRotateAnimation(0, -315, 300));
    }

    /**
     * 双击退出应用
     * <p/>
     * 只移动栈中
     */
   /* public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (ChannelSelectDataManager.getInstance().isUnfold()) {
                ChannelSelectEvent channelSelectEvent = new ChannelSelectEvent();
                channelSelectEvent.eventType = ChannelSelectEvent.Event.TYPE_HIDE_MORE;
                EventBus.getDefault().post(channelSelectEvent);
                return true;
            }

            long secondTime = System.currentTimeMillis();
            if (secondTime - firstTime > 1000) {            //如果两次按键时间间隔大于1000毫秒，则不退
                Toast.makeText(MainActivity.this, "再按一次退出", Toast.LENGTH_SHORT).show();
                firstTime = secondTime;
                return true;
            } else {                                        //否则退出程序
                moveTaskToBack(false);
//                finish();
//                System.exit(0);
                return true;
            }
        }
//        moveTaskToBack(false);
        return super.onKeyDown(keyCode, event);
    }*/

    @Override
    public void onBackPressed() {
        if (ChannelSelectDataManager.getInstance().isUnfold()) {
            ChannelSelectEvent channelSelectEvent = new ChannelSelectEvent();
            channelSelectEvent.eventType = ChannelSelectEvent.Event.TYPE_HIDE_MORE;
            EventBus.getDefault().post(channelSelectEvent);
            return;
        }
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }

    /**
     * 处理通知
     */
    static class MyHandler extends Handler {
        WeakReference<MainActivity> mActivity;              //持有MainActivity对象的弱引用

        MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            final MainActivity activity = mActivity.get();
            if (activity == null) {
                return;
            }
            switch (msg.what) {
                case MessageType.HANDLER_MESSAGE_SUCCESS:
                    break;
                case MessageType.HANDLER_MESSAGE_ERROR:
                    Toast.makeText(activity, "数据库存储失败！", Toast.LENGTH_SHORT).show();
                    break;
                case MessageType.HANDLER_MESSAGE_APPLY_GROUP_SUCCESS:
                    break;
                case 0:
                    activity.frameLayout_top.setVisibility(View.VISIBLE);
                    break;
                case 1:
                    activity.frameLayout_top.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        imServiceConnector.unbindService(this);
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onEventMainThread(UnreadNotifyEvent event) {
        switch (event.eventType) {
            case UNREAD_NOTIFY_EVENT:
                ArrayList list = event.list;
                dealWithUnreadNotify(list);
                break;
        }
    }

    private void dealWithUnreadNotify(ArrayList list) {
        final Context context = this;
        for (int i = 0; i < list.size(); i++) {
            UnreadNotifyEntity entity = (UnreadNotifyEntity) list.get(i);
            switch (Integer.valueOf(entity.getType())) {
                case 1:             //添加好友请求
                    int fromid = Integer.valueOf(entity.getFromid());
                    int toid = Integer.valueOf(entity.getToid());           //接受请求者的id

                    /** 去服务器请求申请者的信息，并将其存入到数据库 */
                    YXUnreadNotifyManager.instance().getAddFriendEntity(toid, fromid, entity.getMsgdata(), unreadNotifyHandler);
                    new GGDialog().showTwoSelcetDialog(this, "好友请求", "附加消息：" + entity.getMsgdata(), new GGDialog.OnDialogButtonClickedListenered() {
                        public void onConfirmClicked() {
                            IMUIHelper.openSystemMessageActivity(MainActivity.this);
                        }

                        public void onCancelClicked() {
                        }
                    });
                    break;
                case 2:             //添加好友确认结果
                    /** 更新User信息 */
                    ArrayList<Integer> userIds = new ArrayList<>(1);
                    if (!TextUtils.isEmpty(entity.getToid())) {
                        userIds.add(Integer.valueOf(entity.getToid()));
                        if (imService != null && imService.getContactManager() != null)
                            imService.getContactManager().reqGetDetaillUsers(userIds);
                    }
                    break;
                case 5:             //被邀请加入群
                    String type = "剧/家族";

                    /** 去服务器请求申请者的信息，并将其存入到数据库 */
                    YXUnreadNotifyManager.instance().setApplyGroupEntity(Integer.valueOf(entity.getFromid()), IMLoginManager.instance().getLoginId(), Integer.valueOf(entity.getGroupid()), "1", unreadNotifyHandler, context);

                    new GGDialog().showTwoSelcetDialog(context, type + "邀请", "有一个" + type + "邀请你", new GGDialog.OnDialogButtonClickedListenered() {
                        public void onConfirmClicked() {
                            /** 跳转剧或者家族申请、邀请列表的界面 */
                            IMUIHelper.openSystemMessageActivity(MainActivity.this);
                        }

                        public void onCancelClicked() {
                        }
                    });
                    break;
                case 4:             //被邀请加入群反馈结果
                    break;
                case 3:             //申请加入群
                    String type1 = "剧/家族";
                    YXUnreadNotifyManager.instance().setApplyGroupEntity(IMLoginManager.instance().getLoginId(), Integer.valueOf(entity.getFromid()), Integer.valueOf(entity.getGroupid()), "0", unreadNotifyHandler, context);

                    new GGDialog().showTwoSelcetDialog(context, type1 + "请求", "有一个小弟要投靠你!", new GGDialog.OnDialogButtonClickedListenered() {
                        public void onConfirmClicked() {
                            /** 跳转剧申请、邀请列表的界面 */
                            IMUIHelper.openSystemMessageActivity(MainActivity.this);
                        }

                        public void onCancelClicked() {
                        }
                    });
                    break;
                case 6:             //申请加入群的确认结果
                    break;
            }
        }
        YXUnreadNotifyManager.instance().clearList();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode)
            return;
        switch (requestCode) {
            case ConstantValues.HOME_TYPE_PLAY:
                MyHomePageFragment myHomePageFragment = (MyHomePageFragment) mFragments[3];
                myHomePageFragment.ReqHomeInfo();
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        HomeWatcherReceiver.init().registerHomeKeyReceiver(getApplicationContext());
    }

    /**
     * 获取设备序列码等其他信息
     *
     * @param context context
     * @return 设备信息
     */
    public static String getDeviceInfo(Context context) {
        try {
            org.json.JSONObject json = new org.json.JSONObject();
            android.telephony.TelephonyManager tm = (android.telephony.TelephonyManager) context
                    .getSystemService(Context.TELEPHONY_SERVICE);

            String device_id = tm.getDeviceId();

            android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) context.getSystemService(Context.WIFI_SERVICE);

            String mac = wifi.getConnectionInfo().getMacAddress();
            json.put("mac", mac);

            if (TextUtils.isEmpty(device_id)) {
                device_id = mac;
            }

            if (TextUtils.isEmpty(device_id)) {
                device_id = android.provider.Settings.Secure.getString(context.getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);
            }

            json.put("device_id", device_id);
            return json.toString();
        } catch (Exception e) {

        }
        return null;
    }

    public void onEventMainThread(LoginEvent event) {
        switch (event) {
            case LOGIN_OUT:
                handleOnLogout();
                break;
        }
    }

    private void handleOnLogout() {
        logger.d("MainActivity#login#handleOnLogout");
        logger.d("MainActivity#login#kill self, and start login activity");
        jumpToLoginPage();
        finish();
    }

}

