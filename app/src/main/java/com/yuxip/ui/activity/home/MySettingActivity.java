package com.yuxip.ui.activity.home;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.yuxip.R;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.other.SettingActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.IMUIHelper;

/**
 * Created by Administrator on 2015/5/20.
 * description : 设置界面
 */
public class MySettingActivity extends TTBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View chatView = View.inflate(MySettingActivity.this, R.layout.activity_setting,topContentView);
        setTitle("设置中心");
        setLeftButton(R.drawable.back_default_btn);
        topBar.setBackgroundColor(Color.parseColor("#f5f5f5"));
        //反馈我们
        chatView. findViewById(R.id.feedback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MySettingActivity.this, FeedBackActivity.class);
                startActivity(it);
            }
        });
        //帮助信息
        chatView.findViewById(R.id.helpInfo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it =new Intent(MySettingActivity.this, HelpMessageActivity.class);
                startActivity(it);
            }
        });
        //关于我们
        chatView.findViewById(R.id.aboutme).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it =new Intent(MySettingActivity.this, AboutMeActivity.class);
                startActivity(it);
            }
        });
        //全局消息设置
        chatView.findViewById(R.id.rl_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it =new Intent(MySettingActivity.this, SettingActivity.class);
                startActivity(it);
            }
        });
        //退出
        chatView.findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new GGDialog().showTwoSelcetDialog(MySettingActivity.this, "退出", "确定退出语戏?", new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        IMLoginManager.instance().setKickout(false);
                        IMLoginManager.instance().logOut();
                        IMUIHelper.openLoginActivity(MySettingActivity.this);
                        MySettingActivity.this.finish();
                    }

                    @Override
                    public void onCancelClicked() {
                    }
                });
            }
        });
    }
}
