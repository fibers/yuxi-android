package com.yuxip.ui.activity.story;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.GroupData;
import com.yuxip.JsonBean.GroupDataJsonBean;
import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.activity.other.ReportActivity;
import com.yuxip.ui.adapter.StoryGroupAdapter;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.widget.GroupManagerGridView;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;


/**
 * Created by SummerRC on 2015/5/24.
 * description:家族资料页面
 */
public class StoryGroupDetailsActivity extends TTBaseActivity {
    private List<Members> memberslist = new ArrayList<>();
    private StoryGroupAdapter adapter;
    private String currentSessionKey;
    private PopupWindow mPopupWindow;

    private IMService imService;

    @InjectView(R.id.tvGroupMemberNum)
    TextView tvGroupMemberNum;

    @InjectView(R.id.tvGroupName)
    TextView tvGroupName;

    @InjectView(R.id.tvGroupId)
    TextView tvGroupId;

    @InjectView(R.id.tvGroupCreateName)
    TextView tvGroupCreateName;

    @InjectView(R.id.tvGroupIntro)
    TextView tvGroupIntro;

    @InjectView(R.id.gv_family_member)
    GroupManagerGridView gridView;

    @InjectView(R.id.rl_set_group_name)
    RelativeLayout rl_set_group_name;

    @InjectView(R.id.messageCommBtn)
    LinearLayout llmessageComm;

    @InjectView(R.id.rl_message_setting)
    RelativeLayout rl_message_setting;


    @InjectView(R.id.rl_group_announcement)
    RelativeLayout rl_group_announcement;       //群公告


    private String groupId = "";
    private String storyId = "";


    @InjectView(R.id.tvSettingImg)
    ImageView tvSettingImg;

    @InjectView(R.id.settingGroupName)
    ImageView settingGroupName;

    @InjectView(R.id.rlSetting)
    RelativeLayout rlSetting;

    private Boolean isCreated = false;
    //0 是审核群  1是对戏群
    private String isplay = "";
    private String shenheId;
    private String creatorId;

    private static final int ChangeStoryGroupName = 1;
    private Logger logger;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("groupmgr#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
            if (imService == null) {
                Toast.makeText(StoryGroupDetailsActivity.this,
                        getResources().getString(R.string.im_service_disconnected), Toast.LENGTH_SHORT).show();
                return;
            }

            GetStoryGroupInfo();
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        logger = Logger.getLogger(StoryGroupDetailsActivity.class);
        View.inflate(this, R.layout.activity_story_family_details, topContentView);
        ButterKnife.inject(this, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        setTitle("资料");
        setRightButton(R.drawable.icon_more);

        storyId = getIntent().getStringExtra(IntentConstant.STORY_ID);
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        boolean isFromChat = getIntent().getBooleanExtra(IntentConstant.IS_FROM_CHAT, false);
        currentSessionKey = "2_" + groupId;

        rl_message_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openGroupMessageSettingActivity(StoryGroupDetailsActivity.this, currentSessionKey);
            }
        });

        /**从群聊天进来就不显示聊天按钮*/
        if (isFromChat) {
            llmessageComm.setVisibility(View.INVISIBLE);
        } else {
            llmessageComm.setVisibility(View.VISIBLE);
            llmessageComm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    IMUIHelper.openDramaChatActivity(StoryGroupDetailsActivity.this, currentSessionKey, storyId);
                }
            });
        }

        gridView.setSelector(new ColorDrawable(Color.TRANSPARENT));// 去掉点击时的黄色背影
        gridView.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItem(position) != null) {
                    Members members = (Members) adapter.getItem(position);
                    IMUIHelper.openHisGroupCardActivity(StoryGroupDetailsActivity.this, members.getId(), storyId, groupId, members.getPortrait());
                }
            }
        });
        imServiceConnector.connect(this);
        EventBus.getDefault().register(this);
    }

    /**
     * 获取剧群信息
     */
    private void GetStoryGroupInfo() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("groupid", groupId);
        params.addParams("storyid", storyId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    memberslist.clear();
                    // TODO GSON解析有问题
                    GroupDataJsonBean jsonBean = new Gson().fromJson(response, GroupDataJsonBean.class);
                    if (jsonBean.getResult().equals("1")) {
                        GroupData data = jsonBean.getGroupData();

                        shenheId = data.getJudgegroupid();
                        if(data.getMembers() == null) {
                            //群成员数量
                            tvGroupMemberNum.setText("0人");
                        } else {
                            //群成员数量
                            tvGroupMemberNum.setText(data.getMembers().size() + "人");
                        }

                        //设置剧群名称
                        tvGroupName.setText(data.getName());
                        tvGroupId.setText(data.getId());
                        tvGroupCreateName.setText(data.getOwnernickname());
                        tvGroupIntro.setText(data.getIntro());

                        creatorId = data.getOwnerid();
                        String loginId = IMLoginManager.instance().getLoginId() + "";
                        isplay = data.getIsplay();

                        /** 我的群名片 */
                        rlSetting.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                IMUIHelper.openGroupLabelActivity(StoryGroupDetailsActivity.this, storyId);
                            }
                        });

                        if (!loginId.equals(creatorId)) {
                            tvSettingImg.setVisibility(View.GONE);
                            settingGroupName.setVisibility(View.GONE);
                            isCreated = false;
                        } else {
                            isCreated = true;
                            //设置管理员修改群的名称
                            rl_set_group_name.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(StoryGroupDetailsActivity.this, ChangeStoryGroupName.class);
                                    intent.putExtra(IntentConstant.GROUP_ID, groupId);
                                    intent.putExtra(IntentConstant.CREATOR_ID, creatorId);
                                    startActivityForResult(intent, IntentConstant.ChangeStoryGroupName);
                                }
                            });
                            //设置群的简介
                            tvGroupIntro.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(StoryGroupDetailsActivity.this, ChangeStoryIntro.class);
                                    intent.putExtra(IntentConstant.GROUP_ID, groupId);
                                    intent.putExtra(IntentConstant.CREATOR_ID, creatorId);
                                    startActivity(intent);
                                }
                            });
                        }
                        //群公告
                        rl_group_announcement.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (isCreated) {
                                    IMUIHelper.openAddGroupAnnouncementActivity(StoryGroupDetailsActivity.this, groupId);
                                } else {
                                    IMUIHelper.openGroupAnnouncementActivity(StoryGroupDetailsActivity.this, groupId);
                                }
                            }
                        });
                        memberslist = data.getMembers();
                        /**
                         * list 数据
                         * groupid 群id
                         * isCreated 是否是创建者
                         * isplay 是否是审核群
                         */
                        //TODO  传递审核群id
                        if (adapter == null) {
                            adapter = new StoryGroupAdapter(StoryGroupDetailsActivity.this,
                                    memberslist,
                                    storyId,
                                    groupId,
                                    isCreated,
                                    isplay,
                                    shenheId,
                                    Integer.valueOf(creatorId),
                                    imService);
                            gridView.setAdapter(adapter);
                        } else {
                            adapter.setMemberslist(memberslist);
                        }
                        initPopupWindow();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                    T.show(getApplicationContext(), "未知错误", 0);
                }

            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    /**
     * 初始化PopupWindow
     */
    private void initPopupWindow() {
        View contentView = View.inflate(this, R.layout.pop_drama, null);
        TextView et_exit = (TextView) contentView.findViewById(R.id.tv_exit);
        TextView et_report = (TextView) contentView.findViewById(R.id.popSet);
        View line = contentView.findViewById(R.id.line);
        isCreated = creatorId.equals(IMLoginManager.instance().getLoginId() + "");
        if (isCreated) {
            et_exit.setText("解散");
        } else {
            et_exit.setText("退群");
            line.setVisibility(View.VISIBLE);
            et_report.setVisibility(View.VISIBLE);
        }

        et_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo 退群操作
                String hintText;
                if (isCreated) {
                    hintText = (String) getResources().getText(R.string.dialog_family_dismiss_content);
                } else {
                    hintText = (String) getResources().getText(R.string.dialog_family_exit_content);
                }

                new GGDialog().showTwoSelcetDialog(StoryGroupDetailsActivity.this,
                        (String) getResources().getText(R.string.dialog_normal_title),
                        hintText,
                        new GGDialog.OnDialogButtonClickedListenered() {
                            @Override
                            public void onConfirmClicked() {

                                Integer userId = IMLoginManager.instance().getLoginId();

                                imService.getGroupManager().quitOrDismissGroup(Integer.valueOf(groupId),
                                        isCreated);

                                Toast.makeText(StoryGroupDetailsActivity.this,
                                        R.string.dialog_family_dismiss_action,
                                        Toast.LENGTH_SHORT).show();
                                finish();
                            }

                            @Override
                            public void onCancelClicked() {

                            }
                        });

                mPopupWindow.dismiss();

            }
        });


        et_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StoryGroupDetailsActivity.this, ReportActivity.class);
                intent.putExtra(IntentConstant.REPORT_TYPE, IntentConstant.STORY_ID);
                intent.putExtra(IntentConstant.STORY_ID, storyId);
                startActivity(intent);

            }
        });

        mPopupWindow = new PopupWindow(contentView, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setRightButton(R.drawable.icon_more);
        topRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPopupWindow.showAsDropDown(topRightBtn);
            }
        });
    }

    @Override
    protected void onDestroy() {
        imServiceConnector.disconnect(this);
        ButterKnife.reset(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    /**
     * 事件驱动通知
     */
    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CHANGE_GROUP_MEMBER_SUCCESS:
                int tempId = event.getGroupEntity().getPeerId();
                if (tempId == Integer.valueOf(groupId)) {
                    GetStoryGroupInfo();
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (RESULT_OK != resultCode || data == null) {
            return;
        }
        switch (requestCode) {
            case IntentConstant.ChangeStoryGroupName:
                String content = data.getStringExtra(IntentConstant.GROUP_NAME);
                tvGroupName.setText(content);
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
