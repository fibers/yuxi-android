package com.yuxip.ui.activity.base;

import android.support.v4.app.FragmentActivity;

import com.umeng.analytics.MobclickAgent;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.SharedPreferenceUtils;

/**
 * 相关Activity的超类
 */
public abstract class TTBaseNewFragmentActivity extends FragmentActivity {
    public boolean isNeedRestart() {
        boolean result;
        result = SharedPreferenceUtils.getBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
        if (result) {
            SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
            IMUIHelper.openSplashActivity(this);
            this.finish();
        }
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name = getClass().getSimpleName();
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(name);
        SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        /** 获取子类名 */
        String name = getClass().getSimpleName();
        MobclickAgent.onPageEnd(name);
        MobclickAgent.onPause(this);
    }
}
