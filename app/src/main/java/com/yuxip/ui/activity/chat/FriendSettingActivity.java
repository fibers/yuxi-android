package com.yuxip.ui.activity.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.RemarkNameManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Edit by SummerRC on 2015/9/2.
 * description:好友设置界面
 */
public class FriendSettingActivity extends TTBaseActivity implements View.OnClickListener {
    @InjectView(R.id.rel_nickname_more_friendsetting)
    RelativeLayout rel_nickname;                        //备注
    @InjectView(R.id.rel_group_more_friendsetting)
    RelativeLayout rel_group;                           //分组
    @InjectView(R.id.rel_power_more_friendsetting)
    RelativeLayout rel_power;                           //权限设置
    @InjectView(R.id.tv_nickname_more_friendsetting)
    TextView tv_remarkName;
    @InjectView(R.id.tv_group_more_friendsetting)
    TextView tv_group;

    private String personid;
    private String remarkName = "";
    private String groupName = "";
    private FG_GroupJsonBean fg_groupJsonBean;          //群组信息
    private boolean isChanged = false;                  //标志备注是否被修改以便刷新上一级界面

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = getLayoutInflater().inflate(R.layout.activity_more_fridendsetting, topContentView);
        ButterKnife.inject(this, view);
        initView();
    }

    private void initView() {
        /** 设置导航栏 */
        setTitle("更多");
        topBar.setBackgroundColor(Color.parseColor("#f5f5f5"));
        /** 返回箭头 */
        topLeftBtn.setVisibility(View.VISIBLE);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChanged) {
                    Intent intent = new Intent();
                    intent.putExtra(IntentConstant.REMARK_NAME, remarkName);
                    setResult(RESULT_OK, intent);
                }
                finish();
            }
        });
        /** 举报 */
        righTitleTxt.setText("举报");
        righTitleTxt.setTextSize(12);
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openReportPersonActivity(FriendSettingActivity.this, personid);
            }
        });

        personid = getIntent().getStringExtra(IntentConstant.PERSION_ID);
        fg_groupJsonBean = FriendGroupManager.getInstance().getGroupByUid(personid);
        if (fg_groupJsonBean != null) {
            groupName = fg_groupJsonBean.getGroupname();
        } else {
            groupName = "";
        }
        remarkName = RemarkNameManager.getInstance().getNickNameByUserId(personid);
        tv_remarkName.setText(remarkName);
        tv_group.setText(groupName);
        rel_nickname.setOnClickListener(this);
        rel_group.setOnClickListener(this);
        rel_power.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rel_nickname_more_friendsetting:      //修改备注
                IMUIHelper.openEditRemarkNameActivity(FriendSettingActivity.this, personid, remarkName);
                break;
            case R.id.rel_group_more_friendsetting:         //修改分组
                if (fg_groupJsonBean == null) {
                    T.show(getApplicationContext(), "请刷新分组列表之后重试", 0);
                    return;
                }
                IMUIHelper.openEditFriendGroupActivity(FriendSettingActivity.this, personid, fg_groupJsonBean.getGroupid());
                break;
            case R.id.rel_power_more_friendsetting:         //修改权限

                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (isChanged) {
            Intent intent = new Intent();
            intent.putExtra(IntentConstant.REMARK_NAME, remarkName);
            setResult(RESULT_OK, intent);
        }
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            case SysConstant.ACTIVITY_EDIT_REMARK_NAME:     //好友昵称修改成功
                isChanged = true;
                remarkName = data.getStringExtra(IntentConstant.REMARK_NAME);
                tv_remarkName.setText(remarkName);
                break;
            case SysConstant.ACTIVITY_FRIEND_MODIFY_GROUP:  //修改好友分组
                fg_groupJsonBean = FriendGroupManager.getInstance().getGroupByUid(personid);
                groupName = fg_groupJsonBean.getGroupname();
                tv_group.setText(groupName);
                break;
        }
    }

}
