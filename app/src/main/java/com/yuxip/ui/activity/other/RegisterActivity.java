package com.yuxip.ui.activity.other;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.telephony.SmsMessage;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.im.Security;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/4/27.
 * 注册界面
 */
public class RegisterActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(RegisterActivity.class);
    private IMService imService;
    private boolean loginSuccess = false;
    private boolean isClickable = true;


    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    private EditText et_input_phone_number;
    private EditText register_code;
    private String mobileno;
    private String password;
    private String vCode;
    private BroadcastReceiver smsReceiver;
    private MyHandler handler;
    private EditText loginPwd;
    private String strContent;
    private TimeCount time;
    private TextView testGetCode;
    private String mPassword;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        imServiceConnector.connect(RegisterActivity.this);
        EventBus.getDefault().register(this);
        time = new TimeCount(60000, 1000);//构造CountDownTimer对象
        initView();
        initViewById();
        initEnterCode();
        initSpannableString();
    }


    private void initView() {
        /** 绑定布局资源(注意放所有资源初始化之前) */
        LayoutInflater.from(this).inflate(R.layout.mb_activity_register, topContentView);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("注册");
    }

    /**
     * 自动输入短信验证码
     */
    private void initEnterCode() {
        handler = new MyHandler(this);
        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("android.provider.Telephony.SMS_RECEIVED");
        filter2.setPriority(Integer.MAX_VALUE);
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Object[] objects = (Object[]) intent.getExtras().get("pdus");
                if(objects==null) {
                    return;
                }
                for (Object obj : objects) {
                    byte[] pdu = (byte[]) obj;
                    SmsMessage sms = SmsMessage.createFromPdu(pdu);
                    /** 短信的内容 */
                    String message = sms.getMessageBody();
                    logger.d("logo", "message     " + message);
                    /** 短息的手机号。。+86开头？ */
                    String from = sms.getOriginatingAddress();
                    logger.d("logo", "from     " + from);
                    if (!TextUtils.isEmpty(from)) {
                        String code = patternCode(message);
                        if (!TextUtils.isEmpty(code)) {
                            strContent = code;
                            handler.sendEmptyMessage(1);
                        }
                    }
                }
            }
        };
        registerReceiver(smsReceiver, filter2);
    }

    private void initViewById() {
        et_input_phone_number = (EditText) findViewById(R.id.et_input_phone_number);
        register_code = (EditText) findViewById(R.id.register_code);
        loginPwd = (EditText) findViewById(R.id.loginPwd);
        testGetCode = (TextView) findViewById(R.id.testGetCode);
        testGetCode.setOnClickListener(this);
        findViewById(R.id.registerSubmit).setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.pb_register_activity);
    }


    /**
     * 注册完成
     */
    private void registerFinish() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("rid", "mb:" + mobileno);
        params.addParams("gender", "0");
        params.addParams("password", password);
        OkHttpClientManager.postAsy(ConstantValues.Register, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("result").equals("1")) {
                                Toast.makeText(getApplicationContext(), "注册成功", Toast.LENGTH_SHORT).show();
                                attemptLogin("mb:" + mobileno, mPassword);
                            } else {
                                T.show(getApplicationContext(), obj.getString("describe"), Toast.LENGTH_SHORT);
                                progressBar.setVisibility(View.GONE);
                                isClickable = true;
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                            progressBar.setVisibility(View.GONE);
                            isClickable = true;
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), e.toString(), Toast.LENGTH_LONG);
                        progressBar.setVisibility(View.GONE);
                        isClickable = true;
                    }
                });
    }

    /**
     * QQ注册成功登陆
     */
    private void attemptLogin(String loginName, String mPassword) {
        if (imService != null) {
            //im服务 获取登陆管理  然后进行登陆操作
            imService.getLoginManager().login(loginName, mPassword);
        }
    }

    /**
     * 校验验证码是否正确
     */
    private void VCodeCheck() {
        if(TextUtils.isEmpty(mobileno) || TextUtils.isEmpty(vCode)) {
            T.show(getApplicationContext(), "验证码和手机号不能为空", 0);
            return;
        }
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", "0");
        params.addParams("mobileno", mobileno);
        params.addParams("vcode", vCode);
        OkHttpClientManager.postAsy(ConstantValues.VCODECHECK, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject obj = new JSONObject(response);
                            if (obj.getString("result").equals("1")) {
                                registerFinish();
                            } else {
                                Toast.makeText(getApplicationContext(), obj.getString("describe"), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                isClickable = true;
                            }
                        } catch (Exception e) {
                            logger.e(e.toString());
                            T.show(getApplicationContext(), e.toString(), 1);
                            progressBar.setVisibility(View.GONE);
                            isClickable = true;
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        T.show(getApplicationContext(), e.toString(), 1);
                        progressBar.setVisibility(View.GONE);
                        isClickable = true;
                    }
                });
    }

    /**
     * 获取验证码
     */
    private void TestGetCode() {
        String uid = IMLoginManager.instance().getLoginId() + "";
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("mobileno", mobileno);

        OkHttpClientManager.postAsy(ConstantValues.TESTGETCODE, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "发送验证码成功", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 匹配短信中间的4个数字（验证码等）
     *
     * @param patternContent String
     * @return String
     */
    private String patternCode(String patternContent) {
        if (TextUtils.isEmpty(patternContent)) {
            return null;
        }
        String patternCoder = "(?<!\\d)\\d{4}(?!\\d)";
        Pattern p = Pattern.compile(patternCoder);
        Matcher matcher = p.matcher(patternContent);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(smsReceiver);                //关闭广播
        EventBus.getDefault().unregister(this);
        imServiceConnector.disconnect(RegisterActivity.this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerSubmit:
                if (!isClickable) {
                    return;
                }
                isClickable = false;
                mPassword = loginPwd.getText().toString().trim();
                password = new String(Security.getInstance().EncryptPass(mPassword));
                /** 点击下一步的时候要校验验证是fou正确 */
                vCode = register_code.getText().toString().trim();
                if (TextUtils.isEmpty(vCode) || TextUtils.isEmpty(mPassword)) {
                    T.show(getApplicationContext(), "密码不能为空", 0);
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    VCodeCheck();
                }
                break;

            case R.id.testGetCode:
                mobileno = et_input_phone_number.getText().toString().trim();
                if (mobileno.length() != 11) {
                    T.show(getApplicationContext(), "你输入的不是手机号", 0);
                } else {
                    time.start();       //开始计时
                    TestGetCode();
                }
                break;
        }
    }


    class TimeCount extends CountDownTimer {
        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);       //参数依次为总时长,和计时的时间间隔
        }

        @Override
        public void onFinish() {                            //计时完毕时触发
            testGetCode.setText("获取验证码");
            testGetCode.setClickable(true);
        }

        @Override
        public void onTick(long millisUntilFinished) {      //计时过程显示
            testGetCode.setClickable(false);
            testGetCode.setText(millisUntilFinished / 1000 + "秒");
        }
    }

    /**
     * 初始化超链接
     */
    private void initSpannableString() {
        TextView tv_registerClause = (TextView) findViewById(R.id.registerClause);
        /** 创建一个 SpannableString对象 */
        SpannableString sp = new SpannableString("我已阅读并同意<<语戏用户协议>>");
        /** 设置超链接 */
        sp.setSpan(new URLSpan(ConstantValues.GetContact), 0, 8, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_registerClause.setText(sp);
        /** 设置TextView可点击 */
        tv_registerClause.setMovementMethod(LinkMovementMethod.getInstance());
    }


    /**
     * ----------------------------event 事件驱动----------------------------
     */
    public void onEventMainThread(LoginEvent event) {
        switch (event) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGIN_OK:               //登陆成功后
                onLoginSuccess();
                break;
            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED:
                if (!loginSuccess) {
                    onLoginFailure(event);
                    progressBar.setVisibility(View.GONE);
                    isClickable = true;
                }
                break;
        }
    }


    public void onEventMainThread(SocketEvent event) {
        switch (event) {
            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                if (!loginSuccess) {
                    onSocketFailure(event);
                    progressBar.setVisibility(View.GONE);
                    isClickable = true;
                }

                break;
        }
    }

    //登陆成功后的操作
    private void onLoginSuccess() {
        logger.i("login#onLoginSuccess");
        loginSuccess = true;
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        RegisterActivity.this.finish();
    }

    //登陆失败后
    private void onLoginFailure(LoginEvent event) {
        logger.e("login#onLoginError -> errorCode:%s", event.name());
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "1", Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        logger.e("login#onLoginError -> errorCode:%s,", event.name());
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        logger.d("login#errorTip:%s", errorTip);
        Toast.makeText(this, errorTip + "2", Toast.LENGTH_SHORT).show();
    }


    static class MyHandler extends Handler {
        WeakReference<RegisterActivity> activityWeakReference;         //持有FriendFragment对象的弱引用

        MyHandler(RegisterActivity activity) {
            activityWeakReference = new WeakReference<>(activity);
        }

        public void handleMessage(Message msg) {
            final RegisterActivity activity = activityWeakReference.get();
            if (activity == null) {
                return;
            }
            activity.register_code.setText(activity.strContent);
        }
    }

}
