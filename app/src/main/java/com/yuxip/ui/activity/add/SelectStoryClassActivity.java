package com.yuxip.ui.activity.add;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.adapter.SelectStoryClassRecyclerAdapter;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by HTP on 2015/07/13.
 * description : 选择创建剧的类别
 */
public class SelectStoryClassActivity extends TTBaseActivity {

    @InjectView(R.id.select_recycler_view)
    UltimateRecyclerView selectRecyclerView;
    SelectStoryClassRecyclerAdapter adapter;
    private List<StoryClass.ListEntity> listEntities;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = LayoutInflater.from(this)
                .inflate(R.layout.activity_select_story_class, topContentView);
        ButterKnife.inject(this, view);

        setTitle("类别");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("选择");
        setRighTitleTextColor(getResources().getColor(R.color.pink));
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        selectRecyclerView.setLayoutManager(layoutManager);
        selectRecyclerView.setHasFixedSize(true);

        request();

        setRighTitleTextClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(adapter == null) {
                    return;
                }
                String ids = adapter.getSelectIds();
                if (TextUtils.isEmpty(ids)) {
                    T.show(getApplicationContext(), "请至少选择一种类别", 0);
                    return;
                }
                IMUIHelper.openCreateStoryActivity(SelectStoryClassActivity.this, ids);
            }
        });
    }

    /**
     * 获取剧的种类
     */
    private void request() {
        OkHttpClientManager.getAsyn(ConstantValues.GetStoryCategory, new OkHttpClientManager.ResultCallback<String>() {
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    StoryClass sc = new Gson().fromJson(content, StoryClass.class);
                    List<StoryClass.ListEntity> list = sc.getList();
                    if (null == listEntities) {
                        listEntities = list;
                        adapter = new SelectStoryClassRecyclerAdapter(listEntities);
                        selectRecyclerView.setAdapter(adapter);
                    }
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }
}
