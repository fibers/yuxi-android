package com.yuxip.ui.activity.story;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.CurseMemberListGroupAdapter;
import com.yuxip.utils.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * \@ 出现的界面     使用了回掉，该Activity通过startActivityForResult启动
 * Created by SummerRC on 2015/6/15.
 */
public class CurseMemberListActivity extends TTBaseNewActivity {
    private Logger logger = Logger.getLogger(CurseMemberListActivity.class);

    @InjectView(R.id.memberslist)
    ListView membersList;

    @InjectView(R.id.topRightAdd)
    TextView topRightAdd;

    @InjectView(R.id.tv_invited_friends)
    TextView tv_invited_friends;

    @InjectView(R.id.tv_invited_Members)
    TextView tv_invited_Members;

    private CurseMemberListGroupAdapter adapter1;
    private String groupId;     //审核群Id
    private String storyId;
    private List<Members> list = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_curse_member_list);
        ButterKnife.inject(CurseMemberListActivity.this);
        initView();
        initData();
    }


    private void initView() {

        findViewById(R.id.black).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        topRightAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapter1 == null || adapter1.getCheckListId().size() <= 0) {
                    Toast.makeText(CurseMemberListActivity.this, "请选择添加成员", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent();
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("name", adapter1.getCheckListName());
                bundle.putIntegerArrayList("id", adapter1.getCheckListId());
                intent.putExtras(bundle);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        /** 点击好友就显示好友列表 */
        tv_invited_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initData();
            }
        });

        /** 点击选择群成员就显示审核群成员列表 */
        tv_invited_Members.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initData();
            }
        });
    }

    private void initData() {
        groupId = getIntent().getStringExtra("groupid");
        storyId = getIntent().getStringExtra("storyid");

        GetStoryGroupInfo();
    }

    /**
     * 获取审核群列表
     */
    private void GetStoryGroupInfo() {
        if(TextUtils.isEmpty(groupId))
            return;
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("groupid", groupId);

        OkHttpClientManager.postAsy(ConstantValues.GetCommentGroupDetail, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    if(membersList == null) {
                        return;
                    }
                    JSONObject jo = new JSONObject(response);
                    if (jo.getString("result").equals("1")) {
                        JSONArray jsonArray = jo.getJSONArray("userlist");
                        if (null != jsonArray && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Members members = new Members();
                                JSONObject j = jsonArray.getJSONObject(i);
                                members.setId(j.getString("userid"));
                                members.setNickname(j.getString("nickname"));
                                members.setPortrait(j.getString("portrait"));
                                list.add(members);
                            }
                            adapter1 = new CurseMemberListGroupAdapter(CurseMemberListActivity.this, list);
                            membersList.setAdapter(adapter1);
                            membersList.setOnScrollListener(
                                    new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
