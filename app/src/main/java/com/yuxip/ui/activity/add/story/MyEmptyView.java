package com.yuxip.ui.activity.add.story;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.R;

/**
 * Created by Administrator on 2015/11/6.
 * description:
 */
public class MyEmptyView extends RelativeLayout {
    private Context context;
    private TextView tvMsg;
    private TextView tvFail;
    private ProgressBar pb;

    public MyEmptyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        inintRes();
    }

    public MyEmptyView(Context context) {
        super(context);
        this.context = context;
        inintRes();
    }

    public MyEmptyView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        inintRes();

    }

    private void inintRes(){
        LayoutInflater.from(context).inflate(R.layout.customview_empty_view, this, true);
        tvMsg = (TextView) findViewById(R.id.tv_msg_empty_view);
        tvFail = (TextView) findViewById(R.id.tv_fail_empty_view);
        pb = (ProgressBar) findViewById(R.id.pb_empty_view);
    }

    public void showMsgText(String msg){
        tvMsg.setText(msg);
        this.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.VISIBLE);
        pb.setVisibility(View.GONE);
        tvFail.setVisibility(View.GONE);
    }

    public  void showFailText(String msg){
        tvFail.setText(msg);
        this.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.GONE);
        pb.setVisibility(View.GONE);
        tvFail.setVisibility(View.VISIBLE);
    }

    public void showProgressBar(){
        this.setVisibility(View.VISIBLE);
        pb.setVisibility(View.VISIBLE);
        tvMsg.setVisibility(View.GONE);
        tvFail.setVisibility(View.GONE);
    }

    public void setFailTextClick(OnClickListener listener){
        tvFail.setOnClickListener(listener);
    }

}
