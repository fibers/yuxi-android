package com.yuxip.ui.activity.square;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.yuxip.JsonBean.TopicCommentDetailJsonBean;
import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.ResponseFloorEvent;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.ResponseFloorManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.CommentInputView;
import com.yuxip.ui.customview.CommentMainItem;
import com.yuxip.ui.customview.PopComment;
import com.yuxip.ui.customview.SquareFootView;
import com.yuxip.utils.IMUIHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/10/10.
 * 每个楼层回复的主要内容的界面 可以在此回复并返回要更新 主楼界面
 */
public class ResponseFloorActivity extends TTBaseActivity implements View.OnClickListener{
    private CommentInputView commentInputView;
    private CommentMainItem commentMainItem;
    private RelativeLayout rel_cover;
    private PopComment popComment;
    private ImageView iv_pop_start;
    private TextView tv_head_content;
    private EditText edit_input;
    private TextView tv_send;
    private ListView listView;

    private SquareFootView footView;
    private TextView tv_loadfail;
    private TextView tv_nomoredata;     //footview中无内容时 点击 加载可能更新出的内容
//    private TextView tv_foot_content;
    private boolean isFirstLoad  =true;
    private boolean canLoad  = false;
    private int index ;
    private int count = 25;
    private final int defaultCount = 25;

    private MyResponseAdapter adapter;
    private TopicCommentDetailJsonBean topicCommentDetailJsonBean;
    private List<TopicDetailsJsonBean.CommentEntity.ChildCommentEntity> list_child_comment = new ArrayList<>();
    private TopicDetailsJsonBean.CommentEntity commentEntity;
    private InputMethodManager inputMethodManager;


    private int deletePosition;
    private boolean responseFloor = true; //判断是回复楼主还是层主  默认回复楼主
    private String toUserName;      //回复的层主 这里使用要先判断是否是回复楼主
    private String toUserId = "";    //回复的层主的id 如果是楼主 则是楼主id 反之 是层主id
    private String floorUserId;      //当前楼层所有者的id 这个要 要判断是否是楼主
    private String topicId;          //当前话题的id  或 当前 剧 或自戏的id
    private String storyId;         //当前 剧 或自戏的id
    private String commentId;        //当前楼层的评论id
    private String childTopicId;     //当前选中回复评论的 子楼层 评论id
    private String content;
    private int type;               //类型 0 表示 剧或 自戏  1  表示话题
    private String commentUrl;
    private String deleteUrl;
    private String requestUrl;
    private ResponseFloorManager responseFloorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_comment_response, topContentView);
        EventBus.getDefault().register(this);
        initRes();
        getIntentRes();
        setListener();
        responseFloorManager.RequestCommentList(type,topicId,storyId,commentId,index,count,requestUrl);
    }

    private void initRes(){
        setLeftButton(R.drawable.back_default_btn);
        inputMethodManager   = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
        responseFloorManager = ResponseFloorManager.getInstance(this);
        commentInputView = (CommentInputView) findViewById(R.id.input_comment_response);
        listView = (ListView) findViewById(R.id.listview_responsefloor);
        rel_cover = (RelativeLayout) findViewById(R.id.rel_response_floor_activity);
        edit_input = (EditText) commentInputView.getInputChildView(CommentInputView.InputViewEnum.EDIT_TEXT_CONTENT);
        tv_send = (TextView) commentInputView.getInputChildView(CommentInputView.InputViewEnum.TEXTVIEW_SEND);

        //listivew footview布局
        footView =new SquareFootView(this);
        tv_loadfail = (TextView) footView.getChildView(SquareFootView.ViewEnum.TEXTVIEW_LOADFAIL);
        tv_nomoredata = (TextView) footView.getChildView(SquareFootView.ViewEnum.TEXTVIEW_NOMOREDATA);
        commentInputView.showInput();

        tv_send.setOnClickListener(this);
        commentMainItem = new CommentMainItem(this);
        listView.addHeaderView(commentMainItem);
        listView.addFooterView(footView);
//        footView.setVisibility(View.GONE);
        footView.showLoadMore();
        adapter = new MyResponseAdapter();
        listView.setAdapter(adapter);
        popComment = (PopComment) commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.POP_COMMENT);
        iv_pop_start = (ImageView) commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.IMAGEVIEW_START_POP);
        tv_head_content = (TextView) commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.TEXTVIEW_CONTENT);
        iv_pop_start.setOnClickListener(this);
        popComment.getCommentView().setOnClickListener(this);
        tv_head_content.setOnClickListener(this);
    }


    private void setListener(){
        rel_cover.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                popComment.hideAnimation();
                return false;
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_FLING || scrollState == SCROLL_STATE_TOUCH_SCROLL) {
                    commentInputView.setVisibility(View.GONE);
                    inputMethodManager.hideSoftInputFromWindow(edit_input.getWindowToken(), 0);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    if (canLoad) {
                        load();
                    }
                }

            }
        });

        tv_loadfail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                footView.showLoadMore();
                responseFloorManager.RequestCommentList(type, topicId, storyId, commentId, index, count, requestUrl);
            }
        });

        tv_nomoredata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(list_child_comment!=null) {
                    footView.showLoadMore();
                    if(list_child_comment.size()>0){
                        index = 1;
                        count = list_child_comment.size();
                    }else{
                        index = 0;
                        count = defaultCount;
                    }
                    responseFloorManager.RequestCommentList(type, topicId, storyId, commentId, index, count, requestUrl);
                }
            }
        });
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void getIntentRes(){
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            type = 0;
            storyId = SquareCommentManager.getInstance().getStoryId();
            requestUrl = ConstantValues.StoryCommentDetail;
            commentUrl = ConstantValues.CommentStoryResource;
            deleteUrl  = ConstantValues.DeleteStoryResource;

        }else {
            type = 1;
            topicId = SquareCommentManager.getInstance().getTopicId();
            requestUrl =ConstantValues.TopicCommentDetail;
            commentUrl = ConstantValues.CommentSquareResource;
            deleteUrl = ConstantValues.DeleteSquareResource;
        }
        if(getIntent().getStringExtra(IntentConstant.FLOOR_NUMBER)!=null){
            setTitle("第"+getIntent().getStringExtra(IntentConstant.FLOOR_NUMBER)+"楼");
        }

        if(getIntent().getStringExtra(IntentConstant.FLOOR_USERNAME)!=null){
            toUserName = getIntent().getStringExtra(IntentConstant.FLOOR_USERNAME);
            edit_input.setText(SquareCommentManager.getInstance().constructCommentContent(toUserName));
            edit_input.setSelection(edit_input.getText().toString().indexOf("：")+1);
            responseFloor =false;
        }
        if(getIntent().getStringExtra(IntentConstant.FlOOR_USERID)!=null){
            floorUserId = toUserId = getIntent().getStringExtra(IntentConstant.FlOOR_USERID);
        }
        if(getIntent().getStringExtra(IntentConstant.FLOOR_COMMENTID)!=null){
            commentId =getIntent().getStringExtra(IntentConstant.FLOOR_COMMENTID);
        }

        if(getIntent().getStringExtra(IntentConstant.FLOOR_CHILD_TOPICID)!=null){
            childTopicId = getIntent().getStringExtra(IntentConstant.FLOOR_CHILD_TOPICID);
        }

    }

      private void firstLoadData(){
          if(topicCommentDetailJsonBean==null){
              Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_loading_fail),Toast.LENGTH_LONG).show();
              return;
          }
          commentEntity = topicCommentDetailJsonBean.getMainComment();
          if(commentEntity !=null){
              commentMainItem.setData(commentEntity);
          }

          addChildList(topicCommentDetailJsonBean.getChildComment());
          if(list_child_comment.size()==0){
              commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.GONE);
              footView.showNoMoreData();
              canLoad =false;
          }else if(list_child_comment.size()>0&&list_child_comment.size()<count){
              commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.VISIBLE);
              footView.showNoMoreData();
              adapter.notifyDataSetChanged();
              canLoad =false;
          }else{
              commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.VISIBLE);
              footView.showLoadMore();
              adapter.notifyDataSetChanged();
              canLoad =true;
          }

          if(!TextUtils.isEmpty(commentEntity.getIsCollection())&&commentEntity.getIsCollection().equals("1")){
              popComment.setCollectSelect(true);
          }else{
              popComment.setCollectSelect(false);
          }
      }

    /**
     *  将请求的 list集合 转成 CommentEnity中形式 加到集合中
     * @param list_child_comment
     */
    private void  addChildList(List<TopicCommentDetailJsonBean.ChildCommentEntity> list_child_comment){
        if(list_child_comment!=null&&list_child_comment.size()>0){
            for(int i=0;i<list_child_comment.size();i++){
                this.list_child_comment.add(list_child_comment.get(i));
            }
        }

    }

    private void load(){
        if(!canLoad){
            return;
        }
        canLoad = false;
        index +=1;
        responseFloorManager.RequestCommentList(type, topicId, storyId, commentId, index, count, requestUrl);

    }

    @Override
    public void onClick(View v) {
        if(topicCommentDetailJsonBean==null)
            return;
      if(v == tv_send){
          if(TextUtils.isEmpty(edit_input.getText().toString())){
              Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_input_nodata),Toast.LENGTH_LONG).show();
              return;
          }
          //如果返回的为null 则为 选中回复层主 但未填写内容
          content =constructMessageContent();
          if(content ==null)
              return;
          if(responseFloor){
              toUserId = floorUserId;
          }
          if(toUserId==null)
              return;
          responseFloorManager.commentSquareResource(responseFloor,commentId,childTopicId,toUserId,content,commentUrl);
          inputMethodManager.hideSoftInputFromWindow(edit_input.getWindowToken(), 0);
          edit_input.setText("");
      }else{
          switch (v.getId()){
              case R.id.iv_pop_comment:
                  if(SquareCommentManager.getInstance().getFoldPopPosition() == 1){
                      SquareCommentManager.getInstance().resetFoldPopPosition();
                      return;
                  }
                  SquareCommentManager.getInstance().setFoldPopPosition(1);
                  popComment.setVisibility(View.VISIBLE);
                  popComment.showOrHidePop();
                  break;
              case R.id.rel_comment_popcomment:
                  responseFloor = true;
                  edit_input.setText("");
                  commentInputView.setVisibility(View.VISIBLE);
                  edit_input.requestFocus();
                  break;
              case R.id.tv_content_floor_comment:
//                  edit_input.setText("");
                  commentInputView.setVisibility(View.VISIBLE);
                  edit_input.requestFocus();
                  break;
          }
      }
    }

    public void onEventMainThread(ResponseFloorEvent event){
        switch (event.eventType){
            case TYPE_LOAD_COMMENTS_SUCCESS:
                onReceiveData(event.netResponse);
                break;
            case TYPE_LOAD_COMMENTS_FAIL:
                footView.showLoadFail();
                break;
            case TYPE_COMMENT_FLOOR_SUCCESS:
                onReceiveResponseFloorData(event.netResponse);
                break;
            case TYPE_COMMENT_FLOOR_FAIL:
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_loading_fail),Toast.LENGTH_LONG).show();
                break;
            case TYPE_DELETE_CHILD_COMMENT_SUCCESS:
                onReceiveDeleteChildFloorData(event.netResponse);
                break;
            case TYPE_DELETE_CHILD_COMMENT_FAIL:
                Toast.makeText(ResponseFloorActivity.this, getResources().getString(R.string.square_loading_fail), Toast.LENGTH_LONG).show();
                break;
        }

    }


    /**
     *     回复某子层 默认格式为" 回复"+"名字"+" :"开头
     *
     *     用户破坏这个形式 就默认为回复该层层主
     */
    private String constructMessageContent(){
        String str = "";
        if(responseFloor){
            str += edit_input.getText().toString();
        }else{
            //回复子楼层  层主 如果用户改变起始结构 则认为是回复主层主
            if(edit_input.getText().toString().startsWith(SquareCommentManager.getInstance().constructCommentContent(toUserName))){
                str =edit_input.getText().toString().substring(edit_input.getText().toString().indexOf("：")+1);
                if(TextUtils.isEmpty(edit_input.getText().toString().substring(edit_input.getText().toString().indexOf("：")+1).trim())){
                    Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_response_nodata),Toast.LENGTH_LONG).show();
                    return null;
                }
            }else{
                responseFloor = true;
                str +=edit_input.getText().toString();
            }
        }
        return  str;
    }


    @Override
    public void finish() {
        if(inputMethodManager!=null&&edit_input!=null){
            inputMethodManager.hideSoftInputFromWindow(edit_input.getWindowToken(),0);
        }
        super.finish();
    }

    @Override
    protected void onDestroy() {
        if (commentEntity != null) {
            SquareCommentEvent squareCommentEvent = new SquareCommentEvent();

            if(canLoad){
                commentEntity.setTotalChildCommentCount(Integer.valueOf(commentEntity.getTotalChildCommentCount())-1+"");
            }else{
                commentEntity.setTotalChildCommentCount(list_child_comment.size()+"");
            }
            /**广场默认最多显示25个 */
            if(list_child_comment.size()>25){
                List<TopicDetailsJsonBean.CommentEntity.ChildCommentEntity> list_comment = new ArrayList<>();
                for(int i =0;i<25;i++){
                    list_comment.add(list_child_comment.get(i));
                }
                commentEntity.setChildComment(list_comment);
            }else{
                commentEntity.setChildComment(list_child_comment);
            }
            squareCommentEvent.commentEntity =commentEntity;
            squareCommentEvent.commentId = commentEntity.getCommentID();
            squareCommentEvent.eventType = SquareCommentEvent.EventType.TYPE_RESET_CHILD_COMMENT;
            EventBus.getDefault().post(squareCommentEvent);
        }
        EventBus.getDefault().unregister(this);
        super.onDestroy();


    }

    class MyResponseAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return list_child_comment==null?0:list_child_comment.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView ==null){
                convertView = LayoutInflater.from(ResponseFloorActivity.this).inflate(R.layout.list_item_child_response,null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.view_line_top.setVisibility(View.GONE);
            if(position ==0){
                viewHolder.view_line_top.setVisibility(View.VISIBLE);
            }
            /**   这里是设置楼层的内容       */
            viewHolder.tv_child_content.setTag(position);
            SpannableStringBuilder spannableStringBuilder = strToSpann(Integer.valueOf(viewHolder.tv_child_content.getTag().toString()));
            viewHolder.tv_child_content.setText(spannableStringBuilder);
            viewHolder.tv_child_content.setMovementMethod(LinkMovementMethod.getInstance());
            viewHolder.iv_child_delete.setVisibility(View.GONE);
            if (list_child_comment.get(position).getFromUser()!=null&&
                    !TextUtils.isEmpty(list_child_comment.get(position).getFromUser().getId())&&
                    list_child_comment.get(position).getFromUser().getId().equals(IMLoginManager.instance().getLoginId()+"")) {
                viewHolder.iv_child_delete.setVisibility(View.VISIBLE);
            }
            viewHolder.iv_child_delete.setTag(position);
            viewHolder.iv_child_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    deletePosition = position;
                    showDeleteAlertDialog();
                }
            });

//            }
            return convertView;
        }
        class ViewHolder {
            TextView tv_child_content;
            ImageView iv_child_delete;
            View view_line_top;

            public ViewHolder(View view){
                tv_child_content = (TextView) view.findViewById(R.id.tv_child_item_content);
                iv_child_delete = (ImageView) view.findViewById(R.id.iv_delete_square_item_comment);
                view_line_top = view.findViewById(R.id.view_hean_nouse_square_item_comment);
            }

        }
        }

    /**
     * 获取评论列表
     * @param response
     */
    private void onReceiveData(String response){
        if(isFirstLoad){
            isFirstLoad = false;
            try{
                topicCommentDetailJsonBean =new Gson().fromJson(response,TopicCommentDetailJsonBean.class);
            }catch (Exception e){
                isFirstLoad = true;
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_topic_not_exists),Toast.LENGTH_LONG).show();
                footView.showLoadFail();
                return;
            }
            if(topicCommentDetailJsonBean!=null){
                firstLoadData();
            }else{
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_loading_fail),Toast.LENGTH_LONG).show();
            }
        }else{
            TopicCommentDetailJsonBean topicCommentDetailJsonBean = null;
            try {
                topicCommentDetailJsonBean = new Gson().fromJson(response,TopicCommentDetailJsonBean.class);
            }catch (Exception e){
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_parse_error),Toast.LENGTH_LONG).show();
                return;
            }

            if(topicCommentDetailJsonBean!=null&&topicCommentDetailJsonBean.getChildComment()!=null){
                if(topicCommentDetailJsonBean.getChildComment().size()==0){
                    footView.showNoMoreData();
                    canLoad =false;
                }else if(topicCommentDetailJsonBean.getChildComment().size()<count){
                    addChildList(topicCommentDetailJsonBean.getChildComment());

                    footView.showNoMoreData();
                    commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.VISIBLE);
                    commentMainItem.notifyData();
                    adapter.notifyDataSetChanged();
                    canLoad = false;
                }else{
                    addChildList(topicCommentDetailJsonBean.getChildComment());
                    footView.showLoadMore();
                    commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.VISIBLE);
                    commentMainItem.notifyData();
                    adapter.notifyDataSetChanged();
                    canLoad = true;
                }

            }

        }
    }

    private void  onReceiveDeleteChildFloorData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("100")){
                commentEntity.setTotalChildCommentCount((Integer.valueOf(commentEntity.getTotalChildCommentCount())-1)+"");
                list_child_comment.remove(list_child_comment.get(deletePosition));
                commentEntity.setChildComment(list_child_comment);
                if(canLoad){
                    commentEntity.setTotalChildCommentCount(Integer.valueOf(commentEntity.getTotalChildCommentCount())-1+"");
                }else{
                    commentEntity.setTotalChildCommentCount(list_child_comment.size()+"");
                }
                setListLineVisibility();

                commentMainItem.notifyData();
                adapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(ResponseFloorActivity.this, getResources().getString(R.string.square_delete_failed), Toast.LENGTH_LONG).show();
        }

    }

    private void onReceiveResponseFloorData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.optString("result","").equals("100")){
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_comment_success),Toast.LENGTH_LONG).show();
                if(!canLoad) {     //如果已经没有更多 则去刷新界面 反之不刷新 只提示 评论成功
                    if(list_child_comment.size()==0){
                        index = 0;
                    }else{
                        index =1;
                        count = list_child_comment.size();
                    }
                    responseFloorManager.RequestCommentList(type,topicId,storyId,commentId,index,count,requestUrl);
                }
            }else{
                Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_comment_failed),Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.square_comment_failed),Toast.LENGTH_LONG).show();
        }
    }


    public SpannableStringBuilder strToSpann(final int position) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        String regex = SquareCommentManager.getInstance().getRegex() ;
        String text = "";
        int start;
        if(SquareCommentManager.getInstance().isLz(list_child_comment.get(position).getFromUser().getId())){
            text = SquareCommentManager.getInstance().constructFloorName(list_child_comment.get(position).getFromUser().getNickName(),true);

        }else{
            text = SquareCommentManager.getInstance().constructFloorName(list_child_comment.get(position).getFromUser().getNickName(), false);

        }
        spannableStringBuilder.append(text);
        if(text.contains(regex)){
            start = text.indexOf(regex);
            Drawable drawable = this.getResources().getDrawable(R.drawable.bg_louzhu);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());//这里设置图片的大小
            ImageSpan imageSpan = new ImageSpan(drawable, ImageSpan.ALIGN_BASELINE);
            spannableStringBuilder.setSpan(imageSpan,start,
                    start+regex.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }

        String content;
        if(list_child_comment.get(position).getToUser()!=null){
            if(list_child_comment.get(position).getLevel().equals("1")){
                content = SquareCommentManager.getInstance().constructCommentContent(null,list_child_comment.get(position).getCommentContent(),list_child_comment.get(position).getCommentTime());
            }else{
                content = SquareCommentManager.getInstance().constructCommentContent(list_child_comment.get(position).getToUser().getNickName(),list_child_comment.get(position).getCommentContent(),list_child_comment.get(position).getCommentTime());
            }
        }else{
            content = SquareCommentManager.getInstance().constructCommentContent(null,list_child_comment.get(position).getCommentContent(),list_child_comment.get(position).getCommentTime());
        }
        text+=content;
        spannableStringBuilder.append(content);

        //设置姓名颜色
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#0da6ce")), 0, text.indexOf("：") ,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        //设置日期颜色
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#949494")), text.lastIndexOf(" "),spannableStringBuilder.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(9, true),text.lastIndexOf(" "),spannableStringBuilder.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);    //去除超链接的下划线
            }

            @Override
            public void onClick(View widget) {
                if(list_child_comment.get(position).getFromUser().getId()!=null)
                    IMUIHelper.openUserHomePageActivity(ResponseFloorActivity.this,list_child_comment.get(position).getFromUser().getId());
            }
        }, 0, text.indexOf("：") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //回复主题内容分的点击  可用以跳转界面
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                responseFloor =false;
                /**
                 *     回复某子层 默认格式为" 回复"+"名字"+" :"开头 这里将输入框 组装成这个形式
                 */
                toUserId = list_child_comment.get(position).getFromUser().getId();
                toUserName = list_child_comment.get(position).getFromUser().getNickName();
                childTopicId = list_child_comment.get(position).getCommentID();
                edit_input.setText(SquareCommentManager.getInstance().constructCommentContent(toUserName));
                edit_input.setSelection(edit_input.getText().toString().indexOf("：")+1);
                commentInputView.setVisibility(View.VISIBLE);
                edit_input.requestFocus();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, text.indexOf("：") + 1, spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableStringBuilder;
    }

    //设置list头布局的底线的显示状态
    private void setListLineVisibility(){
        if(list_child_comment==null||list_child_comment.size()==0){
            commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.GONE);
        }else{
            commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.VIEW_LINE).setVisibility(View.VISIBLE);
        }
    }


    private void showDeleteAlertDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(ResponseFloorActivity.this);
        builder.setTitle(getResources().getString(R.string.square_prompt));
        builder.setMessage(getResources().getString(R.string.square_confim_delete_response));
        builder.setPositiveButton(getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                deleteChildComment();
                responseFloorManager.deleteChildComment(list_child_comment.get(deletePosition).getCommentID(),list_child_comment.get(deletePosition).getLevel(),deleteUrl);
            }
        });
        builder.setNegativeButton(getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

}
