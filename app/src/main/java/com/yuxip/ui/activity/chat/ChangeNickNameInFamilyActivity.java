package com.yuxip.ui.activity.chat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;

import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2015/6/1.
 * description:修改成员在家族的昵称
 */
public class ChangeNickNameInFamilyActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(ChangeNickNameInFamilyActivity.class);

    @InjectView(R.id.setNikename)
    EditText setNikename;
    private String nickname;
    private String groupid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View.inflate(ChangeNickNameInFamilyActivity.this, R.layout.activity_change_nikename, topContentView);
        ButterKnife.inject(this, topContentView);
        groupid = getIntent().getStringExtra("groupid");
        initRes();
        setLeftButton(R.drawable.back_default_btn);
        setTitle("设置昵称");
        setRighTitleText("完成");
    }

    private void initRes() {
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nickname = setNikename.getText().toString().trim();
                if (nickname.isEmpty()) {
                    showToast("输入不能为空");
                } else {
                    ModifyFamilyPersonInfo(nickname, groupid);
                }
            }
        });
    }

    /**
     * 更新家族信息
     */
    private void ModifyFamilyPersonInfo(String nikename, String groupid) {

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("nickname", nikename);
        params.addParams("groupid", groupid);
        OkHttpClientManager.postAsy(ConstantValues.ModifyFamilyPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        showToast("修改成功");
                        Intent intent = new Intent();
                        intent.putExtra(IntentConstant.FAMILY_CHANGE_RESULT,nickname);
                        setResult(RESULT_OK,intent);
                        ChangeNickNameInFamilyActivity.this.finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
