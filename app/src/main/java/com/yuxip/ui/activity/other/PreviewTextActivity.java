
package com.yuxip.ui.activity.other;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.base.TTBaseNewActivity;

public class PreviewTextActivity extends TTBaseNewActivity {
    TextView txtContent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.tt_activity_preview_text);

        txtContent = (TextView) findViewById(R.id.content);

        String displayText = getIntent().getStringExtra(IntentConstant.PREVIEW_TEXT_CONTENT);
        txtContent.setText(displayText);

        ((View) txtContent.getParent()).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreviewTextActivity.this.finish();
            }
        });
    }

}
