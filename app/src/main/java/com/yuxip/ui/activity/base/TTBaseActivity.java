package com.yuxip.ui.activity.base;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.yuxip.R;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;

/**
 * 相关Activity的超类
 */
public abstract class TTBaseActivity extends Activity {
    protected ImageView topLeftBtn;
    protected ImageView topRightBtn;
    protected TextView topTitleTxt;
    protected TextView letTitleTxt;
    protected TextView righTitleTxt;
    protected ViewGroup topBar;
    protected ViewGroup topContentView;
    protected LinearLayout baseRoot;
    public Logger logger;

    @Override
    protected void onCreate(Bundle   savedInstanceState) {
        super.onCreate(savedInstanceState);
        boolean result;
        result = SharedPreferenceUtils.getBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
        if(result) {
            IMUIHelper.openSplashActivity(this);
            this.finish();
            return;
        }

        logger = Logger.getLogger(getClass());
        topContentView = (ViewGroup) LayoutInflater.from(this).inflate(R.layout.tt_activity_base, null);
        topBar = (ViewGroup) topContentView.findViewById(R.id.topbar);
        topTitleTxt = (TextView) topContentView.findViewById(R.id.base_activity_title);
        topLeftBtn = (ImageView) topContentView.findViewById(R.id.left_btn);
        topRightBtn = (ImageView) topContentView.findViewById(R.id.right_btn);
        letTitleTxt = (TextView) topContentView.findViewById(R.id.left_txt);
        righTitleTxt = (TextView) topContentView.findViewById(R.id.right_txt);
        baseRoot = (LinearLayout)topContentView.findViewById(R.id.act_base_root);

        topTitleTxt.setVisibility(View.GONE);
        topRightBtn.setVisibility(View.GONE);
        letTitleTxt.setVisibility(View.GONE);
        topLeftBtn.setVisibility(View.GONE);
        //左侧返回按钮
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });
        setContentView(topContentView);
    }


    protected boolean isNeedRestart() {
        boolean result;
        result = SharedPreferenceUtils.getBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
        if (result) {
            SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
            IMUIHelper.openSplashActivity(this);
            finish();
        }
        return result;
    }

    protected void setLeftText(String text) {
        if (null == text) {
            return;
        }
        letTitleTxt.setText(text);
        letTitleTxt.setVisibility(View.VISIBLE);
    }

    protected void setRighTitleText(String text) {
        if (null == text) {
            return;
        }
        righTitleTxt.setText(text);
        righTitleTxt.setVisibility(View.VISIBLE);
    }
    protected void setRighTitleTextColor(int resId) {
        righTitleTxt.setTextColor(resId);
    }

    protected void setRightTitleTextBackgroundResource(int resId) {
        righTitleTxt.setBackgroundResource(resId);
    }

    protected void setRighTitleTextClick(View.OnClickListener onClickListener) {
        righTitleTxt.setOnClickListener(onClickListener);
    }

    protected void setRighTitleTextClickable(boolean clickable) {
        righTitleTxt.setClickable(clickable);
    }

    protected void setTitle(String title) {
        if (title == null) {
            return;
        }
        if (title.length() > 12) {
            title = title.substring(0, 11) + "...";
        }
        topTitleTxt.setText(title);
        topTitleTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTitle(int id) {
        String strTitle = getResources().getString(id);
        setTitle(strTitle);
    }


    protected void setLeftButton(int resID) {
        if (resID <= 0 || topLeftBtn == null) {
            return;
        }

        topLeftBtn.setVisibility(View.VISIBLE);
        topLeftBtn.setImageResource(resID);
    }

    protected void setRightButton(int resID) {
        if (resID <= 0) {
            return;
        }

        topRightBtn.setImageResource(resID);
        topRightBtn.setVisibility(View.VISIBLE);
    }

    protected void setTopBar(int resID) {
        if (resID <= 0) {
            return;
        }
        topBar.setBackgroundResource(resID);
    }


    /**
     * 短时间显示Toast
     *
     * @param message
     */
    protected  void showToast(String message){
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public ImageView getTopRightBtn() {
        return topRightBtn;
    }

    public static void pushInActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.slide_in_right, 0);
    }

    public static void popOutActivity(Activity activity) {
        activity.overridePendingTransition(0, R.anim.slide_out_right);
    }

    public static void zoomInActivity(Activity activity) {
        activity.overridePendingTransition(R.anim.zoom_in, 0);
    }

    public static void zoomOutActivity(Activity activity) {
        activity.overridePendingTransition(0, R.anim.zoom_out);
    }

    @Override
    protected void onResume() {
        super.onResume();
        String name  = getClass().getSimpleName();
        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart(name);
        SharedPreferenceUtils.saveBooleanDate(getApplicationContext(), SharedPreferenceValues.HOME_KEY, false);
    }

    @Override
    protected void onPause() {
        super.onPause();
        /** 获取子类名 */
        String name  = getClass().getSimpleName();
        MobclickAgent.onPageEnd(name);
        MobclickAgent.onPause(this);
    }
}
