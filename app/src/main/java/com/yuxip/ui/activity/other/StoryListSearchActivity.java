package com.yuxip.ui.activity.other;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseNewCompatActivity;
import com.yuxip.ui.adapter.StoryListAdapter;
import com.yuxip.ui.customview.MyRecyclerView;
import com.yuxip.ui.customview.ProgressView;
import com.yuxip.utils.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zl on 2015/7/7.
 */
public class StoryListSearchActivity extends TTBaseNewCompatActivity implements View.OnClickListener {



    public enum Type {
        SEARCH, //搜索
        PLAY_STORY, //已开戏
        ZI_XI //自戏剧
    }

    private Logger logger = Logger.getLogger(StoryListSearchActivity.class);

    //    @InjectView(R.id.left_btn)
//    ImageView leftBtn;
    @InjectView(R.id.tv_search_note)
    TextView tv_note;
    @InjectView(R.id.recycler_view)
    MyRecyclerView recyclerView;
    //    @InjectView(R.id.tv_name)
//    TextView tvName;
    @InjectView(R.id.edit_search)
    EditText editSearch;
    @InjectView(R.id.iv_back)
    ImageView ivBack;
    @InjectView(R.id.tv_search)
    TextView tvSearch;
//    @InjectView(R.id.exit)
//    ImageView imgClear;
    private List<HotBean.StorysEntity> hotBeans = new ArrayList<>();
    private ProgressView emptyView;
    private StoryListAdapter adapter;

    private static Type type = Type.SEARCH;

    private String loginId;
    private String category; //类别id搜索
    private String cond; //关键字搜索
    private String name; //分类name

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.activity_story_list);
        ButterKnife.inject(this);

        initView();
        initData();
    }

    private void initData() {
        loginId = IMLoginManager.instance().getLoginId() + "";
        recyclerView.setAdapter(adapter = new StoryListAdapter(this, hotBeans));
        adapter.setCustomLoadMoreView(null);
        ivBack.setOnClickListener(this);
        tvSearch.setOnClickListener(this);
//        refreshData();
    }

    private void initView() {
        initSearchView();
        initRecyclerView();
    }

    private void initSearchView() {
        editSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_SEARCH) {
                    cond = editSearch.getText().toString();
                    if (!TextUtils.isEmpty(cond)) {
                        tv_note.setVisibility(View.GONE);
                        recyclerView.setRefreshing(true);
                        refreshData();

                    } else {
                        Toast.makeText(getApplicationContext(), "搜索关键字不能为空", Toast.LENGTH_LONG).show();
                    }
                }
                return false;
            }
        });
//        imgClear.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                editSearch.setText("");
////                cond="";
//            }
//        });
    }


    private void initRecyclerView() {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);


        recyclerView.setDefaultOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                    }
                });
    }

    private void refreshData() {
        switch (type) {
            case SEARCH:
                searchStory();
                break;
            case PLAY_STORY:
                requestPlayStory();
                break;
            case ZI_XI:
                requestZixi();
                break;
        }
    }

    //自戏   数据获取
    private void requestZixi() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);

        OkHttpClientManager.postAsy(ConstantValues.GetSelfStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
//                        recyclerView.setRefreshing(false);
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    String result = jsonObject.getString("result");
                    if ("1".equals(result)) {

                        JSONArray array = jsonObject.getJSONArray("storys");
                        Gson gson = new Gson();
                        for (int i = 0; i < array.length(); i++) {
                            HotBean.StorysEntity s = gson
                                    .fromJson(array.getJSONObject(i).toString(),
                                            HotBean.StorysEntity.class);
                            hotBeans.add(s);
                        }

                        if (hotBeans.size() != 0) {
                            adapter.notifyDataSetChanged();
                        } else {
                            tv_note.setVisibility(View.VISIBLE);
                            tv_note.setText("没有相关的内容(* ﾟДﾟ)");
                        }

                    } else {
                        tv_note.setVisibility(View.VISIBLE);
                        tv_note.setText("未能获得数据");
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                    tv_note.setVisibility(View.VISIBLE);
                    tv_note.setText("未能获得数据");
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                recyclerView.setRefreshing(false);
                tv_note.setVisibility(View.VISIBLE);
                tv_note.setText(getString(R.string.network_err));
            }
        });
    }

    private int indexPlay = 0;

    //已开戏   数据获取
    private void requestPlayStory() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("index", indexPlay + "");
        params.addParams("count", "10");
        OkHttpClientManager.postAsy(ConstantValues.GetPlayingStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
//                        recyclerView.setRefreshing(false);
                try {
                    JSONObject jsonObject = new JSONObject(content);
                    String result = jsonObject.getString("result");
                    if ("1".equals(result)) {

                        JSONArray array = jsonObject.getJSONArray("storys");
                        Gson gson = new Gson();
                        for (int i = 0; i < array.length(); i++) {
                            HotBean.StorysEntity s = gson
                                    .fromJson(array.getJSONObject(i).toString(),
                                            HotBean.StorysEntity.class);
                            hotBeans.add(s);
                        }
                        adapter.notifyDataSetChanged();
                        recyclerView.hideEmptyView();
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                    emptyView.showFailureText("未能得到数据，请点击重试", true);
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                recyclerView.setRefreshing(false);
                emptyView.showFailureText(getString(R.string.network_err), true);
            }
        });
    }


    private void searchStory() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        //类别用数字代替
        if (!TextUtils.isEmpty(category)) {
            params.addParams("category", category);

        }

        if (!TextUtils.isEmpty(cond)) {
            params.addParams("cond", cond);
        }

        OkHttpClientManager.postAsy(ConstantValues.SearchStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                recyclerView.setRefreshing(false);
                HotBean bean = null;
                try {
                    bean = new Gson().fromJson(content, HotBean.class);
                    if (bean.getResult().equals("1")) {
                        List<HotBean.StorysEntity> h = bean.getStorys();
                        hotBeans.clear();
                        hotBeans.addAll(h);
                        if (hotBeans != null && hotBeans.size() != 0) {
                            adapter.notifyDataSetChanged();
                        } else {
                            tv_note.setVisibility(View.VISIBLE);
                            tv_note.setText("没有相关的内容(* ﾟДﾟ)");
                        }
                    } else {
                        tv_note.setVisibility(View.VISIBLE);
                        tv_note.setText("未能获得数据");
                    }
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                    tv_note.setVisibility(View.VISIBLE);
                    tv_note.setText("未能获得数据");
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                recyclerView.setRefreshing(false);
                tv_note.setVisibility(View.VISIBLE);
                tv_note.setText(getString(R.string.network_err));
            }
        });
    }


    /**
     * @param context
     * @param categoryId   类别id  用于搜索
     * @param cond         关键字  用于搜索
     * @param categoryName
     * @param t
     */
    public static void startActivity(Context context, String categoryId, String cond, String categoryName, Type t) {
        type = t;
        Intent intent = new Intent(context, StoryListSearchActivity.class);
        intent.putExtra("category", categoryId);
        intent.putExtra("cond", cond);
        intent.putExtra("name", categoryName);
        context.startActivity(intent);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_search:
                cond = editSearch.getText().toString();
                if (!TextUtils.isEmpty(cond)) {
                    tv_note.setVisibility(View.GONE);
                    recyclerView.setRefreshing(true);
                    refreshData();

                } else {
                    Toast.makeText(getApplicationContext(), "搜索关键字不能为空", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
}
