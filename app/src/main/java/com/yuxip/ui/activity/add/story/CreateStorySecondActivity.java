package com.yuxip.ui.activity.add.story;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.customview.CustomDividerProgressbar;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

/**
 * Created by Ly on 2015/10/30.
 * 剧简介
 */
public class CreateStorySecondActivity extends Activity implements TextWatcher, View.OnClickListener {
    private EditText etAbstract;
    private TextView tvNextStep;
    private boolean isAbstractReady = false;
    private CustomDividerProgressbar progressbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_story_second);
        initRes();
        checkManagerData();
    }

    private void initRes() {
        setTitle(getResources().getString(R.string.story_abstract));
        ImageView ivBack = (ImageView) findViewById(R.id.iv_back_create_story_sec);
        etAbstract = (EditText) findViewById(R.id.et_create_stroy_sec);
        tvNextStep = (TextView) findViewById(R.id.tv_next_step_create_story_sec);
        progressbar = (CustomDividerProgressbar) findViewById(R.id.pb_create_story_sec);
        tvNextStep.setSelected(false);
        etAbstract.addTextChangedListener(this);
        tvNextStep.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        progressbar.setCurProgress((int) Math.ceil(100 * 2 / 6));
        progressbar.setDividerCount(6);
        progressbar.reDrawDivider();
    }

    private void checkManagerData() {
        String content = CreateStoryManager.getInstance().getAbstractContent();
        if (!TextUtils.isEmpty(content) && CreateStoryManager.getInstance().getCharacterCount(content) > 0) {
            etAbstract.setText(content);
            tvNextStep.setSelected(true);
        }
    }

    private void checkStatus() {
        if (isAbstractReady) {
            tvNextStep.setSelected(true);
        } else {
            tvNextStep.setSelected(false);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        if (TextUtils.isEmpty(s) || TextUtils.isEmpty(s.toString())) {
            isAbstractReady = false;
            checkStatus();
        } else {
            if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) == 0) {
                isAbstractReady = false;
                checkStatus();
                return;
            } else if (CreateStoryManager.getInstance().getCharacterCount(s.toString()) > 500) {
                etAbstract.setTextColor(getResources().getColor(R.color.red_text));
            } else {
                etAbstract.setTextColor(getResources().getColor(R.color.black));
            }
            isAbstractReady = true;
            checkStatus();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_next_step_create_story_sec:
                if (!tvNextStep.isSelected()) {
                    T.show(getApplicationContext(), getResources().getString(R.string.story_has_data_not_inflate), 0);
                    return;
                }
                int abstractWordLimit = 500;
                if (CreateStoryManager.getInstance().getCharacterCount(etAbstract.getText().toString().trim()) > abstractWordLimit) {
                    T.show(getApplicationContext(), getResources().getString(R.string.story_abstract_limit_500), 0);
                    return;
                }

                saveManageData();
                IMUIHelper.openCreateStoryOrderActivity(this, 3, null);
                finish();
                break;
            case R.id.iv_back_create_story_sec:
                openPreAcitivty();
                finish();
                break;
        }
    }

    private void saveManageData() {
        CreateStoryManager.getInstance().setAbstractContent(etAbstract.getText().toString().trim());
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            openPreAcitivty();
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void openPreAcitivty() {
        saveManageData();
        IMUIHelper.openCreateStoryOrderActivity(this, 1, IntentConstant.ACTIVITY_OPEN_TYPE_LEFT);
    }

}
