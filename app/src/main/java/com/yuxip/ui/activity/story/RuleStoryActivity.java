package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

/**
 * changed by SummerRC on 2015/5/25.
 */
public class RuleStoryActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(RuleStoryActivity.class);

    private EditText et_content;
    private String storyid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_story_introk, topContentView);
        et_content = (EditText) findViewById(R.id.et_rule);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("规则");
        et_content.setHint("选填.立个群规也是很重要哒.例如戏群严禁灌水,剧情讨论移驾到水聊群:禁 水 禁短字句,起码30字起");


        boolean IS_ADMIN = getIntent().getBooleanExtra("IS_ADMIN", false);
        storyid = getIntent().getStringExtra("storyid");
        String rule = getIntent().getStringExtra("rule");
        et_content.setText(rule);
        if(IS_ADMIN) {
            et_content.setFocusable(true);
            et_content.requestFocus();
            setRighTitleText("完成");
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String  storyIntroContent  = et_content.getText().toString().trim();
                    if (TextUtils.isEmpty(storyIntroContent)){
                        Toast.makeText(getApplicationContext(), "请完善信息", Toast.LENGTH_SHORT).show();
                    }else{
                        /** 修改规则 */
                        changeRule();
                    }
                }
            });
        } else {
            et_content.setFocusable(false);
            et_content.setClickable(false);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    /***
     * 修改规则
     */
    private void changeRule() {
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("content", et_content.getText().toString().trim());
        params.addParams("storyid", storyid);
        params.addParams("type", "2");           //规则
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ModifyStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject obj = new JSONObject(response);
                    if (obj.getString("result").equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        setResult(RESULT_OK);
                        RuleStoryActivity.this.finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "修改失败", Toast.LENGTH_SHORT).show();
                        RuleStoryActivity.this.finish();
                        finish();
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

}
