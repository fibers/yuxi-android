package com.yuxip.ui.activity.add.story;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.yuxip.JsonBean.RoleTypeSettingEntity;
import com.yuxip.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/11/2.
 * description:创建角色时的自定义
 */
public class AddRoleView extends RelativeLayout implements AddRoleItemView.AddRoleCallBack {
    private Context context;
    private LinearLayout llContainer;
    private AddRoleViewCallBack addRoleViewCallBack;

    public AddRoleView(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public AddRoleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();
    }

    public AddRoleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }

    private void initRes() {
        LayoutInflater.from(context).inflate(R.layout.customview_add_role, this, true);
        llContainer = (LinearLayout) findViewById(R.id.ll_custom_add_role);
    }


    public void setAddRoleViewCallBack(AddRoleViewCallBack addRoleViewCallBack) {
        if (addRoleViewCallBack != null) {
            this.addRoleViewCallBack = addRoleViewCallBack;
        }
    }

    public void addNewHintRole(){
        AddRoleItemView addRoleItemView = new AddRoleItemView(context);
        addRoleItemView.setAddRoleListener(this);
        addRoleItemView.setRoleHint(context.getResources().getString(R.string.role_pibiao));
        if (llContainer.getChildCount() == 0)
            addRoleItemView.setTitleVisable();
        llContainer.addView(addRoleItemView);
        refreshChildPosition();
    }


    public void addNewRole(String roleName) {
        AddRoleItemView addRoleItemView = new AddRoleItemView(context);
        addRoleItemView.setAddRoleListener(this);
        addRoleItemView.setRoleName(roleName);
        if (llContainer.getChildCount() == 0)
            addRoleItemView.setTitleVisable();
        llContainer.addView(addRoleItemView);
        refreshChildPosition();
    }

    public void addNewRole(RoleTypeSettingEntity role) {
        if (role != null) {
            AddRoleItemView addRoleItemView = new AddRoleItemView(context);
            addRoleItemView.setAddRoleListener(this);
            addRoleItemView.setRoleName(role.getType());
            addRoleItemView.setRolenum(role.getCount());
            addRoleItemView.setRoleBackground(role.getBackground());
            if (llContainer.getChildCount() == 0)
                addRoleItemView.setTitleVisable();
            llContainer.addView(addRoleItemView);
            refreshChildPosition();
        }
    }

    public void removeRole(int position) {
        if (llContainer.getChildAt(position) != null) {
            llContainer.removeViewAt(position);
            refreshChildPosition();
                if(addRoleViewCallBack!=null){
                    if(isAllItemInflate()) {
                        addRoleViewCallBack.isDataInflate(true);
                    }else{
                        addRoleViewCallBack.isDataInflate(false);
                    }
                }
        }
        if (llContainer.getChildCount() == 0) {
            if (addRoleViewCallBack != null)
                addRoleViewCallBack.noDataExists();
        }
    }

    /**
     * 获取所有的子项的内容
     */
    public List<RoleTypeSettingEntity> getAllChildData() {
        List<RoleTypeSettingEntity> list = new ArrayList<>();
        for (int i = 0; i < llContainer.getChildCount(); i++) {
            if (llContainer.getChildAt(i) instanceof AddRoleItemView) {
                AddRoleItemView addRoleItemView = (AddRoleItemView) llContainer.getChildAt(i);
                list.add(addRoleItemView.getRoleEntity());
            }
        }
        return list;
    }


    private void refreshChildPosition() {
        for (int i = 0; i < llContainer.getChildCount(); i++) {
            if (llContainer.getChildAt(i) instanceof AddRoleItemView) {
                ((AddRoleItemView) llContainer.getChildAt(i)).setPositionInParent(i);
            }
        }
    }

    public boolean isAllItemInflate() {
        for (int i = 0; i < llContainer.getChildCount(); i++) {
            if (llContainer.getChildAt(i) instanceof AddRoleItemView) {
                AddRoleItemView addRoleItemView = (AddRoleItemView) llContainer.getChildAt(i);
                if (!addRoleItemView.isItemOk()) {
                    return false;
                }
            }
        }
//        Toast.makeText(context, "数据填充完毕", Toast.LENGTH_LONG).show();
        return true;
    }


    private void showDeleteRoleDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.square_prompt)).setMessage(context.getResources().getString(R.string.story_delete_role))
                .setNegativeButton(context.getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setPositiveButton(context.getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeRole(position);
            }
        }).show();
    }


    /**
     * 外部回调 通知已经没有内容 无法进行下一步
     */
    public interface AddRoleViewCallBack {
        void noDataExists();
        void isDataInflate(boolean isInflate);
    }


    /**
     * 每个子项的回调 删除某项
     * 检测所有内容
     *
     * @param position      position
     */
    @Override
    public void deleteItem(int position) {
        showDeleteRoleDialog(position);
    }

    @Override
    public void checkAllData() {
        if(isAllItemInflate()){
            if(addRoleViewCallBack!=null)
                addRoleViewCallBack.isDataInflate(true);
        }else{
            if(addRoleViewCallBack!=null)
                addRoleViewCallBack.isDataInflate(false);
        }
    }
}
