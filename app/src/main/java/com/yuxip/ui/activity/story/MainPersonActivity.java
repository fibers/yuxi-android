package com.yuxip.ui.activity.story;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.MainPersonEntity;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 添加主线人物
 * add by SummerRC on 2015/5/25.
 */
public class MainPersonActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(MainPersonActivity.class);

    private EditText introContent;
    private ArrayList<MainPersonEntity> sceneslist  = new ArrayList<>();
    private ListView mainPersionList;
    private String storyid;
    private String uid;
    private MianPersonAdapter adapter;
    private boolean IS_ADMIN = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        LayoutInflater.from(this).inflate(R.layout.activity_persion_main, topContentView);
        mainPersionList=(ListView)findViewById(R.id.mainPersionList);
        /** 顶部导航栏 */
        setLeftButton(R.drawable.back_default_btn);
        setTitle("主线角色");


        introContent = (EditText) findViewById(R.id.introContent);
        storyid = getIntent().getStringExtra("storyid");
        uid = getIntent().getStringExtra("creatorid");

        IS_ADMIN = getIntent().getBooleanExtra("IS_ADMIN", false);
        if(IS_ADMIN) {
            setRighTitleText("添加");
            righTitleTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainPersonActivity. this,MainPersonAddActivity.class);
                    intent.putExtra(IntentConstant.STORY_ID, storyid);
                    intent.putExtra("IS_ADMIN", IS_ADMIN);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        initDate();

    }

    /**
     * 初始化数据
     */
    private void initDate() {
        if(TextUtils.isEmpty(uid)||TextUtils.isEmpty(storyid)){
            return;
        }
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("storyid", storyid);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryRoles, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                sceneslist.clear();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONArray roles=  object.getJSONArray("roles");
                        for (int i = 0; i < roles.length(); i++){
                            JSONObject obj = roles.getJSONObject(i);
                            MainPersonEntity personEntity = new MainPersonEntity();
                            //TODO  需要返回主线人物数量
                            personEntity.setId(obj.get("id").toString());
                            personEntity.setTitle(obj.get("title").toString());
                            personEntity.setIntro(obj.get("intro").toString());
                            personEntity.setNum(obj.get("num").toString());
                            sceneslist.add(personEntity);
                        }
                    }
                    adapter = new MianPersonAdapter(sceneslist, MainPersonActivity.this);
                    mainPersionList.setAdapter(adapter);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }




    private class MianPersonAdapter extends BaseAdapter {
        private ArrayList<MainPersonEntity> sceneslist;
        private Context context;
        private boolean IS_NULL = false;

        public MianPersonAdapter(ArrayList<MainPersonEntity> sceneslist, Context context) {
            this.sceneslist = sceneslist;
            this.context = context;
        }

        @Override
        public int getCount() {
            if (sceneslist == null || sceneslist.size() == 0) {
                IS_NULL = true;
                return 1;
            } else {
                return sceneslist.size();
            }
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            convertView =  View.inflate(context,R.layout.mian_story_item, null);
            TextView tv_main_story_name = (TextView) convertView.findViewById(R.id.tv_main_story_name);
            if(IS_NULL) {
                tv_main_story_name.setText("暂无内容");
                convertView.findViewById(R.id.iv_main_story_name_btm).setVisibility(View.GONE);
            } else {
                final String title = sceneslist.get(position).getTitle();
                final String intro =sceneslist.get(position).getIntro();
                final String num = sceneslist.get(position).getNum();
                if (TextUtils.isEmpty(title)){
                    tv_main_story_name.setText("没有内容");
                }
                tv_main_story_name.setText(sceneslist.get(position).getTitle());
                convertView.findViewById(R.id.rl_btm).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainPersonActivity.this, MainPersonAddActivity.class);
                        intent.putExtra("roleid",sceneslist.get(position).getId());
                        intent.putExtra("num", num);
                        intent.putExtra("IS_ADMIN", IS_ADMIN);
                        intent.putExtra("title", title);
                        intent.putExtra("content", intro);
                        intent.putExtra("modify",true);
                        intent.putExtra(IntentConstant.STORY_ID, storyid);
                        startActivity(intent);
                    }
                });
            }
            return convertView;
        }
    }
}
