package com.yuxip.ui.activity.home;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseActivity;

/**
 * Created by Administrator on 2015/5/4.
 * description:关于我们
 */
public class AboutMeActivity extends TTBaseActivity {
    private TextView tv_about;
    private String versionCode = "1.0.2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View.inflate(AboutMeActivity.this, R.layout.activity_aboutme, topContentView);
        tv_about = (TextView) findViewById(R.id.tv_about);
        setTitle("关于我们");
        setLeftButton(R.drawable.back_default_btn);
        try {
            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        tv_about.setText("语戏" + versionCode + "版本");
    }
}
