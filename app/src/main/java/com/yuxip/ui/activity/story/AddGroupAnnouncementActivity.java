package com.yuxip.ui.activity.story;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.GroupAnnouncementEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;


/**
 * Created by SummerRC on 2015/9/4.
 * description：管理员添加修改群公告以及群成员查看群公告
 */
public class AddGroupAnnouncementActivity extends TTBaseActivity {
    private Logger logger = Logger.getLogger(AddGroupAnnouncementActivity.class);

    @InjectView(R.id.et_title)
    TextView et_title;

    @InjectView(R.id.et_content)
    TextView et_content;

    private String groupId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View.inflate(this, R.layout.activity_add_group_announcement, topContentView);
        ButterKnife.inject(this, topContentView);
        setLeftButton(R.drawable.back_default_btn);
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        initView();
    }

    private void initView() {
        setTitle("群公告");
        setRighTitleText("发布");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(et_title.getText()) || TextUtils.isEmpty(et_content.getText())) {
                    T.show(getApplicationContext(), "请输入内容", 0);
                    return;
                }
                updateStoryGroupBoard();
            }
        });
        getStoryGroupBoard();
    }

    /**
     * 更新群公告
     */
    private void updateStoryGroupBoard() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("groupid", groupId);
        params.addParams("title", et_title.getText().toString());
        params.addParams("content", et_content.getText().toString());
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.UpdateStoryGroupBoard, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        T.show(getApplicationContext(), object.getString("describe"), 0);
                        //saveAnnouncement(object.getString("datetime"));
                        finish();
                    } else {
                        T.show(getApplicationContext(), object.getString("describe"), 0);
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 网络获取群公告
     */
    private void getStoryGroupBoard() {
        if(TextUtils.isEmpty(groupId))
            return;
        String uid = IMLoginManager.instance().getLoginId() + "";
        //** 放在params里面传递 *//*
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("groupid", groupId);
        params.addParams("latesttime", "0");     //获取最新的公告
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupBoard, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        JSONObject contentObject = object.getJSONArray("boardinfo").getJSONObject(0);
                        if (TextUtils.isEmpty(contentObject.getString("title"))) {
                            T.show(getApplicationContext(), "暂无公告！", 0);
                        } else {
                            et_title.setText(contentObject.getString("title"));
                            et_content.setText(contentObject.getString("content"));
                            saveAnnouncement(object.getString("datetime"));
                        }
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 保存公共到本地数据库
     *
     * @param dateTime 公共的时间戳
     */
    public void saveAnnouncement(String dateTime) {
        DBInterface dbInterface = DBInterface.instance();
        assert dbInterface != null;
        GroupAnnouncementEntity announcementEntity = dbInterface.getGroupAnnouncementEntityByGroupId(groupId);
        if (announcementEntity == null) {
            announcementEntity = new GroupAnnouncementEntity();
        }
        announcementEntity.setGroupId(groupId);
        announcementEntity.setTitle(et_title.getText().toString());
        announcementEntity.setContent(et_content.getText().toString());
        announcementEntity.setDateTime(dateTime);
        dbInterface.insertOrUpdateGroupAnnouncementEntity(announcementEntity);
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }
}
