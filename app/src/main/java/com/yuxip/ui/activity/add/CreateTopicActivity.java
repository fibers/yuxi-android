package com.yuxip.ui.activity.add;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 创建话题
 * Created by HeTianpeng on 2015/07/13.
 */
public class CreateTopicActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(CreateTopicActivity.class);

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final int PHOTORESOULT = 3;    // 结果

    public static final String IMAGE_UNSPECIFIED = "image/*";

    @InjectView(R.id.textinput_topic_name)
    TextInputLayout textinputTopicName;
    @InjectView(R.id.textinput_introduce)
    TextInputLayout textinputIntroduce;
    @InjectView(R.id.iv_add_image)
    ImageView ivAddImage;

    private EditText editTitle;
    private EditText editConent;
    private String title;
    private String content;
    private Bitmap photo;
    private int gId; //群id

    private IMService imService;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }

        @Override
        public void onServiceDisconnected() {
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View view = LayoutInflater.from(this).inflate(R.layout.activity_topic, topContentView);
        ButterKnife.inject(this, view);
        EventBus.getDefault().register(this);
        imServiceConnector.connect(this);
        initView();
    }

    @Override
    protected void onDestroy() {
        imServiceConnector.disconnect(this);
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    private void initView() {
        textinputTopicName.setHint("发起话题(30字以内)");
        textinputIntroduce.setHint("话题内容");
        ivAddImage.setOnClickListener(this);
        editTitle = textinputTopicName.getEditText();
        editConent = textinputIntroduce.getEditText();


        setTitle("发起话题");
        setLeftButton(R.drawable.back_default_btn);
        setRighTitleText("发布");
        setRighTitleTextColor(getResources().getColor(R.color.pink));
        setRighTitleTextClick(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                title = editTitle.getText().toString().trim();
                content = editConent.getText().toString().trim();
                if (TextUtils.isEmpty(title) || TextUtils.isEmpty(content)) {
                    T.showShort(CreateTopicActivity.this, "请填写完整内容");
                    return;
                }
                //禁用点击事件，避免连续多次点击
                setRighTitleTextClickable(false);
                T.showShort(CreateTopicActivity.this, getString(R.string.please_wait));
                //创建群
                createGroup(title);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_add_image:
                showAddImgDialog();
                break;
        }
    }

    //弹出添加图片的dialog
    private void showAddImgDialog() {
        new GGDialog().showImgSelcetDialog(CreateTopicActivity.this, "发起话题", "添加图片",
                new GGDialog.OnDialogButtonClickedListenered() {
                    @Override
                    public void onConfirmClicked() {
                        try {
                            Intent intent = new Intent(Intent.ACTION_PICK, null);
                            intent.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, IMAGE_UNSPECIFIED);
                            startActivityForResult(intent, PHOTOZOOM);
                        } catch (ActivityNotFoundException e) {
                            T.show(getApplicationContext(), e.toString(), 0);
                            logger.e(e.toString());
                        }
                    }

                    @Override
                    public void onCancelClicked() {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent, PHOTOHRAPH);
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == NONE || data == null) {

            return;
        }
        ContentResolver resolver = getContentResolver();

        switch (requestCode) {
            case PHOTOHRAPH:  //拍照
            case PHOTOZOOM:   //选图片
                Uri uri = data.getData();
                if (uri == null) {
                    //use bundle to get data
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        photo = (Bitmap) bundle.get("data"); //get bitmap
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateTopicActivity.this);
                        ivAddImage.setImageBitmap(photo);

                    } else {
                        Toast.makeText(getApplicationContext(), "没有图片", Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    try {
                        //to do find the path of pic by uri
                        photo = MediaStore.Images.Media.getBitmap(resolver, uri);
                        photo = ImageUtil.getBigBitmapForDisplay(photo, CreateTopicActivity.this);

                        Log.d("bitmap option",
                                "bitmap width " + photo.getWidth() + " bitmap height : " + photo
                                        .getHeight());
                        ivAddImage.setImageBitmap(photo);
                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG)
                                .show();
                        e.printStackTrace();
                    }
                }
                break;

            case PHOTORESOULT: //暂时没用
                /*  从本地图片取数据
                FileInputStream fis = new FileInputStream(Environment.getExternalStorageDirectory() + "/temp.jpg");
                Bitmap bitmap = BitmapFactory.decodeStream(fis);
                addImage.setImageBitmap(bitmap);
                */
                Bundle extras = data.getExtras();
                if (extras != null) {
                    photo = extras.getParcelable("data");
                    ivAddImage.setImageBitmap(photo);
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private UpImgUtil upImgUtil;

    /**
     * 上传图片
     */
    private void createTopic() {
        if (photo != null) {
            /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
            Date date = new Date();
            /** 获取当前时间并且进一步转化为字符串 */
            SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
            String pic_path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";

            upImgUtil = new UpImgUtil();
            upImgUtil.setFilePath(pic_path, pic_path);
            upImgUtil.saveHeadImg(photo);
            upImgUtil.upLoadPicture(new HeadImgListener() {
                public void notifyImgUploadFinished(String url) {
                    createTopicReq(url);
                }
            });

        } else {
            createTopicReq("");
        }
    }

    private void createTopicReq(String imgUrl) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("title", title);
        params.addParams("imgurls", imgUrl);
        params.addParams("groupid", gId + "");
        params.addParams("content", content);
        OkHttpClientManager.postAsy(ConstantValues.PublishTopic, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                setRighTitleTextClickable(true);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String result = jsonObject.getString("result");
                    if (result.equals("1")) {
                        T.showShort(CreateTopicActivity.this, "发布成功");
                        IMApplication.IS_REFRESH = true;
                        Intent intent = new Intent();
                        intent.setAction(ConstantValues.BROADCAST_REFRESH_HOME);
                        sendBroadcast(intent);
                        finish();
                    } else {
                        T.showShort(CreateTopicActivity.this, "发布失败");
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                setRighTitleTextClickable(true);
                T.showShort(CreateTopicActivity.this, "发布失败");
            }
        });
    }

    /**
     * 创建群
     * @param groupName 群名
     */
    private void createGroup(String groupName) {
        Set<Integer> ListSet = new HashSet<>();
        IMGroupManager groupMgr = imService.getGroupManager();
        int loginId = imService.getLoginManager().getLoginId();
        ListSet.add(loginId);
        groupMgr.reqCreateTempGroup(groupName, ListSet);
    }

    /**
     *
     * @param event 创建群的事件
     */
    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case CREATE_GROUP_OK:
                gId = event.getGroupEntity().getPeerId();
                createTopic();
                break;
            case CREATE_GROUP_FAIL:
            case CREATE_GROUP_TIMEOUT:
                /** 屏蔽掉不在当前Activity执行建群操作通知结果的事件 */
                setRighTitleTextClickable(true);
                T.showShort(this, "发布失败");
                break;
        }
    }
}
