package com.yuxip.ui.activity.chat;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.ui.widget.MyListView;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * edited by SummerRC on 2015/8/30.
 * description:修改好友分组界面
 */
public class EditFriendGroupActivity extends TTBaseActivity implements View.OnClickListener {
    private Logger logger = Logger.getLogger(EditFriendGroupActivity.class);

    @InjectView(R.id.re_add_new_group)
    RelativeLayout re_add_new_group;
    @InjectView(R.id.listView)
    MyListView listView;

    private String groupId;
    private String uid;
    private List<FG_GroupJsonBean> groupJsonBeanList;       //好友分组集
    private MyAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (super.isNeedRestart()) {
            return;
        }
        View view = getLayoutInflater().inflate(R.layout.fg_activity_edit_friend_group, topContentView);
        ButterKnife.inject(this, view);
        initView();
    }

    private void initView() {
        groupJsonBeanList = FriendGroupManager.getInstance().getGroup();
        groupId = getIntent().getStringExtra(IntentConstant.GROUP_ID);
        uid = getIntent().getStringExtra(IntentConstant.UID);
        re_add_new_group.setOnClickListener(this);

        setTitle("好友分组");
        topLeftBtn.setVisibility(View.VISIBLE);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        adapter = new MyAdapter();
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String oid = groupId;
                groupId = groupJsonBeanList.get(position).getGroupid();
                adapter.notifyDataSetChanged();
                changeFriendsGroup(groupId, oid, uid);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.re_add_new_group:
                showAddGroupDialog();
                break;

        }
    }


    class MyAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return groupJsonBeanList.size();
        }

        @Override
        public Object getItem(int position) {
            return groupJsonBeanList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.item_listview_friend_group_move, null);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            if (groupId.equals(groupJsonBeanList.get(position).getGroupid())) {
                holder.iv_check.setVisibility(View.VISIBLE);
            } else {
                holder.iv_check.setVisibility(View.GONE);
            }

            holder.tv_name.setText(groupJsonBeanList.get(position).getGroupname());
            return convertView;
        }
    }

    class ViewHolder {
        TextView tv_name;
        ImageView iv_check;

        public ViewHolder(View view) {
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            iv_check = (ImageView) view.findViewById(R.id.iv_check);
        }
    }


    /**
     * 添加分组的弹出框
     */
    public void showAddGroupDialog() {
        View view = LayoutInflater.from(this).inflate(R.layout.dialog_add_group, null);
        final Dialog dialog = new Dialog(this, R.style.dialog_theme_transparent);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        dialog.getWindow().setContentView(view);

        final EditText dialog_message = (EditText) view.findViewById(R.id.edit_content);
        View.OnClickListener myListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.tv_cancel:
                        dialog.dismiss();
                        break;
                    case R.id.tv_confirm:
                        String newName = dialog_message.getText().toString();
                        if (newName.equals("")) {
                            T.show(EditFriendGroupActivity.this, "名字不能为空！", 0);
                            return;
                        }

                        createFriendsGroup(newName);
                        dialog.dismiss();
                        break;
                }
            }
        };
        view.findViewById(R.id.tv_confirm).setOnClickListener(myListener);
        view.findViewById(R.id.tv_cancel).setOnClickListener(myListener);
    }


    /**
     * 新建好友分组
     *
     * @param name 分组名，可重复
     */
    private void createFriendsGroup(final String name) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("friendgroupname", name);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.CreateFriendsGroup, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (!object.getString("result").equals("0")) {
                        String nid = object.getString("result");
                        FG_GroupJsonBean fg_groupJsonBean = new FG_GroupJsonBean();
                        fg_groupJsonBean.setGroupname(name);
                        fg_groupJsonBean.setGroupid(nid);
                        FriendGroupManager.getInstance().addNewGroup(fg_groupJsonBean);
                        changeFriendsGroup(nid, groupId, uid);
                    } else {
                        T.show(EditFriendGroupActivity.this, object.getString("describe"), 0);
                    }

                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

    /**
     * 变更好友分组
     */
    private void changeFriendsGroup(final String nid, final String oid, final String fid) {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("orignfriendgroupid", oid);
        params.addParams("newfriendgroupid", nid);
        params.addParams("frienduserid", fid);
        params.addParams("token", "1");
        OkHttpClientManager.postAsy(ConstantValues.ChangeFriendsGroupMember, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject object = new JSONObject(response);
                    if (object.getString("result").equals("1")) {
                        FriendGroupManager.getInstance().changeGroup(fid, oid, nid);
                        setResult(RESULT_OK);
                        EditFriendGroupActivity.this.finish();
                    } else {
                        T.show(EditFriendGroupActivity.this, object.getString("describe"), 0);
                    }

                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.show(getApplicationContext(), e.toString(), 1);
            }
        });
    }

}
