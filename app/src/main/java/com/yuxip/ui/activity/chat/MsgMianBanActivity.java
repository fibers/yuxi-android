package com.yuxip.ui.activity.chat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.base.TTBaseActivity;
import com.yuxip.utils.DialogHelper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by SummerRC on 2015/6/5.
 * description : 面板
 */
public class MsgMianBanActivity extends TTBaseActivity {
    @InjectView(R.id.et_input)
    EditText et_input;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        View.inflate(MsgMianBanActivity.this, R.layout.activity_msg_mianban,topContentView);
        String msg = getIntent().getStringExtra(IntentConstant.INPUT_CONTENT);
        ButterKnife.inject(this, topContentView);
        if(msg!=null){
            et_input.setText(msg);
            et_input.setSelection(msg.length());
        }
        //设置光标
        if (!msg.isEmpty()){
            et_input.setSelection(msg.length());
        }
        setLeftButton(R.drawable.back_default_btn);
        topLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard();
                DialogHelper.showExitDialog(MsgMianBanActivity.this);
            }
        });
        setRighTitleText("保存");
        setTitle("面板");
        righTitleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String content = et_input.getText().toString().trim();
                if (content.isEmpty()) {
                    showToast("输入不能为空");
                } else {
                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putString(IntentConstant.INPUT_CONTENT, content);
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    /** 强制隐藏键盘 */
                    InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    finish();
                }
            }
        });

    }


    /**
     * 监听返回键
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:        //监听返回键
                hideKeyBoard();
                DialogHelper.showExitDialog(MsgMianBanActivity.this);
                break;
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        ButterKnife.reset(this);
        super.onDestroy();
    }

    /**
     * 隐藏键盘
     */
    public void hideKeyBoard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        et_input.clearFocus();
        inputManager.hideSoftInputFromWindow(et_input.getWindowToken(), 0);
    }
}

