package com.yuxip.ui.activity.chat;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;

import com.yuxip.JsonBean.StoryShowTypeJsonBean;
import com.yuxip.R;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.ui.activity.base.TTBaseNewFragmentActivity;
import com.yuxip.ui.fragment.other.FamilyAllFragment;
import com.yuxip.utils.IMUIHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Lyy on 2015/9/4.
 * description:所有家族、按时间或者人数排序
 * <p/>
 * 这是单独写的Activity 因为没想好怎么同时继承TTBase 和 FragmentActivity
 */
public class FamilyAllActivity extends TTBaseNewFragmentActivity implements View.OnClickListener {

    @InjectView(R.id.tab_layout_family_all_activity)
    TabLayout tabLayout;
    @InjectView(R.id.viewpager_family_all_activity)
    ViewPager viewPager;
    @InjectView(R.id.img_back_family_all_activity)
    ImageView img_back;
    @InjectView(R.id.img_search_family_all_activity)
    ImageView img_search;

    private List<Fragment> fragmentList = new ArrayList<>();

    private List<StoryShowTypeJsonBean.FamilytypeEntity> list_family = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        setContentView(R.layout.activity_family_all);
        ButterKnife.inject(this);
        initView();
    }

    private void initView() {
        list_family = HomeEntityManager.getInstance().getFamilyList();
        int color_select = getResources().getColor(R.color.pink);
        int color_unSelect = Color.parseColor("#4a4a4a");
        tabLayout.setTabMode(TabLayout.MODE_FIXED);

        tabLayout.setTabTextColors(color_unSelect, color_select);
        initFragments();

        MyAdapter adapter = new MyAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(adapter);
        img_back.setOnClickListener(this);
        img_search.setOnClickListener(this);
    }

    private void initFragments() {
        for (int i = 0; i < list_family.size(); i++) {
            /**********     循环添加碎片    ************/
            FamilyAllFragment familyAllFragment = FamilyAllFragment.instance(list_family.get(i).getTypeid());
            fragmentList.add(familyAllFragment);
            tabLayout.addTab(tabLayout.newTab().setText(list_family.get(i).getTypename()));
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_family_all_activity:
                this.finish();
                break;
            case R.id.img_search_family_all_activity:
                IMUIHelper.openFamilySearchActivity(this);
                break;
        }
    }


    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return list_family.get(position).getTypename();
        }
    }

}
