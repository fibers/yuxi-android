package com.yuxip.ui.activity.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.yuxip.R;
import com.yuxip.ui.activity.base.TTBaseActivity;

/**
 * Created by Administrator on 2015/6/19.
 * description:帮助信息
 */
public class HelpMessageActivity extends TTBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (super.isNeedRestart()) {
            return;
        }
        View view = LayoutInflater.from(this).inflate(R.layout.activity_help_message, topContentView);
        WebView webView = (WebView) view.findViewById(R.id.webView);
        setTitle("帮助信息");
        setLeftButton(R.drawable.back_default_btn);
        webView.loadUrl("http://yuxip.com/androidhelp.php");
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
    }
}
