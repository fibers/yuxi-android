package com.yuxip.ui.activity.chat;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FriendEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.base.TTBaseNewActivity;
import com.yuxip.ui.adapter.SearchPersonAdapter;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * 好友搜索
 * Created by SummerRC on 2015/4/10.
 */
public class FriendSearchActivity extends TTBaseNewActivity {
    private Logger logger = Logger.getLogger(FriendSearchActivity.class);

    private ListView searchListView;
    private SearchPersonAdapter mSearchPersonAdapter;
    private List<FriendEntity> searchPersonList;            // 用于存储的搜索好友列表
    private EditText et_search;
    private ProgressBar progressBar;
    private MyHandler myHandler = new MyHandler(this);

    private IMService imService;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        @Override
        public void onServiceDisconnected() {
        }

        @Override
        public void onIMServiceConnected() {
            logger.d("login#onIMServiceConnected");
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        imServiceConnector.unbindService(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(super.isNeedRestart()) {
            return;
        }
        /** 隐藏标题栏 */
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_search);
        init();
        imServiceConnector.connect(this);
    }

    /**
     * 初始化相关操作
     */
    private void init() {
        searchListView = (ListView) findViewById(R.id.lv_search);
        searchListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                FriendEntity fe = (FriendEntity) mSearchPersonAdapter.getItem(position);
                if(fe.getId().equals(IMLoginManager.instance().getLoginId()+"")) {
                    Toast.makeText(FriendSearchActivity.this, "不能添加自己为好友!", Toast.LENGTH_SHORT).show();
                } else {
                    IMUIHelper.openUserHomePageActivity(FriendSearchActivity.this, fe.getId()+"");
                    finish();
                }
            }
        });
        searchListView.setOnScrollListener(
                new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        findViewById(R.id.iv_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);
            }
        });
        et_search = (EditText)findViewById(R.id.et_search);
        et_search.setHint("请输入好友昵称或ID号");
        et_search.addTextChangedListener(new TextWatcher() {           //监听EditText的文本变化
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        et_search.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER)
                    return true;
                return false;
            }
        });

        /** 点击搜索的监听事件 */
        findViewById(R.id.tv_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchPerson();
            }


        });
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
    }

    /**
     * 搜索好友
     */
    private void SearchPerson() {
        String id = IMLoginManager.instance().getLoginId()+"";
        String cond = et_search.getText().toString().trim();
        /** 放在params里面传递 */
        if(TextUtils.isEmpty(cond)){
            T.show(getApplicationContext(),"输入内容不能为空",1);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", id);
        params.addParams("cond", cond);

        OkHttpClientManager.postAsy(ConstantValues.SEARCHPERSON, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                searchPersonList = new ArrayList<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String id = object.getString("result");
                    if (id.equals("1")) {
                        JSONArray array = object.getJSONArray("personlist");
                        if(array.length() == 0) {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(getApplicationContext(),"用户不存在！",Toast.LENGTH_SHORT).show();
                            return;
                        }
                        for (int i = 0; i < array.length(); i++) {
                            Log.e("arr_length", i + "");
                            JSONObject obj = array.getJSONObject(i);
                            FriendEntity friendEntity = new FriendEntity();
                            friendEntity.setId(obj.get("id")+"");
                            friendEntity.setName(obj.get("name").toString());
                            friendEntity.setGender(obj.get("gender")+"");
                            friendEntity.setPortrait(obj.get("portrait").toString());
                            searchPersonList.add(friendEntity);
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"查找失败请重试",Toast.LENGTH_SHORT).show();
                    }

                    Message msg = new Message();
                    msg.what = 1;
                    msg.obj = searchPersonList;
                    myHandler.sendMessage(msg);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                progressBar.setVisibility(View.GONE);
                T.show(getApplicationContext(), "查找失败请重试", 1);
            }
        });
    }

    static class MyHandler extends Handler {
        WeakReference<FriendSearchActivity> mActivity;         //持有StoryChatActivity对象的弱引用
        MyHandler(FriendSearchActivity activity) {
            mActivity = new WeakReference<>(activity);
        }
        @Override
        public void handleMessage(Message msg) {
            FriendSearchActivity activity = mActivity.get();
            if(activity == null) {
                return;
            }
            switch (msg.what) {
                case 1:     //如果请求成功  设置所有界面的adapter
                    activity.progressBar.setVisibility(View.GONE);
                    activity.mSearchPersonAdapter = new SearchPersonAdapter(activity.searchPersonList, activity, activity.imService);
                    activity.searchListView.setAdapter(activity.mSearchPersonAdapter);
                    break;
            }
        }
    }


}
