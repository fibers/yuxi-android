package com.yuxip.ui.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.utils.Logger;
import com.yuxip.utils.SharedPreferenceUtils;

/**
 * create by SummerRC on 2015/08/07
 * description: 监听Home键的广播接收器
 */
public class HomeWatcherReceiver extends BroadcastReceiver {
    private static final String LOG_TAG = "HomeReceiver";
    private static final String SYSTEM_DIALOG_REASON_KEY = "reason";
    private static final String SYSTEM_DIALOG_REASON_RECENT_APPS = "recentapps";
    private static final String SYSTEM_DIALOG_REASON_HOME_KEY = "homekey";
    private static final String SYSTEM_DIALOG_REASON_LOCK = "lock";
    private static final String SYSTEM_DIALOG_REASON_ASSIST = "assist";
    private Logger logger;

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals(Intent.ACTION_CLOSE_SYSTEM_DIALOGS)) {
            String reason = intent.getStringExtra(SYSTEM_DIALOG_REASON_KEY);
            logger.i("reason: " + reason);

            if (SYSTEM_DIALOG_REASON_HOME_KEY.equals(reason)) {             // 短按Home键
                logger.i("HomeKey");
                SharedPreferenceUtils.saveBooleanDate(context, SharedPreferenceValues.HOME_KEY, true);

            } else if (SYSTEM_DIALOG_REASON_RECENT_APPS.equals(reason)) {   // 长按Home键 或者 activity切换键
                logger.i("long press home key or activity switch");
                SharedPreferenceUtils.saveBooleanDate(context, SharedPreferenceValues.HOME_KEY, true);
            } else if (SYSTEM_DIALOG_REASON_LOCK.equals(reason)) {          // 锁屏
                logger.i(LOG_TAG, "lock");
            } else if (SYSTEM_DIALOG_REASON_ASSIST.equals(reason)) {        // SamSung 长按Home键
                logger.i(LOG_TAG, "assist");
            }
        }
    }

    private HomeWatcherReceiver() {
        logger = Logger.getLogger(HomeWatcherReceiver.class);
    }

    public static HomeWatcherReceiver init() {
        if(mHomeKeyReceiver == null) {
            mHomeKeyReceiver = new HomeWatcherReceiver();
        }
        return mHomeKeyReceiver;
    }
    private static HomeWatcherReceiver mHomeKeyReceiver = null;

    public void registerHomeKeyReceiver(Context context) {
        logger.i("registerHomeKeyReceiver");
        final IntentFilter homeFilter = new IntentFilter(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
        context.registerReceiver(mHomeKeyReceiver, homeFilter);
    }

    public void unregisterHomeKeyReceiver(Context context) {
        logger.i("unregisterHomeKeyReceiver");
        try {
            if (null != mHomeKeyReceiver) {
                context.unregisterReceiver(mHomeKeyReceiver);
            }
        } catch (Exception e) {
            logger.e(e.toString());
        }

    }

}
