package com.yuxip.ui.helper;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.yuxip.R;
import com.yuxip.imservice.manager.http.OkHttpClientManager;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.util.Hashtable;

/**
 * Created by SummerRC on 2015/10/22.
 * description:占位图的缓存
 */
public class DrawableCache {

    public static final int KEY_PERSON_PORTRAIT = 1;
    public static final int KEY_GROUP_PORTRAIT = 2;
    public static final int KEY_DEFAULT_PIC = 3;
    private static DrawableCache drawableCache;
    private Context context;
    private Hashtable<Integer, MySoftReference> hashTable;

    /**
     * 垃圾Reference的队列（所引用的对象已经被回收，则将该引用存入队列中）
     */
    private ReferenceQueue<Drawable> referenceQueue;

    private DrawableCache(Context context) {
        hashTable = new Hashtable<>();
        referenceQueue = new ReferenceQueue<>();
        this.context = context.getApplicationContext();
    }

    /**
     * 单例模式，保证只有一个DrawableCache对象
     *
     * @return OkHttpClientManager
     */
    public static DrawableCache getInstance(Context context) {
        if (drawableCache == null) {
            synchronized (OkHttpClientManager.class) {
                if (drawableCache == null) {
                    drawableCache = new DrawableCache(context);
                }
            }
        }
        return drawableCache;
    }

    public Drawable getDrawable(int key) {
        Drawable drawable = null;
        /** 缓存中是否有该Drawable实例的软引用，如果有，从软引用中取得。 */
        if (hashTable.containsKey(key)) {
            SoftReference<Drawable> softReference = hashTable.get(key);
            drawable = softReference.get();
        }
        /** 如果没有软引用，或者从软引用中得到的实例是null，重新构建一个实例，并保存对这个新建实例的软引用 */
        if (drawable == null) {
            switch (key) {
                case 1:
                    drawable = context.getResources().getDrawable(R.drawable.default_user_avatar_btn);
                    break;
                case 2:
                    drawable = context.getResources().getDrawable(R.drawable.icon_group_message);
                    break;
                case 3:
                    drawable = context.getResources().getDrawable(R.drawable.world_hot_list_img);
                    break;
                default:
                    throw new RuntimeException("Key is impossible !");
            }
            addDrawable(key, drawable);
        }
        return drawable;
    }

    /**
     * 添加缓存
     *
     * @param key      关键字
     * @param drawable Drawable
     */
    private void addDrawable(Integer key, Drawable drawable) {
        cleanCache();
        MySoftReference softReference = new MySoftReference(drawable, referenceQueue, key);
        hashTable.put(key, softReference);
    }

    /**
     * 清除被回收了引用对象的软可及对象
     */
    private void cleanCache() {
        MySoftReference softReference;
        while ((softReference = (MySoftReference) referenceQueue.poll()) != null) {
            hashTable.remove(softReference.key);
        }
    }

    private class MySoftReference extends SoftReference<Drawable> {
        private Integer key;

        public MySoftReference(Drawable drawable, ReferenceQueue<Drawable> q, Integer key) {
            super(drawable, q);
            this.key = key;
        }
    }

}
