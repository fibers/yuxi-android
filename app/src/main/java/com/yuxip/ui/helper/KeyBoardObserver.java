package com.yuxip.ui.helper;

import android.graphics.Rect;
import android.view.View;
import android.view.ViewTreeObserver;

/**
 * Created by Ly on 2015/11/11.
 */
public class KeyBoardObserver {
    private View baseRoot;
    private View displayView;
    private int rootBottom = Integer.MIN_VALUE;

    public void setBaseRootAndView(View baseRoot,View passView){
        this.baseRoot = baseRoot;
        this.displayView = passView;
        baseRoot.getViewTreeObserver().addOnGlobalLayoutListener(onGlobalLayoutListener);
    }

    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            baseRoot.getGlobalVisibleRect(r);
            // 进入Activity时会布局，第一次调用onGlobalLayout，先记录开始软键盘没有弹出时底部的位置
            if (rootBottom == Integer.MIN_VALUE) {
                rootBottom = r.bottom;
                return;
            }
            // adjustResize，软键盘弹出后高度会变小
            if (r.bottom < rootBottom) {
                //按照键盘高度设置表情框和发送图片按钮框的高度
                displayView.setVisibility(View.GONE);

            }else{
                displayView.setVisibility(View.VISIBLE);

            }
        }
    };

}
