package com.yuxip.ui.customview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.StoryDetailsBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.other.ReportActivity;
import com.yuxip.ui.activity.story.StoryEditActivity;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.ShareUtil;
import com.yuxip.utils.T;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 剧详情资料页面的弹出框
 * Created by HeTianpeng on 2015/07/09.
 */
public class StoryDetailsPopupWindow implements View.OnClickListener {
    private Logger logger = Logger.getLogger(StoryDetailsPopupWindow.class);

    /**
     * 包含删除剧和收藏剧的回调方法
     */
    public interface IsDelStoryInterface {
        void isDel(boolean isDel);

        void isCollect(boolean isCollect);
    }

    IsDelStoryInterface delStory;

    @InjectView(R.id.tv_bianji)
    TextView tvBianji;
    @InjectView(R.id.view_bianji)
    View viewBianji;
    @InjectView(R.id.tv_copyright)
    TextView tvCopyright;
    @InjectView(R.id.tv_del)
    TextView tvDel;
    @InjectView(R.id.view_del)
    View viewDel;
    @InjectView(R.id.tv_jubao)
    TextView tvJubao;
    @InjectView(R.id.tv_share)
    TextView tvShare;
    @InjectView(R.id.tv_collect)
    TextView tvCollect;
    AppCompatEditText editDesc;
    AppCompatTextView tvTitle;
    AppCompatTextView tvClass;
    AppCompatButton btnRecommend;
    private PopupWindow pop;
    private Context context;
    private StoryDetailsBean sdb;
    private float density;
    private boolean isCollect;

    public StoryDetailsPopupWindow(Context context) {
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.stroy_details_pop, null);
        ButterKnife.inject(this, view);
        density = context.getResources().getDisplayMetrics().density;
        pop = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        pop.setOutsideTouchable(true);
        pop.setTouchable(true);
        pop.setFocusable(true);
        pop.setBackgroundDrawable(new BitmapDrawable());

        tvBianji.setOnClickListener(this);
        tvCopyright.setOnClickListener(this);
        tvDel.setOnClickListener(this);
        tvJubao.setOnClickListener(this);
        tvShare.setOnClickListener(this);
        tvCollect.setOnClickListener(this);
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        pop.setOnDismissListener(onDismissListener);
    }

    private boolean isMine;

    /**
     * PopupWindow关闭时显示；
     * 显示时关闭。
     */
    public void show(View view, StoryDetailsBean storyDetailsBean) {
        if (sdb == null) {
            sdb = storyDetailsBean;
        }
        isMine = true;
        if (!(IMLoginManager.instance().getLoginId() + "").equals(sdb.getCreatorid())) {
            tvBianji.setVisibility(View.GONE);
            viewBianji.setVisibility(View.GONE);
            tvDel.setText("推荐到招募区");
            tvCollect.setVisibility(View.VISIBLE);
            isMine = false;
        } else {
            isMine = true;
            tvJubao.setText("推荐到招募区");
        }

        if (storyDetailsBean.getIsCollectedByUser().equals("0")) {
            isCollect = false;
            tvCollect.setText("收藏");
        } else {
            isCollect = true;
            tvCollect.setText("取消收藏");
        }

        if (pop != null) {
            if (pop.isShowing()) {
                pop.dismiss();
            } else {
                int height = 0;
                int marginRight = (int) (80 * density);
                height = (int) (132 * density);
                pop.setHeight(height);
                pop.showAsDropDown(view, -marginRight, 4);
            }
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_bianji:            //编辑剧
                intent = new Intent(context, StoryEditActivity.class);
                intent.putExtra(IntentConstant.CREATOR_ID, sdb.getCreatorid());
                intent.putExtra(IntentConstant.STORY_ID, sdb.getId());
                intent.putExtra(IntentConstant.RELATION, sdb.getRelation());
                intent.putExtra(IntentConstant.PERMISSION, sdb.getReadautority());
                ((Activity) context).startActivityForResult(intent, 0);
                pop.dismiss();
                break;
            case R.id.tv_copyright:         //更改为版权
                IMUIHelper.openCopyRightActivity(context, sdb.getId(), sdb.getCopyright());
                break;
            case R.id.tv_del:               //删除剧
                if (isMine) {   //删除
                    showDelAlert();
                } else {        //推荐到招募区
                    requestRecomment();
                }
                break;
            case R.id.tv_jubao:             //举报
                if (isMine) {   //推荐到招募区
                    requestRecomment();
                } else {        //举报
                    intent = new Intent(context, ReportActivity.class);
                    intent.putExtra(IntentConstant.REPORT_TYPE, IntentConstant.STORY_ID);
                    intent.putExtra(IntentConstant.STORY_ID, sdb.getId());
                    context.startActivity(intent);
                }

                break;
            case R.id.btn_recommend:        //确定推荐到招募区       -+
                btnRecommend.setEnabled(false);
                requestRecomment();
                break;
            case R.id.tv_share:             //分享
                new ShareUtil(context).addCustomPlatforms(sdb.getIntro(), ConstantValues.ShareStoryUrl + sdb.getId(), sdb.getTitle(), sdb.getStoryimg());
                break;
            case R.id.tv_collect:
                collectStory();
                break;
        }
    }

    private void showDelAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("警告");
        builder.setMessage("确定要删除该剧吗");
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                delStory();
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }


    /**
     * 删除剧
     */
    private void delStory() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", sdb.getId());
        OkHttpClientManager.postAsy(ConstantValues.DeleteStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;

                try {
                    JSONObject jo = new JSONObject(content);
                    String result = jo.getString("result");
                    if ("1".equals(result)) {
                        T.showShort(context, "删除成功");
                        if (delStory != null) {
                            delStory.isDel(true);
                            pop.dismiss();
                        }
                    } else {
                        T.showShort(context, "删除失败");
                    }
                } catch (JSONException e) {
                    T.show(context.getApplicationContext(), e.toString(), 1);
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                T.showShort(context, context.getString(R.string.network_err));
            }
        });
    }

    /**
     * 推荐到招募区
     */
    private void requestRecomment() {

        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
//        param.addBodyParameter("content", editDesc.getText().toString().trim());
        params.addParams("content", "");
        params.addParams("storyid", sdb.getId());
        OkHttpClientManager.postAsy(ConstantValues.RecommendStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;
                try {
                    JSONObject jo = new JSONObject(content);
                    String result = jo.getString("result");
                    if ("1".equals(result)) {
                        T.showShort(context, "推荐成功,请在招募区查看！");
                    } else {
                        T.showShort(context, "推荐失败");
                    }
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }


    /**
     * 收藏剧
     */
    private void collectStory() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", sdb.getId());
        params.addParams("type", "1");
        if (isCollect) {
            params.addParams("behavior", 0 + "");
        } else {
            params.addParams("behavior", 1 + "");
        }
        OkHttpClientManager.postAsy(ConstantValues.CollectStory, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getString("result").equals("1")) {
                        if (!isCollect) {
                            Toast.makeText(context, "收藏成功", Toast.LENGTH_LONG).show();
                            tvCollect.setText("取消收藏");
                            sdb.setIsCollectedByUser("1");
                            isCollect = true;
                        } else {
                            Toast.makeText(context, "取消收藏成功", Toast.LENGTH_LONG).show();
                            tvCollect.setText("收藏");
                            sdb.setIsCollectedByUser("0");
                            isCollect = false;
                        }
                        if (delStory != null)
                            delStory.isCollect(isCollect);

                    } else {
                        Toast.makeText(context, jsonObject.getString("describe"), Toast.LENGTH_LONG).show();
                    }
                    pop.dismiss();
                } catch (JSONException e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }

    private AlertDialog alertDialog;
    private AlertDialog.Builder builder;

    private void showDialog() {
        builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.recommended_recruit, null);
        editDesc = (AppCompatEditText) view.findViewById(R.id.edit_desc);
        tvTitle = (AppCompatTextView) view.findViewById(R.id.tv_title);
        tvClass = (AppCompatTextView) view.findViewById(R.id.tv_class);
        btnRecommend = (AppCompatButton) view.findViewById(R.id.btn_recommend);
        btnRecommend.setOnClickListener(this);
        builder.setView(view);

        if (sdb != null) {
            tvTitle.setText(sdb.getTitle());
            tvClass.setText(sdb.getCategory());
        }

        alertDialog = builder.show();
    }

    public void setDelStoryInterface(StoryDetailsPopupWindow.IsDelStoryInterface del) {
        if (delStory == null) {
            delStory = del;
        }
    }
}
