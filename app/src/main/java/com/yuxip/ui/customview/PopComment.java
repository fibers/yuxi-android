package com.yuxip.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.R;

/**
 * Created by Ly on 2015/10/9.
 * 每个楼层弹出的 举报 点赞 和收藏的 小控件
 *
 */
public class PopComment  extends RelativeLayout {
    private Context context;
    private ScaleAnimation scaleAnimation_show;
    private ScaleAnimation scaleAnimation_dismiss;

    private RelativeLayout relativeLayout;

    private TextView tv_report; //举报
    private RelativeLayout rel_collect; //收藏
    private RelativeLayout rel_comment; //评论

    private ImageView iv_collect;

    private int position;

    private int showTimes; //显现次数  父布局和子布局 同时只能操作一次

    public void setCurPosition(int positin){
        this.position = positin;
    }
    public PopComment(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public PopComment(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();
    }

    public PopComment(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }


    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.pop_comment,this,true);
        relativeLayout = (RelativeLayout) findViewById(R.id.rel_pop_comment);
        tv_report = (TextView) findViewById(R.id.tv_report_popcomment);
        rel_collect = (RelativeLayout) findViewById(R.id.rel_collect_popcomment);
        rel_comment = (RelativeLayout) findViewById(R.id.rel_comment_popcomment);
        iv_collect = (ImageView) findViewById(R.id.iv_collect_popcomment);
//        tv_report.setOnClickListener(this);
//        rel_collect.setOnClickListener(this);
//        rel_comment.setOnClickListener(this);

        scaleAnimation_show = new ScaleAnimation(0f,1f,1f,1f, Animation.RELATIVE_TO_SELF,1f,Animation.RELATIVE_TO_SELF,1f);
        scaleAnimation_dismiss = new ScaleAnimation(1f,0f,1f,1f,Animation.RELATIVE_TO_SELF,1f,Animation.RELATIVE_TO_SELF,1f);
        scaleAnimation_show.setFillAfter(true);
        scaleAnimation_show.setDuration(200);
        scaleAnimation_dismiss.setFillAfter(true);
        scaleAnimation_dismiss.setDuration(200);

        scaleAnimation_dismiss.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                relativeLayout.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }

    private boolean isShow = false;
    public void showOrHidePop(){
        if(isShow){
            hideAnimation();
        }else{
            showAnimation();
        }
    }

    public boolean isShow(){
        return  this.isShow;
    }

    public void showAnimation(){
        relativeLayout.setVisibility(View.GONE);
        if(isShow)
            return;
//        popComment.setVisibility(View.VISIBLE);
        relativeLayout.setVisibility(View.VISIBLE);
        relativeLayout.startAnimation(scaleAnimation_show);
        isShow = true;
    }

    public void hideAnimation(){
        if(!isShow){
            relativeLayout.setVisibility(View.GONE);
            return;
        }
        relativeLayout.startAnimation(scaleAnimation_dismiss);
//        relativeLayout.setVisibility(View.GONE);
        isShow = false;
    }


    public void setCollectSelect(boolean selected){
        if(selected){
            iv_collect.setSelected(true);
        }else{
            iv_collect.setSelected(false);
        }
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.tv_report_popcomment:
//                Toast.makeText(context,"举报-->>"+position,Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.rel_collect_popcomment:
//                Toast.makeText(context,"收藏-->>"+position,Toast.LENGTH_SHORT).show();
//                break;
//            case R.id.rel_comment_popcomment:
//                Toast.makeText(context,"评论-->>"+position,Toast.LENGTH_SHORT).show();
//                break;
//        }
//    }

    public TextView getReportView(){
        return  this.tv_report;
    }

    public RelativeLayout getCollectView(){
        return  this.rel_collect;
    }

    public RelativeLayout getCommentView(){
        return  this.rel_comment;
    }

}
