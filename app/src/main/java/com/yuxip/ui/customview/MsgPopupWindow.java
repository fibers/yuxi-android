package com.yuxip.ui.customview;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.ui.activity.chat.SearchStoryActivity;
import com.yuxip.utils.IMUIHelper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 消息界面的添加好友，家族，剧。。。
 * Created by HeTianpeng on 2015/07/09.
 */
public class MsgPopupWindow implements View.OnClickListener {
    @InjectView(R.id.tv_add_friend)
    TextView tvAddFriend;
    @InjectView(R.id.tv_add_family)
    TextView tvAddFamily;
    @InjectView(R.id.tv_add_drama)
    TextView tvAddDrama;
    private PopupWindow pop;
    private Context context;

    private int dimenDensity;
    private float density;
    public MsgPopupWindow(Context context) {
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.msg_pop, null);
        ButterKnife.inject(this, view);
        dimenDensity= (int) (160*context.getResources().getDisplayMetrics().density);
        density=context.getResources().getDisplayMetrics().density;
        pop = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        pop.setOutsideTouchable(true);
        pop.setTouchable(true);
        pop.setFocusable(true);
        pop.setBackgroundDrawable(new BitmapDrawable());

        tvAddDrama.setOnClickListener(this);
        tvAddFamily.setOnClickListener(this);
        tvAddFriend.setOnClickListener(this);
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener){
        pop.setOnDismissListener(onDismissListener);
    }

    /**
     * PopupWindow关闭时显示；
     * 显示时关闭。
     *
     * 此处有些问题，后期应该准备多套背景用于替换，而不是改高度
     */
    public void show(View view) {
        if (pop != null) {
            if (pop.isShowing()) {
                pop.dismiss();
            } else {

                showPopWindow(view);
            }
        }
    }
    private void showPopWindow(View view){
        int marginRight=(int) (80*density);
        int height= (int) (100*density);
        pop.setHeight(height);
        pop.showAsDropDown(view,-marginRight,4);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_add_friend: //添加好友
                IMUIHelper.openFriendSearchActivity(context);
                break;
            case R.id.tv_add_family: //添加家族
                IMUIHelper.openFamilySearchActivity(context);
                break;
            case R.id.tv_add_drama: //添加剧群
                context.startActivity(new Intent(context, SearchStoryActivity.class));
                break;
        }
    }
}
