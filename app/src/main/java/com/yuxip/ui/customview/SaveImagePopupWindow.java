package com.yuxip.ui.customview;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageUtil;

import java.io.File;
import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/12.
 * description:剧详情页面点击图片的弹出框：查看大图保存原图
 */
public class SaveImagePopupWindow implements View.OnClickListener {

    @InjectView(R.id.tv_save_saveimgpop)
    TextView saveImg;
    @InjectView(R.id.tv_view_saveimgpop)
    TextView viewImg;
    @InjectView(R.id.tv_cancel_saveimgpop)
    TextView cancelImg;

    private View view;
    private Context context;

    private String path;        //图片的保存路径
    private String imgUrl;
    private ImageMessage imageMessage;
    private ArrayList<ImageMessage> list_mess = new ArrayList<>();
    private PopupWindow popupWindow;
    private SaveImgCallBack saveImgCallBack;

    public SaveImagePopupWindow(Context context, SaveImgCallBack saveImgCallBack) {
        this.context = context;
        this.saveImgCallBack = saveImgCallBack;
        view = LayoutInflater.from(context).inflate(R.layout.pop_saveimg, null);
        ButterKnife.inject(this, view);
        initPopWindow();
        initListener();
    }

    public SaveImagePopupWindow(Context context) {
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.pop_saveimg, null);
        ButterKnife.inject(this, view);
        initPopWindow();
        initListener();
    }

    private void initPopWindow() {
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true);

        popupWindow.setAnimationStyle(R.style.AnimationImageSavePopupWindow);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
    }


    private void initListener() {
        saveImg.setOnClickListener(this);
        cancelImg.setOnClickListener(this);
        viewImg.setOnClickListener(this);
    }


    public void showPop(ImageView view, ImageMessage imageMessage) {
        if (popupWindow != null && !popupWindow.isShowing()) {
            this.imageMessage = imageMessage;
            this.imgUrl = imageMessage.getUrl();
            popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        }
    }

    public void dissmissPop() {
        if (popupWindow != null && popupWindow.isShowing()) {
            popupWindow.dismiss();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_save_saveimgpop:
                Toast.makeText(context.getApplicationContext(), "正在保存", Toast.LENGTH_SHORT).show();
                if (!TextUtils.isEmpty(imgUrl)) {
                    saveImage();
                } else {
                    Toast.makeText(context, "保存失败,路径异常", Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.tv_cancel_saveimgpop:
                break;
            case R.id.tv_view_saveimgpop:
//                Intent intent = new Intent(context, PreviewReadImgActivity.class);
//                intent.putExtra(IntentConstant.CUR_MESSAGE, list_mess);
//                intent.putExtra(IntentConstant.POSITION, 0 + "");
//                context.startActivity(intent);
                IMUIHelper.OpenPreviewReadImaActivity(context, list_mess, 0 + "");
                break;
        }
        popupWindow.dismiss();
    }


    public interface SaveImgCallBack {
        void dismissPop();

        void saveImge(String url);

        void viewImg(ImageMessage imageMessage);
    }


    public void setList(ArrayList<ImageMessage> list) {
        if (list != null)
            this.list_mess = list;
    }

    private void notifyPictures(String filePath) {
        Intent intent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(new File(filePath));
        intent.setData(uri);
        context.sendBroadcast(intent);
    }

    private void saveImage() {
        File file = new File(Environment.getExternalStorageDirectory() + "/higgs/yuxi/");
        if (!file.exists()) {
            file.mkdirs();
        }
        this.path = ImageUtil.getPicPath();
        OkHttpClientManager.downLoadAsy(imgUrl, path,
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (!TextUtils.isEmpty(path)) {
                            Toast.makeText(context, "保存成功", Toast.LENGTH_LONG).show();
                            notifyPictures(path);
                        }
                    }

                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context, "保存失败：" + e.toString(), Toast.LENGTH_LONG).show();
                    }
                });
    }

}
