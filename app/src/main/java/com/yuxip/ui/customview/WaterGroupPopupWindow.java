package com.yuxip.ui.customview;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.utils.IMUIHelper;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/8.
 */
public class WaterGroupPopupWindow implements View.OnClickListener {
    private Context context;
    private PopupWindow pop;
    private float density;
    private String storyid;
    private String groupid;
    private WaterPopGroupMsg waterPopGroupMsg;
    @InjectView(R.id.tv_pop_water_group)
    TextView tv_group_msg;

    @InjectView(R.id.tv_pop_water_play)
    TextView tv_play_msg;

    public WaterGroupPopupWindow(Context context, WaterPopGroupMsg waterPopGroupMsg) {
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.pop_water_group, null);
        ButterKnife.inject(this, view);
        density = context.getResources().getDisplayMetrics().density;
        pop = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        pop.setOutsideTouchable(true);
        pop.setTouchable(true);
        pop.setFocusable(true);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(true);
        tv_group_msg.setOnClickListener(this);
        tv_play_msg.setOnClickListener(this);
        this.waterPopGroupMsg = waterPopGroupMsg;
    }


    public void show(String storyid, String groupid, View view) {
        this.storyid = storyid;
        this.groupid = groupid;
        pop.showAsDropDown(view, -(int) (70 * density), (int) (5 * density));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**剧资料*/
            case R.id.tv_pop_water_play:
                IMUIHelper.openStoryDetailsActivity(context, storyid);
                pop.dismiss();
                break;
            /**群资料*/
            case R.id.tv_pop_water_group:
                if (waterPopGroupMsg != null)
                    waterPopGroupMsg.showGroupMess();
                pop.dismiss();
                break;
        }
    }


    public interface WaterPopGroupMsg {
        void showGroupMess();
    }
}
