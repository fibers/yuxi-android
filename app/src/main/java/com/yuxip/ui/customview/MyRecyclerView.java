package com.yuxip.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

/**
 * Created by HeTianpeng on 2015/07/18.
 */
public class MyRecyclerView extends UltimateRecyclerView {
    public MyRecyclerView(Context context) {
        super(context);
    }

    public MyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRecyclerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public View getEmptyView(){
        return mEmptyView;
    }
}
