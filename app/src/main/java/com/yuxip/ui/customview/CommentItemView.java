package com.yuxip.ui.customview;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.utils.IMUIHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/10/9.
 * 每个楼层的主要 内容 包含 楼层内容 以及楼中回复
 *
 */
public class CommentItemView extends RelativeLayout implements View.OnClickListener{

    private Context context;

    private int position;   //当前条目在listivew中的位置
    private String commentType;
    private ImageView iv_pop_start;
    private PopComment popComment;
    private RelativeLayout rel_top;     //包含标签图片 只有热门和最新第一项显示 其他隐藏
    private ImageView iv_label;         //标签的图片
    private TextView tv_lable_name;     //标签的名字  热门 和 全部
    private RelativeLayout childCommentView;    //楼层的回复布局 没有回复时候隐藏
    private CommentMainItem commentMainItem;

    private TextView tv_moreComments;   //"更多回复" 可展开楼层回复
    private LinearLayout linear_container;//楼层的回复列表


    private TopicDetailsJsonBean.CommentEntity commentEntity;
    private List<TopicDetailsJsonBean.CommentEntity.ChildCommentEntity> list_child_comment = new ArrayList<>();

    private boolean isUnfold = false;
    private final int defalutCount1 = 2;        //默认展开的数目
    private final int defaultCount2 = 10;       //当前界面展开数目的最大数，超出则跳转新界面
    private int totalCount ;                    //总共的评论数, 这里应该传入,
    private int showCount;
    private int deletePosition;
    private int type;
    private String deleteUrl;

    public CommentItemView(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public CommentItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context =context;
        initRes();
    }

    public CommentItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context =context;
        initRes();
    }

    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.comment_item,this,true);
        rel_top = (RelativeLayout) findViewById(R.id.rel_label_comment_top);
        iv_label = (ImageView) findViewById(R.id.iv_label_comment);
        tv_lable_name = (TextView) findViewById(R.id.tv_lable_name);

        commentMainItem = (CommentMainItem) findViewById(R.id.customview_main_comment_item);
        popComment = (PopComment) commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.POP_COMMENT);
        iv_pop_start = (ImageView) commentMainItem.getCommentMainItemChildView(CommentMainItem.CommentMainItemEnum.IMAGEVIEW_START_POP);
        childCommentView = (RelativeLayout) findViewById(R.id.customview_childcommentview);
        linear_container = (LinearLayout) findViewById(R.id.linearlayout_child_comment);
        tv_moreComments = (TextView) findViewById(R.id.tv_remain_comments_child_comment);

        tv_moreComments.setOnClickListener(this);
        iv_pop_start.setOnClickListener(this);

    }



    public void setData(TopicDetailsJsonBean.CommentEntity commentEntity,String commentType){
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            type = 0;
            deleteUrl  =ConstantValues.DeleteStoryResource;
        }else{
            type =1;
            deleteUrl = ConstantValues.DeleteSquareResource;
        }
        this.commentEntity = commentEntity;
        this.commentType =commentType;
        this.list_child_comment = commentEntity.getChildComment();
        totalCount = Integer.valueOf(commentEntity.getTotalChildCommentCount());
        commentMainItem.setData(commentEntity);
        popComment.setVisibility(View.GONE);
        if(commentType.equals(IntentConstant.FLOOR_COMMENT_TYPE_ALL)){
            if(SquareCommentManager.getInstance().containsValeu(Long.valueOf(commentEntity.getCommentID()))){
                resetUnfoldCount();
            }else{
                initCount();
            }
        }else{
            if(SquareCommentManager.getInstance().containsHotValue(Long.valueOf(commentEntity.getCommentID()))){
                resetUnfoldCount();
            }else{
                initCount();
            }
        }

    }

    //显示头部标签  其实是 热门 评论 的那个图片及 文字  下同
    public void showTopLable(){
        rel_top.setVisibility(View.VISIBLE);
    }

    public void hideTopLabel(){
        rel_top.setVisibility(View.GONE);
    }

    public void setSetHotLable(){
        iv_label.setBackgroundResource(R.drawable.square_hot_comment);
        tv_lable_name.setText("热门评论");
        tv_lable_name.setTextColor(Color.parseColor("#da2d92"));
        rel_top.setVisibility(View.VISIBLE);
    }

    public void setCommentLabel(){
        iv_label.setBackgroundResource(R.drawable.square_all_comment);
        tv_lable_name.setText("全部评论");
        tv_lable_name.setTextColor(Color.parseColor("#0da9b1"));
        rel_top.setVisibility(View.VISIBLE);
    }

    //隐藏 弹出框
    public void  hidePopComment(){
        if(getPopCommentStatus())
        popComment.hideAnimation();
    }

    //获取弹出框的状态 是 和 否
    public boolean getPopCommentStatus(){
        return  popComment.isShow();
    }


    public void setCurPosition(int position){
        this.position = position;
        popComment.setCurPosition(position);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_pop_comment:
                if(SquareCommentManager.getInstance().getFoldPopPosition() == position){
                    SquareCommentManager.getInstance().resetFoldPopPosition();
                    return;
                }
                SquareCommentManager.getInstance().setFoldPopPosition(position);
                popComment.setVisibility(View.VISIBLE);
                popComment.showOrHidePop();
                break;
            case R.id.tv_remain_comments_child_comment:
                isUnfold= true;
                //加到管理中，来判断下次重绘是否展开列表
                if(commentType.equals(IntentConstant.FLOOR_COMMENT_TYPE_ALL)){
                    SquareCommentManager.getInstance().addNewData(Long.valueOf(commentEntity.getCommentID()));
                }else{
                    SquareCommentManager.getInstance().addNewHotData(Long.valueOf(commentEntity.getCommentID()));
                }
                //如果展开数 等于 默认的二次展开数目 同时 查看更多可点击 则会跳转新界面
                if(showCount == defaultCount2){
                    IMUIHelper.openResponseFloorActivity(context, commentEntity.getFloorCount(), null, commentEntity.getFromUser().getId(), commentEntity.getCommentID(), null);

                }else{
                    resetUnfoldCount();
                }
                break;
        }
    }

    public boolean isUndfold(){
        return this.isUnfold;
    }



    /**
     *  默认展开的类型  这里默认展开 defaultCount1 数目
     */
   private void initCount() {
      if(!checkStatus()){
          return;
      }

       linear_container.removeAllViews();
       if(totalCount <=defalutCount1){
           showCount = totalCount;
           tv_moreComments.setVisibility(View.GONE);
       }else{
           showCount = defalutCount1;
           tv_moreComments.setText("更多的"+(totalCount-defalutCount1)+"条评论");
           tv_moreComments.setVisibility(View.VISIBLE);
       }
       for(int i =0;i<showCount;i++){
           linear_container.addView(inflateView(i));
       }

   }

    /**
     * 用来填充布局 作为子view 加到linearlayout里面  实现伪listview效果
     * @param position 当前填充布局在容器中位置
     * @return  填充后的view
     */
    private View inflateView(int position){
        View  convertView = LayoutInflater.from(context).inflate(R.layout.list_item_child_comment,null);
        TextView tv_child_content = (TextView) convertView.findViewById(R.id.tv_child_item_content);
        ImageView iv_child_delete = (ImageView) convertView.findViewById(R.id.iv_delete_square_item_comment);
        SpannableStringBuilder spannableStringBuilder = strToSpann(position);
        iv_child_delete.setVisibility(View.GONE);
        if(list_child_comment.get(position).getFromUser()!=null&&
                !TextUtils.isEmpty(list_child_comment.get(position).getFromUser().getId())&&
                list_child_comment.get(position).getFromUser().getId().equals(IMLoginManager.instance().getLoginId()+"")){
            iv_child_delete.setVisibility(View.VISIBLE);
        }
        tv_child_content.setText(spannableStringBuilder);
        tv_child_content.setMovementMethod(LinkMovementMethod.getInstance());
        iv_child_delete.setTag(position);
        iv_child_delete.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                deletePosition = position;
                showDeleteAlertDialog();

            }
        });
        return convertView;
    }

    /**
     *     监测当前状态 0 ,1 分别监测 hot  和 comment
     *     在判断楼层评论是否为空  有值则显示 无值隐藏
     */
    private boolean checkStatus(){
        childCommentView.setVisibility(View.VISIBLE);
        if(totalCount==0){
            childCommentView.setVisibility(View.GONE);
            return false;
        }
            if(list_child_comment == null || list_child_comment.size()==0){
                childCommentView.setVisibility(View.GONE);
                return false;
            }else{
                return  true;
            }
    }

    /**
     *  默认展开的类型  这里默认展开 defaultCount2 数目
     */
    private  void resetUnfoldCount(){
        if(!checkStatus()){
            return;
        }
        linear_container.removeAllViews();
        if(totalCount<=defaultCount2){
            showCount =totalCount;
            tv_moreComments.setVisibility(View.GONE);
        }else{
            showCount = defaultCount2;
            tv_moreComments.setText(("更多的"+(totalCount-defaultCount2)+"条评论"));
            tv_moreComments.setVisibility(View.VISIBLE);
        }
        for(int i =0;i<showCount;i++){
            linear_container.addView(inflateView(i));
        }

    }




    //删除二级评论
    private void deleteChildFloor(){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID", list_child_comment.get(deletePosition).getCommentID());
        if(list_child_comment.get(deletePosition).getLevel()!=null&&list_child_comment.get(deletePosition).getLevel().equals("1")){
            params.addParams("resourceType", "2");
        }else{
            params.addParams("resourceType", "3");
        }
        OkHttpClientManager.postAsy(deleteUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context,getResources().getString(R.string.square_loading_fail),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                     onReceiveDeleteChildFloorData(response);
                    }
                });
    }

   private void  onReceiveDeleteChildFloorData(String response){
       try {
           JSONObject jsonObject = new JSONObject(response);
           if(jsonObject.getString("result").equals("100")){
               list_child_comment.remove(deletePosition);
               commentEntity.setTotalChildCommentCount((totalCount - 1) + "");
               totalCount -= 1;
               if (totalCount == 0) {
                   childCommentView.setVisibility(View.GONE);
                   return;
               }
               if (isUndfold()) {
                   resetUnfoldCount();
               } else {
                   initCount();
               }
               SquareCommentEvent squareCommentEvent = new SquareCommentEvent();
               squareCommentEvent.commentEntity =commentEntity;
               squareCommentEvent.commentId = commentEntity.getCommentID();
               squareCommentEvent.eventType = SquareCommentEvent.EventType.TYPE_RESET_CHILD_COMMENT;
               EventBus.getDefault().post(squareCommentEvent);
           }

       } catch (JSONException e) {
           e.printStackTrace();
       }

   }

    /**
     * 主要是点赞数 和收藏数的改变
     */
    public void notifyMainItemChanged(){
        commentMainItem.notifyBaseChanged();
    }
    /**
     * 根据文本替换成图片
     * @param
     * @return
     */
    public SpannableStringBuilder strToSpann(final int position) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        String text = "";
        String regex = SquareCommentManager.getInstance().getRegex();
        int start;
        if(SquareCommentManager.getInstance().isLz(list_child_comment.get(position).getFromUser().getId())){
            text = SquareCommentManager.getInstance().constructFloorName(list_child_comment.get(position).getFromUser().getNickName(),true);


        }else{
            text = SquareCommentManager.getInstance().constructFloorName(list_child_comment.get(position).getFromUser().getNickName(), false);

        }
        spannableStringBuilder.append(text);
        //  替换  将文字替换为图片
        if(text.contains(regex)){
            start = text.indexOf(regex);
            Drawable drawable = this.getResources().getDrawable(R.drawable.bg_louzhu);
            drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());//这里设置图片的大小
            ImageSpan imageSpan = new ImageSpan(drawable,ImageSpan.ALIGN_BASELINE);
            spannableStringBuilder.setSpan(imageSpan,start,
                    start+regex.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        }
        String content;
        if(list_child_comment.get(position).getToUser()!=null){
            if(list_child_comment.get(position).getLevel().equals("1")) {
                content = SquareCommentManager.getInstance().constructCommentContent(null, list_child_comment.get(position).getCommentContent(), list_child_comment.get(position).getCommentTime());
            }else{
                content = SquareCommentManager.getInstance().constructCommentContent(list_child_comment.get(position).getToUser().getNickName(),list_child_comment.get(position).getCommentContent(),list_child_comment.get(position).getCommentTime());
            }
        }else{
            content = SquareCommentManager.getInstance().constructCommentContent(null,list_child_comment.get(position).getCommentContent(),list_child_comment.get(position).getCommentTime());
        }
        text+=content;
        spannableStringBuilder.append(content);
//        }

        //设置姓名颜色
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#0da6ce")), 0,text.indexOf("："),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //设置日期颜色
        spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.parseColor("#949494")), text.lastIndexOf(" "),spannableStringBuilder.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableStringBuilder.setSpan(new AbsoluteSizeSpan(9, true),text.lastIndexOf(" "),spannableStringBuilder.length(),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        //设置名字的点击事件
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);    //去除超链接的下划线
            }

            @Override
            public void onClick(View widget) {
                if(list_child_comment.get(position).getFromUser().getId()!=null)
               IMUIHelper.openUserHomePageActivity(context,list_child_comment.get(position).getFromUser().getId());

            }
        }, 0, text.indexOf("：") + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        //回复主题内容分的点击  可用以跳转界面
        spannableStringBuilder.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                if(list_child_comment.get(position).getFromUser().getId()!=null)
                IMUIHelper.openResponseFloorActivity(context,commentEntity.getFloorCount(),list_child_comment.get(position).getFromUser().getNickName(),list_child_comment.get(position).getFromUser().getId(),commentEntity.getCommentID(),list_child_comment.get(position).getCommentID());
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        }, text.indexOf("：") + 1, spannableStringBuilder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableStringBuilder;
    }


    private void showDeleteAlertDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.square_prompt));
        builder.setMessage(context.getResources().getString(R.string.square_confim_delete_response));
        builder.setPositiveButton(context.getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteChildFloor();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}


