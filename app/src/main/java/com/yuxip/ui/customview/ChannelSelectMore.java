package com.yuxip.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/9/1.
 * 用于首页列表的下拉展开
 * 一般只初始化一次
 */
public class ChannelSelectMore extends LinearLayout {
    private Context context;
    private View view;
    private GridView gv_default;       //默认的选项，不可更改 ,为保持界面同步与下面都采用 gv
    private GridView gv_add;           //用于添加的选项,可更改,默认选择前四项
    private DefaultAdapter adapter_default; //默认第一个适配器，无卵用
    private AddAdapter adapter_add;         //第二个适配器,添加或取消操作都在这
    private boolean isChanged;          //判断选择内容是否发生改变，从而刷新界面
    private String loadType;                    //判断加载类型   用于判断剧和自戏的载入   0:剧  1:自戏
    private List<NodesEntity> list_total=new ArrayList<>();        //所有数据的集合
    private List<NodesEntity> list_default=new ArrayList<>();      //默认的三个,不可变
    private List<NodesEntity> list_add=new ArrayList<>();         //自己选择的,可改变，第一次默认选中前4个
    private List<NodesEntity> list_saved=new ArrayList<>();    //用以存放选中的标签集合

    private List<Boolean> list_boolean=new ArrayList<>();           //存储是否选中的集合,
    private DBInterface dbInterface;

    public ChannelSelectMore(Context context) {
        super(context);
        this.context=context;
        dbInterface = DBInterface.instance();
        initView();
    }

    public ChannelSelectMore(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        dbInterface = DBInterface.instance();
        initView();
    }

    public ChannelSelectMore(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context=context;
        dbInterface = DBInterface.instance();
        initView();
    }

    private void initView(){
        view=LayoutInflater.from(context).inflate(R.layout.customview_channelselect_more,this,true);
        gv_default= (GridView) view.findViewById(R.id.gv1_channelselect_more);
        gv_add= (GridView) view.findViewById(R.id.gv2_channelselect_more);
        adapter_default=new DefaultAdapter();
        adapter_add=new AddAdapter();
        gv_default.setAdapter(adapter_default);
        gv_add.setAdapter(adapter_add);
    }

    /**************     初始化数据  外部调用 一般只初始化一次    判断是来自哪个片段 剧和自戏通过字段区分  ****************/
    public void setData( List<NodesEntity> list_total,String loadType){
        if(list_total==null)
            return;
        this.loadType=loadType;
        this.list_total=list_total;
        list_saved = dbInterface.loadAllNodesEntityByEntityType(loadType);
        initListData();
        adapter_default.notifyDataSetChanged();
        adapter_add.notifyDataSetChanged();
    }


    /*****************  初始化数据  默认前三个不变  后四个默认初始选中*****************/
    private void initListData(){
        list_default.clear();
        list_add.clear();
        for(int i=0;i<3;i++){
            list_default.add(list_total.get(i));
        }
        for(int i=3;i<list_total.size();i++){
            list_add.add(list_total.get(i));
        }
        for(int i=0;i<list_add.size();i++){
            list_boolean.add(false);
        }
        if(list_saved==null||list_saved.size()==0){
            for(int i=0;i<list_default.size();i++){
                dbInterface.batchInsertOrUpdateNodesEntity(list_default.get(i));
                list_boolean.set(i,true);
            }
            for(int i=0;i<4;i++){
                dbInterface.batchInsertOrUpdateNodesEntity(list_add.get(i));
                list_boolean.set(i,true);
            }
              list_saved = dbInterface.loadAllNodesEntityByEntityType(loadType);
        }else{
            resetListBoolean();
        }
    }


    class DefaultAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return list_default.size();
        }

        @Override
        public Object getItem(int position) {
            return list_default.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
    }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if(convertView==null){
                convertView=LayoutInflater.from(context).inflate(R.layout.item_selectchannelmore_gvitem,null);
                holder=new ViewHolder(convertView);
                convertView.setTag(holder);
            }else{
                holder= (ViewHolder) convertView.getTag();
            }
                    holder.tv.setSelected(true);
                    holder.tv.setText(list_default.get(position).getName());
            return convertView;
        }

        class ViewHolder{
            TextView  tv;
           public  ViewHolder(View view){
                tv= (TextView) view.findViewById(R.id.tv_channelselect_more_gvitem);
            }
        }
    }

    class AddAdapter extends BaseAdapter{
        @Override
        public int getCount() {
            return list_add.size();
        }

        @Override
        public Object getItem(int position) {
            return list_add.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder2 holder;
            if(convertView==null){
                convertView=LayoutInflater.from(context).inflate(R.layout.item_selectchannelmore_gvitem,null);
                holder=new ViewHolder2(convertView);
                convertView.setTag(holder);
                holder.tv.setText(list_add.get(position).getName());
                convertView.setTag(holder);
            }else{
                holder = (ViewHolder2) convertView.getTag();
            }

            holder.tv.setTag(position);
            if(list_boolean.get((Integer) holder.tv.getTag())==true){
                holder.tv.setSelected(true);
            }else{
                holder.tv.setSelected(false);
            }
            holder.tv.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position= (int) v.getTag();
                    if(holder.tv.isSelected()){
                        holder.tv.setSelected(false);
                        list_boolean.set(position,false);
                    }else{
                        holder.tv.setSelected(true);
                        list_boolean.set(position,true);
                    }
                    isChanged=true;
                }
            });

            return convertView;
        }

        class ViewHolder2{
            TextView  tv;
            public  ViewHolder2(View view){
                tv= (TextView) view.findViewById(R.id.tv_channelselect_more_gvitem);
            }
        }
    }

    /*****************  判断保存下拉列表中是否包含保存的数据 是就点亮*****************/
    private boolean  initDataSelect(int position){
            for(int i=0;i<list_saved.size();i++){
                if(list_add.get(position).getId().equals(list_saved.get(i).getId())){
                    return true;
                }
            }
        return  false;
    }

    /********************     重置 list_boolean 集合    ***************************/
    private void resetListBoolean(){
        for(int i=0 ; i < list_saved.size() ;i++){
            for(int j = 0;j <list_add.size() ;j++){
                if(list_add.get(j).getId().equals(list_saved.get(i).getId())){
                    list_boolean.set(j,true);
                }
            }
        }
    }

    /*************  重置数据库数据     ****************/
    public  void resetDbData(){
        dbInterface.deleteNodesEntityByTypeId(loadType);
        for(int i=0;i<list_default.size();i++){
            dbInterface.batchInsertOrUpdateNodesEntity(list_default.get(i));
        }

            for(int i=0;i<list_boolean.size();i++){
                    // 这是添加 选中的
                    if(list_boolean.get(i)==true){
                     dbInterface.batchInsertOrUpdateNodesEntity(list_add.get(i));
                    }
            }
    }
    /***************    判断是否有变化 目前只要有item点击操作就认为改变了内容 = = ********************/
    public boolean getIsChanged(){
        return  isChanged;
    }

    public void setIsChanged(boolean isChanged){
        this.isChanged=isChanged;
    }

    }
