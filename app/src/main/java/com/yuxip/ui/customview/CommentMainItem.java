package com.yuxip.ui.customview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;

import org.json.JSONException;
import org.json.JSONObject;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/10/12.
 * 楼层每层的主要内容 不包含 楼中楼 跳转评论界面会复用这个
 */
public class CommentMainItem extends RelativeLayout implements View.OnClickListener{

    private Context context;
    private PopComment popComment;        //弹出的小框
    private TextView tv_floor_content;   //当前条目的主要内容
    private TextView tv_floor_name_user;
    private TextView tv_floor_num;           //楼层
    private TextView tv_floor_time;         //时间
    private CustomHeadImage iv_floor_userCover;   //头像
    private ImageView iv_floor_zan;     //点赞的图片
    private ImageView iv_floor_louzhu_icon;   //如果是楼主则显示
    private TextView tv_floor_zan_count;       //点赞的数目
    private ImageView  iv_pop_start;            //点击弹出小框
    private View view_line;                     //条目下面的细线

    private TopicDetailsJsonBean.CommentEntity commentEntity;

    private int type;
    private String praiseUrl;
    private String collectUrl;
    private String commentId;
    private boolean hasFavored; //是否已经赞过
    private boolean hasCollected;//是否已经收藏
    private String favorCount;

    public enum CommentMainItemEnum{
        POP_COMMENT,IMAGEVIEW_START_POP,VIEW_LINE,TEXTVIEW_CONTENT
    }

    public CommentMainItem(Context context) {
        super(context);
        this.context = context;
        initRes();

    }

    public CommentMainItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context =context;
        initRes();
    }

    public CommentMainItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }

    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.commentitemview_head,this,true);
        popComment = (PopComment) findViewById(R.id.customview_pop_comment);
        tv_floor_content = (TextView) findViewById(R.id.tv_content_floor_comment);
        tv_floor_name_user = (TextView) findViewById(R.id.tv_name_floor_comment);
        tv_floor_num = (TextView) findViewById(R.id.tv_num_floor_comment);
        tv_floor_time = (TextView) findViewById(R.id.tv_time_floor_comment);
        tv_floor_zan_count = (TextView) findViewById(R.id.tv_zan_count_floor_comment);
        iv_floor_userCover = (CustomHeadImage) findViewById(R.id.iv_usericon_floor_comment);
        iv_floor_zan = (ImageView) findViewById(R.id.iv_zan_cover_floor_commment);
        iv_floor_louzhu_icon = (ImageView) findViewById(R.id.iv_louzhu_icon_floor_comment);
        iv_pop_start = (ImageView) findViewById(R.id.iv_pop_comment);
        view_line = findViewById(R.id.view_line_comment_item_main);
        iv_floor_userCover.setVipSize(12f);
        iv_floor_zan.setSelected(false);
        popComment.setCollectSelect(false);
        iv_floor_userCover.setOnClickListener(this);
        iv_floor_zan.setOnClickListener(this);
        popComment.getCommentView().setOnClickListener(this);
        popComment.getReportView().setOnClickListener(this);
        popComment.getCollectView().setOnClickListener(this);

    }


    public void setData(TopicDetailsJsonBean.CommentEntity commentEntity){
        this.commentEntity = commentEntity;
        commentId = commentEntity.getCommentID();
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            type = 0;
            collectUrl = ConstantValues.CollectStoryResource;
            praiseUrl = ConstantValues.PraiseStoryResource;
        }else{
            type =1;
            collectUrl = ConstantValues.CollectSquareResource;
            praiseUrl = ConstantValues.PraiseSquareResource;
        }
        inflateData();
    }


    /**
     * 除了图片的整体重绘
     */
    public void notifyData(){
        favorCount = commentEntity.getFavorCount();
        iv_floor_louzhu_icon.setVisibility(View.GONE);
        tv_floor_content.setText(commentEntity.getCommentContent());
        tv_floor_name_user.setText(commentEntity.getFromUser().getNickName());
        tv_floor_num.setText(commentEntity.getFloorCount()+"楼");
        tv_floor_time.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(commentEntity.getCommentTime())));
        tv_floor_zan_count.setText(commentEntity.getFavorCount());
        if(!TextUtils.isEmpty(commentEntity.getFromUser().getId())&& SquareCommentManager.getInstance().getLzId().equals(commentEntity.getFromUser().getId())){
            iv_floor_louzhu_icon.setVisibility(View.VISIBLE);
        }
        if(!TextUtils.isEmpty(commentEntity.getIsFavor())&&commentEntity.getIsFavor().equals("1")){
            hasFavored = true;
            iv_floor_zan.setSelected(true);
        }else{
            hasFavored = false;
            iv_floor_zan.setSelected(false);
        }

        if(!TextUtils.isEmpty(commentEntity.getIsCollection())&&commentEntity.getIsCollection().equals("1")){
            popComment.setCollectSelect(true);
            hasCollected = true;
        }else{
            popComment.setCollectSelect(false);
            hasCollected = false;
        }

    }

    /**
     * 只重绘 点赞 收藏
     */
    public void notifyBaseChanged(){
        favorCount = commentEntity.getFavorCount();
        tv_floor_zan_count.setText(commentEntity.getFavorCount());
        if(!TextUtils.isEmpty(commentEntity.getIsFavor())&&commentEntity.getIsFavor().equals("1")){
            hasFavored = true;
            iv_floor_zan.setSelected(true);
        }else{
            hasFavored = false;
            iv_floor_zan.setSelected(false);
        }

        if(!TextUtils.isEmpty(commentEntity.getIsCollection())&&commentEntity.getIsCollection().equals("1")){
            popComment.setCollectSelect(true);
            hasCollected = true;
        }else{
            popComment.setCollectSelect(false);
            hasCollected = false;
        }
    }

    private void inflateData(){
                favorCount = commentEntity.getFavorCount();
                iv_floor_louzhu_icon.setVisibility(View.GONE);
                tv_floor_content.setText(commentEntity.getCommentContent());
                tv_floor_name_user.setText(commentEntity.getFromUser().getNickName());
                tv_floor_num.setText("第"+commentEntity.getFloorCount()+"楼");
                tv_floor_time.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(commentEntity.getCommentTime())));
                tv_floor_zan_count.setText(commentEntity.getFavorCount());
                iv_floor_userCover.configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
                iv_floor_userCover.loadImage(commentEntity.getFromUser().getPortrait());
                if(!TextUtils.isEmpty(commentEntity.getFromUser().getId())&& SquareCommentManager.getInstance().getLzId().equals(commentEntity.getFromUser().getId())){
                    iv_floor_louzhu_icon.setVisibility(View.VISIBLE);
                }
                if(!TextUtils.isEmpty(commentEntity.getIsFavor())&&commentEntity.getIsFavor().equals("1")){
                    hasFavored = true;
                    iv_floor_zan.setSelected(true);
                }else{
                    hasFavored = false;
                    iv_floor_zan.setSelected(false);
                }

              if(!TextUtils.isEmpty(commentEntity.getIsCollection())&&commentEntity.getIsCollection().equals("1")){
                  popComment.setCollectSelect(true);
                  hasCollected = true;
              }else{
                  popComment.setCollectSelect(false);
                  hasCollected = false;
              }

    }


    public View getCommentMainItemChildView(CommentMainItemEnum commentMainItemEnum){
        switch (commentMainItemEnum){
            case POP_COMMENT:
                return this.popComment;
            case IMAGEVIEW_START_POP:
                return  this.iv_pop_start;
            case VIEW_LINE:
                return  this.view_line;
            case TEXTVIEW_CONTENT:
                return  this.tv_floor_content;
        }

        return null;
    }


    @Override
    public void onClick(View v) {
        if(commentEntity==null)
            return;
        switch (v.getId()){
            case R.id.iv_usericon_floor_comment:
                if(!TextUtils.isEmpty(commentEntity.getFromUser().getId()))
                IMUIHelper.openUserHomePageActivity(context,commentEntity.getFromUser().getId());
                break;
            case R.id.iv_zan_cover_floor_commment:
                PraiseSquareResource();
                break;
            case R.id.tv_report_popcomment:
                IMUIHelper.openReportCommentActivty(context,commentId, IntentConstant.FLOOR_REPORTTYPE_COMMENT);
                break;
            case R.id.rel_collect_popcomment:
                Toast.makeText(context,context.getResources().getString(R.string.square_notopen),Toast.LENGTH_LONG).show();
//                CollectSquareResource();
                break;
            case R.id.rel_comment_popcomment:
                IMUIHelper.openResponseFloorActivity(context,commentEntity.getFloorCount(),null,commentEntity.getFromUser().getId(),commentEntity.getCommentID(),null);
                break;
        }
    }

    private void postEvent(){
        SquareCommentEvent squareCommentEvent = new SquareCommentEvent();
        squareCommentEvent.eventType = SquareCommentEvent.EventType.TYPE_RESET_COMMENT_CONTENT;
        squareCommentEvent.commentId = commentEntity.getCommentID();
        squareCommentEvent.commentEntity = commentEntity;
        EventBus.getDefault().post(squareCommentEvent);
    }


    public TopicDetailsJsonBean.CommentEntity getCommentEntity(){
        if(this.commentEntity!=null){
            return  this.commentEntity;
        }else{
            return  null;
        }
    }


    //点赞广场资源
    private void PraiseSquareResource(){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",commentId);
        params.addParams("resourceType", "1");
        if(hasFavored){
            params.addParams("operation","0");
        }else{
            params.addParams("operation","1");
        }
        OkHttpClientManager.postAsy(praiseUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceovedFavorData(response);
                    }
                });
    }

    private void onReceovedFavorData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("100")){
                if(hasFavored){
                    iv_floor_zan.setSelected(false);
                    tv_floor_zan_count.setText((Integer.valueOf(favorCount) - 1) + "");
                    commentEntity.setFavorCount((Integer.valueOf(favorCount) - 1) + "");
                    commentEntity.setIsFavor("0");
                    hasFavored = false;
                }else{
                    iv_floor_zan.setSelected(true);
                    tv_floor_zan_count.setText((Integer.valueOf(favorCount) + 1) + "");
                    commentEntity.setFavorCount((Integer.valueOf(favorCount) + 1) + "");
                    commentEntity.setIsFavor("1");
                    hasFavored = true;
                }
                favorCount = commentEntity.getFavorCount();
                postEvent();
            }else{
                Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
        }
    }



    //收藏广场资源
    private void CollectSquareResource (){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",commentId);
        params.addParams("resourceType", "1");
        if(hasCollected){
            params.addParams("operation","0");
        }else{
            params.addParams("operation","1");
        }
        OkHttpClientManager.postAsy(collectUrl , params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceovedCollectData(response);
                    }
                });
    }

    private void onReceovedCollectData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("100")){
                if(hasCollected){
                    Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
                    popComment.setCollectSelect(false);
                    commentEntity.setIsCollection("0");
                    hasCollected = false;
                }else{
                    Toast.makeText(context, context.getResources().getString(R.string.square_collect_success), Toast.LENGTH_LONG).show();
                    popComment.setCollectSelect(true);
                    commentEntity.setIsCollection("1");
                    hasCollected =true;
                }
                postEvent();
            }else{
                Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
        }
    }



}
