package com.yuxip.ui.customview;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.R;
import com.yuxip.config.SysConstant;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.DensityUtil;
import com.yuxip.utils.ImageLoaderUtil;

/**
 * Created by Ly on 2015/9/9.
 * <p/>
 * 用于展示头像及部分可能装饰
 */
public class CustomHeadImage extends RelativeLayout {
    private CircularImage img_head;
    private ImageView img_vip;
    private Context context;
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;
    private boolean is12 = false;

    public CustomHeadImage(Context context) {
        super(context);
        this.context = context;
        initView();
        initRes();
    }

    public CustomHeadImage(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
        initRes();
    }

    public CustomHeadImage(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initView();
        initRes();
    }


    private void initView() {
        LayoutInflater.from(context).inflate(R.layout.customview_circleimage_layout, this, true);
        img_head = (CircularImage) findViewById(R.id.iv_head_img);
        img_vip = (ImageView) findViewById(R.id.iv_signed_user);
    }

    private void initRes() {
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
    }

    /**
     * 判断是否是vip     接口需求 showVip暂时无用
     *
     * @param url     图片地址
     * @param showVip 是否显示VIP用户
     */
    public void loadImage(String url, String showVip) {
        if (isSigner(url)) {
            img_vip.setVisibility(View.VISIBLE);
            if (is12) {
                if(url.contains("cosimage")) {
                    url = url.substring(0, url.lastIndexOf("?")) + SysConstant.PICTURE_24X24;
                }
            }
        } else {
            img_vip.setVisibility(View.INVISIBLE);
        }
        imageLoader.displayImage(url, img_head, loaderOptions);
    }

    /**
     * 加载头像图片
     *
     * @param url 图片地址
     */
    public void loadImage(String url) {
        if (isSigner(url)) {
            if (is12) {
                if(url.contains("cosimage")) {
                    url = url.substring(0, url.lastIndexOf("?")) + SysConstant.PICTURE_24X24;
                }
            }
            img_vip.setVisibility(View.VISIBLE);
        } else {
            img_vip.setVisibility(View.INVISIBLE);
        }
        imageLoader.displayImage(url, img_head, loaderOptions);
    }

    public void setVipSize(float dpSize) {
        int value = DensityUtil.dip2px(context, dpSize);
        ViewGroup.LayoutParams params = img_vip.getLayoutParams();
        params.height = value;
        params.width = value;
        img_vip.setLayoutParams(params);
        if (dpSize == 12f) {
            is12 = true;
        } else {
            is12 = false;
        }
    }


    public void setHeadMargin(float dpSize) {
        int value = DensityUtil.dip2px(context, dpSize);
        RelativeLayout.LayoutParams params = (LayoutParams) img_head.getLayoutParams();
        params.setMargins(value, value, value, value);
        img_head.setLayoutParams(params);
    }

    /**
     *
     *  @param type
     *  KEY_PERSON_PORTRAIT = 1;    //个人的头像
     *  KEY_GROUP_PORTRAIT = 2;     //群的头像
     *  KEY_DEFAULT_PIC = 3;        //默认的占位图
     */
    public void configImageOptions(int type) {
//        loaderOptions = new DisplayImageOptions.Builder().cacheInMemory(cacheInMemory)
//                .cacheOnDisk(true)
//                .showImageOnLoading(resId)
//                .showImageOnFail(resId)
//                .showImageForEmptyUri(resId)
//                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
//                .bitmapConfig(Bitmap.Config.RGB_565)
//                .resetViewBeforeLoading(true)               //设置图片在下载前是否重置，复位
//                .build();
        loaderOptions = ImageLoaderUtil.getOptions(DrawableCache.getInstance(context).getDrawable(type));
    }

    public void configImageOptions(DisplayImageOptions loaderOptions) {
        if (loaderOptions != null)
            this.loaderOptions = loaderOptions;
    }


    public static boolean isSigner(String url) {
        String type_isSigned;
        if (TextUtils.isEmpty(url)) {
            return false;
        }
        if (url.contains("?")) {
            String type_temp = url.substring(url.lastIndexOf("?") + 1);
            String str[] = type_temp.split("=");
            type_isSigned = str[1];
            if (str[0].equals("issigned") && (type_isSigned.equals("1") || type_isSigned.equals("3"))) {
                return true;
            }
        }
        return false;
    }

}
