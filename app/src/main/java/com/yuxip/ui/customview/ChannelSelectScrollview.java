package com.yuxip.ui.customview;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.R;
import com.yuxip.utils.DensityUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/8/20.
 * 主要对应主界面的可滑动的scrollview及相应的点击回调
 */
public class ChannelSelectScrollview extends RelativeLayout {
    private RelativeLayout relativeLayout;                  //主体背景层
    private HorizontalScrollView horizontalScrollView;      //主体scrollview
    private LinearLayout linearContainer;                   //多个view的父容器
    private ImageView imgMore;                              //更多的按钮
    private TextView  tvTitle;                              //点击按钮隐藏的标题
    private View viewLine;                                  //点击图片左边的轮廓线条
    private Context mContext;
    private List<View> list_view=new ArrayList<>();         //存放每个子项的根视图
    private List<TextView> list_tv=new ArrayList<>();       //存放每个子项的textview
    private List<ImageView> list_img=new ArrayList<>();     //存放每个子项的imageview
    private int color_select;                               //选中text颜色
    private int color_unselect;                             //一般text颜色
    private int color_hightlight;                           //高亮text颜色
    private float textSize;                                 //字体的大小
    private int windowWidth;                                //屏幕宽度
    private int viewWidth;                                  //每个子view的宽度
    private int selectPosition;                             //选中的子项,
    private float density;                                  //屏幕的密度

    private List<NodesEntity> list_nodes=new ArrayList<>();    //nodes节点集合
    public enum  ViewType{TYPE_SCROLLVIEW,TYPE_LINEARLAYOUT,TYPE_IMGMORE,TYPE_TEXTTILE,TYPE_VIEWLINE}     //主控件的枚举，用以返回控件对象

    private ChannelSelectCallBack channelSelectCallBack;

    public ChannelSelectScrollview(Context context) {
       super(context);
        this.mContext=context;
        initView();
        initRes();
    }

    public ChannelSelectScrollview(Context context, AttributeSet attrs) {
        super(context,attrs);
        this.mContext=context;
        initView();
        initRes();
    }

    public ChannelSelectScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext=context;
        initView();
        initRes();
    }


    private void initView(){
        LayoutInflater.from(mContext).inflate(R.layout.customview_channelscroll,this,true);
        relativeLayout= (RelativeLayout) findViewById(R.id.rel_channelselect);
        horizontalScrollView= (HorizontalScrollView) findViewById(R.id.horscroll_channelscroll);
        linearContainer= (LinearLayout) findViewById(R.id.linear_channelselect);
        imgMore= (ImageView) findViewById(R.id.imgmore_channelselect);
        tvTitle= (TextView) findViewById(R.id.tv_channelselect);
        viewLine=findViewById(R.id.view_channelselect);
    }

    private void initRes(){
        color_hightlight= Color.parseColor("#ff2424");
        color_select=mContext.getResources().getColor(R.color.blue_5893d8);
        color_unselect=mContext.getResources().getColor(R.color.grey_838383);
        windowWidth=((Activity)mContext).getWindowManager().getDefaultDisplay().getWidth();
        density=mContext.getResources().getDisplayMetrics().density;
        textSize=15f;
    }

    /**
     *
     * @param list_nodes  节点集合 其实传 title集合 以及 高亮集合 复用性或许更好
     * @param channelSelectCallBack  基本回调
     */
    public void setDataList( List<NodesEntity> list_nodes, final ChannelSelectCallBack channelSelectCallBack){
        if(list_nodes!=null)
        this.list_nodes=list_nodes;
        list_view.clear();
        list_img.clear();
        list_tv.clear();
        linearContainer.removeAllViews();
        if(channelSelectCallBack!=null)
            this.channelSelectCallBack=channelSelectCallBack;
            for(int i=0;i<this.list_nodes.size();i++){
            View view=LayoutInflater.from(mContext).inflate(R.layout.item_channel_select,null);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                int value= DensityUtil.dip2px(mContext,20);
                if(i==0){
                params.setMargins(value,0,value/2,0);
            }else if(i==list_nodes.size()-1){
                params.setMargins(value/2,0,value,0);
            }else{
                params.setMargins(value/2,0,value/2,0);
            }
                view.setLayoutParams(params);

            TextView tv= (TextView) view.findViewById(R.id.tv_item_channel_select);
            ImageView iv= (ImageView) view.findViewById(R.id.iv_item_channel_select);
            tv.setTextSize(textSize);
            tv.setText(list_nodes.get(i).getName());
            view.setTag(i);
            if(i==0){
                tv.setTextColor(color_select);
            }else{
                tv.setTextColor(color_unselect);
            }
            view.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag();
                    scroll(position);
                }
            });

                if(this.list_nodes.get(i).getIshighlight().equals("1")){
                    iv.setVisibility(View.VISIBLE);
                   tv.setTextColor(color_hightlight);
                }
            linearContainer.addView(view);
            list_view.add(view);
            list_tv.add(tv);
            list_img.add(iv);
        }
        resetItemWitdh();

    }

    public void scroll(int position){
        int left=list_view.get(position).getLeft();
        int right=list_view.get(position).getRight();
        /************       选中项剧中       **************/
        horizontalScrollView.smoothScrollTo(left - windowWidth / 2 + ((right-left)/2), 0);
        setTextColor(position);
        channelSelectCallBack.setViewpager(position);

    }

    public void setTextsColor(int color_select,int color_unselect){
        this.color_select=color_select;
        this.color_unselect=color_unselect;
    }

    /**
     *
     * @param color_select  选中时颜色
     * @param color_unselect    常规颜色
     * @param color_hightlight   高亮颜色
     */
    public void setTextsColor(int color_select,int color_unselect,int color_hightlight){
        this.color_select=color_select;
        this.color_unselect=color_unselect;
        this.color_hightlight=color_hightlight;
    }

    /**
     *
     * @param textSize 字体大小
     */
    public void setTextsSize(float textSize){
        this.textSize=textSize;
        if(list_tv!=null&&list_tv.size()>0){
            for(int i=0;i<list_tv.size();i++){
                list_tv.get(i).setTextSize(textSize);
            }
        }
    }

    public void setMainBackgroundColor(int color){
        relativeLayout.setBackgroundColor(color);
    }

    public void setMainBackgroundRes(int res){
        relativeLayout.setBackgroundResource(res);
    }

    public void setMainBackgroundDrawable(Drawable drawable){
        relativeLayout.setBackgroundDrawable(drawable);
    }

    /**************  区分高亮显示和普通的显示   ********************/
    private void setTextColor(int position){
        for(int i=0;i<list_tv.size();i++){
                if(list_nodes.get(i).getIshighlight().equals("1")){
                    list_tv.get(i).setTextColor(color_hightlight);
                }else {
                    list_tv.get(i).setTextColor(color_unselect);
                }

        }
        list_tv.get(position).setTextColor(color_select);
    }


    /**
     *
     * @param viewType  枚举类型
     * @return   主控件对象
     */
    public View getView(ViewType viewType){
        switch (viewType){
            case TYPE_SCROLLVIEW:
                return  horizontalScrollView;
            case TYPE_LINEARLAYOUT:
                return  linearContainer;
            case TYPE_IMGMORE:
                return  imgMore;
            case TYPE_TEXTTILE:
                return  tvTitle;
            case TYPE_VIEWLINE:
                return viewLine;
        }
        return null;
    }

    /**
     *  判断条目是否大于等于 6条 不然排满
     */
    private void resetItemWitdh(){
        if(list_view.size()<6){
            for(int i=0;i<list_view.size();i++){
                list_view.get(i).setMinimumWidth(windowWidth / list_view.size());
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.setMargins(0,0,0,0);
                list_view.get(i).setLayoutParams(params);
            }
        }
    }


    public void setTitleContent(String title){
        tvTitle.setText(title);
    }

    public void setTitleBgColor(int color){
        tvTitle.setBackgroundColor(color);
    }

    /**
     *  一般回调
     */
    public interface  ChannelSelectCallBack{
        void setViewpager(int position);
    }

}
