package com.yuxip.ui.customview;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ly on 2015/10/10.
 * 楼层界面下层输入  包含 一个输入框 以及 收藏 点赞数  评论  是输入 和后者的交替
 * 其中点击评论 会展现输入框  不可逆
 *
 */
public class CommentInputView extends RelativeLayout  implements View.OnClickListener{

    private Context context;

    private RelativeLayout rel_default; //默认初始进入显示的
    private RelativeLayout rel_edit;    //编辑信息发送的
    private RelativeLayout rel_collect;
    private RelativeLayout rel_zan;
    private RelativeLayout rel_comment;
    private ImageView iv_collect;
    private ImageView iv_zan;
    private TextView tv_collect_num;
    private TextView tv_zan_num;
    private EditText edit_input;
    private TextView tv_send;

    private String favorCount = "0";
    private String collectCount = "0";
    private String commentCount = "0";
    private String topicId;
    private String storyId;
    private int type;
    private String collectUrl;
    private String praiseUrl;


    private boolean hasCollected;
    private boolean hasFavored;

    public enum  InputViewEnum{
        EDIT_TEXT_CONTENT,TEXTVIEW_SEND

    };


    public CommentInputView(Context context) {
        super(context);
        this.context =context;
        initRes();
    }

    public CommentInputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context =context;
        initRes();
    }

    public CommentInputView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context =context;
        initRes();
    }

    /**
     *     这进行初始数据的设置
     */
    public  void setData(String topicId,String favorCount,String collectCount,String commentCount,String hasFavored,String hasCollected){
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            type = 0 ;
            collectUrl = ConstantValues.CollectStoryResource;
            praiseUrl = ConstantValues.PraiseStoryResource;
        }else{
            type = 1;
            collectUrl = ConstantValues.CollectSquareResource;
            praiseUrl = ConstantValues.PraiseSquareResource;
        }
        this.topicId = topicId;
        this.favorCount =favorCount;
        this.collectCount = collectCount;
        this.commentCount = commentCount;
        if(!TextUtils.isEmpty(hasCollected)&&hasCollected.equals("1")){
            this.hasCollected = true;
        }else{
            this.hasCollected = false;
        }

        if(!TextUtils.isEmpty(hasFavored)&&hasFavored.equals("1")){
            this.hasFavored = true;
        }else{
            this.hasFavored = false;
        }
        inflateData();

    }



    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.customview_comment_input,this,true);
        rel_default = (RelativeLayout) findViewById(R.id.rel_top_comment_input);
        rel_edit = (RelativeLayout) findViewById(R.id.rel_behind_comment_input);
        rel_collect = (RelativeLayout) findViewById(R.id.rel_collect_comment_input);
        rel_zan = (RelativeLayout) findViewById(R.id.rel_zan_comment_input);
        rel_comment = (RelativeLayout) findViewById(R.id.rel_comment_comment_input);
        iv_collect  = (ImageView) findViewById(R.id.iv_collect_comment_input);
        iv_zan = (ImageView) findViewById(R.id.iv_zan_comment_input);
        tv_zan_num = (TextView) findViewById(R.id.tv_zan_num_comment_input);
        tv_collect_num  = (TextView) findViewById(R.id.tv_collect_num_comment_input);
        edit_input = (EditText) findViewById(R.id.edit_comment_input);
        tv_send = (TextView) findViewById(R.id.tv_send_comment_input);
        rel_comment.setOnClickListener(this);
        rel_collect.setOnClickListener(this);
        rel_zan.setOnClickListener(this);
        edit_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edit_input.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    //这里禁用换行
                    return true;
                }
                return false;
            }
        });
    }

    private void inflateData(){
        tv_zan_num.setText(TextUtils.isEmpty(favorCount)?"0":favorCount);
        tv_collect_num.setText(TextUtils.isEmpty(collectCount)?"0":collectCount);
        if(hasCollected){
            rel_collect.setSelected(true);
        }else{
            rel_collect.setSelected(false);
        }

        if(hasFavored){
            rel_zan.setSelected(true);
        }else{
            rel_zan.setSelected(false);
        }
    }

    @Override
    public void onClick(View v) {
        if(topicId==null||collectUrl==null||praiseUrl==null)
            return;
        switch (v.getId()){
            case R.id.rel_comment_comment_input: //评论
                rel_edit.setVisibility(View.VISIBLE);
                rel_default.setVisibility(View.GONE);
                break;
            case R.id.rel_collect_comment_input:   //收藏
                CollectSquareResource();

                break;
            case R.id.rel_zan_comment_input:   //点赞
                PraiseSquareResource();
                break;
        }
    }

    public View getInputChildView(InputViewEnum inputViewEnum){
        switch (inputViewEnum){
            case EDIT_TEXT_CONTENT:     //返回输入框 editText
                return  this.edit_input;
            case TEXTVIEW_SEND:         //返回发送的按钮  textView
                return  this.tv_send;
        }
        return  null;
    }


    /**
     * 显示输入框
     */
    public void showInput(){
        rel_edit.setVisibility(View.VISIBLE);
        rel_default.setVisibility(View.GONE);
    }

    //点赞广场资源
    private void PraiseSquareResource(){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",topicId);
        params.addParams("resourceType", "0");
        if(hasFavored){
            params.addParams("operation","0");
        }else{
            params.addParams("operation","1");
        }
        OkHttpClientManager.postAsy(praiseUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceovedFavorData(response);
                    }
                });
    }

    private void onReceovedFavorData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("100")){
                if(hasFavored){
                    tv_zan_num.setText(Integer.valueOf(favorCount) -1+ "");
                    favorCount = Integer.valueOf(favorCount)-1+"";
                    rel_zan.setSelected(false);
                    hasFavored =false;
                }else{
                    tv_zan_num.setText(Integer.valueOf(favorCount) + 1 + "");
                    favorCount = Integer.valueOf(favorCount)+1+"";
                    rel_zan.setSelected(true);
                    hasFavored =true;
                }
            }else{
                Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.square_favor_failed), Toast.LENGTH_LONG).show();
        }
    }


    //收藏广场资源
    private void CollectSquareResource (){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",topicId);
        params.addParams("resourceType", "0");
        if(hasCollected){
            params.addParams("operation","0");
        }else{
            params.addParams("operation","1");
        }
        OkHttpClientManager.postAsy(collectUrl , params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        onReceovedCollectData(response);
                    }
                });
    }

    private void onReceovedCollectData(String response){
        try {
            JSONObject jsonObject = new JSONObject(response);
            if(jsonObject.getString("result").equals("100")){
//
                if(hasCollected){
                    tv_collect_num.setText(Integer.valueOf(collectCount)-1+"");
                    collectCount = Integer.valueOf(collectCount)-1+"";
                    rel_collect.setSelected(false);
                    hasCollected =false;

                }else{
                    tv_collect_num.setText(Integer.valueOf(collectCount)+1+"");
                    collectCount = Integer.valueOf(collectCount)+1+"";
                    rel_collect.setSelected(true);
                    hasCollected =true;
                }
            }else{
                Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(context, context.getResources().getString(R.string.square_collect_failed), Toast.LENGTH_LONG).show();
        }
    }



}
