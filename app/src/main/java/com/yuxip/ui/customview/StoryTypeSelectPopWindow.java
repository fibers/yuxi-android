package com.yuxip.ui.customview;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/10/29.
 */
public class StoryTypeSelectPopWindow {
    private final int  selectLimit = 2;
    private PopupWindow popupWindow;
    private View view;
    private Context context;
//    private TextView tvMainSelect1;
//    private TextView tvMainSelect2;
    private TextView tvSure;
    private NoScrollGridView gridViewDefault;   //默认的大标签
    private NoScrollGridView gridViewChild;     //自标签
    private int selectPosition;         //当前选中的父项

    private List<StoryClass.ListEntity> listAllTypes = new ArrayList<>();
    private List<StoryClass.ListEntity> listDefault  = new ArrayList<>();
    private List<StoryClass.ListEntity> listChild = new ArrayList<>();
//    private List<String> listDefault = new ArrayList<>();       //大标签的集合
//    private List<String> listChild = new ArrayList<>();         //每个标签对应的子项
    private List<Boolean> listChildSelect = new ArrayList<>();   //第一个大标签对应的所有子内容的选中状态
    private MyAdapter childAdapter;         //子适配器
    private MyDefaultAdapter defaultAdapter;    //父适配器
    public  StoryTypeSelectPopWindow(Context context){
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.pop_story_type_select,null);
        initPopWindow();
        initRes();
    }

    private void initPopWindow() {
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setAnimationStyle(R.style.AnimationImageSavePopupWindow);
        popupWindow.setTouchable(true);
        popupWindow.setFocusable(true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
    }

    private void initRes(){
        tvSure = (TextView) view.findViewById(R.id.tv_sure_pop_type_select);
        gridViewDefault = (NoScrollGridView) view.findViewById(R.id.gv_pop_type_select_default);
        gridViewChild = (NoScrollGridView) view.findViewById(R.id.gv_pop_type_select);
    }

    private void inflateData(){
        listDefault.clear();
        listChild.clear();
        for(int i=0;i<listAllTypes.size();i++){
            if(i<2){
                listDefault.add(listAllTypes.get(i));
            }else{
                listChild.add(listAllTypes.get(i));
            }

        }
        childAdapter = new MyAdapter();
        defaultAdapter = new MyDefaultAdapter();
        gridViewChild.setAdapter(childAdapter);
        gridViewDefault.setAdapter(defaultAdapter);
        selectPosition = 0;
    }



    public void showPopWindow(View view,List<StoryClass.ListEntity> list){
        if(list==null||list.size()==0)
            return;
        this.listAllTypes = list;
        inflateData();
        initSelectList();
        if(popupWindow!=null&&!popupWindow.isShowing()){
//            popupWindow.showAtLocation(view, Gravity.TOP, 0, 0);
            popupWindow.showAsDropDown(view);
        }
    }

//    public void showPopWindow(View view,View dropView){
//        if(popupWindow!=null&&!popupWindow.isShowing()){
//            popupWindow.showAsDropDown(dropView);
//        }
//    }
    public  void dissmissPopWindow(){
        if(popupWindow!=null&&popupWindow.isShowing()){
            popupWindow.dismiss();
        }
    }


    /**
     *  初始化两个选中状态集合
     */
    private void initSelectList(){
        listChildSelect.clear();
        for(int i =0; i< listChild.size();i++){
            listChildSelect.add(false);
        }

    }


//    /**
//     *  切换主体大标签
//     * @param position
//     */
//    private void changeListData(int position){
//        switch (position){
//            case 0:
//                selectPosition = 0;
//                break;
//            case 1:
//                selectPosition = 1;
//                break;
//        }
//
//    }


    /**
     *  检测 每个子项选中的是否超出限制  这里 不得超过2个
     * @return
     */
    private boolean checkSelectLimits(){
        int  selectSize = 0;
            for(int i=0;i<listChildSelect.size();i++){
                if(listChildSelect.get(i) == true)
                    selectSize +=1;
            }
        if(selectSize<selectLimit){
            return  true;
        }else{
            return false;
        }
    }


    /**
     *  获取选中的所有内容 其中包括 一个 大标签 加 最多两个子标签
     * @return
     */
    public List<StoryClass.ListEntity> getSelectLabels(){
        List<StoryClass.ListEntity> list = new ArrayList<>();
        list.add(listDefault.get(selectPosition));
        for(int i=0;i<listChildSelect.size();i++){
            if(listChildSelect.get(i) == true)
                list.add(listChild.get(i));
        }
       return list;
    }


    public  View getSureView(){
        return  this.tvSure;
    }


    private class MyDefaultAdapter extends  BaseAdapter{

        @Override
        public int getCount() {
            return listDefault.size();
        }

        @Override
        public Object getItem(int position) {
            return listDefault.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;
            if(convertView==null){
                convertView = LayoutInflater.from(context).inflate(R.layout.gv_item_story_type_select,null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.tvItem.setSelected(false);
            if(position == selectPosition)
                viewHolder.tvItem.setSelected(true);
            viewHolder.tvItem.setText(listDefault.get(position).getName());
            viewHolder.tvItem.setTag(position);
            viewHolder.tvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (int) v.getTag();
                    if(selectPosition!=_position){
                        selectPosition = _position;
                        for(int i = 0 ; i<gridViewDefault.getChildCount(); i++){
                            gridViewDefault.getChildAt(i).findViewById(R.id.tv_gv_item_story_type_select).setSelected(false);
                        }
                        gridViewDefault.getChildAt(_position).findViewById(R.id.tv_gv_item_story_type_select).setSelected(true);
                    }
                }
            });
            return convertView;
        }
        class ViewHolder{
            TextView tvItem;
            ViewHolder(View view){
                tvItem = (TextView) view.findViewById(R.id.tv_gv_item_story_type_select);
            }
        }
    }





    private class MyAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return listChild.size();
        }

        @Override
        public Object getItem(int position) {
            return listChild.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView ==null){
                convertView  = LayoutInflater.from(context).inflate(R.layout.gv_item_story_type_select,null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }
            viewHolder.tvItem.setSelected(false);

                if(listChildSelect.get(position) == true){
                    viewHolder.tvItem.setSelected(true);
                }
            viewHolder.tvItem.setText(listChild.get(position).getName());
            viewHolder.tvItem.setTag(position);
            viewHolder.tvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int _position = (int) v.getTag();
                    if(v.isSelected()){
                        v.setSelected(false);
                        listChildSelect.set(_position,false);
                    }else{
                        if(!checkSelectLimits()){
                            Toast.makeText(context,"只能选两项",Toast.LENGTH_LONG).show();
                            return;
                        }
                        v.setSelected(true);
                        listChildSelect.set(_position, true);

                    }


                }
            });
            return convertView;
        }

        class ViewHolder{
            TextView tvItem;
            ViewHolder(View view){
                tvItem = (TextView) view.findViewById(R.id.tv_gv_item_story_type_select);
            }
        }
    }



}
