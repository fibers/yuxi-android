package com.yuxip.ui.customview;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/10/9.
 * 主楼第一项  其中包含一个可以滑动的图片 展示 gridview
 */
public class SquareCommentHeadView extends LinearLayout implements View.OnClickListener{


    private GridView gridView;
    private float density;
    private int width;
    private Context context;
    private int showNum;
    private GridAdapter adapter;
    private TopicDetailsJsonBean.TopicDetailEntity topicDetailEntity;
    private TextView tv_name;
    private TextView tv_time;
    private TextView tv_delete;
    private TextView tv_content;
    private CustomHeadImage iv_portait;
    private ImageView iv_louzhu_icon;
    private List<String> list_images = new ArrayList<>();

    private String loginId ;
    private String creatorId ;
    private boolean isCreator;
    private ArrayList<ImageMessage> listImg = new ArrayList<>();
    private String deleteUrl;

    public SquareCommentHeadView(Context context) {
        super(context);
        this.context = context;
        density = context.getResources().getDisplayMetrics().density;
        initRes();
    }

    public SquareCommentHeadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();
    }

    public SquareCommentHeadView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }


    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.layout_head_comment, this, true);
        gridView = (GridView) findViewById(R.id.gv_mainadapter);
        tv_name = (TextView) findViewById(R.id.tv_louzhu_name);
        tv_time = (TextView) findViewById(R.id.tv_louzhu_time);
        tv_content  = (TextView) findViewById(R.id.tv_louzhu_content);
        tv_delete = (TextView) findViewById(R.id.tv_louzhu_delete);
        iv_portait = (CustomHeadImage) findViewById(R.id.iv_louzhu_icon);
        iv_louzhu_icon = (ImageView) findViewById(R.id.iv_louzhu_icon_head_comment);
        iv_portait.setOnClickListener(this);
        tv_delete.setOnClickListener(this);
        iv_portait.setVipSize(12f);
        View view = LayoutInflater.from(context).inflate(R.layout.grid_item_comment_head,null);
        view.measure(MeasureSpec.UNSPECIFIED,MeasureSpec.UNSPECIFIED);
        width = view.getMeasuredWidth();
        tv_delete.setVisibility(View.GONE);
        iv_louzhu_icon.setVisibility(View.GONE);
    }

    public void setShowNumber(final int count){
        if(count ==0){
            gridView.setVisibility(View.GONE);
            return;
        }
        this.showNum = count;
        adapter = new GridAdapter();
        LayoutParams layoutParams = new LayoutParams((int) (width*(count)+density*(count-1)*15),width);
        gridView.setLayoutParams(layoutParams);
        gridView.setNumColumns(count);
        gridView.setAdapter(adapter);
        initImageMessage();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IMUIHelper.OpenPreviewReadImaActivity(context,listImg,position+"");
            }
        });
    }


    public  void setData(TopicDetailsJsonBean.TopicDetailEntity topicDetailEntity){
        if(SquareCommentManager.getInstance().getType().equals(IntentConstant.FLOOR_TYPE_STORY)){
            deleteUrl = ConstantValues.DeleteStoryResource;
            tv_delete.setVisibility(View.GONE);
        }else{
            deleteUrl = ConstantValues.DeleteSquareResource;
            tv_delete.setVisibility(View.VISIBLE);
        }
        this.topicDetailEntity = topicDetailEntity;
        if(topicDetailEntity == null){
            Toast.makeText(getContext(),"获取数据失败",Toast.LENGTH_LONG).show();
            return;
        }
        tv_delete.setVisibility(View.VISIBLE);
        iv_louzhu_icon.setVisibility(View.VISIBLE);
        iv_portait.configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
        iv_portait.loadImage(this.topicDetailEntity.getCreator().getPortrait());
        creatorId = topicDetailEntity.getCreator().getId();
        loginId = IMLoginManager.instance().getLoginId()+"";
        tv_name.setText(topicDetailEntity.getCreator().getNickName());
        tv_time.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(topicDetailEntity.getCreateTime())));
        tv_content.setText(topicDetailEntity.getContent());
        if(topicDetailEntity.getImages()!=null){
            this.list_images = topicDetailEntity.getImages();
        }
        if(list_images.size()==1){
            if(TextUtils.isEmpty(list_images.get(0))){
               setShowNumber(0);
            }else{
                setShowNumber(list_images.size());
            }
        }else{
            setShowNumber(list_images.size());
        }
        if(loginId.equals(creatorId)){
            isCreator= true;
            tv_delete.setText(context.getResources().getString(R.string.square_delete));
        }else{
            isCreator = false;
            tv_delete.setText(context.getResources().getString(R.string.square_report));
        }

    }

    public void resetDate(){
        tv_time.setText(DateUtil.getDateWithHoursAndSeconds(Long.valueOf(topicDetailEntity.getCreateTime())));
    }

    /**
     * 初始化 传递的list 及 位置 信息
     * 用以 浏览图片视图
     */
    private void initImageMessage(){
        listImg.clear();
        for(int i=0;i<list_images.size();i++){
            ImageMessage img=new ImageMessage();
            if(!TextUtils.isEmpty(list_images.get(i))){
                img.setUrl(list_images.get(i));
            }else{
                img.setUrl("");
            }
            listImg.add(img);
        }
    }

    @Override
    public void onClick(View v) {
        if(topicDetailEntity==null)
            return;
        switch (v.getId()){
            case R.id.tv_louzhu_delete:
                if(isCreator){
                    showDeleteAlertDialog();
                }else{
                    IMUIHelper.openReportCommentActivty(context,topicDetailEntity.getTopicID(), IntentConstant.FLOOR_REPORTTYPE_TOPIC);
                }
                break;
            case R.id.iv_louzhu_icon:
                if(!TextUtils.isEmpty(creatorId))
                IMUIHelper.openUserHomePageActivity(context,creatorId);
                break;
        }
    }


    private class GridAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return showNum;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder viewHolder;
            if(convertView == null){
                convertView = LayoutInflater.from(context).inflate(R.layout.grid_item_comment_head,null);
                viewHolder = new ViewHolder();
                viewHolder.iv= (ImageView) convertView.findViewById(R.id.iv_gv_item);
                convertView.setTag(viewHolder);
            }else{
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC);
            DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
            ImageLoaderUtil.getImageLoaderInstance().displayImage(list_images.get(position)+ SysConstant.PICTURE_58X58,viewHolder.iv, imageOptions);
            return  convertView;
        }

        class ViewHolder {
            ImageView iv;
        }
    }


    /**
     * 删除 操作
     */
    private void deleteTopicResource(){
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId()+"");
        params.addParams("token","110");
        params.addParams("resourceID",topicDetailEntity.getTopicID());
        params.addParams("resourceType","0");
        OkHttpClientManager.postAsy(deleteUrl, params.getParams(),
                new OkHttpClientManager.ResultCallback<String>() {
                    @Override
                    public void onError(Request request, Exception e) {
                        Toast.makeText(context,getResources().getString(R.string.square_loading_fail),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            if(object.getString("result").equals("100")){
                                Toast.makeText(context,getResources().getString(R.string.square_delete_success), Toast.LENGTH_LONG).show();
                                SquareCommentEvent squareCommentEvent = new SquareCommentEvent();
                                squareCommentEvent.eventType   = SquareCommentEvent.EventType.TYPE_DELETE_STORY_TOPIC;
                                EventBus.getDefault().post(squareCommentEvent);
                            }else{
                                Toast.makeText(context,getResources().getString(R.string.square_delete_failed),Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(context,getResources().getString(R.string.square_delete_failed),Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


    private void showDeleteAlertDialog(){
        AlertDialog.Builder builder  = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.square_prompt));
        builder.setMessage(context.getResources().getString(R.string.square_confim_delete_topic));
        builder.setPositiveButton(context.getResources().getString(R.string.square_sure), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteTopicResource();
            }
        });
        builder.setNegativeButton(context.getResources().getString(R.string.square_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

}
