package com.yuxip.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.yuxip.R;

/**
 * Created by Administrator on 2015/11/9.
 */
public class CustomDividerProgressbar extends RelativeLayout {
    private ProgressBar progressBar;
    private CustomDividerView customDividerView;
    private Context context;
    private int dividerCount;
    private int progress;
    public CustomDividerProgressbar(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public CustomDividerProgressbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();
    }

    public CustomDividerProgressbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }

    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.customview_divider_progressbar,this,true);
        progressBar = (ProgressBar) findViewById(R.id.pb_customview_progressbar);
        customDividerView = (CustomDividerView) findViewById(R.id.customview_divider_view);
    }

    public void reDrawDivider(){
        customDividerView.drawView();
    }


    public void  setDividerColor(int color){
        customDividerView.setColor(color);
    }

    public void setDividerWidth(int width){
        customDividerView.setLineWidth(width);
    }

    public  void setDividerCount( int  dividerCount){
        customDividerView.setDividerCount(dividerCount);;
    }

    public  void setCurProgress(int progress){
        progressBar.setProgress(progress);
    }


}
