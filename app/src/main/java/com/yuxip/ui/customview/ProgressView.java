package com.yuxip.ui.customview;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * 
 * @author HeTianpeng
 * 
 */
public class ProgressView extends FrameLayout implements OnClickListener {

	private TextView text;
	private ProgressBar pb;
	private TextOnClickListener tol;

	boolean isTextOnClick;

	public ProgressView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public ProgressView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		init(context);
	}

	public ProgressView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		init(context);
	}

	private void init(Context context) {
		// TODO Auto-generated method stub
		setBackgroundColor(Color.TRANSPARENT);
		LayoutParams lp = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, Gravity.CENTER);

		text = new TextView(context);
		text.setVisibility(View.GONE);
		text.setGravity(Gravity.CENTER);
		text.setLayoutParams(lp);

		addView(text);

		pb = new ProgressBar(context);
		lp = new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		pb.setLayoutParams(lp);
		addView(pb);

		text.setOnClickListener(this);
	}

	/**
	 * 
	 * @param textContent
	 *            显示的文字
	 * @param isTextOnClick
	 *            为true时， TextOnClickLitener可响应。
	 */
	public void showFailureText(String textContent, boolean isTextOnClick) {
		if (isVisibility()) {
			this.isTextOnClick = isTextOnClick;
			pb.setVisibility(View.GONE);
			text.setVisibility(View.VISIBLE);
			text.setText(textContent);
		}
	}
	
	public boolean isVisibility() {
		return View.VISIBLE == getVisibility() ? true : false;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if (isTextOnClick) {
			text.setVisibility(View.GONE);
			pb.setVisibility(View.VISIBLE);
			tol.textOnClick();
		}
	}

	public void setProgressBarVisibility(int v) {
		pb.setVisibility(v);
	}

	public void viewShow() {
		if (getVisibility() == View.GONE) {
			text.setVisibility(View.GONE);
			pb.setVisibility(View.VISIBLE);
			setVisibility(View.VISIBLE);
		}
	}

	public void viewGone() {
		if (getVisibility() == View.VISIBLE) {
			setVisibility(View.GONE);
		}
	}

	public void setTextOnClickListener(TextOnClickListener textOnClickListener) {
		if (null == textOnClickListener) {
			throw new NullPointerException("TextOnClickListener is null");
		}
		tol = textOnClickListener;
	}

	public interface TextOnClickListener {
		void textOnClick();
	}
}
