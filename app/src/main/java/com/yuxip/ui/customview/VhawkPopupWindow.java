package com.yuxip.ui.customview;


import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;

import com.yuxip.R;

/**
 * Created by Administrator on 2015/5/23.
 */
public class VhawkPopupWindow {


    private OnPopItemClickedListener listener;
    private PopupWindow window;

    public void initPopUpwindow(Context context, BaseAdapter adapter, int width) {

        View view = View.inflate(context, R.layout.popupwindow_vhawk, null);
        ListView listView = (ListView) view.findViewById(R.id.popupwindow_list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                listener.onPopItemClicked(position, view);
                window.dismiss();
            }
        });
        if (width == 0) {
            window = new PopupWindow(view, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        } else {
            window = new PopupWindow(view, width, LayoutParams.WRAP_CONTENT);
        }

        window.setOutsideTouchable(true);
        window.setTouchable(true);
        window.setFocusable(true);
        window.setBackgroundDrawable(new BitmapDrawable());
        window.setOnDismissListener(new OnDismissListener() {
            @Override
            public void onDismiss() {

            }
        });
    }

    public void showPop(View anchor) {
        window.showAsDropDown(anchor, 0, 0);
    }

    public void showPop(View anchor, int xOff, int yOff) {
        window.showAsDropDown(anchor, xOff, yOff);
    }

    public void dismissPop() {
        if (window.isShowing()) {
            window.dismiss();
        }
    }

    public void setOnPopItemClicked(OnPopItemClickedListener listener) {
        this.listener = listener;
    }

    public interface OnPopItemClickedListener {
        void onPopItemClicked(int position, View view);
    }

}
