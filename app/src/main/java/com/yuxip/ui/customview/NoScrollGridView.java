package com.yuxip.ui.customview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by Ly on 2015/8/13.
 * description:自定义GridView
 */
public class NoScrollGridView extends GridView {
        public NoScrollGridView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public NoScrollGridView(Context context) {
            super(context);
        }

        public NoScrollGridView(Context context, AttributeSet attrs, int defStyle) {
            super(context, attrs, defStyle);
        }

        @Override
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int expandSpec = MeasureSpec.makeMeasureSpec(
                    Integer.MAX_VALUE >> 2, MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);
        }
}
