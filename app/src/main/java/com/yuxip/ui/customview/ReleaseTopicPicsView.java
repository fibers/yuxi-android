package com.yuxip.ui.customview;

import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.DragEvent;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.R;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.thread.AsyncTaskBase;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.UpImgUtil;
import com.yuxip.utils.listener.HeadImgListener;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by ZQF on 2015/10/22.
 */
public class ReleaseTopicPicsView extends LinearLayout {

    private static Logger logger = Logger.getLogger(ReleaseTopicPicsView.class);
    private Context context;

    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;      // 拍照
    public static final int PHOTOZOOM = 2;       // 缩放
    public static final String IMAGE_UNSPECIFIED = "image/*";

    private HorizontalScrollView mHoScrollView;
    private LinearLayout mPicsLl;                                // 图片列表布局
    private ImageView mAddIv;                                    // 添加图片
    //    private Bitmap photo;                                        // 布局中图片数量
    private int picCount = 0;
    private float pWidth, pHeight;                                             // 屏幕宽高
    private Map<String, String> mUrlMap;                                       // url-map key:本地图片地址 value:图片网络地址,如果为空,上传失败
    private ArrayList<ImageMessage> listImg = new ArrayList<>();
    private TextView mCountTv;                                   // 底部"0/9"
    private boolean isPicsVisible = false;

    private GestureDetector mGestureDetector;
    private View mDrapView;                                                    // 拖动图片的影子
    private int curPosition;


    public ReleaseTopicPicsView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ReleaseTopicPicsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ReleaseTopicPicsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    private void init() {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        pWidth = wm.getDefaultDisplay().getWidth();
        pHeight = wm.getDefaultDisplay().getHeight();
        mUrlMap = new HashMap<>();

        LayoutInflater.from(context).inflate(R.layout.customview_topic_reslase_pics, this, true);
        mHoScrollView = (HorizontalScrollView) findViewById(R.id.hsv_topic_release);
        mCountTv = (TextView) findViewById(R.id.tv_pic_count);
        mAddIv = (ImageView) findViewById(R.id.iv_pic_add);
        mAddIv.setImageResource(R.drawable.topic_pic_add);
        mAddIv.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCountChangeListener != null)
                    mCountChangeListener.onClick();
            }
        });
        mPicsLl = (LinearLayout) findViewById(R.id.ll_pics);
        mGestureDetector = new GestureDetector(context, new DrapGestureListener());
        isPicsVisible = (getVisibility() == View.VISIBLE);
        findViewById(R.id.tv_pic_delete).setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void setVisibility(int visibility) {
        if (visibility == View.VISIBLE) {
            int type = TranslateAnimation.RELATIVE_TO_SELF;

            TranslateAnimation animation = new TranslateAnimation(type, 0, type, 0, type, 1, type, 0);
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isPicsVisible = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            startAnimation(animation);
        } else {
            int type = TranslateAnimation.RELATIVE_TO_SELF;
            TranslateAnimation animation = new TranslateAnimation(type, 0, type, 0, type,
                    0, type, 1);
            animation.setDuration(500);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    isPicsVisible = false;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            startAnimation(animation);
        }
        super.setVisibility(visibility);
    }

    /**
     * 隐藏上传图片布局
     */
    public void closePicsLayout() {
        if (getVisibility() == View.VISIBLE) {
            setVisibility(View.GONE);
        }
    }

    /**
     * 添加图片到布局
     */
    public void addImage(final Intent data) {
        final View layout = LayoutInflater.from(context).inflate(R.layout.topic_release_pic_item, null);
        ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        mPicsLl.addView(layout, mPicsLl.getChildCount() - 1);
        layout.setOnTouchListener(mOnTouchListener);
        layout.setOnDragListener(mOnDragListener);

        TextView delete = (TextView) layout.findViewById(R.id.tv_pic_delete);
        delete.setVisibility(View.VISIBLE);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getVisibility() != View.VISIBLE)
                    return;
                mPicsLl.removeView(layout);
                mUrlMap.remove(layout.getTag());
                if (picCount == 9) {
                    mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.VISIBLE);
                }
                picCount--;
                mCountTv.setText(picCount + " / 9");
                if (mCountChangeListener != null)
                    mCountChangeListener.onChange();
            }
        });
        picCount++;
        if (mCountChangeListener != null)
            mCountChangeListener.onChange();

        mCountTv.setText(picCount + " / 9");
        if (picCount >= 9) {
            mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.GONE);
        }

        Bitmap photo;
        Uri uri = data.getData();
        String filePath = null;
        ContentResolver resolver = context.getContentResolver();
        if (uri == null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                photo = (Bitmap) bundle.get("data"); //get bitmap
               filePath =  ImageUtil.saveBitmapImg(photo);
                if(!photo.isRecycled()){
                    try {
                        photo.recycle();
                        photo = null;
                    }catch (Exception e){
                        e.printStackTrace();
                    }finally {
                        System.gc();
                    }
                }
            } else {
                Toast.makeText(context, "没有图片", Toast.LENGTH_LONG).show();
                return;
            }
        } else {
            try {
                filePath = ImageUtil.getUriPath(uri,context.getContentResolver());
            } catch (Exception e) {
                Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                logger.e(e.toString());
                return;
            }
        }
//        photo = ImageUtil.getBigBitmapForDisplay(photo, context);
        /** 添加图片后滑动到加号位置 */
//        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mAddIv.getLayoutParams();
//        mHoScrollView.smoothScrollTo((layoutParams.width + layoutParams.leftMargin) * mPicsLl.getChildCount(), 0);
//        uploadPic(layout, filePath);
        new AsyncTaskLoading(layout).execute(filePath);
    }


    /**
     * 添加图片到布局
     */
    public void addImages(final String picPath) {
        if (picCount >= 9) {
            return;
        }
        final View layout = LayoutInflater.from(context).inflate(R.layout.topic_release_pic_item, null);
        ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
        mPicsLl.addView(layout, mPicsLl.getChildCount() - 1);
        layout.setOnTouchListener(mOnTouchListener);
        layout.setOnDragListener(mOnDragListener);

        TextView delete = (TextView) layout.findViewById(R.id.tv_pic_delete);
        delete.setVisibility(View.VISIBLE);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getVisibility() != View.VISIBLE)
                    return;
                mPicsLl.removeView(layout);
                mUrlMap.remove(layout.getTag());
                if (picCount == 9) {
                    mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.VISIBLE);
                }
                picCount--;
                mCountTv.setText(picCount + " / 9");
                if (mCountChangeListener != null)
                    mCountChangeListener.onChange();
            }
        });
        picCount++;
        if (mCountChangeListener != null)
            mCountChangeListener.onChange();

        mCountTv.setText(picCount + " / 9");
        if (picCount >= 9) {
            mPicsLl.getChildAt(mPicsLl.getChildCount() - 1).setVisibility(View.GONE);
        }
        new AsyncTaskLoading(layout).execute(picPath);
    }


    /**
     * 上传图片
     */
    private void uploadPic(final View layout,  final String filePath) {
        Bitmap bitmap = null;
        /** 照片的命名，目标文件夹下，以用户名加当前时间数字串为名称，即可确保每张照片名称不相同 */
        Date date = new Date();
        /** 获取当前时间并且进一步转化为字符串 */
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmSS", Locale.getDefault());
        String path = null;
        if (layout.getTag() == null) {
            path = IMLoginManager.instance().getLoginId() + "_" + format.format(date) + ".png";
            layout.setTag(path);
            bitmap = ImageUtil.getPathBitmap(context,filePath,false);   //不压缩
        } else { // tag不为空,重新上传
            path = layout.getTag().toString();
            String imgFilePath = Environment.getExternalStorageDirectory() + "/youxi/" + path;
            try {
                bitmap = BitmapFactory.decodeFile(imgFilePath);
            } catch (Exception e) {
                logger.e(e.toString());
            }
        }
        final String pic_path = path;
        /** 图片根据时间命名,所以可作为唯一标识作为tag携带,对应map里的key */
        UpImgUtil upImgUtil = new UpImgUtil();
        upImgUtil.setFilePath(pic_path, pic_path);
        upImgUtil.saveHeadImg(bitmap);
        if(bitmap.isRecycled()){
            try {
                bitmap.recycle();
                bitmap = null;
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                System.gc();
            }
        }
//        final Bitmap bitmap = photo;
        upImgUtil.upLoadPicture(new HeadImgListener() {
            @Override
            public void notifyImgUploadFinished(String url) {
                final View view = layout.findViewById(R.id.iv_pic_upload_failed);
                ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
                progressBar.setVisibility(View.GONE);
                /** 如果返回url为空,则上传失败 */
                if (url == null || "".equals(url)) {
                    mUrlMap.put(pic_path, "");
                    view.setVisibility(View.VISIBLE);
                    view.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            view.setVisibility(View.GONE);
                            ProgressBar progressBar = (ProgressBar) layout.findViewById(R.id.progress_bar);
                            progressBar.setVisibility(View.VISIBLE);
                            uploadPic(layout, filePath);
                        }
                    });
                } else {
                    if (view.getVisibility() == View.VISIBLE) {
                        view.setVisibility(View.GONE);
                    }
                    mUrlMap.put(pic_path, url);
                }
            }
        });
//        photo = zoomBitmap(photo, (int) pWidth, (int) (pWidth * photo.getHeight() / photo.getWidth()));

    }

    public void recycleAllImages(){
        for (int i = 0; i < mPicsLl.getChildCount() - 1; i++) {
            View view = mPicsLl.getChildAt(i);
            ImageView imageView = (ImageView) view.findViewById(R.id.iv_pic_add);
            Drawable drawable= imageView.getDrawable();
            if(drawable instanceof BitmapDrawable){
                Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                try {
                    bitmap.recycle();
                    bitmap = null;
                }catch ( Exception e){

                }
            }
        }
        System.gc();
    }

    /**
     * 压缩Bitmap
     */
    private static Bitmap zoomBitmap(Bitmap bitmap, int width, int height) {
        if (null == bitmap) {
            return null;
        }
        try {
            int w = bitmap.getWidth();
            int h = bitmap.getHeight();
            Matrix matrix = new Matrix();
            float scaleWidth = ((float) width / w);
            float scaleHeight = ((float) height / h);
            matrix.postScale(scaleWidth, scaleHeight);
            return Bitmap.createBitmap(bitmap, 0, 0, w, h, matrix, true);
        } catch (Exception e) {
            logger.e(e.toString());
            return null;
        }
    }

    public int getPicCount() {
        return picCount;
    }

    public Map<String, String> getUrlMap() {
        return mUrlMap;
    }

    public List<String> getListUrls(){
        List<String> listUrls = new ArrayList<>();
        for (int i = 0; i < mPicsLl.getChildCount() - 1; i++) {
            View view = mPicsLl.getChildAt(i);
            String tag = (String) view.getTag();
            if (!TextUtils.isEmpty(tag)) {
                if(mUrlMap.get(tag)!=null&&mUrlMap.get(tag).startsWith("http")){
                    listUrls.add(mUrlMap.get(tag));
                }else{
                    return null;
                }
            } else {
                listUrls.add("");
            }
        }
        return listUrls;
    }

    private View.OnDragListener mOnDragListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent event) {
            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    // Do nothing
                    break;
                case DragEvent.ACTION_DRAG_ENTERED:
                    View view = (View) event.getLocalState();
                    for (int i = 0, j = mPicsLl.getChildCount(); i < j; i++) {
                        if (mPicsLl.getChildAt(i) == v) {
                            // 当前位置
                            mPicsLl.removeView(view);
                            v.setAlpha(0);
                            mPicsLl.addView(view, i);
                            break;
                        }
                    }
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    v.setAlpha(1F);
                    break;
                case DragEvent.ACTION_DROP:
                    break;
                case DragEvent.ACTION_DRAG_ENDED:
                    v.setAlpha(1F);
                default:
                    break;
            }
            return true;
        }
    };

    /**
     * 拖动图片相关
     */
    private View.OnTouchListener mOnTouchListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            mDrapView = v;
            if (mGestureDetector.onTouchEvent(event))
                return true;

            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_UP:
                    break;
            }
            return false;
        }
    };

    private class DrapGestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
                 listImg.clear();
                for (int i = 0; i < mPicsLl.getChildCount() - 1; i++) {
                    ImageMessage img = new ImageMessage();
                    View view = mPicsLl.getChildAt(i);
                    String tag = (String) view.getTag();
                    if (!TextUtils.isEmpty(tag)) {
                        img.setUrl(mUrlMap.get(tag));
                    } else {
                        img.setUrl("");
                    }
                    listImg.add(img);
                }
                IMUIHelper.OpenPreviewReadImaActivity(context, listImg, curPosition + "");
//            return super.onSingleTapConfirmed(e);
            return  true;
        }

        @Override
        public void onLongPress(MotionEvent e) {
            super.onLongPress(e);
            ClipData data = ClipData.newPlainText("", "");
            MyDragShadowBuilder shadowBuilder = new MyDragShadowBuilder(
                    mDrapView);
            mDrapView.startDrag(data, shadowBuilder, mDrapView, 0);
        }

        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }
    }

    /**
     * 创建图片影子
     */
    private class MyDragShadowBuilder extends View.DragShadowBuilder {

        private final WeakReference<View> mView;

        public MyDragShadowBuilder(View view) {
            super(view);
            mView = new WeakReference<View>(view);
        }

        @Override
        public void onDrawShadow(Canvas canvas) {
//			canvas.scale(1.5F, 1.5F);
            super.onDrawShadow(canvas);
        }

        @Override
        public void onProvideShadowMetrics(Point shadowSize,
                                           Point shadowTouchPoint) {
            // super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);

            final View view = mView.get();
            if (view != null) {
                shadowSize.set((int) (view.getWidth() * 1.5F),
                        (int) (view.getHeight() * 1.5F));
                shadowTouchPoint.set(shadowSize.x / 2, shadowSize.y / 2);
            } else {
                // Log.e(View.VIEW_LOG_TAG,
                // "Asked for drag thumb metrics but no view");
            }
        }
    }

    private CountChangeListener mCountChangeListener;

    public void setAddClickListener(CountChangeListener mCountChangeListener) {
        this.mCountChangeListener = mCountChangeListener;
    }

    /**
     * 添加按钮点击事件监听
     */
    public interface CountChangeListener {
        void onClick();

        void onChange();
    }

    private class AsyncTaskLoading extends AsyncTaskBase {
        private View layout;
//        private Bitmap photo;
        private String filePath;

        public AsyncTaskLoading(View view) {
            super();
            layout = view;
        }

        @Override
        protected Integer doInBackground(String... params) {
            String picPath = params[0];
//            photo = BitmapFactory.decodeFile(picPath);
//            if (photo == null) {
//                return 0;
//            }
//            photo = ImageUtil.getBigBitmapForDisplay(photo, context);
//            uploadPic(layout, filePath);
            filePath = picPath;
            uploadPic(layout, filePath);

            return 1;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            if (result == 1) {
                /** 添加图片后滑动到加号位置 */
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) mAddIv.getLayoutParams();
                mHoScrollView.smoothScrollTo((layoutParams.width + layoutParams.leftMargin) * mPicsLl.getChildCount(), 0);
                ImageView imageView = (ImageView) layout.findViewById(R.id.iv_pic_add);
                imageView.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        for (int i = 0; i < mPicsLl.getChildCount() - 1; i++) {
                            if (v.getParent().equals(mPicsLl.getChildAt(i))) {
                                curPosition = i;
                            }
                        }
                        return false;
                    }
                });
                imageView.setImageBitmap(ImageUtil.getPathBitmap(context, filePath, 400));
            }
        }
    }
}
