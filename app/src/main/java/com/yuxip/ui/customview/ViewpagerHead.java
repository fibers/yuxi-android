package com.yuxip.ui.customview;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.Bannaers;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.activity.other.WebViewFragmentActivity;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.JazzyViewPager.JazzyViewPager;
import com.yuxip.ui.widget.JazzyViewPager.OutlineContainer;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.ViewPagerScroller;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by Ly on 2015/8/19.
 * 头部轮播图片
 */
public class ViewpagerHead extends RelativeLayout {

    private Context mcontext;
    private JazzyViewPager viewPager;
    private LinearLayout indicator;
    private List<ImageView> list_dots = new ArrayList<>();
    private List<Bannaers> list = new ArrayList<>();
    private MyAdapter adapter;
    private List<View> list_view = new ArrayList<>();
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;
    private SwipeRefreshLayout swipeRefreshLayout;
    private MyHandler handler;

    public ViewpagerHead(Context context, SwipeRefreshLayout swipeRefreshLayout) {
        super(context);
        this.mcontext = context;
        this.swipeRefreshLayout = swipeRefreshLayout;
        initView();
        initImageLoader();
    }

    public ViewpagerHead(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mcontext = context;
        initView();
        initImageLoader();
    }

    public ViewpagerHead(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mcontext = context;
        initView();
        initImageLoader();

    }

    private void initImageLoader() {
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        loaderOptions=ImageLoaderUtil.getOptions(DrawableCache.getInstance(mcontext.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC));

        handler = new MyHandler(this);
    }

    private void initView() {
        LayoutInflater.from(mcontext).inflate(R.layout.head_viewpager_recommand, this, true);
        viewPager = (JazzyViewPager) findViewById(R.id.jazzy_pager_head_recommand);
        indicator = (LinearLayout) findViewById(R.id.indicator_head_recommand);
    }

    public void setData(List<Bannaers> list) {
        this.list = list;
        initDots();
        initViewpager();
        setViewPagerScrollSpeed();
        startTimer();
    }


    public void setSwipeRefreshLayout(SwipeRefreshLayout swipeRefreshLayout){
        if(swipeRefreshLayout!=null){
            this.swipeRefreshLayout = swipeRefreshLayout;
        }
    }
    private void initDots() {
        indicator.removeAllViews();
        list_dots.clear();
        for (int i = 0; i < list.size(); i++) {
            ImageView imgView = new ImageView(mcontext);
            //设置点的大小
            LinearLayout.LayoutParams params_linear = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params_linear.setMargins(5, 10, 5, 10);
            imgView.setLayoutParams(params_linear);
            imgView.setBackgroundResource(R.drawable.selector_face_dots);
            list_dots.add(imgView);
            if (i == 0) {
                imgView.setSelected(true);
            } else {
                imgView.setSelected(false);
            }

            indicator.addView(list_dots.get(i));
        }
    }

    private class MyAdapter extends PagerAdapter {

        List<Bannaers> banners;

        public MyAdapter(List<Bannaers> banners) {
            this.banners = banners;
        }

        @Override
        public int getCount() {
            return Integer.MAX_VALUE;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            if (view instanceof OutlineContainer) {
                return ((OutlineContainer) view).getChildAt(0) == object;
            } else {
                return view == object;
            }
        }


        @Override
        public Object instantiateItem(ViewGroup container, final int position) {
            View v = View.inflate(mcontext, R.layout.page, null);
            ImageView imageView = (ImageView) v.findViewById(R.id.figure_img_bg);
            TextView banner_title = (TextView) v.findViewById(R.id.banner_title);
            banner_title.setText(banners.get(position % banners.size()).getBanner_title());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageLoader.displayImage(banners.get(position % banners.size()).getBanner_portrait(), imageView, loaderOptions);
            container.addView(v, ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bannaers banner = banners.get(position % banners.size());

                    if (banner.getIsurl().equals("1")) {
                        String url = banner.getUrl();
                        Intent intent = new Intent(mcontext, WebViewFragmentActivity.class);
                        intent.putExtra(IntentConstant.WEBVIEW_URL, url);
                        intent.putExtra("storyid", banner.getStoryid());
                        intent.putExtra("isselfstory", banner.getIsselfstory());
                        mcontext.startActivity(intent);
                    } else {

                        if (banner.getIsselfstory().equals("0")) {
                            IMUIHelper.openStoryDetailsActivity(mcontext, banner.getStoryid());
                        } else {
                            Intent intent = new Intent(mcontext, ZiXiDetailsActivity.class);
                            intent.putExtra(IntentConstant.STORY_ID, banner.getStoryid());
                            mcontext.startActivity(intent);
                        }
                    }
                }
            });
            viewPager.setObjectForPosition(v, position);
            return v;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView(viewPager.findViewFromObject(position));
        }
    }


    private void initViewpager() {
        adapter = new MyAdapter(list);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {


            }

            @Override
            public void onPageSelected(int position) {
                setDotsState(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()){
                    case MotionEvent.ACTION_DOWN:
//                        stopTimer();
                        if(swipeRefreshLayout!=null)
                        swipeRefreshLayout.setEnabled(false);
                        break;
                    case MotionEvent.ACTION_UP:
//                        startTimer();
                        if(swipeRefreshLayout!=null)
                        swipeRefreshLayout.setEnabled(true);
                        break;
                    default:
                        if(swipeRefreshLayout!=null)
                            swipeRefreshLayout.setEnabled(false);
                        break;
                }
                return  false;
            }
        });
        setViewPagerScrollSpeed();
    }

    private void setDotsState(int position) {
        for (int i = 0; i < list_dots.size(); i++) {
            list_dots.get(i).setSelected(false);
        }
        list_dots.get(position % list_dots.size()).setSelected(true);
    }

    /**
     * 设置ViewPager的滑动速度
     */
    private void setViewPagerScrollSpeed() {
        ViewPagerScroller scroller = new ViewPagerScroller(viewPager.getContext());
        scroller.setScrollDuration(1000);
        scroller.initViewPagerScroll(viewPager);
    }

    public void notifyData() {
        this.adapter.notifyDataSetChanged();
        viewPager.setCurrentItem(0);
        startTimer();
    }

    private ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> future;
    private void startTimer() {

        if (viewPager == null || adapter == null) {
            return;
        }
        try {
            if (future != null) {
                future.cancel(false);
            }
            future = executor.scheduleAtFixedRate(taskRunnable, 2500, 5000, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void stopTimer() {
        try {
            if (future != null) {
                future.cancel(true);
                handler.removeMessages(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private Runnable taskRunnable = new Runnable() {
        @Override
        public void run() {
            handler.sendEmptyMessage(0);
        }
    };


    static class MyHandler extends Handler {
        WeakReference<ViewpagerHead> weakReference;         //持有DramaChatActivity对象的弱引用

        MyHandler(ViewpagerHead viewpagerHead) {
            weakReference = new WeakReference<>(viewpagerHead);
        }

        @Override
        public void handleMessage(Message msg) {
            ViewpagerHead viewpagerHead = weakReference.get();
            if(viewpagerHead == null) {
                return;
            }
            super.handleMessage(msg);
            if (null != viewpagerHead.viewPager && viewpagerHead.adapter != null) {
                viewpagerHead.viewPager.setCurrentItem((viewpagerHead.viewPager.getCurrentItem() + 1) % viewpagerHead.adapter.getCount());
            }
        }
    }

    public void shutDownService(){
        handler.removeMessages(0);
        executor.shutdown();
    }

}
