package com.yuxip.ui.customview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;

import com.yuxip.R;

/**
 * Created by Ly on 2015/11/9.
 */
public class CustomDividerView extends View {
    private int divider = 0;
    private int color;
    private int lineWidth;
    public CustomDividerView(Context context) {
        super(context);
        initRes();
    }

    public CustomDividerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initRes();
    }

    public CustomDividerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initRes();

    }



    private void initRes(){
        color = Color.parseColor("#ffc5c5c6");
        lineWidth = 3;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Paint paint = new Paint();
        paint.setColor(color);
        paint.setStyle(Paint.Style.FILL);
        if(divider == 0 )
            return;
        for(int i = 1;i<divider; i++){
            canvas.drawRect(new Rect((int) (Math.floor(getWidth()*i/divider)-lineWidth),1, (int) (Math.ceil(getWidth()*i/divider)+lineWidth),getHeight()-1), paint);
        }

    }

    public void drawView(){
        invalidate();
    }


    public void setDividerCount(int dividerCount){
        this.divider = dividerCount;
    }

    public void setColor(int color){
        this.color = color;
    }

    public void setLineWidth(int width){
        this.lineWidth = width;
    }
}
