package com.yuxip.ui.customview;

import android.content.Context;
import android.content.Intent;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.ui.activity.square.CatchNewUserActivity;

/**
 * Created by Administrator on 2015/11/26.
 */
public class SquareListHead extends RelativeLayout implements View.OnClickListener {

    private TextView tvSquareHead;
    private Context context;
    public SquareListHead(Context context) {
        super(context);
        this.context =context;
        initRes();
        initClickRes();
    }

    public SquareListHead(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context =context;
        initRes();
        initClickRes();
    }

    public SquareListHead(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context =context;
        initRes();
        initClickRes();
    }

    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.customview_suare_head,this,true);
        tvSquareHead = (TextView) findViewById(R.id.tv_square_head_customview);
    }

    private void initClickRes(){
        tvSquareHead.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case  R.id.tv_square_head_customview:
                Intent intent = new Intent(context, CatchNewUserActivity.class);
                context.startActivity(intent);
                break;
        }
    }
}
