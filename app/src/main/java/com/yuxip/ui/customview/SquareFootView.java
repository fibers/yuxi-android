package com.yuxip.ui.customview;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.R;


/**
 * Created by Ly on 2015/10/19.
 * 广场页加载的脚布局
 */
public class SquareFootView extends RelativeLayout {

    private Context context;
    private RelativeLayout rel_foot_loading;
    private TextView tv_foot_nomoredata;
    private TextView tv_foot_loadfail;
    private TextView tv_foot_loading;
    private View view_line;
    private RelativeLayout rel_bg;

    public enum ViewEnum{
        TEXTVIEW_LOADFAIL,TEXTVIEW_NOMOREDATA
    }
    public SquareFootView(Context context) {
        super(context);
        this.context = context;
        initRes();
    }

    public SquareFootView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initRes();

    }

    public SquareFootView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        initRes();
    }

    private void initRes(){
        LayoutInflater.from(context).inflate(R.layout.footview_square_comment, this, true);
        tv_foot_nomoredata = (TextView) findViewById(R.id.tv_footview_square_nomoredata);
        tv_foot_loadfail = (TextView) findViewById(R.id.tv_footview_square_loadfail);
        tv_foot_loading = (TextView) findViewById(R.id.tv_footview_square_loadmore);
        view_line = findViewById(R.id.view_line_footview_square);
        rel_foot_loading = (RelativeLayout) findViewById(R.id.rel_footview_square_loadmore);
        rel_bg = (RelativeLayout) findViewById(R.id.rel_footview_square);
        tv_foot_nomoredata.setText(context.getResources().getString(R.string.square_nomoredata));
        tv_foot_loadfail.setText(context.getResources().getString(R.string.square_load_failed_try_again));
        tv_foot_loading.setText(context.getResources().getString(R.string.square_loading));
    }

    public  void  showLoadMore(){
        tv_foot_loadfail.setVisibility(View.GONE);
        tv_foot_nomoredata.setVisibility(View.GONE);
        rel_foot_loading.setVisibility(View.VISIBLE);
    }

    public  void showNoMoreData(){
        tv_foot_loadfail.setVisibility(View.GONE);
        tv_foot_nomoredata.setVisibility(View.VISIBLE);
        rel_foot_loading.setVisibility(View.GONE);
    }

    public void  showTopLine(boolean value){
        if(value){
            view_line.setVisibility(View.VISIBLE);
        }else{
          view_line.setVisibility(View.GONE);
        }
    }


    public  void showLoadFail(){
        tv_foot_nomoredata.setVisibility(View.GONE);
        rel_foot_loading.setVisibility(View.GONE);
        tv_foot_loadfail.setVisibility(View.VISIBLE);
    }

//   public  void hideLoadFail(){
//       tv_foot_loadfail.setVisibility(View.GONE);
//   }

    public  void setBackgroundColor(int color){
        rel_bg.setBackgroundColor(color);
    }

    public  void setBackgroundColor(String color){
        rel_bg.setBackgroundColor(Color.parseColor(color));
    }


    public View getChildView(ViewEnum viewEnum){
        switch (viewEnum){
            case TEXTVIEW_LOADFAIL:
                return  this.tv_foot_loadfail;
            case TEXTVIEW_NOMOREDATA:
                return  this.tv_foot_nomoredata;
        }
        return  null;

    }



}
