package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 适配器
 * Created by SummerRC on 2015/6/5.
 */
public class CurseMemberListGroupAdapter extends BaseAdapter {
    private Context ctx;
    private List<Members> list;
    private String loginId;
    /**
     * 在选择面板里面选择的
     */
    private ArrayList<Integer> checkListId = new ArrayList<>();
    private ArrayList<String> checkListName = new ArrayList<>();
    public CurseMemberListGroupAdapter(Context ctx, List<Members> list) {
        this.ctx = ctx;
        this.list = list;
        this.loginId= String.valueOf(IMLoginManager.instance().getLoginId());
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder holder = new ViewHolder();
        view = LayoutInflater.from(ctx).inflate(R.layout.item_1, viewGroup, false);
        holder.friendName = (TextView) view.findViewById(R.id.friendName);
        holder.friendAvatar = (CircularImage) view.findViewById(R.id.friendAvatar);
        holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);

        if (list.get(position).getId().equals(loginId)) {
                holder.checkBox.setText("自己");
                holder.checkBox.setTextColor(ctx.getResources().getColor(R.color.pink));
                holder.checkBox.setButtonDrawable(new BitmapDrawable());
        }

        if (list.get(position).isChecked()) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }

        holder.friendName.setText(list.get(position).getNickname());
//        new BitmapUtils(ctx).display(holder.friendAvatar, list.get(position).getPortrait());
        ImageLoaderUtil.getImageLoaderInstance().displayImage(list.get(position).getPortrait(),holder.friendAvatar);
        holder.checkBox.setClickable(false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).isChecked()) {
                    checkListId.remove(Integer.valueOf(list.get(position).getId()));
                    checkListName.remove(list.get(position).getNickname());
                    holder.checkBox.setChecked(false);
                    list.get(position).setIsChecked(false);

                } else {
                    if (list.get(position).getId().equals(loginId)) {
                        return;
                    }
                    checkListId.add(Integer.valueOf(list.get(position).getId()));
                    checkListName.add(list.get(position).getNickname());

                    holder.checkBox.setChecked(true);
                    list.get(position).setIsChecked(true);

                }
            }
        });

//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if (b) {
//                   if (Integer.valueOf(list.get(position).getId()) == IMLoginManager.instance()
//                            .getLoginId()) {
//                        return;
//                    }
//                    checkListId.add(Integer.valueOf(list.get(position).getId()));
//                    checkListName.add(list.get(position).getNickname());
//                }else{
//                    checkListId.remove(Integer.valueOf(list.get(position).getId()));
//                    checkListName.remove(list.get(position).getNickname());
//                }
//            }
//        });
        return view;
    }

    class ViewHolder {
        CheckBox checkBox;
        TextView friendName;
        TextView tvHint;
        CircularImage friendAvatar;
    }

    public ArrayList<Integer> getCheckListId() {
        return checkListId;
    }

    public ArrayList<String> getCheckListName() {
        return checkListName;
    }
}
