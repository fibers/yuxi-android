package com.yuxip.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.FamilyListEntity;
import com.yuxip.ui.activity.other.FamilyDetailsActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by Administrator on 2015/5/5.
 *
 * 设置现实的数据
 */
public class SearchFamilyAdapter extends BaseAdapter {
    private List<FamilyListEntity> searchFaimalList;
    private Context context;
    private ImageLoader imageLoader;

    public SearchFamilyAdapter(List<FamilyListEntity> searchFaimalList, Context context) {
        this.searchFaimalList = searchFaimalList;
        this.context = context;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    @Override
    public int getCount() {
        if(searchFaimalList == null) {
            return 0;
        } else {
            return searchFaimalList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.activity_search_family_item, null);
            holder.iv_touxiang = (CircularImage) convertView.findViewById(R.id.iv_touxiang);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_intro = (TextView) convertView.findViewById(R.id.tv_intro);
            holder.iv_add = (ImageView) convertView.findViewById(R.id.iv_add);
            holder.rlFamilyItem = (RelativeLayout) convertView.findViewById(R.id.rlFamilyItem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
        DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
        imageLoader.displayImage(searchFaimalList.get(position).getPortrait(),holder.iv_touxiang, imageOptions);
        holder.tv_name.setText(searchFaimalList.get(position).getName());
        holder.tv_intro.setText(searchFaimalList.get(position).getIntro());
        final String familyId= searchFaimalList.get(position).getId();
        final String isMember = searchFaimalList.get(position).getIsMember();
        holder.rlFamilyItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isMember.equals("1")){
//                    IMUIHelper.openFamilyDataActivity(context, IntentConstant.FamilyDataActivityType.TYPE_MEMBER,familyId);
                    Intent i = new Intent(context, FamilyDetailsActivity.class);
                    i.putExtra(IntentConstant.SESSION_KEY, "2_" + familyId);
                    i.putExtra(IntentConstant.FAMILY_INFO_START_FROM, "contact");
                    context.startActivity(i);
                }else {
                    IMUIHelper.openFamilyDataActivity(context, IntentConstant.FamilyDataActivityType.TYPE_NOT_MEMBER,familyId);
                }
            }
        });
        return convertView;
    }


    private static class ViewHolder {
        private CircularImage iv_touxiang;
        private TextView tv_name;
        private ImageView iv_add;
        private TextView familyCreator; //创建者
        private TextView tv_intro;
        private RelativeLayout rlFamilyItem;
    }
}
