package com.yuxip.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.yuxip.R;
import com.yuxip.entity.FriendEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.DialogHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by Administrator on 2015/5/5.
 *
 * 设置现实的数据
 */
public class SearchPersonAdapter extends BaseAdapter {
    private List<FriendEntity> searchPersonList;
    private IMService imService;
    private Context context;


    public SearchPersonAdapter(List<FriendEntity> searchPersonList, Context context, IMService imService) {
        this.searchPersonList = searchPersonList;
        this.context = context;
        this.imService = imService;
    }



    @Override
    public int getCount() {

        if(searchPersonList == null) {
            return 0;
        } else {
            return searchPersonList.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return searchPersonList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.activity_search_item, null);
            holder.iv_touxiang = (CustomHeadImage) convertView.findViewById(R.id.iv_touxiang);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvAddFriend = (TextView) convertView.findViewById(R.id.tv_add_friend_search_person);
            holder.tvIsAddedFriend = (TextView) convertView.findViewById(R.id.tv_isadded_fridend_search_person);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }
        holder.tv_name.setText(searchPersonList.get(position).getName());
        holder.iv_touxiang.loadImage(searchPersonList.get(position).getPortrait()+"");
        holder.tvAddFriend.setOnClickListener(new MyListener(position));
        return convertView;
    }


    private static class ViewHolder {
        private CustomHeadImage iv_touxiang;
        private TextView tv_name;
        private TextView tvAddFriend;
        private TextView tvIsAddedFriend;
        private TextView familyCreator; //创建者
    }

    class MyListener implements View.OnClickListener {
        private int position;

        MyListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            String id = searchPersonList.get(position).getId();
            if(id.equals(IMLoginManager.instance().getLoginId()+"")) {
                Toast.makeText(context, "不能添加自己为好友!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "向"+id+"发送好友请求", Toast.LENGTH_SHORT).show();
                Log.e("ims", String.valueOf(imService == null));
                try {
                    DialogHelper.showAddFriendDialog(context, imService, Integer.valueOf(id));
                } catch (Exception e) {
                    Log.e("SearchPersonAdapter", "error");
                    e.printStackTrace();
                }
            }
        }
    }
}
