package com.yuxip.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;

import java.util.List;

/**
 * Created by HeTianpeng on 2015/07/18.
 */
public class StoryCategoryAdapter extends BaseRecyclerViewAdapter {

    private List<StoryClass.ListEntity> categorys;

    public StoryCategoryAdapter(Context context, List<StoryClass.ListEntity> categorys) {
        super(context);
        this.categorys = categorys;
        setCustomLoadMoreView(null);
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.item_story_category, viewGroup, false);
        return new UltimateRecyclerviewViewHolder(view);
    }

    @Override
    public int getAdapterItemCount() {
        return null == categorys || categorys.isEmpty() ? 0 : categorys.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return 0;
    }


    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int i) {

    }

    @Override
    public void onBinddViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getAdapterItemCount() == 0) {
            return;
        }
        if (holder.itemView instanceof TextView) {
            TextView text = (TextView) holder.itemView;
            text.setText(categorys.get(position).getName());
        }
    }
}
