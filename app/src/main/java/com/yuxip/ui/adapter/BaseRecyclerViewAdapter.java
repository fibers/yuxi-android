package com.yuxip.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;

import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.yuxip.R;

/**
 * Created by HeTianpeng on 2015/07/18.
 */
public abstract class BaseRecyclerViewAdapter extends UltimateViewAdapter {

    protected Context context;

    public BaseRecyclerViewAdapter(Context context) {
        this.context = context;

//        View footer = LayoutInflater.from(context).inflate(R.layout.footer_view, null);
//        setCustomLoadMoreView(footer);
        showCoustomLoadMoreView(true);
    }

    public void showCoustomLoadMoreView(boolean isShow) {
        if (isShow) {
            View footer = LayoutInflater.from(context).inflate(R.layout.footer_view, null);
            setCustomLoadMoreView(footer);
        } else {
            setCustomLoadMoreView(null);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, RecyclerView.ViewHolder holder);
    }

    private OnItemClickListener onItemClickListener;

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (onItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListener.onItemClick(position, holder);
                }
            });
        }
        onBinddViewHolder(holder, position);
    }

    public abstract void onBinddViewHolder(RecyclerView.ViewHolder holder, int position);

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
