package com.yuxip.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.yuxip.JsonBean.FriendGroups;
import com.yuxip.JsonBean.Friends;
import com.yuxip.R;
import com.yuxip.imservice.manager.IMGroupManager;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2015/6/5.
 */
public class GroupMemberInviteAdapter extends BaseAdapter {
    private Context ctx;
    List<FriendGroups> friendGroups;
    private List<Friends> allUserList = new ArrayList<>();
    /**在选择面板里面选择的*/
    private Set<Integer> checkListSet= new HashSet<>();
    private String groupId;

    public GroupMemberInviteAdapter(Context ctx,List<FriendGroups> friendGroups){
        this.ctx = ctx;
        this.friendGroups =friendGroups;
        initData();
    }

    public GroupMemberInviteAdapter(Context ctx,List<FriendGroups> friendGroups,String  groupId){
        this.ctx = ctx;
        this.friendGroups =friendGroups;
        this.groupId = groupId;
        initData();
    }

    private void initData() {
        if (friendGroups!=null){
        for (FriendGroups s : friendGroups){
            List<Friends>  fs = s.getFriends();
            for (Friends fg : fs){
                if(!isGroupMember(fg))
                allUserList.add(fg);
            }
            }
        }
    }
    /**------------------------------  判断 好友  是否是群成员--------------------------------------------------------   start --- -------------*/
    private boolean isGroupMember(Friends fg){
        if(TextUtils.isEmpty(groupId))
            return false;
        Set<Integer> set = IMGroupManager.instance().getGroupMember(groupId);
//        Iterator it = set.iterator();
       for( int id :set){
           if(Integer.valueOf(fg.getId())== id){
               return true;
           }
       }
        return  false;
    }
    /**--------------------------------------------------------------------------------------------   end    --- -------------*/

    @Override
    public int getCount() {
        return allUserList.size();
    }

    @Override
    public Object getItem(int position) {
        return allUserList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
       final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(ctx).inflate(R.layout.item_1, viewGroup,false);
            holder.friendName = (TextView) view.findViewById(R.id.friendName);
            holder.friendAvatar = (CircularImage) view.findViewById(R.id.friendAvatar);
            holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.checkBox.setClickable(false);
        holder.friendName.setText(allUserList.get(position).getName());
//        new BitmapUtils(ctx).display(holder.friendAvatar, allUserList.get(position).getPortrait());
        ImageLoaderUtil.getImageLoaderInstance().displayImage(allUserList.get(position).getPortrait(),holder.friendAvatar);
        if (allUserList.get(position).isChecked()) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);

        }

        holder.checkBox.setClickable(false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (allUserList.get(position).isChecked()) {
                    allUserList.get(position).setIsChecked(false);
                    checkListSet.remove(Integer.valueOf(allUserList.get(position).getId()));
                    holder.checkBox.setChecked(false);
                } else {
                    allUserList.get(position).setIsChecked(true);
                    checkListSet.add(Integer.valueOf(allUserList.get(position).getId()));
                    holder.checkBox.setChecked(true);
                }
            }
        });

//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//                if (allUserList.get(position).isChecked()) {
//                    allUserList.get(position).setIsChecked(false);
//                    checkListSet.remove(Integer.valueOf(allUserList.get(position).getId()));
//                    holder.checkBox.setChecked(false);
//                } else {
//                    allUserList.get(position).setIsChecked(true);
//                    checkListSet.add(Integer.valueOf(allUserList.get(position).getId()));
//                    holder.checkBox.setChecked(true);
//                }
//
////                int userId = 0;
////                if (b) {
////                    userId = Integer.valueOf(allUserList.get(position).getId());
////                    checkListSet.add(userId);
////                } else {
////                    checkListSet.add(userId);
////                }
//            }
//        });
        return view;
    }
    class ViewHolder{
        CheckBox checkBox;
        TextView friendName;
        TextView tvHint;
        CircularImage friendAvatar;
    }

    public void setCheckListSet(Set<Integer> checkListSet) {
        this.checkListSet = checkListSet;
    }

    public Set<Integer> getCheckListSet() {
        return checkListSet;
    }
}
