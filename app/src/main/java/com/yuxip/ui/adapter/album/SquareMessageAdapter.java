package com.yuxip.ui.adapter.album;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.R;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.Random;

/**
 * Created by ZQF on 2015/10/13.
 */
public class SquareMessageAdapter extends BaseAdapter {

    private Context context;
    private ImageLoader imageLoader;

    public SquareMessageAdapter(Context context, Object object) {
        this.context = context;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    @Override
    public int getCount() {
        return 6;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_square_message_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        // 随机数据
        Random random = new Random();
        if (random.nextBoolean()) {
            holder.vUsernameTv.setText("流川枫等" + (random.nextInt(5) + 1) + "人");
            holder.vActionTv.setText("赞了你的话题");
        } else {
            holder.vUsernameTv.setText("你大爷");
            holder.vActionTv.setText("评论了你的话题");
        }
        return convertView;
    }

    static class ViewHolder {
        CustomHeadImage vHeadIv;
        TextView vUsernameTv;
        TextView vTimeTv;
        TextView vActionTv;
        TextView vTitleTv;
        TextView vContentTv;

        public ViewHolder(View view) {
            vHeadIv = (CustomHeadImage) view.findViewById(R.id.iv_square_item_head_img);
            vUsernameTv = (TextView) view.findViewById(R.id.tv_square_item_username);
            vActionTv = (TextView) view.findViewById(R.id.tv_square_item_action);
            vTimeTv = (TextView) view.findViewById(R.id.tv_square_item_time);
            vTitleTv = (TextView) view.findViewById(R.id.tv_square_item_title);
            vContentTv = (TextView) view.findViewById(R.id.tv_square_item_content);
        }
    }
}
