package com.yuxip.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2015/7/1.
 */
public class StoryListAdapter extends BaseRecyclerViewAdapter {
    private List<HotBean.StorysEntity> storyslist;
//    private BitmapUtils bitmapUtils;
    private ImageLoader imageLoader;
    public StoryListAdapter(Context context, List<HotBean.StorysEntity> storyslist) {
        super(context);
        this.storyslist = storyslist;
//        bitmapUtils = new BitmapUtils(context);
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }





    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public void onBinddViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (getAdapterItemCount() == 0) {
            return;
        }

        if (holder instanceof ViewHolder) {
            final ViewHolder viewHolder = (ViewHolder) holder;
            final HotBean.StorysEntity storysEntity = storyslist.get(position);
            if (!storysEntity.getStoryimg().equals("")) {
                viewHolder.fl_hot.setVisibility(View.VISIBLE);
//                bitmapUtils.display(viewHolder.imgBgHot, storysEntity.getStoryimg());
                imageLoader.displayImage(storysEntity.getStoryimg(),viewHolder.imgBgHot);
            } else {
                viewHolder.fl_hot.setVisibility(View.GONE);
//                viewHolder.biaoqian.setVisibility(View.GONE);
                viewHolder.tvLable.setVisibility(View.INVISIBLE);
            }
//            bitmapUtils.display(viewHolder.userHead, storysEntity.getCreatorportrait());  //获取头像
             viewHolder.imgIconHot.loadImage(storysEntity.getCreatorportrait());
//            viewHolder.tvClass1.setText(storysEntity.getCategory());
            viewHolder.tvLable.setText(storysEntity.getCategory());
            viewHolder.tvTitleHot.setText(storysEntity.getTitle());
            viewHolder.tvInfoHot.setText(storysEntity.getIntro());
            viewHolder.tvTimeHot
                    .setText(DateUtil.getSessionTime(Integer.parseInt(storysEntity.getCreatetime())));
            float numWord = Float.parseFloat(storysEntity.getWordcounts());
            if (numWord >= 10000) {
                float n = numWord / 10000;
                viewHolder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "w");

            } else if (numWord >= 1000) {
                float n = numWord / 1000;
                viewHolder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "k");
            } else {
                viewHolder.tvNumOfWordsHot.setText("字数：" + (int) numWord);
            }
            viewHolder.tvTopicFaveNum.setText(storysEntity.getPraisenum());
            viewHolder.tvTopicCommentNum.setText(storysEntity.getCommentnum());
//            final ViewHolder finalHolder = viewHolder;
            //每条点击事件
            viewHolder.rlWorldItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //判断故事还是剧
                    if (storysEntity.getType().equals("1")) {
                        viewHolder.tvNameHot.setText(storysEntity.getCreatorname() + "的故事");
                        Intent intent = new Intent(context, ZiXiDetailsActivity.class);
                        intent.putExtra(IntentConstant.STORY_ID, storysEntity.getStoryid());
                        intent.putExtra(IntentConstant.CREATOR_ID, storysEntity.getCreatorid());
                        context.startActivity(intent);
                    } else {
                        viewHolder.tvNameHot.setText(storysEntity.getCreatorname() + "的剧");
                        IMUIHelper.openStoryDetailsActivity(context, storysEntity.getStoryid());
                    }
                }
            });
            //头像点击事件查看好友资料
            viewHolder.imgIconHot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openUserHomePageActivity(context, storysEntity.getCreatorid());
                }
            });

            //进群按钮
            viewHolder.iv_go_group.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean is_admin = storysEntity.getCreatorid().equals(String.valueOf(IMLoginManager.instance().getLoginId()));
                    IMUIHelper.openStoryGroupSelectActivity(context, storysEntity.getStoryid(), is_admin, storysEntity.getCheckgroupid(), "");
                }
            });

            if (storysEntity.getType().equals(ConstantValues.STORY_TYPE_ZIXI)) {
                viewHolder.tvNameHot.setText(storysEntity.getCreatorname() + "的自戏");
                viewHolder.tvLable.setVisibility(View.INVISIBLE);
                viewHolder.iv_go_group.setVisibility(View.INVISIBLE);

            } else {
                viewHolder.tvNameHot.setText(storysEntity.getCreatorname() + "的剧");
                viewHolder.tvLable.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(storysEntity.getCheckgroupid())&&!storysEntity.getCheckgroupid().equals("@")) {
                    viewHolder.iv_go_group.setVisibility(View.VISIBLE);
                    //进群按钮
                    viewHolder.iv_go_group.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean is_admin = storysEntity.getCreatorid().equals(String.valueOf(IMLoginManager.instance().getLoginId()));
                            IMUIHelper.openStoryGroupSelectActivity(context, storysEntity.getStoryid(), is_admin, storysEntity.getCheckgroupid(), storysEntity.getCheckgrouptitle());
                        }
                    });
                } else {        //没有审核群
                    viewHolder.iv_go_group.setVisibility(View.INVISIBLE);
                }
            }
        }


    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View view = View.inflate(context, R.layout.adapter_story_list_item, null);
        return new ViewHolder(view);
    }

    @Override
    public int getAdapterItemCount() {
        return null == storyslist || storyslist.isEmpty() ? 0 : storyslist.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int i) {

    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'adapter_story_list_item.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends  UltimateRecyclerviewViewHolder{
        @InjectView(R.id.rl_world_item)
        RelativeLayout rlWorldItem;
        //        @InjectView(R.id.ll_biaoqian)
//        LinearLayout biaoqian;
        @InjectView(R.id.img_bg_hot)
        ImageView imgBgHot;
        @InjectView(R.id.fl_hot)
        FrameLayout fl_hot;
        @InjectView(R.id.iv_go_group)
        ImageView iv_go_group;
        @InjectView(R.id.tv_title_hot)
        TextView tvTitleHot;
        @InjectView(R.id.tv_info_hot)
        TextView tvInfoHot;
        @InjectView(R.id.tv_time_hot)
        TextView tvTimeHot;
        @InjectView(R.id.tv_num_of_words_hot)
        TextView tvNumOfWordsHot;
        @InjectView(R.id.iv_topic_fave)
        ImageView ivTopicFave;
        @InjectView(R.id.tv_topic_fave_num)
        TextView tvTopicFaveNum;
        @InjectView(R.id.img_comment_hot)
        ImageView imgCommentHot;
        @InjectView(R.id.tv_topic_comment_num)
        TextView tvTopicCommentNum;
        @InjectView(R.id.hot_user_head_img)
        CustomHeadImage imgIconHot;
        @InjectView(R.id.tv_name_hot)
        TextView tvNameHot;
        //        @InjectView(R.id.tv_class_1)
//        TextView tvClass1;
//        @InjectView(R.id.tv_class_2)
//        TextView tvClass2;
//        @InjectView(R.id.tv_class_3)
//        TextView tvClass3;
        @InjectView(R.id.tv_label_recommand_adapter)
        TextView tvLable;
        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
