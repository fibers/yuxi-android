package com.yuxip.ui.adapter;

import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.yuxip.JsonBean.StoryClass;
import com.yuxip.R;
import com.yuxip.utils.T;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by HTP on 2015/07/13.
 * description:选择要创建的剧的标签类型的适配器
 */
public class SelectStoryClassRecyclerAdapter extends RecyclerView.Adapter<SelectStoryClassRecyclerAdapter.ViewHolder> {

    private List<StoryClass.ListEntity> entities;
    private Map<String, StoryClass.ListEntity> entityMap;
    private int count = 0;

    public SelectStoryClassRecyclerAdapter(List<StoryClass.ListEntity> entities) {
        this.entities = entities;
        entityMap = new HashMap<>();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_select_story_class, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final StoryClass.ListEntity entity = entities.get(position);
        holder.tvStoryClassName.setText(entity.getName());

        if (entity.isSelected()) {
            holder.checkBoxStoryClass.setChecked(true);
        } else {
            holder.checkBoxStoryClass.setChecked(false);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.checkBoxStoryClass.isChecked()) {
                    holder.checkBoxStoryClass.setChecked(false);
                    entity.setIsSelected(false);
                    entityMap.remove(entity.getId());
                    count--;
                } else {
                    if (count >= 3) {
                        T.showShort(holder.itemView.getContext(), "最多选择3个");
                        return;
                    }

                    holder.checkBoxStoryClass.setChecked(true);
                    entity.setIsSelected(true);
                    entityMap.put(entity.getId(), entity);
                    count++;
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return null == entities || entities.isEmpty() ? 0 : entities.size();
    }

    public List<StoryClass.ListEntity> getEntities() {
        return entities;
    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_select_story_class.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends UltimateRecyclerviewViewHolder {
        @InjectView(R.id.tv_story_class_name)
        AppCompatTextView tvStoryClassName;
        @InjectView(R.id.check_box_story_class)
        AppCompatCheckBox checkBoxStoryClass;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }

    public String getSelectIds() {
        String result = "";
        for (String key : entityMap.keySet()) {
            if (result.equals("")) {
                result = key;
            } else {
                result = result + "," + key;
            }
        }
        return result;
    }
}
