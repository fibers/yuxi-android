package com.yuxip.ui.adapter;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.JsonBean.AddMessageBean;
import com.yuxip.JsonBean.GroupAddMessageBean;
import com.yuxip.JsonBean.MessageBean;
import com.yuxip.R;
import com.yuxip.entity.FamilyInfoDao;
import com.yuxip.imservice.entity.TextMessage;
import com.yuxip.imservice.event.UnreadEvent;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 系统消息适配器
 * Edited by SummerRC on 2015/07/06.
 */
public class SystemMessageAdapter extends UltimateViewAdapter {
    private List<MessageBean> messageBeans;
    private IMService imService;
    private ImageLoader imageLoader;
    private DBInterface dbInterface;
    private Logger logger;
    private UnreadEvent unreadEvent;

    public SystemMessageAdapter(List<MessageBean> messageBeans, IMService imService) {
        this.messageBeans = messageBeans;
        if (this.messageBeans != null && this.messageBeans.size() > 0) {
            Collections.reverse(this.messageBeans);
        }
        unreadEvent = new UnreadEvent(UnreadEvent.Event.UNREAD_MSG_SYSTEM);
        this.imService = imService;
        dbInterface = imService.getDbInterface();
        logger = Logger.getLogger(SystemMessageAdapter.class);
    }

    public static class SystemMessageViewHolder extends UltimateRecyclerviewViewHolder {
        @InjectView(R.id.icon)
        ImageView icon;
        @InjectView(R.id.tv_desc)
        TextView tvDesc;
        @InjectView(R.id.tv_agree)
        TextView tv_agree;
        @InjectView(R.id.tv_reject)
        TextView tv_reject;

        public SystemMessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_system_message, viewGroup, false);
        SystemMessageViewHolder holder = new SystemMessageViewHolder(v);
        if (imageLoader == null)
            imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        return holder;
    }

    @Override
    public int getAdapterItemCount() {
        return null == messageBeans || messageBeans.isEmpty() ? 0 : messageBeans.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return 0;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final AddMessageBean mb = (AddMessageBean) messageBeans.get(position);
        final MessageBean.MessageTypeDetailSystem systemType = (MessageBean.MessageTypeDetailSystem) mb.getiMessageTypeDetail();
        MyClickListener listener = new MyClickListener(position, mb, holder);

        final SystemMessageViewHolder sHolder = (SystemMessageViewHolder) holder;

        if (mb.isAgree()) {          //已同意
            sHolder.tv_reject.setVisibility(View.VISIBLE);
            sHolder.tv_reject.setText(R.string.agreed_yes);
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_agree.setClickable(false);
        } else if (mb.isReject()) {   //已拒绝
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_agree.setClickable(false);
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_reject.setVisibility(View.VISIBLE);
            sHolder.tv_reject.setText(R.string.reject_yes);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
        } else {                     //未处理
            sHolder.tv_agree.setVisibility(View.VISIBLE);
            sHolder.tv_agree.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.white));
            sHolder.tv_agree.setBackgroundResource(R.drawable.agree);
            sHolder.tv_agree.setText(R.string.agreed);
            sHolder.tv_agree.setOnClickListener(listener);
            sHolder.tv_reject.setVisibility(View.VISIBLE);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
            sHolder.tv_reject.setBackgroundResource(R.drawable.reject_selector);
            sHolder.tv_reject.setText(R.string.reject);
            sHolder.tv_reject.setOnClickListener(listener);
        }

        ((SystemMessageViewHolder) holder).icon.setOnClickListener(listener);
        displayImage(sHolder, mb);
        sHolder.tvDesc.setText(mb.getMessage());
    }

    /**
     * 同意入群 家族 or 剧  / 申请 or 邀请
     *
     * @param mb
     * @param sHolder
     */
    private void applyGroup(GroupAddMessageBean mb, SystemMessageViewHolder sHolder) {
        ApplyGroupEntity entity = mb.getApplyGroupEntity();

        try {
            entity.setAgree("true");
            /** 同意加群请求并更新数据库 */
            Set<Integer> set = new HashSet<>();
            set.add(Integer.valueOf(entity.getApplyId()));
            /** 更新群的成员信息 */
            int iType = Integer.valueOf(entity.getType());
            if (iType == 0) {        //申请的场合
                imService.getMessageManager().sendMsgGroupAddMemberConfirmReq(
                        Integer.valueOf(entity.getApplyId()), Integer.valueOf(entity.getGroupId()), 1, iType);
            } else {                 //邀请的场合
                imService.getMessageManager().sendMsgGroupAddMemberConfirmReq(Integer.valueOf(entity.getCreatorId()),
                        Integer.valueOf(entity.getGroupId()), 1, iType);
                FamilyInfoDao familyInfoDao = new FamilyInfoDao();
                int groupId = Integer.valueOf(entity.getGroupId());
                familyInfoDao.setGroupid(groupId);
                familyInfoDao.setPortrait(entity.getPortrait());
                YXFamilyManager.instance().addFamilyInfoDao(groupId, familyInfoDao);
            }

            imService.getGroupManager().reqAddGroupMember(Integer.valueOf(entity.getGroupId()), set);
            dbInterface.batchInsertOrUpdateApplyGroupEntity(entity);
            sHolder.tv_reject.setText("已同意");
            sHolder.tv_agree.setBackgroundColor(Color.TRANSPARENT); //背景透明度;
            sHolder.tv_agree.setClickable(false);
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources()
                    .getColor(android.R.color.darker_gray));
            mb.setIsAgree(true);
            EventBus.getDefault().post(unreadEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void applyAddFriend(AddMessageBean mb, SystemMessageViewHolder sHolder) {
        ApplyFriendEntity entity = mb.getAddFriendEntity();
        try {
            entity.setAgree("true");
            /** 更新User信息 */
            ArrayList<Integer> userIds = new ArrayList<>(1);
            userIds.add(Integer.valueOf(entity.getUid()));
            imService.getContactManager().reqGetDetaillUsers(userIds);
            /*************************************      发送一条消息给申请这，帮助申请者的客户端更新session    begin   ***************************************************************/
            String currentSessionKey = "1_" + entity.getUid();
            UserEntity peerEntity = (UserEntity) imService.getSessionManager().findPeerEntity(currentSessionKey);
            /** 构建发送的message */
            TextMessage textMessage = TextMessage.buildForSend("我同意了你的好友请求，现在可以聊天啦！", imService.getLoginManager().getLoginInfo(), peerEntity);
            /** 得到信息管理者，然后将信息发送出去 */
            imService.getMessageManager().sendText(textMessage);
            /** 同意好友请求并更新数据库 */
            imService.getMessageManager()
                    .sendAddFriendConfirmReq(Integer.valueOf(entity.getUid()), 1);

            Toast.makeText(sHolder.itemView.getContext(), "已经变成好友", Toast.LENGTH_SHORT).show();

            dbInterface.batchInsertOrUpdateApplyFriendEntity(entity);
            sHolder.tv_reject.setText("已同意");
            sHolder.tv_agree.setClickable(false);
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_agree.setBackgroundColor(Color.TRANSPARENT); //背景透明度;
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources()
                    .getColor(android.R.color.darker_gray));
            mb.setIsAgree(true);
            FriendGroupManager.getInstance().addFriend(entity);
            EventBus.getDefault().post(unreadEvent);
            /*************************************      发送一条消息给申请这，帮助申请者的客户端更新session     end    ***************************************************************/
        } catch (Exception e) {
           logger.e(e.toString());
        }
    }

    private void displayImage(SystemMessageViewHolder sHolder, MessageBean mb) {
        Drawable drawable = DrawableCache.getInstance(sHolder.icon.getContext().getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
        DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
        imageLoader.displayImage(mb.getIconUrl(), sHolder.icon, imageOptions);
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

    }

    private class MyClickListener implements View.OnClickListener {
        int mPosition;
        MessageBean.MessageTypeDetailSystem systemType;
        SystemMessageViewHolder sHolder;
        AddMessageBean mb;

        public MyClickListener(int mPosition, AddMessageBean mb, RecyclerView.ViewHolder sHolder) {
            this.mPosition = mPosition;
            this.mb = mb;
            this.systemType = (MessageBean.MessageTypeDetailSystem) mb.getiMessageTypeDetail();
            this.sHolder = (SystemMessageViewHolder) sHolder;
        }

        @Override
        public void onClick(View v) {
            switch (systemType) {
                case MESSAGE_TYPE_SYSTEM_FRIEDNS_ADD: //被别人请求添加好友
                    if (v.equals(sHolder.tv_agree)) {
                        applyAddFriend(mb, sHolder);
                    } else if (v.equals(sHolder.tv_reject)) {
                        rejectAddFriend(mb, sHolder);
                    } else if (v.equals(sHolder.icon)) {
                        IMUIHelper.openUserHomePageActivity(sHolder.icon.getContext(), mb.getApplyId() + "");
                    }
                    break;
                case MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_ADD:
                case MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_ADD:
                case MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_INVITATION:
                case MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_INVITATION:
                    if (v.equals(sHolder.tv_agree)) {
                        applyGroup((GroupAddMessageBean) mb, sHolder);
                    } else if (v.equals(sHolder.tv_reject)) {
                        rejectApplyGroup((GroupAddMessageBean) mb, sHolder);
                    }
                    break;
            }
        }
    }


    /**
     * 拒绝好友请求
     *
     * @param mb
     * @param sHolder
     */
    private void rejectAddFriend(AddMessageBean mb, SystemMessageViewHolder sHolder) {
        AddMessageBean amb = mb;
        ApplyFriendEntity entity = amb.getAddFriendEntity();
        try {
            entity.setAgree("reject");
            /** 同意好友请求并更新数据库 */
            imService.getMessageManager().sendAddFriendConfirmReq(Integer.valueOf(entity.getUid()), 0);
            dbInterface.batchInsertOrUpdateApplyFriendEntity(entity);
            sHolder.tv_reject.setText(R.string.reject_yes);
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_agree.setClickable(false);
            sHolder.tv_reject.setBackgroundColor(Color.TRANSPARENT); //背景透明度;
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
            mb.setIsReject(true);
            EventBus.getDefault().post(unreadEvent);
        } catch (Exception e) {
            logger.e(e.toString());
        }
    }


    /**
     * 拒绝群请求
     *
     * @param mb
     * @param sHolder
     */
    private void rejectApplyGroup(GroupAddMessageBean mb, SystemMessageViewHolder sHolder) {
        ApplyGroupEntity entity = mb.getApplyGroupEntity();
        try {
            entity.setAgree("reject");
            dbInterface.batchInsertOrUpdateApplyGroupEntity(entity);
            int iType = Integer.valueOf(entity.getType());
            if (iType == 0) {        //申请的场合
                imService.getMessageManager().sendMsgGroupAddMemberConfirmReq(
                        Integer.valueOf(entity.getApplyId()), Integer.valueOf(entity.getGroupId()), 0, iType);
            } else {                //邀请的场合
                imService.getMessageManager().sendMsgGroupAddMemberConfirmReq(
                        Integer.valueOf(entity.getCreatorId()), Integer.valueOf(entity.getGroupId()), 0, iType);
            }
            sHolder.tv_reject.setText(R.string.reject_yes);
            sHolder.tv_reject.setClickable(false);
            sHolder.tv_reject.setBackgroundColor(Color.TRANSPARENT); //背景透明度;
            sHolder.tv_agree.setVisibility(View.INVISIBLE);
            sHolder.tv_agree.setClickable(false);
            sHolder.tv_reject.setTextColor(sHolder.itemView.getContext().getResources().getColor(android.R.color.darker_gray));
            mb.setIsReject(true);
            EventBus.getDefault().post(unreadEvent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
