package com.yuxip.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.TopiclistEntity;
import com.yuxip.R;
import com.yuxip.config.SysConstant;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by Administrator on 2015/7/1.
 * 广场的适配器
 */
public class SquareAdapter extends BaseAdapter {
    private List<TopiclistEntity> squareList;
    private Context context;
    private ImageLoader imageLoader;

    public SquareAdapter(Context ctx, List<TopiclistEntity> squareList) {
        this.squareList = squareList;
        context = ctx;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    @Override
    public int getCount() {
        return null == squareList || squareList.isEmpty() ? 0 : squareList.size();
    }

    @Override
    public Object getItem(int i) {
        return squareList.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        SquareAdapter.ViewHolder holder;
        if (view == null) {
            view = View.inflate(context, R.layout.adapter_square_list_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String timeStr = squareList.get(position).getCreattime();
        //根据返回的时间戳转换成时间
        String createTime = DateUtil.getDateWithOutYear(Integer.parseInt(timeStr));
        holder.topicTitle.setText(squareList.get(position).getTopictitle());
        holder.userTopic.setText(squareList.get(position).getCreatorname());
        holder.topicTime.setText(createTime);
        holder.topicFaveNum.setText(squareList.get(position).getPraisenum());
        holder.topicCommentNum.setText(squareList.get(position).getCommentnum());
        holder.imgIcon.setVipSize(12f);
        holder.imgIcon.loadImage(squareList.get(position).getCreatorportrait(), "0");
        holder.showImg1.setVisibility(View.GONE);
        holder.showImg2.setVisibility(View.GONE);
        holder.showImg3.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(squareList.get(position).getTopicimgs())) {
            String str[] = getImgs(squareList.get(position).getTopicimgs());
            switch (str.length) {
                case 3:
                    imageLoader.displayImage(str[2] + SysConstant.PICTURE_58X58, holder.showImg3);
                    holder.showImg3.setVisibility(View.VISIBLE);
                case 2:
                    imageLoader.displayImage(str[1] + SysConstant.PICTURE_58X58, holder.showImg2);
                    holder.showImg2.setVisibility(View.VISIBLE);
                case 1:
                    imageLoader.displayImage(str[0] + SysConstant.PICTURE_58X58, holder.showImg1);
                    holder.showImg1.setVisibility(View.VISIBLE);
                    break;
            }
        }
        holder.topicItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openTopicDetailsActivity(context, squareList.get(position).getTopicid());
            }
        });

        holder.imgIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openUserHomePageActivity(context, squareList.get(position).getCreatorid());
            }
        });
        return view;
    }

    /********
     * 图片之间用 逗号 分隔
     ***********/
    private String[] getImgs(String imgstrs) {
        if (imgstrs.contains("[,]")) {
            return imgstrs.split("[,]");
        } else {
            return new String[]{imgstrs};
        }
    }


    static class ViewHolder {
        LinearLayout topicItem;
        TextView userTopic;
        TextView topicTitle;
        ImageView topicFave;
        ImageView topicimgs;
        TextView topicTime;
        TextView topicFaveNum;
        TextView topicCommentNum;
        CustomHeadImage imgIcon;
        ImageView showImg1;
        ImageView showImg2;
        ImageView showImg3;

        public ViewHolder(View itemView) {
            imgIcon = (CustomHeadImage) itemView.findViewById(R.id.img_icon);
            topicItem = (LinearLayout) itemView.findViewById(R.id.ll_topic_item);
            userTopic = (TextView) itemView.findViewById(R.id.tv_user_topic);
            topicTitle = (TextView) itemView.findViewById(R.id.tv_user_topic_title);
            topicFave = (ImageView) itemView.findViewById(R.id.iv_topic_fave);
            topicimgs = (ImageView) itemView.findViewById(R.id.topicimgs);
            topicTime = (TextView) itemView.findViewById(R.id.tv_topic_time);
            topicFaveNum = (TextView) itemView.findViewById(R.id.tv_topic_fave_num);
            topicCommentNum = (TextView) itemView.findViewById(R.id.tv_topic_comment_num);
            showImg1 = (ImageView) itemView.findViewById(R.id.img1_square_item);
            showImg2 = (ImageView) itemView.findViewById(R.id.img2_square_item);
            showImg3 = (ImageView) itemView.findViewById(R.id.img3_square_item);
        }
    }
}
