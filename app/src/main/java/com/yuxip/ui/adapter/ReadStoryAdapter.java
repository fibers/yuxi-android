package com.yuxip.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.im.Security;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.StoryContent;
import com.yuxip.R;
import com.yuxip.imservice.entity.ImageMessage;
import com.yuxip.ui.activity.story.ReadActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ly on 2015/8/10.
 * 阅读界面的适配
 */
public class ReadStoryAdapter extends BaseAdapter {

    private List<StoryContent.ContentlistEntity> list = new ArrayList<>();
    private Context context;
    private ArrayList<ImageMessage> listImg = new ArrayList<>();
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;

    public ReadStoryAdapter(Context context, List<StoryContent.ContentlistEntity> list) {
        this.context = context;
        this.list = list;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        loaderOptions = ImageLoaderUtil.getOptions(DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC));
        initImgList();
    }

    public List<StoryContent.ContentlistEntity> getData() {
        return list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_readstory_item, null);
            holder.tv_content = (TextView) convertView.findViewById(R.id.tv_content_adapter_read);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name_adapter_read);
            holder.tv_time = (TextView) convertView.findViewById(R.id.tv_time_adapter_read);
            holder.iv_content = (ImageView) convertView.findViewById(R.id.iv_content_adapter_read);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tv_content.setVisibility(View.VISIBLE);
        holder.iv_content.setVisibility(View.GONE);
//            holder.tv_name.setText(list.get(position).getSenderrolename());
        if (!TextUtils.isEmpty(list.get(position).getSenderrolename()) && !list.get(position).getSenderrolename().equals("[]")) {
            holder.tv_name.setText(list.get(position).getSenderrolename());
        } else {
            holder.tv_name.setText(list.get(position).getSendername());
        }
        final String content = decodeString(list.get(position).getContent());
        holder.iv_content.setTag(position);
        String imgurl = getImageUrl(content);
        if (!TextUtils.isEmpty(imgurl)) {
            holder.tv_content.setVisibility(View.GONE);
            holder.iv_content.setVisibility(View.VISIBLE);
            imageLoader.displayImage(imgurl, holder.iv_content, loaderOptions);
        } else {

            holder.tv_content.setText(content);
        }

        holder.iv_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                String url = getImageUrl(decodeString(list.get(position).getContent()));
                int currentPosition = getCurrentItem(url);
                IMUIHelper.OpenPreviewReadImaActivity(context, listImg, currentPosition + "");
                ((ReadActivity) context).overridePendingTransition(R.anim.zoom_in, R.anim.tt_stay);
            }
        });

        return convertView;
    }

    /***************************
     * 解析获取的字符串信息
     *************/
    private String decodeString(String content) {
        return new String(Security.getInstance().DecryptMsg(content));
    }


    private String getImageUrl(String content) {
        String imgUrl = null;
        if (content.startsWith("&$#@~^@[{:") && content.endsWith(":}]&$~@#@")) {
            int start = content.indexOf(":");
            int end = content.lastIndexOf(":");
            imgUrl = content.substring(start + 1, end);
        }
        return imgUrl;
    }

    private void initImgList() {
        listImg.clear();
        for (int i = 0; i < list.size(); i++) {
            String content = getImageUrl(decodeString(list.get(i).getContent()));
            if (!TextUtils.isEmpty(content)) {
                ImageMessage img = new ImageMessage();
                img.setUrl(content);
                listImg.add(img);
            }
        }
    }


    private int getCurrentItem(String url) {
        if (url == null)
            return -1;
        for (int i = 0; i < listImg.size(); i++) {
            if (listImg.get(i).getUrl().equals(url)) {
                return i;
            }
        }
        return -1;
    }

    class ViewHolder {
        TextView tv_name;
        TextView tv_time;
        TextView tv_content;
        ImageView iv_content;

    }


    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        initImgList();
    }

}
