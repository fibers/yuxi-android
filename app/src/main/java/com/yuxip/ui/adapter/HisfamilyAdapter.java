package com.yuxip.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.entity.FamilyInfoEntity;
import com.yuxip.ui.activity.other.FamilyDetailsActivity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的/所有家族列表的适配器
 * changed by SummerRC on 15/5/19.
 */
public class HisfamilyAdapter extends BaseAdapter {
    private List<FamilyInfoEntity> familylist = new ArrayList<>();
    private Context context;
    private ImageLoader imageLoader;
    public HisfamilyAdapter(Context context, List<FamilyInfoEntity> familylist) {
        this.familylist = familylist;
        this.context = context;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    @Override
    public int getCount() {
        return familylist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final FamilyInfoEntity entity = familylist.get(position);
        ViewHolder holder ;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(parent.getContext(), R.layout.item_his_family, null);
            holder.headImg = (CircularImage) convertView.findViewById(R.id.iv_imgHead);
            holder.familyName = (TextView) convertView.findViewById(R.id.tv_jiazu_name);
            holder.item = (RelativeLayout) convertView.findViewById(R.id.hisfamily_list_item);
            holder.imgVip = (ImageView) convertView.findViewById(R.id.img_vip);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
        DisplayImageOptions displayImageOptions = ImageLoaderUtil.getOptions(drawable);
        imageLoader.displayImage(entity.getPortrait(),holder.headImg,displayImageOptions);
        /*****************      这里是家族的装饰 比如某些vip家族  *****************/
        holder.familyName.setText(entity.getName());
        if (entity.getIsvip().equals("1")) {
            holder.imgVip.setVisibility(View.VISIBLE);
        } else {
            holder.imgVip.setVisibility(View.GONE);
        }

        /**
         * 家族每条的点击事件  跳转家族详情页
         */
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context, FamilyDetailsActivity.class);
                i.putExtra(IntentConstant.SESSION_KEY, "2_" + entity.getId());
                i.putExtra(IntentConstant.FAMILY_INFO_START_FROM, "contact");
                ((Activity)context).startActivityForResult(i,1);
            }
        });

        return convertView;
    }

    public class ViewHolder {
        CircularImage headImg;
        TextView familyName;
        RelativeLayout item;
        ImageView imgVip;
    }
}