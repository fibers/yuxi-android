package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.imservice.service.IMService;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by SummerRC on 2015/6/4.
 * description:群成员adapter
 */
public class StoryGroupAdapter extends BaseAdapter {
    private boolean showAddTag = false;
    private Context context;
    private String storyid;
    private String groupid;
    private List<Members> memberslist;
    private String currentSessionKey;
    private String isplay;
    private String shenheId;

    private IMService imService;

    private int groupCreatorId = -1;

    private boolean removeState = false;
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    public StoryGroupAdapter(Context c, List<Members> memberslist,
                             String storyid,
                             String groupid,
                             Boolean showAddTag,
                             String isplay,
                             String shenheId,
                             int groupCreatorId, IMService imService) {
        this.context = c;
        this.memberslist = memberslist;
        this.storyid = storyid;
        this.groupid = groupid;
        this.showAddTag = showAddTag;
        this.isplay = isplay;
        this.shenheId = shenheId;
        this.groupCreatorId = groupCreatorId;
        this.imService = imService;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        options = ImageLoaderUtil.getOptions(DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT));
    }

    public void setMemberslist(List<Members> memberslist) {
        this.memberslist = memberslist;
        if (removeState)
            removeState = false;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if (null != memberslist) {
            int memberListSize = memberslist.size();
            if (showAddTag) {
                memberListSize = memberListSize + 2;
            }
            return memberListSize;
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (showAddTag) {
            if (position <= getCount() - 2) {
                return memberslist.get(position);
            } else {
                return null;
            }
        } else {
            return memberslist.get(position);
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final GroupHolder holder;
//        if(convertView==null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.tt_group_grid_item, null);
        holder = new GroupHolder();
        holder.imageView = (CircularImage) convertView.findViewById(R.id.groupUserAvatar);
        holder.deleteImg = convertView.findViewById(R.id.deleteLayout);
        holder.role = (ImageView) convertView.findViewById(R.id.grid_item_image_role);

//            convertView.setTag(holder);
//        } else {
//            holder = (GroupHolder)convertView.getTag();
//        }
        if (position >= 0 && memberslist.size() > position) {
            Members meb = memberslist.get(position);
            holder.member = meb;
            holder.imageView.setVisibility(View.VISIBLE);
            imageLoader.displayImage(memberslist.get(position).getPortrait(), holder.imageView, options);
            //判断不是群住隐藏标示
            if (groupCreatorId > 0 && groupCreatorId == Integer.valueOf(meb.getId())) {
                holder.role.setVisibility(View.VISIBLE);
            } else {
                holder.role.setVisibility(View.INVISIBLE);
            }

            if (removeState && Integer.valueOf(meb.getId()) != groupCreatorId) {
                holder.deleteImg.setVisibility(View.VISIBLE);
            } else {
                holder.deleteImg.setVisibility(View.INVISIBLE);
            }
        } else if ((position == memberslist.size()) && showAddTag) {
            holder.role.setVisibility(View.INVISIBLE);
            holder.imageView.setBackgroundResource(R.drawable.tt_group_manager_add_user);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //判断是不是审核群
                    if (shenheId.equals(groupid)) {
                        IMUIHelper.openGroupMemberInviteActivity(context, groupid);
                    } else {
                        IMUIHelper.openInvitedGroupMembersActivity(context, groupid, storyid, shenheId);
                    }
                }
            });
            holder.deleteImg.setVisibility(View.INVISIBLE);
        } else if (position == (memberslist.size() + 1) && showAddTag) {
            holder.role.setVisibility(View.INVISIBLE);
            holder.imageView.setBackgroundResource(R.drawable.delete_member);
            holder.imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleDeleteIcon();
                }
            });
            holder.deleteImg.setVisibility(View.INVISIBLE);
        }

        /**
         * 删除按钮
         */
        holder.deleteImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int userId = Integer.valueOf(holder.member.getId());
                memberslist.remove(holder.member);
                notifyDataSetChanged();

                Set<Integer> removeMemberlist = new HashSet<>(1);
                removeMemberlist.add(userId);
                imService.getGroupManager().reqRemoveGroupMember(Integer.valueOf(groupid), removeMemberlist);

                Toast.makeText(context, "主人，" + holder.member.getNickname() + "被移出群了！", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;

    }

    final class GroupHolder {
        CircularImage imageView;
        View deleteImg;
        ImageView role;
        Members member;
    }

    public void toggleDeleteIcon() {
        removeState = !removeState;
        notifyDataSetChanged();
    }
}
