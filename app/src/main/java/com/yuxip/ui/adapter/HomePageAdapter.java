package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.utils.DateUtil;

import java.util.List;

/**
 * 主页列表
 * changed by SummerRC on 15/5/19.
 */
public class HomePageAdapter extends BaseAdapter {
    private List<HomeInfo.PersoninfoEntity.MyHomeEntity> mystorys;
    private Context context;

    public HomePageAdapter(Context context, List<HomeInfo.PersoninfoEntity.MyHomeEntity> mystorys) {
        this.mystorys = mystorys;
        this.context = context;
    }

    public List<HomeInfo.PersoninfoEntity.MyHomeEntity> getMystorys() {
        return mystorys;
    }

    @Override
    public int getCount() {
        return mystorys.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        HomeInfo.PersoninfoEntity.MyHomeEntity entity = mystorys.get(position);
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = View.inflate(parent.getContext(), R.layout.item_home_page, null);
            holder.home_title = (TextView) convertView.findViewById(R.id.home_title);
            holder.home_time = (TextView) convertView.findViewById(R.id.home_time);
            holder.item = (RelativeLayout) convertView.findViewById(R.id.hisfamily_list_item);
            holder.home_creator= (TextView) convertView.findViewById(R.id.home_creater);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String creattime = entity.getCreatetime();
        String time = DateUtil.getSessionTime(Integer.parseInt(creattime));
        holder.home_title.setText(entity.getTitle());
        holder.home_time.setText(time);
        holder.home_creator.setText(entity.getCreatorname());
        return convertView;
    }

    public class ViewHolder {
        TextView home_title;
        TextView home_time;
        RelativeLayout item;
        TextView home_creator;
    }
}