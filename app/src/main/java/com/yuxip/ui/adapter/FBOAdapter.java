package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.R;
import com.yuxip.entity.FamilyInfoEntity;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by HeTianpeng on 2015/07/17.
 */
public class FBOAdapter extends BaseRecyclerViewAdapter {

    private List<FamilyInfoEntity> list;

    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    public FBOAdapter(List<FamilyInfoEntity> list, Context context) {
        super(context);
        this.list = list;
        imageLoader= ImageLoaderUtil.getImageLoaderInstance();
        Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
        displayImageOptions = ImageLoaderUtil.getOptions(drawable);
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_all_msg, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getAdapterItemCount() {
        return null == list || list.isEmpty() ? 0 : list.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return 0;
    }

    @Override
    public void onBinddViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getAdapterItemCount() == 0) {
            return;
        }

        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;


            FamilyInfoEntity entity = list.get(position);
//            BitmapUtils bitmapUtils = new BitmapUtils(context);
//            bitmapUtils.configDefaultLoadFailedImage(R.drawable.default_family_avatar_btn);
//            bitmapUtils.configDefaultLoadingImage(R.drawable.default_family_avatar_btn);//默认背景图片
//            bitmapUtils.display(viewHolder.ivImgHead, entity.getPortrait());
            imageLoader.displayImage(entity.getPortrait(),viewHolder.ivImgHead,displayImageOptions);
            viewHolder.tvJiazuName.setText(entity.getName());
            viewHolder.tvTitle.setText(entity.getIntro());
            viewHolder.tvFamilyLeader.setText(entity.getCreatorname());
            viewHolder.tvPersonNum.setText(entity.getNumber());

            if (entity.getIsvip().equals("1")) {
               viewHolder.imgVip.setVisibility(View.VISIBLE);
            } else {
               viewHolder.imgVip.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_all_msg.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends UltimateRecyclerviewViewHolder{
        @InjectView(R.id.iv_imgHead)
        CircularImage ivImgHead;
        @InjectView(R.id.tv_jiazu_name)
        TextView tvJiazuName;
        @InjectView(R.id.tv_zuzhang)
        TextView tvZuzhang;
        @InjectView(R.id.tv_family_leader)
        TextView tvFamilyLeader;
//        @InjectView(R.id.tv_right)
//        TextView tvRight;
        @InjectView(R.id.tv_person_num)
        TextView tvPersonNum;
        @InjectView(R.id.tv_title)
        TextView tvTitle;
        @InjectView(R.id.img_vip)
        ImageView imgVip;

        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
