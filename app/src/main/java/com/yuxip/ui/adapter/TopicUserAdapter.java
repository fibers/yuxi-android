package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.TopicDetailResult;
import com.yuxip.R;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by Administrator on 2015/6/4.
 */
public class TopicUserAdapter extends BaseAdapter {
    private List<TopicDetailResult.TopicdetailEntity.UserlistEntity> userlist;
    private Context context;
    private ImageLoader imageLoader;

    public TopicUserAdapter(Context c, List<TopicDetailResult.TopicdetailEntity.UserlistEntity> userlist) {
        this.context = c;
        this.userlist = userlist;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    @Override
    public int getCount() {
        if (null != userlist) {
            int memberListSize = userlist.size();
            return memberListSize;
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        final GroupHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_topic_content_user, null);
            holder = new GroupHolder();
            holder.userHead = (CircularImage) convertView.findViewById(R.id.userHead);
            convertView.setTag(holder);
        } else {
            holder = (GroupHolder) convertView.getTag();
        }

        Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
        DisplayImageOptions imageOptions = ImageLoaderUtil.getOptions(drawable);
        imageLoader.displayImage(userlist.get(position).getPortrait(), holder.userHead, imageOptions);
        holder.userHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openUserHomePageActivity(context, userlist.get(position).getUserid());
            }
        });
        return convertView;
    }


    final class GroupHolder {
        CircularImage userHead;
    }

}
