package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.SquareListJsonBean;
import com.yuxip.R;
import com.yuxip.config.SysConstant;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

/**
 * Created by ZQF on 2015/10/13.
 * 广场的适配器 10.13改版
 */
public class SquareListAdapter extends BaseAdapter {

    private Context context;
    private ImageLoader imageLoader;
    private List<SquareListJsonBean.SquareListEntity> squareList;

    public SquareListAdapter(Context context, List<SquareListJsonBean.SquareListEntity> squareList) {
        this.squareList = squareList;
        this.context = context;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }


    @Override
    public int getCount() {
        if (squareList == null) {
            return 0;
        }
        return squareList.size();
//        return 6;
    }

    @Override
    public Object getItem(int position) {
        return squareList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_square_list_item_zz, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        SquareListJsonBean.SquareListEntity entity = squareList.get(position);
        holder.vHeadIv.loadImage(entity.getCreator().getPortrait(), "0");
        holder.vUsernameTv.setText(entity.getCreator().getNickName());
        String timeStr = entity.getCreateTime();
        //根据返回的时间戳转换成时间
        String createTime = DateUtil.getDateWithOutYear(Integer.parseInt(timeStr));
        holder.vTimeTv.setText(createTime);
        holder.vFaveTv.setText(entity.getFavorCount());
        holder.vCommentNumTv.setText(entity.getCommentCount());
        holder.vTitleTv.setText(entity.getTitle());
        holder.vContentTv.setText(entity.getContent());
        holder.vPicsLl.getChildAt(0).setVisibility(View.GONE);
        holder.vPicsLl.getChildAt(1).setVisibility(View.GONE);
        holder.vPicsLl.getChildAt(2).setVisibility(View.GONE);
        List<String> imags = entity.getImages();
        if (imags != null && imags.size() > 0) {
            switch (imags.size()) {
                default:
                    imageLoader.displayImage(imags.get(2) + SysConstant.PICTURE_58X58, (ImageView) holder.vPicsLl.getChildAt(2));
                    holder.vPicsLl.getChildAt(2).setVisibility(View.VISIBLE);
                case 2:
                    imageLoader.displayImage(imags.get(1) + SysConstant.PICTURE_58X58, (ImageView) holder.vPicsLl.getChildAt(1));
                    holder.vPicsLl.getChildAt(1).setVisibility(View.VISIBLE);
                case 1:
                    imageLoader.displayImage(imags.get(0) + SysConstant.PICTURE_58X58, (ImageView) holder.vPicsLl.getChildAt(0));
                    holder.vPicsLl.getChildAt(0).setVisibility(View.VISIBLE);
                    break;
            }
        }

//
//        holder.vTitleTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, ReleaseCommentActivity.class);
//                context.startActivity(intent);
//            }
//        });
//        // 广场通知临时入口
//        holder.vContentTv.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, SquareMessageActivity.class);
//                context.startActivity(intent);
//            }
//        });
        return convertView;
    }

    static class ViewHolder {
        CustomHeadImage vHeadIv;
        TextView vUsernameTv;
        TextView vTimeTv;
        TextView vFaveTv;
        TextView vCommentNumTv;
        TextView vTitleTv;
        TextView vContentTv;
        LinearLayout vPicsLl;

        public ViewHolder(View view) {
            vHeadIv = (CustomHeadImage) view.findViewById(R.id.iv_square_item_head_img);
            vUsernameTv = (TextView) view.findViewById(R.id.tv_square_item_username);
            vTimeTv = (TextView) view.findViewById(R.id.tv_square_item_time);
            vFaveTv = (TextView) view.findViewById(R.id.tv_square_item_fave_num);
            vCommentNumTv = (TextView) view.findViewById(R.id.tv_square_item_comment_num);
            vTitleTv = (TextView) view.findViewById(R.id.tv_square_item_title);
            vContentTv = (TextView) view.findViewById(R.id.tv_square_item_content);
            vPicsLl = (LinearLayout) view.findViewById(R.id.ll_square_item_pics);
            vHeadIv.setVipSize(12f);
        }
    }
}
