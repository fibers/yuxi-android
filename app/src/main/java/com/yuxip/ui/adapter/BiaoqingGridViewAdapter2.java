package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.ui.widget.FaceInputView;

import java.util.List;

/**
 * Created by Administrator on 2015/8/6.
 */
public class BiaoqingGridViewAdapter2 extends BaseAdapter {
    private FaceInputView myFaceInputView;
    private Context context;
    private List<String> list;

    public BiaoqingGridViewAdapter2(FaceInputView myFaceInputView, Context context, List<String> list) {
        this.myFaceInputView = myFaceInputView;
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.face_input_view_gridview_item2, null);
            viewHolder.text = (TextView) convertView.findViewById(R.id.tv_face_grid_item2);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.text.setText(list.get(position));
        return convertView;
    }


    class ViewHolder {
        TextView text;
    }
}
