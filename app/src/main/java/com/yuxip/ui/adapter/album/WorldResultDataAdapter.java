package com.yuxip.ui.adapter.album;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.adapter.BaseRecyclerViewAdapter;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Administrator on 2015/7/1.
 * description:剧、自戏列表的适配器
 */
public class WorldResultDataAdapter extends BaseRecyclerViewAdapter {
    private List<HotBean.StorysEntity> storyslist;
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;
    public WorldResultDataAdapter(Context context, List<HotBean.StorysEntity> storyslist) {
        super(context);
        this.storyslist = storyslist;
        initImageLoader();
    }

    private void initImageLoader(){
        imageLoader= ImageLoaderUtil.getImageLoaderInstance();
        Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC);
        loaderOptions = ImageLoaderUtil.getOptions(drawable);
    }

    public void addAll(List<HotBean.StorysEntity> storysEntities) {
        if (null != storysEntities && storyslist != null) {
            storyslist.addAll(storysEntities);
        }
    }

    public List<HotBean.StorysEntity> getData() {
        return storyslist;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public void onBinddViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        if (viewHolder instanceof WorldResultDataAdapter.ViewHolder) {
            WorldResultDataAdapter.ViewHolder holder = (ViewHolder) viewHolder;

            final HotBean.StorysEntity storysEntity = storyslist.get(position);
            holder.fl_hot.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(storysEntity.getStoryimg())) {
                holder.fl_hot.setVisibility(View.VISIBLE);
                imageLoader.displayImage(storysEntity.getStoryimg() + SysConstant.PICTURE_114_95, holder.imgBgHot,loaderOptions);
            }

            holder.imgIconHot.setVipSize(12f);
            holder.imgIconHot.loadImage(storysEntity.getCreatorportrait(),"0");
            if(storysEntity.getType().equals(ConstantValues.STORY_TYPE_ZIXI)){
                holder.tvNameHot.setText(storysEntity.getCreatorname()+"的自戏");
                holder.tvLable.setVisibility(View.INVISIBLE);
                holder.iv_go_group.setVisibility(View.INVISIBLE);
            } else {
                holder.tvNameHot.setText(storysEntity.getCreatorname() + "的剧");
                holder.tvLable.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(storysEntity.getCheckgroupid())&&!storysEntity.getCheckgroupid().equals("@")) {
                    holder.iv_go_group.setVisibility(View.VISIBLE);
                    //进群按钮
                    holder.iv_go_group.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            boolean is_admin = storysEntity.getCreatorid().equals(String.valueOf(IMLoginManager.instance().getLoginId()));
                            IMUIHelper.openStoryGroupSelectActivity(context, storysEntity.getStoryid(), is_admin, storysEntity.getCheckgroupid(), storysEntity.getCheckgrouptitle());
                        }
                    });
                } else {                //没有审核群
                    holder.iv_go_group.setVisibility(View.INVISIBLE);
                }
            }
            String categrory = storysEntity.getCategory();
            holder.tvLable.setText(categrory);
            holder.tvTitleHot.setText(storysEntity.getTitle());
            holder.tvInfoHot.setText(storysEntity.getIntro());
            if(!TextUtils.isEmpty(storysEntity.getUpdatetime())){
                holder.tvTimeHot.setText(DateUtil.getDateWithOutYear(Integer.parseInt(storysEntity.getUpdatetime())));
            }else{
                holder.tvTimeHot.setText(DateUtil.getDateWithOutYear(Integer.parseInt(storysEntity.getCreatetime())));
            }
            float numWord = Float.parseFloat(storysEntity.getWordcounts());
            if (numWord >= 10000) {
                float n = numWord / 10000;
                holder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "w");
            } else if (numWord >= 1000) {
                float n = numWord / 1000;
                holder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "k");
            } else {
                holder.tvNumOfWordsHot.setText("字数：" + (int) numWord);
            }
            holder.tvTopicFaveNum.setText(storysEntity.getPraisenum());
            holder.tvTopicCommentNum.setText(storysEntity.getCommentnum());
            //每条点击事件
            holder.rlWorldItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //判断故事还是剧
                    if (storysEntity.getType().equals(ConstantValues.STORY_TYPE_ZIXI)) {
                        Intent intent = new Intent(context, ZiXiDetailsActivity.class);
                        intent.putExtra(IntentConstant.STORY_ID, storysEntity.getStoryid());
                        intent.putExtra(IntentConstant.CREATOR_ID, storysEntity.getCreatorid());
                        context.startActivity(intent);
                    } else {
                        IMUIHelper.openStoryDetailsActivity(context, storysEntity.getStoryid());
                    }
                }
            });
            //头像点击事件查看好友资料
            holder.imgIconHot.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openUserHomePageActivity(context, storysEntity.getCreatorid());
                }
            });
        }
    }

    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View v = LayoutInflater.from(context).inflate(R.layout.adapter_story_list_item, null);
        WorldResultDataAdapter.ViewHolder holder = new ViewHolder(v);
        return holder;
    }

    @Override
    public int getAdapterItemCount() {
        return null == storyslist || storyslist.isEmpty() ? 0 : storyslist.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int i) {

    }

    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'adapter_story_list_item.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder extends UltimateRecyclerviewViewHolder {
        @InjectView(R.id.rl_world_item)
        RelativeLayout rlWorldItem;
        @InjectView(R.id.img_bg_hot)
        ImageView imgBgHot;
        @InjectView(R.id.fl_hot)
        FrameLayout fl_hot;
        @InjectView(R.id.iv_go_group)
        ImageView iv_go_group;
        @InjectView(R.id.tv_title_hot)
        TextView tvTitleHot;
        @InjectView(R.id.tv_info_hot)
        TextView tvInfoHot;
        @InjectView(R.id.tv_time_hot)
        TextView tvTimeHot;
        @InjectView(R.id.tv_num_of_words_hot)
        TextView tvNumOfWordsHot;
        @InjectView(R.id.iv_topic_fave)
        ImageView ivTopicFave;
        @InjectView(R.id.tv_topic_fave_num)
        TextView tvTopicFaveNum;
        @InjectView(R.id.img_comment_hot)
        ImageView imgCommentHot;
        @InjectView(R.id.tv_topic_comment_num)
        TextView tvTopicCommentNum;
        @InjectView(R.id.hot_user_head_img)
        CustomHeadImage imgIconHot;
        @InjectView(R.id.tv_name_hot)
        TextView tvNameHot;
        @InjectView(R.id.tv_label_recommand_adapter)
        TextView tvLable;
        ViewHolder(View view) {
            super(view);
            ButterKnife.inject(this, view);
        }
    }
}
