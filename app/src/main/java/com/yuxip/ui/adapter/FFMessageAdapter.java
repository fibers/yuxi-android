package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.R;
import com.yuxip.config.DBConstant;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;

import java.util.List;

/**
 * 消息列表最近联系人列表适配器
 * create by SummerRC at 15/06/06
 */
public class FFMessageAdapter extends BaseAdapter {
    private LayoutInflater mInflater = null;
    private List<RecentInfo> recentInfoList;
    private Logger logger = Logger.getLogger(FFMessageAdapter.class);
    private Context context;

    public FFMessageAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setData(List<RecentInfo> recentInfoList, Context context) {
        this.context = context;
        logger.d("recent#set New recent session list");
        logger.d("recent#notifyDataSetChanged");
        if (recentInfoList == null) {
            return;
        }
        this.recentInfoList = recentInfoList;
        notifyDataSetChanged();
    }


    /**
     * 更新单个RecentInfo 屏蔽群组信息
     */
    public void updateRecentInfoByShield(GroupEntity entity) {
        String sessionKey = entity.getSessionKey();
        for (RecentInfo recentInfo : recentInfoList) {
            if (recentInfo.getSessionKey().equals(sessionKey)) {
                int status = entity.getStatus();
                boolean isFor = status == DBConstant.GROUP_STATUS_SHIELD;
                recentInfo.setForbidden(isFor);
                notifyDataSetChanged();
                break;
            }
        }
    }

    public List<RecentInfo> getData() {
        return this.recentInfoList;
    }

    @Override
    public int getCount() {
        return null == recentInfoList || recentInfoList.isEmpty() ? 0 : recentInfoList.size();
    }

    /**
     * 基本HOLDER
     */
    private static class ContactHolderBase {
        public TextView username;
        public TextView lastContent;
        public TextView lastTime;
        public TextView msgCount;
        public ImageView noDisturb;
    }

    /**
     * 用户HOLDER
     */
    private final class ContactViewHolder extends ContactHolderBase {
        public CustomHeadImage avatar;
        public CheckBox checkBox;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return renderUser(position, convertView, parent);
    }

    /**
     * 渲染好友、评论群、系统消息等
     *
     * @param position    position
     * @param convertView convertView
     * @param parent      parent
     * @return View
     */
    private View renderUser(int position, View convertView, ViewGroup parent) {
        ContactViewHolder holder;
        if (null == convertView) {
            convertView = mInflater.inflate(R.layout.tt_item_chat_single_x, parent, false);
            holder = new ContactViewHolder();
            holder.avatar = (CustomHeadImage) convertView.findViewById(R.id.messageImg);
            holder.username = (TextView) convertView.findViewById(R.id.shop_name);
            holder.lastContent = (TextView) convertView.findViewById(R.id.message_body);
            holder.lastTime = (TextView) convertView.findViewById(R.id.message_time);
            holder.msgCount = (TextView) convertView.findViewById(R.id.message_count_notify);
            holder.noDisturb = (ImageView) convertView.findViewById(R.id.message_time_no_disturb_view);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_tt_item_chat);
            convertView.setTag(holder);
        } else {
            holder = (ContactViewHolder) convertView.getTag();
        }

        if (!recentInfoList.isEmpty() && position < getCount()) {
            RecentInfo recentInfo = this.recentInfoList.get(position);
            handleUserContact(holder, recentInfo);
        }
        return convertView;
    }

    private void handleUserContact(ContactViewHolder contactViewHolder, RecentInfo recentInfo) {
        /** 设置未读消息计数 */
        int msgCount = recentInfo.getUnReadCnt();
        if (msgCount > 0) {
            String strCountString = String.valueOf(msgCount);
            if (msgCount > 99) {
                strCountString = "99+";
            }
            contactViewHolder.msgCount.setVisibility(View.VISIBLE);
            contactViewHolder.msgCount.setText(strCountString);
            contactViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.pink));
        } else {
            contactViewHolder.msgCount.setVisibility(View.GONE);
            contactViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.message_time_color_h));
        }

        /** 设置头像 */
        String avatarUrl = "";
        if (null != recentInfo.getAvatar() && recentInfo.getAvatar().size() > 0) {
            avatarUrl = recentInfo.getAvatar().get(0);
        }
        DisplayImageOptions options;
        Drawable drawable;
        switch (recentInfo.getSessionType()) {
            case DBConstant.SESSION_TYPE_SINGLE:
                drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
                options = ImageLoaderUtil.getOptions(drawable);
                break;
            case DBConstant.SESSION_TYPE_GROUP:
                drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
                options = ImageLoaderUtil.getOptions(drawable);
                break;
            case DBConstant.SESSION_TYPE_SYSTEM:
                options = ImageLoaderUtil.getOptions(R.drawable.comment_message);
                break;
            default:
                drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
                options = ImageLoaderUtil.getOptions(drawable);
                break;
        }
        contactViewHolder.avatar.configImageOptions(options);
        contactViewHolder.avatar.setVipSize(12F);
        contactViewHolder.avatar.loadImage(avatarUrl);

        /** 设置其它信息 */
        contactViewHolder.username.setText(recentInfo.getName());
        contactViewHolder.lastContent.setText(recentInfo.getLatestMsgData());
        int time = recentInfo.getUpdateTime();
        contactViewHolder.lastTime.setText(DateUtil.getSessionTime(time));
        if (recentInfo.getSessionType() == DBConstant.SESSION_TYPE_SYSTEM) {
            if (recentInfo.getUpdateTime() <= 0) {
                contactViewHolder.lastTime.setText("");
            }
        }
    }

    @Override
    public RecentInfo getItem(int position) {
        return recentInfoList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

}
