package com.yuxip.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.yuxip.JsonBean.StoryRolesJsonBean;
import com.yuxip.R;
import com.yuxip.ui.activity.other.GroupLabelActivity;
import com.yuxip.ui.activity.other.GroupLableDetailActivity;
import com.yuxip.ui.customview.NoScrollGridView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/8/12.
 */
public class GroupLabelAdapter extends BaseAdapter {
    private List<StoryRolesJsonBean.RolesEntity> listRoles = new ArrayList<>();
    private Context context;
    /**
     * 用以存放textview 的集合
     */
//    private List<TextView> listText=new ArrayList<>();
    private NoScrollGridView gridView;


    public GroupLabelAdapter(Context context, List<StoryRolesJsonBean.RolesEntity> listRoles) {
        this.context = context;
        this.listRoles = listRoles;
    }

    public void setGridView(NoScrollGridView gridView) {
        this.gridView = gridView;
    }

    @Override
    public int getCount() {
        return listRoles.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_gv_pop_grouplabel, null);
            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name_gv_pop_gropulabel);
//            listText.add(holder.tv_name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        holder.tv_name.setText(listRoles.get(position).getTitle());
        if (listRoles.get(position).getId().equals(GroupLabelActivity.roleId)) {
            holder.tv_name.setSelected(true);
//            holder.tv_name.setTextColor(context.getResources().getColor(R.color.white));
        }

        holder.tv_name.setTag(position);
        holder.tv_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag();
                setTextSelect(position);
                Toast.makeText(context, listRoles.get(position).getTitle(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(context, GroupLableDetailActivity.class);
                intent.putExtra("num", listRoles.get(position).getNum());
                intent.putExtra("title", listRoles.get(position).getTitle());
                intent.putExtra("content", listRoles.get(position).getIntro());
                intent.putExtra("name", listRoles.get(position).getTitle());
                intent.putExtra("roleid", listRoles.get(position).getId());
                ((GroupLabelActivity) context).startActivityForResult(intent, 1);

            }
        });

        return convertView;
    }

    private void setTextSelect(int position) {
//        for(int i=0;i<listText.size();i++){
//            listText.get(i).setSelected(false);
////            listText.get(i).setTextColor(context.getResources().getColor(R.color.selector_water_textcolor));
//        }
//        listText.get(position).setSelected(true);
////        listText.get(position).setTextColor(context.getResources().getColor(R.color.white));
        for (int i = 0; i < gridView.getChildCount(); i++) {
            gridView.getChildAt(i).findViewById(R.id.tv_name_gv_pop_gropulabel).setSelected(false);
        }
        gridView.getChildAt(position).findViewById(R.id.tv_name_gv_pop_gropulabel).setSelected(true);
    }


    class ViewHolder {
        TextView tv_name;
    }
}
