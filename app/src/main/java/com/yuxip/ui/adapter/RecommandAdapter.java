package com.yuxip.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.RecommandJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.fragment.story.RecommandFragment;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/19.
 * description:精选剧列表的适配器
 */
public class RecommandAdapter extends BaseAdapter {
    private Context context;
    private List<RecommandJsonBean.SelectedcontentsEntity.StorysEntity> list = new ArrayList<>();
    private List<RecommandFragment.RecommandEntity> list_recommand = new ArrayList<>();
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;      //图片的

    public RecommandAdapter(Context context, List<RecommandJsonBean.SelectedcontentsEntity.StorysEntity> list) {
        this.list = list;
        this.context = context;
        initImageLoader();
    }

    private void initImageLoader() {
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
        loaderOptions=ImageLoaderUtil.getOptions(DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC));
    }

    public void setTypeData(List<RecommandFragment.RecommandEntity> list_recommand) {
        this.list_recommand = list_recommand;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.adapter_recommand_list_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final RecommandJsonBean.SelectedcontentsEntity.StorysEntity storysEntity = list.get(position);
        holder.fl_hot.setVisibility(View.GONE);
        holder.rlRecommandTop.setVisibility(View.GONE);

        for (int i = 0; i < list_recommand.size(); i++) {
            if (position == list_recommand.get(i).getPosition()) {
                holder.rlRecommandTop.setVisibility(View.VISIBLE);
                holder.recommandTitle.setText(list_recommand.get(i).getTitle());
                holder.recommandMore.setTag(i);
                holder.recommandMore.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        Intent intent = new Intent();
                        if (list_recommand.get(_position).getType().startsWith("3")) {
                            intent.putExtra(ConstantValues.RECOMMAND_TYPE, ConstantValues.RECOMMAND_PLAY);
                        } else {
                            intent.putExtra(ConstantValues.RECOMMAND_TYPE, ConstantValues.RECOMMAND_STORY);
                        }

                        intent.putExtra(ConstantValues.RECOMMAND_TYPE_JUMP, list_recommand.get(_position).getType());
                        intent.setAction(ConstantValues.BROADCAST_REFRESH_RECOMMAND);
                        context.sendBroadcast(intent);
                    }
                });
            }
        }


        if (!TextUtils.isEmpty(storysEntity.getStoryimg())) {
            holder.fl_hot.setVisibility(View.VISIBLE);
            imageLoader.displayImage(storysEntity.getStoryimg() + SysConstant.PICTURE_114_95, holder.imgBgHot, loaderOptions);
        }

        holder.imgIconHot.setVipSize(12f);
        holder.iv_go_group.setVisibility(View.INVISIBLE);
        holder.imgIconHot.loadImage(storysEntity.getCreatorportrait(), "0");
        if (storysEntity.getType().equals(ConstantValues.STORY_TYPE_ZIXI)) {
            holder.tvNameHot.setText(storysEntity.getCreatorname() + "的自戏");
            holder.tvLable.setVisibility(View.INVISIBLE);
            holder.iv_go_group.setVisibility(View.INVISIBLE);

        } else {
            holder.tvNameHot.setText(storysEntity.getCreatorname() + "的剧");
            holder.tvLable.setVisibility(View.VISIBLE);
            if (!storysEntity.getCheckgroupid().equals("@")) {
                holder.iv_go_group.setVisibility(View.VISIBLE);
                //进群按钮
                holder.iv_go_group.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean is_admin = storysEntity.getCreatorid().equals(String.valueOf(IMLoginManager.instance().getLoginId()));
                        IMUIHelper.openStoryGroupSelectActivity(context, storysEntity.getStoryid(), is_admin, storysEntity.getCheckgroupid(), storysEntity.getCheckgrouptitle());
                    }
                });
            } else {        //没有审核群
                holder.iv_go_group.setVisibility(View.INVISIBLE);
            }
        }
        String categrory = storysEntity.getCategory();
        holder.tvLable.setText(categrory);
        holder.tvTitleHot.setText(storysEntity.getTitle());
        holder.tvInfoHot.setText(storysEntity.getIntro());
//        holder.tvTimeHot.setText(DateUtil.getDateWithOutYear(Integer.parseInt(storysEntity.getCreatetime())));
        if(!TextUtils.isEmpty(storysEntity.getUpdatetime())){
            holder.tvTimeHot.setText(DateUtil.getDateWithOutYear(Integer.parseInt(storysEntity.getUpdatetime())));
        }else{
            holder.tvTimeHot.setText(DateUtil.getDateWithOutYear(Integer.parseInt(storysEntity.getCreatetime())));
        }
        float numWord = Float.parseFloat(storysEntity.getWordcounts());
        if (numWord >= 10000) {
            float n = numWord / 10000;
            holder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "w");

        } else if (numWord >= 1000) {
            float n = numWord / 1000;
            holder.tvNumOfWordsHot.setText("字数：" + String.format("%.1f", n) + "k");
        } else {
            holder.tvNumOfWordsHot.setText("字数：" + (int) numWord);
        }
        holder.tvTopicFaveNum.setText(storysEntity.getPraisenum());
        holder.tvTopicCommentNum.setText(storysEntity.getCommentnum());
        //每条点击事件
        holder.rlWorldItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断故事还是剧
                if (storysEntity.getType().equals(ConstantValues.STORY_TYPE_ZIXI)) {
                    Intent intent = new Intent(context, ZiXiDetailsActivity.class);
                    intent.putExtra(IntentConstant.STORY_ID, storysEntity.getStoryid());
                    intent.putExtra(IntentConstant.CREATOR_ID, storysEntity.getCreatorid());
                    context.startActivity(intent);
                } else {
                    IMUIHelper.openStoryDetailsActivity(context, storysEntity.getStoryid());
                }
            }
        });
        //头像点击事件查看好友资料
        holder.imgIconHot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openUserHomePageActivity(context, storysEntity.getCreatorid());
            }
        });


        return convertView;
    }


    class ViewHolder {
        @InjectView(R.id.tv_recommand_world_item)
        TextView recommandTitle;
        @InjectView(R.id.tv_more_viewhead_recommand)
        TextView recommandMore;
        @InjectView(R.id.rel_top_world_item)
        RelativeLayout rlRecommandTop;
        @InjectView(R.id.rl_world_item)
        RelativeLayout rlWorldItem;
        @InjectView(R.id.img_bg_hot)
        ImageView imgBgHot;
        @InjectView(R.id.fl_hot)
        FrameLayout fl_hot;
        @InjectView(R.id.iv_go_group)
        ImageView iv_go_group;
        @InjectView(R.id.tv_title_hot)
        TextView tvTitleHot;
        @InjectView(R.id.tv_info_hot)
        TextView tvInfoHot;
        @InjectView(R.id.tv_time_hot)
        TextView tvTimeHot;
        @InjectView(R.id.tv_num_of_words_hot)
        TextView tvNumOfWordsHot;
        @InjectView(R.id.iv_topic_fave)
        ImageView ivTopicFave;
        @InjectView(R.id.tv_topic_fave_num)
        TextView tvTopicFaveNum;
        @InjectView(R.id.img_comment_hot)
        ImageView imgCommentHot;
        @InjectView(R.id.tv_topic_comment_num)
        TextView tvTopicCommentNum;
        @InjectView(R.id.hot_user_head_img)
        CustomHeadImage imgIconHot;
        @InjectView(R.id.tv_name_hot)
        TextView tvNameHot;
        @InjectView(R.id.tv_label_recommand_adapter)
        TextView tvLable;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }

}
