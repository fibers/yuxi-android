package com.yuxip.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.yuxip.DB.entity.ExpressionEntity;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.ui.widget.FaceInputView;
import com.yuxip.utils.IMUIHelper;

import java.util.List;

/**
 * 用户自定义表情用的是GridView这个控件，这个类是相应的适配器
 * Created by SRC on 2015/4/13.
 */
public class BiaoqingGridViewAdapter extends BaseAdapter {
    private FaceInputView myFaceInputView;
    private Context context;
    private List<ExpressionEntity> list;
    private boolean isLastPager;     //如果是最后一页，则存在加号
    private int count;               //表情个数+1（因为存在一个加号)

    public BiaoqingGridViewAdapter(FaceInputView myFaceInputView, Context context, List<ExpressionEntity> list, boolean isLastPager) {
        this.myFaceInputView = myFaceInputView;
        this.context = context;
        this.list = list;
        this.isLastPager = isLastPager;
    }

    @Override
    public int getCount() {
        if (list == null) {
            count = 1;
        } else {
            if (isLastPager) {
                count = list.size() + 1;
            } else {
                count = list.size();
            }

        }
        return count;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(R.layout.face_input_view_gridview_item, null);
            holder.bt_biaoqing = (Button) convertView.findViewById(R.id.bt_biaoqing);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        /** 只有最后一页才有加号 */
        if (position == count - 1 && isLastPager) {
            holder.bt_biaoqing.setBackgroundResource(R.drawable.xukuang);
            holder.bt_biaoqing.setText("+");
            holder.bt_biaoqing.setTextSize(18);
            holder.bt_biaoqing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //myFaceInputView.refresh(position + "");
//                    Intent intent = new Intent(context, AddOrEditDoActivity.class);
//                    context.startActivity(intent);
                    IMUIHelper.openAddOrEditDoActivity(context, "", "", IntentConstant.TYPE_ADD);
                }
            });
        } else {
            holder.bt_biaoqing.setText(list.get(position).getLabel());
            holder.bt_biaoqing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    myFaceInputView.addBiaoqing(list.get(position).getContent());
                }
            });
            holder.bt_biaoqing.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    IMUIHelper.openAddOrEditDoActivity(context, list.get(position).getLabel(), list.get(position).getContent(), IntentConstant.TYPE_EDIT);
                    return true;
                }
            });
        }
        return convertView;
    }


    private static class ViewHolder {
        private Button bt_biaoqing;
    }
}
