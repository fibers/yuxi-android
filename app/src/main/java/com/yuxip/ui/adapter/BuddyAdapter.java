package com.yuxip.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.HisplayResult;
import com.yuxip.R;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.LinkedList;
import java.util.List;

public class BuddyAdapter extends BaseExpandableListAdapter {
    private LinkedList<HisplayResult.StorysEntity> groupArray;
    private List<LinkedList<HisplayResult.StorysEntity.GroupsEntity>> childArray;
    private Context context;
    private LayoutInflater inflater;
    private ImageLoader imageLoader;
    public BuddyAdapter(Context context, LinkedList<HisplayResult.StorysEntity> courseGroupList,
                        List<LinkedList<HisplayResult.StorysEntity.GroupsEntity>> childArray) {
        inflater = ((Activity) context).getLayoutInflater();
        this.context = context;
        this.groupArray = courseGroupList;
        this.childArray = childArray;
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    public Object getChild(int groupPosition, int childPosition) {
        return childArray.get(groupPosition).get(childPosition);
    }

    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    public View getChildView(int groupPosition, int childPosition, boolean arg2, View convertView,
                             ViewGroup arg4) {
        convertView = inflater.inflate(R.layout.buddy_listview_child_item, null);
        //设置剧的名字
        TextView nickTextView = (TextView) convertView.findViewById(R.id.buddy_listview_child_nick);
        nickTextView.setText(childArray.get(groupPosition).get(childPosition).getTitle());
        return convertView;
    }

    public int getChildrenCount(int groupPosition) {
        return childArray.get(groupPosition).size();
    }

    public Object getGroup(int groupPosition) {
        return groupArray.get(groupPosition);
    }

    public int getGroupCount() {
        return groupArray.size();
    }

    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup arg3) {
        convertView = inflater.inflate(R.layout.buddy_listview_group_item, null);
        TextView groupNameTextView = (TextView) convertView.findViewById(R.id.buddy_listview_group_name);
        CircularImage image = (CircularImage) convertView.findViewById(R.id.buddy_listview_image);
        //设置群的标题
        groupNameTextView.setText(groupArray.get(groupPosition).getTitle());
        imageLoader.displayImage(groupArray.get(groupPosition).getPortrait(),image);
        return convertView;
    }

    public boolean hasStableIds() {
        return true;
    }

    // 子选项是否可以选择    
    public boolean isChildSelectable(int arg0, int arg1) {
        // TODO Auto-generated method stub  
        return true;
    }
} 