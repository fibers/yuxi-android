package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuxip.JsonBean.FG_FriendJsonBean;
import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.R;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.IMUIHelper;

import java.util.ArrayList;
import java.util.List;

public class MyExpandableListAdapter extends BaseExpandableListAdapter {
    public List<FG_GroupJsonBean> group = new ArrayList<>();
    public List<List<FG_FriendJsonBean>> child = new ArrayList<>();
    public Context context;

    public MyExpandableListAdapter(Context context) {
        this.context = context;
    }

    public void setData(List<FG_GroupJsonBean> group, List<List<FG_FriendJsonBean>> child) {
        this.group = group;
        this.child = child;
        notifyDataSetChanged();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder2 viewHolder2;
        if (convertView == null) {
            viewHolder2 = new ViewHolder2();
            convertView = View.inflate(context, R.layout.activity_fg_child, null);
            viewHolder2.iv_child = (CustomHeadImage) convertView.findViewById(R.id.iv_child);
            viewHolder2.tv_child = (TextView) convertView.findViewById(R.id.tv_child);
            convertView.setTag(viewHolder2);
        } else {
            viewHolder2 = (ViewHolder2) convertView.getTag();
        }
        if(childPosition >= getChildrenCount(groupPosition)) {
            return new View(context);
        }
        String name = child.get(groupPosition).get(childPosition).getMarkname();
        if (name.equals("")) {
            name = child.get(groupPosition).get(childPosition).getRealname();
        }
        viewHolder2.tv_child.setText(name);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (groupPosition >= getGroupCount() || childPosition >= getChildrenCount(groupPosition)) {
                    return;
                }
                IMUIHelper.openUserHomePageActivity(context, child.get(groupPosition).get(childPosition).getUserid());
            }
        });
        viewHolder2.iv_child.configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
        viewHolder2.iv_child.loadImage(child.get(groupPosition).get(childPosition).getPortrait(), "0");
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return child.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }

    @Override
    public int getGroupCount() {
        return group.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = View.inflate(context, R.layout.activity_fg_group, null);
            viewHolder.tv_group_name = (TextView) convertView.findViewById(R.id.tv_group_name);
            viewHolder.iv_image = (ImageView) convertView.findViewById(R.id.iv_image);
            viewHolder.tv_group_member_num = (TextView) convertView.findViewById(R.id.tv_group_member_num);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_group_name.setText(group.get(groupPosition).getGroupname());
        if (isExpanded) {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.friend_list_parent_item_open);
            viewHolder.iv_image.setImageBitmap(bitmap);
        } else {
            Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.friend_list_parent_item_close);
            viewHolder.iv_image.setImageBitmap(bitmap);
        }
        viewHolder.tv_group_member_num.setText(child.get(groupPosition).size() + "人");
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    class ViewHolder {
        ImageView iv_image;
        TextView tv_group_name;
        TextView tv_group_member_num;
    }

    class ViewHolder2 {
        CustomHeadImage iv_child;
        TextView tv_child;
    }

}
