package com.yuxip.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by Ly on 2015/8/27.
 * 喜欢或收藏的适配器
 */
public class FavorOrCollectAdapter extends BaseAdapter {

    private Context context;
    private List<HomeInfo.PersoninfoEntity.MyHomeEntity> mlists=new ArrayList<>();
    private String type; //类型   0  喜欢     1  收藏
    private ImageLoader imageLoader;
    private DisplayImageOptions loaderOptions;
    private final int TYPE_COUNT = 2;
    private final int TYPE_STORY = 1;
    private final int TYPE_TOPIC = 0;
    private String loginId;
    public  FavorOrCollectAdapter(Context context,String type,List<HomeInfo.PersoninfoEntity.MyHomeEntity> mlists){
        this.context=context;
        this.type=type;
        this.mlists=mlists;
        loginId= IMLoginManager.instance().getLoginId()+"";
        imageLoader= ImageLoaderUtil.getImageLoaderInstance();
        loaderOptions=ImageLoaderUtil.getOptions(DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_DEFAULT_PIC));
    }


    @Override
    public int getCount() {
        return mlists.size();
    }

    @Override
    public Object getItem(int position) {
        return mlists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return  TYPE_COUNT;
    }

    @Override
    public int getItemViewType(int position) {
        /**********     getType类型  0 剧  1 自戏  2 话题***********/
        if(mlists.get(position).getType().equals("2")){
            return  TYPE_TOPIC;
        }else{
            return  TYPE_STORY;
        }
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        ViewHolder2 viewHolder2=null;
        if(convertView ==null) {
            if (getItemViewType(position) == TYPE_STORY) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_listview_favororcollect, parent, false);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                convertView = LayoutInflater.from(context).inflate(R.layout.item2_listview_favororcollect, parent, false);
                viewHolder2 = new ViewHolder2(convertView);
                convertView.setTag(viewHolder2);

            }
        }

            if (getItemViewType(position) == TYPE_STORY) {
                viewHolder = (ViewHolder) convertView.getTag();

                viewHolder.tv_name.setText(mlists.get(position).getTitle());

                if (!TextUtils.isEmpty(mlists.get(position).getCreatetime()))
                    viewHolder.tv_time.setText(DateUtil.getDateWithOutYear(Integer.valueOf(mlists.get(position).getCreatetime())));

                if (mlists.get(position).getType().equals("0")) {
                    viewHolder.tv_title.setText(mlists.get(position).getCreatorname() + "的剧");
                } else {
                    viewHolder.tv_title.setText(mlists.get(position).getCreatorname() + "的自戏");
                }

                if (TextUtils.isEmpty(mlists.get(position).getPortrait())) {
                    viewHolder.iv_cover.setVisibility(View.GONE);
                } else {
                    viewHolder.iv_cover.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(mlists.get(position).getPortrait(), viewHolder.iv_cover, loaderOptions);
                }

                viewHolder.tv_content.setText(mlists.get(position).getContent());

                viewHolder.iv_headImg.configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
                viewHolder.iv_headImg.setVipSize(12f);
                viewHolder.iv_headImg.loadImage(mlists.get(position).getCreatorportrait(), "0");

                viewHolder.iv_headImg.setTag(position);
                viewHolder.rel_outline.setTag(position);
                viewHolder.iv_headImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        IMUIHelper.openUserHomePageActivity(context, mlists.get(_position).getCreatorid());
                    }
                });
                viewHolder.rel_outline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        if (mlists.get(_position).getType().equals("0")) {
                            IMUIHelper.openStoryDetailActivity(context, mlists.get(_position).getId());
                        } else {
                            IMUIHelper.openHisBookActivity(context, mlists.get(_position).getId(), mlists.get(_position).getCreatorid());
                        }

                    }
                });


            } else {
                viewHolder2 = (ViewHolder2) convertView.getTag();
                viewHolder2.tv_name.setText(mlists.get(position).getTitle());
                if (!TextUtils.isEmpty(mlists.get(position).getCreatetime()))
                    viewHolder2.tv_time.setText(DateUtil.getDateWithOutYear(Integer.valueOf(mlists.get(position).getCreatetime())));
                viewHolder2.tv_title.setText(mlists.get(position).getCreatorname() + "的话题");
                viewHolder2.rel_outline.setTag(position);

                if (TextUtils.isEmpty(mlists.get(position).getPortrait())) {
                    viewHolder2.iv_cover.setVisibility(View.GONE);
                } else {
                    viewHolder2.iv_cover.setVisibility(View.VISIBLE);
                    imageLoader.displayImage(mlists.get(position).getPortrait(), viewHolder2.iv_cover, loaderOptions);
                }

                viewHolder2.iv_headImg.configImageOptions(DrawableCache.KEY_PERSON_PORTRAIT);
                viewHolder2.iv_headImg.setVipSize(12f);
                viewHolder2.iv_headImg.loadImage(mlists.get(position).getCreatorportrait(), "0");

                viewHolder2.iv_headImg.setTag(position);
                viewHolder2.iv_headImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        IMUIHelper.openUserHomePageActivity(context, mlists.get(_position).getCreatorid());

                    }
                });
                viewHolder2.rel_outline.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int _position = (int) v.getTag();
                        IMUIHelper.openSquareCommentActivity(context, mlists.get(_position).getTitle(), mlists.get(_position).getId(), IntentConstant.FLOOR_TYPE_TOPIC);
                    }
                });

            }

        return  convertView;
    }



    /**********     这是剧或自戏的     **********/
    class ViewHolder{
        @InjectView(R.id.rel_favor_collect_adapter)
        RelativeLayout rel_outline;
        @InjectView(R.id.iv_headimg_favor_collect_adapter)
        CustomHeadImage iv_headImg;
        @InjectView(R.id.tv_title_favor_collect_adapter)
        TextView  tv_title;
        @InjectView(R.id.tv_name_favor_collect_adapter)
        TextView  tv_name;
        @InjectView(R.id.tv_time_favor_collect_adapter)
        TextView  tv_time;
        @InjectView(R.id.tv_content_favor_collect_adapter)
        TextView tv_content;
        @InjectView(R.id.iv_cover_favor_collect_adapter)
        ImageView iv_cover;
        public ViewHolder(View view){
            ButterKnife.inject(this,view);
        }
    }
    /**********     这是话题的     **********/
    class ViewHolder2 {
        @InjectView(R.id.rel_favor_collect_adapter)
        RelativeLayout rel_outline;
        @InjectView(R.id.iv_headimg_favor_collect_adapter)
        CustomHeadImage iv_headImg;
        @InjectView(R.id.tv_title_favor_collect_adapter)
        TextView  tv_title;
        @InjectView(R.id.tv_name_favor_collect_adapter)
        TextView  tv_name;
        @InjectView(R.id.tv_time_favor_collect_adapter)
        TextView  tv_time;
        @InjectView(R.id.iv_cover_favor_collect_adapter)
        ImageView iv_cover;

        public ViewHolder2(View view) {
            ButterKnife.inject(this,view);
        }
    }

}
