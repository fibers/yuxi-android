package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.yuxip.JsonBean.Members;
import com.yuxip.R;
import com.yuxip.ui.widget.CircularImage;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Administrator on 2015/6/5.
 */
public class InviteMembersAdapter extends BaseAdapter {
    private Context ctx;
    List<Members> list;

    /**在选择面板里面选择的*/
    private Set<Integer> checkListSet= new HashSet<>();

    public InviteMembersAdapter(Context ctx, List<Members> list){
        this.ctx = ctx;
        this.list =list;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
       final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(ctx).inflate(R.layout.item_1, viewGroup,false);
            holder.friendName = (TextView) view.findViewById(R.id.friendName);
            holder.friendAvatar = (CircularImage) view.findViewById(R.id.friendAvatar);
            holder.checkBox = (CheckBox) view.findViewById(R.id.checkBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.checkBox.setClickable(false);
        holder.friendName.setText(list.get(position).getNickname());
//        new BitmapUtils(ctx).display(holder.friendAvatar, list.get(position).getPortrait());
        ImageLoaderUtil.getImageLoaderInstance().displayImage(list.get(position).getPortrait(),holder.friendAvatar);
        if (list.get(position).isChecked()) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);

        }

        holder.checkBox.setClickable(false);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.get(position).isChecked()) {
                    list.get(position).setIsChecked(false);
                    checkListSet.remove(Integer.valueOf(list.get(position).getId()));
                    holder.checkBox.setChecked(false);
                } else {
                    list.get(position).setIsChecked(true);
                    checkListSet.add(Integer.valueOf(list.get(position).getId()));
                    holder.checkBox.setChecked(true);
                }
            }
        });


//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                int userId = 0;
//                if (b){
//                    userId = Integer.valueOf(list.get(position).getId());
//                    checkListSet.add(userId);
//                }else{
//                    checkListSet.add(userId);
//                }
//            }
//        });
        return view;
    }
    class ViewHolder{
        CheckBox checkBox;
        TextView friendName;
        TextView tvHint;
        CircularImage friendAvatar;
    }

    public void setCheckListSet(Set<Integer> checkListSet) {
        this.checkListSet = checkListSet;
    }

    public Set<Integer> getCheckListSet() {
        return checkListSet;
    }
}
