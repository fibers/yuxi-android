package com.yuxip.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.yuxip.JsonBean.RecommendStorysResult;
import com.yuxip.R;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 招募
 * Created by HeTianpeng on 2015/07/01.
 */
public class RecruitAdapter extends BaseRecyclerViewAdapter {
    private List<RecommendStorysResult.PosterlistEntity> posterlist;

    @Override
    public void onBinddViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof RecruitAdapter.ViewHolder) {
            RecruitAdapter.ViewHolder holder = (ViewHolder) viewHolder;
//            bu.display(holder.img, posterlist.get(position).getCreateportrait());
            imageLoader.displayImage(posterlist.get(position).getCreateportrait(),holder.img);
            holder.tvRecommendDesc.setText(posterlist.get(position).getPostercontent());
            holder.tvDramaName.setText(posterlist.get(position).getStorytitle());
            holder.tvRecommendUserName.setText(posterlist.get(position).getCreatorname());
            holder.dramaClass.setText(posterlist.get(position).getStorycatetory());
            holder.tvTime.setText(
                    DateUtil.getSessionTime(
                            Integer.parseInt(posterlist.get(position).getCreattime())));

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IMUIHelper.openUserHomePageActivity(context, posterlist.get(position).getCreatorid());
                }
            });
        }
    }

//    private BitmapUtils bu;
    private ImageLoader imageLoader;
    public RecruitAdapter(Context context,List<RecommendStorysResult.PosterlistEntity> posterlist) {
        super(context);
        this.posterlist = posterlist;
//        bu = new BitmapUtils(context);
        imageLoader = ImageLoaderUtil.getImageLoaderInstance();
    }

    public static class ViewHolder extends UltimateRecyclerviewViewHolder {
        @InjectView(R.id.tv_recommend_desc)
        TextView tvRecommendDesc;
        @InjectView(R.id.tv_drama_name)
        TextView tvDramaName;
        @InjectView(R.id.drama_class)
        TextView dramaClass;
        @InjectView(R.id.tv_recommend_user_name)
        TextView tvRecommendUserName;
        @InjectView(R.id.tv_time)
        TextView tvTime;
        @InjectView(R.id.img_icon)
        ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }


    @Override
    public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup viewGroup) {
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_recruit, null);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {

        return  null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int i) {

    }


    @Override
    public int getAdapterItemCount() {
        return null == posterlist || posterlist.isEmpty() ? 0 : posterlist.size();
    }

    @Override
    public long generateHeaderId(int i) {
        return i;
    }
}
