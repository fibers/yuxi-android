package com.yuxip.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.yuxip.JsonBean.TopicDetailsJsonBean;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.manager.http.SquareCommentManager;
import com.yuxip.ui.customview.CommentItemView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2015/10/9.
 */
public class SquareCommentAdapter extends BaseAdapter {

    private Context context;
    private List<TopicDetailsJsonBean.CommentEntity> list_hotComment = new ArrayList<>();
    private List<TopicDetailsJsonBean.CommentEntity> list_Comment = new ArrayList<>();

    public SquareCommentAdapter(Context context,List<TopicDetailsJsonBean.CommentEntity> list_hotComment,List<TopicDetailsJsonBean.CommentEntity> list_comment){
        this.context = context;
        this.list_hotComment = list_hotComment;
        this.list_Comment = list_comment ;

    }

    public void setListHotComment(List<TopicDetailsJsonBean.CommentEntity> list_hotComment){
        this.list_hotComment =list_hotComment;
    }

    public void setListComment(List<TopicDetailsJsonBean.CommentEntity> list_Comment){
        this.list_Comment = list_Comment;
    }
    public void addNewFloor(String name,String content){
        TopicDetailsJsonBean.CommentEntity commentEntity =new TopicDetailsJsonBean.CommentEntity();
        commentEntity.setCommentContent(content);
        TopicDetailsJsonBean.CommentEntity.FromUserEntity fromUserEntity =new TopicDetailsJsonBean.CommentEntity.FromUserEntity();
        fromUserEntity.setNickName(name);
        fromUserEntity.setId(SquareCommentManager.getInstance().getLzId());
        commentEntity.setFromUser(fromUserEntity);
        commentEntity.setCommentTime(System.currentTimeMillis() + "");
        commentEntity.setTotalChildCommentCount(0 + "");
        if(list_Comment.size()==0){
            commentEntity.setFloorCount("1");
            commentEntity.setCommentID("0");
        }else{
            commentEntity.setFloorCount((Integer.valueOf(list_Comment.get(list_Comment.size() - 1).getFloorCount()) + 1) + "");
            commentEntity.setCommentID(Long.valueOf(list_Comment.get(list_Comment.size()-1).getCommentID())+1+"");
        }
        commentEntity.setFavorCount("0");
        list_Comment.add(commentEntity);
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return list_Comment.size()+list_hotComment.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
                if(convertView == null){
                    convertView =  LayoutInflater.from(context).inflate(R.layout.commentitemview,null);
                    viewHolder = new ViewHolder(convertView);
                    convertView.setTag(viewHolder);
                }else{
                    viewHolder= (ViewHolder) convertView.getTag();
                }

                viewHolder.commentVItemView.hideTopLabel();
                viewHolder.commentVItemView.setCurPosition(position);
//                viewHolder2.commentVItemView.setCommentItemViewCallBack(this);

                    if(position == 0&&list_hotComment.size()>0){
                        viewHolder.commentVItemView.setSetHotLable();
                        viewHolder.commentVItemView.showTopLable();
                    }

                 if(position ==list_hotComment.size()){
                    viewHolder.commentVItemView.setCommentLabel();
                    viewHolder.commentVItemView.showTopLable();
                }

                  if(position<list_hotComment.size()){
                      viewHolder.commentVItemView.setData(list_hotComment.get(position), IntentConstant.FLOOR_COMMENT_TYPE_HOT);
                  }else{
                      viewHolder.commentVItemView.setData(list_Comment.get(position-list_hotComment.size()),IntentConstant.FLOOR_COMMENT_TYPE_ALL);
                  }

        return  convertView;
    }


    class ViewHolder{
        public ViewHolder(View view){
            commentVItemView = (CommentItemView) view.findViewById(R.id.commentitemview);
        }
        CommentItemView commentVItemView;
    }


}

