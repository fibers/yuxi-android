package com.yuxip.ui.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.yuxip.JsonBean.StoryDetailsBean;
import com.yuxip.R;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 剧详情页(StoryDetailsActivity) 中的内容
 * <p/>
 * Select2Fragment 中的adapter
 * <p/>
 * Created by HeTianpeng on 2015/07/14.
 */
public class Select2Adapter extends BaseAdapter {
    Context context;
    private List<? extends Object> data;
    private String tag;

    public Select2Adapter(Context context, List<? extends Object> data, String tag) {
        this.context = context;
        this.data = data;
        this.tag = tag;
    }

    @Override
    public int getCount() {
        return null == data || data.isEmpty() ? 0 : data.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Select2Adapter.ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.activity_role_item, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
//            holder.tvname.setText("测试");
//            holder.tvContent.setText("下雨天,还是下雪天");
        } else {
            holder = (ViewHolder) view.getTag();
        }

        if (tag.equals("2")) { //主线剧情
            StoryDetailsBean.ScenesEntity se = (StoryDetailsBean.ScenesEntity) data.get(i);
            holder.tvname.setVisibility(View.GONE);
//            holder.tvtitle.setText(se.getTitle());
            holder.tvContent.setTextSize(TypedValue.COMPLEX_UNIT_SP,15f);
            holder.tvContent.setText(se.getContent());

        } else { //主线人物
            StoryDetailsBean.RolesEntity re = (StoryDetailsBean.RolesEntity) data.get(i);
            holder.tvNameTitle.setVisibility(View.VISIBLE);
            holder.tvname.setText(re.getTitle()+"x"+re.getNum());
            holder.tvbackground.setVisibility(View.VISIBLE);
            holder.tvContent.setBackgroundColor(context.getResources().getColor(R.color.textview));
            holder.tvContent.setText(re.getIntro());

        }

        return view;
    }


    /**
     * This class contains all butterknife-injected Views & Layouts from layout file 'item_select_2.xml'
     * for easy to all layout elements.
     *
     * @author ButterKnifeZelezny, plugin for Android Studio by Avast Developers (http://github.com/avast)
     */
    static class ViewHolder {
        @InjectView(R.id.tv_name)
        TextView tvname;
        @InjectView(R.id.antecedentstatement)
        TextView tvContent;
        @InjectView(R.id.tv_background)
        TextView tvbackground;
        @InjectView(R.id.tv_name_title)
        TextView tvNameTitle;

        ViewHolder(View view) {
            ButterKnife.inject(this, view);
        }
    }
}
