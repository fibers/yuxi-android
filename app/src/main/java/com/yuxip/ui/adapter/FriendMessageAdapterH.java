package com.yuxip.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.yuxip.JsonBean.MessageBean;
import com.yuxip.R;
import com.yuxip.config.SysConstant;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.IMGroupAvatar;
import com.yuxip.utils.DateUtil;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 消息列表最近联系人列表适配器
 * create by SummerRC at 15/06/06
 */
public class FriendMessageAdapterH extends BaseAdapter {
    private LayoutInflater mInflater = null;
    private List<MessageBean> recentSessionList;
    private Logger logger = Logger.getLogger(FriendMessageAdapterH.class);
    private Context context;
    private static final int TYPE_SINGLE = 0;
    private static final int TYPE_GROUP = 1;

    public FriendMessageAdapterH(Context context) {
        this.mInflater = LayoutInflater.from(context);
    }

    public void setData(List<MessageBean> recentSessionList, Context context) {
        this.context = context;
        logger.d("recent#set New recent session list");
        logger.d("recent#notifyDataSetChanged");
        if(recentSessionList == null) {
            return;
        }
        this.recentSessionList = recentSessionList;
        notifyDataSetChanged();
    }

    public List<MessageBean> getData() {
        return this.recentSessionList;
    }

    @Override
    public int getCount() {
        return null == recentSessionList || recentSessionList.isEmpty() ? 0 : recentSessionList.size();
    }

    /**
     * 基本HOLDER
     */
    private static class ContactHolderBase {
        public TextView username;
        public TextView lastContent;
        public TextView lastTime;
        public TextView msgCount;
        public ImageView noDisturb;
    }

    /**
     * 用户HOLDER
     */
    private final class ContactViewHolder extends ContactHolderBase {
        public CustomHeadImage avatar;
        public CheckBox checkBox;
    }

    /**
     * 群组HOLDER
     */
    private final static class GroupViewHolder extends ContactHolderBase {
        public IMGroupAvatar avatarLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        switch (getItemViewType(position)) {
            case TYPE_SINGLE:
                convertView = renderUser(position, convertView, parent);
                break;
            case TYPE_GROUP:
                convertView = renderUser(position, convertView, parent);
                break;
        }
        return convertView;
    }

    /**
     * 渲染好友、评论群、系统消息等
     *
     * @param position      position
     * @param convertView   convertView
     * @param parent        parent
     * @return              View
     */
    private View renderUser(int position, View convertView, ViewGroup parent) {
        ContactViewHolder holder;
        if (null == convertView) {
            convertView = mInflater.inflate(R.layout.tt_item_chat_single_x, parent, false);
            holder = new ContactViewHolder();
            holder.avatar = (CustomHeadImage) convertView.findViewById(R.id.messageImg);
            holder.username = (TextView) convertView.findViewById(R.id.shop_name);
            holder.lastContent = (TextView) convertView.findViewById(R.id.message_body);
            holder.lastTime = (TextView) convertView.findViewById(R.id.message_time);
            holder.msgCount = (TextView) convertView.findViewById(R.id.message_count_notify);
            holder.noDisturb = (ImageView) convertView.findViewById(R.id.message_time_no_disturb_view);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_tt_item_chat);
            convertView.setTag(holder);
        } else {
            holder = (ContactViewHolder) convertView.getTag();
        }

        if (!recentSessionList.isEmpty() && position<getCount()) {
            MessageBean messageBean = this.recentSessionList.get(position);
            handleUserContact(holder, messageBean);
        }
        return convertView;
    }


    /**
     * 渲染家族群、剧群
     *
     * @param position      position
     * @param convertView   convertView
     * @param parent        parent
     * @return              View
     */
    private View renderGroup(int position, View convertView, ViewGroup parent) {
        GroupViewHolder holder;
        if (null == convertView) {
            convertView = mInflater.inflate(R.layout.tt_item_chat_group_x, parent, false);
            holder = new GroupViewHolder();
            holder.avatarLayout = (IMGroupAvatar) convertView.findViewById(R.id.contact_portrait);
            holder.username = (TextView) convertView.findViewById(R.id.shop_name);
            holder.lastContent = (TextView) convertView.findViewById(R.id.message_body);
            holder.lastTime = (TextView) convertView.findViewById(R.id.message_time);
            holder.msgCount = (TextView) convertView.findViewById(R.id.message_count_notify);
            holder.noDisturb = (ImageView) convertView.findViewById(R.id.message_time_no_disturb_view);
            convertView.setTag(holder);
        } else {
            holder = (GroupViewHolder) convertView.getTag();
        }

        if (!recentSessionList.isEmpty()) {
            MessageBean messageBean = this.recentSessionList.get(position);
            handleGroupContact(holder, messageBean);
        }
        return convertView;
    }


    private void handleUserContact(ContactViewHolder contactViewHolder, MessageBean recentInfo) {
        /** 设置未读消息计数 */
        int msgCount = recentInfo.getMessageCount();
        if (msgCount > 0) {
            String strCountString = String.valueOf(msgCount);
            if (msgCount > 99) {
                strCountString = "99+";
            }
            contactViewHolder.msgCount.setVisibility(View.VISIBLE);
            contactViewHolder.msgCount.setText(strCountString);
            contactViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.pink));
        } else {
            contactViewHolder.msgCount.setVisibility(View.GONE);
            contactViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.message_time_color_h));
        }

        /** 设置头像 */
        String avatarUrl = "";
        if (null != recentInfo.getAvatar() && recentInfo.getAvatar().size() > 0) {
            avatarUrl = recentInfo.getAvatar().get(0);
        }
        DisplayImageOptions options;

        Enum e = recentInfo.getiMessageTypeDetail().getMessageType();
        if (e == MessageBean.MessageType.MESSAGE_TYPE_GROUP) {
            Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_GROUP_PORTRAIT);
            options = ImageLoaderUtil.getOptions(drawable);
        } else if (e == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL) {
            Drawable drawable = DrawableCache.getInstance(context.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT);
            options = ImageLoaderUtil.getOptions(drawable);
        } else if (e == MessageBean.MessageType.MESSAGE_TYPE_COMMENT) {
            options = ImageLoaderUtil.getOptions(R.drawable.comment_message);
        } else {
            options = ImageLoaderUtil.getOptions(R.drawable.icon_system_message);
        }
        contactViewHolder.avatar.configImageOptions(options);
        contactViewHolder.avatar.setVipSize(12F);
        if (!TextUtils.isEmpty(recentInfo.getIconUrl())) {
            contactViewHolder.avatar.loadImage(recentInfo.getIconUrl());
        } else {
            contactViewHolder.avatar.loadImage(avatarUrl);
        }

        /** 设置其它信息 */
        contactViewHolder.username.setText(recentInfo.getName());
        contactViewHolder.lastContent.setText(recentInfo.getMessage());
        int time = 0;
        if (!recentInfo.getTime().equals("")) {
            time = Integer.parseInt(recentInfo.getTime());
        }
        contactViewHolder.lastTime.setText(DateUtil.getSessionTime(time));
        if (recentInfo.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_SYSTEM) {
            if (recentInfo.getMessageCount() <= 0) {
                contactViewHolder.lastTime.setText("");
            }
        }
    }


    private void handleGroupContact(GroupViewHolder groupViewHolder, MessageBean recentInfo) {
        /** 设置未读消息计数 */
        int msgCount = recentInfo.getMessageCount();
        if (msgCount > 0) {
            String strCountString = String.valueOf(msgCount);
            if (msgCount > 99) {
                strCountString = "99+";
            }
            groupViewHolder.msgCount.setVisibility(View.VISIBLE);
            groupViewHolder.msgCount.setText(strCountString);
            groupViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.pink));
        } else {
            groupViewHolder.msgCount.setVisibility(View.GONE);
            groupViewHolder.lastContent.setTextColor(context.getResources().getColor(R.color.message_time_color_h));
        }

        /** 设置其它信息 */
        groupViewHolder.username.setText(recentInfo.getName());
        groupViewHolder.lastContent.setText(recentInfo.getMessage());
        int time = 0;
        if (!recentInfo.getTime().equals("")) {
            time = Integer.parseInt(recentInfo.getTime());
        }
        groupViewHolder.lastTime.setText(DateUtil.getSessionTime(time));
        if (recentInfo.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_SYSTEM) {
            if (recentInfo.getMessageCount() <= 0) {
                groupViewHolder.lastTime.setText("");
            }
        }

        /** 设置头像 */
        if (null != recentInfo.getAvatar() && recentInfo.getAvatar().size() > 0) {
            setGroupAvatar(groupViewHolder, recentInfo.getAvatar());
        }
    }


    /**
     * 设置群头像(注意：用户头像url后面添加的用于区别签约写手后缀需要去掉）
     *
     * @param holder            GroupViewHolder
     * @param avatarUrlList     avatarUrlList
     */
    private void setGroupAvatar(GroupViewHolder holder, List<String> avatarUrlList) {
        try {
            if (null == avatarUrlList) {
                return;
            }
            List<String> newAvatarList = new ArrayList<>();
            String newAvatar;
            for(String str : avatarUrlList) {
                if(str.contains("?issigned=")) {
                    newAvatar = str.substring(0, str.lastIndexOf("?"));
                    newAvatarList.add(newAvatar);
                } else {
                    newAvatarList.add(str);
                }
            }
            holder.avatarLayout.setAvatarUrlAppend(SysConstant.AVATAR_APPEND_32);
            holder.avatarLayout.setChildCorner(3);
            holder.avatarLayout.setAvatarUrls(new ArrayList<>(newAvatarList));
        } catch (Exception e) {
            logger.e(e.toString());
        }

    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public Objects getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * 家族消息和剧群消息需要显示成多人头像集合
     *
     * @param position  position
     * @return          type
     */
    @Override
    public int getItemViewType(int position) {
        try {
            if (position >= recentSessionList.size()) {
                return TYPE_SINGLE;
            }
            MessageBean messageBean = recentSessionList.get(position);
            if(messageBean.getiMessageTypeDetail().getMessageType().ordinal()==2 ||
                    messageBean.getiMessageTypeDetail().getMessageType().ordinal()==4) {
                return TYPE_GROUP;
            } else {
                return TYPE_SINGLE;
            }
        }catch (Exception e){
            logger.e(e.toString());
            return TYPE_SINGLE;
        }
    }
}
