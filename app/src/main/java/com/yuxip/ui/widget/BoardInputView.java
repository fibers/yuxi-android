package com.yuxip.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ExpressionEntity;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.utils.IMUIHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 面板界面的键盘
 * Created by SRC on 2015/4/14.
 */
public class BoardInputView extends LinearLayout implements View.OnClickListener {

    /**
     * 枚举类型：用于标识两个按钮
     */
    public enum MyViewEnum {
        BUTTON_INPUT_BIAOQING, BUTTON_GO_MIANBAN, EDITTEXT_INPUT, TEXTVIEW_TEXT_SEND, BUTTON_BIAOQING_SEND, LL_FACE_INPUT_VIEW_HOLDER
    }

    /**
     * 枚举类型：作为切换表情键盘与文字键盘的标识
     */
    public enum MyTagEnum {
        BUTTON__BACKGROUND_LABEL, BUTTON_BACKGROUND_KEYBOARD
    }

    private MyTagEnum tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;   //按钮默认显示的是标签背景
    private ViewPager viewPager;            //用于装载多页的表情
    private List<View> list;                //要分页显示的视图的集合（每页其实是一个GridView)
    private static List<ExpressionEntity> allGridViewData;
//    private static List<Expression> allGridViewData;              //所有的GridView的数据源，即所有表情集合，需要拆分成单个的List之后分配给每个GridView
    private GridView gv_biaoqing;           //显示每页表情的GridView
    private BiaoqingGridViewAdapter gridViewAdapter;
    private ImageView[] dot_imgs;                                 //4个底部位置标记
    private View view;                                            //布局视图
    private Context context;
    private EditText et_input;
//    private DBUtils dbUtils;
    private DBInterface dbInterface;

    /**
     * 构造函数
     *
     * @param context
     * @param attrs
     */
    public BoardInputView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initTableExpression();
        initData();
        initView();
    }

    /**
     * 构造函数
     *
     * @param context
     */
    public BoardInputView(final Context context) {
        super(context);
        this.context = context;
        initTableExpression();
        initData();
        initView();
    }

    /**
     * 初始化数据库表情表里的数据，因为默认存在6个表情
     */
    private void initTableExpression() {

        /** 先判断默认表情是否已经插入到表中 */
        try {
            ExpressionEntity expressionEntity = dbInterface.getExpressionEntityByLabel("【");
            if (expressionEntity == null) {
                String[] expressions = {"【", "】", "[", "]", "\"", "\""};
                List<ExpressionEntity> list = new ArrayList<>();
                for (int i = 0; i < expressions.length; i++) {
                    ExpressionEntity expressionEntity1 = new ExpressionEntity();
                    expressionEntity1.setLabel(expressions[i]);
                    expressionEntity1.setContent(expressions[i]);
                    list.add(expressionEntity1);
                }
                DBInterface.instance().insertOrUpdateAllExpressionEntity(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * 初始化数据
     */
    private void initData() {
        try {
            dbInterface = DBInterface.instance();
            allGridViewData = dbInterface.loadAllExpressionEntity();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 初始化视图
     */
    private void initView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.board_input_view, this);
        view.findViewById(R.id.bt_biaoqing).setOnClickListener(this);
        et_input = (EditText) view.findViewById(R.id.et_input);
        et_input.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                view.findViewById(R.id.ll__face_input_view_holder).setVisibility(View.VISIBLE);
                et_input.requestFocus();
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et_input, InputMethodManager.SHOW_FORCED);
                /** 设置按钮的背景 */
                findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
                tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
                return false;
            }
        });
        /** 文本框输入内容是显示发送按钮  */
        et_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                /** 如果当前显示的是自定义表情键盘，说明用户再输入自定义表情，则直接返回，不用显示发送TextView */
                if (tag == MyTagEnum.BUTTON_BACKGROUND_KEYBOARD) {
                    return;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

        list = new ArrayList<View>();
        for (int i = 0; i < getPagerNum(); i++) {
            boolean isLastPager = (i == (getPagerNum() - 1));
            list.add(getGridView(getEachGridViewData(i), isLastPager));
        }

        viewPager.setAdapter(new MyViewPagerAdapter());
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
            int preposition = 0;

            @Override
            public void onPageSelected(int position) {
                preposition = position;
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        if (dot_imgs != null) {
            dot_imgs = null;
        }
        dot_imgs = new ImageView[getPagerNum()];
        /** 初始化小点 */
        initIndicator(view);
    }

    /**
     * 通过计算获得ViewPager要显示的页卡数目
     *
     * @return 页卡数目
     */
    private int getPagerNum() {
        /** 所有表情的个数+1（有一个加号）除以15,再+1 */
        return (allGridViewData.size() + 1) % 15 == 0 ? (allGridViewData.size() + 1) / 15 : (allGridViewData.size() + 1) / 15 + 1;
    }

    /**
     * 获得对应页卡的数据源
     *
     * @param position ViewPager的第几个页卡
     * @return 返回相应页卡中GridView需要的数据源（表情集合)
     */

    private List<ExpressionEntity> getEachGridViewData(int position) {
        List<ExpressionEntity> expression_list = new ArrayList<ExpressionEntity>();
        if(position<(getPagerNum()-1) && getPagerNum()>1) {      //装满15个表情的页面
            for(int i=0; i<15; i++) {
                expression_list.add(allGridViewData.get(15*position + i));
            }
        } else {                                             //最后一个页卡
            for (int i = 0; i < (allGridViewData.size() - 15 * position); i++) {
                expression_list.add(allGridViewData.get(15 * position + i));
            }
        }
        return expression_list;
    }

    /**
     * 初始化每个ViewPager需要的视图,ViewPager的每个页卡对应一个GridView
     *
     * @return 一个页卡视图
     */
    private View getGridView(List<ExpressionEntity> list, boolean isLastPager) {
        View view = LayoutInflater.from(context).inflate(R.layout.face_input_view_gridview, null);
        gv_biaoqing = (GridView) view.findViewById(R.id.gv_input_face);
        gridViewAdapter = new BiaoqingGridViewAdapter(this, context, list, isLastPager);
        gv_biaoqing.setAdapter(gridViewAdapter);

        return view;
    }

    /**
     * 监听器
     *
     * @param v 监听的控件
     */
    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        switch (v.getId()) {
            case R.id.bt_biaoqing:
                switch (tag) {
                    case BUTTON__BACKGROUND_LABEL:      //如果当前按钮背景是标签
                        /** 强制隐藏软键盘并显示标签键盘 */
                        imm.hideSoftInputFromWindow(view.findViewById(R.id.et_input).getWindowToken(), 0);
                        view.findViewById(R.id.ll__face_input_view_holder).setVisibility(View.VISIBLE);
                        /** 设置按钮的背景 */
                        findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.keyboard_h);
                        tag = MyTagEnum.BUTTON_BACKGROUND_KEYBOARD;
                        break;
                    case BUTTON_BACKGROUND_KEYBOARD:    //如果当前按钮背景是键盘
                        /** 显示软键盘 */
                        et_input.requestFocus();
                        imm.showSoftInput(et_input, InputMethodManager.SHOW_FORCED);
                        /** 设置按钮的背景 */
                        findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
                        tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
                        break;
                }
                break;
        }

    }

    /**
     * ViewPager的适配器
     */
    private class MyViewPagerAdapter extends PagerAdapter {

        /**
         * @return 页卡的数目
         */
        @Override
        public int getCount() {
            return list.size();
        }

        /**
         * @param container
         * @param position
         * @param object
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            /** 删除页卡 */
            container.removeView(list.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            /** 添加页卡 */
            container.addView(list.get(position));
            return list.get(position);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

    }

    /**
     * 对外提供的接口：在输入框中的内容后面追加表情
     *
     * @param text
     */
    public void addBiaoqing(String text) {
        et_input.append(text);
    }

    /**
     * 对外提供的接口，用于返回自定义输入表情面板的Button或者输入框EditText
     *
     * @return 返回输入框旁边的按钮（表情按钮和去面板的按钮)
     */
    public View getView(MyViewEnum buttonEnum) {
        switch (buttonEnum) {
            case EDITTEXT_INPUT:
                /** 返回文本输入框 */
                return view.findViewById(R.id.et_input);
            case BUTTON_BIAOQING_SEND:
                /** 返回发送表情的按钮 */
                return view.findViewById(R.id.bt_biaoqing_send);
            case TEXTVIEW_TEXT_SEND:
                /** 返回发送文本的TextView */
                return view.findViewById(R.id.tv_text_send);
            case LL_FACE_INPUT_VIEW_HOLDER:
                /** 返回放置表情的LinearLayout */
                return view.findViewById(R.id.ll__face_input_view_holder);
        }
        return null;
    }

    /**
     * 对外提供的接口，用于刷新表情界面
     */
    public void refresh() {
        initData();
        initView();
    }

    /**
     * 初始化引导图标
     * 动态创建多个小圆点，然后组装到线性布局里
     */
    private void initIndicator(View view) {
        ImageView imgView;
        View v = view.findViewById(R.id.ll_dot_container);

        for (int i = 0; i < getPagerNum(); i++) {
            imgView = new ImageView(context);
            LinearLayout.LayoutParams params_linear = new LinearLayout.LayoutParams(10, 10);
            params_linear.setMargins(7, 10, 7, 10);
            imgView.setLayoutParams(params_linear);
            dot_imgs[i] = imgView;

            if (i == 0) {    // 初始化第一个为选中状态
                dot_imgs[i].setBackgroundResource(R.drawable.dot_blue);
            } else {
                dot_imgs[i].setBackgroundResource(R.drawable.dot_white);
            }
            ((ViewGroup) v).addView(dot_imgs[i]);
        }
    }


    public class BiaoqingGridViewAdapter extends BaseAdapter {
        private BoardInputView myFaceInputView;
        private Context context;
        private List<ExpressionEntity> list;
        private boolean isLastPager;     //如果是最后一页，则存在加号
        private int count;               //表情个数+1（因为存在一个加号)

        public BiaoqingGridViewAdapter(BoardInputView myFaceInputView, Context context, List<ExpressionEntity> list, boolean isLastPager) {
            this.myFaceInputView = myFaceInputView;
            this.context = context;
            this.list = list;
            this.isLastPager = isLastPager;
        }

        @Override
        public int getCount() {
            if (list == null) {
                count = 1;
            } else {
                if (isLastPager) {
                    count = list.size() + 1;
                } else {
                    count = list.size();
                }

            }
            return count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                holder = new ViewHolder();
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(R.layout.face_input_view_gridview_item, null);
                holder.bt_biaoqing = (Button) convertView.findViewById(R.id.bt_biaoqing);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            /** 只有最后一页才有加号 */
            if (position == count - 1 && isLastPager) {
                holder.bt_biaoqing.setBackgroundResource(R.drawable.xukuang);
                holder.bt_biaoqing.setText("+");
                holder.bt_biaoqing.setTextSize(18);
                holder.bt_biaoqing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        IMUIHelper.openAddOrEditDoActivity(context, "", "", IntentConstant.TYPE_ADD);
                    }
                });
            } else {
                holder.bt_biaoqing.setText(list.get(position).getLabel());
                holder.bt_biaoqing.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myFaceInputView.addBiaoqing(list.get(position).getContent());
                    }
                });
            }
            return convertView;
        }


        private class ViewHolder {
            private Button bt_biaoqing;
        }
    }


}
