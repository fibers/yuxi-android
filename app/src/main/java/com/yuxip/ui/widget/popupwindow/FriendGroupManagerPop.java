package com.yuxip.ui.widget.popupwindow;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.im.tools.ScreenTools;
import com.yuxip.R;
import com.yuxip.utils.IMUIHelper;

/**
 * Created by SummerRC on 2015/9/1.
 * description:好友分组管理的弹出提示框
 */
public class FriendGroupManagerPop implements View.OnClickListener {
    private PopupWindow pop;
    private Context context;
    private TextView tv_pop;
    private int width = 0;
    private int height = 0;

    public FriendGroupManagerPop(Context context) {
        this.context = context;
        View view = LayoutInflater.from(context).inflate(R.layout.pop_friend_group_manager, null);
        view.setOnClickListener(this);
        tv_pop = (TextView) view.findViewById(R.id.tv_pop);
        tv_pop.setOnClickListener(this);
        pop = new PopupWindow(view, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        pop.setBackgroundDrawable(new BitmapDrawable());
        pop.setOutsideTouchable(true);
        pop.setTouchable(true);
        pop.setFocusable(true);
    }

    public void setOnDismissListener(PopupWindow.OnDismissListener onDismissListener) {
        pop.setOnDismissListener(onDismissListener);
    }

    /**
     * PopupWindow关闭时显示；
     * 显示时关闭。
     */
    public void show(View view) {
        if (pop != null) {
            if (pop.isShowing()) {
                pop.dismiss();
            } else {
                showPopWindow(view);
            }
        }
    }

    private void showPopWindow(View view) {
        /** 需要手动测量控件高度，否则直接使用getWidth()可能得到的值是0 */
        if (width == 0 || height == 0) {
            int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
            tv_pop.measure(w, h);
            height = tv_pop.getMeasuredHeight();
            width = tv_pop.getMeasuredWidth();
        }

        int xOffInPixels = ScreenTools.instance(context).getScreenWidth() / 2 - width / 2;
        int yOffInPixels = 2 * height - 16;
        /** 将pixels转为dip,工具类有问题  */
        int xOffInDip = ScreenTools.instance(context).px2dip(xOffInPixels);
        int yOffInDip = ScreenTools.instance(context).px2dip(yOffInPixels);
        pop.showAsDropDown(view, xOffInPixels, -yOffInPixels);
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_pop:       //点击跳转到分组管理界面
                openFriendGroupManagerActivity();
                break;
        }
    }

    public void openFriendGroupManagerActivity() {
        IMUIHelper.openFriendGroupManagerActivity(context);
        pop.dismiss();
    }
}
