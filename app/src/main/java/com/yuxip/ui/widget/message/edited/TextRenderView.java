package com.yuxip.ui.widget.message.edited;

import android.content.Context;
import android.text.util.Linkify;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.R;
import com.yuxip.imservice.entity.TextMessage;
import com.yuxip.ui.widget.message.BaseMsgRenderView;

/**
 * @author : yingmu on 15-1-9.
 * @email : yingmu@im.com.
 * <p/>
 * 样式根据mine 与other不同可以分成两个
 */
public class
        TextRenderView extends BaseMsgRenderView {
    /**
     * 文字消息体
     */
    private TextView messageContent;
    private TextView txtLength;

    //是否是话题评论的群聊天样式
    private boolean isTopic;

    public static TextRenderView inflater(Context context, ViewGroup viewGroup, boolean isMine) {
//        int resource = isMine?R.layout.tt_mine_text_message_item:R.layout.tt_other_text_message_item;
        int resource = R.layout.tt_other_text_message_item;
        ////在构造函数中将Xml中定义的布局解析出来。
        TextRenderView textRenderView = (TextRenderView) LayoutInflater.from(context)
                .inflate(resource, viewGroup, false);
        textRenderView.setMine(isMine);
        textRenderView.setParentView(viewGroup);
        return textRenderView;
    }

    public static TextRenderView inflaterTopic(Context context, ViewGroup viewGroup) {
//        int resource = isMine?R.layout.tt_mine_text_message_item:R.layout.tt_other_text_message_item;
        int resource = R.layout.topic_text_message_item;
        ////在构造函数中将Xml中定义的布局解析出来。
        TextRenderView textRenderView = (TextRenderView) LayoutInflater.from(context)
                .inflate(resource, viewGroup, false);
//        textRenderView.setMine(isMine);
        textRenderView.setParentView(viewGroup);
        return textRenderView;
    }

    public TextRenderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        messageContent = (TextView) findViewById(R.id.txt_message_content);
        txtLength = (TextView) findViewById(R.id.right_txt_length);
    }

    /**
     * 控件赋值
     *
     * @param messageEntity
     * @param userEntity
     */
    @Override
    public void render(MessageEntity messageEntity, UserEntity userEntity, Context context) {
        super.render(messageEntity, userEntity, context);
        TextMessage textMessage = (TextMessage) messageEntity;
        // 按钮的长按也是上层设定的
        // url 路径可以设定 跳转哦哦
        String content = textMessage.getContent();
        messageContent.setText(Emoparser.getInstance(getContext().getApplicationContext())
                .emoCharsequence(content)); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
//        txtLength.setText(
//                Emoparser.getInstance(getContext()).emoCharsequence(content).length() + "个字");
        txtLength.setVisibility(GONE);
        extractUrl2Link(messageContent);
    }

    /**
     * @param messageEntity
     * @param userEntity
     * @param context
     * @param isMine        这里是为判断是否是自己，是自己的话，要改变字体颜色的。
     */
    public void render(MessageEntity messageEntity, UserEntity userEntity, Context context, boolean isMine) {
        super.render(messageEntity, userEntity, context);
        TextMessage textMessage = (TextMessage) messageEntity;
        // 按钮的长按也是上层设定的
        // url 路径可以设定 跳转哦哦
        String content = textMessage.getContent();
//        messageContent.setText(Emoparser.getInstance(getContext()).emoCharsequence(content)); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
//        txtLength.setText(Emoparser.getInstance(getContext()).emoCharsequence(content).length() + "个字");
        /**貌似是用于转换spd*/
//        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8,
//                context.getResources().getDisplayMetrics());
        if (isMine) {
            messageContent
                    .setTextColor(context.getResources().getColor(R.color.topic_message_text_2));
            name.setTextColor(context.getResources().getColor(R.color.topic_message_text_2));
//            messageContent.setTextSize(size);
//            name.setTextSize(size);
//            name.setText("我");
            name.setVisibility(View.GONE);
            messageContent.setText("我说: “" + Emoparser.getInstance(getContext().getApplicationContext())
                    .emoCharsequence(content) + "”。"); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
        } else {
            messageContent.setTextColor(
                    context.getResources().getColor(R.color.topic_message_text_0));
            name.setTextColor(context.getResources().getColor(R.color.topic_message_text_0));
//            messageContent.setTextSize(size);
//            name.setTextSize(size);
            name.setVisibility(GONE);
            //mod by guoq-s

            /*
            String[] c = Emoparser.getInstance(getContext()).emoCharsequence(content)
                    .toString().split(":");
            messageContent.setText(c[0] + ":“" + c[1] + "”。"); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
            */
            if(userEntity!=null&&userEntity.getMainName()!=null) {

                messageContent.setText(userEntity.getMainName() + ":“" +
                        Emoparser.getInstance(getContext().getApplicationContext()).emoCharsequence(content) + "”。");
            }else{
                messageContent.setText( "神秘人物:“" +
                        Emoparser.getInstance(getContext().getApplicationContext()).emoCharsequence(content) + "”。");
            }
            //mod by guoq-e
        }
        extractUrl2Link(messageContent);
    }

    /**
     * 同上
     *
     * @param messageEntity
     * @param userEntity
     * @param context
     * @param roleName
     * @param isMine
     */
    public void render(MessageEntity messageEntity, UserEntity userEntity, Context context, String roleName, boolean isMine) {
        super.render(messageEntity, userEntity, context, roleName);
        TextMessage textMessage = (TextMessage) messageEntity;
        // 按钮的长按也是上层设定的
        // url 路径可以设定 跳转哦哦
        String content = textMessage.getContent();
        int size = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8,
                context.getResources().getDisplayMetrics());
        if (isMine) {
            messageContent
                    .setTextColor(context.getResources().getColor(R.color.topic_message_text_2));
            name.setTextColor(context.getResources().getColor(R.color.topic_message_text_2));
            messageContent.setTextSize(size);
            name.setTextSize(size);
            messageContent.setText("说: “" + Emoparser.getInstance(getContext().getApplicationContext())
                    .emoCharsequence(content) + "”。"); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
        } else {
            messageContent
                    .setTextColor(context.getResources().getColor(R.color.topic_message_text_0));
            name.setTextColor(context.getResources().getColor(R.color.topic_message_text_0));
            messageContent.setTextSize(size);
            name.setTextSize(size);
            name.setVisibility(GONE);
//            messageContent.setText(Emoparser.getInstance(getContext())
//                    .emoCharsequence(content)); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
            //mod by guoq-s

            /*
            String[] c = Emoparser.getInstance(getContext()).emoCharsequence(content)
                    .toString().split(":");
            messageContent.setText(c[0] + ":“" + c[1] + "”。"); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
            */
            messageContent.setText(userEntity.getMainName()+":“" +
                    Emoparser.getInstance(getContext().getApplicationContext()).emoCharsequence(content) + "”。");
            //mod by guoq-e
        }
        extractUrl2Link(messageContent);
    }

    /**
     * 控件赋值
     *
     * @param messageEntity
     * @param userEntity
     * @param context
     * @param roleName
     */
    @Override
    public void render(MessageEntity messageEntity, UserEntity userEntity, Context context, String roleName) {
        super.render(messageEntity, userEntity, context, roleName);
        TextMessage textMessage = (TextMessage) messageEntity;
        // 按钮的长按也是上层设定的
        // url 路径可以设定 跳转哦哦
        String content = textMessage.getContent();
        messageContent.setText(Emoparser.getInstance(getContext().getApplicationContext())
                .emoCharsequence(content)); // 所以上层还是处理好之后再给我 Emoparser 处理之后的
//        txtLength.setText(
//                Emoparser.getInstance(getContext()).emoCharsequence(content).length() + "个字");
        txtLength.setVisibility(GONE);
        //mod by guoq-s
        extractUrl2Link(messageContent);
        //mod by guoq-e
    }

    private static final String SCHEMA = "com.yuxip://message_private_url";
    private static final String PARAM_UID = "uid";
    private String urlRegex = "((?:(http|https|Http|Https|rtsp|Rtsp):\\/\\/(?:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,64}(?:\\:(?:[a-zA-Z0-9\\$\\-\\_\\.\\+\\!\\*\\'\\(\\)\\,\\;\\?\\&\\=]|(?:\\%[a-fA-F0-9]{2})){1,25})?\\@)?)?((?:(?:[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}\\.)+(?:(?:aero|arpa|asia|a[cdefgilmnoqrstuwxz])|(?:biz|b[abdefghijmnorstvwyz])|(?:cat|com|coop|c[acdfghiklmnoruvxyz])|d[ejkmoz]|(?:edu|e[cegrstu])|f[ijkmor]|(?:gov|g[abdefghilmnpqrstuwy])|h[kmnrtu]|(?:info|int|i[delmnoqrst])|(?:jobs|j[emop])|k[eghimnrwyz]|l[abcikrstuvy]|(?:mil|mobi|museum|m[acdghklmnopqrstuvwxyz])|(?:name|net|n[acefgilopruz])|(?:org|om)|(?:pro|p[aefghklmnrstwy])|qa|r[eouw]|s[abcdeghijklmnortuvyz]|(?:tel|travel|t[cdfghjklmnoprtvwz])|u[agkmsyz]|v[aceginu]|w[fs]|y[etu]|z[amw]))|(?:(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9])\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[1-9]|0)\\.(?:25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[1-9][0-9]|[0-9])))(?:\\:\\d{1,5})?)(\\/(?:(?:[a-zA-Z0-9\\;\\/\\?\\:\\@\\&\\=\\#\\~\\-\\.\\+\\!\\*\\'\\(\\)\\,\\_])|(?:\\%[a-fA-F0-9]{2}))*)?(?:\\b|$)";

    private void extractUrl2Link(TextView v) {

        java.util.regex.Pattern wikiWordMatcher = java.util.regex.Pattern.compile(urlRegex);
        String mentionsScheme = String.format("%s/?%s=", SCHEMA, PARAM_UID);
        Linkify.addLinks(v, wikiWordMatcher, mentionsScheme);


    }

    private void extarUrl2WebLink(TextView v) {
        Linkify.addLinks(v, Linkify.PHONE_NUMBERS|Linkify.WEB_URLS|Linkify.EMAIL_ADDRESSES);
    }

    @Override
    public void msgFailure(MessageEntity messageEntity) {
        super.msgFailure(messageEntity);
    }

    /**
     * ----------------set/get---------------------------------
     */
    public TextView getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(TextView messageContent) {
        this.messageContent = messageContent;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean isMine) {
        this.isMine = isMine;
    }

    public ViewGroup getParentView() {
        return parentView;
    }

    public void setParentView(ViewGroup parentView) {
        this.parentView = parentView;
    }
}
