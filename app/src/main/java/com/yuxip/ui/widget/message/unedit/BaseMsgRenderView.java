package com.yuxip.ui.widget.message.unedit;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.DB.entity.MessageEntity;
import com.yuxip.DB.entity.UserEntity;
import com.yuxip.R;
import com.yuxip.config.MessageConstant;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.helper.DrawableCache;
import com.yuxip.ui.widget.IMBaseImageView;

import java.util.Random;

/**
 * @author : yingmu on 15-1-9.
 * @email : yingmu@im.com.
 */
public abstract class BaseMsgRenderView extends RelativeLayout {
    /**
     * 头像
     */
    protected IMBaseImageView portrait;
    /**
     * 消息状态
     */
    protected ImageView messageFailed;
    protected ProgressBar loadingProgressBar;
    protected TextView name;
    protected ImageView iv_signer;
    /**
     * 渲染的消息实体
     */
    protected MessageEntity messageEntity;
    protected ViewGroup parentView;
    protected boolean isMine;

    private boolean isFriendChat = false;

    protected BaseMsgRenderView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    // 渲染之后做的事情 子类会调用到这个地方嘛?
    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        portrait = (IMBaseImageView) findViewById(R.id.user_portrait);
        messageFailed = (ImageView) findViewById(R.id.message_state_failed);
        loadingProgressBar = (ProgressBar) findViewById(R.id.progressBar1);
        name = (TextView) findViewById(R.id.name);
        iv_signer = (ImageView) findViewById(R.id.user_signerIcon);
    }


    // 消息失败绑定事件 三种不同的弹窗
    // image的load状态就是 sending状态的一个子状态
    public void msgSendinging(final MessageEntity messageEntity) {
        messageFailed.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.VISIBLE);
    }

    public void msgFailure(final MessageEntity messageEntity) {
        messageFailed.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.GONE);
    }

    public void msgSuccess(final MessageEntity messageEntity) {
        messageFailed.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.GONE);
    }

    public void msgStatusError(final MessageEntity messageEntity) {
        messageFailed.setVisibility(View.GONE);
        loadingProgressBar.setVisibility(View.GONE);
    }

    /**
     * 控件赋值
     */
    public void render(final MessageEntity entity, UserEntity userEntity, final Context ctx) {
        this.messageEntity = entity;
        if (userEntity == null) {
            // 没有找到对应的用户信息 todo
            // 请求用户信息 设定默认头像、默认姓名、
            userEntity = new UserEntity();
            Random random = new Random();
            switch (random.nextInt()) {
                case 0:
                    userEntity.setMainName("大帅比");
                    userEntity.setRealName("大帅比");
                    break;
                case 1:
                    userEntity.setMainName("小帅比");
                    userEntity.setRealName("小帅比");
                    break;
                case 2:
                    userEntity.setMainName("帅比");
                    userEntity.setRealName("帅比");
                    break;
                default:
                    userEntity.setMainName("大帅比");
                    userEntity.setRealName("大帅比");
                    break;
            }
        }

        String avatar = userEntity.getAvatar();
        int msgStatus = messageEntity.getStatus();

        /*** ---  add by ly  头像添加是否是签约写手标志  start-- */
        iv_signer.setVisibility(View.INVISIBLE);
        if (CustomHeadImage.isSigner(avatar)) {
            iv_signer.setVisibility(View.VISIBLE);
        }
        /*** ---  add by ly   头像添加是否是签约写手标志   end-- */
        //头像设置
        portrait.setImageDrawable(DrawableCache.getInstance(ctx.getApplicationContext()).getDrawable(DrawableCache.KEY_PERSON_PORTRAIT));
        portrait.setCorner(5);
        /**  注释掉头像路径的拼接   begin **/
        //portrait.setAvatarAppend(SysConstant.AVATAR_APPEND_100);
        /**  注释掉头像路径的拼接   end   **/
        portrait.setImageUrl(avatar);
        // 设定姓名 应该消息都是有的
        if (!isMine && !isFriendChat) {
            name.setText(userEntity.getMainName());
            name.setVisibility(View.VISIBLE);
        }

        /**头像的跳转事件放到上层，因为家族、好友、剧群的跳转事件不一样*/
        /*final int userId = userEntity.getPeerId();
        portrait.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                IMUIHelper.openUserProfileActivity(getContext(), userId);
            }
        });*/
        /**头像事件 end*/

        // 设定三种信息的弹窗类型
        // 判定消息的状态 成功还是失败  todo 具体实现放在子类中
        switch (msgStatus) {
            case MessageConstant.MSG_FAILURE:
                msgFailure(messageEntity);
                break;
            case MessageConstant.MSG_SUCCESS:
                msgSuccess(messageEntity);
                break;
            case MessageConstant.MSG_SENDING:
                msgSendinging(messageEntity);
                break;
            default:
                msgStatusError(messageEntity);
        }

        // 如果消息还是处于loading 状态
        // 判断是否是image类型  image类型的才有 loading，其他的都GONE
        // todo 上面这些由子类做
        // 消息总体的类型有两种
        // 展示类型有四种(图片、文字、语音、混排)
    }

    /**
     * -------------------------set/get--------------------------
     */
    public ImageView getPortrait() {
        return portrait;
    }

    public ImageView getMessageFailed() {
        return messageFailed;
    }


    public ProgressBar getLoadingProgressBar() {
        return loadingProgressBar;
    }

    public TextView getName() {
        return name;
    }

    public boolean isFriendChat() {
        return isFriendChat;
    }

    public void setIsFriendChat(boolean isFriendChat) {
        this.isFriendChat = isFriendChat;
    }
}
