package com.yuxip.ui.widget;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ExpressionEntity;
import com.yuxip.DB.sp.SystemConfigSp;
import com.yuxip.R;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SysConstant;
import com.yuxip.ui.activity.chat.MsgMianBanActivity;
import com.yuxip.ui.adapter.BiaoqingGridViewAdapter;
import com.yuxip.ui.adapter.BiaoqingGridViewAdapter2;
import com.yuxip.ui.adapter.MyFaceViewAdapter;
import com.yuxip.ui.customview.LineGridView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义输入表情键盘视图
 * Created by SRC on 2015/4/14.
 */
public class FaceInputView extends LinearLayout implements View.OnClickListener, View.OnLongClickListener, View.OnTouchListener {

    /** 枚举类型：用于标识两个按钮 */
    public enum MyViewEnum{
        BUTTON_INPUT_BIAOQING, BUTTON_GO_MIANBAN, EDITTEXT_INPUT, TEXTVIEW_TEXT_SEND, BUTTON_BIAOQING_SEND, LL_FACE_INPUT_VIEW_HOLDER
        , IMAGE_VIEW_TAKE_PHOTO, IMAGE_VIEW_TAKE_CAMERA, BUTTON_ADD,BUTTON_BIAOQING
    }

    /** 枚举类型：作为切换表情键盘与文字键盘的标识 */
    public enum MyTagEnum{
        BUTTON__BACKGROUND_LABEL, BUTTON_BACKGROUND_KEYBOARD
    }

    private MyTagEnum tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;   //按钮默认显示的是标签背景
    private ViewPager viewPager;            //用于装载多页的表情
    private ViewPager viewPager2;           //用以装载第二页的文字表情
    private List<View> list;                //要分页显示的视图的集合（每页其实是一个GridView)
    private List<View> list2;               //要分页显示的视图集合(表情界面)
    private static List allGridViewData;      //所有的GridView的数据源，即所有表情集合，需要拆分成单个的List之后分配给每个GridView

    private static List<String> list_face_str=new ArrayList<String>();
    private GridView gv_biaoqing;           //显示每页表情的GridView
    private LineGridView gv_biaoqing2;
    private BiaoqingGridViewAdapter gridViewAdapter;
    private BiaoqingGridViewAdapter2 gridViewAdapter2;
    private ImageView[] dot_imgs;                                 //4个底部位置标记
    private ImageView[] dot_imgs2;                                //表情对应的标记
    private View view;                                            //布局视图
    private Context context;
    private EditText et_input;
    private LinearLayout ll_pic_photo_panel;
    private LinearLayout ll_face_container;
    private InputMethodManager inputManager = null;
    private switchInputMethodReceiver receiver;
    private String currentInputMethod ;
    private final String METHOD_ADJUST_RESIZE = "METHOD_ADJUST_RESIZE";
    private final String METHOD_ADJUST_NOTHIN = "METHOD_ADJUST_NOTHING";
    private boolean panelTag = false;

    private int currentPosition2;                                 //表情页面的索引

    private TextView tv_pibiao,tv_face;
    private LinearLayout ll_delete,ll_viewpager,ll_viewpager2;

    private String lastChat;
    private boolean isUnfold;       //皮表等界面是否展开
    private boolean isKeyboardShow; //键盘是否展开
//    private DBUtils dbUtils;
    private LinearLayout baseRoot;  //父Activity的根布局
    int rootBottom = Integer.MIN_VALUE, keyboardHeight = 0;
    int showHeight , orignHeight ;
    private LinearLayout.LayoutParams params1;
    private LinearLayout.LayoutParams params2;
    private DBInterface dbInterface;

    private float density;
    /**
     * 构造函数
     * @param context
     * @param attrs
     */

    public FaceInputView(final Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context=context;
        dbInterface = DBInterface.instance();
        initTableExpression();
        initData();
        initView();
    }

    /**
     * 构造函数
     * @param context
     */
    public FaceInputView(final Context context) {
        super(context);
        this.context=context;
        initTableExpression();
        initData();
        initView();
    }

    /**个人历史聊天数据*/
    public void setLastChatData(String content){
        if(!TextUtils.isEmpty(content)){
            this.lastChat=content;
            this.refresh();
        }
    }

    public String getLastChatDate(){
        if(!TextUtils.isEmpty(et_input.getText().toString())){
            return et_input.getText().toString();
        }else{
            return  null;
        }
    }




    private ViewTreeObserver.OnGlobalLayoutListener onGlobalLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            Rect r = new Rect();
            baseRoot.getGlobalVisibleRect(r);
            // 进入Activity时会布局，第一次调用onGlobalLayout，先记录开始软键盘没有弹出时底部的位置
            if (rootBottom == Integer.MIN_VALUE) {
                rootBottom = r.bottom;
                return;
            }
            // adjustResize，软键盘弹出后高度会变小
            if (r.bottom < rootBottom) {
                //按照键盘高度设置表情框和发送图片按钮框的高度
                keyboardHeight = rootBottom - r.bottom;
                SystemConfigSp.instance().init(getContext());
                SystemConfigSp.instance().setIntConfig(SystemConfigSp.SysCfgDimension.KEYBOARDHEIGHT, keyboardHeight);
                LayoutParams params = (LayoutParams) ll_face_container.getLayoutParams();
                params.height = keyboardHeight;
                LayoutParams params1 = (LayoutParams) ll_pic_photo_panel.getLayoutParams();
                params1.height = keyboardHeight;
                if(showHeight==0)
                    showHeight = keyboardHeight;
                if(currentInputMethod == METHOD_ADJUST_RESIZE){
                    ll_face_container.setVisibility(View.GONE);
                    ll_pic_photo_panel.setVisibility(View.GONE);
                }else{
                    ll_face_container.setVisibility(View.INVISIBLE);
                    ll_pic_photo_panel.setVisibility(View.GONE);
                }

            }else{
                keyboardHeight = 0 ;


            }
        }
    };

    private void initSoftInputMethod() {
        ((Activity)getContext()).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        currentInputMethod = METHOD_ADJUST_RESIZE;
        inputManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        receiver = new switchInputMethodReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.INPUT_METHOD_CHANGED");
        context.registerReceiver(receiver, filter);
        SystemConfigSp.instance().init(context);
        String str = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
        String[] strArr = str.split("\\.");
        if (strArr.length >= 3) {
            currentInputMethod = strArr[1];
            if (SystemConfigSp.instance().getStrConfig(SystemConfigSp.SysCfgDimension.DEFAULTINPUTMETHOD).equals(currentInputMethod)) {
                keyboardHeight = SystemConfigSp.instance().getIntConfig(SystemConfigSp.SysCfgDimension.KEYBOARDHEIGHT);
            } else {
                SystemConfigSp.instance().setStrConfig(SystemConfigSp.SysCfgDimension.DEFAULTINPUTMETHOD, currentInputMethod);
            }
        }
    }
    /**
     * 初始化数据库表情表里的数据，因为默认存在6个表情
     */
    private void initTableExpression() {
        /** 先判断默认表情是否已经插入到表中 */
        try {
//            dbUtils = DBUtils.instance();
//            Expression expression = null;
//            if (dbUtils != null) {
//                expression = (Expression)dbUtils.getObject(Expression.class, "label", "【");
//            }
//            if(expression == null) {
//                Expression e1= new Expression();
//                e1.setLabel("【");
//                e1.setContengt("【");
//                Expression e2= new Expression();
//                e2.setLabel("】");
//                e2.setContengt("】");
//                Expression e3= new Expression();
//                e3.setLabel("[");
//                e3.setContengt("[");
//                Expression e4= new Expression();
//                e4.setLabel("]");
//                e4.setContengt("]");
//                Expression e5= new Expression();
//                e5.setLabel("\"");
//                e5.setContengt("\"");
//                Expression e6= new Expression();
//                e6.setLabel("\"");
//                e6.setContengt("\"");
//                ArrayList<Expression> list = new ArrayList<Expression>();
//                list.add(e1);
//                list.add(e2);
//                list.add(e3);
//                list.add(e4);
//                list.add(e5);
//                list.add(e6);
//                dbUtils.saveObjectList((List) list);
//            }

            ExpressionEntity expressionEntity = DBInterface.instance().getExpressionEntityByLabel("【");
            if (expressionEntity == null) {
                String[] expressions = {"【", "】", "[", "]", "\"", "\""};
                List<ExpressionEntity> list = new ArrayList<>();
                for (int i = 0; i < expressions.length; i++) {
                    ExpressionEntity expressionEntity1 = new ExpressionEntity();
                    expressionEntity1.setLabel(expressions[i]);
                    expressionEntity1.setContent(expressions[i]);
                    list.add(expressionEntity1);
                }
                DBInterface.instance().insertOrUpdateAllExpressionEntity(list);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }

    }




    /**
     * 初始化数据
     */
    private void initData() {
//        if(dbUtils==null)
//            dbUtils = DBUtils.instance();
//        if(dbUtils.getObjectList(Expression.class)!=null) {
//            allGridViewData = dbInterface.loadAllExpressionEntity();
//        }
        if (dbInterface == null )
            dbInterface = DBInterface.instance();
        if (dbInterface.loadAllExpressionEntity() != null) {
            allGridViewData = dbInterface.loadAllExpressionEntity();
        }

        list_face_str=getFaceList();

    }


    /**
     * 获取本地的文字表情
     * @return
     */
    private List<String> getFaceList(){
        InputStream is=null;
        BufferedReader br=null;
        List<String> faces=new ArrayList<String>();
        try {
            is=getResources().getAssets().open("face_text.txt");
            br=new BufferedReader(new InputStreamReader(is,"UTF-8"));
            String  strTemp=null;
            while((strTemp=br.readLine())!=null){
                String str[]=strTemp.split("[||]");
                for(int i=0;i<str.length;i++)
                    if(!str[i].trim().equals(""))
                        faces.add(str[i].trim());
            }
            return  faces;

        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if(is!=null)
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            if(br==null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return faces;
    }
    /**
     * 初始化视图
     */
    private void initView() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        density = context.getResources().getDisplayMetrics().density;
        view = inflater.inflate(R.layout.face_input_view, this);
        view.findViewById(R.id.bt_biaoqing).setOnClickListener(this);
        view.findViewById(R.id.bt_add).setOnClickListener(this);
        et_input = (EditText)view.findViewById(R.id.et_input);

        /**如果有聊天历史 ,则输入框默认填入历史数据*/
        if(!TextUtils.isEmpty(lastChat))
            et_input.setText(lastChat);
        ll_viewpager= (LinearLayout) findViewById(R.id.ll_viewpager);
        ll_viewpager2= (LinearLayout) findViewById(R.id.ll_viewpager2);

        tv_pibiao= (TextView) findViewById(R.id.tv_face_pibiao);
        tv_face= (TextView) findViewById(R.id.tv_face_biaoqing);
        ll_delete= (LinearLayout) findViewById(R.id.ll_face_delete);
        ll_delete.setOnLongClickListener(this);
        ll_delete.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (handler != null) {
                        handler.removeMessages(0);
                    }
                }
                return false;
            }
        });
        ll_pic_photo_panel = (LinearLayout) findViewById(R.id.ll_pic_photo_panel);
        ll_face_container = (LinearLayout) findViewById(R.id.ll__face_input_view_holder);
        ll_face_container.measure(0,0);
        orignHeight = ll_face_container.getMeasuredHeight();
        ll_face_container.setVisibility(View.GONE);
        ll_pic_photo_panel.setVisibility(View.GONE);
//        params1 = (LayoutParams) ll_face_container.getLayoutParams();
        params2 = (LayoutParams) ll_pic_photo_panel.getLayoutParams();
        params2.height = orignHeight;
        view.findViewById(R.id.bt_add).setOnClickListener(this);
        view.findViewById(R.id.bt_panel).setOnClickListener(this);

        tv_pibiao.setOnClickListener(this);
        tv_face.setOnClickListener(this);
        ll_delete.setOnClickListener(this);
        et_input.setOnTouchListener(this);

        /** 文本框输入内容是显示发送按钮  */
        et_input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                );
                currentInputMethod = METHOD_ADJUST_RESIZE;
                ll_pic_photo_panel.setVisibility(GONE);
//                findViewById(R.id.ll__face_input_view_holder).setVisibility(GONE);
                panelTag = false;
                view.findViewById(R.id.bt_add).setVisibility(GONE);
                view.findViewById(R.id.tv_text_send).setVisibility(VISIBLE);
                /** 如果当前显示的是自定义表情键盘，说明用户再输入自定义表情，则直接返回，不用显示发送TextView */
                if (tag == MyTagEnum.BUTTON_BACKGROUND_KEYBOARD) {
                    return;
                }
                /** 隐藏去面板按钮,显示发送TextView发送TextView */

                findViewById(R.id.ll__face_input_view_holder).setVisibility(GONE);
            }
        });


        viewPager=(ViewPager)view. findViewById(R.id.viewpager);
        viewPager2= (ViewPager) view.findViewById(R.id.viewpager_face);

        list = new ArrayList<View>();
        for(int i=0; i<getPagerNum(); i++) {
            boolean isLastPager = (i==(getPagerNum()-1));
            list.add(getGridView(getEachGridViewData(i), isLastPager));
        }

        list2=new ArrayList<View>();
        for(int i=0;i<getPager2Num();i++){
            list2.add(getGridView2(getEachGridViewData2(i)));
        }

        viewPager.setAdapter(new MyViewPagerAdapter());
        viewPager2.setAdapter(new MyFaceViewAdapter(list2));
        viewPager2.setOffscreenPageLimit(3);
        viewPager.setOnPageChangeListener(new OnPageChangeListener() {
            int preposition = 0;

            @Override
            public void onPageSelected(int position) {
                preposition = position;
                setImageSelect(dot_imgs,position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

        viewPager2.setOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition2=position;
                setImageSelect(dot_imgs2,position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if(dot_imgs != null) {
            dot_imgs = null;
        }
        dot_imgs = new ImageView[getPagerNum()];
        if(dot_imgs2!=null){
            dot_imgs2=null;
        }
        dot_imgs2=new ImageView[getPager2Num()];
        /** 初始化小点 */
        initIndicator(view);
        initIndicator2(view);
        setItemBgColor(tv_pibiao);
    }

    /**
     * 通过计算获得ViewPager要显示的页卡数目
     * @return 页卡数目
     */
    private int getPagerNum() {
        /** 所有表情的个数+1（有一个加号）除以15,再+1 */
        //        return (allGridViewData.size()+1) / 15 + 1;
        try {
            return  (allGridViewData.size()+1)%15==0?(allGridViewData.size()+1)/15:(allGridViewData.size()+1)/15+1;
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * 获得对应页卡的数据源
     * @param position ViewPager的第几个页卡
     * @return 返回相应页卡中GridView需要的数据源（表情集合)
     */
    private List<ExpressionEntity> getEachGridViewData(int position) {
        List<ExpressionEntity> expression_list = new ArrayList<ExpressionEntity>();
        if(position<(getPagerNum()-1) && getPagerNum()>1) {      //装满15个表情的页面
            for(int i=0; i<15; i++) {
                expression_list.add((ExpressionEntity)allGridViewData.get(15*position + i));
            }
        } else {                                             //最后一个页卡
            for(int i=0; i<(allGridViewData.size()-15*position); i++) {
                expression_list.add((ExpressionEntity)allGridViewData.get(15*position + i));
            }
        }
        return expression_list;
    }

    /**
     * 初始化每个ViewPager需要的视图,ViewPager的每个页卡对应一个GridView
     * @return 一个页卡视图
     */
    private View getGridView(List<ExpressionEntity> list, boolean isLastPager) {
        View view = LayoutInflater.from(context).inflate(R.layout.face_input_view_gridview, null);
        gv_biaoqing = (GridView) view.findViewById(R.id.gv_input_face);
        gridViewAdapter = new BiaoqingGridViewAdapter(this,context,list,isLastPager);
        gv_biaoqing.setAdapter(gridViewAdapter);

        return view;
    }

    /**
     * 监听器
     * @param v 监听的控件
     */
    @Override
    public void onClick(View v) {
        InputMethodManager imm = (InputMethodManager)context. getSystemService(Context.INPUT_METHOD_SERVICE);
        switch(v.getId()) {
            case R.id.bt_biaoqing:

                switch (tag){
                    case BUTTON__BACKGROUND_LABEL:      //如果当前按钮背景是标签
//                        imm.hideSoftInputFromWindow(view.findViewById(R.id.et_input).getWindowToken(), 0);
//                        view.findViewById(R.id.ll__face_input_view_holder).setVisibility(
//                                View.VISIBLE);
//                        /** 设置按钮的背景 */
////                        findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.keyboard);
                        findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.keyboard_h);
//                        /** 隐藏发送TextView,显示去面板按钮 */
                        view.findViewById(R.id.tv_text_send).setVisibility(INVISIBLE);
                        view.findViewById(R.id.bt_add).setVisibility(VISIBLE);
//                        ll_pic_photo_panel.setVisibility(GONE);
                        tag = MyTagEnum.BUTTON_BACKGROUND_KEYBOARD;
                        isUnfold =true;
                        isKeyboardShow = false;
                        imm.hideSoftInputFromWindow(view.findViewById(R.id.et_input).getWindowToken(), 0);
                        handler.sendEmptyMessageDelayed(1,50);
                        break;
                    case BUTTON_BACKGROUND_KEYBOARD:    //如果当前按钮背景是键盘
//                        /** 显示软键盘 */

//
                        view.findViewById(R.id.ll__face_input_view_holder).setVisibility(
                                View.GONE);
//                        /** 设置按钮的背景 */
                        findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
//                        /** 隐藏去面板按钮,显示发送TextView发送TextView */
                        view.findViewById(R.id.bt_add).setVisibility(INVISIBLE);
                        ll_pic_photo_panel.setVisibility(GONE);
                        imm.showSoftInput(et_input, 0);
                        view.findViewById(R.id.tv_text_send).setVisibility(VISIBLE);
                        tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
                        isUnfold = false;
                        isKeyboardShow = true;
                        break;
                }

                break;
            case R.id.bt_add:
                imm.hideSoftInputFromWindow(view.findViewById(R.id.et_input).getWindowToken(), 0);
                findViewById(R.id.ll__face_input_view_holder).setVisibility(GONE);
                if(panelTag) {
                    ll_pic_photo_panel.setVisibility(GONE);
                    ll_face_container.setVisibility(View.VISIBLE);
                    isUnfold = true;
                    panelTag = false;
                } else {
                    ll_pic_photo_panel.setVisibility(VISIBLE);
                    ll_face_container.setVisibility(View.GONE);
                    isUnfold = true;
                    panelTag = true;
                }

                break;
            case R.id.tv_face_pibiao:
                ll_viewpager2.setVisibility(View.INVISIBLE);
                ll_viewpager.setVisibility(View.VISIBLE);
                viewPager2.setCurrentItem(0);
                setItemBgColor(tv_pibiao);
                break;
            case R.id.tv_face_biaoqing:
                ll_viewpager.setVisibility(View.INVISIBLE);
                ll_viewpager2.setVisibility(View.VISIBLE);
                viewPager.setCurrentItem(0);
                setItemBgColor(tv_face);
                break;
            case R.id.ll_face_delete:
                handlerDeleteEdit();
                break;
            case R.id.bt_panel:
                Intent i = new Intent(context, MsgMianBanActivity.class);
                i.putExtra(IntentConstant.INPUT_CONTENT, et_input.getText().toString().trim());
                ((Activity) context).startActivityForResult(i, SysConstant.ACTIVITY_RESULT_CODE_BOARD);
                break;
        }

    }

    /**长按时间处理  一般是删除操作 edittext*/
    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.ll_face_delete:
                handler.sendEmptyMessageDelayed(0,100);
                break;
            default:
                break;
        }
        return false;
    }


    /**handler 用以处理 editText的内容删减*/
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    handlerDeleteEdit();
                    handler.sendEmptyMessageDelayed(0,100);
                    break;
                case 1://显示表情及图片选取页面
                    view.findViewById(R.id.ll__face_input_view_holder).setVisibility(
                            View.VISIBLE);
                    ll_pic_photo_panel.setVisibility(GONE);
                    break;
                case 2: //隐藏表情及图片选取页
                    view.findViewById(R.id.ll__face_input_view_holder).setVisibility(
                            View.GONE);
                    view.findViewById(R.id.ll_pic_photo_panel).setVisibility(View.GONE);
                    break;
                case 3:
                    ll_pic_photo_panel.setVisibility(View.GONE);
                    ll_face_container.setVisibility(View.GONE);
                    view.findViewById(R.id.bt_add).setVisibility(INVISIBLE);
                    view.findViewById(R.id.tv_text_send).setVisibility(VISIBLE);
                    findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
                    tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
                    ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
                    currentInputMethod = METHOD_ADJUST_RESIZE;
                    break;
                case 4:
                    et_input.requestFocus();
                    break;
                default:
                    break;
            }
        }
    };

    private void handlerDeleteEdit(){
        int selectionStart = et_input.getSelectionStart();
        if (selectionStart > 0) {
            String body = et_input.getText().toString();
            if (!TextUtils.isEmpty(body)) {
                String tempStr = body.substring(0, selectionStart);
//                    int i = tempStr.lastIndexOf("[");
//                    if (i != -1) {
//                        CharSequence cs = tempStr
//                                .subSequence(i, selectionStart);
//                        if (cs.equals("[fac")) {
//                            et_input.getEditableText().delete(i, selectionStart);
//                            return;
//                        }
//                    }
                et_input.getEditableText().delete(tempStr.length() - 1,
                        selectionStart);
            }
        }
    }


    /**
     * ViewPager的适配器
     */
    private class MyViewPagerAdapter extends PagerAdapter {

        /**
         *
         * @return 页卡的数目
         */
        @Override
        public int getCount() {
            return list.size();
        }

        /**
         *
         * @param container
         * @param position
         * @param object
         */
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            /** 删除页卡 */
            container.removeView(list.get(position));
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            /** 添加页卡 */
            container.addView(list.get(position));
            return list.get(position);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0==arg1;
        }

    }

    /**
     * 对外提供的接口：在输入框中的内容后面追加表情
     * @param text
     */
    public void addBiaoqing(String text) {
        et_input.append(text);
    }

    /**
     * 对外提供的接口，用于返回自定义输入表情面板的Button或者输入框EditText
     * @return 返回输入框旁边的按钮（表情按钮和去面板的按钮)
     */
    public View getView(MyViewEnum buttonEnum){
        switch(buttonEnum) {
            case EDITTEXT_INPUT:
                /** 返回文本输入框 */
                return view.findViewById(R.id.et_input);
            case BUTTON_BIAOQING_SEND:
                /** 返回发送表情的按钮 */
                return view.findViewById(R.id.bt_biaoqing_send);
            case TEXTVIEW_TEXT_SEND:
                /** 返回发送文本的TextView */
                return  view.findViewById(R.id.tv_text_send);
            case BUTTON_GO_MIANBAN:
                /** 返回去面板的按钮 */
                return view.findViewById(R.id.bt_panel);
            case LL_FACE_INPUT_VIEW_HOLDER:
                /** 返回放置表情的LinearLayout */
                return view.findViewById(R.id.ll__face_input_view_holder);
            case BUTTON_ADD:
                return view.findViewById(R.id.bt_add);
            case IMAGE_VIEW_TAKE_CAMERA:
                return view.findViewById(R.id.bt_take_camera);
            case IMAGE_VIEW_TAKE_PHOTO:
                return view.findViewById(R.id.bt_take_photo);
            case BUTTON_BIAOQING:
                return view.findViewById(R.id.bt_biaoqing);
        }
        return null;
    }

    /**
     * 对外提供的接口，用于刷新表情界面
     */
    public void refresh() {
        initData();
        initView();
    }

    /**
     * 初始化引导图标
     * 动态创建多个小圆点，然后组装到线性布局里
     */
    private void initIndicator(View view){
        ImageView imgView;
        View v = view.findViewById(R.id.ll_dot_container);
        try {
            ((ViewGroup)v).removeAllViews();
        }catch (Exception e){
            e.printStackTrace();
        }
        for (int i = 0; i < getPagerNum(); i++) {
            imgView = new ImageView(context);
            LayoutParams params_linear = new LayoutParams(10,10);
            params_linear.setMargins(5, 0, 0, 0);
            imgView.setLayoutParams(params_linear);
            imgView.setBackgroundResource(R.drawable.selector_face_dots);
            dot_imgs[i] = imgView;

            if (i == 0) {    // 初始化第一个为选中状态
                dot_imgs[i].setSelected(true);
            } else {
                dot_imgs[i].setSelected(false);
            }
            ((ViewGroup)v).addView(dot_imgs[i]);
        }
    }


    /**
     * 这里是表情的有关处理（第二个表情界面）
     */



    private int getPager2Num(){
        int pages=list_face_str.size()%12==0?list_face_str.size()/12:list_face_str.size()/12+1;
        return pages;
    }

    private void initIndicator2(View view){
        ImageView imgView;
        View v = view.findViewById(R.id.ll_dot_container2);
        /**避免重复添加 ，最初先清空view的所有子视图*/
        try {
            ((ViewGroup)v).removeAllViews();
        }catch (Exception e){
            e.printStackTrace();
        }

        int pages=getPager2Num();
        for (int i = 0; i < pages; i++) {
            imgView = new ImageView(context);
            LayoutParams params_linear = new LayoutParams(10,10);
            params_linear.setMargins(5, 0, 0, 0);
            imgView.setLayoutParams(params_linear);
            imgView.setBackgroundResource(R.drawable.selector_face_dots);
            dot_imgs2[i] = imgView;

            if(i==0){
                dot_imgs2[i].setSelected(true);
            }else {
                dot_imgs2[i].setSelected(false);
            }
            ((ViewGroup)v).addView(dot_imgs2[i]);
        }
    }

    /**设置滑动viewpager时 小点的选中状态*/
    private void setImageSelect(ImageView[] img,int position){
        for(int i=0;i<img.length;i++){
            img[i].setSelected(false);
        }
        img[position].setSelected(true);
    }

    private List<String> getEachGridViewData2(int position) {
        List<String> list = new ArrayList<String>();
        if(position<getPager2Num()-1){
            for(int i=12*position;i<12*position+12;i++){
                list.add(list_face_str.get(i));
            }
        }else{
            for(int i=12*position;i<list_face_str.size();i++){
                list.add(list_face_str.get(i));
            }
        }
        return list;
    }


    private View getGridView2(final List<String> list) {
        View view = LayoutInflater.from(context).inflate(R.layout.face_input_view_gridview2, null);
        gv_biaoqing2 = (LineGridView) view.findViewById(R.id.gv_input_face);
        gv_biaoqing2.setNumColumns(4);
        gridViewAdapter2 = new BiaoqingGridViewAdapter2(this,context,list);
        gv_biaoqing2.setAdapter(gridViewAdapter2);
        gv_biaoqing2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                et_input.append(list.get(position));
            }
        });
        return view;
    }

    /**         这里手动设置，写drawble太麻烦          */
    private void setItemBgColor(View view){
        switch (view.getId()){
            case R.id.tv_face_pibiao:
                tv_pibiao.setBackgroundColor(context.getResources().getColor(R.color.grey_d1cccc));
                tv_pibiao.setTextColor(context.getResources().getColor(R.color.white));
                tv_face.setBackgroundColor(context.getResources().getColor(R.color.grey_eeecec));
                tv_face.setTextColor(context.getResources().getColor(R.color.white_a1a1a1));
                break;
            case R.id.tv_face_biaoqing:
                tv_face.setBackgroundColor(context.getResources().getColor(R.color.grey_dedbdb));
                tv_face.setTextColor(context.getResources().getColor(R.color.white));
                tv_pibiao.setBackgroundColor(context.getResources().getColor(R.color.grey_e6e4e4));
                tv_pibiao.setTextColor(context.getResources().getColor(R.color.white_a1a1a1));
                break;
        }
    }

    /**        这是 edittext的触碰监听，用于收回表情栏 展开键盘 **/
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v==et_input){
            view.findViewById(R.id.bt_add).setVisibility(INVISIBLE);
            view.findViewById(R.id.tv_text_send).setVisibility(VISIBLE);
            ll_pic_photo_panel.setVisibility(View.GONE);
            ll_face_container.setVisibility(View.GONE);
            findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
            tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
//                if(showHeight!=0&&currentInputMethod == METHOD_ADJUST_NOTHIN){
//                    params1.height = showHeight;
//                    params2.height = showHeight;
//                    ll_pic_photo_panel.setVisibility(View.GONE);
//                    ll_face_container.setVisibility(View.INVISIBLE);
//                }else{
//                    ll_pic_photo_panel.setVisibility(View.GONE);
//                    ll_face_container.setVisibility(View.GONE);
//                    ((Activity)context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
//                    currentInputMethod = METHOD_ADJUST_RESIZE;
//                }
            isKeyboardShow =true;

        }
        return false;
    }

    private class switchInputMethodReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("android.intent.action.INPUT_METHOD_CHANGED")) {
                String str = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
                String[] strArr = str.split("\\.");
                if (strArr.length >= 3) {
                    String strCompany = strArr[1];
                    if (!strCompany.equals(currentInputMethod)) {
                        currentInputMethod = strCompany;
                        SystemConfigSp.instance().setStrConfig(SystemConfigSp.SysCfgDimension.DEFAULTINPUTMETHOD, currentInputMethod);
                        keyboardHeight = 0;
                        ll_pic_photo_panel.setVisibility(View.GONE);
                        ll_face_container.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    public void hideKeyborad(){
        if(isKeyboardShow || isUnfold) {
            InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(et_input.getWindowToken(), 0);
//                params1.height = orignHeight;
//                params2.height = orignHeight;
            ll_pic_photo_panel.setVisibility(View.GONE);
            ll_face_container.setVisibility(View.GONE);
            view.findViewById(R.id.bt_add).setVisibility(INVISIBLE);
            view.findViewById(R.id.tv_text_send).setVisibility(VISIBLE);
            findViewById(R.id.bt_biaoqing).setBackgroundResource(R.drawable.edit_input_h);
            tag = MyTagEnum.BUTTON__BACKGROUND_LABEL;
            isUnfold = false;
            isKeyboardShow = false;
        }
    }
}
