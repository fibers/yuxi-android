package com.yuxip.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yuxip.R;

/**
 * Created by Ly on 2015/8/18.
 * 主界面的切换控件，其实没多大用处- -！
 */
public class TabButtonE extends RelativeLayout {

    private TextView mtitle;            //标题内容
    private TextView mline;             //底部线条
    private Context mContext;
    private int color_select;           //选中颜色
    private int color_unselect;         //常规颜色

    public TabButtonE(Context context) {
        super(context);
        this.mContext = context;
        initFlate();
        initColor();
    }

    public TabButtonE(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        initFlate();
        initColor();
    }

    public TabButtonE(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initFlate();
        initColor();
    }

    private void initFlate() {
        LayoutInflater.from(mContext).inflate(R.layout.tt_navi_tab_buttone, this, true);
        mtitle = (TextView) findViewById(R.id.tab_btn_default);
        mline = (TextView) findViewById(R.id.tab_btn_line);

    }

    private void initColor() {
        color_select = mContext.getResources().getColor(R.color.pink);
        color_unselect = mContext.getResources().getColor(R.color.grey_656464);
    }

    public void setButtonSelect(boolean select) {
        if (select) {
            mtitle.setTextColor(color_select);
            mline.setVisibility(View.VISIBLE);
        } else {
            mtitle.setTextColor(color_unselect);
            mline.setVisibility(View.INVISIBLE);
        }
    }

    public void setTitleColor(int color_select, int color_unselect) {
        this.color_select = color_select;
        this.color_unselect = color_unselect;
    }

    public void setTitle(String title) {
        this.mtitle.setText(title);
    }

    public void setLineColor(int color) {
        this.mline.setBackgroundColor(color);
    }

    public void setTitleTextSize(float size) {
        this.mtitle.setTextSize(size);
    }
}
