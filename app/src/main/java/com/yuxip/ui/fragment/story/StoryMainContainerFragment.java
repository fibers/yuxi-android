package com.yuxip.ui.fragment.story;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.R;
import com.yuxip.imservice.event.ChannelSelectEvent;
import com.yuxip.imservice.manager.http.ChannelSelectDataManager;
import com.yuxip.ui.customview.ChannelSelectMore;
import com.yuxip.ui.customview.ChannelSelectScrollview;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.ViewPagerScroller;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;


/**
 * Created by Ly on 2015/8/20.
 * 用于首页剧场剧的容器 里面的自碎片对应的是 HotFramgent
 */
public class StoryMainContainerFragment extends XYBaseFragment implements ChannelSelectScrollview.ChannelSelectCallBack, View.OnClickListener {
    @InjectView(R.id.viewpager_story_play_container)
    ViewPager viewPager;
    @InjectView(R.id.channelselect_view)
    ChannelSelectScrollview channelSelectScrollview;
    @InjectView(R.id.channelselect_view_more)
    ChannelSelectMore channelSelectMore;
    private View view;
    private List<Fragment> mFragmentList = new ArrayList<>();
    private StoryMainContainerFragment.MyAdapter adapter;
    private ViewPagerScroller viewPagerScroller;
    private List<NodesEntity> listShowType = new ArrayList<>();    //请求过来的node的集合
    private List<NodesEntity> list_showDefault = new ArrayList<>();   //用于传递的nodes的集合,这是与本地数据比对过的（假如服务器删除了某个标签）
    private DBInterface dbInterface;
    private ImageView img_more;     //这是ChannelSelectScrollView 中的下拉图片 ,点击展开 ChannelSelectMore
    private TextView tv_title;      //这是ChannelSelectScrollView 中的标题 ,下拉时显示,显示时隐藏
    private boolean isUnFold;         //判断  选择界面 是否是展开状态


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onEventMainThread(ChannelSelectEvent event){
        switch (event.eventType){
            case TYPE_SET_STORY_PLAY:
                setData();
                break;
            case TYPE_HIDE_MORE:
                showOrHideSelectView();
                break;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_story_play_container, null);
            ButterKnife.inject(this, view);
            initRes();
        }
        return view;
    }

    private void initRes(){
        img_more = (ImageView) channelSelectScrollview.getView(ChannelSelectScrollview.ViewType.TYPE_IMGMORE);
        tv_title = (TextView) channelSelectScrollview.getView(ChannelSelectScrollview.ViewType.TYPE_TEXTTILE);
        channelSelectScrollview.getView(ChannelSelectScrollview.ViewType.TYPE_VIEWLINE).setVisibility(View.VISIBLE);
        img_more.setOnClickListener(this);
        img_more.setVisibility(View.VISIBLE);
        dbInterface = DBInterface.instance();
    }


    private void setData() {
        this.listShowType = ChannelSelectDataManager.getInstance().getListStoryAll();
        /**    给node打标签，与自戏区分      */
        channelSelectScrollview.setBackgroundColor(Color.parseColor("#ffebebeb"));
        channelSelectScrollview.setTextsSize(14f);
        ChannelSelectDataManager.getInstance().initListShow(this.listShowType,"0");
        this.list_showDefault = ChannelSelectDataManager.getInstance().getListStoryShow();
        channelSelectScrollview.setDataList(this.list_showDefault, this);
        channelSelectScrollview.setTitleContent("我的频道");
        /**      0表示剧的数据库存储      **/
        channelSelectMore.setData(this.listShowType, "0");
        initFragmentList();
        initViewpager();
    }

    /**
     *
     * 二次点击的刷新,其中channelselectmore没必要刷新,其中数据存储在channelselectmore里操作
     *
     */
    private void setShowType() {
        if (dbInterface == null)
            dbInterface = DBInterface.instance();
        list_showDefault = dbInterface.loadAllNodesEntityByEntityType("0");
        channelSelectScrollview.setDataList(this.list_showDefault, this);
        initFragmentList();
        adapter.notifyDataSetChanged();
    }

    private void initViewpager() {
        adapter = new StoryMainContainerFragment.MyAdapter(getChildFragmentManager());
        /**  这里不用管 viewpager源码里面scroller是这么定义的   */
        viewPagerScroller = new ViewPagerScroller(viewPager.getContext(), new Interpolator() {
            @Override
            public float getInterpolation(float t) {
                t -= 1.0f;
                return t * t * t * t * t + 1.0f;
            }
        });
        viewPagerScroller.setScrollDuration(500);
        viewPagerScroller.initViewPagerScroll(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                /**     滑动或切换结束后重置轮滑速度    */
                channelSelectScrollview.scroll(position);
                viewPagerScroller.setScrollDuration(500);
                viewPagerScroller.initViewPagerScroll(viewPager);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(adapter);
    }

    /**     填充fragmentList集合     */
    private void initFragmentList() {
        if (listShowType == null)
            return;
        mFragmentList.clear();
        for (int i = 0; i < list_showDefault.size(); i++) {
            HotFragment hotFragment = new HotFragment();
            Bundle bundle = new Bundle();
            bundle.putString("type", list_showDefault.get(i).getId());
            hotFragment.setArguments(bundle);
            mFragmentList.add(hotFragment);
        }
    }

    /**
     * 这是接口回调的方法
     */
    @Override
    public void setViewpager(int position) {
        /**    点击滑动条上的内容时，禁用viewPager的滑动 直接跳转   */
        if(viewPagerScroller!=null){
            viewPagerScroller.setScrollDuration(0);
            viewPagerScroller.initViewPagerScroll(viewPager);
        }
        viewPager.setCurrentItem(position);
    }

    @Override
    public void onClick(View v) {
        /**       这是ChannelSelectScrollview的下拉图片  */
        if (v == img_more) {
            showOrHideSelectView();
        }
    }


    /**
     *     展示或隐藏下拉列表    *
     */
    public void showOrHideSelectView() {
        if (isUnFold) {
            isUnFold = false;
            ChannelSelectDataManager.getInstance().setIsUnfold(false);
            img_more.setSelected(false);
            if (channelSelectMore.getIsChanged()) {
                /**      这里执行数据库操作       **/
                channelSelectMore.resetDbData();
                setShowType();
                viewPagerScroller.setScrollDuration(0);
                viewPagerScroller.initViewPagerScroll(viewPager);
                /**  执行下拉选中或取消后,重绘viewpager 同时 指向第一个碎片   */
                viewPager.setCurrentItem(0);
                channelSelectMore.setIsChanged(false);
            }
            tv_title.setVisibility(View.GONE);

            channelSelectMore.setVisibility(View.GONE);
        } else {
            isUnFold = true;
            ChannelSelectDataManager.getInstance().setIsUnfold(true);
            channelSelectMore.setVisibility(View.VISIBLE);
            img_more.setSelected(true);
            tv_title.setVisibility(View.VISIBLE);
        }
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }

}
