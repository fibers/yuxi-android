package com.yuxip.ui.fragment.other;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.ResponseFamily;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.config.SharedPreferenceValues;
import com.yuxip.entity.FamilyInfoEntity;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.ui.activity.other.FamilyDetailsActivity;
import com.yuxip.ui.adapter.BaseRecyclerViewAdapter;
import com.yuxip.ui.adapter.FBOAdapter;
import com.yuxip.ui.customview.MyRecyclerView;
import com.yuxip.ui.customview.ProgressView;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.SharedPreferenceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ly on 2015/9/4.
 * 家族分类的子碎片
 */
public class FamilyAllFragment extends XYBaseFragment implements BaseRecyclerViewAdapter.OnItemClickListener {

    private View view;
    private MyRecyclerView recyclerView;
    private ProgressView emptyView;
    private String type;
    private int index;
    private String loginId;
    private FBOAdapter adapter;
    private List<FamilyInfoEntity> familyList = new ArrayList<>();

    public static FamilyAllFragment instance(String type) {
        FamilyAllFragment familyAllFragment = new FamilyAllFragment();
        Bundle bundle = new Bundle();
        bundle.putString(IntentConstant.TYPE, type);
        familyAllFragment.setArguments(bundle);
        return familyAllFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_family_all, null);
        initRecycleView();
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recyclerView.post(new Runnable() {
            @Override
            public void run() {
                refreshData();
            }
        });
    }

    private void initRecycleView() {
        type = getArguments().getString(IntentConstant.TYPE);
        loginId = IMLoginManager.instance().getLoginId() + "";
        recyclerView = (MyRecyclerView) view.findViewById(R.id.recycleview_fragment_family_all);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setEmptyView(R.layout.progress_layout);
        recyclerView.showEmptyView();
        emptyView = (ProgressView) recyclerView.getEmptyView();
        emptyView.setTextOnClickListener(new ProgressView.TextOnClickListener() {
            @Override
            public void textOnClick() {
                refreshData();
            }
        });
        recyclerView.setDefaultOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                    }
                });
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int i, int i1) {  //第一个参数是item的总数； 第二个参数是屏幕底部显示的，item的position
                loadData();
            }
        });
        adapter = new FBOAdapter(familyList, getActivity());
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
    }


    private void refreshData() {
        index = 0;
        getAllFamily();
    }

    private void loadData() {
        index = familyList.size();
        getAllFamily();
    }


    private void getAllFamily() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("index", index + "");
        params.addParams("count", "50");
        params.addParams("type", type);
        OkHttpClientManager.postAsy(ConstantValues.GetFamilyListByType, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveInfoResponse(response);
                recyclerView.setRefreshing(false);
            }

            @Override
            public void onError(Request request, Exception e) {
                recyclerView.setRefreshing(false);
                showEmptyInfo();
            }
        });
    }

    /**
     * 获取家族信息
     *
     * @param result
     */
    private void onReceiveInfoResponse(String result) {
        ResponseFamily response = new Gson().fromJson(result, ResponseFamily.class);
        if (response.getResult().equals("1")) {
            List<FamilyInfoEntity> familyListE = response.getFamilylist();
            if (familyListE == null || familyListE.size() == 0) {
                adapter.setCustomLoadMoreView(null);
                recyclerView.disableLoadmore();
                return;
            }
            if (index == 0) {
                this.familyList.clear();
            }
            this.familyList.addAll(familyListE);
            adapter.notifyDataSetChanged();

            if (familyListE.size() < 50) {
                adapter.setCustomLoadMoreView(null);
                recyclerView.disableLoadmore();
            }
        } else {
            adapter.setCustomLoadMoreView(null);
            recyclerView.disableLoadmore();
        }
    }


    private void showEmptyInfo() {
        emptyView.showFailureText("未得到数据，请点击重试", true);
        recyclerView.showEmptyView();
    }


    @Override
    public void onResume() {
        super.onResume();
        SharedPreferenceUtils.saveBooleanDate(getActivity(), SharedPreferenceValues.HOME_KEY, false);
    }

    @Override
    public void onItemClick(int position, RecyclerView.ViewHolder holder) {
        if (isMember(familyList.get(position).getId())) {
            Intent i = new Intent(getActivity(), FamilyDetailsActivity.class);
            i.putExtra(IntentConstant.SESSION_KEY, "2_" + familyList.get(position).getId());
            i.putExtra(IntentConstant.FAMILY_INFO_START_FROM, "contact");
            startActivity(i);
        } else {
            IMUIHelper.openFamilyDataActivity(getActivity(), IntentConstant.FamilyDataActivityType.TYPE_NOT_MEMBER, familyList.get(position).getId());
        }
    }

    private boolean isMember(String id) {
        return YXFamilyManager.instance().isMember(Integer.valueOf(id));
    }
}
