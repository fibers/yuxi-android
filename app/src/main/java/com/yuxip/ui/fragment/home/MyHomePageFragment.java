package com.yuxip.ui.fragment.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.umeng.analytics.MobclickAgent;
import com.yuxip.JsonBean.HomeInfo;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.CreateStoryEvent;
import com.yuxip.imservice.event.CreateStoryTopicEvent;
import com.yuxip.imservice.event.SquareCommentEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.add.ReleaseTopicActivity;
import com.yuxip.ui.activity.home.MySettingActivity;
import com.yuxip.ui.activity.other.PersonalInfoActivity;
import com.yuxip.ui.activity.story.ZiXiDetailsActivity;
import com.yuxip.ui.adapter.HomePageAdapter;
import com.yuxip.ui.customview.CustomHeadImage;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.ui.widget.MyListView;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Administrator on 2015/7/9.
 * 这里与UserHomePageActivity基本一致，差别不太大
 */
public class MyHomePageFragment extends XYBaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener {
    @InjectView(R.id.iv_home_setting)
    ImageView ivHomeSetting;
    @InjectView(R.id.tv_word_num)
    TextView tvWordNum;
    @InjectView(R.id.iv_home_head_img)
    CustomHeadImage ivHomeHeadImg;
    @InjectView(R.id.iv_home_edit)
    TextView ivHomeEdit;
    @InjectView(R.id.tv_home_name)
    TextView tvHomeName;
    @InjectView(R.id.iv_sex_icon)
    ImageView ivSexIcon;
    @InjectView(R.id.tv_id)
    TextView tvId;
    @InjectView(R.id.tv_introduce)
    TextView tvIntroduce;
    @InjectView(R.id.tv_story_num)
    TextView tvStoryNum;
    @InjectView(R.id.ll_home_story)
    LinearLayout llHomeStory;
    @InjectView(R.id.tv_book_num)
    TextView tvBookNum;
    @InjectView(R.id.ll_home_book)
    LinearLayout llHomeBook;
    @InjectView(R.id.tv_topic_num)
    TextView tvTopicNum;
    @InjectView(R.id.ll_home_topic)
    LinearLayout llHomeTopic;
    @InjectView(R.id.homelist)
    MyListView homelist;
    @InjectView(R.id.ll_edit_story_btn)
    LinearLayout editStoryBtn;
    @InjectView(R.id.ll_blue)
    LinearLayout llBlue;
    @InjectView(R.id.ll_home_top_bg)
    RelativeLayout homeTopbg;
    @InjectView(R.id.tv_story_des)
    TextView tvStoryDes;
    @InjectView(R.id.tv_edit_des)
    TextView tvEditDes;
    @InjectView(R.id.iv_test_user)
    ImageView ivTesterUser;
    @InjectView(R.id.iv_signed_user)
    ImageView ivSignedUser;

    @InjectView(R.id.tv_desc)
    TextView tvDes;
    @InjectView(R.id.tv_mstory)
    TextView tvTitle1;
    @InjectView(R.id.tv_mbook)
    TextView tvTitle2;
    @InjectView(R.id.tv_mtopic)
    TextView tvTitle3;

    /**
     * ************    这里是后添加的*******************
     */
    @InjectView(R.id.swipe_layout_home_page)
    SwipeRefreshLayout swipeRefreshLayout;
    @InjectView(R.id.rel_role_apply_homepage)
    RelativeLayout rel_role_apply;
    @InjectView(R.id.rel_family_homepage)
    RelativeLayout rel_family;
    @InjectView(R.id.rel_collect_homepage)
    RelativeLayout rel_collect;
    @InjectView(R.id.rel_favor_homepage)
    RelativeLayout rel_favor;
    @InjectView(R.id.tv_familynum_homepage)
    TextView tv_familynum;
    @InjectView(R.id.tv_collectnum_homepage)
    TextView tv_collectnum;
    @InjectView(R.id.tv_favornum_homepage)
    TextView tv_favormum;
    @InjectView(R.id.tv_applytnum_homepage)
    TextView tv_applynum;
    @InjectView(R.id.iv_singwriter_user_home)
    ImageView iv_signWriter;       //是否是签约写手

    private boolean isDataOk = false;

    private String loginId;
    private LinearLayout mLineartwo;
    private List<HomeInfo.PersoninfoEntity.MyHomeEntity> mystorys = new ArrayList<>();
    private HomePageAdapter adapter;
    private HomeInfo.PersoninfoEntity personinfo;
    private List<TextView> list_title = new ArrayList<>();
    private List<TextView> list_num = new ArrayList<>();
    private boolean hasLoad = false;
    private Logger logger;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
        loginId = IMLoginManager.instance().getLoginId() + "";
        logger = Logger.getLogger(MyHomePageFragment.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_page, container, false);
        ButterKnife.inject(this, view);
        initRes();
        initTextList();
        initListener();
        mLineartwo = (LinearLayout) view.findViewById(R.id.ll_blue);
        setSelected(0);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        adapter = new HomePageAdapter(getActivity(), mystorys);
        homelist.setAdapter(adapter);
        homelist.setFocusable(false);
        homelist.setOnItemClickListener(this);
        ReqHomeInfo();
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }



    public void onEventMainThread(SquareCommentEvent event) {
        switch (event.eventType){
            case TYPE_DELETE_STORY_TOPIC:
                ReqHomeInfo();
                break;
        }
    }

    public void onEventMainThread(CreateStoryTopicEvent event) {
        switch (event.eventType){
            case TYPE_CREATE_TOPIC:
            case TYPE_CREATE_PLAY:
            case TYPE_CREATE_STORY:
                ReqHomeInfo();
                break;
            case TYPE_CREATE_FAMILY:
            case TYPE_DISMISS_FAMILY:
                ReqHomeInfo();
                break;
        }
    }

    public void onEventMainThread(CreateStoryEvent event) {
        switch (event.eventType){
            case TYPE_CREATE_STORY_SUCCESS:
                ReqHomeInfo();
                break;
        }
    }

    private void initListener() {
        rel_role_apply.setVisibility(View.VISIBLE);
        rel_role_apply.setOnClickListener(this);
        rel_family.setOnClickListener(this);
        rel_collect.setOnClickListener(this);
        rel_favor.setOnClickListener(this);
    }

    private int currentSelected = 0;

    private void setSelected(int tag) {
        currentSelected = tag;
        setItemSelect(currentSelected);
    }

    /**
     * 初始化下面选择的text集合
     */
    private void initTextList() {
        tvTitle1.setText("我的剧");
        tvTitle2.setText("我的自戏");
        tvTitle3.setText("我的话题");
        list_title.add(tvTitle1);
        list_title.add(tvTitle2);
        list_title.add(tvTitle3);
        list_num.add(tvStoryNum);
        list_num.add(tvBookNum);
        list_num.add(tvTopicNum);
    }

    /**
     * position 从1开始  1 , 2 , 3 , 4
     */
    private void setItemSelect(int position) {
        for (int i = 0; i < list_title.size(); i++) {
            list_title.get(i).setSelected(false);
            list_num.get(i).setSelected(false);
        }
        list_title.get(position).setSelected(true);
        list_num.get(position).setSelected(true);

    }

    public void ReqHomeInfo() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("personid", loginId);
        OkHttpClientManager.postAsy(ConstantValues.GetPersonInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try{
                    //todo bug
                    swipeRefreshLayout.setRefreshing(false);
                }catch (Exception e){
                    logger.e(e.toString());
                }

                IMApplication.IS_REFRESH = false;
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getString("result").equals("1")) {
                        onReceiveHomeInfoResponse(response);
                    } else {
                        T.show(getActivity().getApplicationContext(), "数据错误，下拉刷新重试", 0);
                    }
                } catch (Exception e) {
                    logger.e(e.toString());
                }

            }

            @Override
            public void onError(Request request, Exception e) {
                try{
                    swipeRefreshLayout.setRefreshing(false);
                }catch (Exception e1){
                    e.printStackTrace();
                }
                T.show(getActivity().getApplicationContext(), R.string.network_err, 1);
            }
        });
    }

    /**
     * 解析数据
     *
     * @param result
     */
    private void onReceiveHomeInfoResponse(String result) {
        HomeInfo homeinfo = new Gson().fromJson(result, HomeInfo.class);
        if (homeinfo.getResult().equals("1")) {
            personinfo = homeinfo.getPersoninfo();
            HomeEntityManager.getInstance().setMypersonInfo(personinfo);
            onResAddData(personinfo);
        }
    }

    /**
     * 添加数据
     *
     * @param personinfo
     */
    private void onResAddData(HomeInfo.PersoninfoEntity personinfo) {
        //我喜欢的剧
        if (mystorys != null)
            mystorys.clear();
        mystorys.addAll(personinfo.getMystorys());
        adapter.notifyDataSetChanged();
        if (editStoryBtn == null) return;
        if (mystorys == null || mystorys.size() == 0) {
            editStoryBtn.setVisibility(View.VISIBLE);
            homelist.setVisibility(View.GONE);
            tvStoryDes.setText("你还没有发布过剧");
            tvEditDes.setText("写剧本");
        }

        tvId.setText("语戏号: " + personinfo.getId());
        if (!TextUtils.isEmpty(personinfo.getWordcount())) {
            tvWordNum.setText(personinfo.getWordcount() + "字");
        }
        tvHomeName.setText(personinfo.getNickname());
        //男女
        String sex = personinfo.getGender();
        if (sex.equals("1")) {
            ivSexIcon.setImageResource(R.drawable.home_man_icon);
            homeTopbg.setBackgroundResource(R.drawable.bg_info_blue);
        } else {
            ivSexIcon.setImageResource(R.drawable.home_woman_icon);
            homeTopbg.setBackgroundResource(R.drawable.bg_info_pink);
        }


        /**      判断是是否是签约写手      */
        String type = personinfo.getType();
        if (type != null) {
            if (type.equals("1") || type.equals("3")) {
                //签约用户
                iv_signWriter.setVisibility(View.VISIBLE);
            } else {
                iv_signWriter.setVisibility(View.GONE);
            }

            if (type.equals("2") || type.equals("3")) {
                //判断是否签约用户
                //暂时隐藏
                ivTesterUser.setVisibility(View.INVISIBLE);
            } else {
                ivTesterUser.setVisibility(View.INVISIBLE);
            }
        }
        //简介
        String intro = personinfo.getIntro();
        if (intro.equals("") || intro.equals(null)) {
            tvIntroduce.setText("暂无简介");
        } else {
            tvIntroduce.setText(personinfo.getIntro());
        }
        //喜欢的数
        if (personinfo.getMyfavor() != null) {
            tv_favormum.setText(personinfo.getMyfavor().size() + "");
        }
        //故事
        if (personinfo.getMyselfstorys() != null) {
            tvBookNum.setText(personinfo.getMyselfstorys().size() + "");
        }
        //话题
        if (personinfo.getMytopics() != null) {
            tvTopicNum.setText(personinfo.getMytopics().size() + "");
        }
        //家族
        if (personinfo.getMyfamily() != null) {
            tv_familynum.setText(personinfo.getMyfamily().size() + "");
        }
        //收藏
        if (personinfo.getMycollects() != null) {
            tv_collectnum.setText(personinfo.getMycollects().size() + "");
        }
        tvStoryNum.setText(mystorys.size() + "");
        tv_applynum.setText(personinfo.getApplycount()==null?"0":personinfo.getApplycount());
        ivHomeHeadImg.loadImage(personinfo.getPortrait(), "0");
        hasLoad = true;  //这里把加载完成的标志设为true
        switch (currentSelected) {
            case 0:
                onClick(llHomeStory);
                break;
            case 1:
                onClick(llHomeBook);
                break;
            case 2:
                onClick(llHomeTopic);
                break;
        }
        isDataOk = true;
    }

    private void initRes() {
        homelist.setSelected(false);
        ivHomeSetting.setOnClickListener(this);
        ivHomeEdit.setOnClickListener(this);
        llHomeBook.setOnClickListener(this);
        llHomeStory.setOnClickListener(this);
        llHomeTopic.setOnClickListener(this);
        swipeRefreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.pink));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ReqHomeInfo();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }


    @Override
    public void onClick(View v) {
        if (!hasLoad) {
            Toast.makeText(getActivity(), "正在加载数据，请稍候", Toast.LENGTH_LONG).show();
            return;
        }
        switch (v.getId()) {
            //设置
            case R.id.iv_home_setting:
                IMUIHelper.openActivity(getActivity(), MySettingActivity.class);
                break;
            //编辑
            case R.id.iv_home_edit:
                startActivityForResult(new Intent(getActivity(), PersonalInfoActivity.class), 1);
                break;

            //剧
            case R.id.ll_home_story:
                setSelected(0);
                if (personinfo != null && personinfo.getMystorys() != null && personinfo.getMystorys().size() > 0) {
                    if (editStoryBtn == null)
                        return;
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMystorys());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                } else {
                    editStoryBtn.setVisibility(View.VISIBLE);
                    homelist.setVisibility(View.GONE);
                    tvEditDes.setVisibility(View.VISIBLE);
                    tvDes.setVisibility(View.VISIBLE);
                    tvStoryDes.setText("你还没有发布过剧");
                    tvEditDes.setText("写剧本");
                    tvEditDes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            IMUIHelper.openSelectStoryClassActivity(getActivity());
                            IMUIHelper.openCreateStoryActivity_New(getActivity());
                        }
                    });
                }
                break;

            //故事
            case R.id.ll_home_book:
                setSelected(1);
//                mystorys.clear();
                if (personinfo != null && personinfo.getMyselfstorys() != null && personinfo.getMyselfstorys().size() > 0) {
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMyselfstorys());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                } else {
                    editStoryBtn.setVisibility(View.VISIBLE);
                    homelist.setVisibility(View.GONE);
                    tvEditDes.setVisibility(View.VISIBLE);
                    tvDes.setVisibility(View.VISIBLE);
                    tvStoryDes.setText("你还没有发布故事");
                    tvEditDes.setText("写故事");
                    tvEditDes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            IMUIHelper.openCreateZiXiActivity(getActivity());
                            ;
                        }
                    });
                }
                break;

            //话题
            case R.id.ll_home_topic:
                setSelected(2);
                if (personinfo != null && personinfo.getMytopics() != null && personinfo.getMytopics().size() > 0) {
                    mystorys.clear();
                    mystorys.addAll(personinfo.getMytopics());
                    adapter.notifyDataSetChanged();
                    editStoryBtn.setVisibility(View.GONE);
                    homelist.setVisibility(View.VISIBLE);
                } else {
                    editStoryBtn.setVisibility(View.VISIBLE);
                    homelist.setVisibility(View.GONE);
                    tvEditDes.setVisibility(View.VISIBLE);
                    tvDes.setVisibility(View.VISIBLE);
                    tvStoryDes.setText("你还没有参与话题");
                    tvEditDes.setText("写话题");
                    tvEditDes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            startActivity(new Intent(getActivity(), CreateTopicActivity.class));
                            startActivity(new Intent(getActivity(), ReleaseTopicActivity.class));
                        }
                    });
                }
                break;

            /**   家族    */
            case R.id.rel_family_homepage:
                IMUIHelper.openHisFamilyEActivity(getActivity(), "1");
                break;
            /**    收藏    */
            case R.id.rel_collect_homepage:   //传值 1 表示收藏  0 表示喜欢
                IMUIHelper.openFavorOrCollectActivity(getActivity(), "1", "1");
                break;
            /**    喜欢   */
            case R.id.rel_favor_homepage:
                IMUIHelper.openFavorOrCollectActivity(getActivity(), "0", "1");
                break;
            case R.id.rel_role_apply_homepage:
                IMUIHelper.openMyRoleAuditActivity(getActivity());
                break;
        }
    }

    /**
     * 短时间显示Toast
     *
     * @param message
     */
    private void showToast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    /**
     * 这里是各种的回调 比如删除自己的剧或自戏 回来要重新请求一遍数据
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == -1) {
            mystorys.clear();
            hasLoad = false;
            ReqHomeInfo();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        HomeInfo.PersoninfoEntity.MyHomeEntity mh = adapter.getMystorys().get(i);
        String type = mh.getType();
        switch (currentSelected) {
            case 0:
                startActivity(mh.getStoryid(), type);
                break;
            case 1:
                Intent intent = new Intent(getActivity(), ZiXiDetailsActivity.class);
                intent.putExtra(IntentConstant.STORY_ID, mh.getStoryid());
                intent.putExtra(IntentConstant.CREATOR_ID, loginId);
                startActivityForResult(intent, 1);
                break;
            case 2:
//                IMUIHelper.openTopicDetailsActivity(getActivity(), mh.getTopicid());
                if(mh!=null)
                IMUIHelper.openSquareCommentActivity(getActivity(),mh.getTitle(),mh.getTopicid(),IntentConstant.FLOOR_TYPE_TOPIC);
                break;
        }
    }


    private void startActivity(String id, String type) {
        if (TextUtils.isEmpty(type)) {
            IMUIHelper.openStoryDetailsActivity(getActivity(), id);
        } else {
            if (type.equals("1")) {
                Intent intent = new Intent(getActivity(), ZiXiDetailsActivity.class);
                intent.putExtra(IntentConstant.STORY_ID, id);
                intent.putExtra(IntentConstant.CREATOR_ID, loginId);
                startActivityForResult(intent, 1);
            } else if (type.equals("0")) {
                IMUIHelper.openStoryDetailsActivity(getActivity(), id);
            } else {
                IMUIHelper.openTopicDetailsActivity(getActivity(), id);
            }
        }
    }

    /**
     * 这里也有回调  一般是跨Act的刷新,需判断全局标志IS_REFRESH的值
     */
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            hasLoad = false;
            ReqHomeInfo();
        }
        MobclickAgent.onPageStart("HomePage(我的)");
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("HomePage(我的)");
    }
}
