package com.yuxip.ui.fragment.story;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.Bannaers;
import com.yuxip.JsonBean.BannersJsonBean;
import com.yuxip.JsonBean.RecommandJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.adapter.RecommandAdapter;
import com.yuxip.ui.customview.ViewpagerHead;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.ui.widget.MyListView;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ly on 2015/8/19.
 * description:精选
 */
public class RecommandFragment extends XYBaseFragment {
    private View view;
    private MyListView listview;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ViewpagerHead viewpagerHead;                        //头部轮播图片的布局 须传递数据
    private RecommandAdapter adapter;
    private List<Bannaers> bannersList = new ArrayList<>();     //轮播图片的banner
    private String uid;
    private List<RecommandJsonBean.SelectedcontentsEntity.StorysEntity> list_total = new ArrayList<>();     //所有推荐数据的集合
    private List<RecommandEntity> list_recommand = new ArrayList<>();                                       //存放recommand推荐标题的集合 包括起始位置 标题 以及对应的type值
    private boolean isLoading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_recommand, null);
            initView();
        }
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ReqImgBanners();
        ReqListData();
        swipeRefreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.pink));
    }

    @Override
    public void onDestroyView() {
        viewpagerHead.shutDownService();
        super.onDestroyView();
    }

    private void initView() {
        uid = IMLoginManager.instance().getLoginId() + "";
        listview = (MyListView) view.findViewById(R.id.listview_recommand_fragment);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout_recommand_fragment);
        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                refresh();
            }
        });
        viewpagerHead = (ViewpagerHead) view.findViewById(R.id.viewpager_head_recommand_framgent);
        viewpagerHead.setSwipeRefreshLayout(this.swipeRefreshLayout);
        listview.setFocusable(false);
        adapter = new RecommandAdapter(getActivity(), list_total);
        listview.setAdapter(adapter);
    }


    /**
     * 刷新功能
     */
    private void refresh() {
        if (isLoading)
            return;
        isLoading = true;
        list_total.clear();
        ReqImgBanners();
        ReqListData();
    }

    /**
     * 获取顶部轮播图片
     */
    private void ReqImgBanners() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GetBanners, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onRecentBannerDataReady(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                try {
                    T.show(getActivity().getApplicationContext(), R.string.network_err, 1);
                }catch (Exception e1){

                }
            }
        });
    }

    /**
     * 广告图数据
     *
     * @param result
     */
    private void onRecentBannerDataReady(String result) {
        BannersJsonBean banners = new Gson().fromJson(result, BannersJsonBean.class);
        if (banners.getResult().equals("1")) {
            bannersList.clear();
            bannersList.addAll(banners.getBannaers());
            viewpagerHead.setData(bannersList);
        }
    }

    /**
     * 获取列表数据
     */
    private void ReqListData() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        OkHttpClientManager.postAsy(ConstantValues.GetSelectedStorys, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                onReceiveSelectData(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                isLoading = false;
                swipeRefreshLayout.setRefreshing(false);
                T.show(getActivity().getApplicationContext(), "获取数据失败", 1);
            }
        });
    }


    private void onReceiveSelectData(String json) {
        RecommandJsonBean recommandJsonBean = new Gson().fromJson(json, RecommandJsonBean.class);
        if (recommandJsonBean != null && recommandJsonBean.getResult().equals("1")) {
            List<RecommandJsonBean.SelectedcontentsEntity> selectedcontentsEntities = recommandJsonBean.getSelectedcontents();
            /**?????????????????  会不会只有一类推荐的情况????????????????????????????*/
            /**######################  分类两部分 前面是剧的集合  后面是自戏的集合  #################################*/

            for (int i = 0; i < selectedcontentsEntities.size(); i++) {
                RecommandEntity recommandEntity = new RecommandEntity();
                recommandEntity.setPosition(list_total.size());     //所有数据都填充到list_total集合中
                list_total.addAll(selectedcontentsEntities.get(i).getStorys());
                recommandEntity.setTitle(selectedcontentsEntities.get(i).getSelectedtypetitle());
                recommandEntity.setType(selectedcontentsEntities.get(i).getSelectedtype());
                list_recommand.add(recommandEntity);
            }
            adapter.setTypeData(list_recommand);
            adapter.notifyDataSetChanged();
        }
        isLoading = false;
        swipeRefreshLayout.setRefreshing(false);
    }

    /**
     * 推荐类型的实体类,记录的是第几条有"更多"的头标签  及相应的title与类型
     */
    public class RecommandEntity {
        private int position;
        private String title;
        /**
         * type 剧以2开头 20001   自戏以3开头   300001
         */
        private String type;

        public void setPosition(int position) {
            this.position = position;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getPosition() {
            return position;
        }

        public String getTitle() {
            return title;
        }

        public String getType() {
            return type;
        }
    }

}
