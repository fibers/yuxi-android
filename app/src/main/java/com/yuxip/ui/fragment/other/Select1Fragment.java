package com.yuxip.ui.fragment.other;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.yuxip.R;
import com.yuxip.ui.fragment.base.XYBaseFragment;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 剧详情页(StoryDetailsActivity) 中的内容
 * Created by HeTianpeng on 2015/07/14.
 */
public class Select1Fragment extends XYBaseFragment {


    @InjectView(R.id.text)
    TextView text;
    private String tag;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_1_fragment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        tag = getTag();
    }

    public void setData(String str) {
        if(text!=null)
        text.setText(str);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }
}
