package com.yuxip.ui.fragment.other;

import android.view.View;
import android.widget.ProgressBar;

import com.yuxip.R;
import com.yuxip.ui.fragment.base.TTBaseFragment;

public abstract class MainFragment extends TTBaseFragment {
	private ProgressBar progressbar;

	public void init(View curView) {
		progressbar = (ProgressBar) curView.findViewById(R.id.progress_bar);
	}

	public void showProgressBar() {
		progressbar.setVisibility(View.VISIBLE);
	}

	public void hideProgressBar() {
		progressbar.setVisibility(View.GONE);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	/*	RefWatcher refWatcher = IMApplication.getRefWatcher(getActivity());
		refWatcher.watch(this);*/
	}

}
