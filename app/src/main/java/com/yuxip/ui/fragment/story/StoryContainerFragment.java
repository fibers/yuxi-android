package com.yuxip.ui.fragment.story;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.umeng.analytics.MobclickAgent;
import com.yuxip.JsonBean.StoryShowTypeJsonBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FamilytypeEntity;
import com.yuxip.imservice.event.ChannelSelectEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.ChannelSelectDataManager;
import com.yuxip.imservice.manager.http.HomeEntityManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.activity.other.StoryListSearchActivity;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.ui.widget.TabButtonE;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by zl on 2015/6/30.
 * description:剧场 其中包括 精选 剧 以及 自戏 三个子标签
 */
public class StoryContainerFragment extends XYBaseFragment implements View.OnClickListener {

    @InjectView(R.id.img_search) ImageView imgSearch;
    @InjectView(R.id.tab1_storycontainer) TabButtonE tab1;          //顶部的三个标签
    @InjectView(R.id.tab2_storycontainer) TabButtonE tab2;
    @InjectView(R.id.tab3_storycontainer) TabButtonE tab3;
    private List<TabButtonE> tablist = new ArrayList<>();
    private List<Fragment> mFragmentList = new ArrayList<>();       //数据源
    private Fragment storymaincontainer;                            //内容的三个子碎片
    private Fragment selfplaymaincontainer;
    private Fragment recommandFragment;
    private FragmentManager fragmentManager;
    private View view;
    private String loginId;
    private String titles[];
    private int color_select;
    private int color_unSelect;
    private List<StoryShowTypeJsonBean.ShowtypeEntity> list_showtype = new ArrayList<>();   //1 精选  2 剧情  3 自戏
    private List<StoryShowTypeJsonBean.FamilytypeEntity> list_familytype = new ArrayList<>(); //用于家族排序的
    private List<FamilytypeEntity> list_family=new ArrayList<>();    //家族排序尝试去除内部类
    private MyReceiver myReceiver;
    private Fragment curFragment;
    private int currentTag;     //用以记录当前选中的子碎片位置


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_base_world, null);
        ButterKnife.inject(this, view);
        loginId = IMLoginManager.instance().getLoginId() + "";
        registerBroadCast();
        initTabs();
        initView();
        RequestShowType();
        return view;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }


    private void initView() {
        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), StoryListSearchActivity.class);
                startActivity(intent);
            }
        });

        color_select = getActivity().getResources().getColor(R.color.pink);
        color_unSelect = getActivity().getResources().getColor(R.color.grey_656464);
        fragmentManager=getChildFragmentManager();
        List<Fragment> fragmentList = fragmentManager.getFragments();
        if(fragmentList != null) {
            for (Fragment fragment : fragmentList) {
                fragmentManager.beginTransaction().remove(fragment).commit();
            }
        }
        if(recommandFragment == null) {
            recommandFragment = new RecommandFragment();
            storymaincontainer = new StoryMainContainerFragment();
            selfplaymaincontainer = new SelfplayMainContainerFragment();
            mFragmentList.clear();
            mFragmentList.add(recommandFragment);
            mFragmentList.add(storymaincontainer);
            mFragmentList.add(selfplaymaincontainer);
            fragmentManager.beginTransaction().add(R.id.linear_storycontainer, recommandFragment).add(R.id.linear_storycontainer, storymaincontainer).add(R.id.linear_storycontainer, selfplaymaincontainer)
                    .hide(recommandFragment).hide(storymaincontainer).hide(selfplaymaincontainer).show(recommandFragment).commit();
            curFragment = recommandFragment;
        }
    }

    private void RequestShowType() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryShowType, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                //todo 应该做判断 返回结果是否是正确的结构
                onReceiveShowType(response);
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }


    private void onReceiveShowType(String json) {
        StoryShowTypeJsonBean storyShowTypeJsonBean = new Gson().fromJson(json, StoryShowTypeJsonBean.class);
        if (storyShowTypeJsonBean != null && storyShowTypeJsonBean.getResult().equals("1")) {
            list_showtype = storyShowTypeJsonBean.getShowtype();
            /**   存储获取的家族类型 在 FamilyAllActivity中要用到 */
            if (storyShowTypeJsonBean.getFamilytype() != null) {
                list_familytype = storyShowTypeJsonBean.getFamilytype();
                HomeEntityManager.getInstance().setFamilyList(list_familytype);
            }
            /** showtype中的nodesentity集合 其中 位置  0  精选  1 剧  2 自戏  */
        ChannelSelectDataManager.getInstance().setListAll(list_showtype.get(1).getNodes(),"0");
        ChannelSelectDataManager.getInstance().setListAll(list_showtype.get(2).getNodes(),"1");
        ChannelSelectEvent channelSelectEvent = new ChannelSelectEvent();
            channelSelectEvent.eventType = ChannelSelectEvent.Event.TYPE_SET_STORY_PLAY;
            EventBus.getDefault().post(channelSelectEvent);
//                ((StoryMainContainerFragment) storymaincontainer).setShowType(list_showtype.get(1).getNodes());
//            ((SelfplayMainContainerFragment) selfplaymaincontainer).setShowType(list_showtype.get(2).getNodes());
        }
    }

    /***
     *  初始化家族类型信息 从内部类转为 实体类
     */
    private void initFamilyType(){
         list_family.clear();
        for(int i=0;i<list_familytype.size();i++){
            FamilytypeEntity familytypeEntity=new FamilytypeEntity();
            familytypeEntity.setTypeid(list_familytype.get(i).getTypeid());
            familytypeEntity.setTypename(list_familytype.get(i).getTypename());
            list_family.add(familytypeEntity);
        }
    }

    /**      注册广播 主要对应 精选中更多的跳转 判断是跳转 剧或是自戏  */
    private void registerBroadCast() {
        myReceiver = new MyReceiver();
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConstantValues.BROADCAST_REFRESH_RECOMMAND);
        getActivity().registerReceiver(myReceiver, filter);
    }


    /**
     * 初始化  tabs  保守
     */
    private void initTabs() {
        color_select = getResources().getColor(R.color.pink);
        color_unSelect = Color.parseColor("#4a4a4a");
        titles = new String[]{getString(R.string.recommand), getString(R.string.story), getString(R.string.selfplay)};
        tab1.setTitle(titles[0]);
        tab2.setTitle(titles[1]);
        tab3.setTitle(titles[2]);
        tab1.setTitleColor(color_select, color_unSelect);
        tab2.setTitleColor(color_select, color_unSelect);
        tab3.setTitleColor(color_select, color_unSelect);
        tab1.setOnClickListener(this);
        tab2.setOnClickListener(this);
        tab3.setOnClickListener(this);
        tab1.setButtonSelect(true);
        tab2.setButtonSelect(false);
        tab3.setButtonSelect(false);
        tablist.add(tab1);
        tablist.add(tab2);
        tablist.add(tab3);
    }

    /*******
     * 设置tab的状态改变
     ********/
    private void setTabStatus(int position) {
        for (int i = 0; i < tablist.size(); i++) {
            tablist.get(i).setButtonSelect(false);
        }
        currentTag=position;
        tablist.get(position).setButtonSelect(true);
    }

    /**      切换顶部3个碎片  */
    private void changeFra(Fragment to) {
        if(to == curFragment) {
            return;
        }
        android.support.v4.app.FragmentTransaction transaction = fragmentManager.beginTransaction();
        if(!to.isAdded()) {
            transaction.hide(curFragment).add(R.id.linear_storycontainer, to).commit();
        } else {
            transaction.hide(curFragment).show(to).commit();
        }
        curFragment = to;
    }

    /**     获取当前的碎片位置  这里主要是用以在MainActivity中处理 storymainfragment中的下拉表的返回键收回 */
    public int getCurrentTag() {
        return currentTag;
    }

    /**     获取当前的碎片 同上 因为还要获取下拉列表的展开状态  -->>>  目前只对storymainfragment有效    */
    public Fragment getCurrentFragment(int position){
        return mFragmentList.get(position);
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(myReceiver);
        ButterKnife.reset(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_storycontainer:
                setTabStatus(0);
                changeFra(mFragmentList.get(0));
                break;
            case R.id.tab2_storycontainer:
                setTabStatus(1);
                changeFra(mFragmentList.get(1));
                break;
            case R.id.tab3_storycontainer:
                setTabStatus(2);
                changeFra(mFragmentList.get(2));
                break;
        }
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("WorldFragment(剧场)");
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("WorldFragment(剧场)");
    }


    /**
     * 广播接收 判断是跳转剧或是自戏
     */
    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String selectType = intent.getStringExtra(ConstantValues.RECOMMAND_TYPE);
            String type = intent.getStringExtra(ConstantValues.RECOMMAND_TYPE_JUMP);
            if (selectType.equals(ConstantValues.RECOMMAND_STORY)) {
                changeFra(mFragmentList.get(1));
                Log.e("onReceive", "1");
                setTabStatus(1);
//                int position = getCurrentPagerPosition(type, list_showtype.get(1).getNodes());
                ((StoryMainContainerFragment) storymaincontainer).setViewpager(0);
            } else if (selectType.equals(ConstantValues.RECOMMAND_PLAY)) {
                changeFra(mFragmentList.get(2));
                Log.e("onReceive", "2");
                setTabStatus(2);
//                int position = getCurrentPagerPosition(type, list_showtype.get(2).getNodes());
                ((SelfplayMainContainerFragment) selfplaymaincontainer).setViewpager(0);
            }
        }
    }

    /**
     * 用以获取当前的type值在对应的viewpager中的位置
     **/
    private int getCurrentPagerPosition(String type, List<StoryShowTypeJsonBean.ShowtypeEntity.NodesEntity> list) {
        for (int i = 0; i < list.size(); i++) {
            if (type.equals(list.get(i).getId())) {
                return i;
            }
        }
        return 0;
    }

}
