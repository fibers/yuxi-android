package com.yuxip.ui.fragment.story;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.adapter.album.WorldResultDataAdapter;
import com.yuxip.ui.customview.MyRecyclerView;
import com.yuxip.ui.customview.ProgressView;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zl on 2015/7/7.
 * description: 最新
 * modify 现在更为自戏的子项
 */
public class NewFragmentC extends XYBaseFragment {
    private Logger logger = Logger.getLogger(NewFragmentC.class);

    @InjectView(R.id.recycler_view_hot)
    MyRecyclerView recyclerView;
    private View view;
    private String loginId;
    private List<HotBean.StorysEntity> hotBeans = new ArrayList<>();
    private ProgressView emptyView;
    private String type;        //传递的type值，请求参数
    private int index = 0;      //索引值，0表示从0开始获取,类似刷新,加载更多需要传索引值(一般指当前list大小),比如当前list.size()==10,index=10,服务器就取10->>>19的数据
    private WorldResultDataAdapter adapter;

    /**
     * 静态的初始传值 这里的数据都是根据type的值相应获取的
     */
    public static NewFragmentC instance(String type) {
        NewFragmentC fragmentC = new NewFragmentC();
        Bundle bundle = new Bundle();
        bundle.putString("type", type);
        fragmentC.setArguments(bundle);
        return fragmentC;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.world_hot_fragment, null);
        ButterKnife.inject(this, view);
        type = getArguments().getString("type");
        initRes();
        return view;
    }

    private void initRes() {
        initRecyclerView();
        adapter = new WorldResultDataAdapter(getActivity(), hotBeans);
        recyclerView.setAdapter(adapter);
    }

    /**
     * 初始化recycleview
     */
    private void initRecyclerView() {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setEmptyView(R.layout.progress_layout);
        recyclerView.hideEmptyView();
        /**  空view 用以无数据或数据加载失败的展示,点击可以重新加载   */
        emptyView = (ProgressView) recyclerView.getEmptyView();
        emptyView.setProgressBarVisibility(View.GONE);
        emptyView.setTextOnClickListener(new ProgressView.TextOnClickListener() {
            @Override
            public void textOnClick() {
                refresh();
            }
        });
        /**     默认的下拉刷新  */
        recyclerView.setDefaultOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refresh();
                    }
                });
        /**     这里只是设置第一次进页面下拉状态 没有请求数据  */
        recyclerView.mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (hotBeans.isEmpty()) {
                    if (recyclerView != null && !recyclerView.mSwipeRefreshLayout.isRefreshing())
                        recyclerView.setRefreshing(true);
                }
            }
        });
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int i, int i1) {  //第一个参数是item的总数； 第二个参数是屏幕底部显示的，item的position
                load();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginId = IMLoginManager.instance().getLoginId() + "";
    }

    public static final String DEL_STORY_ACTION = "com.yuxi.delstory";  //广播过滤,用到了???
    private DelBroadcastReceiver delBroadcastReceiver;     //广播接收，用到了???

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IntentFilter filter = new IntentFilter(DEL_STORY_ACTION);
        getActivity().registerReceiver(delBroadcastReceiver = new DelBroadcastReceiver(), filter);
        if (hotBeans != null && hotBeans.size() == 0) {
            refresh();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            refresh();
        }
    }

    public void refresh() {
        index = 0;
        requestDataC();
    }

    public void load() {
        if (!recyclerView.mSwipeRefreshLayout.isRefreshing()) {
            index = hotBeans.size();
            requestDataC();
        }
    }


    /**
     * 网络请求，获取最热的剧
     */
    private void requestDataC() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("index", index + "");
        params.addParams("count", "10");
        params.addParams("type", type);
        params.addParams("token", "0");

        OkHttpClientManager.postAsy(ConstantValues.GetStorysByType, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                String content = response;

                try {
                    HotBean hotBean = new Gson().fromJson(content, HotBean.class);
                    List<HotBean.StorysEntity> list = hotBean.getStorys();
                    if (null == list || list.isEmpty()) { //第一次请求数据
                        if (hotBeans.isEmpty()) {
                            emptyView.showFailureText(getString(R.string.not_data), true);
                        } else {
                            if (!recyclerView.mSwipeRefreshLayout.isRefreshing()) {
                                adapter.setCustomLoadMoreView(null);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (index == 0) { //刷新操作
                            hotBeans.clear();
                        } //加载操作
                        hotBeans.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                }
                if (recyclerView != null && recyclerView.mSwipeRefreshLayout.isRefreshing()) {
                    recyclerView.setRefreshing(false);
                }
            }

            @Override
            public void onError(Request request, Exception e) {
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        getActivity().unregisterReceiver(delBroadcastReceiver);
    }

    /**
     * 比如自己删除某剧,需要刷新列表,也许以后用到
     */
    class DelBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra("id");

            Iterator<HotBean.StorysEntity> it = hotBeans.iterator();
            while (it.hasNext()) {
                HotBean.StorysEntity se = it.next();
                if (se.getStoryid().equals(id)) {
                    it.remove();
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }
}
