package com.yuxip.ui.fragment.chat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.entity.GroupEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.entity.MemberEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.SessionEvent;
import com.yuxip.imservice.event.UnreadEvent;
import com.yuxip.imservice.event.UserInfoEvent;
import com.yuxip.imservice.manager.DataManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.IMUnreadMsgManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.other.MainActivity;
import com.yuxip.ui.adapter.FFMessageAdapter;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.EmptyViewUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * 消息列表界面
 * Created by SummerRC on 2015/5/11.
 */
public class FFMessageFragment extends XYBaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private FFMessageAdapter ffMessageAdapter;
    private View curView = null;
    private IMService imService;
    private ProgressBar progress_bar;
    private Logger logger;

    /**
     * 每个界面需要重写的方法
     */
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        /**service因异常而断开连接的时候，这个方法才会用到。 */
        @Override
        public void onServiceDisconnected() {
            if (EventBus.getDefault().isRegistered(FFMessageFragment.this)) {
                EventBus.getDefault().unregister(FFMessageFragment.this);
            }
        }

        /** 服务被调用的时候 */
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            /** 依赖联系人回话、未读消息、用户的信息三者的状态(重要，方法有对三者状态的判断） */
            onRecentContactDataReady();
            EventBus.getDefault().registerSticky(FFMessageFragment.this);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger = Logger.getLogger(FFMessageFragment.class);
        imServiceConnector.connect(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        if (null != curView) {
            ((ViewGroup) curView.getParent()).removeView(curView);
        }
        curView = inflater.inflate(R.layout.ff_fragment_message, null);
        progress_bar = (ProgressBar) curView.findViewById(R.id.progress_bar);

        /** 初始化联系人列表视图 */
        initContactListView();
        return curView;
    }


    private void initContactListView() {
        ListView contactListView = (ListView) curView.findViewById(R.id.listView);
        contactListView.setOnItemClickListener(this);
        contactListView.setOnItemLongClickListener(this);
        /** 最近联系人的 消息列表 */
        ffMessageAdapter = new FFMessageAdapter(getActivity());
        contactListView.setAdapter(ffMessageAdapter);
        contactListView.setEmptyView(EmptyViewUtil.getView(getActivity(), contactListView, "暂无消息"));
        contactListView.setOnScrollListener(new PauseOnScrollListener(ImageLoaderUtil.getImageLoaderInstance(), true, true));
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(FFMessageFragment.this)) {
            EventBus.getDefault().unregister(FFMessageFragment.this);
        }
        imServiceConnector.disconnect(getActivity());
        super.onDestroy();
    }

    /**
     * 这个地方跳转一定要快
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!ffMessageAdapter.getData().isEmpty()) {
            RecentInfo recentInfo = ffMessageAdapter.getData().get(position);
            switch (recentInfo.getSessionType()) {
                case DBConstant.SESSION_TYPE_SINGLE:
                    IMUIHelper.openFriendChatActivity(getActivity(), recentInfo.getSessionKey());
                    break;
                case DBConstant.SESSION_TYPE_GROUP:
                    if (YXFamilyManager.instance().isMember(recentInfo.getPeerId())) {
                        IMUIHelper.openFamilyChatActivity(getActivity(), recentInfo.getSessionKey());
                    } else {
                        int groupId = recentInfo.getPeerId();
                        if (groupId == 0) {
                            return;
                        }
                        String storyId = YXStoryManager.instance().getStoryId(groupId) + "";
                        if (storyId.equals("-1")) {
                            T.show(getActivity().getApplicationContext(), "该剧可能已被删除！", 0);
                            return;
                        }
                        String sessionKey = recentInfo.getSessionKey();
                        getDramaGroupInfo(storyId, sessionKey);
                        startDeal();
                    }
                    break;
                case DBConstant.SESSION_TYPE_SYSTEM:
                    IMUIHelper.openSystemMessageActivity(getActivity());
                    break;
                default:
                    break;
            }
            recentInfo.setUnReadCnt(0);
            ffMessageAdapter.notifyDataSetChanged();
        }

    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case GROUP_INFO_OK:
            case CHANGE_GROUP_MEMBER_SUCCESS:
                onRecentContactDataReady();
                break;

            case GROUP_INFO_UPDATED:
                onRecentContactDataReady();
                break;
            case SHIELD_GROUP_OK:
                /** 更新最下栏的未读计数、更新session */
                onShieldSuccess(event.getGroupEntity());
                break;
            case SHIELD_GROUP_FAIL:
            case SHIELD_GROUP_TIMEOUT:
                onShieldFail();
                break;
        }
    }

    /**
     * 更新页面以及 下面的未读总计数
     *
     * @param entity GroupEntity
     */
    private void onShieldSuccess(GroupEntity entity) {
        if (entity == null) {
            return;
        }
        /** 更新某个sessionId */
        ffMessageAdapter.updateRecentInfoByShield(entity);
        IMUnreadMsgManager unreadMsgManager = imService.getUnReadMsgManager();

        int totalUnreadMsgCnt = unreadMsgManager.getTotalUnreadCount();
        ((MainActivity) getActivity()).setUnreadMessageCnt(totalUnreadMsgCnt, MainActivity.UNREAD_MESS_INDEX_FAMILY);
    }

    private void onShieldFail() {
        Toast.makeText(getActivity(), R.string.req_msg_failed, Toast.LENGTH_SHORT).show();
    }

    public void onEventMainThread(SessionEvent sessionEvent) {
        switch (sessionEvent) {
            case RECENT_SESSION_LIST_UPDATE:
            case RECENT_SESSION_LIST_SUCCESS:
            case SET_SESSION_TOP:
                onRecentContactDataReady();
                break;
        }
    }


    public void onEventMainThread(UnreadEvent event) {
        switch (event.event) {
            case UNREAD_MSG_RECEIVED:
            case UNREAD_MSG_LIST_OK:
            case SESSION_READED_UNREAD_MSG:
            case UNREAD_MSG_SYSTEM:
                onRecentContactDataReady();
                break;
        }
    }

    public void onEventMainThread(UserInfoEvent event) {
        switch (event) {
            case USER_INFO_UPDATE:
            case USER_INFO_OK:
                onRecentContactDataReady();
                break;
        }
    }

    /**
     * 这个处于粗暴
     * todo  进行优化，在地方有些卡顿  使用异步？但AsyTask只能执行一次，而且方法调用的时节点很多，如何控制并发？理有点过
     */
    private void onRecentContactDataReady() {
        boolean isUserData = imService.getContactManager().isUserDataReady();
        boolean isSessionData = imService.getSessionManager().isSessionListReady();
        boolean isGroupData = imService.getGroupManager().isGroupReady();

        if (!(isUserData && isSessionData && isGroupData)) {
            return;
        }
        int count = imService.getUnReadMsgManager().getTotalUnreadCount();
        ((MainActivity) getActivity()).setUnreadMessageCnt(count, MainActivity.UNREAD_MESS_INDEX_FAMILY);
        List<RecentInfo> recentInfoList = imService.getSessionManager().getRecentListInfo();
        if (ffMessageAdapter == null)
            return;
        ffMessageAdapter.setData(recentInfoList, getActivity());
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (!ffMessageAdapter.getData().isEmpty()) {
            RecentInfo recentInfo = ffMessageAdapter.getData().get(position);
            switch (recentInfo.getSessionType()) {
                case DBConstant.SESSION_TYPE_SINGLE:
                case DBConstant.SESSION_TYPE_GROUP:
                    handleLongClick(getActivity(), recentInfo);
                    break;
                case DBConstant.SESSION_TYPE_SYSTEM:
                    IMUIHelper.openSystemMessageActivity(getActivity());
                    break;
                default:
                    break;
            }
        }
        return true;
    }

    /**
     * 长压处理消息
     *
     * @param ctx        ctx
     * @param recentInfo recentInfo
     */
    private void handleLongClick(final Context ctx, final RecentInfo recentInfo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle("确认");
        String[] items = new String[]{ctx.getString(R.string.delete_session),};
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        imService.getSessionManager().reqRemoveSession(recentInfo);
                        break;
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


    /**
     * 获取将要聊天的群成员信息用于显示角色名
     */
    private void getDramaGroupInfo(final String storyId, final String sessionKey) {
        final String groupId = sessionKey.split("_")[1];
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        params.addParams("groupid", groupId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                Map<String, MemberEntity> map = new HashMap<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String result = object.getString("result");
                    if (result.equals("1")) {
                        JSONObject object_groupInfo = object.getJSONObject("groupinfo");
                        JSONArray array = object_groupInfo.getJSONArray("members");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            MemberEntity entity = new MemberEntity();
                            entity.setId(obj.getString("id"));
                            entity.setRoleName(obj.getString("rolename"));
                            //这个是皮表的名称
                            entity.setTitle(obj.getString("title"));
                            entity.setPortrait(obj.getString("portrait"));
                            map.put(entity.getId(), entity);
                        }
                    }
                    DataManager.getInstance().putStoryGroupMemberMap(groupId, map);
                    endDeal();
                    IMUIHelper.openDramaChatActivity(getActivity(), sessionKey, storyId);
                } catch (Exception e) {
                    endDeal();
                    T.show(getActivity(), "网络失败请重试", 1);
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                endDeal();
                T.show(getActivity(), "网络失败请重试", 1);
            }
        });
    }

    private void startDeal() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    private void endDeal() {
        progress_bar.setVisibility(View.GONE);
    }
}
