package com.yuxip.ui.fragment.square;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.umeng.analytics.MobclickAgent;
import com.yuxip.R;
import com.yuxip.ui.fragment.base.XYBaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zl on 2015/6/30.
 *  当前的广场界面 目前只有一个列表,整体框架无变动,
 */
public class SquareContainerFragment extends XYBaseFragment {
    @InjectView(R.id.img_gone)
    ImageView imgGone;
    @InjectView(R.id.page)
    ViewPager page;

    private List<Fragment> mFragmentList = new ArrayList<>();       //数据源
    private FragmentAdapter mFragmentAdapter;   //适配器
    private Fragment squareFragment;        //消息
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.fragment_base_square, null);
        ButterKnife.inject(this, view);
        initView();
        return view;
    }

    private void initView() {
//        squareFragment = new SquareFragment();
        squareFragment = new SquareListFragment(); // 10.13日改版广场 未完成
        mFragmentList.add(squareFragment);
        mFragmentAdapter = new FragmentAdapter(this.getChildFragmentManager(), mFragmentList);
        page.setAdapter(mFragmentAdapter);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    public class FragmentAdapter extends FragmentPagerAdapter {
        List<Fragment> fragmentList = new ArrayList<>();
        public FragmentAdapter(FragmentManager fm, List<Fragment> fragmentList) {
            super(fm);
            this.fragmentList = fragmentList;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("SquareRecruitingFragment(广场)");
    }
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("SquareRecruitingFragment(广场)");
    }
}
