package com.yuxip.ui.fragment.other;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.yuxip.JsonBean.StoryDetailsBean;
import com.yuxip.R;
import com.yuxip.ui.adapter.Select2Adapter;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.ui.widget.MyListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * 剧详情页(StoryDetailsActivity) 中的内容
 * Created by HeTianpeng on 2015/07/14.
 */
public class Select2Fragment extends XYBaseFragment {

    private static final String TAG = "TAG";
    private static final String SDB = "SDB";

    private String tag;
    private Select2Adapter adapter;

    @InjectView(R.id.list)
    MyListView  list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.select_2_fragment, container, false);
        ButterKnife.inject(this, view);
        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        tag = getArguments().getString(TAG);
        StoryDetailsBean sdb = (StoryDetailsBean) getArguments().getSerializable(SDB);
        List<? extends Object> data =  new ArrayList<>();
        if (tag.equals("2")) { // 主线剧情
            data = sdb.getScenes();
        } else if (tag.equals("3")) { // 主线人物
            data = sdb.getRoles();
        }

        if (adapter == null) {
            adapter = new Select2Adapter(getActivity(), data, tag);
            list.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }

        list.setSelected(false);
        list.setFocusable(false);
        list.setClickable(false);
        list.setHeaderDividersEnabled(false);
    }

    /*   public void setData(String tag, List<? extends Object> data) {

        Bundle bundle = new Bundle();
        bundle.putString("TAG", curTag);
        bundle.putSerializable("SDB", sdb);

        setArguments(bundle);
        if (adapter == null) {
            adapter = new Select2Adapter(getActivity(), data, tag);
            list.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }*/

    public void setData(String tag, StoryDetailsBean sdb) {
        if(!isAdded()) {
            Bundle bundle = new Bundle();
            bundle.putString(TAG, tag);
            bundle.putSerializable(SDB, sdb);
            setArguments(bundle);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

}
