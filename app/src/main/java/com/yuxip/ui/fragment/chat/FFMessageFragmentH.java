package com.yuxip.ui.fragment.chat;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;
import com.squareup.okhttp.Request;
import com.yuxip.DB.DBInterface;
import com.yuxip.DB.entity.ApplyFriendEntity;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.JsonBean.ChatMessageBean;
import com.yuxip.JsonBean.MessageBean;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.config.DBConstant;
import com.yuxip.entity.MemberEntity;
import com.yuxip.entity.StoryParentEntity;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.event.GroupEvent;
import com.yuxip.imservice.event.LoginEvent;
import com.yuxip.imservice.event.ReconnectEvent;
import com.yuxip.imservice.event.SessionEvent;
import com.yuxip.imservice.event.SocketEvent;
import com.yuxip.imservice.event.UnreadEvent;
import com.yuxip.imservice.event.UserInfoEvent;
import com.yuxip.imservice.manager.DataManager;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.manager.http.YXStoryManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.ui.activity.other.MainActivity;
import com.yuxip.ui.adapter.FriendMessageAdapterH;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.EmptyViewUtil;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.ImageLoaderUtil;
import com.yuxip.utils.Logger;
import com.yuxip.utils.NetworkUtil;
import com.yuxip.utils.T;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.greenrobot.event.EventBus;

/**
 * 消息列表界面
 * Created by SummerRC on 2015/5/11.
 */
public class FFMessageFragmentH extends XYBaseFragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private FriendMessageAdapterH contactAdapter;
    private View curView = null;
    private View noNetworkView;
    private ImageView notifyImage;
    private TextView displayView;
    private ProgressBar reconnectingProgressBar;
    private IMService imService;
    private ProgressBar progress_bar;
    private Logger logger;
    private DBInterface dbInterface;

    /**
     * 是否是手动点击重练。false:不显示各种弹出小气泡. true:显示小气泡直到错误出现
     */
    private volatile boolean isManualMConnect = false;
    private ArrayList<MessageBean> messageBeans = new ArrayList<>();


    /**
     * 每个界面需要重写的方法
     */
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {
        /**service因异常而断开连接的时候，这个方法才会用到。 */
        @Override
        public void onServiceDisconnected() {
            if (EventBus.getDefault().isRegistered(FFMessageFragmentH.this)) {
                EventBus.getDefault().unregister(FFMessageFragmentH.this);
            }
        }

        /** 服务被调用的时候 */
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            /** 依赖联系人回话、未读消息、用户的信息三者的状态(重要，方法有对三者状态的判断） */
            dbInterface = imService.getDbInterface();
            onRecentContactDataReady();
            EventBus.getDefault().registerSticky(FFMessageFragmentH.this);
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        logger = Logger.getLogger(FFMessageFragmentH.class);
        imServiceConnector.connect(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        if (null != curView) {
            ((ViewGroup) curView.getParent()).removeView(curView);
        }
        curView = inflater.inflate(R.layout.ff_fragment_message_h, null);
        /** 多端登陆也在用这个view */
        noNetworkView = curView.findViewById(R.id.layout_no_network);
        reconnectingProgressBar = (ProgressBar) curView.findViewById(R.id.progressbar_reconnect);
        displayView = (TextView) curView.findViewById(R.id.disconnect_text);
        displayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                startActivity(intent);
            }
        });
        notifyImage = (ImageView) curView.findViewById(R.id.imageWifi);
        progress_bar = (ProgressBar) curView.findViewById(R.id.progress_bar);

        /** 初始化联系人列表视图 */
        initContactListView();
        return curView;
    }


    private void initContactListView() {
        ListView contactListView = (ListView) curView.findViewById(R.id.listview);
        contactListView.setOnItemClickListener(this);
        contactListView.setOnItemLongClickListener(this);
        /** 最近联系人的 消息列表 */
        contactAdapter = new FriendMessageAdapterH(getActivity());
        contactListView.setAdapter(contactAdapter);
        contactListView.setEmptyView(EmptyViewUtil.getView(getActivity(), contactListView, "暂无消息"));
        contactListView.setOnScrollListener(new PauseOnScrollListener(ImageLoaderUtil.getImageLoaderInstance(), true, true));
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(FFMessageFragmentH.this)) {
            EventBus.getDefault().unregister(FFMessageFragmentH.this);
        }
        imServiceConnector.disconnect(getActivity());
        super.onDestroy();
    }

    /**
     * 这个地方跳转一定要快
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!contactAdapter.getData().isEmpty()) {
            MessageBean mb = contactAdapter.getData().get(position);
            if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL) {     //好友聊天
                IMUIHelper.openFriendChatActivity(getActivity(), ((ChatMessageBean) mb).getSessionKey());
            } else if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_GROUP) {   //消息类型是：群聊天
                if (YXFamilyManager.instance().isMember(((ChatMessageBean) mb).getPeerId())) {
                    IMUIHelper.openFamilyChatActivity(getActivity(), ((ChatMessageBean) mb).getSessionKey());
                } else {
                    int groupId = ((ChatMessageBean) mb).getPeerId();
                    if (groupId == 0) {
                        return;
                    }
                    String storyId = YXStoryManager.instance().getStoryId(groupId) + "";
                    if (storyId.equals("-1")) {
                        T.show(getActivity().getApplicationContext(), "该剧可能已被删除！", 0);
                        return;
                    }
                    String sessionKey = ((ChatMessageBean) mb).getSessionKey();
                    getDramaGroupInfo(storyId, sessionKey);
                    startDeal();
                }
            } else if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_SYSTEM) { //消息类型是：系统消息
                IMUIHelper.openSystemMessageActivity(getActivity());
            } else if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_COMMENT) { //消息类型：评论群
                IMUIHelper.openFFMessageCommentActivity(getActivity());
            }
            MessageBean m = messageBeans.get(position);
            m.setMessageCount(0);
            if (contactAdapter == null) {
                return;
            }
            contactAdapter.notifyDataSetChanged();
        }
    }

    public void onEventMainThread(GroupEvent event) {
        switch (event.getEvent()) {
            case GROUP_INFO_OK:
            case CHANGE_GROUP_MEMBER_SUCCESS:
                onRecentContactDataReady();
                break;

            case GROUP_INFO_UPDATED:
                onRecentContactDataReady();
                break;
            case SHIELD_GROUP_OK:
                // 更新最下栏的未读计数、更新session
//                onShieldSuccess(event.getGroupEntity());
                break;
            case SHIELD_GROUP_FAIL:
            case SHIELD_GROUP_TIMEOUT:
//                onShieldFail();
                break;
        }
    }

    public void onEventMainThread(SessionEvent sessionEvent) {
        switch (sessionEvent) {
            case RECENT_SESSION_LIST_UPDATE:
            case RECENT_SESSION_LIST_SUCCESS:
            case SET_SESSION_TOP:
                onRecentContactDataReady();
                break;
        }
    }


    public void onEventMainThread(UnreadEvent event) {
        switch (event.event) {
            case UNREAD_MSG_RECEIVED:
            case UNREAD_MSG_LIST_OK:
            case SESSION_READED_UNREAD_MSG:
                onRecentContactDataReady();
                break;
        }
    }

    public void onEventMainThread(UserInfoEvent event) {
        switch (event) {
            case USER_INFO_UPDATE:
            case USER_INFO_OK:
                onRecentContactDataReady();
                break;
        }
    }

    public void onEventMainThread(LoginEvent loginEvent) {
        switch (loginEvent) {
            case LOCAL_LOGIN_SUCCESS:
            case LOGINING: {
                if (reconnectingProgressBar != null) {
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                }
            }
            break;

            case LOCAL_LOGIN_MSG_SERVICE:
            case LOGIN_OK: {
                isManualMConnect = false;
                noNetworkView.setVisibility(View.GONE);
            }
            break;

            case LOGIN_AUTH_FAILED:
            case LOGIN_INNER_FAILED: {
                onLoginFailure(loginEvent);
            }
            break;

            case PC_OFFLINE:
            case KICK_PC_SUCCESS:
                onPCLoginStatusNotify(false);
                break;

            case KICK_PC_FAILED:
                Toast.makeText(getActivity(), getString(R.string.kick_pc_failed),
                        Toast.LENGTH_SHORT).show();
                break;
            case PC_ONLINE:
                onPCLoginStatusNotify(true);
                break;

            default:
                reconnectingProgressBar.setVisibility(View.GONE);
                break;
        }
    }


    public void onEventMainThread(SocketEvent socketEvent) {
        switch (socketEvent) {
            case MSG_SERVER_DISCONNECTED:
                handleServerDisconnected();
                break;

            case CONNECT_MSG_SERVER_FAILED:
            case REQ_MSG_SERVER_ADDRS_FAILED:
                handleServerDisconnected();
                onSocketFailure(socketEvent);
                break;
        }
    }

    public void onEventMainThread(ReconnectEvent reconnectEvent) {
        switch (reconnectEvent) {
            case DISABLE: {
                handleServerDisconnected();
            }
            break;
        }
    }


    private void onLoginFailure(LoginEvent event) {
        if (!isManualMConnect) {
            return;
        }
        isManualMConnect = false;
        String errorTip = getString(IMUIHelper.getLoginErrorTip(event));
        reconnectingProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), errorTip, Toast.LENGTH_SHORT).show();
    }

    private void onSocketFailure(SocketEvent event) {
        if (!isManualMConnect) {
            return;
        }
        isManualMConnect = false;
        String errorTip = getString(IMUIHelper.getSocketErrorTip(event));
        reconnectingProgressBar.setVisibility(View.GONE);
        Toast.makeText(getActivity(), errorTip, Toast.LENGTH_SHORT).show();
    }


    /**
     * 多端，PC端在线状态通知
     *
     * @param isOnline  isOnline
     */
    public void onPCLoginStatusNotify(boolean isOnline) {
        if (isOnline) {
            reconnectingProgressBar.setVisibility(View.GONE);
            noNetworkView.setVisibility(View.VISIBLE);
            notifyImage.setImageResource(R.drawable.pc_notify);
            displayView.setText(R.string.pc_status_notify);
            /**添加踢出事件*/
            noNetworkView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                    imService.getLoginManager().reqKickPCClient();
                }
            });
        } else {
            noNetworkView.setVisibility(View.GONE);
        }
    }

    private void handleServerDisconnected() {

        if (reconnectingProgressBar != null) {
            reconnectingProgressBar.setVisibility(View.GONE);
        }

        if (noNetworkView != null) {
            notifyImage.setImageResource(R.drawable.warning);
            noNetworkView.setVisibility(View.VISIBLE);
            if (imService != null) {
                if (imService.getLoginManager().isKickout()) {
                    displayView.setText(R.string.disconnect_kickout);
                } else {
                    displayView.setText(R.string.no_network);
                }
            }
            /**重连【断线、被其他移动端挤掉】*/
            noNetworkView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NetworkUtil.isNetWorkAvalible(getActivity())) {
                        isManualMConnect = true;
                        IMLoginManager.instance().relogin();
                    } else {
                        Toast.makeText(getActivity(), R.string.no_network_toast, Toast.LENGTH_SHORT)
                                .show();
                        return;
                    }
                    reconnectingProgressBar.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    /**
     * 这个处理有点过于粗暴
     * todo  进行优化，在地方有些卡顿  使用异步？但AsyTask只能执行一次，而且方法调用的时节点很多，如何控制并发？
     */
    private void onRecentContactDataReady() {
        if(imService == null) {
            return;
        }
        boolean isUserData = imService.getContactManager().isUserDataReady();
        boolean isSessionData = imService.getSessionManager().isSessionListReady();
        boolean isGroupData = imService.getGroupManager().isGroupReady();

        if (!(isUserData && isSessionData && isGroupData)) {
            return;
        }

        int unreadCount = 0;
        messageBeans.clear();

        /** 系统消息      begin */
        List<ApplyFriendEntity> addFriendEntities = dbInterface.loadAllApplyFriendEntity();
        if (addFriendEntities != null) {
            MessageBean messageBean = new MessageBean();
            messageBean.setiMessageTypeDetail(MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_FRIEDNS_ADD);
            messageBean.setMessageCount(imService.getUnReadMsgManager().getUnreadAddReqCount());

            ApplyFriendEntity afe;
            if (!addFriendEntities.isEmpty()) {
                afe = addFriendEntities.get(addFriendEntities.size() - 1);
                messageBean.setMessage(afe.getNickname() + "申请添加你为好友");
                messageBean.setId(Integer.parseInt(afe.getUid()));
                messageBean.setTime(afe.getApplyTime());
            } else {
                messageBean.setTime("0");
            }
            messageBean.setName("系统消息");
            moveSysMsg(messageBean);
        }
        List<ApplyGroupEntity> applyGroupEntities = dbInterface.loadAllApplyGroupEntity();
        if (applyGroupEntities != null) {
            getApplyGroupEntity(applyGroupEntities, true);
            applyGroupEntities = dbInterface.loadAllApplyGroupEntityByEntityType("story");
            getApplyGroupEntity(applyGroupEntities, false);
        }
        /** 系统消息      end */


        /** 好友/剧群/家族群消息      start */
        if (imService == null || imService.getSessionManager() == null)
            return;
        List<RecentInfo> recentInfoList = imService.getSessionManager().getRecentListInfo();
        logger.e(recentInfoList.size()+"");
        if (recentInfoList.size() > 0) {
            logger.e(recentInfoList.toString());
            RecentInfo recentInfo;
            for (int i = recentInfoList.size() - 1; i >= 0; i--) {
                recentInfo = recentInfoList.get(i);
                ChatMessageBean chatMB = new ChatMessageBean();
                if (recentInfo.getSessionType() == DBConstant.SESSION_TYPE_SINGLE) {
                    chatMB.setiMessageTypeDetail(MessageBean.MessageTypeDetailPersonal.MESSAGE_TYPE_PERSONAL);
                    chatMB.setAvatar(recentInfo.getAvatar());
                } else if (recentInfo.getSessionType() == DBConstant.SESSION_TYPE_GROUP) {
                    chatMB.setiMessageTypeDetail(MessageBean.MessageTypeDetailGroup.MESSAGE_TYPE_GROUP_FAMILY);
                    String peerId = recentInfo.getSessionKey().split("_")[1];
                    List<String> avatarList = new ArrayList<>();
                    avatarList.add(YXStoryManager.instance().getGroupPortrait(peerId));
                    chatMB.setAvatar(avatarList);
                }
                chatMB.setSessionKey(recentInfo.getSessionKey());
                chatMB.setMessage(recentInfo.getLatestMsgData());
                chatMB.setMessageCount(recentInfo.getUnReadCnt());
                chatMB.setTime(String.valueOf(recentInfo.getUpdateTime()));
                chatMB.setPeerId(recentInfo.getPeerId());
                unreadCount += recentInfo.getUnReadCnt();

                StoryParentEntity spe = YXStoryManager.instance().getStoryByGroupid(recentInfo.getPeerId());
                if (spe != null) {
                    if (TextUtils.isEmpty(recentInfo.getName())) {
                        chatMB.setName("position:" + i + "  recentInfo:" + recentInfo.toString());
                    } else {
                        chatMB.setName(spe.getTitle() + "[" + recentInfo.getName() + "]");
                    }
                    chatMB.setIconUrl(spe.getStoryimg());
                } else {
                    chatMB.setName(recentInfo.getName());
                }

                int index = isHaveChatMsg(chatMB);
                if (index == -1) {
                    messageBeans.add(chatMB);
                } else {
                    if (recentInfo.getUnReadCnt() > 0) {
                        messageBeans.remove(index);
                        messageBeans.add(index, chatMB);
                    }
                }
            }
        }
        /** 好友/剧群/家族群消息      end */


        int count = imService.getUnReadMsgManager().getUnreadAddReqCount();
        ((MainActivity) getActivity()).setUnreadMessageCnt(unreadCount + count, MainActivity.UNREAD_MESS_INDEX_FAMILY);
        if (null != messageBeans && messageBeans.size() > 0) {
            //升序排序
            Collections.sort(messageBeans, new Comparator<MessageBean>() {
                @Override
                public int compare(MessageBean messageBean, MessageBean t1) {
                    try {
                        Integer m = Integer.parseInt(messageBean.getTime());
                        Integer m1 = Integer.parseInt(t1.getTime());
                        return m.compareTo(m1);
                    } catch (Exception e) {
                        return -1;
                    }
                }
            });
            //反转
            Collections.reverse(messageBeans);
            if (contactAdapter == null)
                return;
            contactAdapter.setData(messageBeans, getActivity());
        }

    }

    /**
     * 将 申请 or 邀请的 群 的消息  封装
     *
     * @param applyGroupEntities applyGroupEntities
     * @param isFamily           是否是家族
     */
    private void getApplyGroupEntity(List<ApplyGroupEntity> applyGroupEntities, boolean isFamily) {
        if (null != applyGroupEntities && !applyGroupEntities.isEmpty()) {
            String str = "剧/家族";
            MessageBean mb = new MessageBean();
            mb.setMessageCount(imService.getUnReadMsgManager().getUnreadAddReqCount());
            ApplyGroupEntity age = applyGroupEntities.get(applyGroupEntities.size() - 1);
            mb.setId(Integer.parseInt(age.getGroupId()));
            mb.setTime(age.getCreateTime());
            if (age.getType().equals("0")) { //申请
                mb.setMessage(age.getApplyName() + "申请加入 " + str + age.getEntityName());
                mb.setiMessageTypeDetail(
                        MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_ADD);
            } else if (age.getType().equals("1")) { //邀请
                mb.setMessage(age.getCreator() + "邀请你加入 " + str + age.getEntityName());
                mb.setiMessageTypeDetail(
                        MessageBean.MessageTypeDetailSystem.MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_INVITATION);
            }
            mb.setName("系统消息");
            moveSysMsg(mb);
        }
    }

    /**
     * 移除系统消息
     *
     * @param mb    mb
     */
    private void moveSysMsg(MessageBean mb) {
        int index = isHaveSystemMsg();
        if (index == -1) {
            messageBeans.add(mb);
        } else {
            MessageBean m = messageBeans.get(index);
            if (Integer.valueOf(m.getTime()) < Integer.valueOf(mb.getTime())) {
                messageBeans.remove(index);
                messageBeans.add(index, mb);
            }
        }
    }

    /**
     * 找到系统消息的index，没有返回-1
     *
     * @return  int
     */
    private int isHaveSystemMsg() {
        if (!messageBeans.isEmpty()) {
            for (int i = 0; i < messageBeans.size(); i++) {
                MessageBean mb = messageBeans.get(i);
                if (mb.getiMessageTypeDetail()
                        .getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_SYSTEM) {
                    return i;
                }
            }
        }

        return -1;
    }

    /**
     * 返回这条消息的index，不存在返回-1.
     *
     * @param chatMessageBean   chatMessageBean
     * @return  int
     */
    private int isHaveChatMsg(ChatMessageBean chatMessageBean) {
        if (!messageBeans.isEmpty()) {
            for (int i = 0; i < messageBeans.size(); i++) {
                MessageBean mb = messageBeans.get(i);
                if (mb instanceof ChatMessageBean) {
                    if (((ChatMessageBean) mb).getSessionKey()
                            .equals(chatMessageBean.getSessionKey())) {
                        return i;
                    }
                }

            }
        }
        return -1;
    }


    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        if (!contactAdapter.getData().isEmpty()) {
            MessageBean mb = contactAdapter.getData().get(position);
            if (mb.getiMessageTypeDetail()
                    .getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL || mb
                    .getiMessageTypeDetail()
                    .getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_GROUP) { //消息类型是：聊天
                RecentInfo recentInfo = new RecentInfo();
                ChatMessageBean chatMessageBean = (ChatMessageBean) mb;
                recentInfo.setSessionKey(chatMessageBean.getSessionKey());
                recentInfo.setPeerId(chatMessageBean.getPeerId());

                if (mb.getiMessageTypeDetail().getMessageType() == MessageBean.MessageType.MESSAGE_TYPE_PERSONAL) {
                    recentInfo.setSessionType(1);
                } else {
                    recentInfo.setSessionType(2);
                }
                handleLongClick(getActivity(), recentInfo);
            }
        }
        return true;
    }

    /**
     * 长压处理消息
     *
     * @param ctx           ctx
     * @param recentInfo    recentInfo
     */
    private void handleLongClick(final Context ctx, final RecentInfo recentInfo) {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, android.R.style.Theme_Holo_Light_Dialog));
        builder.setTitle("确认");
        String[] items = new String[]{ctx.getString(R.string.delete_session),};
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        imService.getSessionManager().reqRemoveSession(recentInfo);
                        break;
                }
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.show();
    }


    /**
     * 获取将要聊天的群成员信息用于显示角色名
     */
    private void getDramaGroupInfo(final String storyId, final String sessionKey) {
        final String groupId = sessionKey.split("_")[1];
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", IMLoginManager.instance().getLoginId() + "");
        params.addParams("storyid", storyId);
        params.addParams("groupid", groupId);
        OkHttpClientManager.postAsy(ConstantValues.GetStoryGroupInfo, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                Map<String, MemberEntity> map = new HashMap<>();
                try {
                    JSONObject object = new JSONObject(response);
                    String result = object.getString("result");
                    if (result.equals("1")) {
                        JSONObject object_groupInfo = object.getJSONObject("groupinfo");
                        JSONArray array = object_groupInfo.getJSONArray("members");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject obj = array.getJSONObject(i);
                            MemberEntity entity = new MemberEntity();
                            entity.setId(obj.getString("id"));
                            entity.setRoleName(obj.getString("rolename"));
                            //这个是皮表的名称
                            entity.setTitle(obj.getString("title"));
                            entity.setPortrait(obj.getString("portrait"));
                            map.put(entity.getId(), entity);
                        }
                    }
                    DataManager.getInstance().putStoryGroupMemberMap(groupId, map);
                    endDeal();
                    IMUIHelper.openDramaChatActivity(getActivity(), sessionKey, storyId);
                } catch (Exception e) {
                    logger.e(e.toString());
                    endDeal();
                    try{
                        T.show(getActivity(), "网络失败请重试", 1);
                    }catch (Exception e1){
                        logger.e(e1.toString());
                    }

                }
            }

            @Override
            public void onError(Request request, Exception e) {
                endDeal();
                T.show(getActivity(), "网络失败请重试", 1);
            }
        });
    }

    private void startDeal() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    private void endDeal() {
        progress_bar.setVisibility(View.GONE);
    }
}
