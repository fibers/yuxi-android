package com.yuxip.ui.fragment.story;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.HotBean;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.adapter.album.WorldResultDataAdapter;
import com.yuxip.ui.customview.MyRecyclerView;
import com.yuxip.ui.customview.ProgressView;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by zl on 2015/7/7.
 * description: 热门
 * modify 现在更为剧的子项 与NewFragmentC类似
 */
public class HotFragment extends XYBaseFragment {
    private Logger logger = Logger.getLogger(HotFragment.class);

    @InjectView(R.id.recycler_view_hot)
    MyRecyclerView recyclerView;
    private String loginId;
    private List<HotBean.StorysEntity> hotBeans = new ArrayList<>();
    private ProgressView emptyView;
    //对应的是nodes中的id值，不是type值
    private String type;
    private int index = 0;
    private WorldResultDataAdapter adapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.world_hot_fragment, null);
        ButterKnife.inject(this, view);
        type = getArguments().getString("type");
        initRes();
        return view;
    }

    private void initRes() {
        initRecyclerView();
        adapter = new WorldResultDataAdapter(getActivity(), hotBeans);
        recyclerView.setAdapter(adapter);
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setEmptyView(R.layout.progress_layout);
        recyclerView.hideEmptyView();
        emptyView = (ProgressView) recyclerView.getEmptyView();
        emptyView.setProgressBarVisibility(View.GONE);
        emptyView.setTextOnClickListener(new ProgressView.TextOnClickListener() {
            @Override
            public void textOnClick() {
                refresh();
            }
        });
        recyclerView.setDefaultOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refresh();
                    }
                });
        recyclerView.mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (hotBeans.isEmpty()) {
                    if(recyclerView!=null&&!recyclerView.mSwipeRefreshLayout.isRefreshing())
                    recyclerView.setRefreshing(true);
                }
            }
        });
        recyclerView.enableLoadmore();
        recyclerView.setOnLoadMoreListener(new UltimateRecyclerView.OnLoadMoreListener() {
            @Override
            public void loadMore(int i, int i1) {  //第一个参数是item的总数； 第二个参数是屏幕底部显示的，item的position
                load();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginId = IMLoginManager.instance().getLoginId() + "";
    }

    public static final String DEL_STORY_ACTION = "com.yuxi.delstory";
    private DelBroadcastReceiver delBroadcastReceiver;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        IntentFilter filter = new IntentFilter(DEL_STORY_ACTION);
        getActivity().registerReceiver(delBroadcastReceiver = new DelBroadcastReceiver(), filter);
       if(hotBeans!=null&&hotBeans.size()==0){
           refresh();
       }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            refresh();
        }
    }

    public void refresh() {
        index = 0;
        requestDataC();
    }

    public void load() {
        if (!recyclerView.mSwipeRefreshLayout.isRefreshing()) {
            index = hotBeans.size();
            requestDataC();
        }
    }


    /**
     * 网络请求，获取最热的剧
     */
    private void requestDataC() {
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", loginId);
        params.addParams("index", index + "");
        params.addParams("count", "10");
        params.addParams("type", type);
        params.addParams("token", "0");
        OkHttpClientManager.postAsy(ConstantValues.GetStorysByType, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
                    HotBean hotBean = new Gson().fromJson(response, HotBean.class);
                    List<HotBean.StorysEntity> list = hotBean.getStorys();
                    if (null == list || list.isEmpty()) { //第一次请求数据
                        if (hotBeans.isEmpty()) {
                            if(isAdded())
                                emptyView.showFailureText(getString(R.string.not_data), true);
                        } else {
                            if (recyclerView!=null&&!recyclerView.mSwipeRefreshLayout.isRefreshing()) {
                                adapter.setCustomLoadMoreView(null);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    } else {
                        if (index == 0) { //刷新操作
                            hotBeans.clear();
                        } //加载操作
                        hotBeans.addAll(list);
                        adapter.notifyDataSetChanged();
                    }
                } catch (JsonSyntaxException e) {
                    logger.e(e.toString());
                }
                if(recyclerView!=null&&recyclerView.mSwipeRefreshLayout.isRefreshing()) {
                    recyclerView.setRefreshing(false);
                }
            }

            @Override
            public void onError(Request request, Exception e) {
                if(recyclerView!=null&&recyclerView.mSwipeRefreshLayout.isRefreshing()) {
                    recyclerView.setRefreshing(false);
                }
                if(isAdded())
                    emptyView.showFailureText(getString(R.string.network_err), true);
            }
        });
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        getActivity().unregisterReceiver(delBroadcastReceiver);
    }

    class DelBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String id = intent.getStringExtra("id");
            Iterator<HotBean.StorysEntity> it = hotBeans.iterator();
            while (it.hasNext()) {
                HotBean.StorysEntity se = it.next();
                if (se.getStoryid().equals(id)) {
                    it.remove();
                    adapter.notifyDataSetChanged();
                    return;
                }
            }
        }
    }
}
