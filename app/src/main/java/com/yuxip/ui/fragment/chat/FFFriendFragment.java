package com.yuxip.ui.fragment.chat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.yuxip.JsonBean.FG_FriendJsonBean;
import com.yuxip.JsonBean.FG_GroupJsonBean;
import com.yuxip.R;
import com.yuxip.imservice.entity.RecentInfo;
import com.yuxip.imservice.event.AddFriendConfirmEvent;
import com.yuxip.imservice.event.DelFriendEvent;
import com.yuxip.imservice.event.http.FriendGroupEvent;
import com.yuxip.imservice.manager.http.FriendGroupManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.protobuf.IMMessage;
import com.yuxip.ui.adapter.MyExpandableListAdapter;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.ui.widget.popupwindow.FriendGroupManagerPop;
import com.yuxip.utils.IMUIHelper;

import java.lang.reflect.Field;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * 好友 消息的好友列表界面
 * Created by SummerRC on 2015/5/11.
 */
public class FFFriendFragment extends XYBaseFragment implements View.OnClickListener{
    @InjectView(R.id.progress_bar)
    ProgressBar progress_bar;
    @InjectView(R.id.ll_family_list)
    LinearLayout ll_family_list;
    @InjectView(R.id.ll_story_list)
    LinearLayout ll_story_list;
    @InjectView(R.id.expandableListView)
    ExpandableListView expandableListView;
//    @InjectView(R.id.iv_friend_group_manager)
//    ImageView iv_friend_group_manager;

    private ImageView iv_friend_group_manager;
    private View footerView;
    private MyExpandableListAdapter adapter;
    private FriendGroupManager friendGroupManager;
    private IMService imService;
    private FriendGroupManagerPop pop;
    private IMServiceConnector imServiceConnector = new IMServiceConnector() {

        /** service因异常而断开连接的时候，这个方法才会用到。*/
        @Override
        public void onServiceDisconnected() {
            if (EventBus.getDefault().isRegistered(FFFriendFragment.this)) {
                EventBus.getDefault().unregister(FFFriendFragment.this);
            }
        }

        /** 服务被调用的时候 */
        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle bundle) {
        imServiceConnector.connect(getActivity());
        View view = inflater.inflate(R.layout.ff_fragment_friend, null);
        ButterKnife.inject(this, view);
        EventBus.getDefault().register(this);
        initView();
        initData();
        return view;
    }

    private void initView() {
        initFootView();
        progress_bar.setVisibility(View.INVISIBLE);
        ll_family_list.setOnClickListener(this);
        ll_story_list.setOnClickListener(this);
        iv_friend_group_manager.setOnClickListener(this);
        pop = new FriendGroupManagerPop(getActivity());
        pop.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
            }
        });
        adapter = new MyExpandableListAdapter(getActivity());
        expandableListView.setAdapter(adapter);
        expandableListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                pop.show(view);
                return true;
            }
        });
        expandableListView.addFooterView(footerView);
    }

    private void initData() {
        friendGroupManager = FriendGroupManager.getInstance();
        friendGroupManager.getFriendListWithGroupOK(progress_bar);
    }

    private void initFootView(){
        footerView = LayoutInflater.from(getActivity()).inflate(R.layout.layout_friend_footer,null);
        iv_friend_group_manager = (ImageView) footerView.findViewById(R.id.iv_friend_group_manager);
    }

    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        imServiceConnector.disconnect(getActivity());
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }


    /**
     * 分组信息有变更之后刷新UI
     */
    private void refreshUI() {
        List<FG_GroupJsonBean> group = friendGroupManager.getGroup();                   //分组信息
        List<List<FG_FriendJsonBean>> child = friendGroupManager.getChild();            //所有好友信息
        adapter.setData(group, child);
    }

    /**
     * 网络获取分组信息的事件：成功或者失败
     *
     * @param friendGroupEvent  friendGroupEvent
     */
    public void onEventMainThread(FriendGroupEvent friendGroupEvent) {
        switch (friendGroupEvent.event) {
            case UPDATE_SUCCESS:
            case REFRESH:
                refreshUI();
                break;
            case UPDATE_FAIL:
                break;
        }
    }

    /**
     * 被删除好友的事件通知处理
     *
     * @param delFriendEvent 删除好友的事件
     */
    public void onEventMainThread(DelFriendEvent delFriendEvent) {
        IMMessage.IMMsgDelFriendNotify delFriendNotify = (IMMessage.IMMsgDelFriendNotify) delFriendEvent.object;
        int uid = delFriendNotify.getSessionId();
        /** 构建RecentInfo删除本地会话 */
        RecentInfo recentInfo = new RecentInfo();
        String sessionKey = "1" + "_" + uid;
        recentInfo.setPeerId(uid);
        recentInfo.setSessionType(1);
        recentInfo.setSessionKey(sessionKey);
        imService.getSessionManager().reqRemoveSession(recentInfo);
        friendGroupManager.deleteFriend(uid+"");
        refreshUI();
    }

    public void onEventMainThread(AddFriendConfirmEvent addFriendEvent) {
        IMMessage.IMMsgAddFriendConfirmNotify confirmNotify = (IMMessage.IMMsgAddFriendConfirmNotify) addFriendEvent.object;
        int result = confirmNotify.getResult();
        if (result == 1) {          //同意的场合，更新画面，拒绝的提示已经在上层fragment处理了
            friendGroupManager.getFriendListWithGroupOK(progress_bar);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_family_list:           //家族列表
                IMUIHelper.openHisfamilyActivity(getActivity());
                break;
            case R.id.ll_story_list:            //剧列表
                IMUIHelper.openHisplayActivity(getActivity());
                break;
            case R.id.iv_friend_group_manager:  //分组管理
                pop.openFriendGroupManagerActivity();
                break;
        }

    }
}
