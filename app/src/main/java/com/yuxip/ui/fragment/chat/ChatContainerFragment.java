package com.yuxip.ui.fragment.chat;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.squareup.okhttp.Request;
import com.umeng.analytics.MobclickAgent;
import com.yuxip.DB.entity.ApplyGroupEntity;
import com.yuxip.R;
import com.yuxip.config.ConstantValues;
import com.yuxip.entity.FamilyInfoDao;
import com.yuxip.imservice.entity.MessageType;
import com.yuxip.imservice.event.AddFriendConfirmEvent;
import com.yuxip.imservice.event.AddFriendEvent;
import com.yuxip.imservice.event.ApplyGroupConfirmEvent;
import com.yuxip.imservice.event.ApplyGroupEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.YXUnreadNotifyManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.imservice.manager.http.YXFamilyManager;
import com.yuxip.imservice.service.IMService;
import com.yuxip.imservice.support.IMServiceConnector;
import com.yuxip.protobuf.IMMessage;
import com.yuxip.ui.activity.chat.FamilyAllActivity;
import com.yuxip.ui.customview.GGDialog;
import com.yuxip.ui.customview.MsgPopupWindow;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.Logger;
import com.yuxip.utils.T;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;


/**
 * created by SummerRC
 * 好友的界面:包含顶部的Tab（两个按钮：消息 好友）和一个ViewPager(ViewPager中有两个Fragment,分别是：消息 好友）
 */
public class ChatContainerFragment extends XYBaseFragment implements View.OnClickListener {
    private Logger logger = Logger.getLogger(ChatContainerFragment.class);

    @InjectView(R.id.viewPager)
    ViewPager viewPager;
    @InjectView(R.id.img_gone)
    ImageView imgAllFamily;
    @InjectView(R.id.tab_layout)
    TabLayout tabLayout;
    @InjectView(R.id.img_search)
    ImageView imgPlus;
    private List<Fragment> mFragmentList = new ArrayList<>();       //数据源
    private FragmentAdapter mFragmentAdapter;   //适配器
    private Fragment ff_messageFragment;        //消息
    private Fragment ff_friendFragment;         //好友
    private static String message = "";                //验证消息
    private IMService imService;
    private MyHandler mHandler;
    private MsgPopupWindow msgPopupWindow;
    private int color_select;
    private int color_unSelect;

    private IMServiceConnector imServiceConnector = new IMServiceConnector() {

        @Override
        public void onServiceDisconnected() {
            if (EventBus.getDefault().isRegistered(ChatContainerFragment.this)) {
                EventBus.getDefault().unregister(ChatContainerFragment.this);
            }
        }

        @Override
        public void onIMServiceConnected() {
            imService = imServiceConnector.getIMService();
            mHandler = new MyHandler(ChatContainerFragment.this);
            EventBus.getDefault().registerSticky(ChatContainerFragment.this);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    static class MyHandler extends Handler {
        WeakReference<ChatContainerFragment> mFragment;         //持有FriendFragment对象的弱引用

        MyHandler(ChatContainerFragment fragment) {
            mFragment = new WeakReference<>(fragment);
        }

        @Override
        public void handleMessage(Message msg) {
            final ChatContainerFragment fragment = mFragment.get();
            if (fragment == null) {
                return;
            }
            switch (msg.what) {
                case MessageType.HANDLER_MESSAGE_SUCCESS:
                    int fromid = msg.arg2;       //请求者
                    int toid = msg.arg1;         //本人
                    /** 更新服务器状态 */
                    fragment.updateServerState(String.valueOf(fromid), String.valueOf(toid), null,
                            "1");
                    new GGDialog().showTwoSelcetDialog(fragment.getActivity(), "好友请求", "附加消息:" + message,
                            new GGDialog.OnDialogButtonClickedListenered() {
                                public void onConfirmClicked() {
                                    /** 系统消息 */
                                    IMUIHelper.openSystemMessageActivity(fragment.getActivity());
                                }

                                public void onCancelClicked() {
                                }
                            });
                    message = "";
                    break;
                case MessageType.HANDLER_MESSAGE_ERROR:
                    Toast.makeText(fragment.getActivity(), "数据库存储失败！", Toast.LENGTH_SHORT).show();
                    break;
                case MessageType.HANDLER_MESSAGE_APPLY_GROUP_SUCCESS:
                    ApplyGroupEntity entity = (ApplyGroupEntity) msg.obj;
                    /** 更新服务器状态 */
                    String strType = "3";
                    String fromId;
                    String toId;
                    /** 邀请的场合 */
                    if (entity.getType().equals("1")) {
                        fromId = entity.getCreatorId();
                        toId = entity.getApplyId();
                        strType = "4";
                    } else {
                        fromId = entity.getApplyId();
                        toId = entity.getCreatorId();
                    }
                    fragment.updateServerState(fromId, toId, entity.getGroupId(), strType);
                    break;
            }
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imServiceConnector.connect(getActivity());

        mHandler = new MyHandler(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend_h, null);
        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }


    @Override
    public void onDestroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        imServiceConnector.disconnect(getActivity());
        super.onDestroy();
    }

    private void init() {
        msgPopupWindow = new MsgPopupWindow(getActivity());
        msgPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                imgPlus.setSelected(false);
            }
        });

        imgAllFamily.setOnClickListener(this);
        imgPlus.setOnClickListener(this);
        imgAllFamily.setOnClickListener(this);

        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        color_select = getActivity().getResources().getColor(R.color.pink);
        color_unSelect = Color.parseColor("#4a4a4a");
        tabLayout.setTabTextColors(color_unSelect, color_select);
        String[] titls = {getString(R.string.message), getString(R.string.people)};
        tabLayout.addTab(tabLayout.newTab().setText(titls[0]));
        tabLayout.addTab(tabLayout.newTab().setText(titls[1]));

        //好友搜索
        ff_messageFragment = new FFMessageFragment();
        ff_friendFragment = new FFFriendFragment();

        mFragmentList.add(ff_messageFragment);
        mFragmentList.add(ff_friendFragment);

        mFragmentAdapter = new FragmentAdapter(this.getChildFragmentManager(), mFragmentList,
                titls);
        viewPager.setAdapter(mFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabsFromPagerAdapter(mFragmentAdapter);
    }


    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_gone:     //所有家族
                Intent intent = new Intent(getActivity(), FamilyAllActivity.class);
                startActivity(intent);
                break;
            case R.id.img_search:   //添加
                if (imgPlus.isSelected()) {
                    imgPlus.setSelected(false);
                } else {
                    imgPlus.setSelected(true);
                }
                msgPopupWindow.show(imgPlus);
                break;
        }
    }

    public class FragmentAdapter extends FragmentPagerAdapter {

        List<Fragment> fragmentList = new ArrayList<>();
        String[] titls;

        public FragmentAdapter(FragmentManager fm, List<Fragment> fragmentList, String[] titls) {
            super(fm);
            this.fragmentList = fragmentList;
            this.titls = titls;
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titls[position];
        }
    }

    /**
     * 接收到好友请求的通知
     *
     * @param event
     */
    public void onEventMainThread(AddFriendEvent event) {
        IMMessage.IMMsgAddFriendNotify addFriendNotify = (IMMessage.IMMsgAddFriendNotify) event.object;
        int fromid = addFriendNotify.getSessionId();          //发送请求的id
        int toid = IMLoginManager.instance().getLoginId();    //接受请求的id
        message = addFriendNotify.getMsgData();               //验证消息

        //去服务器请求申请者的信息，并将其存入到数据库
        YXUnreadNotifyManager.instance().getAddFriendEntity(toid, fromid, message, mHandler);
    }


    /**
     * 接收到好友请求确认的通知
     *
     * @param addFriendConfirmEvent
     */
    public void onEventMainThread(AddFriendConfirmEvent addFriendConfirmEvent) {
        /**更新服务器状态 */
        IMMessage.IMMsgAddFriendConfirmNotify addFriendConfirmNotify = (IMMessage.IMMsgAddFriendConfirmNotify) addFriendConfirmEvent.object;
        int result = addFriendConfirmNotify.getResult();
        if (result == 1) {          //同意的场合，更新画面
            String fromid = IMLoginManager.instance().getLoginId() + "";      //发起请求的id  自己
            String toid = addFriendConfirmNotify.getSessionId() + "";         //接受请求的id
            updateServerState(fromid, toid, null, "2");
            /** 更新User信息 */
            ArrayList<Integer> userIds = new ArrayList<>(1);
            userIds.add(Integer.valueOf(toid));
            imService.getContactManager().reqGetDetaillUsers(userIds);
        } else {
            Toast.makeText(getActivity(), "小主，" + addFriendConfirmNotify.getSessionId() + "不想和咱们一起玩耍~", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * 接收到申请/邀请入群的请求的通知
     *
     * @param event
     */
    public void onEventMainThread(ApplyGroupEvent event) {
        IMMessage.IMMsgAddGroupMemberNotify notify = (IMMessage.IMMsgAddGroupMemberNotify) event.object;
        int adminId;
        int applyId;
        int groupId = notify.getGroupId();

        switch (event.event) {
            case MSG_APPLY_GROUP:           //申请入群
                adminId = IMLoginManager.instance().getLoginId();
                applyId = notify.getSessionId();
                YXUnreadNotifyManager.instance().setApplyGroupEntity(adminId, applyId, groupId, "0", mHandler, getActivity());

                new GGDialog().showTwoSelcetDialog(getActivity(), event.type + "请求", "有一个小弟要投靠你!",
                        new GGDialog.OnDialogButtonClickedListenered() {
                            public void onConfirmClicked() {
                                /** 系统消息 */
                                IMUIHelper.openSystemMessageActivity(getActivity());
                            }

                            public void onCancelClicked() {
                            }
                        });
                break;

            case MSG_INVITED_GROUP:         //邀请入群
                adminId = notify.getSessionId();
                applyId = IMLoginManager.instance().getLoginId();
                /** 去服务器请求申请者的信息，并将其存入到数据库 */
                YXUnreadNotifyManager.instance().setApplyGroupEntity(adminId, applyId, groupId, "1", mHandler, getActivity());

                new GGDialog().showTwoSelcetDialog(getActivity(), event.type + "邀请",
                        "有一个" + event.type + "邀请你", new GGDialog.OnDialogButtonClickedListenered() {
                            public void onConfirmClicked() {
                                /** 系统消息 */
                                IMUIHelper.openSystemMessageActivity(getActivity());
                            }

                            public void onCancelClicked() {
                            }
                        });
                break;
        }
    }

    /**
     * 接收到申请/邀请入群确认的通知
     *
     * @param event
     */
    public void onEventMainThread(ApplyGroupConfirmEvent event) {
        /**更新服务器状态 */
        IMMessage.IMMsgAddGroupMemberConfirmNotify addGroupMemberConfirmNotify = (IMMessage.IMMsgAddGroupMemberConfirmNotify) event.object;
        String fromid;
        String toid;
        int result = addGroupMemberConfirmNotify.getResult();

        switch (event.event) {
            case MSG_APPLY_GROUP_CONFIRM:       //申请确认
                fromid = IMLoginManager.instance().getLoginId() + "";             //发起请求的id
                toid = addGroupMemberConfirmNotify.getSessionId() + "";           //接受请求的id
                updateServerState(fromid, toid, String.valueOf(addGroupMemberConfirmNotify.getGroupId()), "4");
                if (result == 1) {
                    FamilyInfoDao familyInfoDao = new FamilyInfoDao();
                    familyInfoDao.setGroupid(addGroupMemberConfirmNotify.getGroupId());
                    YXFamilyManager.instance().addFamilyInfoDao(addGroupMemberConfirmNotify.getGroupId(), familyInfoDao);
                    T.show(getActivity().getApplicationContext(), "管理员已同意你的申请！", 0);
                } else {
                    T.show(getActivity().getApplicationContext(), "管理员拒绝了你的申请！", 0);
                }
                break;
            case MSG_INVITE_GROUP_CONFIRM:      //邀请确认
                fromid = addGroupMemberConfirmNotify.getUserId() + "";
                toid = addGroupMemberConfirmNotify.getSessionId() + "";
                updateServerState(fromid, toid, String.valueOf(addGroupMemberConfirmNotify.getGroupId()), "4");
                if (result == 1) {
                    T.show(getActivity().getApplicationContext(), "对方已接受你的邀请！", 0);
                } else {
                    T.show(getActivity().getApplicationContext(), "对方已拒绝你的邀请！", 0);
                }
                break;
        }

    }


    private void updateServerState(String fromid, String toid, String groupid, final String type) {
        String uid = IMLoginManager.instance().getLoginId() + "";
        /**
         * 放在params里面传递
         */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);            //当前用户id
        params.addParams("fromid", fromid);      //发送请求的id
        params.addParams("toid", toid);          //接受请求的id
        params.addParams("type", type);
        params.addParams("token", "1");
        if (groupid != null) {
            params.addParams("groupid", groupid);
        }

        OkHttpClientManager.postAsy(ConstantValues.UpdateNotifyState, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                try {
//                      Toast.makeText(getActivity(), "成功type:"+type+responseInfo.result.toString(), Toast.LENGTH_SHORT).show();
//                    JSONObject obj = new JSONObject(responseInfo.result);
                } catch (Exception e) {
                    logger.e(e.toString());
                }
            }

            @Override
            public void onError(Request request, Exception e) {

            }
        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("FriendFragmentH(聊天)");
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("FriendFragmentH(聊天)");
    }

}
