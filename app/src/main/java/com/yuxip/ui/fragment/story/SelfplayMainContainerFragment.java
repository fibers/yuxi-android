package com.yuxip.ui.fragment.story;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;

import com.yuxip.DB.entity.NodesEntity;
import com.yuxip.JsonBean.StoryShowTypeJsonBean;
import com.yuxip.R;
import com.yuxip.imservice.event.ChannelSelectEvent;
import com.yuxip.imservice.manager.http.ChannelSelectDataManager;
import com.yuxip.ui.customview.ChannelSelectScrollview;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.ViewPagerScroller;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.greenrobot.event.EventBus;

/**
 * Created by Ly on 2015/8/20.
 * 剧场 自戏viewpager的容器 理论上后期处理与storymaincontainer相同
 * 目前只有两个标签
 */
public class SelfplayMainContainerFragment extends XYBaseFragment implements ChannelSelectScrollview.ChannelSelectCallBack {
    @InjectView(R.id.viewpager_story_play_container)
    ViewPager viewPager;
    @InjectView(R.id.channelselect_view)
    ChannelSelectScrollview channelSelectScrollview;
    private List<Fragment> mFragmentList = new ArrayList<>();
    private MyAdapter adapter;
    private List<NodesEntity> listShowType = new ArrayList<>();
    private View view;
    private ViewPagerScroller viewPagerScroller;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_story_play_container, null);
            ButterKnife.inject(this, view);
        }
        return view;
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    public void onEventMainThread(ChannelSelectEvent event){
        switch (event.eventType){
            case TYPE_SET_STORY_PLAY:
                setShowType(null);
                break;
            case TYPE_HIDE_MORE:
                break;
        }
    }
    private void setShowType(List<StoryShowTypeJsonBean.ShowtypeEntity.NodesEntity> listShowType) {
        this.listShowType = ChannelSelectDataManager.getInstance().getListPlayAll();
        channelSelectScrollview.setBackgroundColor(Color.parseColor("#ffebebeb"));
        channelSelectScrollview.setTextsSize(14f);
        channelSelectScrollview.setDataList(this.listShowType, this);
        initFragmentList();
        adapter.notifyDataSetChanged();
    }


    private void initViewpager() {
        adapter = new MyAdapter(getChildFragmentManager());
        viewPagerScroller = new ViewPagerScroller(viewPager.getContext(), new Interpolator() {
            @Override
            public float getInterpolation(float t) {
                t -= 1.0f;
                return t * t * t * t * t + 1.0f;
            }
        });
        viewPagerScroller.setScrollDuration(500);
        viewPagerScroller.initViewPagerScroll(viewPager);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                channelSelectScrollview.scroll(position);
                viewPagerScroller.setScrollDuration(500);
                viewPagerScroller.initViewPagerScroll(viewPager);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(adapter);

    }


    private void initFragmentList() {
        mFragmentList.clear();
        for (int i = 0; i < listShowType.size(); i++) {
            NewFragmentC fragmentC = NewFragmentC.instance(listShowType.get(i).getId());
            mFragmentList.add(fragmentC);
        }
        initViewpager();
    }

    @Override
    public void setViewpager(int position) {
        viewPagerScroller.setScrollDuration(0);
        viewPagerScroller.initViewPagerScroll(viewPager);
        viewPager.setCurrentItem(position);
    }


    class MyAdapter extends FragmentStatePagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }
    }


}
