package com.yuxip.ui.fragment.square;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.squareup.okhttp.Request;
import com.yuxip.JsonBean.SquareTopic;
import com.yuxip.JsonBean.TopiclistEntity;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.ConstantValues;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.OkHttpClientManager;
import com.yuxip.ui.adapter.SquareAdapter;
import com.yuxip.ui.fragment.base.XYBaseFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by zl on 2015/7/1.
 * 广场界面的子碎片,目前只有一个
 */
public class SquareFragment extends XYBaseFragment implements AppBarLayout.OnOffsetChangedListener {
    private View view;
    private List<TopiclistEntity> squarelist = new ArrayList<>();
    private SquareAdapter squareAdp;
    private String uid;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private View footerView;
    private HashMap<String, String>    topTopicIdMap   =   new HashMap<>();

    private int fromid = 0;
    private int boundaryid = 0;
    private int direction = 0;
    private int fromidTemp = -1;    //貌似会加载多次,用fromidTemp加以限制

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = View.inflate(getActivity(), R.layout.fragment_square, null);
        uid = IMLoginManager.instance().getLoginId() + "";
        listView = (ListView) view.findViewById(R.id.listView);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        squareAdp = new SquareAdapter(getActivity(), squarelist);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecyclerView();
        refresh();
    }

    private void initRecyclerView() {
        listView.addFooterView(footerView = View.inflate(getActivity(), R.layout.footer_view, null));
        listView.setAdapter(squareAdp);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        //重置各种初始参数
                        resetFromId();
                        refresh();
                    }
                });
        swipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (squarelist.isEmpty()) {
                    swipeRefreshLayout.setRefreshing(true);
                }
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount + firstVisibleItem == totalItemCount) {
                    load();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            refresh();
        }
    }

    private void refresh() {
        direction = 0;
        ReqSquareData();
        Log.d("Refresh", "refresh function");
    }
        /**    重置参数   */
    private void resetFromId() {
        fromid = 0;
        boundaryid = 0;
        fromidTemp = -1;
    }

    private void load() {
        if (!swipeRefreshLayout.isRefreshing()) {
            fromid = minTopicId();
            boundaryid = maxTopicId();
            direction = 1;
            if (fromid == fromidTemp) {
                return;
            }
            fromidTemp = fromid;
            ReqSquareData();
            Log.d("Load", "load function");
        }
    }

    private int maxTopicId() {
        if (squarelist != null && !squarelist.isEmpty()) {
            int max = Integer.parseInt(squarelist.get(0).getTopicid());
            for (int i = 1; i < squarelist.size(); i++) {
                int m = Integer.parseInt(squarelist.get(i).getTopicid());
                if (m > max) {
                    max = m;
                }
            }
            return max;
        }
        return 0;
    }

    /**
     * 需要考虑置顶的问题
     * @return
     */
    private int minTopicId() {
        int     minIndex    =   0;
        if (squarelist != null && !squarelist.isEmpty()) {
            int min = Integer.parseInt(squarelist.get(0).getTopicid());
            for (int i = 1; i < squarelist.size(); i++) {
                int m = Integer.parseInt(squarelist.get(i).getTopicid());
                //置顶的情况，如果出现在前面有小的id，那么小id不作为最小id。
                if(minIndex < i && m > min) {
                    min = m;
                    minIndex    =   i;
                } else if (m < min) {
                    min = m;
                    minIndex    =   i;
                }
            }
            return min;
        }
        return 0;
    }

    /**
     * 获取广场
     */
    private void ReqSquareData() {
        /** 放在params里面传递 */
        OkHttpClientManager.RequestParams params = new OkHttpClientManager.RequestParams();
        params.addParams("uid", uid);
        params.addParams("fromid", fromid + "");
        params.addParams("boundaryid", boundaryid + "");
        params.addParams("direction", direction + "");
        params.addParams("count", "10");
        OkHttpClientManager.postAsy(ConstantValues.GetTopics, params.getParams(), new OkHttpClientManager.ResultCallback<String>() {
            /** 请求成功后返回的Json */
            @Override
            public void onResponse(String response) {
                swipeRefreshLayout.setRefreshing(false);
                onReceiveSquareResult(response);
            }

            @Override
            public void onError(Request request, Exception e) {
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    /**
     * 接收返回的数据
     *
     * @param result
     */
    private void onReceiveSquareResult(String result) {
        SquareTopic topicData = new Gson().fromJson(result, SquareTopic.class);
        if (topicData.getResult().equals("1")) {
            List<TopiclistEntity> l = topicData.getTopiclist();
            if (null == l || l.isEmpty()) {
                if (null != footerView) {
                    footerView.setVisibility(View.GONE);
                }
            } else {
                if (direction == 0) {
                    topTopicIdMap.clear();
                    squarelist.clear();
                }
                int oldTopicid  =   0;
                for (int i = l.size() - 1; i >= 0; i --) {
                    TopiclistEntity temp    =   l.get(i);

                    int     newid   =    Integer.valueOf(temp.getTopicid());
                    if(topTopicIdMap.containsKey(temp.getTopicid())) {
                        oldTopicid  =   newid;
                        l.remove(temp);
                        continue;
                    }
                    if (newid < oldTopicid) {
                        topTopicIdMap.put(temp.getTopicid(), "1");
                    }
                    oldTopicid  =   newid;
                }
                squarelist.addAll(l);
                squareAdp.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int i) {
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
