package com.yuxip.ui.fragment.base;

import android.support.v4.app.Fragment;

import com.squareup.leakcanary.RefWatcher;
import com.yuxip.app.IMApplication;

/**
 * Created by SummerRC on 2015/9/14.
 * description:基类用于Fragment内存泄露的检测
 */
public abstract class XYBaseFragment extends Fragment {

    @Override
    public void onDestroy() {
        super.onDestroy();
        RefWatcher refWatcher = IMApplication.getRefWatcher(getActivity());
        refWatcher.watch(this);
    }
}
