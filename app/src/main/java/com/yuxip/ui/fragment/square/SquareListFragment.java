package com.yuxip.ui.fragment.square;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

import com.yuxip.JsonBean.Bannaers;
import com.yuxip.JsonBean.BannersJsonBean;
import com.yuxip.JsonBean.SquareListJsonBean;
import com.yuxip.R;
import com.yuxip.app.IMApplication;
import com.yuxip.config.IntentConstant;
import com.yuxip.imservice.event.http.SquareListEvent;
import com.yuxip.imservice.manager.IMLoginManager;
import com.yuxip.imservice.manager.http.SquareListManager;
import com.yuxip.ui.adapter.SquareListAdapter;
import com.yuxip.ui.customview.SquareListHead;
import com.yuxip.ui.customview.ViewpagerHead;
import com.yuxip.ui.fragment.base.XYBaseFragment;
import com.yuxip.utils.IMUIHelper;
import com.yuxip.utils.T;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by ZQF on 2015/10/13.
 * 广场界面 10.13改版
 */
public class SquareListFragment extends XYBaseFragment {

    private View view;
    private SwipeRefreshLayout mRefreshLayout;
    private ListView mListView;
    private SquareListAdapter mAdapter;
    private View mFootView;
    private SquareListHead headView;
    private List<SquareListJsonBean.SquareListEntity> squarelist = new ArrayList<>();
    private ViewpagerHead viewpagerHead;                        //头部轮播图片的布局 须传递数据
    private List<Bannaers> bannersList = new ArrayList<>();     //轮播图片的banner

    private SquareListManager squareListManager;
    private String uid, mCount = "10";
    private int mIndex = 0;
    private int fromidTemp = -1;    //貌似会加载多次,用fromidTemp加以限制
    private boolean isLoading;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_square, null);
        EventBus.getDefault().register(this);
        squareListManager = SquareListManager.getInstance();
        initView();
        return view;
    }

    private void initView() {
        mListView = (ListView) view.findViewById(R.id.listView);
        mRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        mAdapter = new SquareListAdapter(getActivity(), squarelist);

        uid = IMLoginManager.instance().getLoginId() + "";
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
        refresh();
        mRefreshLayout.setColorSchemeColors(getActivity().getResources().getColor(R.color.pink));
    }

    private void initData() {
//        viewpagerHead = new ViewpagerHead(getActivity(), mRefreshLayout);
//        mListView.addHeaderView(viewpagerHead);
        headView = new SquareListHead(getActivity());
        mListView.addHeaderView(headView);
        mListView.addFooterView(mFootView = View.inflate(getActivity(), R.layout.footer_view, null));
        mListView.setAdapter(mAdapter);
        mFootView.setVisibility(View.GONE);
        mRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refresh();
                    }
                });
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if (squarelist.isEmpty()) {
                    mRefreshLayout.setRefreshing(true);
                }
            }
        });
        mListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (visibleItemCount + firstVisibleItem == totalItemCount) {
                    load();
                }
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                SquareCommentManager.getInstance().setType("1");
//                SquareCommentManager.getInstance().setLzId(squarelist.get(position-1).getCreator().getId());
//                SquareCommentManager.getInstance().setTopicId(squarelist.get(position-1).getTopicID());
                if(position != 0)
                IMUIHelper.openSquareCommentActivity(getActivity(), squarelist.get(position-1).getTitle(), squarelist.get(position-1).getTopicID(), IntentConstant.FLOOR_TYPE_TOPIC);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if (IMApplication.IS_REFRESH) {
            IMApplication.IS_REFRESH = false;
            refresh();
        }
    }

    private void refresh() {
        if (isLoading)
            return;
        isLoading = true;
        mIndex = 0;
//        squareListManager.requestHead(uid);
        ReqSquareData();
    }

    private void load() {
        if (!mRefreshLayout.isRefreshing()) {
            mIndex++;
            ReqSquareData();
        }
    }

    /**
     * 获取广场
     */
    private void ReqSquareData() {
        squareListManager.requestSquareList(uid, "1", String.valueOf(mIndex), mCount);
    }

    public void onEventMainThread(SquareListEvent event) {
        if (event != null) {
            mRefreshLayout.setRefreshing(false);
            switch (event.getEvent()) {
                case HEAD:
                    onRecentBannerDataReady((BannersJsonBean) event.getObject());
                    break;
                case LIST:
                    onReceiveSquareResult((List<SquareListJsonBean.SquareListEntity>) event.getObject());
                    break;
                case REQUEST_FAIL:
                    if (event.getObject() != null) {
                        T.showLong(getActivity(), event.getObject().toString());
                    } else {
                        T.showLong(getActivity(), R.string.network_err);
                    }
                    isLoading = false;
                    mRefreshLayout.setRefreshing(false);
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * 顶部广告图数据
     */
    private void onRecentBannerDataReady(BannersJsonBean banners) {
        if ("1".equals(banners.getResult())) {
            bannersList.clear();
            bannersList.addAll(banners.getBannaers());
            viewpagerHead.setData(bannersList);
        }
    }

    /**
     * 处理返回数据,广场列表
     */
    private void onReceiveSquareResult(List<SquareListJsonBean.SquareListEntity> l) {
        if (null == l || l.isEmpty()) {
            if (null != mFootView) {
                mFootView.setVisibility(View.GONE);
            }
        } else {
            if (mIndex == 0) {
                squarelist.clear();
            }
            squarelist.addAll(l);
            mAdapter.notifyDataSetChanged();
            mFootView.setVisibility(View.VISIBLE);
        }
        isLoading = false;
        mRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
