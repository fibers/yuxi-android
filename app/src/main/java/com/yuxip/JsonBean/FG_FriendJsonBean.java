package com.yuxip.JsonBean;

/**
 * Created by SummerRC on 2015/8/30.
 * description:好友分组列表里的好友实体类
 */
public class FG_FriendJsonBean {
    private String userid;              //用户id
    private String realname;            //昵称
    private String markname;            //备注名
    private String portrait;            //头像
    private String gender;              //性别 0、1


    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }


    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMarkname() {
        return markname;
    }

    public void setMarkname(String markname) {
        this.markname = markname;
    }
}
