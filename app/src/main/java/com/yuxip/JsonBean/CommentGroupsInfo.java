package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by user on 2015/7/23.
 */
public class CommentGroupsInfo {

    /**
     * storys : [{"createtime":"创建时间","title":"被评论主体的标题","creatorid":"创建者id","storyimg":"自戏或者topic的头像","groupid":"35","type":"代表被评论的主体的类型。0: 自戏。 1: 话题","storyid":"自戏的id，或者是话题的id，根据type决定","creatorname":"创建者的名字"},{"createtime":"15655233","title":"关于失眠的话题","creatorid":"10001","storyimg":"http://img.qq.com/head.img","groupid":"1024","type":"1","storyid":"123","creatorname":"一休"}]
     * result : 1
     */
    private List<StorysEntity> storys;
    private String result;

    public void setStorys(List<StorysEntity> storys) {
        this.storys = storys;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<StorysEntity> getStorys() {
        return storys;
    }

    public String getResult() {
        return result;
    }

    public static class StorysEntity {
        /**
         * createtime : 创建时间
         * title : 被评论主体的标题
         * creatorid : 创建者id
         * storyimg : 自戏或者topic的头像
         * groupid : 35
         * type : 代表被评论的主体的类型。0: 自戏。 1: 话题
         * storyid : 自戏的id，或者是话题的id，根据type决定
         * creatorname : 创建者的名字
         */
        private String createtime;
        private String title;
        private String creatorid;
        private String storyimg;
        private String groupid;
        private String type;
        private String storyid;
        private String creatorname;

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setCreatorid(String creatorid) {
            this.creatorid = creatorid;
        }

        public void setStoryimg(String storyimg) {
            this.storyimg = storyimg;
        }

        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public void setType(String type) {
            this.type = type;
        }

        public void setStoryid(String storyid) {
            this.storyid = storyid;
        }

        public void setCreatorname(String creatorname) {
            this.creatorname = creatorname;
        }

        public String getCreatetime() {
            return createtime;
        }

        public String getTitle() {
            return title;
        }

        public String getCreatorid() {
            return creatorid;
        }

        public String getStoryimg() {
            return storyimg;
        }

        public String getGroupid() {
            return groupid;
        }

        public String getType() {
            return type;
        }

        public String getStoryid() {
            return storyid;
        }

        public String getCreatorname() {
            return creatorname;
        }
    }
}
