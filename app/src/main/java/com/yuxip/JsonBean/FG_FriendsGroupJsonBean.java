package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by SummerRC on 2015/8/31.
 * description:好友分组实体类
 */
public class FG_FriendsGroupJsonBean {
    private String groupid;         //分组id
    private String groupname;       //分组名
    private List<FG_FriendJsonBean> groupmembers;       //分组中好友集合

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }

    public List<FG_FriendJsonBean> getGroupmembers() {
        return groupmembers;
    }

    public void setGroupmembers(List<FG_FriendJsonBean> groupmembers) {
        this.groupmembers = groupmembers;
    }


}
