package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/4.
 */
public class StoryRoleApplyJsonBean {
    /**
     * result : 1
     * applystory : 剧名
     * applicantuid : 1000000
     * applyroletype : 教师
     * applicantportrait : http://upyun.com/xxx.png
     * applytext : blah blah blah ...
     * applyrolenatures : [{"id":"1","name":"性别","value":"男"},{"id":"2","name":"性向","value":"女"},{"id":"3","name":"身高","value":"170cm"},"..."]
     * applyrolebackground : blah blah blah ...
     * applicantname : 妖人
     * applytime : 1446139725
     * applystoryid :   剧id
     */
    private String result;
    private String applystory;
    private String applicantuid;
    private String applyroletype;
    private String applicantportrait;
    private String applytext;
    private List<ApplyrolenaturesEntity> applyrolenatures;
    private String applyrolebackground;
    private String applicantname;
    private String applytime;
    private String applystoryid;

    public void setResult(String result) {
        this.result = result;
    }

    public void setApplystory(String applystory) {
        this.applystory = applystory;
    }

    public void setApplicantuid(String applicantuid) {
        this.applicantuid = applicantuid;
    }

    public void setApplyroletype(String applyroletype) {
        this.applyroletype = applyroletype;
    }

    public void setApplicantportrait(String applicantportrait) {
        this.applicantportrait = applicantportrait;
    }

    public void setApplytext(String applytext) {
        this.applytext = applytext;
    }

    public void setApplyrolenatures(List<ApplyrolenaturesEntity> applyrolenatures) {
        this.applyrolenatures = applyrolenatures;
    }

    public void setApplyrolebackground(String applyrolebackground) {
        this.applyrolebackground = applyrolebackground;
    }

    public void setApplicantname(String applicantname) {
        this.applicantname = applicantname;
    }

    public void setApplytime(String applytime) {
        this.applytime = applytime;
    }

    public String getResult() {
        return result;
    }

    public String getApplystory() {
        return applystory;
    }

    public String getApplicantuid() {
        return applicantuid;
    }

    public String getApplyroletype() {
        return applyroletype;
    }

    public String getApplicantportrait() {
        return applicantportrait;
    }

    public String getApplytext() {
        return applytext;
    }

    public List<ApplyrolenaturesEntity> getApplyrolenatures() {
        return applyrolenatures;
    }

    public String getApplyrolebackground() {
        return applyrolebackground;
    }

    public String getApplicantname() {
        return applicantname;
    }

    public String getApplytime() {
        return applytime;
    }

    public String getApplystoryid() {
        return applystoryid;
    }

    public void setApplystoryid(String applystoryid) {
        this.applystoryid = applystoryid;
    }

    public static class ApplyrolenaturesEntity {
        /**
         * id : 1
         * name : 性别
         * value : 男
         */
        private String id;
        private String name;
        private String value;

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }
}
