package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/10/12.
 */
public class TopicCommentDetailJsonBean {

    /**
     * status : {"result":"100","desc":"操作成功"}
     * childComment : [{"floorCount":"0","favorCount":"501","level":"2","commentTime":"89158352453","toUser":{},"fromUser":{"id":"100001","nickName":"纯洁的舍友","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"0","childComment":[],"commentContent":"楼主好机智啊。","commentID":"155481521"},{"floorCount":"0","favorCount":"501","level":"2","commentTime":"89158352453","toUser":{"id":"100001","nickName":"纯洁的舍友","portrait":"http://www.example.com/1.png"},"fromUser":{"id":"100001","nickName":"细菌","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"0","childComment":[],"commentContent":"你的昵称真。。","commentID":"155481521"}]
     * mainComment : {"floorCount":"21","favorCount":"501","level":"1","commentTime":"89158352453","toUser":{},"fromUser":{"id":"100001","nickName":"细菌","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"0","childComment":[],"commentContent":"这个是楼主的聊天内容","commentID":"155481521"}
     */
    private StatusEntity status;
    private List<ChildCommentEntity> childComment;
    private MainCommentEntity mainComment;

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public void setChildComment(List<ChildCommentEntity> childComment) {
        this.childComment = childComment;
    }

    public void setMainComment(MainCommentEntity mainComment) {
        this.mainComment = mainComment;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public List<ChildCommentEntity> getChildComment() {
        return childComment;
    }

    public MainCommentEntity getMainComment() {
        return mainComment;
    }

    public static class StatusEntity {
        /**
         * result : 100
         * desc : 操作成功
         */
        private String result;
        private String desc;

        public void setResult(String result) {
            this.result = result;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getResult() {
            return result;
        }

        public String getDesc() {
            return desc;
        }
    }

    public static class ChildCommentEntity extends TopicDetailsJsonBean.CommentEntity.ChildCommentEntity {

    }

    public static class MainCommentEntity extends TopicDetailsJsonBean.CommentEntity {
    }
}
