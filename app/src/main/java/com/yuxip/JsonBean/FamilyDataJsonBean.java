package com.yuxip.JsonBean;


/**
 * Created by Administrator on 2015/6/4.
 */
public class FamilyDataJsonBean {
    private String result;
    private GroupData familyinfo;

    public void setFamilyinfo(GroupData familyinfo) {
        this.familyinfo = familyinfo;
    }

    public GroupData getFamilyinfo() {
        return familyinfo;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }




}
