package com.yuxip.JsonBean;

import com.yuxip.entity.FamilyInfoEntity;

import java.util.List;

/**
 * Created by Administrator on 2015/6/25.
 */
public class ResponseFamily {
    private String result;
    private String position;
    private List<FamilyInfoEntity> familylist;

    public void setResult(String result) {
        this.result = result;
    }

    public void setFamilylist(List<FamilyInfoEntity> familylist) {
        this.familylist = familylist;
    }

    public String getResult() {
        return result;
    }

    public List<FamilyInfoEntity> getFamilylist() {
        return familylist;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
