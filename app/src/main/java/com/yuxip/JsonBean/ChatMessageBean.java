package com.yuxip.JsonBean;

/**
 * 聊天消息类型
 * Created by HeTianpeng on 2015/07/06.
 */
public class ChatMessageBean extends MessageBean{
    public final static int   MESSAGE_TYPE_STORY  =   0;
    public final static int   MESSAGE_TYPE_OTHER  =   1;
    /**
     * 聊天类型_对方ID
     */
    private String sessionKey;

    /**
     * 对方ID
     */
    private int peerId;


    /**
     * 这条记录的类型。
     */
    private int type;

    /**
     * 获取SeesionKey
     * 格式：聊天类型_对方ID
     * @return
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * 设置SessionKey
     * 格式：聊天类型_对方ID
     * @param sessionKey
     */
    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * 获取对方ID
     * @return
     */
    public int getPeerId() {
        return peerId;
    }

    /**
     * 设置对方的ID
     * @param peerId
     */
    public void setPeerId(int peerId) {
        this.peerId = peerId;
    }

    /**
     * 获取消息的类型。
     * @return
     */
    public int getType() {
        return type;
    }


    /**
     * 设定消息的类型。
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

}
