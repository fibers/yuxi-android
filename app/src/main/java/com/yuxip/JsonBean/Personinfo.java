package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/14.
 */
public class Personinfo {
    private String roleid;
    private String roletitle;
    private String gender;
    private String property;
    private String character;
    private String appearance;
    private String portrait;
    private String favourite;
    private String other;
    private String nickname;

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public void setRoletitle(String roletitle) {
        this.roletitle = roletitle;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setFavourite(String favourite) {
        this.favourite = favourite;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public String getRoleid() {
        return roleid;
    }

    public String getRoletitle() {
        return roletitle;
    }

    public String getGender() {
        return gender;
    }

    public String getProperty() {
        return property;
    }

    public String getCharacter() {
        return character;
    }

    public String getAppearance() {
        return appearance;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getFavourite() {
        return favourite;
    }

    public String getOther() {
        return other;
    }
}
