package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/7/3.
 */
public class TopiclistEntity {
    /**
     * commentnum : 54
     * topicid : 0
     * topictitle : 测试话题题目0
     * creatorid : 100001
     * praisenum : 45
     * creattime : 00000000
     * topiccontent : 测试话题内容0
     * groupid : null
     * topicimgs :
     * creatorname : 测试0
     */
    private int id;
    private String commentnum;
    private String topicid;
    private String topictitle;
    private String creatorid;
    private String praisenum;
    private String creattime;
    private String topiccontent;
    private String groupid;
    private String topicimgs;
    private String creatorname;
    private String creatorportrait;

    public String getCreatorportrait() {
        return creatorportrait;
    }

    public void setCreatorportrait(String creatorportrait) {
        this.creatorportrait = creatorportrait;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    public void setCommentnum(String commentnum) {
        this.commentnum = commentnum;
    }

    public void setTopicid(String topicid) {
        this.topicid = topicid;
    }

    public void setTopictitle(String topictitle) {
        this.topictitle = topictitle;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    public void setPraisenum(String praisenum) {
        this.praisenum = praisenum;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public void setTopiccontent(String topiccontent) {
        this.topiccontent = topiccontent;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public void setTopicimgs(String topicimgs) {
        this.topicimgs = topicimgs;
    }

    public void setCreatorname(String creatorname) {
        this.creatorname = creatorname;
    }

    public String getCommentnum() {
        return commentnum;
    }

    public String getTopicid() {
        return topicid;
    }

    public String getTopictitle() {
        return topictitle;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public String getPraisenum() {
        return praisenum;
    }

    public String getCreattime() {
        return creattime;
    }

    public String getTopiccontent() {
        return topiccontent;
    }

    public String getGroupid() {
        return groupid;
    }

    public String getTopicimgs() {
        return topicimgs;
    }

    public String getCreatorname() {
        return creatorname;
    }
}
