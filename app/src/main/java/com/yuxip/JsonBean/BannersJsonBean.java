package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/6/11.
 */
public class BannersJsonBean {
    private String result;
    private List<Bannaers> bannaers;

    public void setResult(String result) {
        this.result = result;
    }

    public void setBannaers(List<Bannaers> bannaers) {
        this.bannaers = bannaers;
    }

    public String getResult() {
        return result;
    }

    public List<Bannaers> getBannaers() {
        return bannaers;
    }
}
