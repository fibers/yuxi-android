package com.yuxip.JsonBean;

import java.util.List;

/**
 * 消息类
 * Created by HeTianpeng on 2015/07/03.
 */
public class MessageBean {

    /**
     * 详细消息类型
     */
    public interface IMessageTypeDetail {
        MessageType getMessageType();
    }

    /**
     * 消息类型 这个地方不要随便动，其他地方用到了相关顺序
     */
    public enum MessageType {
        MESSAGE_TYPE_SYSTEM,
        MESSAGE_TYPE_PERSONAL,
        MESSAGE_TYPE_GROUP,
        MESSAGE_TYPE_COMMENT,
        MESSAGE_TYPE_Family,
        MESSAGE_TYPE_INVALID
    }

    /**
     * 详细系统消息
     */
    public enum MessageTypeDetailSystem implements IMessageTypeDetail {
        /**
         * 被加好友
         */
        MESSAGE_TYPE_SYSTEM_FRIEDNS_ADD,
        /**
         * 申请加入群组：家族
         */
        MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_ADD,
        /**
         * 申请加入群组：剧
         */
        MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_ADD,
        /**
         * 被邀请加入群组：家族
         */
        MESSAGE_TYPE_SYSTEM_GROUP_FAMILY_INVITATION,
        /**
         * 被邀请加入群组：剧
         */
        MESSAGE_TYPE_SYSTEM_GROUP_DRAMA_INVITATION,

        /**
         * 其他
         */
        MESSAGE_TYPE_SYSTEM_OTHER;

        @Override
        public MessageType getMessageType() {
            return MessageType.MESSAGE_TYPE_SYSTEM;
        }
    }

    /**
     * 详细群组消息
     */
    public enum MessageTypeDetailGroup implements IMessageTypeDetail {
        /**
         * 群消息：家族
         */
        MESSAGE_TYPE_GROUP_FAMILY,
        /**
         * 群消息：剧
         */
        MESSAGE_TYPE_GROUP_DRAMA;

        @Override
        public MessageType getMessageType() {
            return MessageType.MESSAGE_TYPE_GROUP;
        }
    }

    /**
     * 详细个人消息
     */
    public enum MessageTypeDetailPersonal implements IMessageTypeDetail {

        MESSAGE_TYPE_PERSONAL;

        @Override
        public MessageType getMessageType() {
            return MessageType.MESSAGE_TYPE_PERSONAL;
        }
    }

    /**
     * 详细评论消息
     */
    public enum MessageTypeComment implements IMessageTypeDetail {

        MESSAGE_TYPE_COMMENT;

        @Override
        public MessageType getMessageType() {
            return MessageType.MESSAGE_TYPE_COMMENT;
        }
    }


    /**
     * 头像
     */
    private String iconUrl;
    /**
     * 头像
     * 这个是历史遗留，暂时先用的这个,往后希望能转到上面那个
     */
    private List<String> avatar;
    private String name;
    private String message;
    private String time;
    private int messageCount;
    private IMessageTypeDetail iMessageTypeDetail;
    private int id;

    /**
     * 获取消息头像
     *
     * @return
     */
    public String getIconUrl() {
        return iconUrl;
    }

    /**
     * 设置消息头像
     *
     * @param iconUrl
     */
    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    /**
     * 获取消息昵称
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 设置消息昵称
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取消息内容
     *
     * @return
     */
    public String getMessage() {
        return message;
    }

    /**
     * 设置消息内容
     *
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 获取消息时间
     *
     * @return
     */
    public String getTime() {
        return time;
    }

    /**
     * 设置消息时间
     *
     * @param time
     */
    public void setTime(String time) {
        this.time = time;
    }

    /**
     * 获取消息数
     *
     * @return
     */
    public int getMessageCount() {
        return messageCount;
    }

    /**
     * 设置消息数
     *
     * @param messageCount
     */
    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    /**
     * 获取消息类型
     * @return
     */
    public IMessageTypeDetail getiMessageTypeDetail() {
        return iMessageTypeDetail;
    }

    /**
     * 设置消息类型
     * @param iMessageTypeDetail
     */
    public void setiMessageTypeDetail(IMessageTypeDetail iMessageTypeDetail) {
        this.iMessageTypeDetail = iMessageTypeDetail;
    }

    /**\
     * 获取头像
     * @return
     */
    public List<String> getAvatar() {
        return avatar;
    }

    /**
     * 设置头像
     * @param avatar
     */
    public void setAvatar(List<String> avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取个人SessionKey
     * @param applyId 对方id
     * @return
     */
    public static String getSessionKeyPersonal(int applyId) {
        return "1_" + applyId;
    }

    /**
     * 获取群组SessionKey
     * @param applyId 对方id
     * @return
     */
    public static String getSessionKeyGroup(int applyId) {
        return "2_" + applyId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
