package com.yuxip.JsonBean;

import com.yuxip.entity.RemarkNameEntity;

import java.util.List;

/**
 * Created by SummerRC on 2015/8/11.
 * description:Gson解析对应的实体类
 */
public class RemarkNameJsonBean {
    private String result;
    private List<RemarkNameEntity> users;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<RemarkNameEntity> getUsers() {
        return users;
    }

    public void setUsers(List<RemarkNameEntity> users) {
        this.users = users;
    }


    /*public class RemarkNameEntity {
        private int id;
        private String userid;
        private String realname;
        private String nickname;

        private RemarkNameEntity() {}

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }


        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public String getUserid() {
            return userid;
        }

        public void setUserid(String userid) {
            this.userid = userid;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }*/
}
