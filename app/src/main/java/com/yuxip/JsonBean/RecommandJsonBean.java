package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/8/20.
 */
public class RecommandJsonBean {

    /**
     * result : 1
     * selectedcontents : [{"storys":[{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"申请加入剧的人的数量。目前没有用，设置为0","relation":"用户和剧的关系。owner:管理者. other: 浏览者","praisenum":"点赞的数量。例如：15","creatorportrait":"创建者的头像。例如： http://www.limian.com/a.jpg","type":"剧的种类，. 0: 剧。 1: 自戏","ispraised":"是否被本人赞过。1:被评论过。 0: 没有被评论过。","commentgroupid":"评论群的ID。在这个群里进行评论","storyid":"剧的id。111111","creatorname":"创建者的昵称。例如： 辰大神","intro":"剧的内容。这个是三十三离恨天的爱恨情仇","wordcounts":"剧的字的个数","createtime":"创建时间；145671223","commentnum":"评论的数量。例如：15","category":"古风","title":"剧的标题：三十三天离恨天","creatorid":"创建者的ID。例如：11111111","iscomment":"是否被本人评论过。1","storyimg":"剧的图片。http://www.limian.com/1.jpg","storygroupid":"戏群的ID，当为自戏的时候，为0"},{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"0","relation":"other","praisenum":"15","creatorportrait":"http://www.limian.com/b.jpg","type":"1","ispraised":"1","commentgroupid":"1000031","storyid":"222222","creatorname":"辰大神","intro":"这个是三十三离恨天的爱恨情仇","wordcounts":"12","createtime":"145671223","commentnum":"18","category":"古风","title":"三十三天离恨天","creatorid":"100035","iscomment":"0","storyimg":"http://www.limian.com/1.jpg","storygroupid":"1000032"}],"selectedtype":"20 . 这个是点击更多，会跳转的界面显示内容的类型ID","selectedtypetitle":"剧的推荐"},{"storys":[{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"申请加入剧的人的数量。目前没有用，设置为0","relation":"用户和剧的关系。owner:管理者. other: 浏览者","praisenum":"点赞的数量。例如：15","creatorportrait":"创建者的头像。例如： http://www.limian.com/a.jpg","type":"剧的种类，. 0: 剧。 1: 自戏","ispraised":"是否被本人赞过。1:被评论过。 0: 没有被评论过。","commentgroupid":"评论群的ID。在这个群里进行评论","storyid":"剧的id。111111","creatorname":"创建者的昵称。例如： 辰大神","intro":"剧的内容。这个是三十三离恨天的爱恨情仇","wordcounts":"剧的字的个数","createtime":"创建时间；145671223","commentnum":"评论的数量。例如：15","category":"古风","title":"剧的标题：三十三天离恨天","creatorid":"创建者的ID。例如：11111111","iscomment":"是否被本人评论过。1","storyimg":"剧的图片。http://www.limian.com/1.jpg","storygroupid":"戏群的ID，当为自戏的时候，为0"},{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"0","relation":"other","praisenum":"15","creatorportrait":"http://www.limian.com/b.jpg","type":"1","ispraised":"1","commentgroupid":"1000031","storyid":"222222","creatorname":"辰大神","intro":"这个是三十三离恨天的爱恨情仇","wordcounts":"12","createtime":"145671223","commentnum":"18","category":"古风","title":"三十三天离恨天","creatorid":"100035","iscomment":"0","storyimg":"http://www.limian.com/1.jpg","storygroupid":"1000032"}],"selectedtype":"20 . 这个是点击更多，会跳转的界面显示内容的类型ID","selectedtypetitle":"剧的推荐"}]
     */
    private String result;
    private List<SelectedcontentsEntity> selectedcontents;

    public void setResult(String result) {
        this.result = result;
    }

    public void setSelectedcontents(List<SelectedcontentsEntity> selectedcontents) {
        this.selectedcontents = selectedcontents;
    }

    public String getResult() {
        return result;
    }

    public List<SelectedcontentsEntity> getSelectedcontents() {
        return selectedcontents;
    }

    public static class SelectedcontentsEntity {
        /**
         * storys : [{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"申请加入剧的人的数量。目前没有用，设置为0","relation":"用户和剧的关系。owner:管理者. other: 浏览者","praisenum":"点赞的数量。例如：15","creatorportrait":"创建者的头像。例如： http://www.limian.com/a.jpg","type":"剧的种类，. 0: 剧。 1: 自戏","ispraised":"是否被本人赞过。1:被评论过。 0: 没有被评论过。","commentgroupid":"评论群的ID。在这个群里进行评论","storyid":"剧的id。111111","creatorname":"创建者的昵称。例如： 辰大神","intro":"剧的内容。这个是三十三离恨天的爱恨情仇","wordcounts":"剧的字的个数","createtime":"创建时间；145671223","commentnum":"评论的数量。例如：15","category":"古风","title":"剧的标题：三十三天离恨天","creatorid":"创建者的ID。例如：11111111","iscomment":"是否被本人评论过。1","storyimg":"剧的图片。http://www.limian.com/1.jpg","storygroupid":"戏群的ID，当为自戏的时候，为0"},{"ismember":"用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员","applynums":"0","relation":"other","praisenum":"15","creatorportrait":"http://www.limian.com/b.jpg","type":"1","ispraised":"1","commentgroupid":"1000031","storyid":"222222","creatorname":"辰大神","intro":"这个是三十三离恨天的爱恨情仇","wordcounts":"12","createtime":"145671223","commentnum":"18","category":"古风","title":"三十三天离恨天","creatorid":"100035","iscomment":"0","storyimg":"http://www.limian.com/1.jpg","storygroupid":"1000032"}]
         * selectedtype : 20 . 这个是点击更多，会跳转的界面显示内容的类型ID
         * selectedtypetitle : 剧的推荐
         */
        private List<StorysEntity> storys;
        private String selectedtype;
        private String selectedtypetitle;

        public void setStorys(List<StorysEntity> storys) {
            this.storys = storys;
        }

        public void setSelectedtype(String selectedtype) {
            this.selectedtype = selectedtype;
        }

        public void setSelectedtypetitle(String selectedtypetitle) {
            this.selectedtypetitle = selectedtypetitle;
        }

        public List<StorysEntity> getStorys() {
            return storys;
        }

        public String getSelectedtype() {
            return selectedtype;
        }

        public String getSelectedtypetitle() {
            return selectedtypetitle;
        }

        public static class StorysEntity {
            /**
             * ismember : 用户是否是剧的成员（评论群不算在内）1：是剧的成员 0：非剧成员
             * applynums : 申请加入剧的人的数量。目前没有用，设置为0
             * relation : 用户和剧的关系。owner:管理者. other: 浏览者
             * praisenum : 点赞的数量。例如：15
             * creatorportrait : 创建者的头像。例如： http://www.limian.com/a.jpg
             * type : 剧的种类，. 0: 剧。 1: 自戏
             * ispraised : 是否被本人赞过。1:被评论过。 0: 没有被评论过。
             * commentgroupid : 评论群的ID。在这个群里进行评论
             * storyid : 剧的id。111111
             * creatorname : 创建者的昵称。例如： 辰大神
             * intro : 剧的内容。这个是三十三离恨天的爱恨情仇
             * wordcounts : 剧的字的个数
             * createtime : 创建时间；145671223
             * commentnum : 评论的数量。例如：15
             * category : 古风
             * title : 剧的标题：三十三天离恨天
             * creatorid : 创建者的ID。例如：11111111
             * iscomment : 是否被本人评论过。1
             * storyimg : 剧的图片。http://www.limian.com/1.jpg
             * storygroupid : 戏群的ID，当为自戏的时候，为0
             * checkgroupid ：剧里审核群的id，若没有审核群，返回@
             * checkgrouptitle ：审核群的名字
             */
            private String ismember;
            private String applynums;
            private String relation;
            private String praisenum;
            private String creatorportrait;
            private String type;
            private String ispraised;
            private String commentgroupid;
            private String storyid;
            private String creatorname;
            private String intro;
            private String wordcounts;
            private String createtime;
            private String commentnum;
            private String category;
            private String title;
            private String creatorid;
            private String iscomment;
            private String storyimg;
            private String storygroupid;

            private String checkgroupid;
            private String checkgrouptitle;
            private String updatetime;

            public void setIsmember(String ismember) {
                this.ismember = ismember;
            }

            public void setApplynums(String applynums) {
                this.applynums = applynums;
            }

            public void setRelation(String relation) {
                this.relation = relation;
            }

            public void setPraisenum(String praisenum) {
                this.praisenum = praisenum;
            }

            public void setCreatorportrait(String creatorportrait) {
                this.creatorportrait = creatorportrait;
            }

            public void setType(String type) {
                this.type = type;
            }

            public void setIspraised(String ispraised) {
                this.ispraised = ispraised;
            }

            public void setCommentgroupid(String commentgroupid) {
                this.commentgroupid = commentgroupid;
            }

            public void setStoryid(String storyid) {
                this.storyid = storyid;
            }

            public void setCreatorname(String creatorname) {
                this.creatorname = creatorname;
            }

            public void setIntro(String intro) {
                this.intro = intro;
            }

            public void setWordcounts(String wordcounts) {
                this.wordcounts = wordcounts;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }

            public void setCommentnum(String commentnum) {
                this.commentnum = commentnum;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public void setCreatorid(String creatorid) {
                this.creatorid = creatorid;
            }

            public void setIscomment(String iscomment) {
                this.iscomment = iscomment;
            }

            public void setStoryimg(String storyimg) {
                this.storyimg = storyimg;
            }

            public void setStorygroupid(String storygroupid) {
                this.storygroupid = storygroupid;
            }

            public String getIsmember() {
                return ismember;
            }

            public String getApplynums() {
                return applynums;
            }

            public String getRelation() {
                return relation;
            }

            public String getPraisenum() {
                return praisenum;
            }

            public String getCreatorportrait() {
                return creatorportrait;
            }

            public String getType() {
                return type;
            }

            public String getIspraised() {
                return ispraised;
            }

            public String getCommentgroupid() {
                return commentgroupid;
            }

            public String getStoryid() {
                return storyid;
            }

            public String getCreatorname() {
                return creatorname;
            }

            public String getIntro() {
                return intro;
            }

            public String getWordcounts() {
                return wordcounts;
            }

            public String getCreatetime() {
                return createtime;
            }

            public String getCommentnum() {
                return commentnum;
            }

            public String getCategory() {
                return category;
            }

            public String getTitle() {
                return title;
            }

            public String getCreatorid() {
                return creatorid;
            }

            public String getIscomment() {
                return iscomment;
            }

            public String getStoryimg() {
                return storyimg;
            }

            public String getStorygroupid() {
                return storygroupid;
            }

            public String getCheckgroupid() {
                return checkgroupid;
            }

            public void setCheckgroupid(String checkgroupid) {
                this.checkgroupid = checkgroupid;
            }

            public String getCheckgrouptitle() {
                return checkgrouptitle;
            }

            public void setCheckgrouptitle(String checkgrouptitle) {
                this.checkgrouptitle = checkgrouptitle;
            }

            public void setUpdatetime(String updatetime) {
                this.updatetime = updatetime;
            }

            public String getUpdatetime() {
                return updatetime;
            }
        }
    }
}
