package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/4.
 */
public class Members {
    /**
     * 剧成员的资料
     */
        private String id;
        private String title;
        private String nickname;
        private String portrait;
        private boolean isChecked = false;

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getNickname() {
            return nickname;
        }

        public String getPortrait() {
            return portrait;
        }

}
