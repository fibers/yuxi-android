package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/5.
 */
public class MyRoleAuditsJsonBean {
    /**
     * result : 1
     * list : [{"applyroletype":"教师","applicantname":"妖人","applyid":"100000","applystoryname":"剧名","applytime":"1446139725"},{"applyroletype":"教师","applicantname":"妖人","applyid":"100000","applystoryname":"剧名","applytime":"1446139725"}]
     */
    private String result;
    private List<ListEntity> list;

    public void setResult(String result) {
        this.result = result;
    }

    public void setList(List<ListEntity> list) {
        this.list = list;
    }

    public String getResult() {
        return result;
    }

    public List<ListEntity> getList() {
        return list;
    }

    public static class ListEntity {
        /**
         * applyroletype : 教师
         * applicantname : 妖人
         * applyid : 100000
         * applystoryname : 剧名
         * applytime : 1446139725
         */
        private String applyroletype;
        private String applicantname;
        private String applyid;
        private String applystoryname;
        private String applytime;
        private String applicantportrait;
        private String applicantuid;

        public String getApplicantuid() {
            return applicantuid;
        }

        public void setApplicantuid(String applicantuid) {
            this.applicantuid = applicantuid;
        }

        public void setApplicantportrait(String applicantportrait) {
            this.applicantportrait = applicantportrait;
        }

        public String getApplicantportrait() {
            return applicantportrait;
        }

        public void setApplyroletype(String applyroletype) {
            this.applyroletype = applyroletype;
        }

        public void setApplicantname(String applicantname) {
            this.applicantname = applicantname;
        }

        public void setApplyid(String applyid) {
            this.applyid = applyid;
        }

        public void setApplystoryname(String applystoryname) {
            this.applystoryname = applystoryname;
        }

        public void setApplytime(String applytime) {
            this.applytime = applytime;
        }

        public String getApplyroletype() {
            return applyroletype;
        }

        public String getApplicantname() {
            return applicantname;
        }

        public String getApplyid() {
            return applyid;
        }

        public String getApplystoryname() {
            return applystoryname;
        }

        public String getApplytime() {
            return applytime;
        }
    }
}
