package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by SummerRC on 2015/8/31.
 * description:好友分组组名的实体类
 */
public class FG_GSon_GetFriendListWithGroup {
    private String result;
    private List<FG_FriendsGroupJsonBean> friendgroups;
    private String datetime;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<FG_FriendsGroupJsonBean> getFriendgroups() {
        return friendgroups;
    }

    public void setFriendgroups(List<FG_FriendsGroupJsonBean> friendgroups) {
        this.friendgroups = friendgroups;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
