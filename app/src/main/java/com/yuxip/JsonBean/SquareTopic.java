package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/7/2.
 */
public class SquareTopic {


    /**
     * topiclist : [{"commentnum":"54","topicid":"0","topictitle":"测试话题题目0","creatorid":"100001","praisenum":"45","creattime":"00000000","topiccontent":"测试话题内容0","groupid":null,"topicimgs":"","creatorname":"测试0"},{"commentnum":"54","topicid":"1","topictitle":"测试话题题目1","creatorid":"100002","praisenum":"45","creattime":"00000000","topiccontent":"测试话题内容1","groupid":null,"topicimgs":"","creatorname":"测试1"},{"commentnum":"54","topicid":"2","topictitle":"测试话题题目2","creatorid":"100003","praisenum":"45","creattime":"00000000","topiccontent":"测试话题内容2","groupid":null,"topicimgs":"","creatorname":"测试2"},{"commentnum":"54","topicid":"3","topictitle":"测试话题题目3","creatorid":"100004","praisenum":"45","creattime":"00000000","topiccontent":"测试话题内容3","groupid":null,"topicimgs":"","creatorname":"测试3"},{"commentnum":"54","topicid":"4","topictitle":"测试话题题目4","creatorid":"100005","praisenum":"45","creattime":"00000000","topiccontent":"测试话题内容4","groupid":null,"topicimgs":"","creatorname":"测试4"}]
     * position : 5
     * result : 1
     * direction : null
     */
    private List<TopiclistEntity> topiclist;
    private String position;
    private String result;
    private String direction;

    public void setTopiclist(List<TopiclistEntity> topiclist) {
        this.topiclist = topiclist;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public List<TopiclistEntity> getTopiclist() {
        return topiclist;
    }

    public String getPosition() {
        return position;
    }

    public String getResult() {
        return result;
    }

    public String getDirection() {
        return direction;
    }


}
