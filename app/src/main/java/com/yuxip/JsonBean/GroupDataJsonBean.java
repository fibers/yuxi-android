package com.yuxip.JsonBean;


/**
 * Created by Administrator on 2015/6/4.
 */
public class GroupDataJsonBean {
    private String result;
    private GroupData groupinfo;

    public void setGroupData(GroupData groupData) {
        this.groupinfo = groupData;
    }

    public GroupData getGroupData() {
        return groupinfo;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }




}
