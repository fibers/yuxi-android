package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/3.
 */
public class StoryRoleNature {
    /**
     * result : 1
     * list : [{"id":"1","name":"性别"},{"id":"2","name":"性向"},{"id":"3","name":"身高"}]
     */
    private String result;
    private List<ListEntity> list;

    public void setResult(String result) {
        this.result = result;
    }

    public void setList(List<ListEntity> list) {
        this.list = list;
    }

    public String getResult() {
        return result;
    }

    public List<ListEntity> getList() {
        return list;
    }

    public static class ListEntity {
        /**
         * id : 1
         * name : 性别
         */
        private String id;
        private String name;

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
