package com.yuxip.JsonBean;

import java.util.LinkedList;

/**
 * Created by Administrator on 2015/7/1.
 */
public class HisplayResult {

    /**
     * storys : [{"nickname":"","praisenum":"0","relation":"owner","applynums":"0","copyright":"15%","creatorname":"Debug","portrait":"http://cosimage.b0.upaiyun.com/youxi/1000204_201506242007415.png","intro":"1","createtime":"1435147622","id":"931","praisednum":"0","title":"1","creatorid":"1000204","wordsnum":"0","role":"","groups":[{"title":"","isplay":"0","groupid":"2406"},{"title":"","isplay":"2","groupid":"2407"},{"title":"","isplay":"1","groupid":"2408"}],"ispraisedbyuser":"0"}]
     * result : 1
     */
    private LinkedList<StorysEntity> storys;
    private String result;

    public void setStorys(LinkedList<StorysEntity> storys) {
        this.storys = storys;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public LinkedList<StorysEntity> getStorys() {
        return storys;
    }

    public String getResult() {
        return result;
    }

    public class StorysEntity {
        /**
         * nickname :
         * praisenum : 0
         * relation : owner
         * applynums : 0
         * copyright : 15%
         * creatorname : Debug
         * portrait : http://cosimage.b0.upaiyun.com/youxi/1000204_201506242007415.png
         * intro : 1
         * createtime : 1435147622
         * id : 931
         * praisednum : 0
         * title : 1
         * creatorid : 1000204
         * wordsnum : 0
         * role :
         * groups : [{"title":"","isplay":"0","groupid":"2406"},{"title":"","isplay":"2","groupid":"2407"},{"title":"","isplay":"1","groupid":"2408"}]
         * ispraisedbyuser : 0
         */
        private String nickname;
        private String praisenum;
        private String relation;
        private String applynums;
        private String copyright;
        private String creatorname;
        private String portrait;
        private String intro;
        private String createtime;
        private String id;
        private String praisednum;
        private String title;
        private String creatorid;
        private String wordsnum;
        private String role;
        private LinkedList<GroupsEntity> groups;
        private String ispraisedbyuser;

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setPraisenum(String praisenum) {
            this.praisenum = praisenum;
        }

        public void setRelation(String relation) {
            this.relation = relation;
        }

        public void setApplynums(String applynums) {
            this.applynums = applynums;
        }

        public void setCopyright(String copyright) {
            this.copyright = copyright;
        }

        public void setCreatorname(String creatorname) {
            this.creatorname = creatorname;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setPraisednum(String praisednum) {
            this.praisednum = praisednum;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setCreatorid(String creatorid) {
            this.creatorid = creatorid;
        }

        public void setWordsnum(String wordsnum) {
            this.wordsnum = wordsnum;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public void setGroups(LinkedList<GroupsEntity> groups) {
            this.groups = groups;
        }

        public void setIspraisedbyuser(String ispraisedbyuser) {
            this.ispraisedbyuser = ispraisedbyuser;
        }

        public String getNickname() {
            return nickname;
        }

        public String getPraisenum() {
            return praisenum;
        }

        public String getRelation() {
            return relation;
        }

        public String getApplynums() {
            return applynums;
        }

        public String getCopyright() {
            return copyright;
        }

        public String getCreatorname() {
            return creatorname;
        }

        public String getPortrait() {
            return portrait;
        }

        public String getIntro() {
            return intro;
        }

        public String getCreatetime() {
            return createtime;
        }

        public String getId() {
            return id;
        }

        public String getPraisednum() {
            return praisednum;
        }

        public String getTitle() {
            return title;
        }

        public String getCreatorid() {
            return creatorid;
        }

        public String getWordsnum() {
            return wordsnum;
        }

        public String getRole() {
            return role;
        }

        public LinkedList<GroupsEntity> getGroups() {
            return groups;
        }

        public String getIspraisedbyuser() {
            return ispraisedbyuser;
        }

        public class GroupsEntity {
            /**
             * title :
             * isplay : 0
             * groupid : 2406
             */
            private String title;
            private String isplay;
            private String groupid;

            public void setTitle(String title) {
                this.title = title;
            }

            public void setIsplay(String isplay) {
                this.isplay = isplay;
            }

            public void setGroupid(String groupid) {
                this.groupid = groupid;
            }

            public String getTitle() {
                return title;
            }

            public String getIsplay() {
                return isplay;
            }

            public String getGroupid() {
                return groupid;
            }
        }
    }
}
