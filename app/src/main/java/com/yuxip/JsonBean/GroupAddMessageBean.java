package com.yuxip.JsonBean;

import com.yuxip.DB.entity.ApplyGroupEntity;

/**
 * 群的申请或邀请
 * Created by HeTianpeng on 2015/07/07.
 */
public class GroupAddMessageBean extends AddMessageBean {

    /**
     * 群id
     */
    private String groupId;
    /**
     * 家族id or 剧id
     */
    private String entityId;

    /**
     * 申请者名字
     */
    private String applyName;

    private ApplyGroupEntity applyGroupEntity;

    /**
     * 获取群id
     * @return
     */
    public String getGroupId() {
        return groupId;
    }

    /**
     * 设置群id
     * @param groupId
     */
    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    /**
     * 获取“家族” or “剧”id
     * 依据MessageTypeDetailSystem而定
     * @return
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * 设置“家族” or “剧”id
     * 依据MessageTypeDetailSystem而定
     * @return
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * 获取“原有的，申请 or 被邀请 进群的，数据结构的实体”
     * 这里是为了便于“数据库的操作”，因为数据库的数据结构就是这个，
     * 与我现在写的不同
     * @return
     */
    public ApplyGroupEntity getApplyGroupEntity() {
        return applyGroupEntity;
    }

    /**
     * 设置“原有的，申请 or 被邀请 进群的，数据结构的实体”
     * 这里是为了便于“数据库的操作”，因为数据库的数据结构就是这个，
     * 与我现在写的不同
     * @param applyGroupEntity
     */
    public void setApplyGroupEntity(ApplyGroupEntity applyGroupEntity) {
        this.applyGroupEntity = applyGroupEntity;
    }

    /**
     * 获取申请者name
     * @return
     */
    public String getApplyName() {
        return applyName;
    }

    /**
     * 设置申请者name
     * @param applyName
     */
    public void setApplyName(String applyName) {
        this.applyName = applyName;
    }
}
