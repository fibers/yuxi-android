package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/10/9.
 */
public class TopicDetailsJsonBean {

    /**
     * status : {"result":"100","desc":"操作成功"}
     * topicDetail : {"content":"话题的内容1","favorCount":"30","createTime":"9562787843","title":"话题的标题1","isFavor":"0","collectionCount":"20","images":["http://www.example.com/1.png","http://www.example.com/2.png","http://www.example.com/3.png"],"topicID":"125126282233","commentCount":"10","creator":{"id":"100001","nickName":"细菌","portrait":"http://www.example.com/1.png"}}
     * hotComment : [{"floorCount":"214","favorCount":"501","level":"1","commentTime":"89158352453","toUser":{},"fromUser":{"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"2","childComment":[{"floorCount":"0","favorCount":"501","level":"2","commentTime":"89158352453","toUser":{"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"},"fromUser":{"id":"1001548","nickName":"第二级用户昵称1","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"0","childComment":[],"commentContent":"这个是一个二级的评论内容1","commentID":"12342745815"}],"commentContent":"这个是一个一级的评论内容2","commentID":"12342745815"}]
     * comment : [{"floorCount":"2","favorCount":"501","level":"1","commentTime":"89158352453","toUser":{},"fromUser":{"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"},"totalChildCommentCount":"2","childComment":[{"floorCount":"0","favorCount":"501","level":"2","commentTime":"89158352453","toUser":{"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"},"fromUser":{"id":"1001548","nickName":"第二级用户昵称1","portrait":"http://www.example.com/1.png"},"childComment":[],"commentContent":"这个是一个二级的评论内容1","commentID":"12342745815"}],"commentContent":"这个是一个一级的评论内容2","commentID":"12342745815"}]
     */
    private StatusEntity status;
    private TopicDetailEntity topicDetail;
    private List<HotCommentEntity> hotComment;
    private List<CommentEntity> comment;

    public void setStatus(StatusEntity status) {
        this.status = status;
    }

    public void setTopicDetail(TopicDetailEntity topicDetail) {
        this.topicDetail = topicDetail;
    }

    public void setHotComment(List<HotCommentEntity> hotComment) {
        this.hotComment = hotComment;
    }

    public void setComment(List<CommentEntity> comment) {
        this.comment = comment;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public TopicDetailEntity getTopicDetail() {
        return topicDetail;
    }

    public List<HotCommentEntity> getHotComment() {
        return hotComment;
    }

    public List<CommentEntity> getComment() {
        return comment;
    }

    public static class StatusEntity {
        /**
         * result : 100
         * desc : 操作成功
         */
        private String result;
        private String desc;

        public void setResult(String result) {
            this.result = result;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public String getResult() {
            return result;
        }

        public String getDesc() {
            return desc;
        }
    }

    public static class TopicDetailEntity {
        /**
         * content : 话题的内容1
         * favorCount : 30
         * createTime : 9562787843
         * title : 话题的标题1
         * isFavor : 0
         * collectionCount : 20
         * images : ["http://www.example.com/1.png","http://www.example.com/2.png","http://www.example.com/3.png"]
         * topicID : 125126282233
         * commentCount : 10
         * creator : {"id":"100001","nickName":"细菌","portrait":"http://www.example.com/1.png"}
         */
        private String content;
        private String favorCount;
        private String createTime;
        private String title;
        private String isFavor;
        private String isCollection;
        private String collectionCount;
        private List<String> images;
        private String topicID;
        private String commentCount;
        private CreatorEntity creator;

        public String getIsCollection() {
            return isCollection;
        }

        public void setIsCollection(String isCollection) {
            this.isCollection = isCollection;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setFavorCount(String favorCount) {
            this.favorCount = favorCount;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setIsFavor(String isFavor) {
            this.isFavor = isFavor;
        }

        public void setCollectionCount(String collectionCount) {
            this.collectionCount = collectionCount;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public void setTopicID(String topicID) {
            this.topicID = topicID;
        }

        public void setCommentCount(String commentCount) {
            this.commentCount = commentCount;
        }

        public void setCreator(CreatorEntity creator) {
            this.creator = creator;
        }

        public String getContent() {
            return content;
        }

        public String getFavorCount() {
            return favorCount;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getTitle() {
            return title;
        }

        public String getIsFavor() {
            return isFavor;
        }

        public String getCollectionCount() {
            return collectionCount;
        }

        public List<String> getImages() {
            return images;
        }

        public String getTopicID() {
            return topicID;
        }

        public String getCommentCount() {
            return commentCount;
        }

        public CreatorEntity getCreator() {
            return creator;
        }

        public static class CreatorEntity {
            /**
             * id : 100001
             * nickName : 细菌
             * portrait : http://www.example.com/1.png
             */
            private String id;
            private String nickName;
            private String portrait;

            public void setId(String id) {
                this.id = id;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public void setPortrait(String portrait) {
                this.portrait = portrait;
            }

            public String getId() {
                return id;
            }

            public String getNickName() {
                return nickName;
            }

            public String getPortrait() {
                return portrait;
            }
        }
    }

    public static class HotCommentEntity extends CommentEntity {
    }

    public static class CommentEntity {
        /**
         * floorCount : 2
         * favorCount : 501
         * level : 1
         * commentTime : 89158352453
         * toUser : {}
         * fromUser : {"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"}
         * totalChildCommentCount : 2
         * childComment : [{"floorCount":"0","favorCount":"501","level":"2","commentTime":"89158352453","toUser":{"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"},"fromUser":{"id":"1001548","nickName":"第二级用户昵称1","portrait":"http://www.example.com/1.png"},"childComment":[],"commentContent":"这个是一个二级的评论内容1","commentID":"12342745815"}]
         * commentContent : 这个是一个一级的评论内容2
         * commentID : 12342745815
         */
        private String floorCount;
        private String favorCount;
        private String level;
        private String commentTime;
        private ToUserEntity toUser;
        private FromUserEntity fromUser;
        private String totalChildCommentCount;
        private List<ChildCommentEntity> childComment;
        private String commentContent;
        private String commentID;
        private String isFavor;
        private String isCollection;

        public String getIsCollection() {
            return isCollection;
        }

        public void setIsCollection(String isCollection) {
            this.isCollection = isCollection;
        }

        public String getIsFavor() {
            return isFavor;
        }

        public void setIsFavor(String isFavor) {
            this.isFavor = isFavor;
        }

        public void setFloorCount(String floorCount) {
            this.floorCount = floorCount;
        }

        public void setFavorCount(String favorCount) {
            this.favorCount = favorCount;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public void setCommentTime(String commentTime) {
            this.commentTime = commentTime;
        }

        public void setToUser(ToUserEntity toUser) {
            this.toUser = toUser;
        }

        public void setFromUser(FromUserEntity fromUser) {
            this.fromUser = fromUser;
        }

        public void setTotalChildCommentCount(String totalChildCommentCount) {
            this.totalChildCommentCount = totalChildCommentCount;
        }

        public void setChildComment(List<ChildCommentEntity> childComment) {
            this.childComment = childComment;
        }

        public void setCommentContent(String commentContent) {
            this.commentContent = commentContent;
        }

        public void setCommentID(String commentID) {
            this.commentID = commentID;
        }

        public String getFloorCount() {
            return floorCount;
        }

        public String getFavorCount() {
            return favorCount;
        }

        public String getLevel() {
            return level;
        }

        public String getCommentTime() {
            return commentTime;
        }

        public ToUserEntity getToUser() {
            return toUser;
        }

        public FromUserEntity getFromUser() {
            return fromUser;
        }

        public String getTotalChildCommentCount() {
            return totalChildCommentCount;
        }

        public List<ChildCommentEntity> getChildComment() {
            return childComment;
        }

        public String getCommentContent() {
            return commentContent;
        }

        public String getCommentID() {
            return commentID;
        }

        public static class ToUserEntity {
        }

        public static class FromUserEntity {
            /**
             * id : 1001548
             * nickName : 第一级用户昵称
             * portrait : http://www.example.com/1.png
             */
            private String id;
            private String nickName;
            private String portrait;

            public void setId(String id) {
                this.id = id;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public void setPortrait(String portrait) {
                this.portrait = portrait;
            }

            public String getId() {
                return id;
            }

            public String getNickName() {
                return nickName;
            }

            public String getPortrait() {
                return portrait;
            }
        }

        public static class ChildCommentEntity {
            /**
             * floorCount : 0
             * favorCount : 501
             * level : 2
             * commentTime : 89158352453
             * toUser : {"id":"1001548","nickName":"第一级用户昵称","portrait":"http://www.example.com/1.png"}
             * fromUser : {"id":"1001548","nickName":"第二级用户昵称1","portrait":"http://www.example.com/1.png"}
             * childComment : []
             * commentContent : 这个是一个二级的评论内容1
             * commentID : 12342745815
             */
            private String floorCount;
            private String favorCount;
            private String level;
            private String commentTime;
            private ToUserEntity toUser;
            private FromUserEntity fromUser;
            private List<?> childComment;
            private String commentContent;
            private String commentID;

            public void setFloorCount(String floorCount) {
                this.floorCount = floorCount;
            }

            public void setFavorCount(String favorCount) {
                this.favorCount = favorCount;
            }

            public void setLevel(String level) {
                this.level = level;
            }

            public void setCommentTime(String commentTime) {
                this.commentTime = commentTime;
            }

            public void setToUser(ToUserEntity toUser) {
                this.toUser = toUser;
            }

            public void setFromUser(FromUserEntity fromUser) {
                this.fromUser = fromUser;
            }

            public void setChildComment(List<?> childComment) {
                this.childComment = childComment;
            }

            public void setCommentContent(String commentContent) {
                this.commentContent = commentContent;
            }

            public void setCommentID(String commentID) {
                this.commentID = commentID;
            }

            public String getFloorCount() {
                return floorCount;
            }

            public String getFavorCount() {
                return favorCount;
            }

            public String getLevel() {
                return level;
            }

            public String getCommentTime() {
                return commentTime;
            }

            public ToUserEntity getToUser() {
                return toUser;
            }

            public FromUserEntity getFromUser() {
                return fromUser;
            }

            public List<?> getChildComment() {
                return childComment;
            }

            public String getCommentContent() {
                return commentContent;
            }

            public String getCommentID() {
                return commentID;
            }

            public static class ToUserEntity {
                /**
                 * id : 1001548
                 * nickName : 第一级用户昵称
                 * portrait : http://www.example.com/1.png
                 */
                private String id;
                private String nickName;
                private String portrait;

                public void setId(String id) {
                    this.id = id;
                }

                public void setNickName(String nickName) {
                    this.nickName = nickName;
                }

                public void setPortrait(String portrait) {
                    this.portrait = portrait;
                }

                public String getId() {
                    return id;
                }

                public String getNickName() {
                    return nickName;
                }

                public String getPortrait() {
                    return portrait;
                }
            }

            public static class FromUserEntity {
                /**
                 * id : 1001548
                 * nickName : 第二级用户昵称1
                 * portrait : http://www.example.com/1.png
                 */
                private String id;
                private String nickName;
                private String portrait;

                public void setId(String id) {
                    this.id = id;
                }

                public void setNickName(String nickName) {
                    this.nickName = nickName;
                }

                public void setPortrait(String portrait) {
                    this.portrait = portrait;
                }

                public String getId() {
                    return id;
                }

                public String getNickName() {
                    return nickName;
                }

                public String getPortrait() {
                    return portrait;
                }
            }
        }
    }
}
