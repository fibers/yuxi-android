package com.yuxip.JsonBean;

/**
 * Created by SummerRC on 2015/8/30.
 * description:好友分组实体类
 */
public class FG_GroupJsonBean {
    private String groupid;         //分组id
    private String groupname;       //分组名


    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getGroupname() {
        return groupname;
    }

    public void setGroupname(String groupname) {
        this.groupname = groupname;
    }
}
