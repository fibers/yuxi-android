package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by ZQF on 2015/10/17.
 */
public class SquareListJsonBean {

    /**
     * Status : {"result":"100","describe":"操作成功"}
     * squareList : [{"topicID":"17205","creator":{"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"},"createTime":"1444898854","title":"11","content":"此次","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/2015/10/1444898837277_896000_595000.jpg","http://cosimage.b0.upaiyun.com/2015/10/1444898837323_896000_595000.jpg"]},{"topicID":"17204","creator":{"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"},"createTime":"1444898805","title":"11","content":"此次","commentCount":"1","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/2015/10/1444898794299_896000_595000.jpg"]},{"topicID":"17202","creator":{"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"},"createTime":"1444898057","title":"11","content":"此次","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/2015/10/1444894512072_584000_875000.jpg"]},{"topicID":"17200","creator":{"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"},"createTime":"1444897708","title":"11","content":"dasd","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/2015/10/1444894512072_584000_875000.jpg"]},{"topicID":"17199","creator":{"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"},"createTime":"1444891317","title":"11","content":"此次","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":[]},{"topicID":"17198","creator":{"id":"1000057","portrait":"http://cosimage.b0.upaiyun.com/youxi/1000057_201506041054540.png?issigned=1","nickName":"夏夏"},"createTime":"1444873844","title":"test911","content":"testcontent","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://www.example.com/1.png","http://www.example.com/2.png","http://www.example.com/3.png"]},{"topicID":"17195","creator":{"id":"1000181","portrait":"http://q.qlogo.cn/qqapp/1104542960/02F9FEC2AABBC5765619F950B65D34D7/100","nickName":"SummerRC"},"createTime":"1444618789","title":"212","content":"1122","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":[]},{"topicID":"17194","creator":{"id":"1000057","portrait":"http://cosimage.b0.upaiyun.com/youxi/1000057_201506041054540.png?issigned=1","nickName":"夏夏"},"createTime":"1444356978","title":"3333","content":"33333","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/youxi/1000057_201506051217987.png"]},{"topicID":"17175","creator":{"id":"1009420","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443504878659_155000_155000.jpg","nickName":"我去"},"createTime":"1443505700","title":"花古堡","content":"感觉快乐","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":["http://cosimage.b0.upaiyun.com/2015/9/1443505697821_488000_600000.jpg"]},{"topicID":"17033","creator":{"id":"1009420","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443504878659_155000_155000.jpg","nickName":"我去"},"createTime":"1443068411","title":"通缉令","content":"是垃圾下午","commentCount":"0","collectionCount":"0","favorCount":"0","isFavor":"0","isCollection":"0","images":[]}]
     */

    private StatusEntity status;
    private List<SquareListEntity> squareList;

    public void setStatus(StatusEntity Status) {
        this.status = Status;
    }

    public void setSquareList(List<SquareListEntity> squareList) {
        this.squareList = squareList;
    }

    public StatusEntity getStatus() {
        return status;
    }

    public List<SquareListEntity> getSquareList() {
        return squareList;
    }

    public static class StatusEntity {
        /**
         * result : 100
         * describe : 操作成功
         */

        private String result;
        private String describe;

        public void setResult(String result) {
            this.result = result;
        }

        public void setDescribe(String describe) {
            this.describe = describe;
        }

        public String getResult() {
            return result;
        }

        public String getDescribe() {
            return describe;
        }
    }

    public static class SquareListEntity {
        /**
         * topicID : 17205
         * creator : {"id":"1000439","portrait":"http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg","nickName":"1000439"}
         * createTime : 1444898854
         * title : 11
         * content : 此次
         * commentCount : 0
         * collectionCount : 0
         * favorCount : 0
         * isFavor : 0
         * isCollection : 0
         * images : ["http://cosimage.b0.upaiyun.com/2015/10/1444898837277_896000_595000.jpg","http://cosimage.b0.upaiyun.com/2015/10/1444898837323_896000_595000.jpg"]
         */

        private String topicID;
        private CreatorEntity creator;
        private String createTime;
        private String title;
        private String content;
        private String commentCount;
        private String collectionCount;
        private String favorCount;
        private String isFavor;
        private String isCollection;
        private List<String> images;

        public void setTopicID(String topicID) {
            this.topicID = topicID;
        }

        public void setCreator(CreatorEntity creator) {
            this.creator = creator;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setCommentCount(String commentCount) {
            this.commentCount = commentCount;
        }

        public void setCollectionCount(String collectionCount) {
            this.collectionCount = collectionCount;
        }

        public void setFavorCount(String favorCount) {
            this.favorCount = favorCount;
        }

        public void setIsFavor(String isFavor) {
            this.isFavor = isFavor;
        }

        public void setIsCollection(String isCollection) {
            this.isCollection = isCollection;
        }

        public void setImages(List<String> images) {
            this.images = images;
        }

        public String getTopicID() {
            return topicID;
        }

        public CreatorEntity getCreator() {
            return creator;
        }

        public String getCreateTime() {
            return createTime;
        }

        public String getTitle() {
            return title;
        }

        public String getContent() {
            return content;
        }

        public String getCommentCount() {
            return commentCount;
        }

        public String getCollectionCount() {
            return collectionCount;
        }

        public String getFavorCount() {
            return favorCount;
        }

        public String getIsFavor() {
            return isFavor;
        }

        public String getIsCollection() {
            return isCollection;
        }

        public List<String> getImages() {
            return images;
        }

        public static class CreatorEntity {
            /**
             * id : 1000439
             * portrait : http://cosimage.b0.upaiyun.com/2015/9/1443436354112_681000_681000.jpg
             * nickName : 1000439
             */

            private String id;
            private String portrait;
            private String nickName;

            public void setId(String id) {
                this.id = id;
            }

            public void setPortrait(String portrait) {
                this.portrait = portrait;
            }

            public void setNickName(String nickName) {
                this.nickName = nickName;
            }

            public String getId() {
                return id;
            }

            public String getPortrait() {
                return portrait;
            }

            public String getNickName() {
                return nickName;
            }
        }
    }
}
