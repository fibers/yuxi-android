package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/6.
 */
public class Groups {
    private String groupid;
    private String title;
    private String isplay;
    private String intro;

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIsplay(String isplay) {
        this.isplay = isplay;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getGroupid() {
        return groupid;
    }

    public String getTitle() {
        return title;
    }

    public String getIsplay() {
        return isplay;
    }

    public String getIntro() {
        return intro;
    }
}
