package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/11/4.
 */
public class RoleTypeSettingEntity {
    private String type;
    private String count;
    private String background;

    public RoleTypeSettingEntity(){
            super();
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getCount() {
        return count;
    }

    public String getBackground() {
        return background;
    }

    @Override
        public String toString() {
            return "{"+"\"type\":"+"\""+type+"\","+"\""+"count"+"\":"+"\""+count+"\","+"\""+"background"+"\":"+"\""+background+"\""+"}";
        }
}
