package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/7/13.
 */
public class RecommendStorysResult {

    /**
     * position : null
     * result : 1
     * direction : 0
     * posterlist : [{"creatorid":"1000286","storycatetory":"原创现代","postercontent":"s46ayhar3tbhq3","creattime":"1436778647","storytitle":"（￣ c￣）y▂ξ。。。","posterid":"10","storyid":"470","creatorname":"裎狄。"},{"creatorid":"1000240","storycatetory":"","postercontent":"75ewsrh66awu5a","creattime":"1436778621","storytitle":"SPN-Lucifer自戏","posterid":"9","storyid":"453","creatorname":"帅炸你全家的年糕爹"},{"creatorid":"1000184","storycatetory":"","postercontent":"bH98hih0jj","creattime":"1436778600","storytitle":"烟盒与花","posterid":"8","storyid":"422","creatorname":"寒单\u2022Iliadº索尔"},{"creatorid":"1000171","storycatetory":"","postercontent":"bqnw4azh54yh5y","creattime":"1436778562","storytitle":"2014，仞儿。","posterid":"7","storyid":"406","creatorname":"陆临。硝烟枪火"},{"creatorid":"1000086","storycatetory":"","postercontent":"nsry","creattime":"1436778539","storytitle":"梦金陵","posterid":"6","storyid":"124","creatorname":"程琰    帝权  -策-花"},{"creatorid":"1000087","storycatetory":"","postercontent":"hatesfdgtaer","creattime":"1436778507","storytitle":"面具主题（一）自戏梗","posterid":"5","storyid":"113","creatorname":"语戏\u2014夏夏"},{"creatorid":"1000057","storycatetory":"","postercontent":"ysrdfgsdry","creattime":"1436778491","storytitle":"黑炎帮主日记--《凌晨，意外收获》","posterid":"4","storyid":"93","creatorname":"夏夏"},{"creatorid":"1000059","storycatetory":"","postercontent":"edghergdfgg","creattime":"1436778423","storytitle":"古风自戏\u2014\u2014武林盟主与魔教教主兄弟相见","posterid":"3","storyid":"85","creatorname":"哈哈"},{"creatorid":"1000755","storycatetory":"","postercontent":"edghergdfgg","creattime":"1436778421","storytitle":"存渣戏","posterid":"2","storyid":"847","creatorname":"欧.白时.辰弈. 帝权.白社会"},{"creatorid":"1000678","storycatetory":"","postercontent":"gasdgasreg","creattime":"1436778382","storytitle":"子非星辰 · 02","posterid":"1","storyid":"807","creatorname":"心干手净喻文州"}]
     */
    private String position;
    private String result;
    private String direction;
    private List<PosterlistEntity> posterlist;

    public void setPosition(String position) {
        this.position = position;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setPosterlist(List<PosterlistEntity> posterlist) {
        this.posterlist = posterlist;
    }

    public String getPosition() {
        return position;
    }

    public String getResult() {
        return result;
    }

    public String getDirection() {
        return direction;
    }

    public List<PosterlistEntity> getPosterlist() {
        return posterlist;
    }

    public class PosterlistEntity {
        /**
         * creatorid : 1000286
         * storycatetory : 原创现代
         * postercontent : s46ayhar3tbhq3
         * creattime : 1436778647
         * storytitle : （￣ c￣）y▂ξ。。。
         * posterid : 10
         * storyid : 470
         * creatorname : 裎狄。
         * createportrait": "http://cosimage.b0.upaiyun.com/2015/7/1437483414805_200000_200000.jpg
         */
        private String creatorid;
        private String storycatetory;
        private String postercontent;
        private String creattime;
        private String storytitle;
        private String posterid;
        private String storyid;
        private String creatorname;
        private String createportrait;

        public String getCreateportrait() {
            return createportrait;
        }

        public void setCreateportrait(String createportrait) {
            this.createportrait = createportrait;
        }

        public void setCreatorid(String creatorid) {
            this.creatorid = creatorid;
        }

        public void setStorycatetory(String storycatetory) {
            this.storycatetory = storycatetory;
        }

        public void setPostercontent(String postercontent) {
            this.postercontent = postercontent;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public void setStorytitle(String storytitle) {
            this.storytitle = storytitle;
        }

        public void setPosterid(String posterid) {
            this.posterid = posterid;
        }

        public void setStoryid(String storyid) {
            this.storyid = storyid;
        }

        public void setCreatorname(String creatorname) {
            this.creatorname = creatorname;
        }

        public String getCreatorid() {
            return creatorid;
        }

        public String getStorycatetory() {
            return storycatetory;
        }

        public String getPostercontent() {
            return postercontent;
        }

        public String getCreattime() {
            return creattime;
        }

        public String getStorytitle() {
            return storytitle;
        }

        public String getPosterid() {
            return posterid;
        }

        public String getStoryid() {
            return storyid;
        }

        public String getCreatorname() {
            return creatorname;
        }
    }
}
