package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/26.
 */
public class NewUserListJsonBean {
    /**
     * userlist : [{"id":"55555","name":"辰氏","gender":"0","registtime":"用户注册的时间，符合UTC标准，例如 1345678.","portrait":"http://www.limian.com/1.jpg","isfriend":"1"},{"id":"55554","name":"辰氏","gender":"1","registtime":"用户注册的时间，符合UTC标准，例如 1345677.","portrait":"http://www.limian.com/2.jpg","isfriend":"0代表非好友"},{"id":"55553","name":"辰氏","gender":"1","registtime":"用户注册的时间，符合UTC标准，例如 1345677.","portrait":"http://www.limian.com/2.jpg","isfriend":"0"}]
     * result : 1
     */
    private List<UserlistEntity> userlist;
    private String result;

    public void setUserlist(List<UserlistEntity> userlist) {
        this.userlist = userlist;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public List<UserlistEntity> getUserlist() {
        return userlist;
    }

    public String getResult() {
        return result;
    }

    public static class UserlistEntity {
        /**
         * id : 55555
         * name : 辰氏
         * gender : 0
         * registtime : 用户注册的时间，符合UTC标准，例如 1345678.
         * portrait : http://www.limian.com/1.jpg
         * isfriend : 1
         */
        private String id;
        private String name;
        private String gender;
        private String registtime;
        private String portrait;
        private String isfriend;

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public void setRegisttime(String registtime) {
            this.registtime = registtime;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public void setIsfriend(String isfriend) {
            this.isfriend = isfriend;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public String getGender() {
            return gender;
        }

        public String getRegisttime() {
            return registtime;
        }

        public String getPortrait() {
            return portrait;
        }

        public String getIsfriend() {
            return isfriend;
        }
    }
}
