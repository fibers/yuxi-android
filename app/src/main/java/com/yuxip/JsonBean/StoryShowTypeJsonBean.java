package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/8/20.
 */
public class StoryShowTypeJsonBean {

    /**
     * result : 1
     * showtype : [{"id":"1","nodes":[{"id":"10。通过这个id从服务器获取消息。服务器利用这个ID来判断","ishighlight":"0 代表是否高亮显示。0: 普通显示。 1: 高亮显示","name":"空。这个是显示用的名字","type":"native. 该节点的类型, 为native或url。 url:代表页面是html页面。native: 本地页面。 ","url":"当类型为url的时候，该栏目代表超链接的网址。类型为native的时候，本项目为空字符串。"}],"name":"精选。这里是一级名称"},{"id":"2","nodes":[{"id":"20","ishighlight":"0","name":"精选","type":"native","url":""},{"id":"21","ishighlight":"1","name":"热门","type":"native","url":""}],"name":"剧。这里是一级名称"},{"id":"2","nodes":[{"id":"20","ishighlight":"0","name":"精选","type":"native","url":""},{"id":"21","ishighlight":"1","name":"热门","type":"native","url":""}],"name":"自戏"}]
     */
    private String result;
    private List<ShowtypeEntity> showtype;
    private List<FamilytypeEntity> familytype;

    public void setResult(String result) {
        this.result = result;
    }

    public void setShowtype(List<ShowtypeEntity> showtype) {
        this.showtype = showtype;
    }

    public String getResult() {
        return result;
    }

    public List<ShowtypeEntity> getShowtype() {
        return showtype;
    }

    public void setFamilytype(List<FamilytypeEntity> familytype) {
        this.familytype = familytype;
    }

    public List<FamilytypeEntity> getFamilytype() {
        return familytype;
    }

    public static class FamilytypeEntity{


        private int id;
        private String typeid;
        private String typename;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTypeid() {
            return typeid;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypeid(String typeid) {
            this.typeid = typeid;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }
    }




    public static class ShowtypeEntity {
        /**
         * id : 1
         * nodes : [{"id":"10。通过这个id从服务器获取消息。服务器利用这个ID来判断","ishighlight":"0 代表是否高亮显示。0: 普通显示。 1: 高亮显示","name":"空。这个是显示用的名字","type":"native. 该节点的类型, 为native或url。 url:代表页面是html页面。native: 本地页面。 ","url":"当类型为url的时候，该栏目代表超链接的网址。类型为native的时候，本项目为空字符串。"}]
         * name : 精选。这里是一级名称   (第一位 精选  第二位 剧  第三位 自习)
         */
        private String id;
        private List<NodesEntity> nodes;
        private String name;

        public void setId(String id) {
            this.id = id;
        }

        public void setNodes(List<NodesEntity> nodes) {
            this.nodes = nodes;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public List<NodesEntity> getNodes() {
            return nodes;
        }

        public String getName() {
            return name;
        }

        public static class NodesEntity  extends com.yuxip.DB.entity.NodesEntity{
//            /**
//             * id : 10。通过这个id从服务器获取消息。服务器利用这个ID来判断
//             * ishighlight : 0 代表是否高亮显示。0: 普通显示。 1: 高亮显示
//             * name : 空。这个是显示用的名字
//             * type : native. 该节点的类型, 为native或url。 url:代表页面是html页面。native: 本地页面。
//             * url : 当类型为url的时候，该栏目代表超链接的网址。类型为native的时候，本项目为空字符串。
//             * loadtype : 载入字段 用于数据库存储 自戏与剧的区别  其中 值  0  表示剧  1 表示自戏
//             */
//            @Id
//            private int _id;
//            private String id;
//            private String ishighlight;
//            private String name;
//            private String type;
//            private String url;
//            private String loadType;
//
//            public String getLoadType() {
//                return loadType;
//            }
//
//            public void setLoadType(String loadType) {
//                this.loadType = loadType;
//            }
//
//            public int get_id() {
//                return _id;
//            }
//
//            public void set_id(int _id) {
//                this._id = _id;
//            }
//
//            public void setId(String id) {
//                this.id = id;
//            }
//
//            public void setIshighlight(String ishighlight) {
//                this.ishighlight = ishighlight;
//            }
//
//            public void setName(String name) {
//                this.name = name;
//            }
//
//            public void setType(String type) {
//                this.type = type;
//            }
//
//            public void setUrl(String url) {
//                this.url = url;
//            }
//
//            public String getId() {
//                return id;
//            }
//
//            public String getIshighlight() {
//                return ishighlight;
//            }
//
//            public String getName() {
//                return name;
//            }
//
//            public String getType() {
//                return type;
//            }
//
//            public String getUrl() {
//                return url;
//            }
        }
    }
}
