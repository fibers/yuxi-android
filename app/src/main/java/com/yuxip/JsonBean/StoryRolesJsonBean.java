package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/8/12.
 */
public class StoryRolesJsonBean {

    /**
     * result : 1
     * roles : [{"id":"1111","num":"1","title":"皇上","intro":"普天之下，莫非王土"},{"id":"2222","num":"5","title":"贵妃","intro":"后宫三千"}]
     */
    private String result;
    private List<RolesEntity> roles;

    public void setResult(String result) {
        this.result = result;
    }

    public void setRoles(List<RolesEntity> roles) {
        this.roles = roles;
    }

    public String getResult() {
        return result;
    }

    public List<RolesEntity> getRoles() {
        return roles;
    }

    public static class RolesEntity {
        /**
         * id : 1111
         * num : 1
         * title : 皇上
         * intro : 普天之下，莫非王土
         */
        private String id;
        private String num;
        private String title;
        private String intro;

        public void setId(String id) {
            this.id = id;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getId() {
            return id;
        }

        public String getNum() {
            return num;
        }

        public String getTitle() {
            return title;
        }

        public String getIntro() {
            return intro;
        }
    }
}
