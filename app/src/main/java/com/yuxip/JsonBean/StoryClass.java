package com.yuxip.JsonBean;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * 剧类别
 * Created by HeTianpeng on 2015/07/13.
 */
public class StoryClass implements Parcelable{

    /**
     * result : 1
     * list : [{"id":"12","name":"原创现代"},{"id":"11","name":"漫画"},{"id":"10","name":"欧美"},{"id":"9","name":"古风"},{"id":"8","name":"宫斗"},{"id":"7","name":"BLC"},{"id":"6","name":"同人"},{"id":"5","name":"小说"},{"id":"4","name":"影视"},{"id":"3","name":"游戏"},{"id":"2","name":"韩圈"},{"id":"1","name":"其它"}]
     */
    private int result;
    private List<ListEntity> list;

    protected StoryClass(Parcel in) {
        result = in.readInt();
    }

    public static final Creator<StoryClass> CREATOR = new Creator<StoryClass>() {
        @Override
        public StoryClass createFromParcel(Parcel in) {
            return new StoryClass(in);
        }

        @Override
        public StoryClass[] newArray(int size) {
            return new StoryClass[size];
        }
    };

    public void setResult(int result) {
        this.result = result;
    }

    public void setList(List<ListEntity> list) {
        this.list = list;
    }

    public int getResult() {
        return result;
    }

    public List<ListEntity> getList() {
        return list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(result);
    }

    public static class ListEntity implements Parcelable{
        /**
         * id : 12
         * name : 原创现代
         */
        private String id;
        private String name;
        private boolean isSelected;

        protected ListEntity(Parcel in) {
            id = in.readString();
            name = in.readString();
        }

        public static final Creator<ListEntity> CREATOR = new Creator<ListEntity>() {
            @Override
            public ListEntity createFromParcel(Parcel in) {
                return new ListEntity(in);
            }

            @Override
            public ListEntity[] newArray(int size) {
                return new ListEntity[size];
            }
        };

        public void setId(String id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public boolean isSelected() {
            return isSelected;
        }

        public void setIsSelected(boolean isSelected) {
            this.isSelected = isSelected;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(id);
            parcel.writeString(name);
        }
    }
}
