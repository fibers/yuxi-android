package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/6/4.
 */
public class GroupData {

        private String id;
        private String intro;
        private String name;
        private String nickname;
        private String ownerid;
        private String ownername;
        private String ownernickname;
        private String rank;
        private String ismember;
        private String permission;
        private String judgegroupid;
        private String isplay;

    public void setIsplay(String isplay) {
        this.isplay = isplay;
    }

    public String getIsplay() {
        return isplay;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getPortrait() {
        return portrait;
    }

    private String portrait;


    public void setJudgegroupid(String judgegroupid) {
        this.judgegroupid = judgegroupid;
    }

    public String getJudgegroupid() {
        return judgegroupid;
    }

    private List<Members> members;

        public String getId() {
            return id;
        }

        public String getIntro() {
            return intro;
        }

        public String getName() {
            return name;
        }

        public String getNickname() {
            return nickname;
        }

        public String getOwnerid() {
            return ownerid;
        }

        public String getOwnername() {
            return ownername;
        }

        public String getOwnernickname() {
            return ownernickname;
        }

        public String getRank() {
            return rank;
        }

        public String getIsmember() {
            return ismember;
        }

        public String getPermission() {
            return permission;
        }

        public List<Members> getMembers() {
            return members;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setOwnerid(String ownerid) {
            this.ownerid = ownerid;
        }

        public void setOwnername(String ownername) {
            this.ownername = ownername;
        }

        public void setOwnernickname(String ownernickname) {
            this.ownernickname = ownernickname;
        }

        public void setRank(String rank) {
            this.rank = rank;
        }

        public void setIsmember(String ismember) {
            this.ismember = ismember;
        }

        public void setPermission(String permission) {
            this.permission = permission;
        }

        public void setMembers(List<Members> members) {
            this.members = members;
        }
}
