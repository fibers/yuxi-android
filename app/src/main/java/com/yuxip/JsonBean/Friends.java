package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/5.
 */
public class Friends {
    private String id;
    private String gender;
    private String name;
    private String portrait;
    private String FirstLetter;
    private boolean isChecked;

    public boolean isChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setFirstLetter(String firstLetter) {
        FirstLetter = firstLetter;
    }

    public String getId() {
        return id;
    }

    public String getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getFirstLetter() {
        return FirstLetter;
    }
}
