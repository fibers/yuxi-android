package com.yuxip.JsonBean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by HeTianpeng on 2015/07/14.
 */
public class StoryDetailsBean implements Serializable {

    /**
     * result : 1
     * updatetime : 1435070693
     * applynums : 0
     * wordcount : 0
     * creatorname : swindler.
     * version : 20150623120927
     * createtime : 1435070693
     * id : 927
     * title : Drrr-无头骑士异闻录
     * scenes : [{"content":"今天发生了一件什么事情","id":"1111","title":"主线剧情1"},{"content":"今天要考试了吧","id":"1112","title":"主线剧情2"}]
     * describe :
     * rule :
     * groups : [{"title":"审核群","isplay":"0","groupid":"2394","members":null},{"title":"水聊群","isplay":"2","groupid":"2395","members":null},{"title":"对戏群","isplay":"1","groupid":"2396","members":null}]
     * ispraisedbyuser : 0
     * ismember : 0
     * relation : none
     * praisenum : 0
     * copyright : http://123.56.154.93:88/ichees/copyright?storyid=927
     * intro : 请随意一些-仅仅是个语c群而已哦。
     * portrait : http://cosimage.b0.upaiyun.com/2015/6/1435071564559_177000_177000.jpg
     * section : http://123.56.154.93:88/ichees/read?storyid=927
     * category : 原创现代,小说
     * creatorid : 1000908
     * storyimg : http://cosimage.b0.upaiyun.com/2015/6/1435070630161_289000_406000.jpg
     * roles : [{"id":"1111","num":"1","title":"皇上","intro":"普天之下，莫非王土"},{"id":"2222","num":"5","title":"贵妃","intro":"后宫三千"}]
     * getReadautoruty : 阅读权限  0 表示无权阅读  1 表示可以查看
     */
    private String result;
    private String updatetime;
    private String applynums;
    private String wordcount;
    private String creatorname;
    private String version;
    private String createtime;
    private String id;
    private String title;
    private List<ScenesEntity> scenes;
    private String describe;
    private String rule;
    private List<GroupsEntity> groups;
    private String ispraisedbyuser;
    private String ismember;
    private String relation;
    private String praisenum;
    private String copyright;
    private String intro;
    private String portrait;
    private String section;
    private String category;
    private String creatorid;
    private String storyimg;
    private String isCollectedByUser;
    private String readautority;
    private List<RolesEntity> roles;


    public void setIsCollectedByUser(String isCollectedByUser) {
        this.isCollectedByUser = isCollectedByUser;
    }

    public String getIsCollectedByUser() {
        return isCollectedByUser;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public void setApplynums(String applynums) {
        this.applynums = applynums;
    }

    public void setWordcount(String wordcount) {
        this.wordcount = wordcount;
    }

    public void setCreatorname(String creatorname) {
        this.creatorname = creatorname;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setCreatetime(String createtime) {
        this.createtime = createtime;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setScenes(List<ScenesEntity> scenes) {
        this.scenes = scenes;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public void setGroups(List<GroupsEntity> groups) {
        this.groups = groups;
    }

    public void setIspraisedbyuser(String ispraisedbyuser) {
        this.ispraisedbyuser = ispraisedbyuser;
    }

    public void setIsmember(String ismember) {
        this.ismember = ismember;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public void setPraisenum(String praisenum) {
        this.praisenum = praisenum;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCreatorid(String creatorid) {
        this.creatorid = creatorid;
    }

    public void setStoryimg(String storyimg) {
        this.storyimg = storyimg;
    }

    public void setRoles(List<RolesEntity> roles) {
        this.roles = roles;
    }

    public String getResult() {
        return result;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public String getApplynums() {
        return applynums;
    }

    public String getWordcount() {
        return wordcount;
    }

    public String getCreatorname() {
        return creatorname;
    }

    public String getVersion() {
        return version;
    }

    public String getCreatetime() {
        return createtime;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<ScenesEntity> getScenes() {
        return scenes;
    }

    public String getDescribe() {
        return describe;
    }

    public String getRule() {
        return rule;
    }

    public List<GroupsEntity> getGroups() {
        return groups;
    }

    public String getIspraisedbyuser() {
        return ispraisedbyuser;
    }

    public String getIsmember() {
        return ismember;
    }

    public String getRelation() {
        return relation;
    }

    public String getPraisenum() {
        return praisenum;
    }

    public String getCopyright() {
        return copyright;
    }

    public String getIntro() {
        return intro;
    }

    public String getPortrait() {
        return portrait;
    }

    public String getSection() {
        return section;
    }

    public String getCategory() {
        return category;
    }

    public String getCreatorid() {
        return creatorid;
    }

    public String getStoryimg() {
        return storyimg;
    }

    public List<RolesEntity> getRoles() {
        return roles;
    }

    public String getReadautority() {
        return readautority;
    }

    public void setReadautority(String readautority) {
        this.readautority = readautority;
    }

    public class ScenesEntity implements Serializable{
        /**
         * content : 今天发生了一件什么事情
         * id : 1111
         * title : 主线剧情1
         */
        private String content;
        private String id;
        private String title;

        public void setContent(String content) {
            this.content = content;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }
    }

    public class GroupsEntity implements Serializable{
        /**
         * title : 审核群
         * isplay : 0
         * groupid : 2394
         * members : null
         */
        private String title;
        private String isplay;
        private String groupid;
        private String members;

        public void setTitle(String title) {
            this.title = title;
        }

        public void setIsplay(String isplay) {
            this.isplay = isplay;
        }

        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public void setMembers(String members) {
            this.members = members;
        }

        public String getTitle() {
            return title;
        }

        public String getIsplay() {
            return isplay;
        }

        public String getGroupid() {
            return groupid;
        }

        public String getMembers() {
            return members;
        }
    }

    public class RolesEntity implements Serializable{
        /**
         * id : 1111
         * num : 1
         * title : 皇上
         * intro : 普天之下，莫非王土
         */
        private String id;
        private String num;
        private String title;
        private String intro;

        public void setId(String id) {
            this.id = id;
        }

        public void setNum(String num) {
            this.num = num;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public String getId() {
            return id;
        }

        public String getNum() {
            return num;
        }

        public String getTitle() {
            return title;
        }

        public String getIntro() {
            return intro;
        }
    }
}
