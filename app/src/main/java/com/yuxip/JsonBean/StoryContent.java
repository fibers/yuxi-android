package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/8/10.
 */
public class StoryContent {

    /**
     * position : 返回数据中的最大（direction为0）或最小（direction为1）的数据在所有数据中的位置
     * result : 1
     * contentlist : [{"content":"ZFFDADFSDFWERWERFDSFDSF#¥@！","senderid":"发送者的ID","msgid":"消息在对应的表中的ID","sendername":"发送者的名字，昵称","updatetime":"更新时间，UTC, 1345678","senderrolename":"发送者的角色名字.例如：[客人]风哥","groupid":"剧对应的戏群的群id"},{"content":"ZFFDADFSDFWERWERFDSFDSF#¥@！","senderid":"100034","msgid":"1000","sendername":"大黑牛","updatetime":"1345678","senderrolename":"发送者的角色名字","groupid":"1000156"}]
     * direction : 获取数据的方向，0: 获取更大的数据。 1: 获取更老得数据
     */
    private String position;
    private String result;
    private List<ContentlistEntity> contentlist;
    private String direction;
    private String totalcontens;

    public void setTotalcontens(String totalcontens) {
        this.totalcontens = totalcontens;
    }

    public String getTotalcontens() {
        return totalcontens;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setContentlist(List<ContentlistEntity> contentlist) {
        this.contentlist = contentlist;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getPosition() {
        return position;
    }

    public String getResult() {
        return result;
    }

    public List<ContentlistEntity> getContentlist() {
        return contentlist;
    }

    public String getDirection() {
        return direction;
    }

    public static class ContentlistEntity {
        /**
         * content : ZFFDADFSDFWERWERFDSFDSF#¥@！
         * senderid : 发送者的ID
         * msgid : 消息在对应的表中的ID
         * sendername : 发送者的名字，昵称
         * updatetime : 更新时间，UTC, 1345678
         * senderrolename : 发送者的角色名字.例如：[客人]风哥
         * groupid : 剧对应的戏群的群id
         */
        private String content;
        private String senderid;
        private String msgid;
        private String sendername;
        private String updatetime;
        private String senderrolename;
        private String groupid;

        public void setContent(String content) {
            this.content = content;
        }

        public void setSenderid(String senderid) {
            this.senderid = senderid;
        }

        public void setMsgid(String msgid) {
            this.msgid = msgid;
        }

        public void setSendername(String sendername) {
            this.sendername = sendername;
        }

        public void setUpdatetime(String updatetime) {
            this.updatetime = updatetime;
        }

        public void setSenderrolename(String senderrolename) {
            this.senderrolename = senderrolename;
        }

        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public String getContent() {
            return content;
        }

        public String getSenderid() {
            return senderid;
        }

        public String getMsgid() {
            return msgid;
        }

        public String getSendername() {
            return sendername;
        }

        public String getUpdatetime() {
            return updatetime;
        }

        public String getSenderrolename() {
            return senderrolename;
        }

        public String getGroupid() {
            return groupid;
        }
    }
}
