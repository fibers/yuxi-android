package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/11/4.
 */
public class StoryRoleSettingJsonBean {
    /**
     * result : 1
     * rolesettings : {"rolerule":"禁黄，禁玛丽苏","roletypes":[{"count":"3","applied":"1","background":"blah blah blah...","type":"老师"},{"count":"4","applied":"0","background":"blah blah blah...","type":"学生"},{"count":"3","applied":"3","background":"blah blah blah...","type":"校长"}],"rolenatures":[{"id":"1","name":"性别"},{"id":"2","name":"性向"},{"id":"3","name":"身高"}]}
     */
    private String result;
    private RolesettingsEntity rolesettings;

    public void setResult(String result) {
        this.result = result;
    }

    public void setRolesettings(RolesettingsEntity rolesettings) {
        this.rolesettings = rolesettings;
    }

    public String getResult() {
        return result;
    }

    public RolesettingsEntity getRolesettings() {
        return rolesettings;
    }

    public static class RolesettingsEntity {
        /**
         * rolerule : 禁黄，禁玛丽苏
         * roletypes : [{"count":"3","applied":"1","background":"blah blah blah...","type":"老师"},{"count":"4","applied":"0","background":"blah blah blah...","type":"学生"},{"count":"3","applied":"3","background":"blah blah blah...","type":"校长"}]
         * rolenatures : [{"id":"1","name":"性别"},{"id":"2","name":"性向"},{"id":"3","name":"身高"}]
         */
        private String rolerule;
        private List<RoletypesEntity> roletypes;
        private List<RolenaturesEntity> rolenatures;

        public void setRolerule(String rolerule) {
            this.rolerule = rolerule;
        }

        public void setRoletypes(List<RoletypesEntity> roletypes) {
            this.roletypes = roletypes;
        }

        public void setRolenatures(List<RolenaturesEntity> rolenatures) {
            this.rolenatures = rolenatures;
        }

        public String getRolerule() {
            return rolerule;
        }

        public List<RoletypesEntity> getRoletypes() {
            return roletypes;
        }

        public List<RolenaturesEntity> getRolenatures() {
            return rolenatures;
        }

        public static class RoletypesEntity {
            /**
             * count : 3
             * applied : 1
             * background : blah blah blah...
             * type : 老师
             */
            private String count;
            private String applied;
            private String background;
            private String type;

            public void setCount(String count) {
                this.count = count;
            }

            public void setApplied(String applied) {
                this.applied = applied;
            }

            public void setBackground(String background) {
                this.background = background;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getCount() {
                return count;
            }

            public String getApplied() {
                return applied;
            }

            public String getBackground() {
                return background;
            }

            public String getType() {
                return type;
            }
        }

        public static class RolenaturesEntity {
            /**
             * id : 1
             * name : 性别
             */
            private String id;
            private String name;

            public void setId(String id) {
                this.id = id;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public String getName() {
                return name;
            }
        }
    }
}
