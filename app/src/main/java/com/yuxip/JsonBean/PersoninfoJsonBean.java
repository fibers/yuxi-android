package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/14.
 */
public class PersoninfoJsonBean {
    private String result;
    private Personinfo personinfo;

    public void setResult(String result) {
        this.result = result;
    }

    public void setPersoninfo(Personinfo personinfo) {
        this.personinfo = personinfo;
    }

    public String getResult() {
        return result;
    }

    public Personinfo getPersoninfo() {
        return personinfo;
    }
}
