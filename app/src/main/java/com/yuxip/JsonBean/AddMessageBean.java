package com.yuxip.JsonBean;

import com.yuxip.DB.entity.ApplyFriendEntity;

/**
 * 被添加   消息数据结构  暂时结构还不是很明确，类名可能还需要改，先这样
 * Created by HeTianpeng on 2015/07/06.
 */
public class AddMessageBean extends MessageBean {
    /**
     *  申请者id
     */
    private int applyId;
    private ApplyFriendEntity applyFriendEntity;
    /**
     * 是否同意
     */
    private boolean isAgree;

    /**
     * 是否拒绝
     */
    private boolean isReject;

    /**
     * 获取申请者id
     * @return
     */
    public int getApplyId() {
        return applyId;
    }

    /**
     * 设置申请者id
     * @param applyId
     */
    public void setApplyId(int applyId) {
        this.applyId = applyId;
    }

    /**
     * 获取“原有的，添加好友的，数据结构的实体”
     * 这里是为了便于“数据库的操作”，因为数据库的数据结构就是这个，
     * 与我现在写的不同
     * @return
     */
    public final ApplyFriendEntity getAddFriendEntity() {
        return applyFriendEntity;
    }

    /**
     * 获取“原有的，添加好友的，数据结构的实体”
     * 这里是为了便于“数据库的操作”，因为数据库的数据结构就是这个，
     * 与我现在写的不同
     * @param addFriendEntity
     */
    public final void setAddFriendEntity(ApplyFriendEntity addFriendEntity) {
        this.applyFriendEntity = addFriendEntity;
    }

    /**
     * 是否已经同意这条请求
     * @return
     */
    public boolean isAgree() {
        return isAgree;
    }

    /**
     * 设置是否同意这条请求
     * @param isAgree
     */
    public void setIsAgree(boolean isAgree) {
        this.isAgree = isAgree;
    }

    /**
     * 是否已经拒绝这条请求
     * @return
     */
    public boolean isReject() {
        return isReject;
    }

    /**
     * 设置是否拒绝这条请求
     * @param isReject
     */
    public void setIsReject(boolean isReject) {
        this.isReject = isReject;
    }
}
