package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/6/11.
 */
public class Bannaers {
    private String storyid;
    private String banner_portrait;
    private String type;
    private String banner_title;
    private String isurl;       // 0    : 无     1      :  有
    private String url;         //
    private String relation;    // none ：非成员  member ： 成员   owner  ： 管理员
    private String isselfstory;

    public String getStoryid() {
        return storyid;
    }

    public String getBanner_portrait() {
        return banner_portrait;
    }

    public String getType() {
        return type;
    }

    public String getBanner_title() {
        return banner_title;
    }

    public void setStoryid(String storyid) {
        this.storyid = storyid;
    }

    public void setBanner_portrait(String banner_portrait) {
        this.banner_portrait = banner_portrait;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setBanner_title(String banner_title) {
        this.banner_title = banner_title;
    }

    public String getIsurl() {
        return isurl;
    }

    public void setIsurl(String isurl) {
        this.isurl = isurl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public void setIsselfstory(String isselfstory) {
        this.isselfstory = isselfstory;
    }

    public String getIsselfstory() {
        return isselfstory;
    }
}
