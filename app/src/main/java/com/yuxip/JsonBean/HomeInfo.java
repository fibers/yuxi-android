package com.yuxip.JsonBean;


import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2015/7/16.
 */
public class HomeInfo {


    /**
     * result : 1
     * personinfo : {"id":"1000305","myselfstorys":[{"title":"[风晴雪自戏]","creattime":"1434904536","storyid":"804"}],"storycount":"2","likeStorys":"3","nickname":"阜杞望","myfavor":[{"id":"471","creatime":"1437014830","title":"全职高手的剧本来一发(/ω＼)","type":"0"},{"id":"667","creatime":"1437014830","title":"全职高手","type":"0"},{"id":"804","creatime":"1437014830","title":"[风晴雪自戏]","type":"1"}],"age":"0","gender":"1","mytopics":[{"id":"471","creatime":"1437014830","title":"全职高手的剧本来一发(/ω＼)"}],"wordcount":"0","portrait":"http://q.qlogo.cn/qqapp/1104542960/A8CC4229D1215A8AC77CBAD6FBE7796A/100","intro":"语c圈阜家家主，主古原全职blc，话废暖男。","mystorys":[{"title":"全职高手","creattime":"1434788332","storyid":"667"}],"isfriend":"0"}
     */
    private String result;
    private PersoninfoEntity personinfo;

    public void setResult(String result) {
        this.result = result;
    }

    public void setPersoninfo(PersoninfoEntity personinfo) {
        this.personinfo = personinfo;
    }

    public String getResult() {
        return result;
    }

    public PersoninfoEntity getPersoninfo() {
        return personinfo;
    }

    public class PersoninfoEntity {
        /**
         * id : 1000305
         * myselfstorys : [{"title":"[风晴雪自戏]","creattime":"1434904536","storyid":"804"}]
         * storycount : 2
         * likeStorys : 3
         * nickname : 阜杞望
         * myfavor : [{"id":"471","creatime":"1437014830","title":"全职高手的剧本来一发(/ω＼)","type":"0"},{"id":"667","creatime":"1437014830","title":"全职高手","type":"0"},{"id":"804","creatime":"1437014830","title":"[风晴雪自戏]","type":"1"}]
         * age : 0
         * gender : 1
         * mytopics : [{"id":"471","creatime":"1437014830","title":"全职高手的剧本来一发(/ω＼)"}]
         * wordcount : 0
         * portrait : http://q.qlogo.cn/qqapp/1104542960/A8CC4229D1215A8AC77CBAD6FBE7796A/100
         * intro : 语c圈阜家家主，主古原全职blc，话废暖男。
         * mystorys : [{"title":"全职高手","creattime":"1434788332","storyid":"667"}]
         * isfriend : 0
         */
        private String id;
        private List<MyHomeEntity> myselfstorys;
        private String storycount;
        private String likeStorys;
        private String nickname;
        private List<MyHomeEntity> myfavor;
        private String age;
        private String gender;
        private List<MyHomeEntity> mytopics;
        private String wordcount = "0";
        private String applycount;
        private String portrait;
        private String intro;
        private List<MyHomeEntity> mystorys;
        private String isfriend;
        private List<MyHomeEntity> myfamily;
        private List<MyHomeEntity> mycollects;

        private String  type;

        public void setId(String id) {
            this.id = id;
        }

        public void setApplycount(String applycount) {
            this.applycount = applycount;
        }

        public String getApplycount() {
            return applycount;
        }

        public List<MyHomeEntity> getMyfamily() {
            return myfamily;
        }

        public void setMyfamily(List<MyHomeEntity> myfamily) {
            this.myfamily = myfamily;
        }

        public List<MyHomeEntity> getMycollects() {
            return mycollects;
        }

        public void setMycollects(List<MyHomeEntity> mycollects) {
            this.mycollects = mycollects;
        }

        public void setMyselfstorys(List<MyHomeEntity> myselfstorys) {
            this.myselfstorys = myselfstorys;
        }

        public void setStorycount(String storycount) {
            this.storycount = storycount;
        }

        public void setLikeStorys(String likeStorys) {
            this.likeStorys = likeStorys;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public void setMyfavor(List<MyHomeEntity> myfavor) {
            this.myfavor = myfavor;
        }

        public void setAge(String age) {
            this.age = age;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public void setMytopics(List<MyHomeEntity> mytopics) {
            this.mytopics = mytopics;
        }

        public void setWordcount(String wordcount) {
            this.wordcount = wordcount;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public void setIntro(String intro) {
            this.intro = intro;
        }

        public void setMystorys(List<MyHomeEntity> mystorys) {
            this.mystorys = mystorys;
        }

        public void setIsfriend(String isfriend) {
            this.isfriend = isfriend;
        }

        public String getId() {
            return id;
        }

        public List<MyHomeEntity> getMyselfstorys() {
            return myselfstorys;
        }

        public String getStorycount() {
            return storycount;
        }

        public String getLikeStorys() {
            return likeStorys;
        }

        public String getNickname() {
            return nickname;
        }

        public List<MyHomeEntity> getMyfavor() {
            return myfavor;
        }

        public String getAge() {
            return age;
        }

        public String getGender() {
            return gender;
        }

        public List<MyHomeEntity> getMytopics() {
            return mytopics;
        }

        public String getWordcount() {
            return wordcount;
        }

        public String getPortrait() {
            return portrait;
        }

        public String getIntro() {
            return intro;
        }

        public List<MyHomeEntity> getMystorys() {
            return mystorys;
        }

        public String getIsfriend() {
            return isfriend;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }


        public class MyHomeEntity implements Serializable{
            /**
             * id : 471
             * creatime : 1437014830
             * title : 全职高手的剧本来一发(/ω＼)
             * type : 0
             */
            private String id;
            private String createtime;
            private String title;
            private String type;
            private String storyid;
            private String topicid;
            private String creatorname;
            private String creatorid;
            private String creattime;
            private String portrait;
            private String creatorportrait;
            private String content;

            public String getPortrait() {
                return portrait;
            }

            public void setPortrait(String portrait) {
                this.portrait = portrait;
            }

            public String getCreatorportrait() {
                return creatorportrait;
            }

            public void setCreatorportrait(String creatorportrait) {
                this.creatorportrait = creatorportrait;
            }

            public String getContent() {
                return content;
            }

            public void setContent(String content) {
                this.content = content;
            }

            public String getCreattime() {
                return creattime;
            }

            public void setCreattime(String creattime) {
                this.creattime = creattime;
            }

            public void setCreatorname(String creatorname) {
                this.creatorname = creatorname;
            }

            public void setCreatorid(String creatorid) {
                this.creatorid = creatorid;
            }

            public String getCreatorid() {
                return creatorid;
            }

            public String getCreatorname() {
                return creatorname;
            }

            public String getTopicid() {
                return topicid;
            }

            public void setTopicid(String topicid) {
                this.topicid = topicid;
            }

            public String getStoryid() {
                return storyid;
            }

            public void setStoryid(String storyid) {
                this.storyid = storyid;
            }

            public void setId(String id) {
                this.id = id;
            }


            public void setTitle(String title) {
                this.title = title;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getId() {
                return id;
            }
            public String getCreatetime() {
                return createtime;
            }

            public void setCreatetime(String createtime) {
                this.createtime = createtime;
            }
            public String getTitle() {
                return title;
            }

            public String getType() {
                return type;
            }
        }

    }
}
