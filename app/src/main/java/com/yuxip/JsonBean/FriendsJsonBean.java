package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/6/5.
 */
public class FriendsJsonBean {
    private String result;
    private List<FriendGroups> groups;

    public String getResult() {
        return result;
    }

    public List<FriendGroups> getGroups() {
        return groups;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public void setGroups(List<FriendGroups> groups) {
        this.groups = groups;
    }
}
