package com.yuxip.JsonBean;

import java.io.Serializable;

/**
 * Created by Administrator on 2015/7/13.
 */
public class BookDetailResult {

    /**
     * result : 1
     * detatils : {"createtime":"1436153838","content":"lol","id":"954","title":"同","creatorid":"1000190","storyimg":"","praisenum":"0","portrait":"http://q.qlogo.cn/qqapp/1104542960/4A770E3889CFB1C142F3DFDD6A4BE4DE/40","creatorname":"我心故我在","ispraisedbyuser":"0"}
     */
    private String result;
    private DetatilsEntity detatils;

    public void setResult(String result) {
        this.result = result;
    }

    public void setDetatils(DetatilsEntity detatils) {
        this.detatils = detatils;
    }

    public String getResult() {
        return result;
    }

    public DetatilsEntity getDetatils() {
        return detatils;
    }

    public class DetatilsEntity implements Serializable{
        /**
         * createtime : 1436153838
         * content : lol
         * id : 954
         * title : 同
         * creatorid : 1000190
         * storyimg : http://cosimage.b0.upaiyun.com/2015/7/1437550109103_415000_259000.jpg
         * praisenum : 0
         * portrait : http://q.qlogo.cn/qqapp/1104542960/4A770E3889CFB1C142F3DFDD6A4BE4DE/40
         * creatorname : 我心故我在
         * ispraisedbyuser : 0
         * commentgroupid : 2931
         * wordcount : 0
         * commentnum : 1
         * isCollectedByUser 0代表未收藏，
         */
        private String createtime;
        private String content;
        private String id;
        private String title;
        private String creatorid;
        private String storyimg;
        private String praisenum;
        private String portrait;
        private String creatorname;
        private String ispraisedbyuser;
        private String commentgroupid;
        private String wordcount;
        private String commentnum;
        private String isCollectedByUser;

        public String getIsCollectedByUser() {
            return isCollectedByUser;
        }

        public void setIsCollectedByUser(String isCollectedByUser) {
            this.isCollectedByUser = isCollectedByUser;
        }

        public void setCreatetime(String createtime) {
            this.createtime = createtime;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setCreatorid(String creatorid) {
            this.creatorid = creatorid;
        }

        public void setStoryimg(String storyimg) {
            this.storyimg = storyimg;
        }

        public void setPraisenum(String praisenum) {
            this.praisenum = praisenum;
        }

        public void setPortrait(String portrait) {
            this.portrait = portrait;
        }

        public void setCreatorname(String creatorname) {
            this.creatorname = creatorname;
        }

        public void setIspraisedbyuser(String ispraisedbyuser) {
            this.ispraisedbyuser = ispraisedbyuser;
        }

        public String getCreatetime() {
            return createtime;
        }

        public String getContent() {
            return content;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getCreatorid() {
            return creatorid;
        }

        public String getStoryimg() {
            return storyimg;
        }

        public String getPraisenum() {
            return praisenum;
        }

        public String getPortrait() {
            return portrait;
        }

        public String getCreatorname() {
            return creatorname;
        }

        public String getIspraisedbyuser() {
            return ispraisedbyuser;
        }

        public String getCommentgroupid() {
            return commentgroupid;
        }

        public void setCommentgroupid(String commentgroupid) {
            this.commentgroupid = commentgroupid;
        }

        public String getWordcount() {
            return wordcount;
        }

        public void setWordcount(String wordcount) {
            this.wordcount = wordcount;
        }

        public String getCommentnum() {
            return commentnum;
        }

        public void setCommentnum(String commentnum) {
            this.commentnum = commentnum;
        }
    }
}
