package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/7/15.
 */
public class DeleteStoryResult {

    /**
     * result : 1
     * describe : 删除成功
     * datetime : 2015-07-15T16:00:16.2368596+08:00
     */
    private String result;
    private String describe;
    private String datetime;

    public void setResult(String result) {
        this.result = result;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getResult() {
        return result;
    }

    public String getDescribe() {
        return describe;
    }

    public String getDatetime() {
        return datetime;
    }
}
