package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/6/5.
 */
public class FriendGroups {
    private String firstLetter;
    private List<Friends> friends;

    public void setFirstLetter(String firstLetter) {
        this.firstLetter = firstLetter;
    }

    public void setFriends(List<Friends> friends) {
        this.friends = friends;
    }

    public String getFirstLetter() {
        return firstLetter;
    }

    public List<Friends> getFriends() {
        return friends;
    }
}
