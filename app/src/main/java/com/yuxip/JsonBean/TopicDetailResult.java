package com.yuxip.JsonBean;

import java.util.List;

/**
 * Created by Administrator on 2015/7/14.
 */
public class TopicDetailResult {

    /**
     * result : 1
     * topicdetail : {"topicid":"话题的id，例如：1","topictitle":"话题标题","iscommented":"是否被当前用户评价过。0: 没有被当前用户评价过。1: 被当前用户评价过","praisenum":"话题点赞数，例如：3","topiccontent":"话题内容","creatorportrait":"创建者的头像。例如： http://www.limian.com/a.jpg","ispraised":"是否被当前用户赞过。0: 没有被当前用户赞过。1: 被当前用户赞过","creatorname":"话题创建者昵称，例如：辰子安","userlist":[{"sex":"用户性别","nickname":"用户昵称","userid":"用户的id，例如1000036","portrait":"用户头像"},{"sex":"用户性别","nickname":"张三丰","userid":"1000037","portrait":"http://www.limian.com/portrait.jpg"}],"commentnum":"话题评论数，例如：4","creatorid":"话题创建者id，例如：1000123","creattime":"话题创建时间，UTC格式，例如：13456456","topicimgs":"话题所属图片的url拼接的字符串，半角逗号隔开，例如：http://www.yuxip.com/1.jpg,http://www.yuxip.com/2.jpg","groupid":"和话题绑定的群组id，比如：1000100"}
     */
    private String result;
    private TopicdetailEntity topicdetail;

    public void setResult(String result) {
        this.result = result;
    }

    public void setTopicdetail(TopicdetailEntity topicdetail) {
        this.topicdetail = topicdetail;
    }

    public String getResult() {
        return result;
    }

    public TopicdetailEntity getTopicdetail() {
        return topicdetail;
    }

    public class TopicdetailEntity {
        /**
         * topicid : 话题的id，例如：1
         * topictitle : 话题标题
         * iscommented : 是否被当前用户评价过。0: 没有被当前用户评价过。1: 被当前用户评价过
         * praisenum : 话题点赞数，例如：3
         * topiccontent : 话题内容
         * creatorportrait : 创建者的头像。例如： http://www.limian.com/a.jpg
         * ispraised : 是否被当前用户赞过。0: 没有被当前用户赞过。1: 被当前用户赞过
         * creatorname : 话题创建者昵称，例如：辰子安
         * userlist : [{"sex":"用户性别","nickname":"用户昵称","userid":"用户的id，例如1000036","portrait":"用户头像"},{"sex":"用户性别","nickname":"张三丰","userid":"1000037","portrait":"http://www.limian.com/portrait.jpg"}]
         * commentnum : 话题评论数，例如：4
         * creatorid : 话题创建者id，例如：1000123
         * creattime : 话题创建时间，UTC格式，例如：13456456
         * topicimgs : 话题所属图片的url拼接的字符串，半角逗号隔开，例如：http://www.yuxip.com/1.jpg,http://www.yuxip.com/2.jpg
         * groupid : 和话题绑定的群组id，比如：1000100
         */
        private String topicid;
        private String topictitle;
        private String iscommented;
        private String praisenum;
        private String topiccontent;
        private String creatorportrait;
        private String ispraised;
        private String creatorname;
        private List<UserlistEntity> userlist;
        private String commentnum;
        private String creatorid;
        private String creattime;
        private String topicimgs;
        private String groupid;

        public void setTopicid(String topicid) {
            this.topicid = topicid;
        }

        public void setTopictitle(String topictitle) {
            this.topictitle = topictitle;
        }

        public void setIscommented(String iscommented) {
            this.iscommented = iscommented;
        }

        public void setPraisenum(String praisenum) {
            this.praisenum = praisenum;
        }

        public void setTopiccontent(String topiccontent) {
            this.topiccontent = topiccontent;
        }

        public void setCreatorportrait(String creatorportrait) {
            this.creatorportrait = creatorportrait;
        }

        public void setIspraised(String ispraised) {
            this.ispraised = ispraised;
        }

        public void setCreatorname(String creatorname) {
            this.creatorname = creatorname;
        }

        public void setUserlist(List<UserlistEntity> userlist) {
            this.userlist = userlist;
        }

        public void setCommentnum(String commentnum) {
            this.commentnum = commentnum;
        }

        public void setCreatorid(String creatorid) {
            this.creatorid = creatorid;
        }

        public void setCreattime(String creattime) {
            this.creattime = creattime;
        }

        public void setTopicimgs(String topicimgs) {
            this.topicimgs = topicimgs;
        }

        public void setGroupid(String groupid) {
            this.groupid = groupid;
        }

        public String getTopicid() {
            return topicid;
        }

        public String getTopictitle() {
            return topictitle;
        }

        public String getIscommented() {
            return iscommented;
        }

        public String getPraisenum() {
            return praisenum;
        }

        public String getTopiccontent() {
            return topiccontent;
        }

        public String getCreatorportrait() {
            return creatorportrait;
        }

        public String getIspraised() {
            return ispraised;
        }

        public String getCreatorname() {
            return creatorname;
        }

        public List<UserlistEntity> getUserlist() {
            return userlist;
        }

        public String getCommentnum() {
            return commentnum;
        }

        public String getCreatorid() {
            return creatorid;
        }

        public String getCreattime() {
            return creattime;
        }

        public String getTopicimgs() {
            return topicimgs;
        }

        public String getGroupid() {
            return groupid;
        }

        public class UserlistEntity {
            /**
             * sex : 用户性别
             * nickname : 用户昵称
             * userid : 用户的id，例如1000036
             * portrait : 用户头像
             */
            private String sex;
            private String nickname;
            private String userid;
            private String portrait;

            public void setSex(String sex) {
                this.sex = sex;
            }

            public void setNickname(String nickname) {
                this.nickname = nickname;
            }

            public void setUserid(String userid) {
                this.userid = userid;
            }

            public void setPortrait(String portrait) {
                this.portrait = portrait;
            }

            public String getSex() {
                return sex;
            }

            public String getNickname() {
                return nickname;
            }

            public String getUserid() {
                return userid;
            }

            public String getPortrait() {
                return portrait;
            }
        }
    }
}
