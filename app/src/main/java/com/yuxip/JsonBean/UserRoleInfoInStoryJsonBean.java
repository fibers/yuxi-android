package com.yuxip.JsonBean;

/**
 * Created by Administrator on 2015/8/12.
 */
public class UserRoleInfoInStoryJsonBean {


    /**
     * result : 1
     * roleinfo : {"roletitle":"角色的职位信息。","roleid":"角色id","userroleinfo":"用户设定的角色的详细. 如果用户已经设定，返回用户设定的信息，如果用户没有设定，则把管理员的人设模版返回。","userrolename":"用户设定的角色的昵称"}
     * datetime : 1456789
     */
    private String result;
    private RoleinfoEntity roleinfo;
    private String datetime;

    public void setResult(String result) {
        this.result = result;
    }

    public void setRoleinfo(RoleinfoEntity roleinfo) {
        this.roleinfo = roleinfo;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getResult() {
        return result;
    }

    public RoleinfoEntity getRoleinfo() {
        return roleinfo;
    }

    public String getDatetime() {
        return datetime;
    }

    public static class RoleinfoEntity {
        /**
         * roletitle : 角色的职位信息。
         * roleid : 角色id
         * userroleinfo : 用户设定的角色的详细. 如果用户已经设定，返回用户设定的信息，如果用户没有设定，则把管理员的人设模版返回。
         * userrolename : 用户设定的角色的昵称
         */
        private String roletitle;
        private String roleid;
        private String userroleinfo;
        private String userrolename;

        public void setRoletitle(String roletitle) {
            this.roletitle = roletitle;
        }

        public void setRoleid(String roleid) {
            this.roleid = roleid;
        }

        public void setUserroleinfo(String userroleinfo) {
            this.userroleinfo = userroleinfo;
        }

        public void setUserrolename(String userrolename) {
            this.userrolename = userrolename;
        }

        public String getRoletitle() {
            return roletitle;
        }

        public String getRoleid() {
            return roleid;
        }

        public String getUserroleinfo() {
            return userroleinfo;
        }

        public String getUserrolename() {
            return userrolename;
        }
    }
}
